package com.fullnetworkbasketball;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.amap.api.services.core.LatLonPoint;
import com.fullnetworkbasketball.models.CurrentAddress;
import com.fullnetworkbasketball.models.User;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.BuildConfig;

import net.dr.qwzq.PlayerManager;

import java.lang.ref.WeakReference;

import cn.jpush.android.api.JPushInterface;


/**
 * Created by MagicBean on 2016/02/23 11:11:38
 */
public class App extends Application {
    public static App INS;
    private static User user;
    private WeakReference<Activity> mCurrentActivity;
    private CurrentAddress currentAddress;
    private int screenWidth;
    private int screenHigh;

    @Override
    public void onCreate() {
        super.onCreate();
        INS = this;
        Logger.init("football").setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE);
        SharePreHelper.getIns().initialize(this, null);
        initializeSocialSdk();
        JPushInterface.init(this);
        MultiDex.install(this);
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                mCurrentActivity = new WeakReference<Activity>(activity);

                Logger.i("showActivity onActivityCreated:" + activity.getClass().getSimpleName());
            }

            @Override
            public void onActivityStarted(Activity activity) {
                Logger.i("showActivity onActivityStarted:" + activity.getClass().getSimpleName());
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Logger.i("showActivity onActivityResumed:" + activity.getClass().getSimpleName());
                if (mCurrentActivity != null) {
                    mCurrentActivity.clear();
                    mCurrentActivity = null;
                }
                mCurrentActivity = new WeakReference<Activity>(activity);
            }

            @Override
            public void onActivityPaused(Activity activity) {
                Logger.i("showActivity onActivityPaused:" + activity.getClass().getSimpleName());
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Logger.i("showActivity onActivityStopped:" + activity.getClass().getSimpleName());
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Logger.i("showActivity onActivityDestroyed:" + activity.getClass().getSimpleName());
//                mCurrentActivity.clear();
            }
        });
    }

    private void initializeSocialSdk() {
        //微信 appid appsecret
//        PlatformConfig.setWeixin("wx47214887960b7089", "c4e6874610b2e68f8265973db902a9e8");
//        // QQ和Qzone appid appkey
//        PlatformConfig.setQQZone("1105134189", "c7zkrvggWrj3a3eo");
//        //新浪微博 appkey appsecret
//        PlatformConfig.setSinaWeibo("1510346764", "32cc9fb610a37756ed25cca5f023b0ea");
    }

    /**
     * 设置当前地址
     *
     * @param lntLng
     */
    public void setCurrentAddress(LatLonPoint lntLng,String address) {
        if (currentAddress == null) {
            currentAddress = new CurrentAddress();
        }
        currentAddress.latLng = lntLng;

        currentAddress.address =address;
    }
    public CurrentAddress getAddress(){

        return currentAddress;
    }
    public CurrentAddress getCurrentAddress(){

        return currentAddress;
    }
    public static App getInst() {
        return INS;
    }

    public User getUser() {
        if (user == null) {
            user = UserManager.getIns().getUser();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void toLogin() {
        setUser(null);
        UserManager.getIns().clearToken();
        if (mCurrentActivity != null) {
            Activity act = mCurrentActivity.get();
            if (act != null) {
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("fromHome", true);
                act.startActivity(intent);
            }
        }
    }

    public static long downloadId;

    public Activity getCurrentShowActivity() {
        return mCurrentActivity.get();
    }



    public int getScreenWidth(){
        screenWidth = ScreenUtils.getScreenW(this);
        return screenWidth;
    }
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getScreenHigh(){
        screenHigh = ScreenUtils.getScreenH(this);
        return screenHigh;
    }
    /**
     * 每个格子的长度
     */
    public double getGridLengh(){
        int  i = ScreenUtils.dp2px(App.getInst().getCurrentShowActivity(),80);
        int courtHigh = (App.getInst().getScreenHigh()- i);
        //每个格子的长度
        double gridLengh = getScreenWidth()/PlayerManager.GRID_X;
        return gridLengh;
    }

    /***
     * 右边球场需要减掉的格子数
     */
    public double rightCancelGrid(){

        int  i = ScreenUtils.dp2px(App.getInst().getCurrentShowActivity(),80);


        int courtHigh = (App.getInst().getScreenHigh()- i-App.getInst().getStatusBarHeight());
        //每个格子的长度
        double gridLengh = getScreenWidth()/PlayerManager.GRID_X;

        return  (getScreenWidth()*3/5)/gridLengh;

    }
    public double rightCancelGrid2(){
        double gridLengh = getScreenWidth()/PlayerManager.GRID_X;

        return  (getScreenWidth()*3/5)/gridLengh+1;
    }

}
