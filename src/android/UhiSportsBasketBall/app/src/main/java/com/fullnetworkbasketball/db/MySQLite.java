package com.fullnetworkbasketball.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fullnetworkbasketball.models.DynamicReplayData;
import com.fullnetworkbasketball.models.ReservationData;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.File;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2015/12/30 0030.
 */
public class MySQLite {

    DBHelper dbHelper = null;

    public MySQLite(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void addReply(DynamicReplayData data) {
        //初始化将数据写入

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();


        contentValues.put(File.PUSH_TYPE, data.getPushType());
        contentValues.put(File.DYNAMIC_ID, data.getDynamicId());
        contentValues.put(File.SENDER_ID, data.getSenderId());

        contentValues.put(File.CONTENT, data.getContent());
        contentValues.put(File.SEND_TIME, data.getSendTime());
        contentValues.put(File.SENDER_NAME, data.getSendName());

        contentValues.put(File.SENDER_HEADER, data.getSenderHeader());
        contentValues.put(File.USER_ID, UserManager.getIns().getUser().id);

        db.insert(File.TABLE_NAME, null, contentValues);
        db.close();

    }


    /**
     * 查询分页回复操作
     */
    public List<DynamicReplayData> queryReply(int pageNumber) {
        List<DynamicReplayData> dataList;
        int pageSize = 5;


        int startIndex = 0;
        int end = pageNumber * pageSize;

        String sql = "select * from " + File.TABLE_NAME + " where userId=" + UserManager.getIns().getUser().id + " order by id DESC limit " + startIndex + "," + end;


        //数据读取
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //创建游标形式依次读取数据库中的消息
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor == null) {
            db.close();
            return null;
        }
        dataList = new ArrayList<DynamicReplayData>();
        DynamicReplayData data = null;
        while (cursor.moveToNext()) {
            //依次遍历获取获取每一条数据


            data = new DynamicReplayData();

            data.setSenderHeader(cursor.getString(cursor.getColumnIndex(File.SENDER_HEADER)));

            data.setPushType(cursor.getString(cursor.getColumnIndex(File.PUSH_TYPE)));
            data.setSenderId(cursor.getString(cursor.getColumnIndex(File.SENDER_ID)));
            data.setDynamicId(cursor.getString(cursor.getColumnIndex(File.DYNAMIC_ID)));

            data.setContent(cursor.getString(cursor.getColumnIndex(File.CONTENT)));
            data.setSendTime(cursor.getString(cursor.getColumnIndex(File.SEND_TIME)));
            data.setSendName(cursor.getString(cursor.getColumnIndex(File.SENDER_NAME)));


            dataList.add(data);
        }

        cursor.close();
        db.close();
        return dataList;
    }


    /**
     * 删除数据操作 动态  dynamicId
     */


    public void DeleteData(String dynamicId) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(File.TABLE_NAME, "dynamicId=?", new String[]{dynamicId});

    }

    /**
     * 查询回复操作
     */
    public List<DynamicReplayData> queryReplyAll() {
        List<DynamicReplayData> dataList;

        String sql = "select * from " + File.TABLE_NAME + " where userId=" + UserManager.getIns().getUser().id;

        //数据读取
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //创建游标形式依次读取数据库中的消息
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor == null) {
            db.close();
            return null;
        }
        dataList = new ArrayList<DynamicReplayData>();
        DynamicReplayData data = null;
        while (cursor.moveToNext()) {
            //依次遍历获取获取每一条数据


            data = new DynamicReplayData();

            data.setSenderHeader(cursor.getString(cursor.getColumnIndex(File.SENDER_HEADER)));

            data.setPushType(cursor.getString(cursor.getColumnIndex(File.PUSH_TYPE)));
            data.setSenderId(cursor.getString(cursor.getColumnIndex(File.SENDER_ID)));
            data.setDynamicId(cursor.getString(cursor.getColumnIndex(File.DYNAMIC_ID)));

            data.setContent(cursor.getString(cursor.getColumnIndex(File.CONTENT)));
            data.setSendTime(cursor.getString(cursor.getColumnIndex(File.SEND_TIME)));
            data.setSendName(cursor.getString(cursor.getColumnIndex(File.SENDER_NAME)));


            dataList.add(data);
        }

        cursor.close();
        db.close();
        return dataList;
    }

    /**
     * 数据添加  应约 取消应约
     */

    public void addYue(ReservationData data) {
        //初始化将数据写入

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(File.SENDER_HEADER, data.getSenderHeader());
        contentValues.put(File.CONTENT, data.getContent());
        contentValues.put(File.SEND_TIME, data.getSendTime());

        contentValues.put(File.MATCH_ID, data.getMatchId());
        contentValues.put(File.PUSH_TYPE, data.getPushType());
        contentValues.put(File.USER_ID, UserManager.getIns().getUser().id);

        db.insert(File.TABLE_NAME_ONE, null, contentValues);
        db.close();
    }

    /**
     * 删除数据操作 约雪 回复  dynamicId
     */


    public void DeleteYueData(int matchId) {

        String match_id = String.valueOf(matchId);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(File.TABLE_NAME_ONE, "matchId=?", new String[]{match_id});
    }

    /**
     * 查询
     */

    public List<ReservationData> queryYue(int pageNumber) {

        List<ReservationData> dataList;

        int pageSize = 5;
        int end = pageNumber * pageSize;
        // int startIndex = (pageNumber - 1) * 5;
        int startIndex = 0;
        String sql = "select * from " + File.TABLE_NAME_ONE + " where userId=" + UserManager.getIns().getUser().id + " order by id DESC limit " + startIndex + "," + end;

        //数据读取
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //创建游标形式依次读取数据库中的消息
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor == null) {
            db.close();
            return null;
        }
        dataList = new ArrayList<ReservationData>();
        ReservationData data = null;
        while (cursor.moveToNext()) {
            //依次遍历获取获取每一条数据


            data = new ReservationData();

            data.setSenderHeader(cursor.getString(cursor.getColumnIndex(File.SENDER_HEADER)));

            data.setPushType(cursor.getString(cursor.getColumnIndex(File.PUSH_TYPE)));

            data.setMatchId(cursor.getString(cursor.getColumnIndex(File.MATCH_ID)));

            data.setContent(cursor.getString(cursor.getColumnIndex(File.CONTENT)));
            data.setSendTime(cursor.getString(cursor.getColumnIndex(File.SEND_TIME)));


            dataList.add(data);
        }

        cursor.close();
        db.close();
        return dataList;

    }

    /**
     * 查询 全部
     */

    public List<ReservationData> queryYueAll() {


        List<ReservationData> dataList;

        String sql = "select * from " + File.TABLE_NAME_ONE + " where userId=" + UserManager.getIns().getUser().id;

        //数据读取
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //创建游标形式依次读取数据库中的消息
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor == null) {
            db.close();
            return null;
        }
        dataList = new ArrayList<ReservationData>();
        ReservationData data = null;
        while (cursor.moveToNext()) {
            //依次遍历获取获取每一条数据


            data = new ReservationData();

            data.setSenderHeader(cursor.getString(cursor.getColumnIndex(File.SENDER_HEADER)));

            data.setPushType(cursor.getString(cursor.getColumnIndex(File.PUSH_TYPE)));

            data.setMatchId(cursor.getString(cursor.getColumnIndex(File.MATCH_ID)));

            data.setContent(cursor.getString(cursor.getColumnIndex(File.CONTENT)));
            data.setSendTime(cursor.getString(cursor.getColumnIndex(File.SEND_TIME)));


            dataList.add(data);
        }

        cursor.close();
        db.close();
        return dataList;

    }

}
