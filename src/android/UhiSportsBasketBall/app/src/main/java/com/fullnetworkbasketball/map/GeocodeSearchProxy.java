package com.fullnetworkbasketball.map;

import android.content.Context;
import android.widget.Toast;

import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeAddress;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;

/**
 * @author 柏江龙
 * @ClassName: GeocodeSearchProxy
 * @Description 地理位置转换
 * @date 2015-4-11
 */
public class GeocodeSearchProxy implements GeocodeSearch.OnGeocodeSearchListener {

    /**
     * 位置定位实体
     */
    private GeocodeSearch geocoderSearch;

    /**
     * android上下文
     */
    private Context mContext;

    /**
     * 定位回调接口
     */
    private OnGeocodeListener listener;

    /**
     * 设置回调接口
     *
     * @param listener 接口
     */
    public void setOnGeocodeListener(OnGeocodeListener listener) {
        this.listener = listener;
    }

    /**
     * 回调接口定义
     */
    public interface OnGeocodeListener {
        /**
         * 当通过位置转换为gps回调
         *
         * @param point
         */
        public void onGeocodeSearched(LatLonPoint point);

        /**
         * 当通过gps转换为位置
         *
         * @param address 位置
         * @param latLng  gps
         */
        public void onRegeocodeSearched(String address, LatLonPoint latLng);
    }

    /**
     * 位置代理构造函数
     *
     * @param context 上下文
     */
    public GeocodeSearchProxy(Context context) {

        this.mContext = context;

        geocoderSearch = new GeocodeSearch(mContext);
        geocoderSearch.setOnGeocodeSearchListener(this);


    }

    /**
     * 当位置定位成功之后回调
     *
     * @param result
     * @param rCode
     */
    @Override
    public void onRegeocodeSearched(RegeocodeResult result, int rCode) {
        if (rCode == 0) {
            if (result != null && result.getRegeocodeAddress() != null
                    && result.getRegeocodeAddress().getFormatAddress() != null) {
                String address = result.getRegeocodeAddress().getFormatAddress();
//                Toast.makeText(mContext, address, Toast.LENGTH_SHORT).show();
                if (listener != null) {
                    listener.onRegeocodeSearched(address, result.getRegeocodeQuery().getPoint());
                }
            }
        }
    }

    /**
     * 当位置定位成功之后回调
     *
     * @param result
     * @param rCode
     */
    @Override
    public void onGeocodeSearched(GeocodeResult result, int rCode) {
        if (rCode == 0) {
            if (result != null && result.getGeocodeAddressList() != null
                    && result.getGeocodeAddressList().size() > 0) {
                GeocodeAddress address = result.getGeocodeAddressList().get(0);
                if (listener != null) {
                    listener.onGeocodeSearched(address.getLatLonPoint());
                }
                Toast.makeText(mContext, address.getFormatAddress(), Toast.LENGTH_SHORT).show();
            }

        }
    }

    /**
     * 解析经纬度
     */
    public void parsePoint(LatLonPoint point) {
        RegeocodeQuery query = new RegeocodeQuery(point, 200,
                GeocodeSearch.AMAP);
        geocoderSearch.getFromLocationAsyn(query);
    }


}
