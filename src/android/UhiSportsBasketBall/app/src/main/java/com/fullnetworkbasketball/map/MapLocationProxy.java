package com.fullnetworkbasketball.map;

import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @ClassName: MapLocationProxy
 * @Description 地理位置定位
 * @date 2015-4-11
 */
public class MapLocationProxy implements AMapLocationListener, AMap.InfoWindowAdapter {

    /**
     * 位置监听
     */
    public List<OnLocateListener> listeners;

    /**
     * 位置管理代理
     */
    private LocationManagerProxy mAMapLocationManager;

    /**
     * 地图标记集
     */
    private List<Marker> markers;


    /**
     * 当前位置标记
     */
    private Marker current;

    /**
     * 地图
     */
    private AMap aMap;

    /**
     * 位置搜索器
     */
    private GeocodeSearch geocoderSearch;
    private MapLocationProxy locationProxy;


    /**
     * 上下文
     */
    private Context mContext;
    private List<Marker> markers2;
    private double latitude;
    private double longtitude;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public MapLocationProxy(Context context) {
        listeners = new ArrayList<OnLocateListener>();
        this.mContext = context;
        markers = new ArrayList<Marker>();
        markers2 = new ArrayList<Marker>();
    }

    /**
     * 添加位置监听器
     *
     * @param listener
     */

    public void addLocateListener(OnLocateListener listener) {
        this.listeners.add(listener);
    }

    /**
     * 位置改变回调
     *
     * @param aMapLocation
     */
    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (listeners != null && aMapLocation != null) {
            if (aMapLocation.getAMapException().getErrorCode() == 0) {
                for (OnLocateListener listener : listeners) {
                    listener.onLocationChanged(aMapLocation);// 显示系统小蓝点
                }
            }
        }

        deactivate();

    }

    /**
     * 位置改变
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {

    }

    /**
     * 状态改变
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * 获得窗口内容
     *
     * @param marker
     * @return
     */
    @Override
    public View getInfoWindow(Marker marker) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
//        View view = inflater.inflate(R.layout.map_info, null);

        return null;
    }

    /**
     * 获得窗口内容
     *
     * @param marker
     * @return
     */
    @Override
    public View getInfoContents(Marker marker) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
//        View view = inflater.inflate(R.layout.map_info, null);
        return null;
    }


    /**
     * 位置监听
     */
    public interface OnLocateListener {
        public void onLocationChanged(AMapLocation location);
    }

    public void addMarkerM(LatLng latLng) {

        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("football ")
                .draggable(false)
                .snippet("football");

        Marker marker = aMap.addMarker(options);
        marker.setObject(latLng);
        marker.showInfoWindow();
    }
    /**
     * 激活
     */
    public void activate() {
        if (mAMapLocationManager == null) {
            mAMapLocationManager = LocationManagerProxy.getInstance(mContext);
        }
        mAMapLocationManager.requestLocationData(LocationProviderProxy.AMapNetwork, 60 * 1000, -1, this);
    }


    /**
     * 解除激活
     */
    public void deactivate() {
        if (mAMapLocationManager != null) {
            mAMapLocationManager.removeUpdates(this);
            mAMapLocationManager.destroy();
        }
        mAMapLocationManager = null;
    }


    /**
     * 初始化地图
     */
    public void initMap(MapView mapView, AMap.OnMarkerClickListener markerClickListener) {
        if (aMap == null) {
            aMap = mapView.getMap();
            aMap.getUiSettings().setMyLocationButtonEnabled(false);
            aMap.getUiSettings().setZoomControlsEnabled(false);
            aMap.setMyLocationEnabled(true);
            aMap.setOnMarkerClickListener(markerClickListener);
            aMap.setInfoWindowAdapter(this);
        }


    }


    /**
     * 获得标记
     *
     * @param marker
     * @return
     */
    public List<Marker> getMarkers(Marker marker) {
        ArrayList<Marker> collection = new ArrayList<Marker>();
        Point point = aMap.getProjection().toScreenLocation(marker.getPosition());
        for (Marker m : markers) {
            Point other = aMap.getProjection().toScreenLocation(m.getPosition());
            if (Math.sqrt(Math.pow(other.x - point.x, 2) + Math.pow(other.y - point.y, 2)) < 480) {
                collection.add(m);
            }
        }
        return collection;
    }
    public List<Marker> getMarkers2(Marker marker) {
        ArrayList<Marker> collection = new ArrayList<Marker>();
        Point point = aMap.getProjection().toScreenLocation(marker.getPosition());
        for (Marker m : markers2) {
            Point other = aMap.getProjection().toScreenLocation(m.getPosition());
            if (Math.sqrt(Math.pow(other.x - point.x, 2) + Math.pow(other.y - point.y, 2)) < 480) {
                collection.add(m);
            }
        }
        return collection;
    }


    /**
     * 清楚地图上的所有标签
     */
    public void clearMarks() {
        for (Marker marker : markers) {
            marker.remove();
        }
        markers.clear();
//        aMap.clear();
    }

    /**
     * 更新当前标记
     *
     * @param latLng
     */
    public void updateCurrentMarker(LatLng latLng) {
        removeCurrentMark();
        MarkerOptions options = new MarkerOptions().anchor(0.5f, 0.5f)
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.find_address))
                .title("edrive ")
                .snippet("edrive");

        current = aMap.addMarker(options);

        current.hideInfoWindow();


        aMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                latLng, 15));


    }

    /**
     * 删除标记
     */
    public void removeCurrentMark() {
        if (current != null) {
            current.remove();
        }
    }
}
