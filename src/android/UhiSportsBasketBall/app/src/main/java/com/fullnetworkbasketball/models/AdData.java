package com.fullnetworkbasketball.models;

/**
 * Created by Administrator on 2016/6/22 0022.
 */
public class AdData {

    /**
     * 图片地址
     */
    public String srcPath;

    /**
     * 链接跳转地址
     */
    public String urlPath;

    /**
     * 广告是不是开启
     */
    public boolean isOpen;

}
