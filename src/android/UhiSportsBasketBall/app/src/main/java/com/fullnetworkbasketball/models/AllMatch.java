package com.fullnetworkbasketball.models;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/19 0019.
 */
public class AllMatch {
    public ArrayList<Match> content;

    public Pageable pageable;
    public int total;
    public int pageNumber;
    public int pageSize;
    public int totalPages;
}
