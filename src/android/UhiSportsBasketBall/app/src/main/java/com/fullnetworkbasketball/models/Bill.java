package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/6/23 0023.
 */
public class Bill implements Serializable{
    public int mid;
    public String date;
    public String address;
    public double total;
    public double averge;
    public boolean leader;
    public int leaderId;
    public ArrayList<BillPlayer> billPayDtos;

}
