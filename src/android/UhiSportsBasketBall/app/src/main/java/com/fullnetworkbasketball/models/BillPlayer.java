package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/23 0023.
 */
public class BillPlayer implements Serializable{
    public int id;
    public String name;
    public String avatar;
    public int number;
    public boolean ispay;
}
