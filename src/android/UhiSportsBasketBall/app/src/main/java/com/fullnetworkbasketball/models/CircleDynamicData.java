package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * cht
 * 圈子动态
 * Created by Administrator on 2016/6/2 0002.
 */
public class CircleDynamicData implements Serializable {


    /**
     * 动态id
     */
    public int id;
    /**
     * 内容
     */
    public String content;
    /**
     * 图片数组
     */
    public String[] pictures;
    /**
     * 评论数
     */
    public int commentNum;
    /**
     * 阅读数
     */
    public int readNum;
    /**
     * 点赞数
     */
    public int laudNum;
    /**
     * 经度
     */
    public double longitude;
    /**
     * 纬度
     */
    public double latitude;
    /**
     * 地址
     */
    public String addr;
    /**
     * 时间
     */
    public String pubTime;
    /**
     * 用户个人消息
     */
    public User user;
    public String images;
    public int userId;
}
