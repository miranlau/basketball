package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/3 0003.
 */
public class CommentDtos implements Serializable {

    /**
     * 评论id
     */
    public int id;

    /**
     * 动态id
     */
    public int dynamicid;
    /**
     * 内容
     */
    public String content;

    /**
     * 状态
     */
    public int status;
    /**
     * 时间
     */

    public String pubTime;

    /**
     * 发送者
     */
    public User sender;
}
