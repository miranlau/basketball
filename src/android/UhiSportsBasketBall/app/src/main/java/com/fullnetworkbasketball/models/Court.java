package com.fullnetworkbasketball.models;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class Court {
    public int id;
    public String name;
    public String icon;
    public String url;
    public String address;
    public String telephone_name_1;
    public String telephone_number_1;
    public int status;
    public double longitude;
    public double latitude;
    public int company_id;
    public int stars;
}
