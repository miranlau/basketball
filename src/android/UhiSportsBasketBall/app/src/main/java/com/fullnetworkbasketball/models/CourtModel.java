package com.fullnetworkbasketball.models;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class CourtModel {
    public ArrayList<Court> content;
    public Pageable pageable;
    public int pageNumber;
    public int pageSize;
    public int totalPages;
    public int total;
}
