package com.fullnetworkbasketball.models;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/28 0028.
 */
public class Display implements Serializable{
    public int id;
    public double x;
    public double y;
    public String avatar;
    public int number;
    public String name;
    public String realName;
    public long playingTime;
    public String  toJson(){
        String json = new Gson().toJson(this);
        return json;
    }
}
