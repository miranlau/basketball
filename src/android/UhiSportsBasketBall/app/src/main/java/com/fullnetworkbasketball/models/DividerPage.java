package com.fullnetworkbasketball.models;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/13 0013.
 */
public class DividerPage {
    public ArrayList<Team> content;
    public Pageable pageable;
    public int total;
    public int pageNumber;
    public int pageSize;
    public int totalPages;

}
