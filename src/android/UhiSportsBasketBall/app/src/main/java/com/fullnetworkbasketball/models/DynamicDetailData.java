package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/6/3 0003.
 */
public class DynamicDetailData implements Serializable {

    public int id;

    public String content;

    public String[] pictures;

    public int commentNum;

    public int readNum;

    public int laudNum;

    public User user;

    public double longitude;

    public double latitude;

    public String addr;

    public String pubTime;

    public ArrayList<CommentDtos> commentDtos;

}
