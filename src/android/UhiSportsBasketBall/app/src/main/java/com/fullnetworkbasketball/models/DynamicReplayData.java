package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/7 0007.
 */
public class DynamicReplayData implements Serializable {


    /**
     * 推送过来的  回复 评论 消息实体
     */

    public String pushType;

    public String senderId;

    public String dynamicId;

    public String content;

    public String senderHeader;

    public String sendName;

    public String sendTime;

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderHeader() {
        return senderHeader;
    }

    public void setSenderHeader(String senderHeader) {
        this.senderHeader = senderHeader;
    }

    public String getSendName() {
        return sendName;
    }

    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }




}
