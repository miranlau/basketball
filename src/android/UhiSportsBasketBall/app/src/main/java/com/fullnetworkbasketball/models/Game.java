package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/28 0028.
 */
public class Game implements Serializable{
    public int duration;
    public Team home;
    public Team visiting;
    public ArrayList<Display> homeStarting;
    public ArrayList<Player> homePlayers;
    public boolean isSuspend;
    public int status;
    //主队分数
    public int homeScore;
    //客队分数
    public int visitingScore;
    //赛制
    public int type;
    public int id;
    public boolean isSureFirst;
    public ArrayList<Display> visitingStarting;
    public ArrayList<Player> visitingPlayers;
    public boolean isStop;
    public ArrayList<String> actions;

}
