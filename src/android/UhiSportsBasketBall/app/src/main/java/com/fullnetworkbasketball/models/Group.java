package com.fullnetworkbasketball.models;

/**
 * Created by Administrator on 2016/6/20 0020.
 */
public class Group {
    /**
     * 球队名.
     */
    public String teamName;

    /**
     * 球队LOGO.
     */
    public String logo;

    /**
     * 群聊会话ID.
     */
    public String groupId;

    /**
     *球队ID
     */
    public int teamId;
}
