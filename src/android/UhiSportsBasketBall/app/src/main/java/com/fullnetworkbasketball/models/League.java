package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/24 0024.
 */
public class League implements Serializable {
    /**
     * 记分id
     */
    public int id;
    /**
     * 赛事名称
     */
    public String name;
    /**
     * 赛事logo
     */
    public String image;
    /**
     * 地区名称
     */
    public String areaname;
    /**
     * 联赛状态 0：报名中  1；报名结束  2；比赛中  3；比赛结束
     */
    public int status;

//    /**
//     * 报名截止
//     */
//    public String deadline;
//
    /**
     * 开始日期
     */
    public String begindate;

    /**
     * 结束日期
     */
    public String enddate;

    /**
     * 关注数
     */

    public int follows;
    /**
     * 赛制 几人制
     */
    public int matchtype;

    /**
     * 联赛详情
     */
    public String details;

    /**
     * 是否已报名 gauntlet
     */

    public boolean gauntlet;

    /**
     * 是不是已经关注
     */
    public boolean isfollow;

}
