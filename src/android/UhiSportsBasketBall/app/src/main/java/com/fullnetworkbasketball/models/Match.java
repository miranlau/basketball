package com.fullnetworkbasketball.models;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/19 0019.
 */
public class Match extends MultiItemEntity implements Serializable {

    //匹配赛进行中
    public static final int MATCH_ING=1;
    //匹配赛约赛中（赛前）
    public static final int MATCH_BEFORE=2;
    //匹配赛约赛成功（未开始比赛）
    public static final int MATCH_BEFORE_TWO=4;
    //单队赛赛前
    public static final int SINGLE=3;
    //单队赛赛后
    public static final int SINGLE_ING=5;
    //联赛
    public static final int LEAGULE=6;
    //比赛ID
    public int id;
    //赛事ID
    public int lid;
    //应约球队列表
    public ArrayList<Team> dating;
    //赛事名称
    public String leagueName;
    //主队
    public Team home;
    //客队
    public Team visiting;
    //比赛状态
    public int status;
    //比赛场地名称
    public String venue;
    //比赛时间
    public String date;
    //赛制
    public int type;
    //比赛描述
    public String desc;
    //是否有专业记分员
    public boolean isrecord;
    //主队记分员ID
    public int homerecord;
    //客队记分员ID
    public int visitingrecord;
    //比赛类型
    public int playerType;
    //主队分数
    public int homescore;
    //客队分数
    public int visitingscore;
    //地区编号
    public String areacord;
    //对手战斗力
    public String power;
    //对手年龄
    public String age;
    //主队队员列表
    public ArrayList<Player> homeusers;
    //客队队员列表
    public ArrayList<Player> visitingusers;
    public String addr;
    //是否已报名
    public boolean enroll;
    //是否已应约
    public boolean gauntlet;
    //应约球队数
    public int datings;
//    //是否有专业记分员
//    public int isrecord;

    public int visitingPenalty;
    public int homePenalty;
    public boolean isPenalty;
    public int homeTotalScore;
    public int vistingTotalScore;
}
