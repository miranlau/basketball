package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/6/24 0024.
 */
public class MatchBill implements Serializable{
    public int id;
    public int lid;
    public int home;
    public int visiting;
    public int homescore;
    public int visitingscore;
    public int status;
    public String addr;
    public String date;
    public int type;
    public ArrayList<Display> homeDisplay;
    public ArrayList<Display> visitingDisplay;
}
