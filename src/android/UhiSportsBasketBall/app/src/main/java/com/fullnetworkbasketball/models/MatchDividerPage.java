package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by cht on 2016/5/19 0019.
 *
 * 赛事分页
 */
public class MatchDividerPage implements Serializable {

    /**
     * 赛事列表
     */
    public ArrayList<League> content;

    public Pageable pageable;
    public int total;
    public int pageNumber;
    public int pageSize;
    public int totalPages;
}
