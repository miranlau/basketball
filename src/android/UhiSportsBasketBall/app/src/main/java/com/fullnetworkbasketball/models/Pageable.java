package com.fullnetworkbasketball.models;

/**
 * Created by Administrator on 2016/5/13 0013.
 */
public class Pageable {
    public int pageNumber;
    public int pageSize;
    public int total;
}
