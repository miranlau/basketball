package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class Player implements Serializable {
    //球员ID
    public int id;
    // 姓名
    public String name;
    //头像
    public String avatar;
    //身高
    public double height;
    //体重
    public double weight;
    //年龄
    public int age;
    //位置
    public String position;
    //评分
    public int stars;
    //信誉
    public double credit;
    //比赛数
    public int matches;
    //进球数
    public int scores;
    //等级
    public int level;
    //球衣号码
    public int number;

    public Radar radarDto;

    public ArrayList<Team> teams;

    public boolean follow;
    //关注数
    public int myFocus;
    //我的发表数
    public int myPublish;
    //我的粉丝数
    public int myFans;
    //允许查看
    public boolean canview;
    //真实姓名
    public String realName;
    //总得分
    public int score;
     // 总篮板
    public int backboard;

    /** 场均得分 */
    public double scoreCount;
    /** 场均盖帽*/
    public double avgCover;
    /** 场均抢断 */
    public double avgSteals;
    /** 场均篮板 */
    public double avgBackboard;
    /** 场均助攻 */
    public double avgHelp;
    /** 场均失误 */
    public double avgAnError;






}
