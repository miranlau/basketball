package com.fullnetworkbasketball.models;

/**
 * Created by Administrator on 2016/5/30 0030.
 */
public class QueryMatchFive {

    public String date;


    /**
     * 比赛类型
     */
    public int playerType;
    /**
     * 赛事id
     */
    public int id;

    /**
     * lid  10000是单对赛，10100匹配赛 其他的是联赛
     */
    public int lid;

    /**
     * 地址
     */
    public String addr;

    /**
     * 描述
     */

    public String desc;

    /**
     * 主队分数
     */
    public int homescore;

    /**
     * 客队 分数
     */

    public int visitingscore;


    /**
     * 主队
     */

    public HomeTeamData home;

    /**
     * 客队
     */
    public VisitingTeamData visiting;
    /**
     * 赛事状态
     */
    public int status;
}
