package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class Radar implements Serializable {
    public int jingong;
    public int fangshou;
    public int tineng;
    public int sudu;
    public int credit;
    public int stars;
    public double common;
}
