package com.fullnetworkbasketball.models;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/8 0008.
 * <p/>
 * 预约比赛 推送消息字段
 */
public class ReservationData implements Serializable {

    /**
     * 图片
     */
    public String senderHeader;

    /**
     * 发送时间
     */
    public String sendTime;
    /**
     * 赛事id
     */
    public String matchId;

    /**
     * 推送内容
     */
    public String content;

    /**
     * 推送类型
     */
    public String pushType;

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getSenderHeader() {
        return senderHeader;
    }

    public void setSenderHeader(String senderHeader) {
        this.senderHeader = senderHeader;
    }
}
