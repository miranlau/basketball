package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class Team implements Serializable{
    //球队ID
    public int id;
    //球队名称
    public String name;
    //球队图标
    public String logo;
    //暗号
    public String password;
    //队长ID
    public int leader;
    //队长名称
    public String leaderName;
    //平均年龄
    public double age;
    //评价
    public int stars;
    //胜场
    public int win;
    //平局次数
    public int deuce;
    //失利次数
    public int lose;
    //等级
    public int level;
    //城市
    public int city;
    //属性
    public String attr;
    //球员列表
    public ArrayList<Player> players;

    //联系方式
    public String mobile;
}
