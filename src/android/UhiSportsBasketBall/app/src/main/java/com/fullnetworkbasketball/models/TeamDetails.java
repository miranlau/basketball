package com.fullnetworkbasketball.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class TeamDetails implements Serializable{
    public ArrayList<Player> players;
    public Radar radarDto;
    public int id;
    public String name;
    public String logo;
    public String password;
    public int leader;
    public String leaderName;
    public int age;
    public int stars;
    public int win;
    public int deuce;
    public int lose;
    public int level;
    public int city;
    public String attr;
    public int right;
    public boolean follow;

    /** 场均得分 */
    public double countScore;
    /** 场均助攻*/
    public double avgHelp;
    /** 场均篮板 */
    public double avgBackboard;
    /** 场均失误 */
    public double avgAnError;
    /** 场均盖帽*/
    public double avgCover;
    /** 场均抢断 */
    public double avgSteals;

}
