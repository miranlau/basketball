package com.fullnetworkbasketball.models;

/**
 * Created by MagicBean on 2016/02/23 12:12:05
 */
public class User {
    //球员ID
    public int id;
    //姓名
    public String name;
    public String username;
    public String no;
    //手机号
    public String mobile;

    public String token;
    public String image;
    public double balance;
    //头像
    public String avatar;
    //身高
    public int height;
    //体重
    public int weight;
    //年龄
    public int age;
    //位置
    public String position;
    //评分
    public String stars;
    //信誉
    public int credit;
    //比赛数
    public int matches;
    //进球数
    public int scores;
    //等级
    public int level;
    //球衣号码
    public int number;
    //身份证号
    public String idcard;
    //是否允许陌生人查看我
    public boolean canview ;
    //是否允许陌生人邀请我
    public boolean caninvite;
    //是否允许聊天通知
    public boolean chatpush ;
    //是否自动下载更新

    public boolean autoupdate ;

//    public boolean isCanview() {
//        return canview;
//    }
//
//    public void setCanview(boolean canview) {
//        this.canview = canview;
//    }
//
//    public boolean isCanpush() {
//        return chatpush;
//    }
//
//    public void setCanpush(boolean chatpush) {
//        this.chatpush = chatpush;
//    }
//
//    public boolean isAutoupdate() {
//        return autoupdate;
//    }
//
//    public void setAutoupdate(boolean autoupdate) {
//        this.autoupdate = autoupdate;
//    }

    public boolean ishide;
    public String license;
    public String radar;
    public String email;

    public String realName;
}
