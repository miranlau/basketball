package com.fullnetworkbasketball.net;


import android.os.Message;

import com.fullnetworkbasketball.models.AdData;
import com.fullnetworkbasketball.models.AllCircleData;
import com.fullnetworkbasketball.models.AllMatch;
import com.fullnetworkbasketball.models.AppUpdateInfo;
import com.fullnetworkbasketball.models.Bill;
import com.fullnetworkbasketball.models.CourtModel;
import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.DividerPage;
import com.fullnetworkbasketball.models.DynamicDetailData;
import com.fullnetworkbasketball.models.Flag;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.GoodsTypeDetails;
import com.fullnetworkbasketball.models.GoodsTypeListEntity;
import com.fullnetworkbasketball.models.GrabEntity;
import com.fullnetworkbasketball.models.HotWords;
import com.fullnetworkbasketball.models.League;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.MatchBill;
import com.fullnetworkbasketball.models.MatchDividerPage;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.RechargeResult;
import com.fullnetworkbasketball.models.RobGoodsList;
import com.fullnetworkbasketball.models.ShareData;
import com.fullnetworkbasketball.models.SystemNotificationEntity;
import com.fullnetworkbasketball.models.SystemSetting;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.models.TeamDetails;
import com.fullnetworkbasketball.models.User;
import com.fullnetworkbasketball.ui.circle.PublishDynamicActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.ui.user.MyInfoSettingActivity;
import com.fullnetworkbasketball.updataVersion.UpdateInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


/**
 * Created by MagicBean on 2016/01/13 10:10:37
 */
public interface ApiService {


    /**
     * 篮球计分接口
     */
    @GET("game/submit")
    Call<HttpResponseFWF2> basketballMatchScore(@QueryMap HashMap<String, Object> params);


    /**
     * 登录获取验证码
     *
     * @param params
     * @return
     */
    @GET("register/getVerifyCode")
    Call<HttpResponse> loginVerCode(@QueryMap HashMap<String, Object> params);

    /**
     * 找回密码获取验证码
     *
     * @param params
     * @return
     */
    @GET("user/getVerifyCode")
    Call<HttpResponse> findVerCode(@QueryMap HashMap<String, Object> params);

    /***
     * 换人
     */
    @GET("game/change")
    Call<HttpResponseFWF2> changePeople(@Query("display") String players, @Query("teamId") int teamId, @Query("matchId") int matchId);


    /**
     * 找回密码获取验证码
     *
     * @param params
     * @return
     */
    @GET("user/findPassword")
    Call<HttpResponse> findPassword(@QueryMap HashMap<String, Object> params);

    /**
     * 注册
     */
    @GET("register/submit")
    Call<HttpResponseFWF2> registerFB(@QueryMap HashMap<String, Object> params);

    /***
     * 修改密码
     */
    @GET("user/modifyPassword")
    Call<HttpResponseFWF> modifyPWD(@QueryMap HashMap<String, Object> params);

    /**
     * 登录
     *
     * @param params
     * @return
     */
    @GET("login/submit")
    Call<HttpResponseFWF2<User>> loginFB(@QueryMap HashMap<String, Object> params);

    /***
     * 修改信息
     */
    @GET("user/modifyData")
    Call<HttpResponseFWF> modifyData(@QueryMap HashMap<String, Object> params);

    /***
     * 查询用户信息
     */
    @GET("user/getUserInfo")
    Call<HttpResponseFWF2<User>> getUserInfo(@QueryMap HashMap<String, Object> params);

    /***
     * 查询自己的信息
     */
    @GET("player/getPlayerInfo")
    Call<HttpResponseFWF2<Player>> getMyInfo(@QueryMap HashMap<String, Object> params);


    /***
     * 创建球队
     */
    @GET("team/create")
    Call<HttpResponseFWF> creamTeam(@QueryMap HashMap<String, Object> params);


    /***
     * 加入球队
     */
    @GET("team/join")
    Call<HttpResponseFWF> joinTeam(@QueryMap HashMap<String, Object> params);

    /***
     * 修改球队信息
     */
    @GET("team/modify")
    Call<HttpResponseFWF> modifyTeam(@QueryMap HashMap<String, Object> params);

    /***
     * 修改球衣号码
     */
    @GET("team/modifyNumber")
    Call<HttpResponseFWF> modifyTeamPlayerNumber(@QueryMap HashMap<String, Object> params);

    /***
     * 解散球队
     */
    @GET("team/dissolve")
    Call<HttpResponseFWF> brokeTeam(@QueryMap HashMap<String, Object> params);

    /***
     * 退出球队
     */
    @GET("team/quit")
    Call<HttpResponseFWF> quitTeam(@QueryMap HashMap<String, Object> params);


    /***
     * 踢出球队
     */
    @GET("team/kickout")
    Call<HttpResponseFWF> kickoutPlayer(@QueryMap HashMap<String, Object> params);

    /***
     * 获取球队列表
     */
    @GET("team/getAllTeam")
    Call<HttpResponseFWFArray<Team>> getTeamList(@QueryMap HashMap<String, Object> params);


    /***
     * 获取球队信息
     */
    @GET("team/getTeam")
    Call<HttpResponseFWF2<TeamDetails>> getTeamDtails(@QueryMap HashMap<String, Object> params);


    /***
     * 搜索球队
     */
    @GET("team/query")
    Call<HttpResponseFWF2<DividerPage>> searchTeam(@QueryMap HashMap<String, Object> params);


    /**
     * cht 搜索赛事
     */

    @GET("league/query")
    Call<HttpResponseFWF2<MatchDividerPage>> SearchLeague(@QueryMap HashMap<String, Object> params);

    /**
     * cht 赛事详情
     */
    @GET("league/queryById")
    Call<HttpResponseFWF2<League>> LeagueDetail(@QueryMap HashMap<String, Object> params);

    /**
     * cht 参加联赛
     */

    @GET("league/enroll")
    Call<HttpResponseFWF2> JoinLeague(@QueryMap HashMap<String, Object> params);

    /**
     * cht 取消参加联赛
     */

    @GET("league/cancelEnroll")
    Call<HttpResponseFWF2> CancelLeague(@QueryMap HashMap<String, Object> params);

    /**
     * cht 取消关注
     */

    @GET("league/unfollow")
    Call<HttpResponseFWF2> cancelAttation(@QueryMap HashMap<String, Object> params);

    /**
     * cht 关注
     */

    @GET("league/follow")
    Call<HttpResponseFWF2> Attation(@QueryMap HashMap<String, Object> params);

    /***
     * 关注球员
     */
    @GET("user/follow")
    Call<HttpResponseFWF2> attationPlayer(@QueryMap HashMap<String, Object> params);

    /***
     * 取消关注球员
     */
    @GET("user/unfollow")
    Call<HttpResponseFWF2> cancelAttationPlayer(@QueryMap HashMap<String, Object> params);

    /***
     * 关注球队
     */
    @GET("team/follow")
    Call<HttpResponseFWF2> attationTeam(@QueryMap HashMap<String, Object> params);

    /***
     * 取消关注球队
     */
    @GET("team/unfollow")
    Call<HttpResponseFWF2> cancelAttationTeam(@QueryMap HashMap<String, Object> params);

    /**
     * cht 记分员查询计分球队的所有已报名球员
     */
    @GET("game/queryAll")
    Call<HttpResponseFWFArray<Player>> haveSignedUp(@QueryMap HashMap<String, Object> params);


    /**
     * cht 删除报名球员   /game/deletePlayer
     */
    @GET("game/deletePlayer")
    Call<HttpResponseFWF2> deleteGamePlayer(@QueryMap HashMap<String, Object> params);

    /**
     * cht 查找没有报名的球员
     */
    @GET("game/queryOthers")
    Call<HttpResponseFWFArray<Player>> searchPlayers(@QueryMap HashMap<String, Object> params);


    /**
     * cht 添加报名球员
     */
    @GET("game/addPlayer")
    Call<HttpResponseFWF2> addPlayers(@Query("userId") ArrayList<Integer> list, @Query("matchId") int matchId, @Query("teamId") int teamId);

    /**
     * cht 修改比赛球员球衣号码
     */

    @GET("game/modifyNumber")
    Call<HttpResponseFWF2> modifyRacingPlayerNumber(@QueryMap HashMap<String, Object> params);

    /**
     * cht 动态评论数据
     */
    @GET("league/commentLeagueInfo?")
    Call<HttpResponseFWF2> DynamicReplyData(@QueryMap HashMap<String, Object> params);

    /**
     * cht 动态点赞
     */
    @GET("league/addLaud?")
    Call<HttpResponseFWF2> DynamicPraiseData(@QueryMap HashMap<String, Object> params);


    /**
     * cht  最近5场比赛列表
     */
    @GET("match/queryMatchByTeamId?")
    Call<HttpResponseFWFArray<Match>> QueryMatchFive(@QueryMap HashMap<String, Object> params);

    /**
     * cht 图文直播 文字消息
     * game/SendMessage？
     */
    @GET("game/sendMessage?")
    Call<HttpResponseFWF> SendMessage(@QueryMap HashMap<String, Object> params);

    /**
     * cht 图片消息
     */

    @POST("resource/upload")
    Call<HttpResponseFWF<InteractiveLiveActivity.UploadResult>> SendPhoto(@Body RequestBody photo);

    /**
     * cht 图片上传
     *
     * @param photo
     * @return
     */

    @POST("resource/upload")
    Call<HttpResponseFWF<PublishDynamicActivity.UploadImageResult>> dynamicUpload(@Body RequestBody photo);


    /**
     * cht 说说图片上传
     *
     * @return
     */

    @GET("dynamic/publish")
    Call<HttpResponseFWF2> dynamicSend(@Query("imgs") ArrayList<String> list, @Query("content") String content, @Query("longitude") double longitude, @Query("latitude") double latitude, @Query("addr") String addr);

    /**
     * cht 获取圈子全部动态
     */
    @GET("dynamic/getAllDynamicPager")
    Call<HttpResponseFWF2<AllCircleData>> getAllDynamicPager(@QueryMap HashMap<String, Object> params);

    /**
     * cht 获取动态详情
     */

    @GET("dynamic/dynamicDetail")
    Call<HttpResponseFWF2<DynamicDetailData>> dynamicDetail(@QueryMap HashMap<String, Object> params);

    /**
     * cht 动态点赞
     */
    @GET("dynamic/clickLaud")
    Call<HttpResponseFWF2> clickLaud(@QueryMap HashMap<String, Object> params);

    /**
     * cht 动态评论
     */

    @GET("dynamic/commentDynamic")
    Call<HttpResponseFWF2> commentDynamic(@QueryMap HashMap<String, Object> params);


    /**
     * cht 获取好友消息
     */

    @GET("dynamic/getFriendDynamicPager")
    Call<HttpResponseFWF2<AllCircleData>> getFriendDynamicPager(@QueryMap HashMap<String, Object> params);

    /**
     * cht 更新deviceToken
     */
    @GET("deviceToken/update")
    Call<HttpResponseFWF2> upDeviceToken(@QueryMap HashMap<String, Object> params);

    /**
     * match/queryMatchStatus
     * cht 数据比赛状态跳转
     */
    @GET("match/queryMatchStatus")
    Call<HttpResponseFWF2> queryMatchStatus(@QueryMap HashMap<String, Object> params);


    /**
     * cht 我关注的球员
     * /team/myFocusTeam
     */
    @GET("player/myFocusPlayer")
    Call<HttpResponseFWFArray<Player>> myFocusPlayer(@QueryMap HashMap<String, Object> params);

    /**
     * cht 我关注的球队
     * /team/myFocusTeam
     */
    @GET("team/myFocusTeam")
    Call<HttpResponseFWFArray<Team>> myFocusTeam(@QueryMap HashMap<String, Object> params);

    /**
     * cht 我关注的联赛
     * /team/myFocusTeam
     */
    @GET("league/myFocusLeague")
    Call<HttpResponseFWFArray<League>> myFocusLeague(@QueryMap HashMap<String, Object> params);

    /**
     * cht 我的粉丝
     * user/myFans
     */
    @GET("user/myFans")
    Call<HttpResponseFWFArray<Player>> myFans(@QueryMap HashMap<String, Object> params);

    /**
     * cht 我的发表
     * dynamic/myDynamicList
     */
    @GET("dynamic/myDynamicList")
    Call<HttpResponseFWF2<AllCircleData>> myDynamicList(@QueryMap HashMap<String, Object> params);


    /**
     * cht 删除我的发表
     */

    @GET("dynamic/deleteDynamic")
    Call<HttpResponseFWF2> deleteDynamic(@QueryMap HashMap<String, Object> params);

    /**
     * cht 获取我的赛程
     * http://localhost:8082/api/ctl/match/queryMyMatchShedule
     */
    @GET("match/queryMyMatchShedule")
    Call<HttpResponseFWF> queryMyMatchShedule(@QueryMap HashMap<String, Object> params);

    /**
     * cht   点击时出现弹窗
     * match/myMatchSheduleDetail?date=2016-01-01
     */

    @GET("match/myMatchSheduleDetail")
    Call<HttpResponseFWFArray<Match>> myMatchSheduleDetail(@QueryMap HashMap<String, Object> params);

    /**
     * cht 版本更新功能
     */
    @GET("version/getVersion")
    Call<HttpResponseFWF2<UpdateInfo>> getVersion(@QueryMap HashMap<String, Object> params);

    /**
     * cht 广告返回值
     */
    @GET("advert/get")
    Call<HttpResponseFWF2<AdData>> getAd(@QueryMap HashMap<String, Object> params);


    /**
     * cht 注销登录接口
     */
    @GET("user/logoff")
    Call<HttpResponseFWF2<AdData>> logoff(@QueryMap HashMap<String, Object> params);

    /**
     * http://192.168.1.100:8080/football-api/ctl/match/share?matchId=203577
     * cht 分享的状态
     */
    @GET("match/share?")
    Call<HttpResponseFWF2<ShareData>> share(@QueryMap HashMap<String, Object> params);


    /**
     * http://192.168.0.132:8082/football-api/ctl/address/update
     * <p>
     * cht 上传电话号码数组
     */
    @GET("address/update")
    Call<HttpResponseFWF2> updatePhone(@Query("contactList") ArrayList<String> list);


    /***
     * 创建匹配赛
     */
    @GET("match/createMatching")
    Call<HttpResponse> createMatch(@QueryMap HashMap<String, Object> params);


    /***
     * 创建单队赛
     */
    @GET("match/createSingle")
    Call<HttpResponse> creatSingleMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 查询全部比赛
     */
    @GET("match/query")
    Call<HttpResponseFWF2<AllMatch>> queryAllMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 判断是否是记分员
     */
    @GET("game/isScoring")
    Call<HttpResponseFWF2<Flag>> judgeIsScore(@QueryMap HashMap<String, Object> params);

    /***
     * 查询我的比赛
     */
    @GET("match/queryByUser")
    Call<HttpResponseFWF2<AllMatch>> queryMyMatch(@QueryMap HashMap<String, Object> params);


    /***
     * 确认首发
     */
    @GET("game/starting")
    Call<HttpResponseFWF2> startPlayerBeforeMatch(@Query("homeDisplay") String players, @Query("visitingDisplay") String player2, @Query("matchId") int matchId);

    /***
     * 篮球换人
     */
    @GET("game/change")
    Call<HttpResponseFWF2> BasketBallChangePeople(@Query("homeDisplay") String players, @Query("visitingDisplay") String player2, @Query("matchId") int matchId);
    /***
     * 球队账单接口
     */
    @GET("team/getDoneMatch")
    Call<HttpResponseFWFArray<MatchBill>> teamBillList(@QueryMap HashMap<String, Object> params);


    /**
     * 账单详情
     */
    @GET("team/getBillList")
    Call<HttpResponseFWF2<Bill>> teamBillDetails(@QueryMap HashMap<String, Object> params);

    /**
     * 提交球队账本信息
     */
    @GET("team/saveBill")
    Call<HttpResponseFWF2> commitTeamBill(@Query("playerIds") ArrayList<Integer> list, @Query("mid") int matchId, @Query("tid") int teamId, @Query("total") double total, @Query("averge") double averge);


    /***
     * 判断是否已经确认过整容
     */
    @GET("game/isStarting")
    Call<HttpResponseFWF2> isStarting(@QueryMap HashMap<String, Object> params);

    /***
     * 获取场上阵容
     */
    @GET("game/getLineup")
    Call<HttpResponseFWFArray<Display>> getLineUp(@QueryMap HashMap<String, Object> params);

    /***
     * 获取替补阵容
     */
    @GET("game/getLinedown")
    Call<HttpResponseFWF2> getLineDown(@QueryMap HashMap<String, Object> params);


    /***
     * 查询附近比赛
     */
    @GET("match/queryNear")
    Call<HttpResponseFWF2<AllMatch>> queryNearMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 比赛中数据
     */
    @GET("game/info")
    Call<HttpResponseFWF2<Game>> gaming(@QueryMap HashMap<String, Object> params);


    /***
     * 查询比赛详情
     */
    @GET("match/queryById")
    Call<HttpResponseFWF2<Match>> queryMatchDetails(@QueryMap HashMap<String, Object> params);

    /***
     * 应约比赛
     */
    @GET("match/gauntlet")
    Call<HttpResponseFWF2> joinMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 取消应约比赛
     */
    @GET("match/cancelGauntlet")
    Call<HttpResponseFWF2> cancelJoinMatch(@QueryMap HashMap<String, Object> params);

    @GET("match/cancelGauntlet")
    Call<HttpResponseFWF2> dasd(@Query("userId") List list, @Query("matchId") int matchId);

    /***
     * 修改比赛
     */
    @GET("match/modify")
    Call<HttpResponseFWF2> modifyMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 删除比赛
     */
    @GET("match/delete")
    Call<HttpResponseFWF2> deleteMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 选择应约球队
     */
    @GET("match/chooseTeam")
    Call<HttpResponseFWF2> chooseTeamForMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 球员报名参赛
     */
    @GET("match/enroll")
    Call<HttpResponseFWF2> signUpToMatch(@QueryMap HashMap<String, Object> params);

    /***
     * 球员取消报名参赛
     */
    @GET("match/cancelEnroll")
    Call<HttpResponseFWF2> cancelSignUpToMatch(@QueryMap HashMap<String, Object> params);


    /***
     * 比赛计分
     */
    @GET("game/submit")
    Call<HttpResponseFWF2> matchScore(@QueryMap HashMap<String, Object> params);

    /***
     * 球场列表
     */
    @GET("arenas/list")
    Call<HttpResponseFWF2<CourtModel>> findCourt(@QueryMap HashMap<String, Object> params);


    /***
     * 直播互动
     */

    /**
     * 微信支付
     *
     * @param params
     * @return
     */
    @FormUrlEncoded
    @POST("wechat/payment")
    Call<HttpResponse<RechargeResult>> paymentByWechat(@FieldMap HashMap<String, Object> params);

    /**
     * 支付宝支付
     *
     * @param params
     * @return
     */
    @FormUrlEncoded
    @POST("alipay/payment")
    Call<HttpResponse<RechargeResult>> paymentByAliPay(@FieldMap HashMap<String, Object> params);

    /**
     * 登录
     *
     * @param params
     * @return
     */
    @GET("appuser/login")
    Call<HttpResponse<User>> login(@QueryMap HashMap<String, Object> params);

    /**
     * 第三方登录
     *
     * @param params
     * @return
     */
    @GET("appuser/other/login")
    Call<HttpResponse<User>> threeLogin(@QueryMap HashMap<String, Object> params);

    /**
     * 首页夺宝
     *
     * @param params
     * @return
     */
    @GET("index")
    Call<HttpResponse<GrabEntity>> index(@QueryMap HashMap<String, Object> params);


    /**
     * 退出登录
     *
     * @param params
     * @return
     */
    @GET("appuser/logout")
    Call<HttpResponse> logout(@QueryMap HashMap<String, Object> params);

    /**
     * 刷新用户信息
     *
     * @param params
     * @return
     */
    @GET("appuser/details")
    Call<HttpResponse<User>> refreshUserInfo(@QueryMap HashMap<String, Object> params);

    /**
     * 分类详情列表
     *
     * @param params
     * @return
     */
    @GET("getByGoodsType")
    Call<HttpResponse<GoodsTypeListEntity>> getByGoodsType(@QueryMap HashMap<String, Object> params);

    /**
     * 头像上传(User)
     *
     * @param photo
     * @return
     */
    @POST("resource/upload")
    Call<HttpResponseFWF<MyInfoSettingActivity.UploadImageResult>> uploadFacePic(@Body RequestBody photo);

    /**
     * 头像上传(Team)
     *
     * @param photo
     * @return
     */
    @POST("resource/upload")
    Call<HttpResponseFWF> uploadFacePicTeam(@Body RequestBody photo);

    /***
     * 获取会话列表
     */
    @GET("team/getChatData")
    Call<HttpResponseFWFArray<com.fullnetworkbasketball.models.Group>> HXGroupList(@QueryMap HashMap<String, Object> params);

    /**
     * 消息中心
     *
     * @param params
     * @return
     */
    @GET("usercenter/messages")
    Call<HttpResponse<ArrayList<Message>>> message(@QueryMap Map<String, Object> params);

    /**
     * 用户信息修改
     *
     * @param params
     * @return
     */
    @GET("appuser/modify")
    Call<HttpResponse> modifyUserInfo(@QueryMap Map<String, Object> params);

    /**
     * 注册
     *
     * @param params
     * @return
     */
    @GET("register/submit")
    Call<HttpResponse<String>> register(@QueryMap Map<String, String> params);

    /**
     * 搜索热词
     *
     * @param params
     * @return
     */
    @GET("hotwords")
    Call<HttpResponse<HotWords.BaseHotWords>> hotWords(@QueryMap Map<String, Object> params);

    /**
     * 搜索
     *
     * @param params
     * @return
     */
    @GET("search/robgoods")
    Call<HttpResponse<GoodsTypeDetails>> search(@QueryMap Map<String, Object> params);

    /**
     * 清单列表
     *
     * @param params
     * @return
     */
    @GET("appuser/detailed/list")
    Call<HttpResponse<RobGoodsList>> shoppingCarList(@QueryMap Map<String, Object> params);

    /**
     * 注册推送 token
     *
     * @param params
     * @return
     */
    @GET("pushtoken/upload")
    Call<HttpResponse> registerPushtokn(@QueryMap Map<String, Object> params);


    /**
     * 检测更新
     *
     * @return
     */
    @GET("checkUpdate")
    Call<HttpResponse<AppUpdateInfo>> checkUpdate(@QueryMap Map<String, Object> params);

    /**
     * 获取系统通知
     *
     * @param params
     * @return
     */
    @GET("appuser/getPushInfo")
    Call<HttpResponse<SystemNotificationEntity.SystemNotification>> getNotfication(@QueryMap Map<String, Object> params);

    /**
     * 获取首页通知
     *
     * @param params
     * @return
     */
    @GET("appuser/pushInfoEnable")
    Call<HttpResponse<SystemNotificationEntity.SystemNotification>> getHomeNotitication(@QueryMap Map<String, Object> params);

    /**
     * 系统设置
     *
     * @param params
     * @return
     */
    @GET("getSettings")
    Call<HttpResponse<SystemSetting>> getSettings(@QueryMap Map<String, Object> params);


}
