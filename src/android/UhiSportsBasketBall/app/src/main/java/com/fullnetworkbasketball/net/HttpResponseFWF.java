package com.fullnetworkbasketball.net;


import com.fullnetworkbasketball.utils.TextUtil;

/**
 * Created by MagicBean on 2015/10/16 10:10:32
 */
public class HttpResponseFWF<T> {
    public int code;
    public int flag;
    public String msg;
//    public ArrayList<T> data = new ArrayList<>();
    public Object data;
    public boolean state;
    public static final int SUCCESS = 0;
    public static final int NO_LOGIN = 401;//未登录
    public static final int DISABLE_LOGIN = 403;//禁用

    public int getCode() {

        return code;
    }

    public Object getData() {
        return data;
    }

//    public T getDataFrist() {
//        if (TextUtil.isValidate(data))
//            return data.get(0);
//        return null;
//    }

    public boolean isSuccess() {
        if (code != SUCCESS) {
            if (code == NO_LOGIN) {
            } else if (code == DISABLE_LOGIN) {
            } else {
                if (code != 90001 || code != 90002);
            }
        }
        return code == 0 ? true : false;
    }

    public String getMessage() {
        return TextUtil.isValidate(msg) ? msg : "";
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "data=" + data +
                ", msg=" + msg +
                ", Flag=" + flag +
                ", code=" + code +
                '}';
    }
}
