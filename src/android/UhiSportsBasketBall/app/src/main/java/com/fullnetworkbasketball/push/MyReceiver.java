package com.fullnetworkbasketball.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.fullnetworkbasketball.db.MySQLite;
import com.fullnetworkbasketball.models.DynamicReplayData;
import com.fullnetworkbasketball.models.ReservationData;
import com.fullnetworkbasketball.ui.find.DynamicDetailsActivity;
import com.fullnetworkbasketball.ui.home.MessageFragment;
import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.utils.File;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2015/12/30 0030.
 */

/**
 * 广播消息传递机制
 */
public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = "JPush";


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        //注册ID
        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
            Log.i(TAG, "[MyReceiver] 接收Registration Id : " + regId);

        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            Log.i(TAG, "[MyReceiver] 接收到推送下来的自定义消息: " + bundle.getString(JPushInterface.EXTRA_MESSAGE));

            MySQLite mySQLite = new MySQLite(context);

            String data = bundle.getString(JPushInterface.EXTRA_EXTRA);

            //判断pushTyped的值 0 为评论回复  1 位赛事管家

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(data);

                String pushType = jsonObject.getString("pushType");


                if (Integer.parseInt(pushType) == 0) {

                    DynamicReplayData dynamicReplayData = new DynamicReplayData();

                    String dynamicId = jsonObject.getString(File.DYNAMIC_ID);
                    String senderId = jsonObject.getString(File.SENDER_ID);
                    String senderName = jsonObject.getString(File.SENDER_NAME);

                    String content = jsonObject.getString(File.CONTENT);
                    String senderHeader = jsonObject.getString(File.SENDER_HEADER);
                    String sendTime = jsonObject.getString(File.SEND_TIME);


                    dynamicReplayData.setDynamicId(dynamicId);
                    dynamicReplayData.setPushType(pushType);
                    dynamicReplayData.setSenderId(senderId);

                    dynamicReplayData.setContent(content);
                    dynamicReplayData.setSendName(senderName);
                    dynamicReplayData.setSendTime(sendTime);

                    dynamicReplayData.setSenderHeader(senderHeader);

                    mySQLite.addReply(dynamicReplayData);

                } else if (Integer.parseInt(pushType) == 1) {


                    ReservationData reservationData = new ReservationData();

                    String matchId = jsonObject.getString(File.MATCH_ID);

                    String content = jsonObject.getString(File.CONTENT);
                    String senderHeader = jsonObject.getString(File.SENDER_HEADER);
                    String sendTime = jsonObject.getString(File.SEND_TIME);


                    reservationData.setSenderHeader(senderHeader);
                    reservationData.setContent(content);
                    reservationData.setSendTime(sendTime);

                    reservationData.setPushType(pushType);
                    reservationData.setMatchId(matchId);

                    mySQLite.addYue(reservationData);
                } else {
                    Intent intent_refresh = new Intent();
                    intent_refresh.setAction("refresh");

                    context.sendBroadcast(intent_refresh);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.i(TAG, "content:" + data);
            processCustomMessage(context, bundle);

        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {


            Log.i(TAG, "[MyReceiver] 接收到推送下来的通知");

            int notifactionId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            Log.i(TAG, "[MyReceiver] 接收到推送下来的通知的ID: " + notifactionId);

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {


//
            String content = bundle.getString(JPushInterface.EXTRA_EXTRA);


            Log.i(TAG, "[MyReceiver] 接收到推送下来的通知:" + content);

            JSONObject jsonObject = null;

            try {
                jsonObject = new JSONObject(content);
                String pushType = jsonObject.getString("pushType");


                if (Integer.parseInt(pushType) == 0) {

                    String dynamicId = jsonObject.getString(File.DYNAMIC_ID);
                    int id = Integer.parseInt(dynamicId);

                    Intent i = new Intent(context, DynamicDetailsActivity.class);
                    i.putExtra("id", id);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                } else if (Integer.parseInt(pushType) == 1) {

                    String matchStatus = jsonObject.getString("matchStatus");
                    String matchId = jsonObject.getString(File.MATCH_ID);

                    int match_id = Integer.parseInt(matchId);
                    int status = Integer.parseInt(matchStatus);

                    Intent i = new Intent();
                    if (status == 0) {

                        i.putExtra("matchId", match_id);
                        i.setClass(context, RacingActivity.class);

                    } else if (status == 10) {
                        i.putExtra("matchId", match_id);
                        i.setClass(context, TeamDetailsSignUpActivity.class);
                    } else if (status == 20) {
                        i.putExtra("matchId", match_id);
                        i.setClass(context, TeamDetailsSignUpActivity.class);
                    } else if (status == 100) {
                        i.putExtra("matchId", match_id);
                        i.setClass(context, InteractiveLiveActivity.class);

                    }


//                    // mContext.startActivity(intent);
//
//                    Intent i = new Intent(context, PushYueActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


//            Intent i = new Intent(context, HomeActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(i);


        } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent.getAction())) {

            Log.d(TAG, "[MyReceiver] 用户收到到RICH PUSH CALLBACK: " + bundle.getString(JPushInterface.EXTRA_EXTRA));
            //在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity， 打开一个网页等..

        } else if (JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent.getAction())) {
            boolean connected = intent.getBooleanExtra(JPushInterface.EXTRA_CONNECTION_CHANGE, false);
            Log.i(TAG, "[MyReceiver]" + intent.getAction() + " connected state change to " + connected);
        } else {
            Log.d(TAG, "[MyReceiver] Unhandled intent - " + intent.getAction());
        }
    }

    public void processCustomMessage(Context context, Bundle bundle) {

        if (MessageFragment.isForeground) {
            String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);

            Log.i(TAG, "[MyReceiver]----" + extras);

            Intent intent = new Intent(MessageFragment.MESSAGE_RECEIVED_ACTION);

            intent.putExtra(MessageFragment.KEY_EXTRAS, extras);
            context.sendBroadcast(intent);
        }

    }
}
