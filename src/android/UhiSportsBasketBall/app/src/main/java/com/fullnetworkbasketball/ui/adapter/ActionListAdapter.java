package com.fullnetworkbasketball.ui.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.uhisports.basketball.R;

import java.util.List;

/**
 * Created by magicbeans2 on 16/8/10.
 */
public class ActionListAdapter extends BaseQuickAdapter<String> {
    public ActionListAdapter(int layoutResId, List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, String s) {
        baseViewHolder.setText(R.id.action_item,s);
    }
}
