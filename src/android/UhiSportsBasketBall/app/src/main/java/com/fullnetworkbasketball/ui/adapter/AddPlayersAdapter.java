package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2016/5/25 0025.
 */
public class AddPlayersAdapter extends BaseAdapter {

    public ArrayList<Player> list;
    public Activity activity;
    public int teamId;

    public int matchId;
    ArrayList<Integer> playerIdList = new ArrayList<>();
    public boolean flag;
    private Game game;

    public int type;

    static HashMap<Integer, Boolean> isCheck = new HashMap<Integer, Boolean>();

    public AddPlayersAdapter(Activity activity, ArrayList<Player> list, int teamId, int matchId,Game game) {



        this.list = list;
        this.activity = activity;
        this.teamId = teamId;

        this.matchId = matchId;

        this.game = game;


        initDate();

    }


    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {

            convertView = LayoutInflater.from(activity).inflate(R.layout.list_add_players_item, null);
            viewHolder = new ViewHolder(convertView);
            (convertView).setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        Player player = list.get(position);
        viewHolder.init(player, position, convertView);


        return convertView;
    }

    public void setFlag(boolean flag, int type) {
        this.flag = flag;
        this.type = type;

        addAllData();
    }

    class ViewHolder {

        ImageView header;
        TextView name;
        TextView number;

        CheckBox choose;
        LinearLayout lt;
        public ImageView leader;

        public ViewHolder(View view) {

            header = (ImageView) view.findViewById(R.id.header_iv);
            lt = (LinearLayout) view.findViewById(R.id.lt);

            name = (TextView) view.findViewById(R.id.name);
            number = (TextView) view.findViewById(R.id.number_tv);
            choose = (CheckBox) view.findViewById(R.id.choose_cb);
            leader = (ImageView)view.findViewById(R.id.leader);


        }

        public void init(final Player player, final int position, View convertView) {

            //  choose.setChecked(isCheck.get(position) == null ? false : true);
            if (UserManager.getIns().getUser()!=null){
                if (game.visiting!=null){
                    if (game.home.leader==player.id||game.visiting.leader==player.id){
                        leader.setVisibility(View.VISIBLE);
                    }else {
                        leader.setVisibility(View.GONE);
                    }
                }else {
                    if (game.home.leader==player.id){
                        leader.setVisibility(View.VISIBLE);
                    }else {
                        leader.setVisibility(View.GONE);
                    }
                }
            }

            if (position % 2 == 0) {
                lt.setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
            } else {
                lt.setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
            }
            ImageLoader.loadCicleImage(activity, player.avatar, R.mipmap.default_person, header);

            if (player.realName == null) {
                name.setText(player.name);
            } else {
                if (player.realName.isEmpty()) {
                    name.setText(player.name);
                } else {
                    name.setText(player.realName);
                }
            }


            number.setText(activity.getResources().getString(R.string.player_game_number) + player.number);
            Log.i("number", "位置=============================>" + position);


            final int playerId = player.id;

            if (getCheck().get(position)) {

                choose.setChecked(getCheck().get(position));

                Log.i("list", "playerId:" + playerId);
                Log.i("list", "全选长度：" + playerIdList.size());


                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (choose.isChecked()) {

                            isCheck.put(position, false);
                            setCheck(isCheck);

                            choose.setChecked(false);

                            for (int i = 0; i < playerIdList.size(); i++) {
                                if (playerIdList.get(i) == playerId) {
                                    playerIdList.remove(i);

                                    break;
                                }
                            }
                            Log.i("size", "删除后长度：" + playerIdList.size());
                        } else {

                            isCheck.put(position, true);
                            choose.setChecked(true);
                            setCheck(isCheck);

                            playerIdList.add(playerId);

                            Log.i("size", "选择后长度：" + playerIdList.size());
                        }





                        Log.i("list", "删除后长度：" + playerIdList.size());

                    }
                });


            }


            /**
             * 开始没有选择时的情况
             */
            if (!getCheck().get(position)) {

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (choose.isChecked()) {

                            isCheck.put(position, false);
                            setCheck(isCheck);

                            choose.setChecked(false);

                            for (int i = 0; i < playerIdList.size(); i++) {
                                if (playerIdList.get(i) == playerId) {
                                    playerIdList.remove(i);

                                    break;
                                }
                            }
                            Log.i("size", "删除后长度：" + playerIdList.size());
                        } else {

                            isCheck.put(position, true);
                            choose.setChecked(true);
                            setCheck(isCheck);

                            playerIdList.add(playerId);

                            Log.i("size", "选择后长度：" + playerIdList.size());
                        }
                    }
                });
            }

            choose.setChecked(getCheck().get(position));


        }


    }


    /**
     * 新增球员网络接口
     */

    public void requestNewsPlayer() {
        if (playerIdList.size() == 0) {

            T.showShort(activity, activity.getResources().getString(R.string.add_new_players));
            return;
        }


        Api.getRetrofit().addPlayers(playerIdList, matchId, teamId).enqueue(new RequestCallback<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                if (response.isSuccess()) {
                    activity.finish();
                    T.showShort(activity, response.getMessage());
                } else {
                    T.showShort(activity, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });

    }


    public HashMap<Integer, Boolean> getCheck() {
        return isCheck;
    }

    public void setCheck(HashMap<Integer, Boolean> isCheck) {
        AddPlayersAdapter.isCheck = isCheck;
    }

    private void initDate() {
        for (int i = 0; i < list.size(); i++) {
            getCheck().put(i, false);
        }
    }

    public void addAllData() {

        if (flag) {
            for (int i = 0; i < list.size(); i++) {
                Player player = list.get(i);

                playerIdList.add(player.id);
            }
        } else {
            playerIdList.removeAll(playerIdList);
        }


    }
}



