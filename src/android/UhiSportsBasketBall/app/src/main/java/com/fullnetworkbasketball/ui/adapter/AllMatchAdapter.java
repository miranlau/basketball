package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.Flag;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.ui.match.SingleMatchBeforeActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.DateUtil;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/5/19 0019.
 */
public class AllMatchAdapter extends BaseMultiItemQuickAdapter<Match> {
    private Activity activity;
    private int data1;

    public AllMatchAdapter(Activity context, List<Match> data) {
        super(data);
        this.activity = context;

        addItemType(Match.MATCH_ING, R.layout.all_match_item_one);
        addItemType(Match.MATCH_BEFORE, R.layout.all_match_item_two);
        addItemType(Match.MATCH_BEFORE_TWO, R.layout.all_match_item_four);
        addItemType(Match.SINGLE, R.layout.all_match_item_three);
        addItemType(Match.SINGLE_ING, R.layout.all_match_item_five);
        addItemType(Match.LEAGULE, R.layout.all_match_item_one);
    }

    private void judgeDate(BaseViewHolder baseViewHolder, int position) {
        TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_rv_item_date);

        if (position == 0) {
            String date = ((Match) getData().get(position)).date.substring(0, 10);
            textView.setVisibility(View.VISIBLE);
            textView.setText(getDate(date) + DateUtil.formatDisplayTime(((Match) getData().get(position)).date, "yyyy-MM-dd HH:mm:ss"));
//          DateUtil.formatDisplayTime(date,"yyyy-MM-dd HH:mm:ss")
            if (date.equals(getStringDateShort())) {
                textView.setBackgroundResource(R.mipmap.include_yellow);
                textView.setText(getDate(date) + DateUtil.formatDisplayTime(((Match) getData().get(position)).date, "yyyy-MM-dd HH:mm:ss"));
            } else {
                textView.setBackgroundResource(R.mipmap.include_blue);
            }
        } else {
            String currentDate = ((Match) getData().get(position)).date.substring(0, 10);
            int preIndex = position - 1;
            String preDate = ((Match) getData().get(preIndex)).date.substring(0, 10);
            boolean isDifferent = !preDate.equals(currentDate);

            if (isDifferent) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(getDate(currentDate) + DateUtil.formatDisplayTime(((Match) getData().get(position)).date, "yyyy-MM-dd HH:mm:ss"));
                if (currentDate.equals(getStringDateShort())) {
                    textView.setBackgroundResource(R.mipmap.include_yellow);
                    textView.setText(getDate(currentDate) + DateUtil.formatDisplayTime(((Match) getData().get(position)).date, "yyyy-MM-dd HH:mm:ss"));
//                    textView.setText(getDate(currentDate) + DateUtil.getTimeC(((Match) getData().get(position)).date));
                }
            } else {
                textView.setVisibility(View.GONE);
                textView.setBackgroundResource(R.mipmap.include_blue);
            }
        }

    }

    public static String getDate(String date) {
        String sub = date.substring(5, 7) + "月" + date.substring(8, 10) + "日";
        return sub;
    }

    public static String getDateMilli(String date) {
        String sub = date.substring(11, 16);
        return sub;
    }

    /**
     * 获取现在时间
     *
     * @return 返回短时间字符串格式yyyy-MM-dd
     */
    public static String getStringDateShort() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    private void judgeIsScoring(final Match match, final BaseViewHolder baseViewHolder) {
        if (UserManager.getIns().getUser() == null) {

            Intent intent = new Intent(activity, InteractiveLiveActivity.class);
            intent.putExtra("matchId", match.id);
            if (match.status == 100) {
                intent.putExtra("isEnd", true);
            }
            activity.startActivity(intent);
            return;

        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", match.id);
        Api.getRetrofit().judgeIsScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Flag>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Flag> response) {
                if (response.isSuccess()) {
                    Flag f= response.getData();
                    if (match.status == 100) {
                        Intent intent = new Intent(activity, InteractiveLiveActivity.class);
                        intent.putExtra("matchId", match.id);
                        intent.putExtra("isEnd", true);
                        activity.startActivity(intent);

                        return;
                    }
                    if (f.isScoring == 0) {

                        Intent intent = new Intent(activity, InteractiveLiveActivity.class);
                        intent.putExtra("matchId", match.id);
                        activity.startActivity(intent);
                    } else {
                        switch (baseViewHolder.getItemViewType()){
                            case Match.SINGLE_ING:
                                Intent intent = new Intent(activity, SingleMatchBeforeActivity.class);
                                intent.putExtra("matchId", match.id);
                                activity.startActivity(intent);
                                break;
                            case Match.MATCH_ING:
                                Intent intent1 = new Intent(activity, TeamDetailsSignUpActivity.class);
                                intent1.putExtra("matchId", match.id);
                                activity.startActivity(intent1);
                                break;
                        }

                    }


                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void convert(final BaseViewHolder baseViewHolder, final Match match) {
        RelativeLayout relativeLayout = (RelativeLayout) baseViewHolder.getConvertView().findViewById(R.id.home_page_rv_item_info_of_game);
        if (baseViewHolder.getLayoutPosition() % 2 == 0) {

            relativeLayout.setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
        } else {
            relativeLayout.setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
        }
        if (baseViewHolder.getLayoutPosition() == 0) {

        }
        Log.i("type", "位置-----》" + baseViewHolder.getLayoutPosition() + "类型-----》" + baseViewHolder.getItemViewType() + "");
        switch (baseViewHolder.getItemViewType()) {

            case Match.MATCH_ING:
                judgeDate(baseViewHolder, baseViewHolder.getLayoutPosition());
                baseViewHolder.setText(R.id.home_page_tv_left_teamName, match.home.name);
                ImageView header_host = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left);
                ImageLoader.loadCicleImage(activity, match.home.logo, R.mipmap.default_team, header_host);
                TextView textview2 = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                textview2.setTextSize(20);
                if (match.status == 21) {
                    baseViewHolder.setTextColor(R.id.home_page_tv_score, activity.getResources().getColor(R.color.red));
                    baseViewHolder.setText(R.id.home_page_tv_score,"即将开赛");
                } else {
                    baseViewHolder.setTextColor(R.id.home_page_tv_score, activity.getResources().getColor(R.color.green));
                    baseViewHolder.setText(R.id.home_page_tv_score,"比赛中");
                }

                if (match.playerType == 2) {

//                    baseViewHolder.setText(R.id.home_page_tv_association, match.leagueName);
                    TextView textView = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_address);
                    textView.setVisibility(View.GONE);
                    TextView textView1 = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_association);
                    textView1.setText(match.leagueName);


                } else {
//                    baseViewHolder.setText(R.id.home_page_tv_association, match.type + activity.getResources().getString(R.string.system));
                    TextView textView = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_address);
                    textView.setVisibility(View.VISIBLE);
                    TextView textView1 = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_association);
                    textView1.setText(match.type + activity.getResources().getString(R.string.system));
                    textView1.setSingleLine();
                }
                baseViewHolder.setText(R.id.home_page_tv_address, match.addr);
                ImageView header_visit = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_right);
                if (match.visiting == null) {
                    ImageLoader.loadCicleImage(activity, R.mipmap.default_team, header_visit);
                    baseViewHolder.setText(R.id.home_page_tv_right_teamName, "");
                } else {
                    ImageLoader.loadCicleImage(activity, match.visiting.logo, R.mipmap.default_team, header_visit);
                    baseViewHolder.setText(R.id.home_page_tv_right_teamName, match.visiting.name);
                }
                ImageView home_page_iv_team_left_winner = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left_winner);
                ImageView home_page_iv_team_right_winner = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_right_winner);
                if (match.status == 100) {
                    baseViewHolder.setTextColor(R.id.home_page_tv_score, activity.getResources().getColor(R.color.white));
                    TextView textview = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                    textview.setTextSize(36);
                    if (match.isPenalty) {
                        baseViewHolder.setText(R.id.home_page_tv_score, match.homeTotalScore + " : " + match.vistingTotalScore);
                        TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_DQ);
                        textView.setVisibility(View.VISIBLE);
                    } else {
                        baseViewHolder.setText(R.id.home_page_tv_score, match.homescore + " : " + match.visitingscore);
                        TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_DQ);
                        textView.setVisibility(View.GONE);
                    }
                    if (match.homeTotalScore > match.vistingTotalScore) {
                        home_page_iv_team_left_winner.setVisibility(View.GONE);
                        home_page_iv_team_right_winner.setVisibility(View.GONE);
                    } else if (match.homeTotalScore < match.vistingTotalScore) {
                        home_page_iv_team_right_winner.setVisibility(View.GONE);
                        home_page_iv_team_left_winner.setVisibility(View.GONE);
                    } else {
                        home_page_iv_team_right_winner.setVisibility(View.GONE);
                        home_page_iv_team_left_winner.setVisibility(View.GONE);
                    }

                } else {
                    home_page_iv_team_right_winner.setVisibility(View.GONE);
                    home_page_iv_team_left_winner.setVisibility(View.GONE);
                    TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_DQ);
                    textView.setVisibility(View.GONE);
                }

                header_host.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.home.id);
                        activity.startActivity(intent);
                    }
                });
                header_visit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.visiting.id);
                        activity.startActivity(intent);
                    }
                });
                baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        judgeIsScoring(match,baseViewHolder);

                    }
                });
                break;
            case Match.MATCH_BEFORE:
                judgeDate(baseViewHolder, baseViewHolder.getLayoutPosition());
                baseViewHolder.setText(R.id.home_page_tv_left_teamName, match.home.name);
                ImageView header_left = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left);
                ImageLoader.loadCicleImage(activity, match.home.logo, R.mipmap.default_team, header_left);
//                baseViewHolder.setText(R.id.home_page_tv_score, match.date);
                TextView home_page_tv_score = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                home_page_tv_score.setText(getDateMilli(match.date));
                home_page_tv_score.setTextColor(activity.getResources().getColor(R.color.orange));
                baseViewHolder.setText(R.id.home_page_iv_team_right_agree, match.datings + "队应约");


                if (match.gauntlet && match != null) {
                    LinearLayout ll_home_page_iv_team_right_agree = (LinearLayout) baseViewHolder.getConvertView().findViewById(R.id.ll_home_page_iv_team_right_agree);
                    ll_home_page_iv_team_right_agree.setVisibility(View.VISIBLE);
                }
                if (match.playerType == 2) {
                    baseViewHolder.setText(R.id.home_page_tv_association, match.leagueName);

                } else {
                    baseViewHolder.setText(R.id.home_page_tv_association, match.type + activity.getResources().getString(R.string.system));
                }
                baseViewHolder.setText(R.id.home_page_tv_address, match.addr);
                header_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.home.id);
                        activity.startActivity(intent);
                    }
                });
                baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, RacingActivity.class);
                        intent.putExtra("match", match);

                        intent.putExtra("matchId", match.id);
                        activity.startActivity(intent);
                    }
                });
                break;
            case Match.MATCH_BEFORE_TWO:
                judgeDate(baseViewHolder, baseViewHolder.getLayoutPosition());
                baseViewHolder.setText(R.id.home_page_tv_left_teamName, match.home.name);
                ImageView header_left_ = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left);
                ImageLoader.loadCicleImage(activity, match.home.logo, R.mipmap.default_team, header_left_);
//                baseViewHolder.setText(R.id.home_page_tv_score, match.date);
                TextView home_page_tv_score_ = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                home_page_tv_score_.setText(getDateMilli(match.date));
                home_page_tv_score_.setTextColor(activity.getResources().getColor(R.color.orange));
//                baseViewHolder.setText(R.id.home_page_iv_team_right_agree,match.datings+"队应约");
//                baseViewHolder.setText(R.id.home_page_tv_association, match.leagueName);
//                baseViewHolder.setText(R.id.home_page_tv_address,match.addr);
                ImageView header_visit_ = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_right);
                if (match.visiting == null) {
                    ImageLoader.loadCicleImage(activity, R.mipmap.default_team, header_visit_);
                    baseViewHolder.setText(R.id.home_page_tv_right_teamName, "");
                } else {
                    ImageLoader.loadCicleImage(activity, match.visiting.logo, R.mipmap.default_team, header_visit_);
                    baseViewHolder.setText(R.id.home_page_tv_right_teamName, match.visiting.name);
                }

//                if (match.gauntlet&&match!=null){
//                    LinearLayout ll_home_page_iv_team_right_agree = (LinearLayout) baseViewHolder.getConvertView().findViewById(R.id.ll_home_page_iv_team_right_agree);
//                    ll_home_page_iv_team_right_agree.setVisibility(View.VISIBLE);
//                }
                if (match.playerType == 2) {
//                    baseViewHolder.setText(R.id.home_page_tv_association, match.leagueName);
                    TextView textView = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_address);
                    textView.setVisibility(View.GONE);
                    TextView textView1 = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_association);
                    textView1.setText(match.leagueName);
                } else {
                    TextView textView = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_address);
                    textView.setVisibility(View.VISIBLE);
//                    baseViewHolder.setText(R.id.home_page_tv_association, match.type + activity.getResources().getString(R.string.system));
                    TextView textView1 = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_association);
                    textView1.setText(match.type + activity.getResources().getString(R.string.system));
                    textView1.setSingleLine();
                }
                baseViewHolder.setText(R.id.home_page_tv_address, match.addr);
                header_left_.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.home.id);
                        activity.startActivity(intent);
                    }
                });
                header_visit_.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.visiting.id);
                        activity.startActivity(intent);
                    }
                });
                baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, TeamDetailsSignUpActivity.class);
                        intent.putExtra("matchId", match.id);
                        activity.startActivity(intent);
                    }
                });
                break;
            case Match.SINGLE:
                judgeDate(baseViewHolder, baseViewHolder.getLayoutPosition());
                baseViewHolder.setText(R.id.home_page_tv_left_teamName, match.home.name);
                ImageView header_ = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left);
                ImageLoader.loadCicleImage(activity, match.home.logo, R.mipmap.default_team, header_);
//                baseViewHolder.setTextI(R.id.home_page_tv_score, match.date);
                TextView home_page_tv_score__ = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                home_page_tv_score__.setText(getDateMilli(match.date));
                home_page_tv_score__.setTextColor(activity.getResources().getColor(R.color.orange));
                baseViewHolder.setText(R.id.home_page_tv_association, match.type + activity.getResources().getString(R.string.system));
                baseViewHolder.setText(R.id.home_page_tv_address, match.addr);
                header_.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.home.id);
                        activity.startActivity(intent);
                    }
                });
                baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, SingleMatchBeforeActivity.class);
                        intent.putExtra("matchId", match.id);
                        activity.startActivity(intent);
                    }
                });
                break;
            case Match.SINGLE_ING:
                judgeDate(baseViewHolder, baseViewHolder.getLayoutPosition());
                baseViewHolder.setText(R.id.home_page_tv_left_teamName, match.home.name);
                ImageView header_S = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left);
                ImageLoader.loadCicleImage(activity, match.home.logo, R.mipmap.default_team, header_S);
//                baseViewHolder.setText(R.id.home_page_tv_score, match.date);
                TextView home_page_tv_score_single = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                TextView textview = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                textview.setTextSize(20);
                if (match.status == 21) {
                    home_page_tv_score_single.setTextColor(activity.getResources().getColor(R.color.red));
                    home_page_tv_score_single.setText("即将开赛");
                } else {
                    home_page_tv_score_single.setTextColor(activity.getResources().getColor(R.color.green));
                    home_page_tv_score_single.setText("比赛中");
                }

                baseViewHolder.setText(R.id.home_page_tv_association, match.type + activity.getResources().getString(R.string.system));
                baseViewHolder.setText(R.id.home_page_tv_address, match.addr);
                header_S.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, OtherTeamDetailsActivity.class);
                        intent.putExtra("teamId", match.home.id);
                        activity.startActivity(intent);
                    }
                });
                if (match.status == 100) {
                    ImageView home_page_iv_team_left_winner1 = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.home_page_iv_team_left_winner);
                    home_page_tv_score_single.setTextColor(activity.getResources().getColor(R.color.white));

                    TextView textview1 = (TextView)baseViewHolder.getConvertView().findViewById(R.id.home_page_tv_score);
                    textview1.setTextSize(36);
                    if (match.isPenalty) {
                        home_page_tv_score_single.setText(match.homeTotalScore + " : " + match.vistingTotalScore);
                        TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_DQ);
                        textView.setVisibility(View.VISIBLE);
                    } else {
                        home_page_tv_score_single.setText(match.homescore + " : " + match.visitingscore);
                        TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_DQ);
                        textView.setVisibility(View.GONE);
                    }
                    if (match.homeTotalScore > match.vistingTotalScore) {
                        home_page_iv_team_left_winner1.setVisibility(View.VISIBLE);
                    } else if (match.homeTotalScore <= match.vistingTotalScore) {
                        home_page_iv_team_left_winner1.setVisibility(View.GONE);
                    } else {
                        home_page_iv_team_left_winner1.setVisibility(View.GONE);
                    }

                } else {
                    TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_DQ);
                    textView.setVisibility(View.GONE);
                }
                baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        judgeIsScoring(match,baseViewHolder);

                    }
                });
                break;
        }
    }


}
