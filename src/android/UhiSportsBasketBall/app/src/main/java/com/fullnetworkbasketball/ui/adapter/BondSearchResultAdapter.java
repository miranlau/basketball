package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.fullnetworkbasketball.views.ObserverHScrollView;
import com.uhisports.basketball.R;

import org.json.JSONObject;

/**
 * Created by Administrator on 2016/4/26 0026.
 */
public class BondSearchResultAdapter extends BaseAdapter {
    private  Context context;
        public BondSearchResultAdapter(Context context){
            this.context = context;
        }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 查找控件
        ViewHolder holder = null;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.activity_hvscorll_listview_item, null);
            holder = new ViewHolder();


            //列表水平滚动条
            ObserverHScrollView scrollView1 = (ObserverHScrollView) convertView.findViewById(R.id.horizontalScrollView1);
            holder.scrollView = (ObserverHScrollView) convertView.findViewById(R.id.horizontalScrollView1);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // 装填数据
        JSONObject data = (JSONObject) getItem(position);
        try {
            holder.txt1.setText(data.optString("zqmc"));
            holder.txt2.setText(data.optString("zqjc"));
            holder.txt3.setText(data.optString("zqdm"));
            holder.txt4.setText(data.optString("fxjc"));
            holder.txt5.setText(data.optString("ksr"));
            holder.txt6.setText(data.optString("fxjg"));
            holder.txt7.setText(data.optString("sjfxze"));
            holder.txt8.setText(data.optString("jxfs"));
            holder.txt9.setText(data.optString("fxzq"));
            holder.txt10.setText(data.optString("pmll"));
            holder.txt11.setText(data.optString("lc"));
            holder.txt12.setText(data.optString("zjll"));
            holder.txt13.setText(data.optString("zqqx"));
            holder.txt14.setText(data.optString("zqpj"));
            holder.txt15.setText(data.optString("ztpj"));
            holder.txt16.setText(data.optString("zqpjjg"));
            holder.txt17.setText(data.optString("ztpjjg"));
            holder.txt18.setText(data.optString("zqzwdjr"));
            holder.txt19.setText(data.optString("ltr"));
            holder.txt20.setText(data.optString("jzghr"));
            holder.txt21.setText(data.optString("qxr"));
            holder.txt22.setText(data.optString("dqr"));
        } catch (Exception e) {
        }

        return convertView;
    }



    private class ViewHolder {
        TextView txt1;
        TextView txt2;
        TextView txt3;
        TextView txt4;
        TextView txt5;
        TextView txt6;
        TextView txt7;
        TextView txt8;
        TextView txt9;
        TextView txt10;
        TextView txt11;
        TextView txt12;
        TextView txt13;
        TextView txt14;
        TextView txt15;
        TextView txt16;
        TextView txt17;
        TextView txt18;
        TextView txt19;
        TextView txt20;
        TextView txt21;
        TextView txt22;
        HorizontalScrollView scrollView;
    }
}
