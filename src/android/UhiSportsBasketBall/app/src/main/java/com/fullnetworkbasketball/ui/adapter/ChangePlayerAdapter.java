package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/5/31 0031.
 */
public class ChangePlayerAdapter extends BaseAdapter {

    // 用来控制CheckBox的选中状况
    private Map<Integer, Boolean> isSelected;
    //用来存放选中的对象
    private ArrayList<Player> beSelectedData = new ArrayList();
    private ArrayList<Player> players;
    //供adapter获取选中数据的方法
    public ArrayList<Player> getBeSelectedData() {
        return beSelectedData;
    }
    private Activity context;
    //初始化Adapter的同时初始化Map，将Map中所有数据置为false，对应CheckBox的未选中状态
    public ChangePlayerAdapter(Activity context,ArrayList<Player> players) {
        this.context = context;
        this.players = players;
        isSelected = new HashMap<Integer, Boolean>();
        for (int i = 0; i < players.size(); i++) {
            isSelected.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder=null;

        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.start_players_item,parent,false);
            holder=new ViewHolder();
            holder.choose= (ImageView) convertView.findViewById(R.id.iv_choose);
            holder.number = (TextView)convertView.findViewById(R.id.number);
            holder.name = (TextView)convertView.findViewById(R.id.name);
            holder.iv_leader = (ImageView)convertView.findViewById(R.id.player_header);
            holder.itelRL = (LinearLayout)convertView.findViewById(R.id.lt);
            holder.player_header = (ImageView)convertView.findViewById(R.id.player_header);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        if (players.get(position).realName==null){
            holder.name.setText(players.get(position).name);
        }else {
            if (!players.get(position).realName.isEmpty()){
                holder.name.setText(players.get(position).realName);
            }else {
                holder.name.setText(players.get(position).name);
            }
        }


        holder.number.setText(players.get(position).number + "");
        ImageLoader.loadCicleImage(context,players.get(position).avatar,R.mipmap.default_person,holder.player_header);
//        if (players.get(position).id)
        final ViewHolder finalHolder = holder;
        //给Item布局设置点击事件，这里并不操作CheckBox视图，而是操作CheckBox对应的Map容器
        holder.itelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取map中对应位置的布尔值，取其相反值（因为点击一次我们要改变其状态）
                boolean isSelect=!isSelected.get(position);
                // 先将所有的置为FALSE（通过遍历HashMap将所有CheckBox对应的布尔值都设为false）
                for(Integer p : isSelected.keySet()) {
                    isSelected.put(p, false);
                }
                // 再将当前选择CB的实际状态
                isSelected.put(position, isSelect);
                notifyDataSetChanged();

                beSelectedData.clear();
                if(isSelect) beSelectedData.add(players.get(position));

            }
        });

        //这里才是设置CheckBox的选中状态，根据上面处理好的Map容器中的数据显示
//        holder.checkBox.setChecked(isSelected.get(position));
            if (isSelected.get(position)){
                holder.choose.setVisibility(View.VISIBLE);
            }else {
                holder.choose.setVisibility(View.INVISIBLE);
            }
        return convertView;
    }

    class ViewHolder{
        ImageView choose;
        LinearLayout itelRL;
        TextView number;
        ImageView player_header;
        ImageView iv_leader;
        TextView name;

    }
}