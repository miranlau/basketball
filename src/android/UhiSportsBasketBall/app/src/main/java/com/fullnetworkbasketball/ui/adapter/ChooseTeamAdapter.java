package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class ChooseTeamAdapter extends RecyclerView.Adapter<ChooseTeamAdapter.ViewHolder> {

    private Context context;

    public ChooseTeamAdapter(Context context) {
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_team_item_layout, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        holder.choose_team_name.setText("safa");
    }

    @Override
    public int getItemCount() {
        return 9;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
       public TextView choose_team_name;
        public ImageView choose_team_iv;

        public ViewHolder(View itemView) {
            super(itemView);
            choose_team_name = (TextView)itemView.findViewById(R.id.choose_team_name);
            choose_team_iv = (ImageView)itemView.findViewById(R.id.choose_team_iv);
        }
    }
}
