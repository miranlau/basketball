package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.CircleDynamicData;
import com.fullnetworkbasketball.ui.find.DynamicDetailsActivity;
import com.fullnetworkbasketball.ui.user.PlayerPersonalActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.uhisports.basketball.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/6/2 0002.
 */
public class CircleFragmentAdapter extends BaseQuickAdapter<CircleDynamicData> {
    private Activity activity;

    private int flag;

    public CircleFragmentAdapter(Activity context, int layoutResId, List<CircleDynamicData> data, int flag) {
        super(layoutResId, data);

        this.activity = context;


        this.flag = flag;
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, final CircleDynamicData circleDynamicData) {

        CircleImageView header = (CircleImageView) baseViewHolder.getConvertView().findViewById(R.id.header_iv);
        TextView name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.name_tv);
        TextView time = (TextView) baseViewHolder.getConvertView().findViewById(R.id.time_formate_tv);

        TextView content = (TextView) baseViewHolder.getConvertView().findViewById(R.id.content_tv);
        TextView read = (TextView) baseViewHolder.getConvertView().findViewById(R.id.read_num_tv);
        TextView comment = (TextView) baseViewHolder.getConvertView().findViewById(R.id.commit_num_tv);

        TextView praise = (TextView) baseViewHolder.getConvertView().findViewById(R.id.praise_num_tv);
        LinearLayout li = (LinearLayout) baseViewHolder.getConvertView().findViewById(R.id.image_list_ll);
        LinearLayout nextLi = (LinearLayout) baseViewHolder.getConvertView().findViewById(R.id.next_ll);


        if (circleDynamicData.user == null) {

        } else {


            ImageLoader.loadImage(activity, circleDynamicData.user.avatar, R.mipmap.default_person, header);
        }


        //判断是不是能够查看个人消息

        // final boolean isCanView = circleDynamicData.user.canview;


        header.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {

                                          Intent intent = new Intent(mContext, PlayerPersonalActivity.class);
                                          intent.putExtra("playerId", circleDynamicData.user.id);
                                          mContext.startActivity(intent);
//                //  if (!isCanView) {
//                if (UserManager.getIns().getUser().id == circleDynamicData.user.id) {
//                    Intent intent = new Intent(mContext, PlayerPersonalActivity.class);
//                    intent.putExtra("playerId", circleDynamicData.user.id);
//                    mContext.startActivity(intent);
//                } else if (isCanView) {
//                    Intent intent = new Intent(mContext, PlayerPersonalActivity.class);
//                    intent.putExtra("playerId", circleDynamicData.user.id);
//                    mContext.startActivity(intent);
//                } else {
//                    T.showShort(activity, activity.getResources().getString(R.string.isCanView));
//                }
                                      }

                                  }

        );

        name.setText(circleDynamicData.user.name);
        time.setText(circleDynamicData.pubTime);

        content.setText(circleDynamicData.content);
        read.setText(circleDynamicData.readNum + "");
        comment.setText(circleDynamicData.commentNum + " ");

        praise.setText(circleDynamicData.laudNum + "");

//
//        if (UserManager.getIns().getUser().id == circleDynamicData.user.id) {
//            SharePreHelper.getIns().putIsMyPublish(true);
//        } else {
//            SharePreHelper.getIns().putIsMyPublish(false);
//        }
//        if (flag == 0) {
//            SharePreHelper.getIns().putIsMyPublish(true);
//        } else {
//            SharePreHelper.getIns().putIsMyPublish(false);
//        }
        nextLi.setOnClickListener(new View.OnClickListener()

                                  {
                                      @Override
                                      public void onClick(View view) {

                                          if (UserManager.getIns().getUser() != null && UserManager.getIns().getUser().id == circleDynamicData.user.id) {

                                              SharePreHelper.getIns().putIsMyPublish(true);

                                          } else {
                                              SharePreHelper.getIns().putIsMyPublish(false);
                                          }
                                          Intent intent = new Intent();
                                          intent.setClass(activity, DynamicDetailsActivity.class);
                                          intent.putExtra("id", circleDynamicData.id);
                                          activity.startActivity(intent);
                                      }
                                  }

        );
        li.removeAllViews();

        if (circleDynamicData.pictures == null)

        {

        } else

        {
            initAddView(circleDynamicData.pictures, li);
        }


        if (baseViewHolder.getLayoutPosition() % 2 == 0)

        {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
        } else

        {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
        }

    }

    public void initAddView(String[] pictureList, LinearLayout li) {

        li.removeAllViews();

        int size = pictureList.length;
        if (size > 3) {

            for (int i = 0; i < 3; i++) {

                View view = View.inflate(activity, R.layout.include_circle_list_image, null);
                ImageView picture = (ImageView) view.findViewById(R.id.image_iv);
                TextView account = (TextView) view.findViewById(R.id.accout_iv);


                ImageLoader.loadImaged(activity, pictureList[i], R.mipmap.load_ing, R.mipmap.load_failed, picture);
                if (i == 2) {
                    account.setVisibility(View.VISIBLE);
                    account.setText("共" + size + "张");
                }


                li.addView(view);
            }
        } else {
            for (int i = 0; i < size; i++) {

                View view = View.inflate(activity, R.layout.include_circle_list_image, null);
                ImageView picture = (ImageView) view.findViewById(R.id.image_iv);
                TextView account = (TextView) view.findViewById(R.id.accout_iv);

                account.setVisibility(View.GONE);
                ImageLoader.loadImaged(activity, pictureList[i], R.mipmap.load_ing, R.mipmap.load_failed, picture);
                li.addView(view);

            }
        }

    }

}
