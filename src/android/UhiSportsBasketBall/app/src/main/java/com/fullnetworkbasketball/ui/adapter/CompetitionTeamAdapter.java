package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.Utility;
import com.uhisports.basketball.R;

import java.util.List;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class CompetitionTeamAdapter extends BaseQuickAdapter<Team> {

    private Activity context;


    public CompetitionTeamAdapter(Activity context, int layoutResId, List<Team> data) {
        super( layoutResId, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final Team team) {
        ImageView team_header = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.team_header);
        TextView teamName = (TextView) baseViewHolder.getConvertView().findViewById(R.id.team_name);
        TextView team_record = (TextView) baseViewHolder.getConvertView().findViewById(R.id.team_record);

        TextView team_level = (TextView) baseViewHolder.getConvertView().findViewById(R.id.team_level);
        TextView team_avg_age = (TextView) baseViewHolder.getConvertView().findViewById(R.id.team_avg_age);
        RatingBar ratingBar = (RatingBar) baseViewHolder.getConvertView().findViewById(R.id.rb_evaluation_racing);


        ImageLoader.loadCicleImage(context, team.logo, R.mipmap.default_team, team_header);
        teamName.setText(team.name);
        team_record.setText(context.getResources().getString(R.string.record) + team.win + context.getResources().getString(R.string.win) + "  " + team.lose + context.getResources().getString(R.string.lose));

        team_level.setText(team.level + context.getResources().getString(R.string.team_level));
        team_avg_age.setText(context.getResources().getString(R.string.avg_age) + team.age);


        ratingBar.setRating(Utility.formatInt(team.stars));
        if (baseViewHolder.getLayoutPosition() % 2 == 0) {
            baseViewHolder.getConvertView().setBackgroundColor(context.getResources().getColor(R.color.item_bg));
        } else {
            baseViewHolder.getConvertView().setBackgroundColor(context.getResources().getColor(R.color.item_bg_other));
        }

        baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, OtherTeamDetailsActivity.class);
                intent.putExtra("teamId", team.id);
                context.startActivity(intent);

            }
        });
    }
}


