package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fullnetworkbasketball.ui.find.DynamicDetailsActivity;
import com.uhisports.basketball.R;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class DynamicDetailsAdapter extends RecyclerView.Adapter<DynamicDetailsAdapter.ViewHolder> {

    private Context context;

    public DynamicDetailsAdapter(Context context) {
        this.context = context;

    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_details_over_adapter_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
    if (position%2==0){
        holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg));
    }else {
        holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg_other));
    }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DynamicDetailsActivity.class);
                intent.putExtra("isCircle",1);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 9;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
