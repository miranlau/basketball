package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.CommentDtos;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Administrator on 2016/6/3 0003.
 */
public class DynamicDetailsListAdapter extends BaseQuickAdapter<CommentDtos> {
    private Activity activity;

    public DynamicDetailsListAdapter(Activity context, int layoutResId, List<CommentDtos> data) {
        super(layoutResId, data);

        this.activity = context;
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, CommentDtos commentDtos) {

        CircleImageView header = (CircleImageView) baseViewHolder.getConvertView().findViewById(R.id.header_iv);
        TextView name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.name_tv);
        TextView time = (TextView) baseViewHolder.getConvertView().findViewById(R.id.time_formate_tv);

        TextView content = (TextView) baseViewHolder.getConvertView().findViewById(R.id.content_tv);

        if (TextUtils.isEmpty(commentDtos.sender.avatar)) {


        } else {
            ImageLoader.loadImage(activity, commentDtos.sender.avatar, R.mipmap.default_person, header);
        }

        name.setText(commentDtos.sender.name);

        time.setText(commentDtos.pubTime);
        content.setText(commentDtos.content);

        baseViewHolder.setOnClickListener(R.id.header_iv, new OnItemChildClickListener());
//        baseViewHolder.setOnItemClickListener();

        if (baseViewHolder.getLayoutPosition() % 2 == 0) {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
        } else {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
        }
    }
}
