package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.List;


/**
 * Created by Administrator on 2016/6/3 0003.
 */
public class DynamicPictureAdapter extends BaseQuickAdapter<String> {

    private Activity activity;

    public DynamicPictureAdapter(Activity context, int layoutResId, List<String> data) {
        super(layoutResId, data);
        this.activity = context;
    }


    @Override
    protected void convert(final BaseViewHolder baseViewHolder, final String data) {
        ImageView imageView = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.dynamic_image_iv);
        ImageView delete = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.delete_tv);

        ImageLoader.loadImaged(activity, data, R.mipmap.load_ing, R.mipmap.load_failed, imageView);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                remove(baseViewHolder.getLayoutPosition());

                notifyDataSetChanged();
            }
        });

    }


}
