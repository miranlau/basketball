package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fullnetworkbasketball.ui.find.CourtDetailsActivity;
import com.uhisports.basketball.R;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class FindCourtAdapter extends RecyclerView.Adapter<FindCourtAdapter.ViewHolder> {
    private Context context;
    public FindCourtAdapter(Context context){
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_find_court_adapter_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CourtDetailsActivity.class);
                context.startActivity(intent);
            }
        });
        if (position%2==0){
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg));
        }else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg_other));
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
