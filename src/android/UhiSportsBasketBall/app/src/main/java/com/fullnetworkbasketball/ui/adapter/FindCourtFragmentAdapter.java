package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.Court;
import com.fullnetworkbasketball.ui.find.StadiumDetailsActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.Utility;
import com.uhisports.basketball.R;

import java.util.List;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class FindCourtFragmentAdapter extends BaseQuickAdapter<Court> {
    private Activity activity;

    public FindCourtFragmentAdapter(Activity context, int layoutResId, List<Court> data) {
        super( layoutResId, data);
        this.activity = context;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final Court court) {
        ImageView imageView = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.find_court_image);
        ImageLoader.loadImage(activity, court.icon, R.mipmap.court_details_big, imageView);
        TextView name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.find_court_name);
        name.setText(court.name);
        TextView mobile = (TextView) baseViewHolder.getConvertView().findViewById(R.id.find_court_mobile);
        mobile.setText(court.telephone_number_1);
        TextView address = (TextView) baseViewHolder.getConvertView().findViewById(R.id.find_court_address);
        address.setText(court.address);
        RatingBar ratingBar = (RatingBar) baseViewHolder.getConvertView().findViewById(R.id.find_court_rb);
        ratingBar.setRating(Utility.formatInt(court.stars));

        baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, StadiumDetailsActivity.class);
                intent.putExtra("arenasId", court.id);
                activity.startActivity(intent);
            }
        });
    }
}
