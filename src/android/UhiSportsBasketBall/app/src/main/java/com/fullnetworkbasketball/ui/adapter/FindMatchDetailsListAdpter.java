package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/4/27 0027.
 */
public class FindMatchDetailsListAdpter extends BaseAdapter {
    private Context context;
    private int typeList;
    private String[] setText = {"1", "2", "3", "4", "5", "6"};
    private String[] stringsofTeam = {"皇家马德里", "巴塞罗那", "曼联", "曼城", "拜仁慕尼黑", "波尔图"};
    private String[] stringsOfWin = {"6/1/1", "6/1/1", "6/1/1", "6/1/1", "6/1/1", "6/1/1"};
    private String[] stringsGoal = {"11/3/8", "11/3/8", "11/3/8", "11/3/8", "11/3/8", "11/3/8"};
    private String[] stringsScores = {"19", "19", "19", "19", "19", "19"};
    private String[] stringsofPlayer = {"德罗巴", "C罗", "波多尔斯基", "巴拉克", "本泽马", "贝尔"};
    private String[] stringsGoalOfTop = {"6", "5", "4", "3", "2", "1"};
    private String[] redCard= {"1", "0", "1", "0", "0", "1"};
    private String[] yellowCard= {"3", "2", "3", "2", "5", "1"};
    private String[] doubleCard= {"2", "1", "0", "1", "2", "1"};
    public FindMatchDetailsListAdpter(Context context, int typeList) {
        this.context = context;
        this.typeList = typeList;
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        System.out.println("postion:" + position + "-type:" + type);
        if (type == 0) {
            convertView = createListOfTotalScore(position, convertView, parent);
        } else if (type == 1) {
            convertView = createListOfTopScorer(position, convertView, parent);

        } else if (type == 2) {
            convertView = createListOfCardRecord(position, convertView, parent);
        }
        return convertView;
    }

    private View createListOfTotalScore(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.integal_table_sub_title_layout, null);
            holder = new ViewHolder(convertView,position);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.init(position);

        return convertView;
    }

    private View createListOfTopScorer(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ViewHolder2 holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.top_score_sub_title_layout, null);
            holder = new ViewHolder2(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder2) convertView.getTag();
        }
        holder.init2(position);

        return convertView;
    }

    private View createListOfCardRecord(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ViewHolder3 holder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.card_record_sub_title_layout, null);
            holder = new ViewHolder3(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder3) convertView.getTag();
        }
            holder.init3(position);

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if (typeList == 1) {
            type = 0;
        } else if (typeList == 2) {
            type = 1;
        } else if (typeList == 3) {
            type = 2;
        }
        return type;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    public class ViewHolder {
        TextView rank_of_integral, team_of_integral, win_integral, goal_integral, total_score_integral;

        public ViewHolder(View view,int position) {
            rank_of_integral = (TextView) view.findViewById(R.id.rank_of_integral);
            team_of_integral = (TextView) view.findViewById(R.id.team_of_integral);
            win_integral = (TextView) view.findViewById(R.id.win_integral);
            goal_integral = (TextView) view.findViewById(R.id.goal_integral);
            total_score_integral = (TextView) view.findViewById(R.id.total_score_integral);
            if (position%2==0){

            }
        }

        public void init(int position) {
            rank_of_integral.setText(setText[position]);
            team_of_integral.setText(stringsofTeam[position]);
            win_integral.setText(stringsOfWin[position]);
            goal_integral.setText(stringsGoal[position]);
            total_score_integral.setText(stringsScores[position]);

        }
    }

    public class ViewHolder2 {
        TextView rank_of_top_scorer,player_top_scorer,team_top_scorer,goal_top_scorer;
        public ViewHolder2(View view) {
            rank_of_top_scorer = (TextView)view.findViewById(R.id.rank_of_top_scorer);
            player_top_scorer =(TextView)view.findViewById(R.id.player_top_scorer);
            team_top_scorer = (TextView)view.findViewById(R.id.team_top_scorer);
            goal_top_scorer =(TextView)view.findViewById(R.id.goal_top_scorer);
        }

        public void init2(int position) {
            rank_of_top_scorer.setText(setText[position]);
            player_top_scorer.setText(stringsofPlayer[position]);
            team_top_scorer.setText(stringsofTeam[position]);
            goal_top_scorer.setText(stringsGoalOfTop[position]);
        }
    }

    public class ViewHolder3 {
        TextView card_record_rank,car_record_goal,red_card,yellow_card,double_yellow_card;
        public ViewHolder3(View view) {
            card_record_rank = (TextView)view.findViewById(R.id.card_record_rank);
            car_record_goal = (TextView)view.findViewById(R.id.car_record_goal);
            red_card = (TextView)view.findViewById(R.id.red_card);
            yellow_card = (TextView)view.findViewById(R.id.yellow_card);
            double_yellow_card = (TextView)view.findViewById(R.id.double_yellow_card);
        }

        public void init3(int position) {
            card_record_rank.setText(setText[position]);
            car_record_goal.setText(stringsofPlayer[position]);
            red_card.setText(redCard[position]);
            yellow_card.setText(yellowCard[position]);
            double_yellow_card.setText(doubleCard[position]);
        }
    }
}
