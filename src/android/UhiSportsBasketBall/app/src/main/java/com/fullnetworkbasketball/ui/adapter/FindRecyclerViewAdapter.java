package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.League;
import com.fullnetworkbasketball.ui.find.MatchDetailsActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.List;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class FindRecyclerViewAdapter extends BaseQuickAdapter<League> {

    private Activity context;

    //赛事数据集合


    public FindRecyclerViewAdapter(Activity context, int layoutResId, List<League> data) {
        super( layoutResId, data);
        this.context = context;

    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, final League league) {

        TextView go_to_match_details = (TextView) baseViewHolder.getConvertView().findViewById(R.id.go_to_match_details);
        TextView match_name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.match_name_tv);
        TextView match_time = (TextView) baseViewHolder.getConvertView().findViewById(R.id.match_time_tv);

        TextView match_area = (TextView) baseViewHolder.getConvertView().findViewById(R.id.match_area_tv);
        TextView match_type = (TextView) baseViewHolder.getConvertView().findViewById(R.id.match_type_tv);
        ImageView match_logo = (ImageView) baseViewHolder.getConvertView().findViewById(R.id.match_logo_iv);

        LinearLayout match_list = (LinearLayout) baseViewHolder.getConvertView().findViewById(R.id.match_list);

        if (baseViewHolder.getLayoutPosition() % 2 == 0) {
            match_list.setBackgroundColor(context.getResources().getColor(R.color.item_bg));
        } else {
            match_list.setBackgroundColor(context.getResources().getColor(R.color.item_bg_other));
        }

        //联赛状态 0：报名中 1；报名结束 2；比赛中 3；比赛结束
//
        final int code = league.status;

        if (code == 0) {
            go_to_match_details.setText(R.string.going_register);
            go_to_match_details.setBackgroundResource(R.mipmap.state_orange);

        } else if (code == 1) {
            go_to_match_details.setText(R.string.register_over);
            go_to_match_details.setBackgroundResource(R.mipmap.state_blue);
        } else if (code == 2) {
            go_to_match_details.setText(R.string.going_match);
            go_to_match_details.setBackgroundResource(R.mipmap.state_green);
        } else {

            go_to_match_details.setText(R.string.game_over);
            go_to_match_details.setBackgroundResource(R.mipmap.state_blue_black);
        }
        match_name.setText(league.name);
        match_area.setText(league.areaname);
        Log.i("data", "赛制类型：" + league.matchtype);
        match_type.setText(league.matchtype + "人制");
//

        if (league.begindate!=null&&league.enddate!=null){
            match_time.setText(Date(league.begindate) + "至" + Date(league.enddate));
        }else {
            match_time.setVisibility(View.GONE);
        }

        ImageLoader.loadImage(context, league.image, R.mipmap.banner, match_logo);

        /**
         * 跳转
         */

        match_list.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MatchDetailsActivity.class);

                intent.putExtra("status", code);
                intent.putExtra("matchId", league.id);
                context.startActivity(intent);
            }
        });


    }


    public String Date(String date) {

        String sub = date.substring(0, 11);

        return sub;
    }

}

//