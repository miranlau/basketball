package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/3/31 0031.
 */
public class GuideAdapter extends BaseAdapter {

    public Activity mContext;

    String paths[];

    public GuideAdapter(Activity context, String[] paths) {

        this.mContext = context;

        this.paths = paths;
    }

    @Override
    public int getCount() {

        if (paths.length == 0) {
            return 0;
        }
        return paths.length;
    }

    @Override
    public Object getItem(int position) {
        return paths.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int postion, View convertView, ViewGroup viewGroup) {

        convertView = LayoutInflater.from(mContext).inflate(R.layout.include_dynamic_gride_image, null);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image_iv);


        ImageLoader.loadImaged(mContext, paths[postion], R.mipmap.load_ing, R.mipmap.load_failed, imageView);
        return convertView;
    }


}
