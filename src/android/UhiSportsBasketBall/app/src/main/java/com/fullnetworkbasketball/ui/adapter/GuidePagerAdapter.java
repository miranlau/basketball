package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Administrator on 2015/5/26 0026.
 */
public class GuidePagerAdapter extends PagerAdapter {


    private List<View> views;
    public Context mContext;


    public GuidePagerAdapter(Context context, List<View> views) {
        this.views = views;

        this.mContext = context;
    }

    @Override
    public int getCount() {
        return views == null ? 0 : views.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
        // super.destroyItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        // 初始化将要显示的图片，将该图片加入到container中，即viewPager中

        container.addView(views.get(position));

        return views.get(position);
    }

}
