package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.uhisports.basketball.R;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class HomePageAdapter extends RecyclerView.Adapter<HomePageAdapter.ViewHolder> {

    private Context context;

    public HomePageAdapter(Context context) {
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_page_rv_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        if (position == 0) {
            holder.home_page_rv_item_date.setVisibility(View.VISIBLE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.GONE);
        } else if (position == 1) {
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
        }else if (position==2){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_iv_team_right.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("0:1");
            holder.home_page_iv_team_left_winner.setVisibility(View.GONE);
            holder.home_page_iv_team_right_winner.setVisibility(View.VISIBLE);
            holder.home_page_tv_left_teamName.setText("新南门竞技");
            holder.home_page_tv_right_teamName.setText("皇家后子门");
        }else if (position==3){
            holder.home_page_rv_item_date.setVisibility(View.VISIBLE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.GONE);
            holder.home_page_rv_item_date.setBackgroundResource(R.mipmap.include_yellow);
            holder.home_page_rv_item_date.setText("3月23日 今天");
        }else if (position==4){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_iv_team_right.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("1:1");
            holder.home_page_iv_team_left_winner.setVisibility(View.GONE);
            holder.home_page_iv_team_right_winner.setVisibility(View.GONE);
            holder.home_page_tv_left_teamName.setText("八宝街俱乐部");
            holder.home_page_tv_right_teamName.setText("春熙路金刚FC");
            holder.home_page_tv_association.setText("“李伯清”杯5人制足球联赛");
            holder.home_page_tv_address.setText("华西医大足球场");
        }else if (position==5){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_iv_team_right.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("18:30开始");
            holder.home_page_tv_score.setTextSize(20);
            holder.home_page_iv_team_left_winner.setVisibility(View.GONE);
            holder.home_page_iv_team_right_winner.setVisibility(View.GONE);
            holder.home_page_tv_left_teamName.setText("川大之星");
            holder.home_page_tv_right_teamName.setText("川音女队");
            holder.home_page_tv_association.setText("成都高校友谊赛");
            holder.home_page_tv_address.setText("川大2号足球场");
        }else if (position==6){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_iv_team_right.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("19:45开始");
            holder.home_page_tv_score.setTextSize(20);
            holder.home_page_iv_team_left_winner.setVisibility(View.GONE);
            holder.home_page_iv_team_right_winner.setVisibility(View.GONE);
            holder.home_page_tv_left_teamName.setText("八宝街俱乐部");
            holder.home_page_tv_right_teamName.setText("春熙路金刚FC");
            holder.home_page_tv_association.setText("“马明宇杯”杯5人制足球振兴联赛");
            holder.home_page_tv_address.setText("华西医大足球场");
        }else if (position==7){
            holder.home_page_rv_item_date.setVisibility(View.VISIBLE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.GONE);
            holder.home_page_rv_item_date.setBackgroundResource(R.mipmap.include_blue);
            holder.home_page_rv_item_date.setText("3月24日 明天");
        }else if (position==8){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_tv_score.setText("1:0");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TeamDetailsSignUpActivity.class);
                intent.putExtra("isScorer",1);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 9;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public  TextView home_page_rv_item_date,home_page_tv_left_teamName,home_page_tv_right_teamName,home_page_tv_score,home_page_tv_association,home_page_tv_address;
        public RelativeLayout home_page_rv_item_info_of_game;
        public  ImageView home_page_iv_team_left,home_page_iv_team_right,home_page_iv_team_left_winner,home_page_iv_team_right_winner;


        public ViewHolder(View itemView) {
            super(itemView);
            home_page_rv_item_date = (TextView) itemView.findViewById(R.id.home_page_rv_item_date);
            home_page_rv_item_info_of_game = (RelativeLayout) itemView.findViewById(R.id.home_page_rv_item_info_of_game);
            home_page_iv_team_left = (ImageView)itemView.findViewById(R.id.home_page_iv_team_left);
            home_page_iv_team_right = (ImageView)itemView.findViewById(R.id.home_page_iv_team_right);
            home_page_tv_left_teamName = (TextView)itemView.findViewById(R.id.home_page_tv_left_teamName);
            home_page_tv_right_teamName = (TextView)itemView.findViewById(R.id.home_page_tv_right_teamName);
            home_page_tv_score = (TextView)itemView.findViewById(R.id.home_page_tv_score);
            home_page_tv_association = (TextView)itemView.findViewById(R.id.home_page_tv_association);
            home_page_tv_address = (TextView)itemView.findViewById(R.id.home_page_tv_address);
            home_page_iv_team_left_winner = (ImageView)itemView.findViewById(R.id.home_page_iv_team_left_winner);
            home_page_iv_team_right_winner = (ImageView)itemView.findViewById(R.id.home_page_iv_team_right_winner);
        }
    }
}
