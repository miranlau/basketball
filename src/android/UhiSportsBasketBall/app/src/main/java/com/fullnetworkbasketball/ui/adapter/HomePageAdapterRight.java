package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.uhisports.basketball.R;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class HomePageAdapterRight extends RecyclerView.Adapter<HomePageAdapterRight.ViewHolder> {

    private Context context;

    public HomePageAdapterRight(Context context) {
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_page_rv_item_right, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        if (position == 0) {
            holder.home_page_rv_item_date.setVisibility(View.VISIBLE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.GONE);
        } else if (position == 1) {
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
        }else if (position==2){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("18:30");
            holder.home_page_tv_left_teamName.setText("川大之星");
            holder.ll_home_page_iv_team_right_agree.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg_other));
            holder.home_page_iv_team_right_agree.setText("5队应约");
        }else if (position==3){




            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("20:00");
            holder.home_page_tv_left_teamName.setText("八宝街俱乐部");
            holder.home_page_tv_association.setText("5人制");
            holder.home_page_tv_address.setText("川大2号足球场");
            holder.home_page_iv_team_right_agree.setText("3队应约");
        }else if (position==4){
            holder.home_page_rv_item_date.setVisibility(View.VISIBLE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.GONE);
            holder.home_page_rv_item_date.setBackgroundResource(R.mipmap.include_blue);
            holder.home_page_rv_item_date.setText("3月24日 明天");
        }else if (position==5){
            holder.home_page_rv_item_date.setVisibility(View.GONE);
            holder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
            holder.home_page_iv_team_left.setImageResource(R.mipmap.default_team);
            holder.home_page_tv_score.setText("18:30开始");
            holder.home_page_tv_left_teamName.setText("川大之星");
            holder.home_page_tv_association.setText("11人制");
            holder.home_page_tv_address.setText("川大1号足球场");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = holder.itemView.getContext();
                Intent intent = new Intent(context, RacingActivity.class);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 6;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public  TextView home_page_rv_item_date,home_page_tv_left_teamName,home_page_tv_score,home_page_tv_association,home_page_tv_address,home_page_iv_team_right_agree;
        public RelativeLayout home_page_rv_item_info_of_game;
        public  ImageView home_page_iv_team_left;
        public LinearLayout ll_home_page_iv_team_right_agree;

        public ViewHolder(View itemView) {
            super(itemView);
            home_page_rv_item_date = (TextView) itemView.findViewById(R.id.home_page_rv_item_date);
            home_page_rv_item_info_of_game = (RelativeLayout) itemView.findViewById(R.id.home_page_rv_item_info_of_game);
            home_page_iv_team_left = (ImageView)itemView.findViewById(R.id.home_page_iv_team_left);
            home_page_tv_left_teamName = (TextView)itemView.findViewById(R.id.home_page_tv_left_teamName);
            home_page_tv_score = (TextView)itemView.findViewById(R.id.home_page_tv_score);
            home_page_tv_association = (TextView)itemView.findViewById(R.id.home_page_tv_association);
            home_page_tv_address = (TextView)itemView.findViewById(R.id.home_page_tv_address);
            home_page_iv_team_right_agree  =(TextView)itemView.findViewById(R.id.home_page_iv_team_right_agree);
            ll_home_page_iv_team_right_agree = (LinearLayout)itemView.findViewById(R.id.ll_home_page_iv_team_right_agree);
        }
    }
}
