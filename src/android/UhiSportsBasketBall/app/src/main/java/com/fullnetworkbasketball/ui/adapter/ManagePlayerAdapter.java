package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.user.ManageTeamModifyActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.SwipeLayout;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 描述：
 * 作者 mjd
 * 日期：2016/1/28 22:28
 */
public class ManagePlayerAdapter extends BaseAdapter {
    private Activity context;
    private String[] names;
    private SwipeLayout lastOpenedSwipeLayout;
    private ArrayList<Player> players;
    private ContactOpponentCaptainDialog dialog;
    private int teamId;

    public ManagePlayerAdapter(Activity context, ArrayList<Player> players,int teamId) {
        this.context = context;
        this.players = players;
        this.teamId = teamId;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.manage_player_list_item, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.name = (TextView) convertView.findViewById(R.id.name);
        holder.name.setText(players.get(position).name);
        holder.number = (TextView)convertView.findViewById(R.id.number);
        holder.number.setText(context.getResources().getString(R.string.player_number)+players.get(position).number+"");
        holder.manage_player_header= (ImageView)convertView.findViewById(R.id.manage_player_header);
        ImageLoader.loadCicleImage(context,players.get(position).avatar,R.mipmap.default_person,holder.manage_player_header);
        holder.delete= (ImageView)convertView.findViewById(R.id.delete);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (players.get(position).id== UserManager.getIns().getUser().id){
                    T.showShort(context,context.getResources().getString(R.string.can_not_delete_self));
                    return;
                }

                if (dialog == null) {
                    dialog = new ContactOpponentCaptainDialog(context, R.style.callDialog, 2);
                    dialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                        @Override
                        public void onConfirmOrder(int i) {
                            if (i == 1) {

                                deletePlayer(position, players.get(position));
                            }
                        }
                    });
                }
                dialog.show();
            }
        });
        holder.ll_click = (LinearLayout)convertView.findViewById(R.id.ll_click);
        holder.ll_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ManageTeamModifyActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("teamId", teamId);
                bundle.putInt("userId", players.get(position).id);
                bundle.putInt("modify", 5);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
         holder.swipeLayout = (SwipeLayout) convertView;

        holder.swipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onOpen(SwipeLayout swipeLayout) {
                //当前 item 被打开时，记录下此 item
                lastOpenedSwipeLayout = swipeLayout;
            }

            @Override
            public void onClose(SwipeLayout swipeLayout) {
            }

            @Override
            public void onStartOpen(SwipeLayout swipeLayout) {
                //当前 item 将要打开时关闭上一次打开的 item
                if (lastOpenedSwipeLayout != null) {
                    lastOpenedSwipeLayout.close();
                }
            }

            @Override
            public void onStartClose(SwipeLayout swipeLayout) {
            }
        });


        return convertView;
    }
    public class ViewHolder{
        ImageView manage_player_header;
        TextView name;
        TextView number;
        ImageView delete;
        SwipeLayout swipeLayout;
        LinearLayout ll_click;
    }
    private void deletePlayer(final int position,Player player){

        HashMap<String,Object> params= HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        params.put("userId", player.id);
        Api.getRetrofit().kickoutPlayer(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                if (!response.isSuccess()) {

                } else {
                    players.remove(position);
                    notifyDataSetChanged();
                    T.showShort(context, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
}
