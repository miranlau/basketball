package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/3/31 0031.
 */
public class MatchScoreTeamGridAdapter extends BaseAdapter {

    public Activity mContext;
    public int leader;

    public MatchScoreTeamGridAdapter(Activity context) {

        this.mContext = context;

    }

    @Override
    public int getCount() {

//        if (list.size() == 0) {
//            return 0;
//        }
        return 11;
    }

    @Override
    public Object getItem(int position) {
        return 11;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view==null){
            view = LayoutInflater.from(mContext).inflate(R.layout.basketball_match_score_player_item, null);
            holder = new ViewHolder();
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }




         holder.name = (TextView) view.findViewById(R.id.team_details_player_item_name);

       switch (position){
           case 0:
               holder.name .setText("3分");

               break;
           case 1:
               holder.name .setText("2分");

               break;
           case 2:
               holder.name .setText("罚球");

               break;
           case 3:
               holder.name .setText("篮板");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_red);
               break;
           case 4:
               holder.name .setText("盖帽");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_red);
               break;
           case 5:
               holder.name .setText("助攻");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_blue);
               break;
           case 6:
               holder.name .setText("抢断");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_red);
               break;
           case 7:
               holder.name .setText("失误");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_blue);
               break;
           case 8:
               holder.name .setText("犯规");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_blue);
               break;
           case 9:
               holder.name .setText("换人");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_blue_black);
               break;
           case 10:
               holder.name .setText("撤销");
               holder.name .setBackgroundResource(R.drawable.shape_for_score_blue_black);
               break;
       }


        return view;
    }
    public class ViewHolder{
        TextView name;
    }

}
