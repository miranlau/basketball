package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.ui.user.PlayerPersonalActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.Utility;
import com.uhisports.basketball.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class MyAttentionAdapter extends BaseQuickAdapter<Player> {
    private Activity activity;

    public MyAttentionAdapter(Activity context, int layoutResId, List<Player> data) {
        super(layoutResId, data);
        this.activity = context;
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, final Player player) {


        CircleImageView header = (CircleImageView) baseViewHolder.getConvertView().findViewById(R.id.header_iv);

        TextView name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.name_tv);

        RatingBar evaluation = (RatingBar) baseViewHolder.getConvertView().findViewById(R.id.rb_evaluation);

        TextView joinMatch = (TextView) baseViewHolder.getConvertView().findViewById(R.id.join_match_accout);

        TextView getBall = (TextView) baseViewHolder.getConvertView().findViewById(R.id.get_ball_accout);

        TextView grade = (TextView) baseViewHolder.getConvertView().findViewById(R.id.grade_accout);

        evaluation.setRating(Utility.formatInt(player.stars));

        ImageLoader.loadImage(activity, player.avatar, R.mipmap.default_person, header);

        name.setText(player.name);

        joinMatch.setText(activity.getResources().getString(R.string.join_match) + player.matches);

        getBall.setText(activity.getResources().getString(R.string.goal_player_details) + player.score);

        grade.setText(activity.getResources().getString(R.string.level) +  player.backboard);


        if (baseViewHolder.getLayoutPosition() % 2 == 0) {

            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
        } else {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
        }

        baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();

                intent.putExtra("playerId", player.id);
                intent.setClass(activity, PlayerPersonalActivity.class);
                activity.startActivity(intent);
            }
        });
    }

}
