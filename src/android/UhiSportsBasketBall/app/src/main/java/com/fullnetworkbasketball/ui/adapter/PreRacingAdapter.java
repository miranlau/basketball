package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.user.ManageTeamModifyActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.SwipeLayout;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2016/5/25 0025.
 */
public class PreRacingAdapter extends RecyclerView.Adapter<PreRacingAdapter.ViewHolder> {

    public Activity mContext;

    public ArrayList<Player> list;
    private SwipeLayout lastOpenedSwipeLayout;
    //球队id
    public int teamId;
    //赛事id

    public int matchId;
    private Game game;

    //弹窗
    ContactOpponentCaptainDialog dialog;

    public PreRacingAdapter(Activity context, ArrayList<Player> list, int teamId, int matchId,Game game) {

        this.mContext = context;
        this.list = list;
        this.teamId = teamId;
        this.matchId = matchId;
        this.game=game;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_player_list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Player player = list.get(position);

        holder.init(player, position);


    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView header;
        public TextView name;
        public TextView number;
        public LinearLayout lt;
        public ImageView delete;

        public SwipeLayout swipeLayout;
        public ImageView leader;

        public ViewHolder(View itemView) {
            super(itemView);

            header = (ImageView) itemView.findViewById(R.id.manage_player_header);
            name = (TextView) itemView.findViewById(R.id.name);
            number = (TextView) itemView.findViewById(R.id.number);

            lt = (LinearLayout) itemView.findViewById(R.id.ll_click);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.sl);
            leader = (ImageView)itemView.findViewById(R.id.leader);



        }

        public void init(final Player player, final int position) {
            if (UserManager.getIns().getUser()!=null){
                if (game.visiting!=null){
                    if (game.home.leader==player.id||game.visiting.leader==player.id){
                        leader.setVisibility(View.VISIBLE);
                    }else {
                        leader.setVisibility(View.GONE);
                    }
                }else {
                    if (game.home.leader==player.id){
                        leader.setVisibility(View.VISIBLE);
                    }else {
                        leader.setVisibility(View.GONE);
                    }
                }

            }
            if (position % 2 == 0) {
                lt.setBackgroundColor(mContext.getResources().getColor(R.color.item_bg_other));
            } else {
                lt.setBackgroundColor(mContext.getResources().getColor(R.color.item_bg));
            }
            ImageLoader.loadCicleImage(mContext, player.avatar, R.mipmap.default_person, header);
            if (player == null) {
                return;
            } else {
                if (player.realName == null) {
                    name.setText(player.name);
                } else {
                    name.setText(player.realName);
                }
            }


            Log.i("number", "球衣号码=============================>" + player.number);

            number.setText(mContext.getResources().getString(R.string.player_game_number) + player.number);

            //跳转到修改球衣号码
            lt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent();
                    intent.setClass(mContext, ManageTeamModifyActivity.class);
                    intent.putExtra("modify", 6);

                    intent.putExtra("userId", player.id);
                    intent.putExtra("matchId", matchId);
                    intent.putExtra("teamId", teamId);
                    mContext.startActivity(intent);

                }
            });
            //删除球员
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    //判断能不能够删除自己
//                    if (player.id == UserManager.getIns().getUser().id) {
//                        T.showShort(mContext, mContext.getResources().getString(R.string.can_not_delete_yourself));
//                        return;
//                    }
                    //删除球员提示
                    if (dialog == null) {
                        dialog = new ContactOpponentCaptainDialog(mContext, R.style.callDialog, 2);
                        dialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                            @Override
                            public void onConfirmOrder(int i) {
                                if (i == 1) {
                                    deletePlayer(player, position);
                                }
                            }
                        });
                    }
                    dialog.show();

                }
            });


            //滑动事件
            swipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
                @Override
                public void onOpen(SwipeLayout swipeLayout) {
                    //当前 item 被打开时，记录下此 item
                    lastOpenedSwipeLayout = swipeLayout;
                }

                @Override
                public void onClose(SwipeLayout swipeLayout) {
                }

                @Override
                public void onStartOpen(SwipeLayout swipeLayout) {
                    //当前 item 将要打开时关闭上一次打开的 item
                    if (lastOpenedSwipeLayout != null) {
                        lastOpenedSwipeLayout.close();
                    }
                }

                @Override
                public void onStartClose(SwipeLayout swipeLayout) {
                }
            });

        }

        /**
         * 删除球员
         *
         * @param player
         * @param position
         */

        public void deletePlayer(Player player, final int position) {

            HashMap<String, Object> params = new HashMap<>();
            params.put("teamId", teamId);
            params.put("userId", player.id);

            params.put("matchId", matchId);

            Api.getRetrofit().deleteGamePlayer(params).enqueue(new RequestCallback<HttpResponseFWF2>(mContext) {
                @Override
                public void onSuccess(HttpResponseFWF2 response) {

                    if (response.isSuccess()) {

                        if (onConfirmOrderListener != null) {
                            onConfirmOrderListener.onConfirmOrder(1);
                        }
                        list.remove(position);

                        notifyDataSetChanged();
                        T.showShort(mContext, response.getMessage());
                    }

                }

                @Override
                public void onFinish() {

                }
            });
        }
    }

    /**
     * 确认监听事件
     */
    private OnConfirmOrderListener onConfirmOrderListener;

    public interface OnConfirmOrderListener {
        void onConfirmOrder(int i);

    }

    /**
     * 设置确定事件
     *
     * @param onClickListener
     */
    public void setConfirmClickListener(OnConfirmOrderListener onClickListener) {
        this.onConfirmOrderListener = onClickListener;
    }
}
