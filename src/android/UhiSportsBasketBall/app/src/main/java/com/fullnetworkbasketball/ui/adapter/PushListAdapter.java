package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.DynamicReplayData;
import com.fullnetworkbasketball.ui.find.DynamicDetailsActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Administrator on 2016/6/3 0003.
 */
public class PushListAdapter extends BaseQuickAdapter<DynamicReplayData> {
    private Activity activity;

    public PushListAdapter(Activity context, int layoutResId, List<DynamicReplayData> data) {
        super(layoutResId, data);

        this.activity = context;

    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, final DynamicReplayData data) {


        if (baseViewHolder.getLayoutPosition() % 2 == 0) {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
        } else {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
        }
        CircleImageView header = (CircleImageView) baseViewHolder.getConvertView().findViewById(R.id.header_iv);

        TextView name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.name_tv);

        TextView time = (TextView) baseViewHolder.getConvertView().findViewById(R.id.time_formate_tv);

        TextView content = (TextView) baseViewHolder.getConvertView().findViewById(R.id.content_tv);

        TextView hour = (TextView) baseViewHolder.getConvertView().findViewById(R.id.hour_tv);


        ImageLoader.loadImage(activity, data.getSenderHeader(), R.mipmap.default_person, header);

        String times[] = data.getSendTime().split(" ");


        name.setText(data.getSendName());
        content.setText(data.getContent());

        time.setText(times[0]);
        hour.setText(times[1]);


        baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, DynamicDetailsActivity.class);

                intent.putExtra("id", Integer.parseInt(data.getDynamicId()));

                mContext.startActivity(intent);
            }
        });
    }
}
