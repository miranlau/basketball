package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fullnetworkbasketball.models.ReservationData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Administrator on 2016/6/3 0003.
 */
public class PushYueListAdapter extends BaseQuickAdapter<ReservationData> {
    private Activity activity;
    double status;

    public PushYueListAdapter(Activity context, int layoutResId, List<ReservationData> data) {
        super(layoutResId, data);

        this.activity = context;

    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final ReservationData data) {


        if (baseViewHolder.getLayoutPosition() % 2 == 0) {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg));
        } else {
            baseViewHolder.getConvertView().setBackgroundColor(activity.getResources().getColor(R.color.item_bg_other));
        }
        CircleImageView header = (CircleImageView) baseViewHolder.getConvertView().findViewById(R.id.header_iv);

        TextView name = (TextView) baseViewHolder.getConvertView().findViewById(R.id.name_tv);

        TextView time = (TextView) baseViewHolder.getConvertView().findViewById(R.id.time_formate_tv);

        TextView content = (TextView) baseViewHolder.getConvertView().findViewById(R.id.content_tv);
        TextView hour = (TextView) baseViewHolder.getConvertView().findViewById(R.id.hour_tv);


        String times[] = data.getSendTime().split(" ");

        time.setText(times[0]);
        hour.setText(times[1]);

        ImageLoader.loadImage(activity, data.getSenderHeader(), R.mipmap.leage_manager, header);


        content.setText(data.getContent());
        name.setText(R.string.event_manager);


        baseViewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestStatus(Integer.parseInt(data.getMatchId()), data);


            }
        });

    }

    public void requestStatus(int matchId, final ReservationData data) {

        HashMap<String, Object> params = new HashMap<>();

        params.put("matchId", matchId);

        Api.getRetrofit().queryMatchStatus(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                if (response.isSuccess()) {
                    status = (double) response.getData();
                    Log.i("tag", "status:" + status);

                    Intent intent = new Intent();

                    if (status == 0.0) {

                        intent.putExtra("matchId", Integer.parseInt(data.getMatchId()));
                        intent.setClass(mContext, RacingActivity.class);

                    } else if (status == 10.0) {
                        intent.putExtra("matchId", Integer.parseInt(data.getMatchId()));
                        intent.setClass(mContext, TeamDetailsSignUpActivity.class);
                    } else if (status == 20.0) {
                        intent.putExtra("matchId", Integer.parseInt(data.getMatchId()));
                        intent.setClass(mContext, TeamDetailsSignUpActivity.class);
                    } else if (status == 100.0) {
                        intent.putExtra("matchId", Integer.parseInt(data.getMatchId()));
                        intent.setClass(mContext, InteractiveLiveActivity.class);

                    }


                    mContext.startActivity(intent);
                } else {
                    T.showShort(mContext, response.getMessage());
                }


            }

            @Override
            public void onFinish() {

            }
        });
    }
}

