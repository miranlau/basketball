package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.QueryMatchFive;
import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.ui.match.SingleMatchBeforeActivity;
import com.fullnetworkbasketball.utils.DateUtil;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Administrator on 2016/5/30 0030.
 */
public class QueryMatchFiveAdapter extends BaseAdapter {

    public ArrayList<QueryMatchFive> list;

    public Activity mContext;

    public QueryMatchFiveAdapter(Activity context, ArrayList<QueryMatchFive> list) {

        this.list = list;
        this.mContext = context;


    }


    @Override
    public int getCount() {

        Log.i("length", "长度：" + list.size());
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {


        return 2;
    }


    public int getItemViewType(int position) {
        QueryMatchFive data = list.get(position);
        if (data.lid == 10000) {



            return 0;
        } else {
            return 1;
        }


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHoldOne viewHoldOne = null;
        ViewHoldTwo viewHoldTwo = null;
        int type = getItemViewType(position);

        QueryMatchFive queryMatchFive = list.get(position);

        if (convertView == null) {
            switch (type) {

                case 0:
                    LayoutInflater inflater = LayoutInflater.from(mContext);
                    convertView = inflater.inflate(R.layout.all_match_item_five, null);
                    viewHoldOne = new ViewHoldOne(convertView, position);
                    convertView.setTag(viewHoldOne);

                    break;
                case 1:
                    LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                    convertView = layoutInflater.inflate(R.layout.all_match_item_one, null);
                    viewHoldTwo = new ViewHoldTwo(convertView, position);
                    convertView.setTag(viewHoldTwo);

                    break;
            }

        } else {
            switch (type) {
                case 0:
                    viewHoldOne = (ViewHoldOne) convertView.getTag();

                    break;

                case 1:
                    viewHoldTwo = (ViewHoldTwo) convertView.getTag();

                    break;
            }

        }

        switch (type)

        {
            case 0:
                viewHoldOne.init(queryMatchFive, position);
                break;

            case 1:
                viewHoldTwo.init(queryMatchFive, position);
                break;
        }


        return convertView;
    }

    class ViewHoldOne {

        ImageView header;

        ImageView winner;

        TextView teamName;

        TextView descible;

        TextView address;

        RelativeLayout home_page_rv_item_info_of_game;

        TextView date;

        public ViewHoldOne(View view, int position) {

            header = (ImageView) view.findViewById(R.id.home_page_iv_team_left);
            winner = (ImageView) view.findViewById(R.id.home_page_iv_team_left_winner);
            teamName = (TextView) view.findViewById(R.id.home_page_tv_left_teamName);

            date = (TextView) view.findViewById(R.id.home_page_rv_item_date);

            descible = (TextView) view.findViewById(R.id.home_page_tv_association);

            address = (TextView) view.findViewById(R.id.home_page_tv_address);

            home_page_rv_item_info_of_game = (RelativeLayout) view.findViewById(R.id.home_page_rv_item_info_of_game);

            judgeDate(date, position);


        }

        public void init(final QueryMatchFive queryMatchFive, int position) {

            ImageLoader.loadCicleImage(mContext, queryMatchFive.home.logo, R.mipmap.default_team, header);

            teamName.setText(queryMatchFive.home.name);

            descible.setText(queryMatchFive.desc);

            address.setText(queryMatchFive.addr);

            home_page_rv_item_info_of_game.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (queryMatchFive.status == 100) {
                        Intent intent = new Intent();
                        intent.putExtra("matchId", queryMatchFive.id);
                        intent.setClass(mContext, InteractiveLiveActivity.class);
                        mContext.startActivity(intent);
                    } else if (queryMatchFive.status == 10 || queryMatchFive.status == 20) {
                        Intent intent = new Intent();
                        intent.putExtra("matchId", queryMatchFive.id);
                        intent.setClass(mContext, SingleMatchBeforeActivity.class);
                        mContext.startActivity(intent);
                    }

                }
            });




            if (position % 2 == 0) {
                home_page_rv_item_info_of_game.setBackgroundColor(mContext.getResources().getColor(R.color.item_bg_other));
            } else {
                home_page_rv_item_info_of_game.setBackgroundColor(mContext.getResources().getColor(R.color.item_bg));
            }
        }
    }


    class ViewHoldTwo {

        ImageView headerLeft;
        ImageView winnerLeft;
        TextView teamNameLeft;

        TextView date;
        ImageView headerRight;
        ImageView winnerRight;
        TextView teamNameRight;

        TextView descible;
        TextView address;
        TextView score;

        RelativeLayout home_page_rv_item_info_of_game;

        public ViewHoldTwo(View view, int position) {

            date = (TextView) view.findViewById(R.id.home_page_rv_item_date);
            headerLeft = (ImageView) view.findViewById(R.id.home_page_iv_team_left);
            winnerLeft = (ImageView) view.findViewById(R.id.home_page_iv_team_left_winner);
            teamNameLeft = (TextView) view.findViewById(R.id.home_page_tv_left_teamName);

            score = (TextView) view.findViewById(R.id.home_page_tv_score);
            descible = (TextView) view.findViewById(R.id.home_page_tv_association);
            address = (TextView) view.findViewById(R.id.home_page_tv_address);

            headerRight = (ImageView) view.findViewById(R.id.home_page_iv_team_right);
            winnerRight = (ImageView) view.findViewById(R.id.home_page_iv_team_right_winner);
            teamNameRight = (TextView) view.findViewById(R.id.home_page_tv_right_teamName);


            home_page_rv_item_info_of_game = (RelativeLayout) view.findViewById(R.id.home_page_rv_item_info_of_game);

            judgeDate(date, position);


        }

        public void init(final QueryMatchFive queryMatchFive, int position) {

            ImageLoader.loadCicleImage(mContext, queryMatchFive.home.logo, R.mipmap.default_team, headerLeft);
            ImageLoader.loadCicleImage(mContext, queryMatchFive.visiting.logo, R.mipmap.default_team, headerRight);

            teamNameLeft.setText(queryMatchFive.home.name);
            teamNameRight.setText(queryMatchFive.visiting.name);

            // date.setVisibility(View.GONE);

            score.setText(queryMatchFive.homescore + ":" + queryMatchFive.visitingscore);
            address.setText(queryMatchFive.addr);
            descible.setText(queryMatchFive.desc);

            if (queryMatchFive.homescore > queryMatchFive.visitingscore) {
                winnerLeft.setVisibility(View.VISIBLE);
            } else if (queryMatchFive.homescore < queryMatchFive.visitingscore) {
                winnerRight.setVisibility(View.VISIBLE);
            }

            home_page_rv_item_info_of_game.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (queryMatchFive.status == 100) {
                        Intent intent = new Intent();
                        intent.putExtra("matchId", queryMatchFive.id);
                        intent.setClass(mContext, InteractiveLiveActivity.class);
                        mContext.startActivity(intent);
                    } else if (queryMatchFive.status==0){
                        Intent intent = new Intent();
                        intent.putExtra("matchId", queryMatchFive.id);
                        intent.setClass(mContext, RacingActivity.class);
                        mContext.startActivity(intent);
                    }else if (queryMatchFive.status==10||queryMatchFive.status==20){
                        Intent intent = new Intent();
                        intent.putExtra("matchId", queryMatchFive.id);
                        intent.setClass(mContext, TeamDetailsSignUpActivity.class);
                        mContext.startActivity(intent);
                    }


                }
            });

            if (position % 2 == 0) {
                home_page_rv_item_info_of_game.setBackgroundColor(mContext.getResources().getColor(R.color.item_bg_other));
            } else {
                home_page_rv_item_info_of_game.setBackgroundColor(mContext.getResources().getColor(R.color.item_bg));
            }
        }
    }

    private void judgeDate(TextView textView, int position) {
        //  TextView textView = (TextView) baseViewHolder.getConvertView().findViewById(R.id.home_page_rv_item_date);


        if (position == 0) {
            String date = (list.get(position)).date.substring(0, 10);
            textView.setVisibility(View.VISIBLE);
            textView.setText(getDate(date) + DateUtil.formatDisplayTime((list.get(position)).date, "yyyy-MM-dd HH:mm:ss"));
//+ DateUtil.formatDisplayTime(date,"yyyy-MM-dd HH:mm:ss")
            if (date.equals(getStringDateShort())) {
                textView.setBackgroundResource(R.mipmap.include_yellow);
                textView.setText(getDate(date) + DateUtil.formatDisplayTime((list.get(position)).date, "yyyy-MM-dd HH:mm:ss"));
            } else {
                textView.setBackgroundResource(R.mipmap.include_blue);
            }
        } else {
            String currentDate = (list.get(position)).date.substring(0, 10);
            int preIndex = position - 1;
            String preDate = (list.get(preIndex)).date.substring(0, 10);
            boolean isDifferent = !preDate.equals(currentDate);

            if (isDifferent) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(getDate(currentDate) + DateUtil.formatDisplayTime((list.get(position)).date, "yyyy-MM-dd HH:mm:ss"));
                if (currentDate.equals(getStringDateShort())) {
                    textView.setBackgroundResource(R.mipmap.include_yellow);
                    textView.setText(getDate(currentDate) + DateUtil.formatDisplayTime((list.get(position)).date, "yyyy-MM-dd HH:mm:ss"));
//                    textView.setText(getDate(currentDate) + DateUtil.getTimeC(((Match) getData().get(position)).date));
                }
            } else {
                textView.setVisibility(View.GONE);
                textView.setBackgroundResource(R.mipmap.include_blue);
            }
        }

    }

    public static String getDate(String date) {
        String sub = date.substring(5, 7) + "月" + date.substring(8, 10) + "日";
        return sub;
    }

    public static String getDateMilli(String date) {
        String sub = date.substring(11, 16);
        return sub;
    }

    /**
     * 获取现在时间
     *
     * @return 返回短时间字符串格式yyyy-MM-dd
     */
    public static String getStringDateShort() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

}
