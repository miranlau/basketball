package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/6/1 0001.
 */
public class SelectSystemAdapter extends BaseAdapter {


    public Context mContext;

    public String[] array;

    public SelectSystemAdapter(Context context, String[] array) {

        this.mContext = context;
        this.array = array;
    }

    @Override
    public int getCount() {
        return array.length == 0 ? 0 : array.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_system_item, null);
            viewHolder = new ViewHolder(convertView);
            (convertView).setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.init(array[position]);

        return convertView;
    }

    class ViewHolder {

        TextView account;

        public ViewHolder(View view) {
            account = (TextView) view.findViewById(R.id.account_tv);
        }

        public void init(String s) {

            account.setText(s);
        }
    }
}
