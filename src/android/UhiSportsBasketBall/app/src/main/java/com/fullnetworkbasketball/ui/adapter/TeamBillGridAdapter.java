package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.BillPlayer;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.ArrayList;


/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class TeamBillGridAdapter extends BaseAdapter {
    private Activity context;
    private LayoutInflater inflater;
    private ArrayList<BillPlayer> billPlayers;
    private int leaderId;
    public TeamBillGridAdapter(Activity context,ArrayList<BillPlayer> billPlayers,int leaderId){
        this.context = context;
        this.billPlayers = billPlayers;
        this.leaderId = leaderId;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return billPlayers.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView==null){
            convertView = inflater.inflate(R.layout.team_details_player_item,null);

           viewHolder = new ViewHolder();
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.player_header = (ImageView)convertView.findViewById(R.id.player_header);
        ImageLoader.loadCicleImage(context,billPlayers.get(position).avatar,R.mipmap.default_person,viewHolder.player_header);
        viewHolder.iv_leader = (ImageView)convertView.findViewById(R.id.iv_leader);
        viewHolder.number = (TextView)convertView.findViewById(R.id.number);
        if (billPlayers.get(position).id==leaderId){
            viewHolder.iv_leader.setVisibility(View.VISIBLE);
        }else {
            viewHolder.iv_leader.setVisibility(View.GONE);
        }
        viewHolder.iv_choose = (ImageView)convertView.findViewById(R.id.iv_choose);
        if (billPlayers.get(position).ispay){
            viewHolder.iv_choose.setVisibility(View.VISIBLE);
        }else {
            viewHolder.iv_choose.setVisibility(View.GONE);
        }
        viewHolder.number.setText(""+billPlayers.get(position).number);
        return convertView;
    }


    public class ViewHolder{
        private ImageView player_header;
        private ImageView iv_leader;
        private TextView number;
        private ImageView iv_choose;

    }
}
