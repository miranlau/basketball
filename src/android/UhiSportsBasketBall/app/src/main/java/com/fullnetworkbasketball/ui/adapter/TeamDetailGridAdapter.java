package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.ui.user.PlayerPersonalActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.ArrayList;


/**
 * Created by Administrator on 2016/3/31 0031.
 */
public class TeamDetailGridAdapter extends BaseAdapter {

    public Activity mContext;
    public int leader;
    public ArrayList<Player> list;

    public TeamDetailGridAdapter(Activity context, ArrayList<Player> list, int leader) {

        this.mContext = context;

        this.list = list;
        this.leader = leader;
    }

    @Override
    public int getCount() {

        if (list.size() == 0) {
            return 0;
        }
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = LayoutInflater.from(mContext).inflate(R.layout.team_details_player_item_final, null);

        final Player player = list.get(position);
        ImageView header = (ImageView) view.findViewById(R.id.player_header);
        ImageLoader.loadCicleImage(mContext, player.avatar, R.mipmap.default_person, header);

        TextView number = (TextView) view.findViewById(R.id.number);
        TextView name = (TextView) view.findViewById(R.id.team_details_player_item_name);
        name.setText(player.name);
        number.setText(player.number + "");

        ImageView iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
        int id = player.id;
        // int leader = teamDetails.leader;
        if (id == leader) {
            iv_leader.setVisibility(View.VISIBLE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserManager.getIns().getUser() == null) {
                    return;
                }
                Intent intent = new Intent(mContext, PlayerPersonalActivity.class);
                intent.putExtra("playerId", player.id);
                mContext.startActivity(intent);

            }
        });

        return view;
    }


}
