package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/4/26 0026.
 */
public class TeamDetailsInteractiveImgTxtAdapter extends RecyclerView.Adapter<TeamDetailsInteractiveImgTxtAdapter.ViewHolder> {
    private Context context;
    public TeamDetailsInteractiveImgTxtAdapter (Context context){
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_details_interactive_imgtxt_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position%2==0){
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg_other));
        }else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.item_bg));
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
