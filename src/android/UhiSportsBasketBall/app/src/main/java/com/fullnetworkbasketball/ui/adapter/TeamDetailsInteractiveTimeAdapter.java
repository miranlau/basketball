package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/4/26 0026.
 */
public class TeamDetailsInteractiveTimeAdapter extends RecyclerView.Adapter<TeamDetailsInteractiveTimeAdapter.ViewHolder> {
    private Context context;
    public TeamDetailsInteractiveTimeAdapter(Context context){
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_details_interactive_time_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      if (position==7){
          holder.line.setVisibility(View.GONE);
      }
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView line;
        public ViewHolder(View itemView) {
            super(itemView);
            line = (TextView)itemView.findViewById(R.id.line);
        }

    }
}
