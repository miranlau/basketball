package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.uhisports.basketball.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/4/25 0025.
 */
public class TechnicalStatisticsAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> list_left;
    private ArrayList<String> list_right;
    private LayoutInflater inflater;
    private Integer[] integers={R.string.firing_rate,R.string.shoot,R.string.corner,R.string.free_kick,R.string.steals,
    R.string.intercept,R.string.rescue,R.string.fighting,R.string.foul,R.string.offside,R.string.yellow_card,R.string.red_card};
    public TechnicalStatisticsAdapter(Context context,ArrayList<String> list_left,ArrayList<String> list_right){
        this.context = context;
        this.list_left = list_left;
        this.list_right = list_right;
        this.inflater= LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return list_left.size();
    }

    @Override
    public Object getItem(int position) {
        return getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView==null){
            convertView = inflater.inflate(R.layout.technical_statistics_item_layout,null);
            viewHolder = new ViewHolder();
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.progressBar_left = (ProgressBar)convertView.findViewById(R.id.pb_left);
        viewHolder.progressBar_right = (ProgressBar)convertView.findViewById(R.id.pb_right);
        viewHolder.textView_left = (TextView)convertView.findViewById(R.id.tv_left);
        viewHolder.textView_middle = (TextView)convertView.findViewById(R.id.tv_middle);
        viewHolder.textView_right = (TextView)convertView.findViewById(R.id.tv_right);
if (position==0){
    viewHolder.progressBar_left.setProgress(Integer.parseInt(list_left.get(position).substring(0,list_left.get(position).length()-1)));
    viewHolder.progressBar_right.setProgress(Integer.parseInt(list_right.get(position).substring(0,list_right.get(position).length()-1)));
}else {
    viewHolder.progressBar_left.setProgress(Integer.parseInt(list_left.get(position)));
    viewHolder.progressBar_right.setProgress(Integer.parseInt(list_left.get(position)));
}


        viewHolder.textView_left.setText(list_left.get(position));
        viewHolder.textView_middle.setText(integers[position]);
        viewHolder.textView_right.setText(list_right.get(position));
        return convertView;
    }
    public class ViewHolder{
        private ProgressBar progressBar_left;
        private TextView textView_left;
        private TextView textView_middle;
        private TextView textView_right;
        private ProgressBar progressBar_right;


    }
}
