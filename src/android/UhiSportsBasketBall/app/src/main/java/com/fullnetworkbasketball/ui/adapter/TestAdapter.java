package com.fullnetworkbasketball.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class TestAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    public TestAdapter(Context context){
        this.context = context;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return 9;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView==null){
            convertView = inflater.inflate(R.layout.home_page_rv_item,null);

           viewHolder = new ViewHolder();
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.home_page_rv_item_date =(TextView)convertView.findViewById(R.id.home_page_rv_item_date);
        viewHolder.home_page_rv_item_info_of_game = (RelativeLayout)convertView.findViewById(R.id.home_page_rv_item_info_of_game);
       if (position==0||position==3||position==7){
           viewHolder.home_page_rv_item_date.setVisibility(View.VISIBLE);
           viewHolder.home_page_rv_item_info_of_game.setVisibility(View.GONE);
       }else {
           viewHolder.home_page_rv_item_date.setVisibility(View.GONE);
           viewHolder.home_page_rv_item_info_of_game.setVisibility(View.VISIBLE);
       }
        return convertView;
    }


    public class ViewHolder{


        private TextView home_page_rv_item_date;
        public RelativeLayout home_page_rv_item_info_of_game;


    }
}
