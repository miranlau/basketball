package com.fullnetworkbasketball.ui.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Administrator on 2015/5/26 0026.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private List<View> views;

    private Activity activity;

    public ViewPagerAdapter(List<View> views, Activity activity) {
        this.views = views;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return views == null ? 0 : views.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        super.destroyItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        views.get(position).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });


        ((ViewPager) container).addView(views.get(position), 0);
        return views.get(position);
    }

}
