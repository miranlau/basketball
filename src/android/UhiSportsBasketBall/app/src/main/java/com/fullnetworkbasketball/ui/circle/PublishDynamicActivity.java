package com.fullnetworkbasketball.ui.circle;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.CurrentAddress;
import com.fullnetworkbasketball.models.ShareData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.DynamicPictureAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.uhisports.basketball.R;
import com.utils.multi_image_selector.MultiImageSelectorActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2016/5/3 0003.
 */
public class PublishDynamicActivity extends BaseActivity {

    private EditText contentEt;

    //  private LinearLayout dynamicll;

    private ImageView add;

    private RecyclerView recyclerView;
    private String path;

    //图片数组

    private ArrayList<String> pictures;

    private ArrayList<String> paths;

    public DynamicPictureAdapter adapter;

    private String content;

    private ShareData shareData;

    private String sharePath="";

    private int type ;

    private String shareContent;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_publish_dynamic_layout);
    }

    @Override
    protected void initializeViews() {


        SharePreHelper.getIns().putIsCircle(true);
        contentEt = (EditText) findViewById(R.id.content_et);
        //  dynamicll = (LinearLayout) findViewById(R.id.dynamic_picture_ll);
        add = (ImageView) findViewById(R.id.add_picture_iv);

        recyclerView = (RecyclerView) findViewById(R.id.rv);


        LinearLayoutManager layoutManager = new LinearLayoutManager(PublishDynamicActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    protected void initializeData() {


        Intent intent = getIntent();

        type = intent.getIntExtra("type", 6);

        if (type == 1) {

            shareData = (ShareData) intent.getSerializableExtra("data");


            shareContent = shareData.description + "\n" + shareData.url;

            contentEt.setText(shareContent);

            contentEt.setSelection(shareContent.length());


        } else if (type==0){

            sharePath = intent.getStringExtra("data");

            contentEt.setText("我在使用全网足球，这是我的个人数据." + sharePath);

            contentEt.setSelection(contentEt.getText().length());

        }else if (type==2){
            sharePath = intent.getStringExtra("data");

            contentEt.setText("我在使用全网足球，这是球队详情数据." + sharePath);

            contentEt.setSelection(contentEt.getText().length());
        }

        add.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_picture_iv:
                choosePicture();

                break;
        }
    }

    /**
     * 图片选择
     */
    private void choosePicture() {
        Intent intent = new Intent(this, MultiImageSelectorActivity.class);
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 9);
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);
        startActivityForResult(intent, 200);
    }

    /**
     * 图片选择回调
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {

                paths = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                int length = paths.size();
                if (length == 0) {
                    return;
                } else {
                    // upPicture();
                }
                if (length >= 9) {
                    add.setVisibility(View.GONE);
                }
                if (paths != null) {

                    adapter = new DynamicPictureAdapter(PublishDynamicActivity.this, R.layout.include_dynamic_image, paths);

                    recyclerView.setAdapter(adapter);


                }

            }
        }
    }

    /**
     * 图片上传
     */
    private void upPicture() {
        pictures = new ArrayList<>();

        List<File> fileList = new ArrayList<>();
        List<RequestBody> requestBodies = new ArrayList<>();
        MultipartBody boy = null;
        for (int i = 0; i < paths.size(); i++) {
            File file = new File(paths.get(i));
            if (!file.exists()) {
                return;
            }

            fileList.add(file);

            RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            requestBodies.add(fileBody);

        }
        if (fileList != null) {
            if (fileList.size() == 1) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))

                        .build();
            } else if (fileList.size() == 2) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .build();
            } else if (fileList.size() == 3) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .build();
            } else if (fileList.size() == 4) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .addFormDataPart("files", fileList.get(3).getName(), requestBodies.get(3))
                        .build();
            } else if (fileList.size() == 5) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .addFormDataPart("files", fileList.get(3).getName(), requestBodies.get(3))
                        .addFormDataPart("files", fileList.get(4).getName(), requestBodies.get(4))
                        .build();
            } else if (fileList.size() == 6) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .addFormDataPart("files", fileList.get(3).getName(), requestBodies.get(3))
                        .addFormDataPart("files", fileList.get(4).getName(), requestBodies.get(4))
                        .addFormDataPart("files", fileList.get(5).getName(), requestBodies.get(5))
                        .build();
            } else if (fileList.size() == 7) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .addFormDataPart("files", fileList.get(3).getName(), requestBodies.get(3))
                        .addFormDataPart("files", fileList.get(4).getName(), requestBodies.get(4))
                        .addFormDataPart("files", fileList.get(5).getName(), requestBodies.get(5))
                        .addFormDataPart("files", fileList.get(6).getName(), requestBodies.get(5))
                        .build();
            } else if (fileList.size() == 8) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .addFormDataPart("files", fileList.get(3).getName(), requestBodies.get(3))
                        .addFormDataPart("files", fileList.get(4).getName(), requestBodies.get(4))
                        .addFormDataPart("files", fileList.get(5).getName(), requestBodies.get(5))
                        .addFormDataPart("files", fileList.get(6).getName(), requestBodies.get(6))
                        .addFormDataPart("files", fileList.get(7).getName(), requestBodies.get(7))
                        .build();
            } else if (fileList.size() == 9) {
                boy = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", "other")
                        .addFormDataPart("files", fileList.get(0).getName(), requestBodies.get(0))
                        .addFormDataPart("files", fileList.get(1).getName(), requestBodies.get(1))
                        .addFormDataPart("files", fileList.get(2).getName(), requestBodies.get(2))
                        .addFormDataPart("files", fileList.get(3).getName(), requestBodies.get(3))
                        .addFormDataPart("files", fileList.get(4).getName(), requestBodies.get(4))
                        .addFormDataPart("files", fileList.get(5).getName(), requestBodies.get(5))
                        .addFormDataPart("files", fileList.get(6).getName(), requestBodies.get(6))
                        .addFormDataPart("files", fileList.get(7).getName(), requestBodies.get(7))
                        .addFormDataPart("files", fileList.get(8).getName(), requestBodies.get(8))
                        .build();
            }
        }


        Api.getRetrofit().dynamicUpload(boy).enqueue(new RequestCallback<HttpResponseFWF<UploadImageResult>>(PublishDynamicActivity.this) {
            @Override
            public void onSuccess(HttpResponseFWF<UploadImageResult> response) {

                if (response.isSuccess()) {

                    if (response.getCode() == 0) {
                        pictures = (ArrayList) response.getData();
                        requestUpload();
                    }


                } else {
                    T.showShort(PublishDynamicActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });


    }

    /**
     * 说说内容上传
     * <p/>
     * imgs	上传图片url数组
     * content	评论内容
     * longitude	经度
     * latitude	纬度
     */

    public void requestUpload() {


        CurrentAddress currentAddress = App.INS.getAddress();
        double latitude = currentAddress.latLng.getLatitude();
        double longitude = currentAddress.latLng.getLongitude();
        String address = currentAddress.address;
//
//        if (type == 1) {
//            content = contentEt.getText().toString() + "\n" + shareData.url;
//        } else {
        content = contentEt.getText().toString();
//        }


        if (TextUtil.isEmpty(content)) {
            T.showShort(PublishDynamicActivity.this, getResources().getString(R.string.dynamic_content_tempty));
            return;
        }


        Api.getRetrofit().dynamicSend(pictures, content, longitude, latitude, address).enqueue(new RequestCallbackFWF<HttpResponseFWF2>(PublishDynamicActivity.this) {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                if (response.isSuccess()) {

                    T.showShort(PublishDynamicActivity.this, response.getMessage());
                    SharePreHelper.getIns().putIsCircle(true);
                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_txt:
                if (paths == null || paths.size() == 0) {

                    requestUpload();

                } else {

                    upPicture();

                }

                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.publish_dynamic);
        getCustomActionBar().getRightText().setVisibility(View.VISIBLE);
        getCustomActionBar().getRightText().setTextColor(getResources().getColor(R.color.white));
        getCustomActionBar().getRightText().setText(R.string.publish);

    }


    public static class UploadImageResult {
        public String fid;
    }
}
