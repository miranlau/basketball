package com.fullnetworkbasketball.ui.circle;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.adapter.ViewPagerAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.hyphenate.easeui.widget.photoview.EasePhotoView;
import com.hyphenate.easeui.widget.photoview.PhotoViewAttacher;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.List;


public class ShowBigImgActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private String[] path;
    private int position;
    private ViewPager viewPager;
    private TextView account;


    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_show_big_img);

        Intent intent = getIntent();
        path = intent.getStringArrayExtra("path");
        position = intent.getIntExtra("position", 0);
    }

    @Override
    protected void initializeViews() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        account = (TextView) findViewById(R.id.accout);
        initBannerData();

    }

    /**
     * 滑动图
     */
    private void initBannerData() {

        List<View> views = new ArrayList<>();

        for (int i = 0; i < path.length; i++) {


            EasePhotoView imageView = new EasePhotoView(this);


            imageView.setMaxScale(8.0f);

            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            ImageLoader.loadImaged(ShowBigImgActivity.this, path[i], R.mipmap.load_ing, R.mipmap.load_failed, imageView);


            views.add(imageView);
            imageView.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
                @Override
                public void onPhotoTap(View view, float x, float y) {

                    finish();
                }
            });
        }

        ViewPagerAdapter adapter = new ViewPagerAdapter(views, ShowBigImgActivity.this);
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(position);
        viewPager.setOffscreenPageLimit(views.size());

        viewPager.setOnPageChangeListener(this);

        position = position + 1;
        account.setText(position + "-" + path.length);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.zoomout);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    @Override
    public void onPageSelected(int position) {
        position += 1;
        account.setText(position + "-" + path.length);
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
