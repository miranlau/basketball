package com.fullnetworkbasketball.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by Administrator on 2016/4/24 0024.
 */
public class AgreeSuccessDialog extends Dialog {
    private TextView good;
    private TextView success_title;
    private int isRegister;
    private Context context;
    public AgreeSuccessDialog(Context context, int themeResId,int isRegister) {
        super(context, themeResId);
        this.isRegister = isRegister;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agree_success_dialog_layout);
        success_title = (TextView)findViewById(R.id.success_title);
        good = (TextView)findViewById(R.id.good);
        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if (isRegister==1){
            success_title.setText(context.getResources().getString(R.string.success_register));
        }else if (isRegister==3){
            success_title.setText(context.getResources().getString(R.string.apply_join_success));
        }else if(isRegister==4){
            success_title.setText(context.getResources().getString(R.string.cancel_success));
        }
    }
}
