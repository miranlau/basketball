package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;

import com.uhisports.basketball.R;


/**
 * Created by EiT on 2015/1/28.
 */
public class BallStateDialog extends Dialog implements View.OnClickListener {

    public Activity activity;

    public static final int TAKE_PICTURE = 10;
    private int state;

    public BallStateDialog(Activity context) {
        super(context);
        this.activity = context;
    }


    public BallStateDialog(Activity context, int theme) {
        super(context, theme);
        this.activity = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ball_state_dialog);


        findViewById(R.id.goal).setOnClickListener(this);
        findViewById(R.id.miss).setOnClickListener(this);
//        findViewById(R.id.over_just).setOnClickListener(this);
//        findViewById(R.id.eleven_system).setOnClickListener(this);

        findViewById(R.id.delete).setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        super.show();

        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.goal:

                dismiss();
                setCallBack(1);
                break;

            case R.id.miss:
               dismiss();
                setCallBack(2);

                break;

            case R.id.delete:
                dismiss();
                setCallBack(3);

                break;
        }
    }

    private void setCallBack(int i){
        if (onConfirmOrderListener!=null){
            onConfirmOrderListener.onConfirmOrder(i);
        }

    }
    /**
     * 确认监听事件
     */
    private OnConfirmOrderListener onConfirmOrderListener;

    public interface OnConfirmOrderListener {
        void onConfirmOrder(int i);
    }

    /**
     * 设置确定事件
     *
     * @param onClickListener
     */
    public void setConfirmClickListener(OnConfirmOrderListener onClickListener) {
        this.onConfirmOrderListener = onClickListener;
    }

}
