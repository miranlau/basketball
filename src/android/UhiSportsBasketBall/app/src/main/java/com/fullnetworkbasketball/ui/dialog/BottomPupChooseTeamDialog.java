package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.ui.adapter.ChooseTeamAdapter;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.uhisports.basketball.R;

import java.util.ArrayList;

import butterknife.Bind;


/**
 * Created by EiT on 2015/1/28.
 */
public class BottomPupChooseTeamDialog extends Dialog implements View.OnClickListener {

    public Activity activity;

    public static final int TAKE_PICTURE = 10;
    @Bind(R.id.horizontal_scrollView)
    HorizontalScrollView horizontalScrollView;
    private LinearLayout teams;
    private XRecyclerView xRecyclerView;
    private ChooseTeamAdapter adapter;
    private AgreeSuccessDialog agreeSuccessDialog;
    private int isRegister;
    private TextView title;


    //联赛Id
    private int leagueId;


    private ArrayList<Team> myteams;



    public BottomPupChooseTeamDialog(Activity context) {
        super(context);
        this.activity = context;
    }


    public BottomPupChooseTeamDialog(Activity context, int theme, int isRegister) {
        super(context, theme);
        this.activity = context;
        this.isRegister = isRegister;

    }


    public BottomPupChooseTeamDialog(Activity context, int theme, int isRegister, int leagueId) {
        super(context, theme);
        this.activity = context;
        this.isRegister = isRegister;

        this.leagueId = leagueId;

    }






    public BottomPupChooseTeamDialog(Activity context, int theme, final int isRegister,ArrayList<Team> myteams) {

        super(context, theme);
        this.activity = context;
        this.isRegister = isRegister;
        this.myteams = myteams;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_pup_choose_team_dialog_layout);

        setCanceledOnTouchOutside(true);
        teams = (LinearLayout) findViewById(R.id.teams);
        title = (TextView) findViewById(R.id.title);

        if (isRegister==1){
            initAddView2();
        }else {
            initAddView();

        }

    }

    @Override
    public void show() {
        super.show();

        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {

        }
    }

    /**
     * 确认监听事件
     */
    private OnConfirmOrderListener onConfirmOrderListener;

    public interface OnConfirmOrderListener {
        void onConfirmOrder(Team team);
    }

    /**
     * 设置确定事件
     *
     * @param onClickListener
     */
    public void setConfirmClickListener(OnConfirmOrderListener onClickListener) {
        this.onConfirmOrderListener = onClickListener;
    }

    /**
     * by cht
     * 动态加载 view数据
     *
     *
     */
    public void initAddView() {

        teams = (LinearLayout) findViewById(R.id.teams);
        title = (TextView) findViewById(R.id.title);

        for (final Team team : myteams) {
            View view = activity.getLayoutInflater().inflate(R.layout.choose_team_item_layout, null);
            TextView name = (TextView) view.findViewById(R.id.choose_team_name);
            ImageView image = (ImageView) view.findViewById(R.id.choose_team_iv);
            final ImageView choose = (ImageView) view.findViewById(R.id.choose_iv);
            choose.setVisibility(View.GONE);

            ImageLoader.loadCicleImage(activity, team.logo, R.mipmap.default_team, image);

      //      ImageLoader.loadCicleImage(activity, team.logo, image);

            name.setText(team.name);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //choose.setVisibility(View.VISIBLE);

                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(team);
                    }


                    dismiss();
                    if (agreeSuccessDialog == null) {
                        if (isRegister == 1) {
                            agreeSuccessDialog = new AgreeSuccessDialog(activity, R.style.callDialog, 1);
                        } else {
                            agreeSuccessDialog = new AgreeSuccessDialog(activity, R.style.callDialog, 0);
                        }

                    }
                    agreeSuccessDialog.show();
                }
            });
            teams.addView(view);

        }
    }


    public void initAddView2() {

        teams = (LinearLayout) findViewById(R.id.teams);
        title = (TextView) findViewById(R.id.title);
        for (final Team team : myteams) {
            View view = activity.getLayoutInflater().inflate(R.layout.choose_team_item_layout, null);
            TextView name = (TextView) view.findViewById(R.id.choose_team_name);
            ImageView image = (ImageView) view.findViewById(R.id.choose_team_iv);
            ImageLoader.loadCicleImage(activity, team.logo, R.mipmap.default_team, image);
            name.setText(team.name);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(team);
                    }
                }
            });
            teams.addView(view);

        }
    }

}



