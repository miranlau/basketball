package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.CreateTeamStepFinalActivity;
import com.fullnetworkbasketball.ui.home.CreateTeamStepOneActivity;
import com.fullnetworkbasketball.ui.home.LaunchMatchActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.TextUtil;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by EiT on 2015/1/28.
 */
public class BottomPupDialog extends Dialog implements View.OnClickListener {

    public Activity activity;
    private int isFromTeamDetails;
    public static final int TAKE_PICTURE = 10;
    private ImageView iv_left;
    private TextView tv_left;
    private ImageView iv_right;
    private TextView tv_right;
    private AgreeSuccessDialog agreeSuccessDialog;
    private ArrayList<Team> teams;
    private ArrayList<Team> myTeams;
    private ContactOpponentCaptainDialog dialog;

    public BottomPupDialog(Activity context) {
        super(context);
        this.activity = context;
    }


    public BottomPupDialog(Activity context, int theme,int isFromTeamDetails) {
        super(context, theme);
        this.activity = context;
        this.isFromTeamDetails = isFromTeamDetails;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_pup_dialog_layout);
        findViewById(R.id.ll_i_want_crate_team).setOnClickListener(this);
        findViewById(R.id.ll_i_want_launch_match).setOnClickListener(this);
        iv_left = (ImageView)findViewById(R.id.iv_left);
        tv_left = (TextView)findViewById(R.id.tv_left);
        iv_right =(ImageView)findViewById(R.id.iv_right);
        tv_right = (TextView)findViewById(R.id.tv_right);
        if (isFromTeamDetails==1){
            iv_left.setBackgroundResource(R.mipmap.apply_join);
            tv_left.setText(activity.getResources().getString(R.string.apply_join));
            iv_right.setBackgroundResource(R.mipmap.code_join);
            tv_right.setText(activity.getResources().getString(R.string.code_join));
        }
//        setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        super.show();

        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_i_want_crate_team:
                dismiss();
                if (isFromTeamDetails==1){
                    if (agreeSuccessDialog == null) {

                            agreeSuccessDialog = new AgreeSuccessDialog(activity, R.style.callDialog,3);
                        }

                    agreeSuccessDialog.show();
                }else {
                    if (UserManager.getIns().getUser()==null){
                        App.getInst().toLogin();
                    }else {
                        if (!TextUtil.isValidate(UserManager.getIns().getUser().token)){
                            App.getInst().toLogin();
                        }else {
                            Intent intent = new Intent(getContext(), CreateTeamStepOneActivity.class);
                            getContext().startActivity(intent);
                        }
                    }


                }


                break;

            case R.id.ll_i_want_launch_match:
                dismiss();
                if (isFromTeamDetails==1){
                    Intent intent = new Intent(activity, CreateTeamStepFinalActivity.class);
                    intent.putExtra("isApplyCodeJoin",1);
                    activity.startActivity(intent);
                }else {

                    if (UserManager.getIns().getUser()==null){
                        App.getInst().toLogin();
                    }else {
                        if (!TextUtil.isValidate(UserManager.getIns().getUser().token)){
                            App.getInst().toLogin();
                        }else {
                            myTeam();

                        }
                    }

                }


                break;

        }
    }
    //获取自己所属球队，并取出自己创建的球队
    private void myTeam(){
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        if (UserManager.getIns().getUser()!=null){
            params.put("userId", UserManager.getIns().getUser().id);
        }
        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()){
                    teams = response.getData().teams;
                    myTeams = new ArrayList<Team>();
                    for (Team team:teams){
                        if (team.leader==UserManager.getIns().getUser().id){
                            myTeams.add(team);
                        }
                    }
                    if (myTeams.size()==0){
                        if (dialog == null) {
                            dialog = new ContactOpponentCaptainDialog(getContext(), R.style.callDialog,4);
                        }
                        dialog.show();

                        return;
                    }
                    Intent intent1 = new Intent(getContext(), LaunchMatchActivity.class);
                    intent1.putExtra("teams",myTeams);
                    getContext().startActivity(intent1);
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

}
