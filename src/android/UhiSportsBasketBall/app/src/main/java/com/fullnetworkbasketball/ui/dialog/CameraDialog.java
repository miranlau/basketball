package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;

import com.fullnetworkbasketball.utils.CameraUtil;
import com.uhisports.basketball.R;

/**
 * Created by EiT on 2015/1/28.
 */
public class CameraDialog extends Dialog implements View.OnClickListener {

    public Activity activity;

    public static final int TAKE_PICTURE = 10;

    public CameraDialog(Activity context) {
        super(context);
        this.activity = context;
    }


    public CameraDialog(Activity context, int theme) {
        super(context, theme);
        this.activity = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_layout);
        findViewById(R.id.takepicture).setOnClickListener(this);
        findViewById(R.id.select_phone).setOnClickListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        super.show();

        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.takepicture:

                dismiss();
                activity.startActivityForResult(CameraUtil.getCameraIntent(), CameraUtil.TAKE_PHOTO);
                break;

            case R.id.select_phone:
                activity.startActivityForResult(CameraUtil.getAlbumIntent(), CameraUtil.PICK_PHOTO);
               dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }


}
