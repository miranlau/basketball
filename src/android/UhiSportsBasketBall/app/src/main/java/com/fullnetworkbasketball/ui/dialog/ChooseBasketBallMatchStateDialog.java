package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.uhisports.basketball.R;


/**
 * Created by EiT on 2015/1/28.
 */
public class ChooseBasketBallMatchStateDialog extends Dialog implements View.OnClickListener {

    public Activity activity;

    public static final int TAKE_PICTURE = 10;
    private int state;

    public ChooseBasketBallMatchStateDialog(Activity context) {
        super(context);
        this.activity = context;
    }


    public ChooseBasketBallMatchStateDialog(Activity context, int theme,int state) {
        super(context, theme);
        this.activity = context;
        this.state = state;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_basketball_match_state);
        //比分相同
        if (state==1){
        }
        //比分不同
        else if (state==2){
          TextView  more =  (TextView)findViewById(R.id.more_time);
          TextView  next =  (TextView)findViewById(R.id.next_court);
          more.setText("本节结束");
          next.setText("本场结束");
        }

        else if (state == 3){
            TextView  more =  (TextView)findViewById(R.id.more_time);
            TextView  next =  (TextView)findViewById(R.id.next_court);
            more.setText("下一节");
            next.setText("取消");
        }
        else if (state == 4){
            TextView  more =  (TextView)findViewById(R.id.more_time);
            TextView  next =  (TextView)findViewById(R.id.next_court);
            more.setText("结束");
            next.setText("继续");
        }


        findViewById(R.id.more_time).setOnClickListener(this);
        findViewById(R.id.next_court).setOnClickListener(this);
//        findViewById(R.id.over_just).setOnClickListener(this);
//        findViewById(R.id.eleven_system).setOnClickListener(this);

        findViewById(R.id.delete).setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        super.show();
        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.more_time:
                dismiss();
                setCallBack(1);
                break;

            case R.id.next_court:
               dismiss();
                setCallBack(2);

                break;
            case R.id.delete:
                dismiss();
                setCallBack(4);

                break;
        }
    }

    private void setCallBack(int i){
        if (onConfirmOrderListener!=null){
            onConfirmOrderListener.onConfirmOrder(i);
        }

    }
    /**
     * 确认监听事件
     */
    private OnConfirmOrderListener onConfirmOrderListener;

    public interface OnConfirmOrderListener {
        void onConfirmOrder(int i);
    }

    /**
     * 设置确定事件
     *
     * @param onClickListener
     */
    public void setConfirmClickListener(OnConfirmOrderListener onClickListener) {
        this.onConfirmOrderListener = onClickListener;
    }

}
