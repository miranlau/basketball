package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fullnetworkbasketball.ui.adapter.SelectSystemAdapter;
import com.uhisports.basketball.R;

/**
 * Created by EiT on 2015/1/28.
 */
public class ChooseMatchAssoaciationDialog extends Dialog implements View.OnClickListener, AdapterView.OnItemClickListener {

    public Activity activity;

    public static final int TAKE_PICTURE = 10;

    public SelectSystemAdapter adapter;

    public String[] systemArray;
    public ListView listView;

    public ChooseMatchAssoaciationDialog(Activity context) {
        super(context);
        this.activity = context;
    }


    public ChooseMatchAssoaciationDialog(Activity context, int theme) {
        super(context, theme);
        this.activity = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_match_assoaciation_layout);
//        findViewById(R.id.five_person).setOnClickListener(this);
//        findViewById(R.id.seven_system).setOnClickListener(this);
//        findViewById(R.id.nine_system).setOnClickListener(this);
//        findViewById(R.id.eleven_system).setOnClickListener(this);

        listView = (ListView) findViewById(R.id.list);

        systemArray = activity.getResources().getStringArray(R.array.select_system_array);

        listView.setOnItemClickListener(this);

        adapter = new SelectSystemAdapter(activity, systemArray);

        listView.setAdapter(adapter);


        findViewById(R.id.cancel).setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void show() {
        super.show();

        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
//            case R.id.five_person:
//
//                dismiss();
//                setCallBack(5);
//                break;
//
//            case R.id.seven_system:
//               dismiss();
//                setCallBack(7);
//
//                break;
//            case R.id.nine_system:
//                setCallBack(9);
//                dismiss();
//                break;
//            case R.id.eleven_system:
//
//                dismiss();
//                setCallBack(11);
//                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }

//    private void setCallBack(int i) {
//        if (onConfirmOrderListener != null) {
//            onConfirmOrderListener.onConfirmOrder(i);
//        }
//
//    }

    /**
     * 确认监听事件
     */
    private OnConfirmOrderListener onConfirmOrderListener;

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        if (onConfirmOrderListener != null) {
            onConfirmOrderListener.onConfirmOrder(position);
        }

        dismiss();
    }

    public interface OnConfirmOrderListener {
        void onConfirmOrder(int i);
    }

    /**
     * 设置确定事件
     *
     * @param onClickListener
     */
    public void setConfirmClickListener(OnConfirmOrderListener onClickListener) {
        this.onConfirmOrderListener = onClickListener;
    }

}
