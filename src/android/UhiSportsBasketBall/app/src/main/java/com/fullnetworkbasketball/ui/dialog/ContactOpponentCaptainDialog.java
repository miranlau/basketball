package com.fullnetworkbasketball.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.AdData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.CreateTeamStepOneActivity;
import com.fullnetworkbasketball.ui.home.HomeActivity;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.MyInfoSettingActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.hyphenate.EMCallBack;
import com.hyphenate.chatuidemo.DemoHelper;
import com.uhisports.basketball.R;

import java.util.HashMap;

/**
 * Created by Administrator on 2016/4/24 0024.
 */
public class ContactOpponentCaptainDialog extends Dialog {
    private TextView good;
    private ImageView delete;
    private TextView up;
    private TextView perfect_data;
    private int isFirst;

    //取消报名时调用 matchId 赛事id

    private int matchId;
    private Context mcontext;
    private String phoneNum;

    public String leaderName;

    public ContactOpponentCaptainDialog(Context context, int themeResId, int isFirst) {
        super(context, themeResId);
        this.isFirst = isFirst;
        this.mcontext = context;
    }

    public ContactOpponentCaptainDialog(Context context, int themeResId, int isFirst, String phoneNum, String leaderName) {
        super(context, themeResId);
        this.isFirst = isFirst;
        this.phoneNum = phoneNum;
        this.mcontext = context;
        this.leaderName = leaderName;
    }


    public ContactOpponentCaptainDialog(Context context, int themeResId, int isFirst, int matchId) {
        super(context, themeResId);
        this.isFirst = isFirst;
        this.matchId = matchId;
        this.mcontext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_opponent_captain_dialog_layout);
        good = (TextView) findViewById(R.id.good);
        delete = (ImageView) findViewById(R.id.delete);
        up = (TextView) findViewById(R.id.up);
        perfect_data = (TextView) findViewById(R.id.perfect_data);
        if (isFirst == 1) {
            perfect_data.setVisibility(View.VISIBLE);
            up.setText(R.string.welcome);
            good.setText("好");
        } else if (isFirst == 2) {
            up.setText(R.string.is_delete_player);
            good.setText(R.string.sure);
        } else if (isFirst == 3) {
            up.setText(R.string.is_sure_exit);
            good.setText(R.string.sure);

        } else if (isFirst == 4) {
            up.setText(R.string.not_create_team);
            good.setText(R.string.go_create);

        } else if (isFirst == 5) {
            up.setText(R.string.is_sure_delete_match);
            good.setText(R.string.sure);

        } else if (isFirst == 6) {
            up.setText(R.string.cancel_title);
            good.setText(R.string.cancel_true);


        } else if (isFirst == 7) {
            up.setText(R.string.not_join_match);
            good.setText(R.string.go_create);

        } else if (isFirst == 5) {
            up.setText(R.string.is_sure_delete_match);
            good.setText(R.string.sure);

        } else if (isFirst == 8) {
            up.setText(R.string.not_login);
            good.setText(R.string.go_login);
            delete.setVisibility(View.GONE);
            setCanceledOnTouchOutside(false);

        } else if (isFirst == 9) {
            up.setText(leaderName + ":" + phoneNum);
            good.setText(R.string.sure);
        } else if (isFirst == 10) {
            up.setText("是否未看清球员？");
            good.setText(R.string.sure);
        } else if (isFirst == 11) {
            up.setText("是否结束比赛？");
            good.setText(R.string.sure);
        } else if (isFirst == 12) {
            up.setText("抱歉，该用户不允许陌生人查看资料");
            good.setText(R.string.sure);
            delete.setVisibility(View.GONE);
        }else if (isFirst == 13) {
            up.setText("是否确认删除");
            good.setText(R.string.sure);

        }else if (isFirst==14){
            up.setText("是否进入中场休息？");
            good.setText(R.string.sure);
        }else if (isFirst==15){
            up.setText("是否未看清助攻队员？");
            good.setText(R.string.sure);
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (isFirst == 1) {
                    Intent intent = new Intent(getContext(), MyInfoSettingActivity.class);
                    getContext().startActivity(intent);
                } else if (isFirst == 2) {
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(1);
                    }
                } else if (isFirst == 3) {
                    UserManager.getIns().clearToken();
                    Intent intent = new Intent(getContext(), HomeActivity.class);
                    SharePreHelper.getIns().saveHeaderUrl("");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    SharePreHelper.getIns().isModifyHeader(false);
                    getContext().startActivity(intent);
                    logout();
                    logoff();

                } else if (isFirst == 4) {
                    Intent intent = new Intent(getContext(), CreateTeamStepOneActivity.class);
                    getContext().startActivity(intent);

                } else if (isFirst == 6) {

                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(1);
                    }

                    AgreeSuccessDialog dialog = new AgreeSuccessDialog(mcontext, R.style.callDialog, 4);
                    dialog.show();

                } else if (isFirst == 5) {
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(5);
                    }


                } else if (isFirst == 7) {
                    Intent intent = new Intent(getContext(), CreateTeamStepOneActivity.class);
                    getContext().startActivity(intent);

                } else if (isFirst == 8) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    getContext().startActivity(intent);
                } else if (isFirst == 9) {

                    call(phoneNum);
                } else if (isFirst == 10) {
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(10);
                    }
                } else if (isFirst == 11) {
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(11);
                    }
                }
                    else if (isFirst == 12) {


                        if (onConfirmOrderListener != null) {
                            onConfirmOrderListener.onConfirmOrder(12);
                        }

                    } else if (isFirst == 13) {


                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(13);
                    }

                }else if (isFirst==14){
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(14);
                    }
                }else if (isFirst==15){
                    if (onConfirmOrderListener != null) {
                        onConfirmOrderListener.onConfirmOrder(15);
                    }
                }

            }
        });
    }

    private void logout() {
        DemoHelper.getInstance().logout(false, new EMCallBack() {

            @Override
            public void onSuccess() {

            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {

            }
        });
    }


    /**
     * 调用注销接口
     */
    public void logoff() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().logoff(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AdData>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<AdData> response) {

            }

            @Override
            public void onFinish() {

            }
        });

    }

    /**
     * 拨打电话
     */

    public void call(String phoneNum) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        mcontext.startActivity(intent);
    }

    /**
     * 确认监听事件
     */
    private OnConfirmOrderListener onConfirmOrderListener;

    public interface OnConfirmOrderListener {
        void onConfirmOrder(int i);


    }

    /**
     * 设置确定事件
     *
     * @param onClickListener
     */
    public void setConfirmClickListener(OnConfirmOrderListener onClickListener) {
        this.onConfirmOrderListener = onClickListener;
    }
}
