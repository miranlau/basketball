package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.ui.adapter.AllMatchAdapter;
import com.uhisports.basketball.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/4/24 0024.
 */
public class DayRaceDataDialog extends Dialog {
    private ImageView delete;
    private TextView time;

    private RecyclerView recyclerView;
    private Activity context;
    private AllMatchAdapter mAdapter;
    private ArrayList<Match> matches;
    private String date;

    public DayRaceDataDialog(Activity context, int themeResId, ArrayList<Match> list, String date) {
        super(context, themeResId);

        this.context = context;

        this.matches = list;

        this.date = date;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_race_data_dialog_layout);
        time = (TextView) findViewById(R.id.time);
        delete = (ImageView) findViewById(R.id.delete);

        recyclerView = (RecyclerView) findViewById(R.id.rv_team);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        time.setText(date);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //数据显示
        initData();

    }

    public void initData() {
        addTypeToData(matches);
        mAdapter = new AllMatchAdapter(context, matches);
        recyclerView.setAdapter(mAdapter);

    }
    //TODO
    private void addTypeToData(ArrayList<Match> list) {
        for (Match match : list) {
            if (match.lid == 10000) {
                if (match.status == 10) {
                    match.setItemType(Match.SINGLE);
                } else if (match.status == 20 || match.status == 100) {
                    match.setItemType(Match.SINGLE_ING);
                }
            } else {
                if (match.status == 0) {
                    match.setItemType(Match.MATCH_BEFORE);
                } else if (match.status == 10) {
                    match.setItemType(Match.MATCH_BEFORE_TWO);
                } else {
                    match.setItemType(Match.MATCH_ING);
                }

            }


        }
    }
}
