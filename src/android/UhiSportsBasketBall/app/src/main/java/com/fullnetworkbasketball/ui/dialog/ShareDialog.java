package com.fullnetworkbasketball.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.fullnetworkbasketball.models.ShareData;
import com.fullnetworkbasketball.ui.circle.PublishDynamicActivity;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

import static cn.sharesdk.framework.Platform.ShareParams;

/**
 * Created by Administrator on 2016/7/4 0004.
 */
public class ShareDialog extends Dialog implements OnClickListener {

    public Activity activity;
    public int account;

    public String pathUrl;

    public ShareData shareData;

    public ShareDialog(Context context) {
        super(context);
    }

    public ShareDialog(Context context, int themeResId) {
        super(context, themeResId);
    }


    public ShareDialog(Activity context, int themeResId, int account, ShareData shareData) {
        super(context, themeResId);

        this.activity = context;
        this.account = account;
        this.shareData = shareData;
    }

    public ShareDialog(Activity context, int themeResId, int account, String pathUrl) {
        super(context, themeResId);

        this.activity = context;
        this.account = account;
        this.pathUrl = pathUrl;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_dialog_layout);
        ShareSDK.initSDK(activity);

        ((LinearLayout) findViewById(R.id.share_wechat)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.share_qzone)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.share_sinaweibo)).setOnClickListener(this);

        ((LinearLayout) findViewById(R.id.share_wechatmoments)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.share_circle)).setOnClickListener(this);

    }


    @Override
    public void show() {
        super.show();

        DisplayMetrics display = getContext().getResources().getDisplayMetrics();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (display.widthPixels - 2 * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getContext().getResources().getDisplayMetrics())); //设置宽度
        lp.width = display.widthPixels;
        getWindow().setAttributes(lp);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.share_qzone:

                shareType(0);
                break;

            case R.id.share_wechat:
                shareType(1);
                break;

            case R.id.share_wechatmoments:
                shareType(2);
                break;

            case R.id.share_sinaweibo:
                shareType(3);
                break;

            case R.id.share_circle:

                if (UserManager.getIns().getUser() == null) {
                    Intent intent = new Intent();
                    intent.setClass(activity, LoginActivity.class);
                    activity.startActivity(intent);
                } else {
                    Intent intent = new Intent();
                    if (account == 1) {


                        intent.putExtra("data", shareData);
                        intent.putExtra("type", 1);
                        intent.setClass(activity, PublishDynamicActivity.class);


                        dismiss();
                    } else if (account == 0) {

                        intent.putExtra("data", pathUrl);
                        intent.putExtra("type", 0);
                        intent.setClass(activity, PublishDynamicActivity.class);

                        dismiss();
                    } else {
                        intent.putExtra("data", pathUrl);
                        intent.putExtra("type", 2);
                        intent.setClass(activity, PublishDynamicActivity.class);

                        dismiss();
                    }
                    activity.startActivity(intent);
                }

                break;

        }

    }

    //分享的get类型
    String Type[] = new String[]{QZone.NAME, Wechat.NAME, WechatMoments.NAME, SinaWeibo.NAME};


    /**
     * 0 qq空间  1 微信好友  2 朋友圈  3 新浪微博
     *
     * @param type
     */
    //  shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.logo));
    public void shareType(int type) {

        ShareParams shareParams = new ShareParams();


        Platform platform = null;
        if (type == 0) {

            if (account == 1) {
                shareParams.setTitle(shareData.title);
                shareParams.setTitleUrl(shareData.url); // 标题的超链接
                shareParams.setText(shareData.description);

                shareParams.setSite(shareData.description);
                shareParams.setSiteUrl(shareData.url);
                shareParams.setImagePath(shareData.icon);


            } else if(account==0){

                shareParams.setTitle(activity.getResources().getString(R.string.app_name));
                shareParams.setTitleUrl(pathUrl);
                shareParams.setText("我在使用全网足球，这是我的个人数据");

                shareParams.setSite(activity.getResources().getString(R.string.app_name));
                shareParams.setSiteUrl(pathUrl);
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));

            }else {
                shareParams.setTitle(activity.getResources().getString(R.string.app_name));
                shareParams.setTitleUrl(pathUrl);
                shareParams.setText("我在使用全网足球，这是球队详情数据");
                shareParams.setSite(activity.getResources().getString(R.string.app_name));
                shareParams.setSiteUrl(pathUrl);
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
            }

            platform = ShareSDK.getPlatform(Type[0]);


        }

        if (type == 1) {
            shareParams.setShareType(Platform.SHARE_WEBPAGE);
            platform = ShareSDK.getPlatform(Type[1]);
            if (account == 1) {


                shareParams.setTitle(activity.getResources().getString(R.string.app_name));

                shareParams.setText(shareData.description);
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setUrl(shareData.url);

            } else if(account==0) {

                shareParams.setTitle(activity.getResources().getString(R.string.app_name));

                shareParams.setText("我在使用全网足球，这是我的个人数据");
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setUrl(pathUrl);

            }else {
                shareParams.setTitle(activity.getResources().getString(R.string.app_name));

                shareParams.setText("我在使用全网足球，这是球队详情数据");
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setUrl(pathUrl);
            }


        }


        if (type == 2) {

            platform = ShareSDK.getPlatform(Type[2]);
            shareParams.setShareType(Platform.SHARE_WEBPAGE);
            if (account == 1) {


                shareParams.setTitle(activity.getResources().getString(R.string.app_name));

                shareParams.setText(shareData.description);
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setUrl(shareData.url);

            } else if(account==0) {
                shareParams.setTitle(activity.getResources().getString(R.string.app_name));

                shareParams.setText("我在使用全网足球，这是我的个人数据");
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setUrl(pathUrl);


            }else {
                shareParams.setTitle(activity.getResources().getString(R.string.app_name));

                shareParams.setText("我在使用全网足球，这是球队详情数据");
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setUrl(pathUrl);

            }

        }


        if (type == 3) {
            platform = ShareSDK.getPlatform(Type[3]);
            if (account == 1) {

                shareParams.setImagePath(shareData.icon);
                shareParams.setText(shareData.description + shareData.url);

            } else if(account==0) {
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setText("我在使用全网足球，这是我的个人数据." + pathUrl);
            }else {
                shareParams.setImageData(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.icon));
                shareParams.setText("我在使用全网足球，这是球队详情数据." + pathUrl);
            }
        }


        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                T.showShort(activity, "分享成功");
                dismiss();
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                T.showShort(activity, "分享失败");
                dismiss();

                Log.i("error", throwable.toString());

            }

            @Override
            public void onCancel(Platform platform, int i) {

                T.showShort(activity, "分享取消");
                dismiss();
            }
        });
        platform.share(shareParams);

    }


}
