package com.fullnetworkbasketball.ui.find;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.uhisports.basketball.R;

public class CompetitionTeamActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {


    public WebView webView;

    //赛事id
    public int leagueId;

    public LoadingDialog loadingDialog;
    public SwipeRefreshLayout swipeRefreshLayout;
    String path;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_competition_team);
    }

    @Override
    protected void initializeViews() {

        webView = (WebView) findViewById(R.id.race_wb);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void initializeData() {

        Intent intent = getIntent();
        leagueId = intent.getIntExtra("leagueId", 0);
        path = Api.getWebBaseUrl() + "ctl/league/queryTeamsByLeagueId?leagueId=" + leagueId;

        // initWebView(path);

    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    public void initWebView(String path) {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(CompetitionTeamActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });
        webView.addJavascriptInterface(new JavaScriptObject(), "JSBridge");
        webView.loadUrl(path);


    }

    @Override
    public void onRefresh() {
        initWebView(path);
    }

    class JavaScriptObject {

        /**
         * 球队
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void ActionAccessTeam(String teamId) {
            Intent intent = new Intent();
            intent.putExtra("teamId", Integer.parseInt(teamId));
            intent.setClass(CompetitionTeamActivity.this, OtherTeamDetailsActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.competition_team);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }
}
