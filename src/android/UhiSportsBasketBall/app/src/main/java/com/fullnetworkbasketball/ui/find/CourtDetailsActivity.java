package com.fullnetworkbasketball.ui.find;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class CourtDetailsActivity extends BaseActivity {
    @Bind(R.id.rb_evaluation_racing)
    RatingBar rbEvaluationRacing;
    @Bind(R.id.label_court_one)
    TextView labelCourtOne;
    @Bind(R.id.image_court_one)
    ImageView imageCourtOne;
    @Bind(R.id.filter_football_court_one)
    RelativeLayout filterFootballCourtOne;
    @Bind(R.id.label_court_two)
    TextView labelCourtTwo;
    @Bind(R.id.image_court_two)
    ImageView imageCourtTwo;
    @Bind(R.id.filter_football_court_two)
    RelativeLayout filterFootballCourtTwo;
    @Bind(R.id.label_court_three)
    TextView labelCourtThree;
    @Bind(R.id.image_cout_three)
    ImageView imageCoutThree;
    @Bind(R.id.filter_football_court_three)
    RelativeLayout filterFootballCourtThree;
    @Bind(R.id.label_court_four)
    TextView labelCourtFour;
    @Bind(R.id.image_court_four)
    ImageView imageCourtFour;
    @Bind(R.id.filter_football_court_four)
    RelativeLayout filterFootballCourtFour;
    @Bind(R.id.label_court_five)
    TextView labelCourtFive;
    @Bind(R.id.image_court_five)
    ImageView imageCourtFive;
    @Bind(R.id.filter_football_court_five)
    RelativeLayout filterFootballCourtFive;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_court_details_layout);
    }

    @Override
    protected void initializeViews() {
        filterFootballCourtOne.setOnClickListener(this);
        filterFootballCourtTwo.setOnClickListener(this);
        filterFootballCourtThree.setOnClickListener(this);
        filterFootballCourtFour.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.court_details);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }
    private void refreshState(){
        labelCourtOne.setSelected(false);
        imageCourtOne.setVisibility(View.GONE);
        labelCourtTwo.setSelected(false);
        imageCourtTwo.setVisibility(View.GONE);
        labelCourtThree.setSelected(false);
        imageCoutThree.setVisibility(View.GONE);
        labelCourtFour.setSelected(false);
        imageCourtFour.setVisibility(View.GONE);
    }
    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
