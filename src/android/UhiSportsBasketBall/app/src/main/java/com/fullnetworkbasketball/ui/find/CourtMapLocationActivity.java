package com.fullnetworkbasketball.ui.find;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.fullnetworkbasketball.map.GeocodeSearchProxy;
import com.fullnetworkbasketball.map.MapLocationProxy;
import com.uhisports.basketball.R;

public class CourtMapLocationActivity extends Activity implements View.OnClickListener, GeocodeSearchProxy.OnGeocodeListener, AMap.OnMarkerClickListener, GeocodeSearch.OnGeocodeSearchListener {

    private MapView mapView;
    private double longitude;
    private double latitude;
    private AMap mMap;

    private PoiSearch.Query query;
    private PoiSearch poiSearch;

    private GeocodeSearchProxy geocodeSearchProxy;
    private LatLonPoint mCurrentLocation;
    private Marker mMarker;

    private MapLocationProxy locationProxy;
    private GeocodeSearch geocoderSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_court_map_location);

        initData();

        initDataMap(savedInstanceState);

    }

    private void initData() {
        Intent intent = getIntent();
        longitude = intent.getDoubleExtra("longitude", 0.0);
        latitude = intent.getDoubleExtra("latitude", 0.0);

        geocodeSearchProxy = new GeocodeSearchProxy(this);
        geocodeSearchProxy.setOnGeocodeListener(this);
        mapView = (MapView) findViewById(R.id.location);
        ((ImageView) findViewById(R.id.back)).setOnClickListener(this);

        if (mMap == null) {
            mMap = mapView.getMap();
            setUpMap();
        }
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
    }

    private void setUpMap() {

        mMap.setOnMarkerClickListener(this);// 设置点击marker事件监听器
    }

    public void initDataMap(Bundle bundle) {
        mapView.onCreate(bundle);
        // AMap.moveCamera(CameraUpdate update);

        //locationProxy = new MapLocationProxy(this);
        geocoderSearch = new GeocodeSearch(this);
        geocoderSearch.setOnGeocodeSearchListener(this);

        LatLonPoint latLonPoint = new LatLonPoint(latitude, longitude);
        //latLonPoint参数表示一个Latlng，第二参数表示范围多少米，GeocodeSearch.AMAP表示是国测局坐标系还是GPS原生坐标系
        RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200, GeocodeSearch.AMAP);
        geocoderSearch.getFromLocationAsyn(query);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
        }

    }

    /**
     * 当通过位置转换为gps回调
     *
     * @param point
     */
    @Override
    public void onGeocodeSearched(LatLonPoint point) {


    }

    /**
     * 当通过gps转换为位置
     *
     * @param address 位置
     * @param latLng  gps
     */
    @Override
    public void onRegeocodeSearched(String address, LatLonPoint latLng) {


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    //逆地理编码回调接口
    @Override
    public void onRegeocodeSearched(RegeocodeResult result, int i) {


        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16f));

            if (mMarker != null) {
                mMarker.destroy();
            }
            mMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).anchor(0.5f, 0.5f).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_icon)));
        }


    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


}
