package com.fullnetworkbasketball.ui.find;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.db.MySQLite;
import com.fullnetworkbasketball.models.CommentDtos;
import com.fullnetworkbasketball.models.DynamicDetailData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.DynamicDetailsAdapter;
import com.fullnetworkbasketball.ui.adapter.DynamicDetailsListAdapter;
import com.fullnetworkbasketball.ui.adapter.GuideAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.circle.ShowBigImgActivity;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.fullnetworkbasketball.views.MyGridView;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class DynamicDetailsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    LinearLayout rlPublishEvaluation;

    private ImageView send;
    private EditText dynamicEt;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;

    private DynamicDetailsAdapter mAdapter;


    public int reieverid;

    public String senderName;
    public DynamicDetailsListAdapter adapter;
    public int dynamicId;

    //0代表评论 1代表回复
    public int flag = 0;

    private ContactOpponentCaptainDialog captainDialog;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_dynamic_details);
    }

    @Override
    protected void initializeViews() {


        dynamicEt = (EditText) findViewById(R.id.reply_et);
        ((ImageView) findViewById(R.id.send_iv)).setOnClickListener(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        recyclerView = (RecyclerView) findViewById(R.id.rv_circle_fragment);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(this);


    }

    @Override
    protected void initializeData() {

        Intent intent = getIntent();

        dynamicId = intent.getIntExtra("id", 0);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.send_iv:
                if (UserManager.getIns().getUser() == null) {
                    T.showShort(DynamicDetailsActivity.this, getResources().getString(R.string.dynamic_login_send));
                    return;
                }
                if (flag == 0) {
                    requestComment();

                } else {

                    commentDynamicReplay();

                }


                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
        SharePreHelper.getIns().putIsCircle(true);
    }

    /**
     * 获取动态详情数据网络请求
     */

    public void requestDynamicDetail() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("dynamicId", dynamicId);
        Api.getRetrofit().dynamicDetail(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<DynamicDetailData>>() {
            @Override
            public void onSuccess(final HttpResponseFWF2<DynamicDetailData> response) {
                if (response.isSuccess()) {
                    Log.i("dynamic", "动态详情:" + response.getData().content);

                    final ArrayList<CommentDtos> list = response.getData().commentDtos;

                    adapter = new DynamicDetailsListAdapter(DynamicDetailsActivity.this, R.layout.list_reply_item, list);


                    View view = DynamicDetailsActivity.this.getLayoutInflater().inflate(R.layout.include_dynamic_details_header, (ViewGroup) recyclerView.getParent(), false);

                    CircleImageView header = (CircleImageView) view.findViewById(R.id.header_iv);
                    TextView name = (TextView) view.findViewById(R.id.name_tv);
                    TextView time = (TextView) view.findViewById(R.id.time_formate_tv);

                    TextView read = (TextView) view.findViewById(R.id.read_num_tv);
                    TextView comment = (TextView) view.findViewById(R.id.commit_num_tv);
                    TextView priase = (TextView) view.findViewById(R.id.praise_num_tv);

                    TextView content = (TextView) view.findViewById(R.id.content_tv);
                    LinearLayout praiseLi = (LinearLayout) view.findViewById(R.id.praise_lt);
                    RelativeLayout deletell = (RelativeLayout) view.findViewById(R.id.delete_ll);


                    content.setText(response.getData().content);
                    ImageLoader.loadImage(DynamicDetailsActivity.this, response.getData().user.avatar, R.mipmap.default_person, header);
                    name.setText(response.getData().user.name);
                    time.setText(response.getData().pubTime);

                    read.setText(response.getData().readNum + "");
                    priase.setText(response.getData().laudNum + "");
                    comment.setText(response.getData().commentNum + "");
                    MyGridView myGridView = (MyGridView) view.findViewById(R.id.gridView);

                    if (response.getData().pictures == null) {

                    } else {
                        GuideAdapter guideAdapter = new GuideAdapter(DynamicDetailsActivity.this, response.getData().pictures);
                        myGridView.setAdapter(guideAdapter);

                        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                Intent intent = new Intent();
//
                                intent.setClass(DynamicDetailsActivity.this, ShowBigImgActivity.class);

                                intent.putExtra("path", response.getData().pictures);

                                intent.putExtra("position", position);
                                DynamicDetailsActivity.this.startActivity(intent);
                                overridePendingTransition(R.anim.zoomin, 0);
                            }
                        });
                    }

                    if (UserManager.getIns().getUser() != null && SharePreHelper.getIns().getIsMyPublish()) {

                        deletell.setVisibility(View.VISIBLE);

                        deletell.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                DeleteDialog();

                            }
                        });
                    }

//                    if (UserManager.getIns().getUser() == null) {
//                        T.showShort(DynamicDetailsActivity.this, getResources().getString(R.string.dynamic_login));
//                    } else {
                    praiseLi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ClickLaud();
                        }
                    });
//                    }

                    adapter.addHeaderView(view);
                    recyclerView.setAdapter(adapter);

//

                    adapter.setOnRecyclerViewItemClickListener(new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            // Toast.makeText(DynamicDetailsActivity.this, "" + Integer.toString(position), Toast.LENGTH_LONG).show();

                            CommentDtos commentDtos = list.get(position);
                            reieverid = commentDtos.sender.id;
                            senderName = commentDtos.sender.name;
                            // Toast.makeText(DynamicDetailsActivity.this, "" + Integer.toString(position) + senderName, Toast.LENGTH_LONG).show();
                            String commentone = "回复" + senderName + ":";
                            dynamicEt.setText(commentone);
                            dynamicEt.setSelection(commentone.length());
                            flag = 1;

                        }
                    });


                }
            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }


    public void DeleteDialog() {
        if (captainDialog == null) {
            captainDialog = new ContactOpponentCaptainDialog(DynamicDetailsActivity.this, R.style.callDialog, 13);
            captainDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            captainDialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {

                @Override
                public void onConfirmOrder(int i) {
                    if (i == 13) {
                        deleteDynamic();
                    }
                }

            });

        }
        captainDialog.show();


    }


    /**
     * 删除我的发表
     */
    public void deleteDynamic() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("dynamicId", dynamicId);
        Api.getRetrofit().deleteDynamic(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                MySQLite mySQLite = new MySQLite(DynamicDetailsActivity.this);

                String id = String.valueOf(dynamicId);
                mySQLite.DeleteData(id);
                onRefresh();
                finish();
            }

            @Override
            public void onFinish() {

            }
        });
    }

    /**
     * 动态评论
     */

    public void requestComment() {

        flag = 0;
        String commentContent = dynamicEt.getText().toString();
        if (TextUtil.isEmpty(commentContent)) {
            T.showShort(DynamicDetailsActivity.this, getResources().getString(R.string.comment_content_tempty));
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("dynamicid", dynamicId);
        params.put("content", commentContent);

        Api.getRetrofit().commentDynamic(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                //这里作用于关闭软键盘
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(dynamicEt.getWindowToken(), 0);
                dynamicEt.setText("");
                onRefresh();
            }

            @Override
            public void onFinish() {

            }
        });

    }

    /**
     * 动态回复
     */

    public void commentDynamicReplay() {

        flag = 1;

        String commentContent = dynamicEt.getText().toString();


        if (TextUtil.isEmpty(commentContent)) {
            T.showShort(DynamicDetailsActivity.this, getResources().getString(R.string.comment_content_tempty));
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("dynamicid", dynamicId);
        params.put("content", commentContent);
        params.put("reieverid", reieverid);

        Api.getRetrofit().commentDynamic(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                dynamicEt.setText("");
                //这里作用于关闭软键盘
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(dynamicEt.getWindowToken(), 0);
                onRefresh();
            }

            @Override
            public void onFinish() {

            }
        });

    }

    /**
     * 点赞接口
     */

    public void ClickLaud() {
        if (UserManager.getIns().getUser() == null) {
            T.showShort(DynamicDetailsActivity.this, getResources().getString(R.string.dynamic_login));
            return;
        }
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("dynamicId", dynamicId);
        Api.getRetrofit().clickLaud(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

//                if (response.isSuccess()) {


                if (response.getCode() == -1005) {


                    T.showShort(DynamicDetailsActivity.this, getResources().getString(R.string.dynamic_priased));
                } else if (response.getCode() == 0) {

                    onRefresh();

                    T.showShort(DynamicDetailsActivity.this, response.getMessage());
                    SharePreHelper.getIns().putIsCircle(true);
                }
            }
//            }

            @Override
            public void onFinish() {


            }
        });
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();

                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.dynamic_details);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onRefresh() {
        requestDynamicDetail();

    }
}
