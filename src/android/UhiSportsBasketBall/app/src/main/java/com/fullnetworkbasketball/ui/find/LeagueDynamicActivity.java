package com.fullnetworkbasketball.ui.find;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.uhisports.basketball.R;

import java.util.HashMap;
import java.util.Map;

public class LeagueDynamicActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    public WebView webView;
    String path;
    int infoId;
    LoadingDialog loadingDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_league_dynamic);
    }

    @Override
    protected void initializeViews() {
        webView = (WebView) findViewById(R.id.webview_wb);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void initializeData() {
        Intent intent = getIntent();
        infoId = intent.getIntExtra("infoId", 0);


        path = Api.getWebBaseUrl() + "ctl/league/leagueInfoDetail?infoId=" + infoId;
        onRefresh();
    }

    private void initWebView(String path) {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);


        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(LeagueDynamicActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });
        //传递token头文件
        Map<String, String> extraHeaders = new HashMap<String, String>();
        extraHeaders.put("token", UserManager.getIns().getUser().token);
        webView.addJavascriptInterface(new JavaScriptObject(), "JSBridge");

        //加载网页
        webView.loadUrl(path, extraHeaders);


    }

    @Override
    public void onRefresh() {
        initWebView(path);
    }


    class JavaScriptObject {

        /**
         * 动态回复
         *
         * @param comment
         */

        @JavascriptInterface //sdk17版本以上加上注解

        //球队
        public void ActionComment(String[] comment) {


            HashMap<String, Object> params = new HashMap<>();

            params.put("content", comment[0]);
            params.put("infoId", comment[1]);
            Api.getRetrofit().DynamicReplyData(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
                @Override
                public void onSuccess(HttpResponseFWF2 response) {
                    if (response.isSuccess()) {
//                        Log.i("tag", "数据：" + response.getMessage());
                        initWebView(path);

                    }

                }

                @Override
                public void onFinish() {

                }
            });
        }

        /**
         * 点赞 接口
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void ActionLaud(int inf) {

            HashMap<String, Object> params = new HashMap<>();

            params.put("infoId", infoId);

            Api.getRetrofit().DynamicPraiseData(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
                @Override
                public void onSuccess(HttpResponseFWF2 response) {
                    if (response.isSuccess()) {
                        initWebView(path);
                    }

                }

                @Override
                public void onFinish() {

                }
            });
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.dynamic_detail_league);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                LeagueDynamicActivity.this.finish();
                break;
        }
    }


    /**
     * 设置返回键盘监听，用于处理退出不掉
     *
     * @param keyCode
     * @param event
     * @return
     */


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            LeagueDynamicActivity.this.finish();
            return true;
        }
        //return super.onKeyDown(keyCode, event);
        return false;
    }
}
