package com.fullnetworkbasketball.ui.find;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.uhisports.basketball.R;

public class ListActivity extends BaseActivity {

    public WebView webView;

    public LoadingDialog loadingDialog;
    //public SwipeRefreshLayout swipeRefreshLayout;
    public String path;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_list);
    }

    @Override
    protected void initializeViews() {

        webView = (WebView) findViewById(R.id.web);

        //  swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);


        // swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // onRefresh();
    }

    @Override
    protected void initializeData() {

        Intent intent = getIntent();


        path = Api.getWebBaseUrl() + "ctl/league/leagueBang?leagueId=" + intent.getIntExtra("leagueId", 0);

        initWebView(path);


    }

    private void initWebView(String path) {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);


        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(ListActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                loadingDialog.dismiss();

                // swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });
        //加载网页
        webView.loadUrl(path);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.list_list);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

//    @Override
//    public void onRefresh() {
//
//        initWebView(path);
//    }
}
