package com.fullnetworkbasketball.ui.find;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.adapter.FindMatchDetailsListAdpter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/27 0027.
 */
public class ListOfFindActivity extends BaseActivity {
    @Bind(R.id.label_integral)
    TextView labelIntegral;
    @Bind(R.id.image_integral)
    ImageView imageIntegral;
    @Bind(R.id.filter_football_find_integral)
    RelativeLayout filterFootballFindIntegral;
    @Bind(R.id.label_top_score)
    TextView labelTopScore;
    @Bind(R.id.image_top_score)
    ImageView imageTopScore;
    @Bind(R.id.filter_football_find_top_score)
    RelativeLayout filterFootballFindTopScore;
    @Bind(R.id.label_card)
    TextView labelCard;
    @Bind(R.id.image_card)
    ImageView imageCard;
    @Bind(R.id.filter_football_find_card)
    RelativeLayout filterFootballFindCard;
    @Bind(R.id.find_list_sub_title)
    LinearLayout findListSubTitle;
    @Bind(R.id.listView_score_list)
    ListView listViewScoreList;
    private FindMatchDetailsListAdpter adpter;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_find_list_layout);
    }

    @Override
    protected void initializeViews() {
        filterFootballFindIntegral.setOnClickListener(this);
        filterFootballFindTopScore.setOnClickListener(this);
        filterFootballFindCard.setOnClickListener(this);
        refreshState();
        labelIntegral.setSelected(true);
        imageIntegral.setVisibility(View.VISIBLE);
        View view = getLayoutInflater().inflate(R.layout.integal_table_sub_title_layout, null);
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        findListSubTitle.removeAllViews();
        findListSubTitle.addView(view);
        adpter = new FindMatchDetailsListAdpter(this,1);
        listViewScoreList.setAdapter(adpter);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_find_integral:
                refreshState();
                labelIntegral.setSelected(true);
                imageIntegral.setVisibility(View.VISIBLE);
                View view = getLayoutInflater().inflate(R.layout.integal_table_sub_title_layout, null);
                view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                findListSubTitle.removeAllViews();
                findListSubTitle.addView(view);
                adpter = new FindMatchDetailsListAdpter(this,1);
                listViewScoreList.setAdapter(adpter);
                break;
            case R.id.filter_football_find_top_score:
                refreshState();
                labelTopScore.setSelected(true);
                imageTopScore.setVisibility(View.VISIBLE);
                View view1 = getLayoutInflater().inflate(R.layout.top_score_sub_title_layout, null);
                view1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                findListSubTitle.removeAllViews();
                findListSubTitle.addView(view1);

                adpter = new FindMatchDetailsListAdpter(this,2);
                listViewScoreList.setAdapter(adpter);
                break;
            case R.id.filter_football_find_card:
                refreshState();
                labelCard.setSelected(true);
                imageCard.setVisibility(View.VISIBLE);
                View view2 = getLayoutInflater().inflate(R.layout.card_record_sub_title_layout, null);
                view2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                findListSubTitle.removeAllViews();
                findListSubTitle.addView(view2);
                adpter = new FindMatchDetailsListAdpter(this,3);
                listViewScoreList.setAdapter(adpter);
                break;
        }

    }

    private void refreshState() {
        labelIntegral.setSelected(false);
        imageIntegral.setVisibility(View.GONE);
        labelTopScore.setSelected(false);
        imageTopScore.setVisibility(View.GONE);
        labelCard.setSelected(false);
        imageCard.setVisibility(View.GONE);
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.list);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
