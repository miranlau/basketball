package com.fullnetworkbasketball.ui.find;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.League;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupChooseTeamDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.JavaScriptObject;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/27 0027.
 */
public class MatchDetailsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private int matchId;
    @Bind(R.id.join_the_match)

    TextView joinTheMatch;
    WebView webView;

    ContactOpponentCaptainDialog dialog;
    /**
     * 报名类型
     */

    public int data_type;

    //是不是已经关注
    public boolean isfollow;

    private BottomPupChooseTeamDialog bottomPupDialog;

    private LoadingDialog loadingDialog;
    /**
     * by  cht
     * 判断是否有球队
     */
    private ArrayList<Team> list = new ArrayList<>();


    /**
     * by cht
     * <p/>
     * 创建球队
     */

    private ContactOpponentCaptainDialog createDialog;
    /**
     * by cht
     * 取消报名
     */


    private ContactOpponentCaptainDialog cancelDialog;
    /**
     * by cht
     * 判断是不是已经报名
     */

    private boolean isRegister;

    String path;


    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_find_match_details_cht);
    }

    @Override
    protected void initializeViews() {

        joinTheMatch.setOnClickListener(this);

        webView = (WebView) findViewById(R.id.match_wb);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void initializeData() {
//
//        if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {
//
//            LoginDialog();
//            return;
//        }
        getTeamMessage();
        Intent intent = getIntent();
        matchId = intent.getIntExtra("matchId", 0);
        // type = intent.getIntExtra("staus", 0);
        Log.i("data", "赛事id：" + matchId);

        path = Api.getWebBaseUrl() + "ctl/league/queryLeagueView?leagueId=" + matchId;
        Log.i("data", "网页地址：" + path);


        //  initWebView(path);

    }

    /**
     * cht
     * 网络请求
     */
    public void requestData() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("leagueId", matchId);

        Api.getRetrofit().LeagueDetail(params).enqueue(new RequestCallback<HttpResponseFWF2<League>>() {
                                                           @Override
                                                           public void onSuccess(HttpResponseFWF2<League> response) {


                                                               Log.i("data", "数据请求：" + response.getData());

                                                               isRegister = response.getData().gauntlet;
                                                               data_type = response.getData().status;
                                                               isfollow = response.getData().isfollow;
                                                               // T.showShort(MatchDetailsActivity.this, ":" + type);

                                                               if (data_type == 0) {
                                                                   isCompetition(isRegister);
                                                                   //               }
                                                                   // else if (data_type == 1) {
//                    joinTheMatch.setText(R.string.end_the_deadline);
//                    joinTheMatch.setBackgroundResource(R.mipmap.black_bt);
//
                                                               } else {
                                                                   joinTheMatch.setVisibility(View.GONE);
                                                               }

                                                               initWebView(data_type);

                                                               Log.i("data", "数据请求：" + data_type);
                                                           }

                                                           @Override
                                                           public void onFinish() {

                                                           }
                                                       }

        );


    }

    /**
     * 初始化 加载网页
     *
     * @param data_type
     */

    public void initWebView(int data_type) {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(MatchDetailsActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });


        if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {
            webView.loadUrl(path);
        } else {
            Map<String, String> extraHeaders = new HashMap<String, String>();
            extraHeaders.put("token", UserManager.getIns().getUser().token);
            webView.loadUrl(path, extraHeaders);
        }


        Log.i("data", "类型Type:" + data_type);

        if (data_type == 0) {

//            if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {
//
//                LoginDialog();
//
//                return;
//
//            }
            //报名中
            webView.addJavascriptInterface(new DyJavaScriptObject(), "JSBridge");
        } else if (data_type == 1) {


//            if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {
//
//                LoginDialog();
//
//                return;
//
//            }

            webView.addJavascriptInterface(new DyJavaScriptObject(), "JSBridge");
        } else if (data_type == 2) {
            //比赛中
            //球队

            webView.addJavascriptInterface(new JavaScriptObject(MatchDetailsActivity.this, matchId), "JSBridge");

        } else if (data_type == 3) {
            //比赛结束
            webView.addJavascriptInterface(new JavaScriptObject(MatchDetailsActivity.this, matchId), "JSBridge");

        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        // requestData();
        onRefresh();
    }

    @Override
    public void onRefresh() {
        requestData();
        //initWebView(data_type);
    }

    //关注与否
    public class DyJavaScriptObject {


        @JavascriptInterface //sdk17版本以上加上注解

        public void PayAttentionAction() {

            if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {

//                LoginDialog();

                Intent intent = new Intent(MatchDetailsActivity.this, LoginActivity.class);
                MatchDetailsActivity.this.startActivity(intent);
                return;

            }
            if (isfollow) {
                //取消关注
                CancelAttention();


            } else {

                //关注
                Attention();

            }
        }

    }

    //取消关注

    public void CancelAttention() {

        HashMap<String, Object> params = new HashMap<>();
        params.put("leagueId", matchId);

        Api.getRetrofit().cancelAttation(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                isfollow = false;
                T.showShort(MatchDetailsActivity.this, "取消关注");
            }

            @Override
            public void onFinish() {

            }
        });
    }

    //关注

    public void Attention() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("leagueId", matchId);

        Api.getRetrofit().Attation(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                isfollow = true;
                T.showShort(MatchDetailsActivity.this, "已经关注");
            }

            @Override
            public void onFinish() {

            }
        });
    }


    /**
     * 处理 是否已经报名参赛的情况
     */

    public void isCompetition(boolean isRegister) {

        if (!isRegister) {
            joinTheMatch.setBackgroundResource(R.mipmap.blue_bt);
            joinTheMatch.setText(R.string.join_the_match);
        } else {
            joinTheMatch.setBackgroundResource(R.mipmap.bottom_red_bt);
            joinTheMatch.setText(R.string.cancel_the_match);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.join_the_match:


                if (UserManager.getIns().getUser() == null) {
                    Intent intent = new Intent(MatchDetailsActivity.this, LoginActivity.class);
                    MatchDetailsActivity.this.startActivity(intent);
//                    // LoginDialog();
//                    App.getInst().toLogin();
                } else {
                    if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
                        App.getInst().toLogin();
                    } else if (data_type == 1) {
                        T.showShort(MatchDetailsActivity.this, "抱歉，报名已截止");
                    } else if (list.size() == 0) {

                        createTeamDialog();
                    } else if (TextUtils.equals(joinTheMatch.getText(), "报名参赛")) {
                        popDialog();
                    } else if (TextUtils.equals(joinTheMatch.getText(), "取消报名")) {
                        cancelDialog();
                    }
                }

                break;
        }
    }


    /**
     * 没有球队，去创建球队参加比赛
     */

    private void createTeamDialog() {

        if (createDialog == null) {
            createDialog = new ContactOpponentCaptainDialog(MatchDetailsActivity.this, R.style.callDialog, 7);
            createDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);

        }
        createDialog.show();

        WindowManager windowManager1 = MatchDetailsActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = createDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        createDialog.getWindow().setAttributes(lp1);
    }


    /**
     * 报名弹窗
     */
    private void popDialog() {
        if (bottomPupDialog == null) {
            bottomPupDialog = new BottomPupChooseTeamDialog(MatchDetailsActivity.this, R.style.callDialog, 1, list);
            bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
            bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            bottomPupDialog.setConfirmClickListener(new BottomPupChooseTeamDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(Team team) {

                    requestJoinMatch(team.id);

                }
            });
        }
        bottomPupDialog.show();

        WindowManager windowManager1 = MatchDetailsActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        bottomPupDialog.getWindow().setAttributes(lp1);
    }

    /**
     * 取消报名弹窗
     */

    private void cancelDialog() {
        if (cancelDialog == null) {
            cancelDialog = new ContactOpponentCaptainDialog(MatchDetailsActivity.this, R.style.callDialog, 6, matchId);
            cancelDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);

            cancelDialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {

                @Override
                public void onConfirmOrder(int i) {
                    requestCanelMatch();
                }

            });
        }
        cancelDialog.show();

        WindowManager windowManager1 = MatchDetailsActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = cancelDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        cancelDialog.getWindow().setAttributes(lp1);
    }


    /**
     * by cht
     * 获取 报名该比赛
     */
    public void requestJoinMatch(int teamId) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("leagueId", matchId);
        params.put("teamId", teamId);

        // Log.i("data", "联赛id：" + leagueId + "  队伍id:" + teamId);

        Api.getRetrofit().JoinLeague(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                joinTheMatch.setBackgroundResource(R.mipmap.bottom_red_bt);
                joinTheMatch.setText(R.string.cancel_the_match);

            }

            @Override
            public void onFinish() {

            }
        });
    }


    /**
     * by cht
     * 确定取消报名网络接口
     */

    public void requestCanelMatch() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("leagueId", matchId);


        Api.getRetrofit().CancelLeague(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                joinTheMatch.setBackgroundResource(R.mipmap.blue_bt);
                joinTheMatch.setText(R.string.join_the_match);
            }

            @Override
            public void onFinish() {

            }
        });

    }

    /**
     * by cht
     * 获取 球队消息 网络数据请求
     */

    public void getTeamMessage() {


        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();


        if (UserManager.getIns().getUser() != null) {
            params.put("userId", UserManager.getIns().getUser().id);
        }
        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallback<HttpResponseFWF2<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {
                if (response.isSuccess()) {

                    Player player = response.getData();

                    for (Team team : player.teams) {
                        if (team.leader == UserManager.getIns().getUser().id) {
                            list.add(team);
                        }
                    }


                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    /**
     * 跳转到登录弹窗
     */

    public void LoginDialog() {

        if (dialog == null) {
            dialog = new ContactOpponentCaptainDialog(MatchDetailsActivity.this, R.style.callDialog, 8);
        }
        dialog.show();
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.match_details);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

}
