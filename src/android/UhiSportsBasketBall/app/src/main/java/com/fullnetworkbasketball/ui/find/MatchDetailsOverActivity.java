package com.fullnetworkbasketball.ui.find;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fullnetworkbasketball.ui.adapter.FindRecyclerViewOfMatchDetailsOverAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/27 0027.
 */
public class MatchDetailsOverActivity extends BaseActivity {
    @Bind(R.id.recyclerView_match_details_over)
    XRecyclerView recyclerViewMatchDetailsOver;
    private FindRecyclerViewOfMatchDetailsOverAdapter adapter;
    private LinearLayout header_of_list;
    private LinearLayout competition_team;
    private LinearLayout schedule;
    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_match_details_over);
    }

    @Override
    protected void initializeViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        View header = LayoutInflater.from(this).inflate(R.layout.match_details_over_activity_header_layout, null);
        recyclerViewMatchDetailsOver.setLayoutManager(layoutManager);
        header.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        recyclerViewMatchDetailsOver.addHeaderView(header);

        adapter = new FindRecyclerViewOfMatchDetailsOverAdapter(this);
        recyclerViewMatchDetailsOver.setAdapter(adapter);
        header_of_list = (LinearLayout)header.findViewById(R.id.header_of_list);
        header_of_list.setOnClickListener(this);
        competition_team = (LinearLayout)header.findViewById(R.id.competition_team);
        competition_team.setOnClickListener(this);
        schedule = (LinearLayout)header.findViewById(R.id.schedule);
        schedule.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
    switch (v.getId()){
        case R.id.header_of_list:
            startActivity(ListOfFindActivity.class);
            break;
        case R.id.competition_team:
            startActivity(TeamCompetitionActivity.class);
            break;
        case R.id.schedule:
            startActivity(ScheduleActivity.class);
            break;
    }
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()){
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.match_details);
        getCustomActionBar().getRightText().setVisibility(View.VISIBLE);
        getCustomActionBar().getRightText().setTextSize(14);
        getCustomActionBar().getRightText().setText(R.string.attention);
        getCustomActionBar().getRightText().setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
