package com.fullnetworkbasketball.ui.find;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.uhisports.basketball.R;

public class RaceCalendarActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    public WebView webView;
    private int leagueId;

    public LoadingDialog loadingDialog;
    public SwipeRefreshLayout swipeRefreshLayout;
    String path;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_race_calendar);

    }

    @Override
    protected void initializeViews() {

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        webView = (WebView) findViewById(R.id.match_wb);

        swipeRefreshLayout.setOnRefreshListener(this);
    }


    @Override
    protected void initializeData() {

        Intent intent = getIntent();
        leagueId = intent.getIntExtra("leagueId", 0);

        //192.168.1.68:8082/api/ctl/league/queryMatchByLeagueId?leagueId=20122
        path = Api.getWebBaseUrl() + "ctl/league/queryMatchByLeagueId?leagueId=" + leagueId;

        // initWebView(path);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    public void initWebView(String path) {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(RaceCalendarActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                loadingDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });
        //加载网页
        webView.loadUrl(path);

        webView.addJavascriptInterface(new JavaScriptObject(), "JSBridge");

    }

    @Override
    public void onRefresh() {
        initWebView(path);
    }

    class JavaScriptObject {
        /**
         * 赛程 球队
         */

        @JavascriptInterface //sdk17版本以上加上注解
        public void ActionGoToTeam(int teamId) {
            Intent intent = new Intent();
            intent.putExtra("teamId", teamId);
            intent.putExtra("isFormHere", true);
            intent.putExtra("leagueId", leagueId);
            intent.setClass(RaceCalendarActivity.this, OtherTeamDetailsActivity.class);
            RaceCalendarActivity.this.startActivity(intent);


        }

        /**
         * 赛程 比赛
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void ActionGoToMatch(String[] matchArray) {

            int status = Integer.parseInt(matchArray[0]);
            if (status == 100) {

                Intent intent = new Intent(RaceCalendarActivity.this, InteractiveLiveActivity.class);
                intent.putExtra("matchId", Integer.parseInt(matchArray[1]));
                startActivity(intent);
            } else if (status == 0) {
                Intent intent = new Intent();
                intent.putExtra("matchId", Integer.parseInt(matchArray[1]));
                intent.setClass(RaceCalendarActivity.this, RacingActivity.class);
                startActivity(intent);
            } else if (status == 10 || status == 20) {
                Intent intent = new Intent();
                intent.putExtra("matchId", Integer.parseInt(matchArray[1]));
                intent.setClass(RaceCalendarActivity.this, TeamDetailsSignUpActivity.class);
                RaceCalendarActivity.this.startActivity(intent);
            }

        }


        /**
         * 赛程 比赛
         */
    }

    @Override
    public void onClick(View v) {

    }

    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.schedule);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }


    /**
     * 设置返回键盘监听，用于处理退出不掉
     *
     * @param keyCode
     * @param event
     * @return
     */


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
