package com.fullnetworkbasketball.ui.find;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.fullnetworkbasketball.ui.adapter.HomePageAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class ScheduleActivity extends BaseActivity {
    @Bind(R.id.recyclerView_schedule)
    XRecyclerView recyclerViewSchedule;
    private HomePageAdapter mAdapter;
    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_schedule);
    }

    @Override
    protected void initializeViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewSchedule.setLayoutManager(layoutManager);
        mAdapter = new HomePageAdapter(this);
        recyclerViewSchedule.setAdapter(mAdapter);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.schedule);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
