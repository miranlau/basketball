package com.fullnetworkbasketball.ui.find;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.uhisports.basketball.R;

public class StadiumDetailsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private WebView webView;

    private int arenasId;

    private String path;

    private SwipeRefreshLayout swipeRefreshLayout;

    private LoadingDialog loadingDialog;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_stadium_details);
    }

    @Override
    protected void initializeViews() {


        Intent intent = getIntent();
        arenasId = intent.getIntExtra("arenasId", 0);
        webView = (WebView) findViewById(R.id.court_web);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);

        swipeRefreshLayout.setOnRefreshListener(this);

        path = Api.getWebBaseUrl() + "ctl/arenas/detail?arenasId=" + arenasId;


    }


    public void initWebView() {

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBlockNetworkImage(false);
        webView.getSettings().setUseWideViewPort(true);


//
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(StadiumDetailsActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                loadingDialog.dismiss();

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });
        webView.loadUrl(path);

        webView.addJavascriptInterface(new JavaScriptObject(), "JSBridge");

    }


    @Override
    protected void initializeData() {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);

        getCustomActionBar().setTitleText(getResources().getString(R.string.court_details));

    }


    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRefresh() {

        initWebView();
    }

    class JavaScriptObject {

        //拨打电话
        @JavascriptInterface

        public void takePhone(String number) {

            CallMobile(number);

        }


        // 调用地图
        //  data是longitude和latitude的数组


        @JavascriptInterface

        public void takeMap(double[] data) {

            //经度
            double longitude = data[0];
            //纬度
            double latitude = data[1];

            if (longitude == 0 && latitude == 0) {
                return;
            }

            Intent intent = new Intent(StadiumDetailsActivity.this, CourtMapLocationActivity.class);
            // 市中心位于北纬30.67度，东经104.06度。
            intent.putExtra("longitude", longitude);

            intent.putExtra("latitude", latitude);

            StadiumDetailsActivity.this.startActivity(intent);
        }

    }

    public void CallMobile(String number) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:" + number);
        intent.setData(data);
        startActivity(intent);

    }
}
