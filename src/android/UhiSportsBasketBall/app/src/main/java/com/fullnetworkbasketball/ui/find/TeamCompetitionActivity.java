package com.fullnetworkbasketball.ui.find;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.fullnetworkbasketball.ui.adapter.CompetitionTeamAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/28 0028.
 */
public class TeamCompetitionActivity extends BaseActivity {
    @Bind(R.id.recyclerView_team_competition)
    XRecyclerView recyclerViewTeamCompetition;
    private CompetitionTeamAdapter adapter;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_team_competition);
    }

    @Override
    protected void initializeViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewTeamCompetition.setLayoutManager(layoutManager);
        adapter = new CompetitionTeamAdapter(this,0,null);
        recyclerViewTeamCompetition.setAdapter(adapter);
        recyclerViewTeamCompetition.setLoadingMoreEnabled(false);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.competition_team);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()){
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
