package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.adapter.AddPlayersAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddPlayersActivity extends BaseActivity {
    public Map<Integer, Boolean> check;
    //球队id
    public int teamId;
    //赛事id
    public int matchId;
    public ListView listView;

    public TextView sure;
    public ArrayList<Player> list;
    public AddPlayersAdapter addPlayersAdapter;

    public TextView all;
    private TextView right;

    public boolean flag;//全选，取消
    private Game game;

    public int type;//全选类型存在

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_add_players);
        game = (Game) getIntent().getSerializableExtra("game");
    }

    @Override
    protected void initializeViews() {
        listView = (ListView) findViewById(R.id.add_player_list);
        sure = (TextView) findViewById(R.id.sure_tv);
        all = (TextView) findViewById(R.id.all_tv);

//        all.setOnClickListener(this);
        sure.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

        Intent intent = getIntent();
        teamId = intent.getIntExtra("teamId", 0);
        matchId = intent.getIntExtra("matchId", 0);

        list = new ArrayList<Player>();
        requestSearchPlayer();

        check = new HashMap<Integer, Boolean>();
        for (int i = 0; i < list.size(); i++) {
            check.put(i, false);
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sure_tv:

                //添加新增球员
                addPlayersAdapter.requestNewsPlayer();


                break;

            case R.id.all_tv:


                break;
        }
    }


    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.add_new_player);

        getCustomActionBar().setRightTextVisible(true);

        right = getCustomActionBar().getRightText();

        right.setText("全选");
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals("全选", right.getText().toString())) {


                    for (int i = 0; i < list.size(); i++) {
                        addPlayersAdapter.getCheck().put(i, true);
                    }
                    flag = true;
                    type = 1;

                    getCustomActionBar().setRightText("取消");
                    //  right.setText("取消");
                } else {
                    for (int i = 0; i < list.size(); i++) {

                        if (addPlayersAdapter.getCheck().get(i)) {
                            addPlayersAdapter.getCheck().put(i, false);
                        }
                    }

                    flag = false;
                    getCustomActionBar().setRightText("全选");

                }
                addPlayersAdapter.setFlag(flag, type);
                addPlayersAdapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * 查找队伍中没有报名的球员
     */
    public void requestSearchPlayer() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("teamId", teamId);
        params.put("matchId", matchId);

        Api.getRetrofit().searchPlayers(params).enqueue(new RequestCallback<HttpResponseFWFArray<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWFArray<Player> response) {

                if (response.isSuccess()) {

                    list = response.getData();
                    addPlayersAdapter = new AddPlayersAdapter(AddPlayersActivity.this, list, teamId, matchId,game);
                    listView.setAdapter(addPlayersAdapter);
                    Log.i("data", "数据======>：" + response.getData().toString());
//
                } else {
                    T.showShort(AddPlayersActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                finish();
                break;


        }

    }


}
