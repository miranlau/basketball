package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/24 0024.
 */
public class AvgAgeChoiceActivity extends BaseActivity {
    @Bind(R.id.age_left)
    EditText ageLeft;
    @Bind(R.id.age_right)
    EditText ageRight;

    @Override
    protected void setContentView() {
        setContentView(R.layout.avg_age_choice_activity_layout);

    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void initializeData() {
        ageLeft.setText(getIntent().getStringExtra("youngest"));
        ageRight.setText(getIntent().getStringExtra("oldest"));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.age_choice_title);
        getCustomActionBar().getRightText().setVisibility(View.VISIBLE);
        getCustomActionBar().setRightText(R.string.save);
        getCustomActionBar().setRightTextColor(R.color.white);
    }

    private void verification() {


        if (ageLeft.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_youngest_age));
            return;
        }
        if (ageRight.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_oldest_age));
            return;
        }


        int left = Integer.parseInt(ageLeft.getText().toString());
        int right = Integer.parseInt(ageRight.getText().toString());

        if (left < 13 || left > 60 || right < 13 || right > 60||left > right) {

            T.showShort(this, getResources().getString(R.string.age_value));
            return;
        }

        Intent intent = new Intent();
        intent.putExtra("youngest", ageLeft.getText().toString().trim());
        intent.putExtra("oldest", ageRight.getText().toString().trim());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_txt:
                verification();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
