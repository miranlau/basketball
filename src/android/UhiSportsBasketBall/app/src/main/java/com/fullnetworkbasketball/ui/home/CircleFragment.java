package com.fullnetworkbasketball.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.AllCircleData;
import com.fullnetworkbasketball.models.CircleDynamicData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.CircleFragmentAdapter;
import com.fullnetworkbasketball.ui.base.BaseFragment;
import com.fullnetworkbasketball.ui.circle.PublishDynamicActivity;
import com.fullnetworkbasketball.ui.user.MineActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2016/4/15 0015.
 */
public class CircleFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.label_circle_all)
    TextView labelCircleAll;
    @Bind(R.id.image_circle_all)
    ImageView imageCircleAll;
    @Bind(R.id.filter_football_circle_all)
    RelativeLayout filterFootballCircleAll;
    @Bind(R.id.label_circle_friend)
    TextView labelCircleFriend;
    @Bind(R.id.image_circle_friend)
    ImageView imageCircleFriend;
    @Bind(R.id.filter_football_circle_friend)
    RelativeLayout filterFootballCircleFriend;
//    @Bind(R.id.rv_circle_fragment)

    @Bind(R.id.publish)
    ImageView publish;
    @Bind(R.id.mine)


    ImageView mine;
//    @Bind(R.id.swipeLayout)
//    SwipeLayout swipeLayout;


    private SwipeRefreshLayout swipeLayout;

    private RecyclerView recyclerViewCircleFragment;

    private int mCurrentCounter = 0;
    private int total;

    public int flag = 0;
    private int total_count;
    //圈子数据集合
    private ArrayList<CircleDynamicData> list;

    public int pageNumber = 1;

    private CircleFragmentAdapter adapter;

    @Override
    protected int getRootViewLayoutId() {
        return R.layout.fragment_circle_layout;
    }

    @Override
    protected void initializeViews() {
        filterFootballCircleAll.setOnClickListener(this);
        filterFootballCircleFriend.setOnClickListener(this);
        mine.setOnClickListener(this);
        refreshState();
        labelCircleAll.setSelected(true);
        imageCircleAll.setVisibility(View.VISIBLE);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeLayout.setOnRefreshListener(this);

        recyclerViewCircleFragment = (RecyclerView) findViewById(R.id.rv_circle_fragment);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewCircleFragment.setLayoutManager(layoutManager);
        onRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserManager.getIns().getUser() == null) {
            mine.setImageResource(R.mipmap.default_person);
            onRefresh();

        } else {
            if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
//                mine.setImageResource(R.mipmap.default_person);
//                onRefresh();
            } else {
                UserManager.getIns().autoLogin();
                upDeviceToken();

                if (SharePreHelper.getIns().getModifuHeader()) {
                    ImageLoader.loadCicleImage(this, SharePreHelper.getIns().getHeaderUrl(),R.mipmap.default_person, mine);
                } else {
                    ImageLoader.loadCicleImage(this, UserManager.getIns().getUser().avatar,R.mipmap.default_person, mine);
                }
                if (SharePreHelper.getIns().getIsCircle()) {
                    onRefresh();
                    SharePreHelper.getIns().putIsCircle(false);
                }
            }
        }


    }

    /**
     * 上传deviceToken网络请求
     */
    public void upDeviceToken() {

        //deviceToken
        String deviceToken = JPushInterface.getRegistrationID(getContext());

        //设备id
        final TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "" + tm.getDeviceId();
        int deviceType = 0;

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", UserManager.getIns().getUser().id);
        params.put("deviceId", deviceId);
        params.put("deviceToken", deviceToken);
        params.put("deviceType", deviceType);
        Api.getRetrofit().upDeviceToken(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {
        publish.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_circle_all:

                recyclerViewCircleFragment.removeAllViews();
                flag = 0;
                refreshState();
                labelCircleAll.setSelected(true);
                imageCircleAll.setVisibility(View.VISIBLE);

                onRefresh();
                break;
            case R.id.filter_football_circle_friend:

                recyclerViewCircleFragment.removeAllViews();
                flag = 1;
                refreshState();
                labelCircleFriend.setSelected(true);
                imageCircleFriend.setVisibility(View.VISIBLE);
                if (UserManager.getIns().getUser() == null) {
                    list = null;
                    adapter = new CircleFragmentAdapter(getActivity(), R.layout.match_details_over_adapter_item_img_txt, list, 1);
                    recyclerViewCircleFragment.setAdapter(adapter);
                    getFriendDynamicPager();
                } else {
                    onRefresh();
                }

                break;
            case R.id.publish:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                } else {
                    startActivity(PublishDynamicActivity.class);
                }

                break;
            case R.id.mine:

                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                } else {
                    if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
                        App.getInst().toLogin();
                    } else {
                        startActivity(MineActivity.class);
                    }
                }
                break;
        }

    }


    /**
     * 好友
     */
    public void getFriendDynamicPager() {


        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("pageNumber", pageNumber);

        params.put("pageSize", 20);
        Api.getRetrofit().getFriendDynamicPager(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllCircleData>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<AllCircleData> response) {

                if (response.isSuccess()) {

                    list = response.getData().content;
                    total_count = response.getData().total;
                    total = list.size();
                    if (total > 0) {
                        pageNumber++;
                    }
                    adapter = new CircleFragmentAdapter(getActivity(), R.layout.match_details_over_adapter_item_img_txt, list, 1);


                    adapter.openLoadMore(list.size(), true);
                    adapter.openLoadAnimation();
                    lodMore(adapter);
                    recyclerViewCircleFragment.setAdapter(adapter);
                    mCurrentCounter = adapter.getData().size();


                } else {
                    T.showShort(getActivity(), response.getMessage());

                }

            }

            @Override
            public void onFinish() {
                swipeLayout.setRefreshing(false);
            }
        });


    }


    /**
     * cht
     * 网络请求
     */

    public void requestAllDynamicData() {

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageSize", 20);
        params.put("pageNumber", pageNumber);
        Api.getRetrofit().getAllDynamicPager(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllCircleData>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<AllCircleData> response) {

                if (response.isSuccess()) {

                    list = response.getData().content;
                    total_count = response.getData().total;
                    total = list.size();
                    if (total > 0) {
                        pageNumber++;
                    }
                    adapter = new CircleFragmentAdapter(getActivity(), R.layout.match_details_over_adapter_item_img_txt, list, 1);


                    adapter.openLoadMore(list.size(), true);
                    adapter.openLoadAnimation();
                    lodMore(adapter);
                    recyclerViewCircleFragment.setAdapter(adapter);
                    mCurrentCounter = adapter.getData().size();

                } else {
                    T.showShort(getActivity(), response.getMessage());

                }

            }

            @Override
            public void onFinish() {
                swipeLayout.setRefreshing(false);
            }
        });


    }


    public void lodMore(final CircleFragmentAdapter adapter) {

        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {

                recyclerViewCircleFragment.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mCurrentCounter >= total_count) {
                            adapter.notifyDataChangedAfterLoadMore(false);
                            View view = getActivity().getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) recyclerViewCircleFragment.getParent(), false);
                            adapter.addFooterView(view);
                        } else {

                            HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                            params.put("pageNumber", pageNumber);

                            params.put("pageSize", 20);
                            Api.getRetrofit().getAllDynamicPager(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllCircleData>>(getActivity()) {
                                @Override
                                public void onSuccess(HttpResponseFWF2<AllCircleData> response) {

                                    if (response.isSuccess()) {

                                        if (response.getData().content.size() > 0) {
                                            adapter.notifyDataChangedAfterLoadMore(response.getData().content, true);
                                            mCurrentCounter = adapter.getData().size();
                                            pageNumber++;
                                        }
                                    }
                                }

                                @Override
                                public void onFinish() {

                                }
                            });
                        }
                    }

                });

            }

        });
    }

    private void refreshState() {
        labelCircleAll.setSelected(false);
        imageCircleAll.setVisibility(View.GONE);
        labelCircleFriend.setSelected(false);
        imageCircleFriend.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRefresh() {
        pageNumber = 1;
        if (flag == 0) {

            requestAllDynamicData();
        } else {

            getFriendDynamicPager();
        }

        SharePreHelper.getIns().putIsCircle(false);
    }
}