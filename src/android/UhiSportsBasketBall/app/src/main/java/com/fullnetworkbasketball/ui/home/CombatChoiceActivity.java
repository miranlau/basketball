package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/24 0024.
 */
public class CombatChoiceActivity extends BaseActivity {
    @Bind(R.id.combat_left)
    EditText combatLeft;
    @Bind(R.id.combat_right)
    EditText combatRight;

    @Override
    protected void setContentView() {
        setContentView(R.layout.combat_choice_activity_layout);


    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void initializeData() {
        combatLeft.setText(getIntent().getStringExtra("lowest"));
        combatRight.setText(getIntent().getStringExtra("highest"));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.combat_choice);
        getCustomActionBar().getRightText().setVisibility(View.VISIBLE);
        getCustomActionBar().setRightText(R.string.save);
        getCustomActionBar().setRightTextColor(R.color.white);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_txt:
                verification();
                break;
        }
    }

    private void verification() {


        if (combatLeft.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_lowest_combat));
            return;
        }
        if (combatRight.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_highest_combat));
            return;
        }

        int left = Integer.parseInt(combatLeft.getText().toString());
        int right = Integer.parseInt(combatRight.getText().toString());

        if (left < 1 || left > 100 || right < 1 || right > 100 || left > right) {

            T.showShort(this, getResources().getString(R.string.fight_value));
            return;
        }


        Intent intent = new Intent();
        intent.putExtra("lowest", combatLeft.getText().toString().trim());
        intent.putExtra("highest", combatRight.getText().toString().trim());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
