package com.fullnetworkbasketball.ui.home;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class CreateTeamStepFinalActivity extends BaseActivity {
    @Bind(R.id.et_input_team_name)
    EditText etInputTeamName;
    @Bind(R.id.next_step)
    TextView nextStep;
    @Bind(R.id.tv_code)
    TextView tvCode;
    private String url="";
    private String teamName= "";


    @Override
    protected void setContentView() {
        setContentView(R.layout.creat_team_step_final);
    }

    @Override
    protected void initializeViews() {
        nextStep.setOnClickListener(this);
        if (getIntent().getIntExtra("isApplyCodeJoin",0)==1){
            tvCode.setText("请输入该球队暗号");
            nextStep.setText("加入");
        }
    }

    @Override
    protected void initializeData() {
        teamName = getIntent().getExtras().getString("teamName");
        url= getIntent().getExtras().getString("header");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_step:
                if (getIntent().getIntExtra("isApplyCodeJoin",0)==1){
                    finish();
                }else {

                    if (etInputTeamName.getText().toString().trim().isEmpty()){
                        T.showShort(this, R.string.input_anhao);
                        return;
                    }

                    HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                    params.put("name",teamName);
                    params.put("password",etInputTeamName.getText().toString().trim());
                    params.put("logo",url);
                    Api.getRetrofit().creamTeam(params).enqueue(new RequestCallback<HttpResponseFWF>() {
                        @Override
                        public void onSuccess(HttpResponseFWF response) {
                            Logger.i("msg:" + response.toString());
                            if (!response.isSuccess()) {

                            } else {
                                T.showShort(getApplicationContext(), response.getMessage());
                                startActivity(HomeActivity.class);
                                finish();

                            }
                        }

                        @Override
                        public void onFinish() {

                        }
                    });




                    startActivity(HomeActivity.class);
                }

                break;
        }
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }


    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setTitleText(R.string.title);
        getCustomActionBar().setLeftImageView(R.mipmap.back);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
