package com.fullnetworkbasketball.ui.home;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class CreateTeamStepOneActivity extends BaseActivity {
    @Bind(R.id.et_input_team_name)
    EditText etInputTeamName;
    @Bind(R.id.next_step)
    TextView nextStep;

    @Override
    protected void setContentView() {
        setContentView(R.layout.creat_team_step_one);
    }

    @Override
    protected void initializeViews() {
        nextStep.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_step:
                if (etInputTeamName.getText().toString().trim().isEmpty()){
                    T.showShort(this, R.string.input_teamName);
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("teamName",etInputTeamName.getText().toString().trim());
                startActivity(CreateTeamStepTwoActivity.class,bundle);
                break;
        }
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }


    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setTitleText(R.string.title);
        getCustomActionBar().setLeftImageView(R.mipmap.white_delete);
    }


}
