package com.fullnetworkbasketball.ui.home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.CameraDialog;
import com.fullnetworkbasketball.utils.BitmapUtil;
import com.fullnetworkbasketball.utils.CameraUtil;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2016/4/19 0019.
 */
public class CreateTeamStepTwoActivity extends BaseActivity {
    @Bind(R.id.create_team_add_header)
    CircleImageView createTeamAddHeader;
    @Bind(R.id.next_step)
    TextView nextStep;
    @Bind(R.id.wait_upload)
    TextView waitUpload;
    private CameraDialog cameraDialog;
    private String teamName="";
    private LoadingDialog loadingDialog;
    private ArrayList<String> url;

    @Override
    protected void setContentView() {
        setContentView(R.layout.creat_team_step_two);
    }

    @Override
    protected void initializeViews() {
        createTeamAddHeader.setOnClickListener(this);
        nextStep.setOnClickListener(this);
        waitUpload.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {
        teamName = getIntent().getExtras().getString("teamName");
        url = new ArrayList<String>();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_team_add_header:
                if (cameraDialog == null) {
                    cameraDialog = new CameraDialog(this, R.style.callDialog);
                    cameraDialog.getWindow().setGravity(Gravity.BOTTOM);
                    cameraDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                }
                cameraDialog.show();
                break;
            case R.id.next_step:
                if (url.isEmpty()){
                    T.showShort(this, R.string.add_team_header);
                    return;
                }
                Bundle b = new Bundle();
                b.putString("teamName",teamName);
                b.putString("header",url.get(0));
                startActivity(CreateTeamStepFinalActivity.class,b);
                break;
            case R.id.wait_upload:
                Bundle b1 = new Bundle();
                b1.putString("teamName",teamName);
                startActivity(CreateTeamStepFinalActivity.class,b1);
                break;
        }
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.title);
    }
    private boolean uploadImage(String path) {
        if (!checkNetWork()) return false;
        File file = new File(path);
        if (!file.exists()) return true;
        RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody boy = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", "team")
                .addFormDataPart("files", file.getName(), fileBody)

                .build();
        loadingDialog = new LoadingDialog(this);
        loadingDialog.show();
        Api.getRetrofit().uploadFacePicTeam(boy).enqueue(new RequestCallback<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response != null && response.isSuccess()) {
//                    App.getInst().getUser().image = response.getDataFrist().fid;
//                    modifyUserInfo(response.getDataFrist().fid);

                    url= (ArrayList) response.getData();
                    Logger.i("url:" +url.get(0));

                }
            }

            @Override
            public void onFinish() {
                loadingDialog.dismiss();
            }

        });
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharePreHelper.getIns().setShouldShowNotification(false);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CameraUtil.TAKE_PHOTO:
                    String path = CameraUtil.getRealFilePath();
                    if (new File(path).exists()) {
                        //图片存在？
                        Logger.i("imagePath:" + path);
                        try {
                            Bitmap temp = BitmapUtil.revitionImageSize(path);
                            path = BitmapUtil.saveBitmap(this, temp, path);
                            createTeamAddHeader.setImageBitmap(temp);
                            if (uploadImage(path)) return;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }
                    break;
                case CameraUtil.PICK_PHOTO:
                    try {
                        String pickPath = CameraUtil.resolvePhotoFromIntent(CreateTeamStepTwoActivity.this, data);
                        Uri uri = Uri.fromFile(new File(pickPath));

                        if (!new File(pickPath).exists()) {
                            Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                        Bitmap header = BitmapUtil.revitionImageSize(pickPath);
                        String finalPath = BitmapUtil.saveBitmap(this, header, pickPath);
                        createTeamAddHeader.setImageBitmap(header);
                        if (uploadImage(finalPath)) return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

}
