package com.fullnetworkbasketball.ui.home;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Court;
import com.fullnetworkbasketball.models.CourtModel;
import com.fullnetworkbasketball.models.DividerPage;
import com.fullnetworkbasketball.models.League;
import com.fullnetworkbasketball.models.MatchDividerPage;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.CompetitionTeamAdapter;
import com.fullnetworkbasketball.ui.adapter.FindCourtFragmentAdapter;
import com.fullnetworkbasketball.ui.adapter.FindRecyclerViewAdapter;
import com.fullnetworkbasketball.ui.base.BaseFragment;
import com.fullnetworkbasketball.ui.user.MineActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2016/4/15 0015.
 */
public class FindFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.label_match)
    TextView labelMatch;
    @Bind(R.id.image_match)
    ImageView imageMatch;
    @Bind(R.id.filter_football_find_match)
    RelativeLayout filterFootballFindMatch;
    @Bind(R.id.label_team)
    TextView labelTeam;
    @Bind(R.id.image_team)
    ImageView imageTeam;
    @Bind(R.id.filter_football_find_team)
    RelativeLayout filterFootballFindTeam;
    @Bind(R.id.label_court)
    TextView labelCourt;
    @Bind(R.id.image_court)
    ImageView imageCourt;
    @Bind(R.id.filter_football_find_court)
    RelativeLayout filterFootballFindCourt;
    //    @Bind(R.id.recyclerView_find_fragment)
//    XRecyclerView recyclerViewFindFragment;
    @Bind(R.id.mine)
    ImageView mine;
    @Bind(R.id.search_team)
    EditText searchTeam;

    TextView cancel;
    RecyclerView recyclerView;

    SwipeRefreshLayout swipeRefreshLayout;


    /**
     *
     */
    private FindRecyclerViewAdapter adapter;

    private CompetitionTeamAdapter competitionTeamAdapter;

    private FindCourtFragmentAdapter findCourtFragmnetAdapter;

    private int mCurrentCounter = 0;
    private int total;


    private int total_count;
    /**
     * 球队 集合
     */
    private ArrayList<Team> teamsList;

    /**
     * 球队搜索
     */

    private ArrayList<Team> teamSearchList;

    /**
     * by cht
     * 赛事 集合
     */
    private ArrayList<League> matchesList;

    /**
     * 赛事搜索
     */

    private ArrayList<League> matchSearchList;

    /**
     * cht 赛事分页
     * matchPageNumber
     * 默认为 1
     */
    public int matchPageNumber = 1;

    public int teamPageNumber = 1;
    public int courtPageNumber = 1;

    /**
     * 场地集合
     */
    private ArrayList<Court> courtArrayList;

    /**
     * 赛事 每页的长度
     * <p/>
     * pageSize
     */

    public int pageSize = 20;
    /**
     * 设置 刷新时网络请求进哪一个 0 赛事 1 球队 2球场 3 搜索
     */
    public int flag = 0;

    @Override
    protected int getRootViewLayoutId() {
        return R.layout.fragment_find_layout;
    }

    @Override
    protected void initializeViews() {

        recyclerView = (RecyclerView) findViewById(R.id.rv_find_fragment);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);

        filterFootballFindMatch.setOnClickListener(this);
        filterFootballFindTeam.setOnClickListener(this);
        filterFootballFindCourt.setOnClickListener(this);
        mine.setOnClickListener(this);
        refreshState();
        labelMatch.setSelected(true);
        imageMatch.setVisibility(View.VISIBLE);

        cancel = (TextView) findViewById(R.id.cancel_tv);
        cancel.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        //如果不在这初始化 网络请求 则第一次进入不会看到数据

        swipeRefreshLayout.setOnRefreshListener(this);

        onRefresh();

        searchTeam.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {//修改回车键功能
// 先隐藏键盘
                    ((InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(
                                    getActivity()
                                            .getCurrentFocus()
                                            .getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);

                    if (flag == 0) {

                        onRefresh();


                    } else if (flag == 1) {

                        onRefresh();

                    } else {
                        onRefresh();
                    }


                }
                return false;
            }
        });
    }


    /**
     * by cht
     * 初始化全局的集合
     */

    public void initList() {

        matchesList = new ArrayList<League>();
        /**
         * 球队集合
         */
        teamsList = new ArrayList<Team>();

        teamSearchList = new ArrayList<Team>();

        matchSearchList = new ArrayList<League>();

    }


    /**
     * 搜索赛事
     */
    private void searchMatch() {
        if (searchTeam.getText().toString().trim().isEmpty()) {
            T.showShort(getActivity(), getActivity().getResources().getString(R.string.input_keyword));
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", matchPageNumber);

        params.put("pageSize", pageSize);
        params.put("keyword", searchTeam.getText().toString().trim());
//        try {
//            //params.put("keyword", URLEncoder.encode(searchTeam.getText().toString().trim(), "utf-8"));
//
//
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        Api.getRetrofit().SearchLeague(params).enqueue(new RequestCallback<HttpResponseFWF2<MatchDividerPage>>(getActivity()) {


            @Override
            public void onSuccess(HttpResponseFWF2<MatchDividerPage> response) {
                if (!response.isSuccess()) {

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    matchesList = response.getData().content;
                    total_count = response.getData().total;
//                        total_count = 0;


                    total = matchesList.size();


                    adapter = new FindRecyclerViewAdapter(getActivity(), R.layout.fragment_find_adapter_item, matchesList);
                    adapter.openLoadMore(matchesList.size(), true);

                    if (total > 0) {
                        matchPageNumber++;
                    }
                    if (mCurrentCounter < total_count) {
                        lodMore(adapter, 1);
                    }
                    recyclerView.setAdapter(adapter);
                    mCurrentCounter = adapter.getData().size();


                }
            }


            @Override
            public void onFinish() {

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 搜索球场
     */
    private void searchCourt() {
        if (searchTeam.getText().toString().trim().isEmpty()) {
            T.showShort(getActivity(), getActivity().getResources().getString(R.string.input_keyword));
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", courtPageNumber);

        params.put("pageSize", pageSize);
        params.put("keyword", searchTeam.getText().toString().trim());
//        try {
//            params.put("keyword", URLEncoder.encode(searchTeam.getText().toString().trim(), "utf-8"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        Api.getRetrofit().findCourt(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<CourtModel>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<CourtModel> response) {

                if (!response.isSuccess()) {
                    T.showShort(getActivity(), response.getMessage());
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    courtArrayList = response.getData().content;

                    total_count = response.getData().total;

                    total = courtArrayList.size();

                    if (total > 0) {
                        courtPageNumber++;
                    }

                    findCourtFragmnetAdapter = new FindCourtFragmentAdapter(getActivity(), R.layout.fragment_find_court_adapter_item, courtArrayList);
                    findCourtFragmnetAdapter.openLoadMore(courtArrayList.size(), true);

                    if (total_count > mCurrentCounter) {
                        lodMoreCourt(findCourtFragmnetAdapter, 1);
                    }

                    recyclerView.setAdapter(findCourtFragmnetAdapter);
                    mCurrentCounter = findCourtFragmnetAdapter.getData().size();

                }

            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }

        });
    }

    /**
     * 搜索球队
     */
    private void searchTeam() {
        if (searchTeam.getText().toString().trim().isEmpty()) {
            T.showShort(getActivity(), getActivity().getResources().getString(R.string.input_keyword));
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", teamPageNumber);

        params.put("pageSize", pageSize);
        params.put("keyword", searchTeam.getText().toString().trim());
//        try {
//            params.put("keyword", URLEncoder.encode(searchTeam.getText().toString().trim(), "utf-8"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        Api.getRetrofit().searchTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<DividerPage>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<DividerPage> response) {

                if (!response.isSuccess()) {
                    T.showShort(getActivity(), response.getMessage());
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    teamSearchList = response.getData().content;

                    total_count = response.getData().total;

                    total = matchesList.size();

                    if (total > 0) {
                        teamPageNumber++;
                    }

                    competitionTeamAdapter = new CompetitionTeamAdapter(getActivity(), R.layout.competition_team_adapter_item, teamSearchList);
                    competitionTeamAdapter.openLoadMore(teamSearchList.size(), true);

                    if (total_count > mCurrentCounter) {
                        lodMoreTeam(competitionTeamAdapter, 1);
                    }

                    recyclerView.setAdapter(competitionTeamAdapter);
                    mCurrentCounter = competitionTeamAdapter.getData().size();

                }

            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }

        });
    }

    @Override
    protected void initializeData() {


    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserManager.getIns().getUser() == null) {
            mine.setImageResource(R.mipmap.default_person);
        } else {
            if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {

                //UserManager.getIns().autoLogin();

                // upDeviceToken();
            } else {
                UserManager.getIns().autoLogin();

                upDeviceToken();
                if (SharePreHelper.getIns().getModifuHeader()) {
                    ImageLoader.loadCicleImage(this, SharePreHelper.getIns().getHeaderUrl(), R.mipmap.default_person,mine);
                } else {
                    ImageLoader.loadCicleImage(this, UserManager.getIns().getUser().avatar,R.mipmap.default_person, mine);
                }
            }
            onRefresh();
        }
    }

    /**
     * 球队数据
     */
    private void getTeamList() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", teamPageNumber);
        params.put("pageSize", pageSize);
        Api.getRetrofit().searchTeam(params).enqueue(new RequestCallback<HttpResponseFWF2<DividerPage>>(getActivity()) {
                                                         @Override
                                                         public void onSuccess(HttpResponseFWF2<DividerPage> response) {
                                                             Logger.i("msg:" + response.toString());
                                                             if (!response.isSuccess()) {
                                                                 T.showShort(getActivity(), response.getMessage());
                                                             } else {
                                                                 recyclerView.setVisibility(View.VISIBLE);
                                                                 teamSearchList = response.getData().content;


                                                                 total_count = response.getData().total;
                                                                 total = teamSearchList.size();

                                                                 if (total > 0) {
                                                                     teamPageNumber++;
                                                                 }

                                                                 competitionTeamAdapter = new CompetitionTeamAdapter(getActivity(), R.layout.competition_team_adapter_item, teamSearchList);
                                                                 competitionTeamAdapter.openLoadMore(teamSearchList.size(), true);
                                                                 if (total_count > mCurrentCounter) {
                                                                     lodMoreTeam(competitionTeamAdapter, 0);
                                                                 }

                                                                 recyclerView.setAdapter(competitionTeamAdapter);
                                                                 mCurrentCounter = competitionTeamAdapter.getData().size();

                                                             }

                                                         }

                                                         @Override
                                                         public void onFinish() {
                                                             swipeRefreshLayout.setRefreshing(false);
                                                         }
                                                     }
        );

    }

    /**
     * 球队数据加载更多
     */
    private void lodMoreTeam(final CompetitionTeamAdapter competitionTeamAdapter, final int staus) {

        competitionTeamAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
                                                         @Override
                                                         public void onLoadMoreRequested() {
                                                             recyclerView.post(new Runnable() {
                                                                                   @Override
                                                                                   public void run() {
                                                                                       if (mCurrentCounter >= total_count) {
                                                                                           competitionTeamAdapter.notifyDataChangedAfterLoadMore(false);
                                                                                           View view = getActivity().getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) recyclerView.getParent(), false);
                                                                                           competitionTeamAdapter.addFooterView(view);
                                                                                       } else {


                                                                                           if (staus == 0) {
                                                                                               HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                                                                                               params.put("pageNumber", teamPageNumber);
                                                                                               params.put("pageSize", pageSize);
                                                                                               Api.getRetrofit().searchTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<DividerPage>>(getActivity()) {
                                                                                                   @Override
                                                                                                   public void onSuccess(HttpResponseFWF2<DividerPage> response) {
                                                                                                       if (response.isSuccess()) {
                                                                                                           recyclerView.setVisibility(View.VISIBLE);
                                                                                                           if (response.getData().content.size() > 0) {
                                                                                                               competitionTeamAdapter.notifyDataChangedAfterLoadMore(response.getData().content, true);

                                                                                                               mCurrentCounter = competitionTeamAdapter.getData().size();

                                                                                                               teamPageNumber++;
                                                                                                           }
                                                                                                       }
                                                                                                   }

                                                                                                   @Override
                                                                                                   public void onFinish() {

                                                                                                   }
                                                                                               });
                                                                                           } else {
                                                                                               HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                                                                                               params.put("pageNumber", teamPageNumber);
                                                                                               try {
                                                                                                   params.put("keyword", URLEncoder.encode(searchTeam.getText().toString().trim(), "utf-8"));
                                                                                               } catch (UnsupportedEncodingException e) {
                                                                                                   e.printStackTrace();
                                                                                               }
                                                                                               params.put("pageSize", pageSize);
                                                                                               Api.getRetrofit().searchTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<DividerPage>>(getActivity()) {
                                                                                                   @Override
                                                                                                   public void onSuccess(HttpResponseFWF2<DividerPage> response) {
                                                                                                       if (response.isSuccess()) {
                                                                                                           recyclerView.setVisibility(View.VISIBLE);
                                                                                                           if (response.getData().content.size() > 0) {
                                                                                                               competitionTeamAdapter.notifyDataChangedAfterLoadMore(response.getData().content, true);

                                                                                                               mCurrentCounter = competitionTeamAdapter.getData().size();

                                                                                                               teamPageNumber++;
                                                                                                           }
                                                                                                       }
                                                                                                   }

                                                                                                   @Override
                                                                                                   public void onFinish() {

                                                                                                   }
                                                                                               });
                                                                                           }


                                                                                       }


                                                                                   }
                                                                               }

                                                             );
                                                         }
                                                     }

        );

    }

    /**
     * 场地数据加载更多
     */
    private void lodMoreCourt(final FindCourtFragmentAdapter competitionTeamAdapter, final int staus) {

        competitionTeamAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
                                                         @Override
                                                         public void onLoadMoreRequested() {
                                                             recyclerView.post(new Runnable() {
                                                                                   @Override
                                                                                   public void run() {
                                                                                       if (mCurrentCounter >= total_count) {
                                                                                           competitionTeamAdapter.notifyDataChangedAfterLoadMore(false);
                                                                                           View view = getActivity().getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) recyclerView.getParent(), false);
                                                                                           competitionTeamAdapter.addFooterView(view);
                                                                                       } else {


                                                                                           if (staus == 0) {
                                                                                               HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                                                                                               params.put("pageNumber", courtPageNumber);
                                                                                               params.put("pageSize", pageSize);
                                                                                               Api.getRetrofit().findCourt(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<CourtModel>>(getActivity()) {
                                                                                                   @Override
                                                                                                   public void onSuccess(HttpResponseFWF2<CourtModel> response) {
                                                                                                       if (response.isSuccess()) {
                                                                                                           recyclerView.setVisibility(View.VISIBLE);
                                                                                                           if (response.getData().content.size() > 0) {
                                                                                                               competitionTeamAdapter.notifyDataChangedAfterLoadMore(response.getData().content, true);

                                                                                                               mCurrentCounter = competitionTeamAdapter.getData().size();

                                                                                                               courtPageNumber++;
                                                                                                           }
                                                                                                       }
                                                                                                   }

                                                                                                   @Override
                                                                                                   public void onFinish() {

                                                                                                   }
                                                                                               });
                                                                                           } else {
                                                                                               HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                                                                                               params.put("pageNumber", courtPageNumber);
                                                                                               try {
                                                                                                   params.put("keyword", URLEncoder.encode(searchTeam.getText().toString().trim(), "utf-8"));
                                                                                               } catch (UnsupportedEncodingException e) {
                                                                                                   e.printStackTrace();
                                                                                               }
                                                                                               params.put("pageSize", pageSize);
                                                                                               Api.getRetrofit().findCourt(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<CourtModel>>(getActivity()) {
                                                                                                   @Override
                                                                                                   public void onSuccess(HttpResponseFWF2<CourtModel> response) {
                                                                                                       if (response.isSuccess()) {
                                                                                                           recyclerView.setVisibility(View.VISIBLE);
                                                                                                           if (response.getData().content.size() > 0) {
                                                                                                               competitionTeamAdapter.notifyDataChangedAfterLoadMore(response.getData().content, true);

                                                                                                               mCurrentCounter = competitionTeamAdapter.getData().size();

                                                                                                               courtPageNumber++;
                                                                                                           }
                                                                                                       }
                                                                                                   }

                                                                                                   @Override
                                                                                                   public void onFinish() {

                                                                                                   }
                                                                                               });
                                                                                           }


                                                                                       }


                                                                                   }
                                                                               }

                                                             );
                                                         }
                                                     }

        );

    }

    /**
     * cht 赛事列表 网络请求
     */

    public void getMatchList() {

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", matchPageNumber);
        params.put("pageSize", pageSize);
        Api.getRetrofit().SearchLeague(params).enqueue(new RequestCallback<HttpResponseFWF2<MatchDividerPage>>(getActivity()) {

            @Override
            public void onSuccess(HttpResponseFWF2<MatchDividerPage> response) {

                if (!response.isSuccess()) {
                    T.showShort(getActivity(), response.getMessage());
                } else {
                    Log.i("data", "赛事数据" + response.toString());
                    recyclerView.setVisibility(View.VISIBLE);
                    matchesList = response.getData().content;
                    total_count = response.getData().total;
                    total = matchesList.size();

                    if (total > 0) {
                        matchPageNumber++;
                    }

                    adapter = new FindRecyclerViewAdapter(getActivity(), R.layout.fragment_find_adapter_item, matchesList);
                    adapter.openLoadMore(matchesList.size(), true);
                    lodMore(adapter, 0);

                    recyclerView.setAdapter(adapter);
                    mCurrentCounter = adapter.getData().size();
                }
            }

            @Override
            public void onFinish() {

                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    /***
     * 场地
     */
    private void findCourt() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", courtPageNumber);
        params.put("pageSize", pageSize);
        Api.getRetrofit().findCourt(params).enqueue(new RequestCallback<HttpResponseFWF2<CourtModel>>(getActivity()) {

            @Override
            public void onSuccess(HttpResponseFWF2<CourtModel> response) {

                if (!response.isSuccess()) {
                    T.showShort(getActivity(), response.getMessage());
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    Log.i("data", "场地数据" + response.toString());
                    courtArrayList = response.getData().content;
                    total_count = response.getData().total;
                    total = matchesList.size();

                    if (total > 0) {
                        courtPageNumber++;
                    }

                    findCourtFragmnetAdapter = new FindCourtFragmentAdapter(getActivity(), R.layout.fragment_find_court_adapter_item, courtArrayList);
                    findCourtFragmnetAdapter.openLoadMore(courtArrayList.size(), true);
                    lodMoreCourt(findCourtFragmnetAdapter, 0);

                    recyclerView.setAdapter(findCourtFragmnetAdapter);
                    mCurrentCounter = findCourtFragmnetAdapter.getData().size();
                }
            }

            @Override
            public void onFinish() {

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    /**
     * 赛事adapter
     *
     * @param adapter
     */
    private void lodMore(final FindRecyclerViewAdapter adapter, final int status) {
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mCurrentCounter >= total_count) {
                            adapter.notifyDataChangedAfterLoadMore(false);
                            View view = getActivity().getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) recyclerView.getParent(), false);
                            adapter.addFooterView(view);
                        } else {


                            if (status == 0) {
                                HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                                params.put("pageNumber", matchPageNumber);

                                params.put("pageSize", pageSize);
                                Api.getRetrofit().SearchLeague(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<MatchDividerPage>>(getActivity()) {
                                    @Override
                                    public void onSuccess(HttpResponseFWF2<MatchDividerPage> response) {
                                        if (response.isSuccess()) {
                                            recyclerView.setVisibility(View.VISIBLE);
                                            if (response.getData().content.size() > 0) {
                                                adapter.notifyDataChangedAfterLoadMore(response.getData().content, true);

                                                mCurrentCounter = adapter.getData().size();

                                                matchPageNumber++;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFinish() {

                                    }
                                });
                            } else {
                                HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                                params.put("pageNumber", matchPageNumber);
                                try {
                                    params.put("keyword", URLEncoder.encode(searchTeam.getText().toString().trim(), "utf-8"));
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                params.put("pageSize", pageSize);
                                Api.getRetrofit().SearchLeague(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<MatchDividerPage>>(getActivity()) {
                                    @Override
                                    public void onSuccess(HttpResponseFWF2<MatchDividerPage> response) {
                                        if (response.isSuccess()) {
                                            recyclerView.setVisibility(View.VISIBLE);
                                            if (response.getData().content.size() > 0) {
                                                adapter.notifyDataChangedAfterLoadMore(response.getData().content, true);

                                                mCurrentCounter = adapter.getData().size();

                                                matchPageNumber++;
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFinish() {

                                    }
                                });
                            }


                        }


                    }
                });
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_find_match:
                flag = 0;

                refreshState();
                recyclerView.removeAllViews();
                labelMatch.setSelected(true);
                imageMatch.setVisibility(View.VISIBLE);
                matchPageNumber = 1;
                searchTeam.setText("");
                recyclerView.setVisibility(View.INVISIBLE);
                onRefresh();
                // recyclerViewFindFragment.setAdapter(adapter);

                break;
            case R.id.filter_football_find_team:

                flag = 1;
                refreshState();
                recyclerView.removeAllViews();
                labelTeam.setSelected(true);
                imageTeam.setVisibility(View.VISIBLE);

                searchTeam.setText("");
                recyclerView.setVisibility(View.INVISIBLE);
                onRefresh();

                break;
            case R.id.filter_football_find_court:
                flag = 2;
                refreshState();
                labelCourt.setSelected(true);
                imageCourt.setVisibility(View.VISIBLE);
                searchTeam.setText("");
//                FindCourtAdapter adapter1 = new FindCourtAdapter(getActivity());
//                recyclerView.setAdapter(adapter1);
                recyclerView.setVisibility(View.INVISIBLE);
                onRefresh();
                break;

            case R.id.mine:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                } else {
                    if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
                        App.getInst().toLogin();
                    } else {
                        startActivity(MineActivity.class);
                    }
                }
                break;

            case R.id.cancel_tv:

                searchTeam.setText("");
                onRefresh();
                break;
        }
    }

    private void refreshState() {
        labelMatch.setSelected(false);
        labelTeam.setSelected(false);
        labelCourt.setSelected(false);
        imageMatch.setVisibility(View.GONE);
        imageTeam.setVisibility(View.GONE);
        imageCourt.setVisibility(View.GONE);
    }

    /**
     * 上传deviceToken网络请求
     */
    public void upDeviceToken() {

        //deviceToken
        String deviceToken = JPushInterface.getRegistrationID(getContext());

        //设备id
        final TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "" + tm.getDeviceId();
        int deviceType = 0;

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", UserManager.getIns().getUser().id);
        params.put("deviceId", deviceId);
        params.put("deviceToken", deviceToken);
        params.put("deviceType", deviceType);
        Api.getRetrofit().upDeviceToken(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

            }

            @Override
            public void onFinish() {

            }
        });
    }


    /**
     * 数据刷新
     */
    @Override
    public void onRefresh() {


        if (flag == 0) {

            recyclerView.removeAllViews();
            matchPageNumber = 1;


            if (!TextUtils.isEmpty(searchTeam.getText().toString())) {

                cancel.setVisibility(View.VISIBLE);
                searchMatch();
            } else {
                cancel.setVisibility(View.GONE);
                getMatchList();
            }


        } else if (flag == 1) {
            recyclerView.removeAllViews();
            teamPageNumber = 1;


            if (!TextUtils.isEmpty(searchTeam.getText().toString())) {
                cancel.setVisibility(View.VISIBLE);
                searchTeam();
            } else {
                cancel.setVisibility(View.GONE);
                getTeamList();
            }

        } else if (flag == 2) {

            recyclerView.removeAllViews();
            courtPageNumber = 1;


            if (!TextUtils.isEmpty(searchTeam.getText().toString())) {
                cancel.setVisibility(View.VISIBLE);
                searchCourt();
            } else {
                cancel.setVisibility(View.GONE);
                findCourt();
            }
        }

    }

}