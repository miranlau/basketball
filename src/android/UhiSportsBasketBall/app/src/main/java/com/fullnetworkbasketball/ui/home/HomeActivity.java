package com.fullnetworkbasketball.ui.home;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.services.core.LatLonPoint;
import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.map.MapLocationProxy;
import com.fullnetworkbasketball.models.SystemNotificationEntity;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponse;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.base.BaseFragment;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.updataVersion.UpdateInfo;
import com.fullnetworkbasketball.updataVersion.UpdateInfoService;
import com.fullnetworkbasketball.utils.ContactUtil;
import com.fullnetworkbasketball.utils.PermissionsChecker;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.CustomTabContainer;
import com.hyphenate.EMContactListener;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chatuidemo.Constant;
import com.hyphenate.chatuidemo.DemoHelper;
import com.hyphenate.chatuidemo.ui.ChatActivity;
import com.hyphenate.chatuidemo.ui.GroupsActivity;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import cn.jpush.android.api.JPushInterface;

public class HomeActivity extends BaseActivity implements CustomTabContainer.OnTabChangeListener, MapLocationProxy.OnLocateListener, AMapLocationListener {
    @Bind(R.id.tabContainer)
    CustomTabContainer mTabContainer;

    private HomeFragment homeFragment;
    private FindFragment findFragment;
    private CircleFragment circleFragment;
    private MessageFragment messageFragment;

    BaseFragment mCurrentFragment;
    private int showTab;
    boolean isFristShow;
    private boolean mIsBackground;
    private ContactOpponentCaptainDialog dialog;
    private MapLocationProxy locationProxy;
    private LocationManagerProxy mLocationManagerProxy;
    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver broadcastReceiver;
    private HashMap<String, String> phoneMap = new HashMap<>();

    UpdateInfoService updateInfoService;
    UpdateInfo updateInfo;
    private ProgressDialog progressDialog;

    /**
     * 权限管理
     */
    private static final int REQUEST_CODE = 0; // 请求码

    // 所需的全部权限
    static final String[] PERMISSIONS = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
            Manifest.permission.CAMERA


    };
    private PermissionsChecker mPermissionsChecker; // 权限检测器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean exit = getIntent().getBooleanExtra("exit", false);
        super.onCreate(savedInstanceState);
        upDatePhoneNumber();
        getVersion();
        mPermissionsChecker = new PermissionsChecker(this);

//        getAuthor();
        if (savedInstanceState != null && savedInstanceState.getBoolean(Constant.ACCOUNT_REMOVED, false)) {
            // 防止被移除后，没点确定按钮然后按了home键，长期在后台又进app导致的crash
            // 三个fragment里加的判断同理
            DemoHelper.getInstance().logout(false, null);
            finish();
            startActivity(new Intent(this, LoginActivity.class));
            return;
        } else if (savedInstanceState != null && savedInstanceState.getBoolean("isConflict", false)) {
            // 防止被T后，没点确定按钮然后按了home键，长期在后台又进app导致的crash
            // 三个fragment里加的判断同理
            finish();
            startActivity(new Intent(this, LoginActivity.class));
            return;
        }


        if (exit) {
            UserManager.getIns().clearToken();
//            startActivity(LoginActivity.class);
            finish();
        }
        init();
        initIM();
        locationProxy = new MapLocationProxy(this);
        locationProxy.addLocateListener(this);
        locationProxy.activate();
    }

    private void initIM() {
        //注册local广播接收者，用于接收demohelper中发出的群组联系人的变动通知
        registerBroadcastReceiver();

        EMClient.getInstance().contactManager().setContactListener(new MyContactListener());
    }

    public class MyContactListener implements EMContactListener {
        @Override
        public void onContactAdded(String username) {
        }

        @Override
        public void onContactDeleted(final String username) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (ChatActivity.activityInstance != null && ChatActivity.activityInstance.toChatUsername != null &&
                            username.equals(ChatActivity.activityInstance.toChatUsername)) {
                        String st10 = getResources().getString(R.string.have_you_removed);
                        Toast.makeText(HomeActivity.this, ChatActivity.activityInstance.getToChatUsername() + st10, Toast.LENGTH_LONG)
                                .show();
                        ChatActivity.activityInstance.finish();
                    }
                }
            });
        }

        @Override
        public void onContactInvited(String username, String reason) {
        }

        @Override
        public void onContactAgreed(String username) {
        }

        @Override
        public void onContactRefused(String username) {
        }
    }

    private void unregisterBroadcastReceiver() {
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcastReceiver();
    }

    EMMessageListener messageListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            // 提示新消息


            for (EMMessage message : messages) {
                DemoHelper.getInstance().getNotifier().onNewMsg(message);
            }
            refreshUIWithMessage();
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
        }

        @Override
        public void onMessageReadAckReceived(List<EMMessage> messages) {
        }

        @Override
        public void onMessageDeliveryAckReceived(List<EMMessage> message) {
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
        }
    };

    private void refreshUIWithMessage() {
        runOnUiThread(new Runnable() {
            public void run() {
                // 刷新bottom bar消息未读数
                updateUnreadLabel();

                messageFragment.onResume();
//                if (currentTabIndex == 0) {
//                    // 当前页面如果为聊天历史页面，刷新此页面
//                    if (conversationListFragment != null) {
//                        conversationListFragment.refresh();
//                    }
//                }
            }
        });
    }

    private void registerBroadcastReceiver() {
        broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.ACTION_CONTACT_CHANAGED);
        intentFilter.addAction(Constant.ACTION_GROUP_CHANAGED);
        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                updateUnreadLabel();


//                updateUnreadAddressLable();
//                if (currentTabIndex == 0) {
//                    // 当前页面如果为聊天历史页面，刷新此页面
//                    if (conversationListFragment != null) {
//                        conversationListFragment.refresh();
//                    }
//                } else if (currentTabIndex == 1) {
//                    if(contactListFragment != null) {
//                        contactListFragment.refresh();
//                    }
//                }
                String action = intent.getAction();
                if (action.equals(Constant.ACTION_GROUP_CHANAGED)) {
                    if (EaseCommonUtils.getTopActivity(HomeActivity.this).equals(GroupsActivity.class.getName())) {
                        GroupsActivity.instance.onResume();
                    }
                }
            }
        };
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 刷新未读消息数
     */
    public void updateUnreadLabel() {
        int count = getUnreadMsgCountTotal();
        if (count > 0) {
//            unreadLabel.setText(String.valueOf(count));
//            unreadLabel.setVisibility(View.VISIBLE);
        } else {
//            unreadLabel.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 获取未读消息数
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        int unreadMsgCountTotal = 0;
        int chatroomUnreadMsgCount = 0;
        unreadMsgCountTotal = EMClient.getInstance().chatManager().getUnreadMsgsCount();
        for (EMConversation conversation : EMClient.getInstance().chatManager().getAllConversations().values()) {
            if (conversation.getType() == EMConversation.EMConversationType.ChatRoom)
                chatroomUnreadMsgCount = chatroomUnreadMsgCount + conversation.getUnreadMsgCount();
        }
        return unreadMsgCountTotal - chatroomUnreadMsgCount;
    }

    private void init() {
        mLocationManagerProxy = LocationManagerProxy.getInstance(this);
        //此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        //注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
        //在定位结束后，在合适的生命周期调用destroy()方法
        //其中如果间隔时间为-1，则定位只定一次
        mLocationManagerProxy.requestLocationData(
                LocationProviderProxy.AMapNetwork, 60 * 1000, 15, HomeActivity.this);
        mLocationManagerProxy.setGpsEnable(false);
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void initializeViews() {

    }


    @Override
    protected void initializeViews(Bundle savedInstanceState) {
        super.initializeViews(savedInstanceState);

        if (savedInstanceState != null) {
            showTab = savedInstanceState.getInt("showTab", 0);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.home_first_fragment);
        findFragment = (FindFragment) getSupportFragmentManager().findFragmentById(R.id.home_find_fragment);
        circleFragment = (CircleFragment) getSupportFragmentManager().findFragmentById(R.id.home_circle_fragment);
        messageFragment = (MessageFragment) getSupportFragmentManager().findFragmentById(R.id.home_message_fragment);
        transaction.hide(homeFragment)
                .hide(findFragment)
                .hide(circleFragment)
                .hide(messageFragment)
                .commitAllowingStateLoss();
        mCurrentFragment = homeFragment;

        mTabContainer.setOnTabChangeListener(this);
        mTabContainer.addTab(new CustomTabContainer.TabEntity(getString(R.string.home_tab_home_page), R.drawable.home_bottom_first_selector))
                .addTab(new CustomTabContainer.TabEntity(getString(R.string.home_tab_find), R.drawable.home_bottom_find_selector))
                .addTab(new CustomTabContainer.TabEntity(getString(R.string.home_tab_circle), R.drawable.home_bottom_circle_selector))
                .addTab(new CustomTabContainer.TabEntity(getString(R.string.home_tab_message), R.drawable.home_bottom_message_selector, true))
                .notifyDataChanged();
        mTabContainer.selectTab(showTab);


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("showTab", showTab);
    }

    @Override
    protected void initializeData() {
//        PushManager.getInstance().initialize(this.getApplicationContext());
//        convertData();
//        updateCarSize();
//        CheckUpdateUtil.checkNewVersion();
        isFristShow = true;
        if (isFristShow) {
            SharePreHelper.getIns().setAdTime(System.currentTimeMillis());
            fetchNotification();
            isFristShow = false;
        }
    }

//    private void convertData() {
//        User user = App.getInst().getUser();
//        if (user != null) {
//            HashMap<Integer, GoodsModel> goodsMap = UserManager.getIns().getGoodsMap();
//            if (goodsMap != null && goodsMap.size() > 0) {
//                ArrayList<GoodsModel> temps = new ArrayList<>();
//                GoodsModel goodsModel;
//                for (Map.Entry<Integer, GoodsModel> goodsModelEntry : goodsMap.entrySet()) {
//                    goodsModel = new GoodsModel();
//                    goodsModel.quantity = goodsModelEntry.getValue().quantity;
//                    goodsModel.grabId = goodsModelEntry.getValue().grabId;
//                    temps.add(goodsModel);
//                }
//                SpCarDbHelper.getInst().saveAllGoods(temps);
//                UserManager.getIns().getGoodsMap().clear();
//            }
//        }
//    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onTabChange(int position, CustomTabContainer.TabEntity tab) {
        showTab = position;
        switch (position) {
            case 0://首页
                switchContent(homeFragment);
                break;
            case 1://发现
                switchContent(findFragment);
                break;
            case 2://圈子
                switchContent(circleFragment);
                break;
            case 3://消息
                switchContent(messageFragment);
                break;

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        boolean exit = intent.getBooleanExtra("exit", false);
        boolean finish = intent.getBooleanExtra("exit_finish", false);
        if (finish) {
            finish();
            return;
        }
        if (exit) {
            UserManager.getIns().clearToken();
//            startActivity(LoginActivity.class);
            finish();
        } else {
            int show_position = getIntent().getIntExtra("show_position", -1);
            if (show_position != -1)
                mTabContainer.selectTab(show_position);
            boolean refreshUser = getIntent().getBooleanExtra("refreshUserInfo", false);
            if (refreshUser) {
//                UserManager.getIns().refreshUserInfo();
            }
//            convertData();
            updateCarSize();
//            if (mCurrentFragment != null) {
//                mCurrentFragment.onHiddenChanged(false);
//            }
//            if (mCurrentFragment != null && mCurrentFragment instanceof GrabFragment) {
//                ((GrabFragment) mCurrentFragment).initializeData();
//            }
        }
        if (getIntent().getIntExtra("isNewUser", 0) == 1) {
            if (dialog == null) {
                dialog = new ContactOpponentCaptainDialog(HomeActivity.this, R.style.callDialog, 1);
            }
            dialog.show();
        }
    }

    public void switchPage(int index) {
        mTabContainer.selectTab(index);
    }

    public void switchContent(BaseFragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction t = fm.beginTransaction();
        if (mCurrentFragment != null) {
            t.hide(mCurrentFragment);
        }
        t.show(fragment);
        t.commitAllowingStateLoss();
        mCurrentFragment = fragment;
    }

    private void fetchNotification() {
        if (App.getInst().getUser() == null) return;
        HashMap<String, Object> params = HttpParamsHelper.createParams();
        params.put("offset", 1);
        params.put("rows", 100);
        params.put("deviceType", "Android");
        Api.getRetrofit().getHomeNotitication(params).enqueue(new RequestCallback<HttpResponse<SystemNotificationEntity.SystemNotification>>() {
            @Override
            public void onSuccess(HttpResponse<SystemNotificationEntity.SystemNotification> response) {
                if (response != null && response.code != 91001) {
                    if (response.code == 0) {
//                        SystemNotificationEntity.SystemNotification data = response.getDataFrist();
//                        if (data != null && TextUtil.isValidate(data.list)) {
//                            new NotificationDialog(HomeActivity.this).setData(data.list).show();
//                        }
                    }
                }
            }

            @Override
            public void onFinish() {
                SharePreHelper.getIns().setAdTime(System.currentTimeMillis());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


        // 缺少权限时, 进入权限配置页面
        if (mPermissionsChecker.lacksPermissions(PERMISSIONS)) {
            startPermissionsActivity();
        }


        JPushInterface.onResume(this);
        if (mCurrentFragment instanceof CircleFragment || mCurrentFragment instanceof MessageFragment) {
            mCurrentFragment.onResume();
        }

        // unregister this event listener when this activity enters the
        // background
        DemoHelper sdkHelper = DemoHelper.getInstance();
        sdkHelper.pushActivity(this);

        EMClient.getInstance().chatManager().addMessageListener(messageListener);
    }


    @Override
    protected void onStop() {
        EMClient.getInstance().chatManager().removeMessageListener(messageListener);
        DemoHelper sdkHelper = DemoHelper.getInstance();
        sdkHelper.popActivity(this);

        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(this);
        if (mCurrentFragment != null) {
            mCurrentFragment.setUserVisibleHint(true);
            if (mCurrentFragment instanceof CircleFragment || mCurrentFragment instanceof MessageFragment) {
                mCurrentFragment.onPause();
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        convertData();
        updateCarSize();
//        if (mCurrentFragment != null) {
//            if (mCurrentFragment instanceof NewMessageFragment || mCurrentFragment instanceof ShowOrderFragment) {
//                mCurrentFragment.onResume();
//            }
//            if (mCurrentFragment instanceof ProfileFragment) {
//                ((ProfileFragment) mCurrentFragment).updateUser();
//            }
//        }
        if (SharePreHelper.getIns().shouldShowNotification()) {
            SharePreHelper.getIns().setShouldShowNotification(false);
            if (!isFristShow) {
                long adTime = SharePreHelper.getIns().getAdTime();
                if (System.currentTimeMillis() - adTime >= SharePreHelper.getIns().getAdInterval()) {
                    fetchNotification();
                }
            }
        }
    }

    public void updateCarSize() {
        if (App.getInst().getUser() != null) {
//            ArrayList<GoodsModel> temps = SpCarDbHelper.getInst().getAllGoods();
//            if (temps != null) {
//                mTabContainer.setTabNewMsgCount(3, temps.size());
//            } else {
//                mTabContainer.setTabNewMsgCount(3, 0);
//            }
//        } else {
//            mTabContainer.setTabNewMsgCount(3, UserManager.getIns().getGoodsMap().size());
//        }
        }
    }


//    private long firstTime = 0;
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        switch (keyCode) {
//            case KeyEvent.KEYCODE_BACK:
//                long secondTime = System.currentTimeMillis();
//                if (secondTime - firstTime > 2000) {
//                    Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
//                    firstTime = secondTime;
//                    return true;
//                } else {
//                    finish();
//                }
//                break;
//        }
//        return false;
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            moveTaskToBack(false);
//            return true;
//        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onLocationChanged(AMapLocation location) {
        locationProxy.deactivate();
        App.getInst().setCurrentAddress(new LatLonPoint(location.getLatitude(), location.getLongitude()), location.getCity());
    }


    /**
     * 版本更新网络请求
     */

    public void getVersion() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("type", 0);

        Api.getRetrofit().getVersion(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<UpdateInfo>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<UpdateInfo> response) {

                updateInfo = response.getData();
                updateInfoService = new UpdateInfoService(HomeActivity.this, updateInfo);

                int mincode = updateInfo.minCode;

                updateVersion(mincode);
            }

            @Override
            public void onFinish() {

            }
        });

    }


    /**
     * 更新版本
     */
    public void updateVersion(int mincode) {

        PackageInfo packageInfo = null;
        try {
            PackageManager packageManager = getApplication().getPackageManager();
            packageInfo = packageManager.getPackageInfo(getApplication().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //比较版本号
        int now_version = packageInfo.versionCode;

        boolean allowUpdate;

        //设置是不是已经登录，在没有登录的情况下默认开启自动更新
        if (UserManager.getIns().getUser() == null) {
            allowUpdate = true;
        } else {
            allowUpdate = UserManager.getIns().getUser().autoupdate;
        }
        //如果最低版本大于最新版本则强制更新 并且不需要管开关按钮是否打开
        if (mincode > now_version) {
            allowUpdate = true;
        }

        if (allowUpdate) {

            //判断是不是需要版本更新

            if (updateInfoService.isNeedUpdate()) {

                //如果最低版本大于最新版本则强制更新
                if (mincode > now_version) {
                    if (Environment.getExternalStorageState().equals(
                            Environment.MEDIA_MOUNTED)) {
                        downFile(updateInfo.downloadURL, 0);
                    } else {
                        Toast.makeText(HomeActivity.this, "SD卡不可用，请插入SD卡",
                                Toast.LENGTH_SHORT).show();
                    }

                } else {
//
//                    if (SharePreHelper.getIns().getIsCancelUpdata()) {
//
//                    }
                    showUpdateDialog();
                }


            }


        }
    }


    //显示是否要更新的对话框
    private void showUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("   有新的版本啦,请升级");
        // builder.setMessage(updateInfo.getName());
        builder.setCancelable(false);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Environment.getExternalStorageState().equals(
                        Environment.MEDIA_MOUNTED)) {
                    downFile(updateInfo.downloadURL, 1);
                } else {
                    Toast.makeText(HomeActivity.this, "SD卡不可用，请插入SD卡",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //如果点了取消按钮  则默认不更新只会提示一次 并且只是在有新版本时才会进行提示
                SharePreHelper.getIns().setIsCancelUpdata(true);

            }
        });
        builder.create().show();
    }

    //下载
    public void downFile(String url, int type) {
        progressDialog = new ProgressDialog(HomeActivity.this);    //进度条，在下载的时候实时更新进度，提高用户友好度
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        if (type == 0) {
            progressDialog.setTitle("您的版本已不适用，正在更新");
        } else {
            progressDialog.setTitle("正在下载");
        }
        progressDialog.setMessage("请稍候...");
        progressDialog.setProgress(0);
        progressDialog.show();
        updateInfoService.downLoadFile(url, progressDialog);
    }

    /**
     * 上传手机通讯录
     */


    public void upDatePhoneNumber() {

        boolean isUpDataPhone = SharePreHelper.getIns().getIsUpDataPhone();
        if (isUpDataPhone && UserManager.getIns().getUser() != null) {
            // 读取通讯录
            HashMap<String, String> map = ContactUtil.getPhoneContacts(HomeActivity.this);
            phoneMap.putAll(map);

            // 没有联系人
            if (phoneMap == null || phoneMap.size() <= 0) {
                T.showShort(HomeActivity.this, "未获取联系人");

                return;
            }
            // 遍历获取手机号
            ArrayList<String> phoneList = new ArrayList<>();
            Iterator iter = phoneMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                String key = (String) entry.getKey();
                phoneList.add(key);
            }

            Api.getRetrofit().updatePhone(phoneList).enqueue(new RequestCallback<HttpResponseFWF2>() {
                @Override
                public void onSuccess(HttpResponseFWF2 response) {

                }

                @Override
                public void onFinish() {

                }
            });

        }

    }

    /**
     * 应用权限管理
     */

    private void startPermissionsActivity() {
        PermissionsActivity.startActivityForResult(this, REQUEST_CODE, PERMISSIONS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 拒绝时, 关闭页面, 缺少主要权限, 无法运行
        if (requestCode == REQUEST_CODE && resultCode == PermissionsActivity.PERMISSIONS_DENIED) {
            finish();
        }
    }
}
