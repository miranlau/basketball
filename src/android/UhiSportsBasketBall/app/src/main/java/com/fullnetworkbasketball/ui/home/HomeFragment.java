package com.fullnetworkbasketball.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.AllMatch;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.AllMatchAdapter;
import com.fullnetworkbasketball.ui.adapter.HomePageAdapterRight;
import com.fullnetworkbasketball.ui.base.BaseFragment;
import com.fullnetworkbasketball.ui.dialog.BottomPupDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.user.MineActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;

/**
 * Created by Administrator on 2016/4/15 0015.
 */
public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.label_all)
    TextView labelAll;
    @Bind(R.id.image_all)
    ImageView imageAll;
    @Bind(R.id.filter_football_home_all)
    RelativeLayout filterFootballHomeAll;
    @Bind(R.id.label_mine)
    TextView labelMine;
    @Bind(R.id.image_mine)
    ImageView imageMine;
    @Bind(R.id.filter_football_home_mine)
    RelativeLayout filterFootballHomeMine;
    @Bind(R.id.label_near)
    TextView labelNear;
    @Bind(R.id.image_near)
    ImageView imageNear;
    @Bind(R.id.filter_football_home_near)
    RelativeLayout filterFootballHomeNear;
    @Bind(R.id.rv_home_fragment)
    RecyclerView rvHomeFragment;

    @Bind(R.id.home_page_add)
    ImageView homePageAdd;
    @Bind(R.id.mine)
    ImageView mine;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private AllMatchAdapter mAdapter;
    private BottomPupDialog bottomPupDialog;
    private HomePageAdapterRight homePageAdapterRight;
    private AllMatch allMatch;
    private int delayMillis = 1000;
    private int mCurrentCounter = 0;
    private static final int TOTAL_COUNTER = 18;
    private int pageNum = 1;
    private int total;
    private ArrayList<Match> list;
    private int flag = 0;
    private int total_count;
    private ContactOpponentCaptainDialog dialog;

    @Override
    protected int getRootViewLayoutId() {
        return R.layout.fragment_home_layout;
    }

    @Override
    protected void initializeViews() {
        filterFootballHomeAll.setOnClickListener(this);
        filterFootballHomeMine.setOnClickListener(this);
        filterFootballHomeNear.setOnClickListener(this);

        swipeLayout.setOnRefreshListener(this);
        homePageAdd.setOnClickListener(this);
        mine.setOnClickListener(this);
        refreshSate();
        labelAll.setSelected(true);
        imageAll.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvHomeFragment.setLayoutManager(layoutManager);
        onRefresh();
        initAdapter();
    }

    private void initAdapter() {

    }

    @Override
    protected void initializeData() {


    }

    /**
     * 我的比赛
     */
    private void getMyMatch() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", pageNum);
        Api.getRetrofit().queryMyMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllMatch>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<AllMatch> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    rvHomeFragment.setVisibility(View.VISIBLE);
                    list = response.getData().content;
                    addTypeToData(list);
                    total = list.size();
                    if (total > 0) {
                        pageNum++;
                    }
                    mAdapter = new AllMatchAdapter(getActivity(), list);

                    mAdapter.openLoadMore(true);
                    mAdapter.openLoadAnimation();
                    lodMore(mAdapter);
                    rvHomeFragment.setAdapter(mAdapter);
                    mCurrentCounter = mAdapter.getItemCount();
                    allMatch = response.getData();
                } else {
                    T.showShort(getActivity(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeLayout.setRefreshing(false);
            }
        });
    }

    private void getNearMatch() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", pageNum);
        params.put("longitude", App.getInst().getCurrentAddress().latLng.getLongitude());
        params.put("latitude", App.getInst().getCurrentAddress().latLng.getLatitude());
        Api.getRetrofit().queryNearMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllMatch>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<AllMatch> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    rvHomeFragment.setVisibility(View.VISIBLE);
                    list = response.getData().content;
                    addTypeToData(list);
                    total = list.size();
                    if (total > 0) {
                        pageNum++;
                    }
                    mAdapter = new AllMatchAdapter(getActivity(), list);

                    mAdapter.openLoadMore(true);
                    mAdapter.openLoadAnimation();
                    lodMore(mAdapter);
                    rvHomeFragment.setAdapter(mAdapter);
                    mCurrentCounter = mAdapter.getItemCount();
                    allMatch = response.getData();
                } else {
                    T.showShort(getActivity(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeLayout.setRefreshing(false);
            }
        });
    }

    private void addTypeToData(ArrayList<Match> list) {
        for (Match match : list) {
            if (match.playerType == 0) {
                if (match.status == 10) {
                    match.setItemType(Match.SINGLE);
                } else if (match.status == 20 || match.status == 100||match.status==21) {
                    match.setItemType(Match.SINGLE_ING);
                }
            } else if (match.playerType == 1 || match.playerType == 2) {
                if (match.status == 0) {
                    match.setItemType(Match.MATCH_BEFORE);
                } else if (match.status == 10) {
                    match.setItemType(Match.MATCH_BEFORE_TWO);
                } else {
                    match.setItemType(Match.MATCH_ING);
                }

            }


//         ArrayList<Match> section = new ArrayList<>();
//            for (int i=0;i<list.size();i++){
//                HashMap<String,Object> map=new HashMap<String,Object>();
//                if (match.date.equals(list.get(i).date)){
//                    section.add(match);
//                }
//                map.put(match.date,section);
//            }
//        }
        }
    }

    /**
     * 所有比赛
     */
    private void getAllMatch() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("pageNumber", pageNum);
        Api.getRetrofit().queryAllMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllMatch>>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2<AllMatch> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    rvHomeFragment.setVisibility(View.VISIBLE);
                    list = response.getData().content;
                    if (list == null || list.size() == 0) {
                        return;
                    }
                    addTypeToData(list);
                    total_count = response.getData().total;
                    total = list.size();
                    if (total > 0) {
                        pageNum++;
                    }

                    mAdapter = new AllMatchAdapter(getActivity(), list);
                    mAdapter.openLoadMore(list.size(), true);
                    mAdapter.openLoadAnimation();
                    lodMore(mAdapter);
                    rvHomeFragment.setAdapter(mAdapter);
                    mCurrentCounter = mAdapter.getData().size();
                    allMatch = response.getData();
                } else {
                    T.showShort(getActivity(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeLayout.setRefreshing(false);
            }
        });
    }

    private void lodMore(final AllMatchAdapter mAdapter) {
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                rvHomeFragment.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mCurrentCounter >= total_count) {
                            mAdapter.notifyDataChangedAfterLoadMore(false);
                            View view = getActivity().getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) rvHomeFragment.getParent(), false);
                            mAdapter.addFooterView(view);
                        } else {
                            HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                            params.put("pageNumber", pageNum);
                            Api.getRetrofit().queryAllMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllMatch>>() {
                                @Override
                                public void onSuccess(HttpResponseFWF2<AllMatch> response) {
                                    if (response.isSuccess()) {


                                        if (response.getData().content.size() > 0) {
                                            addTypeToData(response.getData().content);
                                            mAdapter.notifyDataChangedAfterLoadMore(response.getData().content, true);
                                            mCurrentCounter = mAdapter.getData().size();

                                            pageNum++;

                                        }

                                    }
                                }

                                @Override
                                public void onFinish() {

                                }
                            });
                        }

                    }


                });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserManager.getIns().getUser() == null) {
            mine.setImageResource(R.mipmap.default_person);
             if (SharePreHelper.getIns().getNoLoginMyMatch()){
                 flag=0;
                 pageNum=1;
                 getAllMatch();
             }



        } else {

            if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
            } else {
                UserManager.getIns().autoLogin();

                upDeviceToken();

                if (SharePreHelper.getIns().getModifuHeader()) {
                    ImageLoader.loadCicleImage(this, SharePreHelper.getIns().getHeaderUrl(), R.mipmap.default_person,mine);
                } else {
                    ImageLoader.loadCicleImage(this, UserManager.getIns().getUser().avatar,R.mipmap.default_person, mine);
                }

                if (SharePreHelper.getIns().getIsCreateMatch()) {
                    onRefresh();
                    SharePreHelper.getIns().putIsCreateMatch(false);
                }

            }
        }
    }


    /**
     * 上传deviceToken网络请求
     */
    public void upDeviceToken() {

        //deviceToken
        String deviceToken = JPushInterface.getRegistrationID(getContext());

        //设备id
        final TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "" + tm.getDeviceId();
        int deviceType = 0;

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", UserManager.getIns().getUser().id);
        params.put("deviceId", deviceId);
        params.put("deviceToken", deviceToken);
        params.put("deviceType", deviceType);
        Api.getRetrofit().upDeviceToken(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>(getActivity()) {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_home_all:
                refreshSate();
                labelAll.setSelected(true);
                imageAll.setVisibility(View.VISIBLE);
                rvHomeFragment.setVisibility(View.INVISIBLE);
                flag = 0;
                onRefresh();
                break;
            case R.id.filter_football_home_mine:

                if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {

//                    if (dialog == null) {
//                        dialog = new ContactOpponentCaptainDialog(getContext(), R.style.callDialog, 8);
//                    }
//                    dialog.show();

                    App.getInst().toLogin();
                    SharePreHelper.getIns().setNoLoginMYMatch(true);
//                    Intent intent = new Intent(getActivity(), LoginActivity.class);
//                    getActivity().startActivity(intent);
                    rvHomeFragment.setVisibility(View.INVISIBLE);
                    return;

                } else {
                    refreshSate();
                    labelMine.setSelected(true);
                    imageMine.setVisibility(View.VISIBLE);
                    flag = 1;
                }

                rvHomeFragment.setVisibility(View.INVISIBLE);
                onRefresh();
                break;
            case R.id.filter_football_home_near:
                refreshSate();
                labelNear.setSelected(true);
                imageNear.setVisibility(View.VISIBLE);
                flag = 2;
                rvHomeFragment.setVisibility(View.INVISIBLE);
                onRefresh();
                break;
            case R.id.home_page_add:
                if (bottomPupDialog == null) {
                    bottomPupDialog = new BottomPupDialog(getActivity(), R.style.callDialog, 0);
                    bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
                    bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                }
                bottomPupDialog.show();

                WindowManager windowManager1 = getActivity().getWindowManager();
                Display display1 = windowManager1.getDefaultDisplay();
                WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
                lp1.width = (int) (display1.getWidth()); //设置宽度
                bottomPupDialog.getWindow().setAttributes(lp1);
                break;
            case R.id.mine:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                } else {
                    if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
                        App.getInst().toLogin();
                    } else {
                        startActivity(MineActivity.class);
                    }
                }
                break;
        }
    }

    private void refreshSate() {
        labelAll.setSelected(false);
        labelMine.setSelected(false);
        labelNear.setSelected(false);
        imageAll.setVisibility(View.GONE);
        imageMine.setVisibility(View.GONE);
        imageNear.setVisibility(View.GONE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRefresh() {
        pageNum = 1;

        if (flag == 0) {
            getAllMatch();
        } else if (flag == 1) {
            getMyMatch();
        } else if (flag == 2) {
            getNearMatch();
        }

    }


}
