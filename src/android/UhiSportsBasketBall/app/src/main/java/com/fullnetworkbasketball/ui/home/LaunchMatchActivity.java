package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePopupWindow;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponse;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupChooseTeamDialog;
import com.fullnetworkbasketball.ui.dialog.ChooseMatchAssoaciationDialog;
import com.fullnetworkbasketball.ui.match.AddAddressActivity;
import com.fullnetworkbasketball.utils.DateTimePickDialogUtil2;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/20 0020.
 */
public class LaunchMatchActivity extends BaseActivity {
    @Bind(R.id.label_left)
    TextView labelLeft;
    @Bind(R.id.image_left)
    ImageView imageLeft;
    @Bind(R.id.filter_football_launch_match_left)
    RelativeLayout filterFootballLaunchMatchLeft;
    @Bind(R.id.label_right)
    TextView labelRight;
    @Bind(R.id.image_right)
    ImageView imageRight;
    @Bind(R.id.filter_football_launch_match_right)
    RelativeLayout filterFootballLaunchMatchRight;
    @Bind(R.id.ll_launch_match)
    LinearLayout llLaunchMatch;
    @Bind(R.id.tv_launch_match)
    TextView tvLaunchMatch;
    TextView launchMatchRecord;
    TextView launchMatchLevel;
    TextView launchMatchAge;
    TextView launchMatchName;
    RatingBar launchMatchEvaluation;
    ImageView launchMatchHeader;
    private LinearLayout ll_change_team;
    private BottomPupChooseTeamDialog bottomPupDialog;
    private LinearLayout ll_opponent_battle;
    private LinearLayout ll_avg_age_choice;
    private int position = 1;
    private ArrayList<Team> myTeams;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;
    private TimePopupWindow pwTime;
    private LinearLayout launch_match_time;
    private TextView time;
    private String initEndDateTime = ""; // 初始化时间
    private LinearLayout ll_launch_match_system;
    private ChooseMatchAssoaciationDialog matchAssoaciationDialog;
    private int system;
    private TextView tv_launch_match_system;
    private LinearLayout ll_launch_match_address;
    private TextView tv_launch_match_address;
    private TextView tv_launch_match_battle;
    private String combat = "";
    private String chooseAge = "";
    private TextView tv_launch_match_age;
    private EditText launch_match_remark;
    private Team team1;
    private double Latitude;
    private double Longitude;

    //选择人制度集合
    private String[] array;
    private String low="";
    private String high = "";
    private String young="";
    private String old="";

    @Override
    protected void setContentView() {
        setContentView(R.layout.launch_match_activity);
        myTeams = (ArrayList<Team>) getIntent().getExtras().get("teams");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
        initEndDateTime = sdf.format(System.currentTimeMillis());
        team1 = myTeams.get(0);
    }

    @Override
    protected void initializeViews() {
        filterFootballLaunchMatchLeft.setOnClickListener(this);
        filterFootballLaunchMatchRight.setOnClickListener(this);
        refreshSate();
        labelLeft.setSelected(true);
        imageLeft.setVisibility(View.VISIBLE);

        array = this.getResources().getStringArray(R.array.select_system_array);
        changeVIew(team1);
    }
    // TODO
    private void changeVIew(Team team) {
        View view = LayoutInflater.from(this).inflate(R.layout.launch_match_left_layout, null);

        llLaunchMatch.addView(view);
        ll_change_team = (LinearLayout) view.findViewById(R.id.ll_change_team);
        ll_change_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popDialog();
            }
        });
        ll_opponent_battle = (LinearLayout) view.findViewById(R.id.ll_opponent_battle);
        tv_launch_match_battle = (TextView) view.findViewById(R.id.tv_launch_match_battle);
        ll_opponent_battle.setOnClickListener(this);

        ll_avg_age_choice = (LinearLayout) view.findViewById(R.id.ll_avg_age_choice);
        tv_launch_match_age = (TextView) view.findViewById(R.id.tv_launch_match_age);
        ll_avg_age_choice.setOnClickListener(this);

        tvLaunchMatch.setOnClickListener(this);
        launchMatchRecord = (TextView) view.findViewById(R.id.launch_match_record);
        launchMatchLevel = (TextView) view.findViewById(R.id.launch_match_level);
        launchMatchAge = (TextView) view.findViewById(R.id.launch_match_age);
        launchMatchName = (TextView) view.findViewById(R.id.launch_match_name);
        launchMatchEvaluation = (RatingBar) view.findViewById(R.id.launch_match_evaluation);
        launchMatchHeader = (ImageView) view.findViewById(R.id.launch_match_header);
        launch_match_time = (LinearLayout) view.findViewById(R.id.launch_match_time);
        tv_launch_match_system = (TextView) view.findViewById(R.id.tv_launch_match_system);
        launch_match_time.setOnClickListener(this);

        ll_launch_match_system = (LinearLayout) view.findViewById(R.id.ll_launch_match_system);
        ll_launch_match_system.setOnClickListener(this);
        tv_launch_match_address = (TextView) view.findViewById(R.id.tv_launch_match_address);

        ll_launch_match_address = (LinearLayout) view.findViewById(R.id.ll_launch_match_address);
        ll_launch_match_address.setOnClickListener(this);

        time = (TextView) view.findViewById(R.id.time);
        launch_match_remark = (EditText) view.findViewById(R.id.launch_match_remark);

        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
        ImageLoader.loadCicleImage(LaunchMatchActivity.this, team.logo, R.mipmap.default_team, launchMatchHeader);
        launchMatchName.setText(team.name);
        launchMatchEvaluation.setRating((float) team.stars);
        launchMatchRecord.setText(record + team.win + win + team.deuce + decue + team.lose + lose);
        launchMatchLevel.setText(level + team.level);
        launchMatchAge.setText(avgAge + team.age);
    }


    private void changeRight(Team team) {
        View view = LayoutInflater.from(this).inflate(R.layout.launch_match_right_layout, null);

        llLaunchMatch.addView(view);
        ll_change_team = (LinearLayout) view.findViewById(R.id.ll_launch_single_match_change_team);
        ll_change_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popDialog();
            }
        });
        launchMatchRecord = (TextView) view.findViewById(R.id.launch_single_match_record);
        launchMatchLevel = (TextView) view.findViewById(R.id.launch_single_match_level);
        launchMatchAge = (TextView) view.findViewById(R.id.launch_single_match_age);
        launchMatchName = (TextView) view.findViewById(R.id.launch_single_match_name);

        launchMatchEvaluation = (RatingBar) view.findViewById(R.id.launch_single_match_evaluation);
        launchMatchHeader = (ImageView) view.findViewById(R.id.launch_single_match_header);
        launch_match_time = (LinearLayout) view.findViewById(R.id.ll_launch_single_match_time);
        tv_launch_match_system = (TextView) view.findViewById(R.id.tv_launch_single_match_system);
        time = (TextView) view.findViewById(R.id.tv_launch_single_match_time);
        launch_match_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseTime();
            }
        });
        ll_launch_match_system = (LinearLayout) view.findViewById(R.id.ll_launch_single_match_system);
        ll_launch_match_system.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popChooseSysttem();
            }
        });

        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
        ImageLoader.loadCicleImage(LaunchMatchActivity.this, team.logo, R.mipmap.default_team, launchMatchHeader);
        launchMatchName.setText(team.name);
        launchMatchEvaluation.setRating((float) team.stars);
        launchMatchRecord.setText(record + team.win + win + team.deuce + decue + team.lose + lose);
        launchMatchLevel.setText(level + team.level);
        launchMatchAge.setText(avgAge + team.age);
    }

    private void popDialog() {
        if (bottomPupDialog == null) {
            bottomPupDialog = new BottomPupChooseTeamDialog(LaunchMatchActivity.this, R.style.callDialog, 1, myTeams);
            bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
            bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            bottomPupDialog.setConfirmClickListener(new BottomPupChooseTeamDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(Team team) {
                    team1 = team;
                    llLaunchMatch.removeAllViews();
                    refreshSate();
                    if (position == 1) {
                        labelLeft.setSelected(true);
                        imageLeft.setVisibility(View.VISIBLE);
                        changeVIew(team1);
                    } else if (position == 2) {
                        labelRight.setSelected(true);
                        imageRight.setVisibility(View.VISIBLE);
                        changeRight(team1);
                    }

                }
            });
        }
        bottomPupDialog.show();

        WindowManager windowManager1 = LaunchMatchActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        bottomPupDialog.getWindow().setAttributes(lp1);
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent data) {
        super.onActivityResult(arg0, arg1, data);
        if (arg1 == RESULT_OK) {
            switch (arg0) {
                case 10001:
                    combat = data.getStringExtra("lowest") + "-" + data.getStringExtra("highest");
                    low = data.getStringExtra("lowest");
                    high = data.getStringExtra("highest");
                    tv_launch_match_battle.setText(combat);
                    break;
                case 10002:
                    chooseAge = data.getStringExtra("youngest") + "-" + data.getStringExtra("oldest");
                    young = data.getStringExtra("youngest");
                    old = data.getStringExtra("oldest");
                    tv_launch_match_age.setText(chooseAge);
                    break;
                case 10003:
                    tv_launch_match_address.setText(data.getStringExtra("address"));
                    Longitude = data.getDoubleExtra("Longitude",0);
                    Latitude = data.getDoubleExtra("Latitude",0);

                    break;
            }
        }
    }

    @Override
    protected void initializeData() {

    }

    private void launchMatch() {
        if (time.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_date));
            return;
        }
        if (tv_launch_match_system.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_system));
            return;
        }
//        if (tv_launch_match_address.getText().toString().trim().isEmpty()) {
//            T.showShort(this, getResources().getString(R.string.input_address));
//            return;
//        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", team1.id);

        params.put("date",time.getText().toString()+":00");
        params.put("power",combat);
        params.put("age",chooseAge);
        params.put("desc",launch_match_remark.getText().toString().trim());
        params.put("longitude",Longitude);//30.5547811989,104.0647765911
        params.put("latitude",Latitude);
        params.put("type",system);
        params.put("addr",tv_launch_match_address.getText().toString().trim());//tv_launch_match_address.getText().toString().trim()

        Api.getRetrofit().createMatch(params).enqueue(new RequestCallbackFWF<HttpResponse>(LaunchMatchActivity.this) {
            @Override
            public void onSuccess(HttpResponse response) {
                Logger.i("msg:" + response.toString()+"longitude"+"---->"+Longitude+"---"+"Latitude"+"------>"+Latitude);
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    SharePreHelper.getIns().putIsCreateMatch(true);
                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });

    }

    private void launchSingleMatch() {
        if (time.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_date));
            return;
        }
        if (tv_launch_match_system.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_system));
            return;
        }

//        if (tv_launch_match_address.getText().toString().trim().isEmpty()){
//            T.showShort(this,getResources().getString(R.string.input_address));
//            return;
//        }

//        if (tv_launch_match_address.getText().toString().trim().isEmpty()) {
//            T.showShort(this, getResources().getString(R.string.input_address));
//            return;
//        }

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", team1.id);
        params.put("date", time.getText().toString()+":00");
        params.put("type", system);
        Api.getRetrofit().creatSingleMatch(params).enqueue(new RequestCallbackFWF<HttpResponse>(LaunchMatchActivity.this) {
            @Override
            public void onSuccess(HttpResponse response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    SharePreHelper.getIns().putIsCreateMatch(true);
                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_launch_match_left:
                refreshSate();
                labelLeft.setSelected(true);
                imageLeft.setVisibility(View.VISIBLE);
                llLaunchMatch.removeAllViews();
                changeVIew(team1);
                position = 1;
                break;
            case R.id.filter_football_launch_match_right:
                refreshSate();
                labelRight.setSelected(true);
                imageRight.setVisibility(View.VISIBLE);
                llLaunchMatch.removeAllViews();
                changeRight(team1);
                position = 2;
                break;
            case R.id.tv_launch_match:

                if (position == 2) {
                    launchSingleMatch();
                } else if (position == 1) {
                    launchMatch();
                }

                break;
            case R.id.launch_match_time:
                chooseTime();
                break;
            case R.id.ll_launch_match_system:
                popChooseSysttem();
                break;
            case R.id.ll_launch_match_address: //

                Intent intent_address = new Intent(LaunchMatchActivity.this,AddAddressActivity.class);
                startActivityForResult(intent_address,10003);
                break;
            case R.id.ll_opponent_battle:
                Intent intent = new Intent(LaunchMatchActivity.this, CombatChoiceActivity.class);
                intent.putExtra("lowest",low);
                intent.putExtra("highest",high);
                startActivityForResult(intent, 10001);
                break;
            case R.id.ll_avg_age_choice:
                Intent intent_age = new Intent(LaunchMatchActivity.this, AvgAgeChoiceActivity.class);
                intent_age.putExtra("youngest",young);
                intent_age.putExtra("oldest",old);
                startActivityForResult(intent_age, 10002);
                break;
        }
    }

    private void popChooseSysttem() {
        if (matchAssoaciationDialog == null) {
            matchAssoaciationDialog = new ChooseMatchAssoaciationDialog(LaunchMatchActivity.this, R.style.callDialog);
            matchAssoaciationDialog.getWindow().setGravity(Gravity.BOTTOM);
            matchAssoaciationDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            matchAssoaciationDialog.setConfirmClickListener(new ChooseMatchAssoaciationDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(int i) {
                    system = i+2;

                    tv_launch_match_system.setText(array[i]);

//                    if (system == 3) {
//                        tv_launch_match_system.setText(getResources().getString(R.string.five_person_system));
//                    } else if (system == 4) {
//                        tv_launch_match_system.setText(getResources().getString(R.string.seven_person_system));
//                    } else if (system == 5) {
//                        tv_launch_match_system.setText(getResources().getString(R.string.nine_person_system));
//                    } else if (system == 6) {
//                        tv_launch_match_system.setText(getResources().getString(R.string.eleven_person_system));
//                    }
                }
            });
        }
        matchAssoaciationDialog.show();
    }

    public static String getTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }
    /**
     * 获取现在时间
     *
     * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateString = formatter.format(currentTime);
        return dateString;
    }
    private void chooseTime() {
        if (android.os.Build.VERSION.SDK_INT > 20) {
            pwTime = new TimePopupWindow(this, TimePopupWindow.Type.ALL);
            pwTime.setOnTimeSelectListener(new TimePopupWindow.OnTimeSelectListener() {

                @Override
                public void onTimeSelect(Date date) {
                    Long l = date.getTime();
                    Calendar c = Calendar.getInstance();

                    Log.i("month",date.getDay()+"");
                    Log.i("month", c.get(Calendar.YEAR) + "");
                    Log.i("month", c.get(Calendar.MONTH) + "");
                 if (!getTime(date).equals(getStringDate())){
                     if (l < System.currentTimeMillis()) {
                        T.showShort(LaunchMatchActivity.this, "日期不能小于当前日期！");
                         time.setText("");
                        return;
                    }
                 }

//                    if ((date.getYear()==c.get(Calendar.YEAR))&&((date.getMonth()+1)==(c.get(Calendar.MONTH)+1))&&(date.getDay()==c.get(Calendar.DAY_OF_MONTH))&&(date.getHours()==c.get(Calendar.HOUR_OF_DAY))){
//                        if (date.getMinutes()<c.get(Calendar.MINUTE)){
//                            T.showShort(LaunchMatchActivity.this, "日期不能小于当前日期！");
//                            return;
//                        }
//                    }



//                    if (l < System.currentTimeMillis()) {
//                        T.showShort(LaunchMatchActivity.this, "日期不能小于当前日期！");
//                        return;
//                    }
                    time.setText(getTime(date));


                }
            });
            pwTime.showAtLocation(time, Gravity.BOTTOM, 0, 0, new Date());

        } else {
            DateTimePickDialogUtil2 dateTimePicKDialog = new DateTimePickDialogUtil2(
                    LaunchMatchActivity.this, initEndDateTime, 1);
            dateTimePicKDialog.dateTimePicKDialog(time);

        }

    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.launch_match);
    }

    private void refreshSate() {
        labelLeft.setSelected(false);
        labelRight.setSelected(false);
        imageLeft.setVisibility(View.GONE);
        imageRight.setVisibility(View.GONE);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
