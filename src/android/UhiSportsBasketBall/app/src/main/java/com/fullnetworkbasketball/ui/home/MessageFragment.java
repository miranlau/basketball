package com.fullnetworkbasketball.ui.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Group;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseFragment;
import com.fullnetworkbasketball.ui.message.PushYueActivity;
import com.fullnetworkbasketball.ui.user.MineActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.File;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chatuidemo.Constant;
import com.hyphenate.chatuidemo.ui.ChatActivity;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.easeui.utils.EaseSmileUtils;
import com.hyphenate.util.DateUtils;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/4/15 0015.
 */
public class MessageFragment extends BaseFragment {
    @Bind(R.id.mine)
    ImageView mine;

    public static final String MESSAGE_RECEIVED_ACTION = "com.tuisongjpush.MESSAGE_RECEIVED_ACTION";

    public static final String KEY_EXTRAS = "extras";

    public static boolean isForeground = true;

    MessageReceiver messageReceiver;

    //cht 回复评论
    public CircleImageView header_one;
    public TextView content_one;
    public TextView time_one;
    public TextView hour_one;
    public TextView name_one;

    public LinearLayout reply_lt;

    //cht 赛事管家
    public CircleImageView header_two;
    public TextView content_two;
    public TextView time_two;
    public TextView name_two;
    public TextView hour_two;

    public LinearLayout yue_lt;
    @Bind(R.id.chat_lt)
    LinearLayout chatLt;
    private ArrayList<Group> list;


    @Override
    protected int getRootViewLayoutId() {
        return R.layout.fragment_message_layout;
    }
    private void refrehGroup(TextView view,Group group,TextView text,TextView date){
//        EMConversation conversation = EMChatManager.getConversation(group.groupId, EMConversation.EMConversationType.GroupChat);
        EMConversation conversation= EMClient.getInstance().chatManager().getConversation(group.groupId, EMConversation.EMConversationType.GroupChat);
        if (conversation==null){
            view.setVisibility(View.INVISIBLE);
            text.setText("");
            date.setText("");
            return;
        }
        if (conversation.getUnreadMsgCount() > 0) {
            // 显示与此用户的消息未读数
            view.setText(String.valueOf(conversation.getUnreadMsgCount()));
            view.setVisibility(View.VISIBLE);

        } else {
            view.setVisibility(View.INVISIBLE);
        }

        if (conversation.getAllMsgCount() != 0) {
            // 把最后一条消息的内容作为item的message内容
            EMMessage lastMessage = conversation.getLastMessage();
            text.setText(EaseSmileUtils.getSmiledText(getContext(), EaseCommonUtils.getMessageDigest(lastMessage, (this.getContext()))),
                    TextView.BufferType.SPANNABLE);
            date.setText(DateUtils.getTimestampString(new Date(lastMessage.getMsgTime())));
//            holder.time.setText(DateUtils.getTimestampString(new Date(lastMessage.getMsgTime())));
//            if (lastMessage.direct() == EMMessage.Direct.SEND && lastMessage.status() == EMMessage.Status.FAIL) {
//                holder.msgState.setVisibility(View.VISIBLE);
//            } else {
//                holder.msgState.setVisibility(View.GONE);
//            }
        }
    }

    private void getGroupList() {

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        if (UserManager.getIns().getUser() != null) {
            params.put("token", UserManager.getIns().getUser().token);
        }
        Api.getRetrofit().HXGroupList(params).enqueue(new RequestCallbackFWF<HttpResponseFWFArray<Group>>() {
            @Override
            public void onSuccess(HttpResponseFWFArray<Group> response) {

                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    list = response.getData();
                    chatLt.removeAllViews();
                    for (final Group group : list) {
                        View view = LayoutInflater.from(getActivity()).inflate(R.layout.message_group_item_layout, null);
                        TextView unRead = (TextView)view.findViewById(R.id.unread_msg_number);
                        TextView date = (TextView)view.findViewById(R.id.unread_msg_date);
                        ImageView imageView = (ImageView) view.findViewById(R.id.message_group_team_logo);
                        TextView teamName = (TextView) view.findViewById(R.id.message_group_team_name);
                        TextView text = (TextView)view.findViewById(R.id.message_group_team_new);
                        ImageLoader.loadCicleImage(getActivity(), group.logo, R.mipmap.default_team, imageView);
                        teamName.setText(group.teamName);
                        refrehGroup(unRead,group,text,date);
                        chatLt.addView(view);

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // 进入群聊
                                Intent intent = new Intent(getActivity(), ChatActivity.class);
                                // it is group chat
                                intent.putExtra("chatType", Constant.CHATTYPE_GROUP);
                                intent.putExtra("userId", group.groupId);
                                intent.putExtra("teamId",group.teamId);
                                startActivity(intent);
                            }
                        });
                    }
                } else {
                    T.showShort(getActivity(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeViews() {

        registerMessageReceiver();
        header_one = (CircleImageView) findViewById(R.id.header_iv);
        content_one = (TextView) findViewById(R.id.content_tv);
        time_one = (TextView) findViewById(R.id.time_formate_tv);
        name_one = (TextView) findViewById(R.id.name_tv);
        hour_one = (TextView) findViewById(R.id.time);

        header_two = (CircleImageView) findViewById(R.id.header_keeper_iv);
        content_two = (TextView) findViewById(R.id.content_keeper_tv);
        time_two = (TextView) findViewById(R.id.time_formate_keeper_tv);
        name_two = (TextView) findViewById(R.id.name_keeper_tv);
        hour_two = (TextView) findViewById(R.id.hour_keeper_tv);

        yue_lt = (LinearLayout) findViewById(R.id.yue_lt);
        reply_lt = (LinearLayout) findViewById(R.id.reply_lt);

        reply_lt.setOnClickListener(this);
        yue_lt.setOnClickListener(this);

        header_two.setImageResource(R.mipmap.leage_manager);

    }

    @Override
    protected void initializeData() {

        mine.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mine:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                } else {
                    if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
                        App.getInst().toLogin();
                    } else {
                        startActivity(MineActivity.class);
                    }
                }
                break;

            case R.id.reply_lt:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                    return;
                }

                Intent intent = new Intent();
                intent.setClass(getActivity(), PushMeaageListActivity.class);
                startActivity(intent);
                break;

            case R.id.yue_lt:

                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                    return;
                }

                Intent yue = new Intent();
                yue.setClass(getActivity(), PushYueActivity.class);
                startActivity(yue);
                break;
        }

    }

    @Override
    public void onResume() {

        chatLt.removeAllViews();
//        isForeground = true;
        super.onResume();
        //    JPushInterface.onResume(getActivity());

        if (UserManager.getIns().getUser() == null) {
            mine.setImageResource(R.mipmap.default_person);
//

        } else {
            if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
            } else {

                UserManager.getIns().autoLogin();
                upDeviceToken();
                getGroupList();
                if (SharePreHelper.getIns().getModifuHeader()) {

                    ImageLoader.loadCicleImage(this, SharePreHelper.getIns().getHeaderUrl(),R.mipmap.default_person, mine);
                } else {
                    ImageLoader.loadCicleImage(this, UserManager.getIns().getUser().avatar, R.mipmap.default_person,mine);
                }
            }
        }
    }


    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(messageReceiver);
        super.onDestroy();

    }


    @Override
    public void onPause() {
        //  isForeground = false;
        super.onPause();


        // JPushInterface.onPause(getActivity());
    }


    /**
     * 上传deviceToken网络请求
     */
    public void upDeviceToken() {

        //deviceToken
        String deviceToken = JPushInterface.getRegistrationID(getContext());

        //设备id
        final TelephonyManager tm = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "" + tm.getDeviceId();
        int deviceType = 0;

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", UserManager.getIns().getUser().id);
        params.put("deviceId", deviceId);
        params.put("deviceToken", deviceToken);
        params.put("deviceType", deviceType);
        Api.getRetrofit().upDeviceToken(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

            }

            @Override
            public void onFinish() {

            }
        });
    }


    /**
     * 注册消息模式
     */
    public void registerMessageReceiver() {
        messageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        getContext().registerReceiver(messageReceiver, filter);
    }

    /**
     * 消息传递到这
     */
    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {

                String extras = intent.getStringExtra(KEY_EXTRAS);

                if (!TextUtils.isEmpty(extras)) {

                    setCostomMsg(extras);
                }

            }
        }
    }

    /**
     * 若通知传递成功则显示在这里
     */

    public void setCostomMsg(String msg) {

        Log.i("msg", msg);
        try {
            JSONObject jsonObject = new JSONObject(msg);
            if (Integer.parseInt(jsonObject.getString(File.PUSH_TYPE)) == 0) {

                String senderName = jsonObject.getString(File.SENDER_NAME);
                String content = jsonObject.getString(File.CONTENT);
                String senderHeader = jsonObject.getString(File.SENDER_HEADER);
                String sendTime = jsonObject.getString(File.SEND_TIME);

                ImageLoader.loadImage(getActivity(), senderHeader, R.mipmap.default_person, header_one);

                if (TextUtils.isEmpty(sendTime)) {

                } else {

                    String time[] = sendTime.split(" ");
                    time_one.setText(time[0]);
                    hour_one.setText(time[1]);
                }

                content_one.setText(content);

                name_one.setText(senderName);
            } else if (Integer.parseInt(jsonObject.getString(File.PUSH_TYPE)) == 1) {

                String content = jsonObject.getString(File.CONTENT);
                String senderHeader = jsonObject.getString(File.SENDER_HEADER);
                String sendTime = jsonObject.getString(File.SEND_TIME);
                if (TextUtils.isEmpty(sendTime)) {

                } else {

                    String time[] = sendTime.split(" ");
                    time_two.setText(time[0]);
                    hour_two.setText(time[1]);
                }

//                ImageLoader.loadImage(getActivity(), senderHeader, R.mipmap.apply_join, header_two);
                content_two.setText(content);

            } else {
                return;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    //    @Override
//    protected void onResume() {
//        isForeground = true;
//        super.onResume();
//        JPushInterface.onResume(this);
//    }
//
//
//    @Override
//    protected void onPause() {
//        isForeground = false;
//        super.onPause();
//        JPushInterface.onPause(this);
//    }
//
//    @Override
//    protected void onDestroy() {
//        unregisterReceiver(messageReceiver);
//        super.onDestroy();
//    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}