package com.fullnetworkbasketball.ui.home;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.TeamDetails;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.AllMatchAdapter;
import com.fullnetworkbasketball.ui.adapter.TeamDetailGridAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupDialog;
import com.fullnetworkbasketball.ui.dialog.ShareDialog;
import com.fullnetworkbasketball.ui.user.ManageTeamActivity;
import com.fullnetworkbasketball.ui.user.ManageTeamModifyActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.fullnetworkbasketball.utils.Utility;
import com.fullnetworkbasketball.views.MyGridView;
import com.fullnetworkbasketball.views.RadarView;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/26 0026.
 */
public class OtherTeamDetailsActivity extends BaseActivity {
    //    @Bind(R.id.ll_other_team_details_header_of_player)
//    LinearLayout llOtherTeamDetailsHeaderOfPlayer;
    @Bind(R.id.go_fighting)
    TextView goFighting;
    @Bind(R.id.join_the_team)
    TextView joinTheTeam;
    @Bind(R.id.team_details_header)
    ImageView teamDetailsHeader;
    @Bind(R.id.team_details_record)
    TextView teamDetailsRecord;
    @Bind(R.id.team_details_level)
    TextView teamDetailsLevel;
    @Bind(R.id.team_details_avg_age)
    TextView teamDetailsAvgAge;

   /* @Bind(R.id.team_details_attack)
    TextView teamDetailsAttack;
    @Bind(R.id.team_details_speed)
    TextView teamDetailsSpeed;
    @Bind(R.id.team_details_credit)
    TextView teamDetailsCredit;
    @Bind(R.id.team_details_evaluation)
    TextView teamDetailsEvaluation;
    @Bind(R.id.team_details_physical_fitness)
    TextView teamDetailsPhysicalFitness;
    @Bind(R.id.team_details_defense)
    TextView teamDetailsDefense;*/
    RadarView teamDetailsRadar;
   /* @Bind(R.id.ll_radar)
    LinearLayout llRadar;*/
   @Bind(R.id.tv_ountcScore)
   TextView teamOuntcScore;
    @Bind(R.id.tv_avgHelp)
    TextView teamAvgHelp;
    @Bind(R.id.tv_avgBackboard)
    TextView teamAvgBackboard;
    @Bind(R.id.tv_avgSteals)
    TextView teamAvgSteals;
    @Bind(R.id.tv_avgCover)
    TextView teamAvgCover;
    @Bind(R.id.tv_avgAnError)
    TextView teamAvgAnError;


    @Bind(R.id.team_details_name)
    TextView teamDetailsName;
    @Bind(R.id.rb_evaluation_team_details)
    RatingBar rbEvaluationTeamDetails;
    @Bind(R.id.up)
    FrameLayout up;
    @Bind(R.id.dowm)
    TextView dowm;
    @Bind(R.id.attention_team)
    TextView attentionTeam;
    @Bind(R.id.rv_team_details)
    RecyclerView rvTeamDetails;
    private ImageView iv_leader;
    private BottomPupDialog bottomPupDialog;

    private int teamId;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;
    private String attack;
    private String defense;
    private String credit;
    private String physics_fitness;
    private String evaliation;
    private String speed;

    private double[] data;

    public ListView list;

    public MyGridView myGridView;

    private ShareDialog shareDialog;

    private  TeamDetails teamDetails;

    //    public ArrayList<QueryMatchFive> queryMatchFives = new ArrayList<>();
    public ArrayList<Match> queryMatchFives = new ArrayList<>();
    private AllMatchAdapter mAdapter;

    @Override
    protected void setContentView() {
        setContentView(R.layout.other_team_details_activity);
    }

    @Override
    protected void initializeViews() {

        list = (ListView) findViewById(R.id.list);

        myGridView = (MyGridView) findViewById(R.id.team_detail_grid);


        goFighting.setOnClickListener(this);
        joinTheTeam.setOnClickListener(this);
        attentionTeam.setOnClickListener(this);

    }

    private void attentionPlayer() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        Api.getRetrofit().attationTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    T.showShort(getApplicationContext(), response.getMessage());
                    attentionTeam.setText(getResources().getString(R.string.cancel_attention_player));
                    onResume();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void cancelAttentionPlayer() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        Api.getRetrofit().cancelAttationTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    T.showShort(getApplicationContext(), response.getMessage());
                    attentionTeam.setText(getResources().getString(R.string.attention));
                    onResume();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //llOtherTeamDetailsHeaderOfPlayer.removeAllViews();
        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
       /* attack = getResources().getString(R.string.attack);
        defense = getResources().getString(R.string.defense);
        credit = getResources().getString(R.string.credit);
        evaliation = getResources().getString(R.string.evaluation);
        speed = getResources().getString(R.string.speed);
        physics_fitness = getResources().getString(R.string.physics_fitness);*/
        teamId = getIntent().getExtras().getInt("teamId");

        Log.i("teamId", "teamId:" + teamId);
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        Api.getRetrofit().getTeamDtails(params).enqueue(new RequestCallback<HttpResponseFWF2<TeamDetails>>(this) {
            @Override
            public void onSuccess(HttpResponseFWF2<TeamDetails> response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    T.showShort(getApplicationContext(), response.getMessage());
                } else {
                    requestDatailData();
                    teamDetails = response.getData();
                    ImageLoader.loadCicleImage(OtherTeamDetailsActivity.this, teamDetails.logo, R.mipmap.default_team, teamDetailsHeader);
                    teamDetailsName.setText(teamDetails.name);

                    teamOuntcScore.setText(teamDetails.countScore+"");

                    teamAvgHelp.setText(teamDetails.avgHelp+"");

                    teamAvgBackboard.setText(teamDetails.avgBackboard+"");

                    teamAvgSteals.setText(teamDetails.avgSteals+"");

                   teamAvgAnError.setText(teamDetails.avgCover+"");
                    teamAvgAnError.setText(teamDetails.avgAnError+"");


                    rbEvaluationTeamDetails.setRating(Utility.formatInt(teamDetails.stars));
                    teamDetailsRecord.setText(record + teamDetails.win + win + "  " + teamDetails.lose + lose);
                    teamDetailsLevel.setText(level + teamDetails.level);
                    teamDetailsAvgAge.setText(avgAge + teamDetails.age);
                   /* teamDetailsAttack.setText(attack + teamDetails.radarDto.jingong);
                    teamDetailsCredit.setText(credit + teamDetails.radarDto.credit);
                    teamDetailsDefense.setText(defense + teamDetails.radarDto.fangshou);
                    teamDetailsEvaluation.setText(evaliation + teamDetails.radarDto.stars);
                    teamDetailsPhysicalFitness.setText(physics_fitness + teamDetails.radarDto.tineng);
                    teamDetailsSpeed.setText(speed + teamDetails.radarDto.sudu);
                    data = new double[6];
                    data[0] = teamDetails.radarDto.fangshou;
                    data[1] = teamDetails.radarDto.stars;
                    data[2] = teamDetails.radarDto.sudu;
                    data[3] = teamDetails.radarDto.jingong;
                    data[4] = teamDetails.radarDto.credit;
                    data[5] = teamDetails.radarDto.tineng;
                    teamDetailsRadar = new RadarView(OtherTeamDetailsActivity.this, data);*/
//                        Animation operatingAnim = AnimationUtils.loadAnimation(MineActivity.this, R.anim.roatate);
//                        LinearInterpolator lin = new LinearInterpolator();
//                        operatingAnim.setInterpolator(lin);
//
//                    if (operatingAnim != null) {
//                        teamDetailsRadar.startAnimation(operatingAnim);
//                    }
/** 设置旋转动画 */
                   /* final RotateAnimation animation = new RotateAnimation(0f, 30f, Animation.RELATIVE_TO_SELF,
                            0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    animation.setDuration(100);//设置动画持续时间
*//** 常用方法 *//*
                    animation.setRepeatCount(0);//设置重复次数
                    animation.setFillAfter(true);//动画执行完后是否停留在执行完的状态
                    teamDetailsRadar.startAnimation(animation);
                    llRadar.addView(teamDetailsRadar);*/

                    TeamDetailGridAdapter gridAdapter = new TeamDetailGridAdapter(OtherTeamDetailsActivity.this, teamDetails.players, teamDetails.leader);

                    myGridView.setAdapter(gridAdapter);

//                    for (final Player player : teamDetails.players) {
//                        Logger.i("player_size", teamDetails.players.size() + "");
//                        View view = getLayoutInflater().inflate(R.layout.team_details_player_item_final, null);
//                        ImageView header = (ImageView) view.findViewById(R.id.player_header);
//                        ImageLoader.loadCicleImage(OtherTeamDetailsActivity.this, player.avatar, R.mipmap.default_person, header);
//                        TextView number = (TextView) view.findViewById(R.id.number);
//                        TextView name = (TextView)view.findViewById(R.id.team_details_player_item_name);
//                        name.setText(player.name);
//                        number.setText(player.number + "");
//                        iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
//                        int id = player.id;
//                        int leader = teamDetails.leader;
//                        if (id == leader) {
//                            iv_leader.setVisibility(View.VISIBLE);
//                        }
//                        llOtherTeamDetailsHeaderOfPlayer.addView(view);
//                        view.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Bundle bundle = new Bundle();
//                                bundle.putInt("playerId", player.id);
//                                startActivity(PlayerPersonalActivity.class, bundle);
//                            }
//                        });
//                    }

                    if (UserManager.getIns().getUser() == null) {
                        goFighting.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                App.getInst().toLogin();
                            }
                        });
                        joinTheTeam.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                App.getInst().toLogin();
                            }
                        });
                    } else {
                        if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
                            goFighting.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    App.getInst().toLogin();
                                }
                            });
                            joinTheTeam.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    App.getInst().toLogin();
                                }
                            });
                        } else {
                            Logger.i("right", teamDetails.right + "");
                            if (teamDetails.right == 1) {
                                up.setVisibility(View.GONE);
                                dowm.setVisibility(View.VISIBLE);
                                dowm.setText(getResources().getString(R.string.exit_team));
                                dowm.setBackgroundResource(R.mipmap.bottom_red_bt);
                                dowm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        quitTeam();
                                    }
                                });
                            } else if (teamDetails.right == 2) {
                                up.setVisibility(View.GONE);
                                dowm.setVisibility(View.VISIBLE);
                                dowm.setText(getResources().getString(R.string.manage_team));
                                dowm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("teamId", teamDetails.id);
                                        startActivity(ManageTeamActivity.class, bundle);
                                    }
                                });
                            } else if (teamDetails.right == 0) {
                                up.setVisibility(View.GONE);
                                dowm.setVisibility(View.VISIBLE);
                                dowm.setText(getResources().getString(R.string.join_the_team));

                                dowm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("modify", 1);
                                        bundle.putInt("teamId", teamDetails.id);
                                        startActivity(ManageTeamModifyActivity.class, bundle);
                                    }
                                });
                            }
                        }
                    }
//                    if (UserManager.getIns().getUser().id==teamDetails.leader){
//                        attentionTeam.setVisibility(View.INVISIBLE);
//                    }else {
//                        attentionTeam.setVisibility(View.VISIBLE);
//                    }
                    if (teamDetails.follow) {
                        attentionTeam.setText(getResources().getString(R.string.cancel_attention_player));
                    } else {
                        attentionTeam.setText(getResources().getString(R.string.attention));
                    }


                }
            }

            @Override
            public void onFinish() {

            }
        });

    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            // listAdapter.getCount()返回数据项的数目
            View listItem = listAdapter.getView(i, null, listView);
            // 计算子项View 的宽高
            listItem.measure(0, 0);
            // 统计所有子项的总高度
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        // listView.getDividerHeight()获取子项间分隔符占用的高度
        // params.height最后得到整个ListView完整显示需要的高度
        listView.setLayoutParams(params);
    }

    /**
     * cht 最近5场比赛
     * <p/>
     * lid  10000是单对赛，10100匹配赛 其他的是联赛
     */

    public void requestDatailData() {

        HashMap<String, Object> params = new HashMap<>();
        params.put("teamId", teamId);

        Api.getRetrofit().QueryMatchFive(params).enqueue(new RequestCallback<HttpResponseFWFArray<Match>>() {

            @Override
            public void onSuccess(HttpResponseFWFArray<Match> response) {


                if (response.isSuccess()) {
                    Log.i("match", "团队：" + response.getMessage());

                    queryMatchFives = response.data;
//                    QueryMatchFiveAdapter adapter = new QueryMatchFiveAdapter(OtherTeamDetailsActivity.this, queryMatchFives);
//                    list.setAdapter(adapter);
//                    setListViewHeightBasedOnChildren(list);
                    addTypeToData(queryMatchFives);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(OtherTeamDetailsActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    if (rvTeamDetails == null) {
                        return;
                    }
                    rvTeamDetails.setLayoutManager(layoutManager);
                    mAdapter = new AllMatchAdapter(OtherTeamDetailsActivity.this, queryMatchFives);
                    rvTeamDetails.setAdapter(mAdapter);

                } else {
                    T.showShort(OtherTeamDetailsActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });

    }

    private void addTypeToData(ArrayList<Match> list) {
        for (Match match : list) {
            if (match.lid == 10000) {
                if (match.status == 10) {
                    match.setItemType(Match.SINGLE);
                } else if (match.status == 20 || match.status == 100 || match.status == 21) {
                    match.setItemType(Match.SINGLE_ING);
                }
            } else {
                if (match.status == 0) {
                    match.setItemType(Match.MATCH_BEFORE);
                } else if (match.status == 10) {
                    match.setItemType(Match.MATCH_BEFORE_TWO);
                } else {
                    match.setItemType(Match.MATCH_ING);
                }

            }


//         ArrayList<Match> section = new ArrayList<>();
//            for (int i=0;i<list.size();i++){
//                HashMap<String,Object> map=new HashMap<String,Object>();
//                if (match.date.equals(list.get(i).date)){
//                    section.add(match);
//                }
//                map.put(match.date,section);
//            }
//        }
        }
    }


    private void quitTeam() {

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        params.put("userId", UserManager.getIns().getUser().id);
        Api.getRetrofit().quitTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>(this) {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                T.showShort(OtherTeamDetailsActivity.this, response.getMessage());
                if (!response.isSuccess()) {

                } else {


                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.go_fighting:
//                if (UserManager.getIns().getUser() == null) {
//                    App.getInst().toLogin();
//                }else {
//                    if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
//                        App.getInst().toLogin();
//                    }else {
//                        startActivity(TeamDetailsSignUpActivity.class);
//                    }
//                }
//
//                break;
//            case R.id.join_the_team:
//                if (bottomPupDialog == null) {
//                    bottomPupDialog = new BottomPupDialog(this, R.style.callDialog, 1);
//                    bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
//                    bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
//                }
//                bottomPupDialog.show();
//
//                WindowManager windowManager1 = this.getWindowManager();
//                Display display1 = windowManager1.getDefaultDisplay();
//                WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
//                lp1.width = (int) (display1.getWidth()); //设置宽度
//                bottomPupDialog.getWindow().setAttributes(lp1);
//                break;
            case R.id.attention_team:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                    return;
                }
                if (teamDetails.follow) {
                    cancelAttentionPlayer();
                } else {
                    attentionPlayer();
                }
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.team_details_title);
        getCustomActionBar().setRightImageView(R.mipmap.share);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:

                onBackPressed();
                break;
            case R.id.actionbar_right_image:


                String path = Api.getWebBaseUrl() + "ctl/team/share?teamId=" + teamId;
                if (shareDialog == null) {

                    shareDialog = new ShareDialog(OtherTeamDetailsActivity.this, R.style.callDialog, 2, path);
                    shareDialog.getWindow().setGravity(Gravity.BOTTOM);
                }

                shareDialog.show();


                break;

        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (getIntent().getBooleanExtra("isFormHere", false)) {
//                Intent intent = new Intent(OtherTeamDetailsActivity.this, RaceCalendarActivity.class);
//                intent.putExtra("leagueId", getIntent().getIntExtra("leagueId", 0));
//                startActivity(intent);
//                finish();
//            } else {
//                onBackPressed();
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
