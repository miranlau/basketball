package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.adapter.PreRacingAdapter;
import com.fullnetworkbasketball.ui.match.MatchScoreActivity;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;

/**
 * cht
 * 赛前确认
 */
public class PreRacingActivity extends com.fullnetworkbasketball.ui.base.BaseActivity implements PreRacingAdapter.OnConfirmOrderListener {

    //球队id
    public int teamId;
    //赛事id
    public int matchId;
    //所有球员消息集合
    public ArrayList<Player> list;
    //加载的数据adapter
    public PreRacingAdapter adapter;

    public XRecyclerView xRecyclerView;
    @Bind(R.id.bt_choose_right)
    TextView btChooseRight;
    private int type;
    private boolean isSureFirst = false;
    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_pre_racing);
        type = getIntent().getIntExtra("type", 0);
        isSureFirst = getIntent().getBooleanExtra("isSureFirst", false);
        game = (Game) getIntent().getSerializableExtra("game");

    }

    @Override
    protected void initializeViews() {
        xRecyclerView = (XRecyclerView) findViewById(R.id.pre_racing_xv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        xRecyclerView.setLayoutManager(layoutManager);
        xRecyclerView.setPullRefreshEnabled(false);
        ((TextView) findViewById(R.id.add_player_tv)).setOnClickListener(this);
        ((TextView) findViewById(R.id.next_tv)).setOnClickListener(this);
    }

    @Override
    protected void initializeData() {
        Intent intent = getIntent();
        teamId = intent.getIntExtra("teamId", 0);
        matchId = intent.getIntExtra("matchId", 0);
        Log.i("matchId", "球队Id:" + teamId + "," + "比赛Id:" + matchId);
        list = new ArrayList<Player>();
        // requestAllPlayers();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_player_tv:
                Intent intent = new Intent();
                intent.putExtra("teamId", teamId);
                intent.putExtra("matchId", matchId);
                intent.putExtra("game", game);
                intent.setClass(PreRacingActivity.this, AddPlayersActivity.class);
                startActivity(intent);
                break;
            case R.id.next_tv:
                if (isSureFirst) {
                    Intent intent1 = new Intent(PreRacingActivity.this, MatchScoreActivity.class);
                    intent1.putExtra("matchId", matchId);
                    intent1.putExtra("teamId", teamId);
                    intent1.putExtra("game", game);
                    startActivity(intent1);
                } else {
                    Intent intent1 = new Intent(this, TeamDetailsSignUpActivity.class);
                    intent1.putExtra("matchId", matchId);
                    startActivity(intent1);
                    finish();
                }
                break;
        }
    }

    /**
     * 网络请求 获取所有参赛球员
     */

    public void requestAllPlayers() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("teamId", teamId);
        params.put("matchId", matchId);
        Api.getRetrofit().haveSignedUp(params).enqueue(new RequestCallback<HttpResponseFWFArray<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWFArray<Player> response) {
                if (response.isSuccess()) {
                    Log.i("preData", response.getData().toString());
                    list = response.getData();
                    adapter = new PreRacingAdapter(PreRacingActivity.this, list, teamId, matchId,game);
                    xRecyclerView.setAdapter(adapter);
                    adapter.setConfirmClickListener(PreRacingActivity.this);
                }
            }
            @Override
            public void onFinish() {
            }
        });
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.pre_match_sure);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                Intent intent1 = new Intent(this, TeamDetailsSignUpActivity.class);
                intent1.putExtra("matchId", matchId);
                startActivity(intent1);
                finish();
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        requestAllPlayers();
    }

    @Override
    public void onConfirmOrder(int i) {
        if (i == 1) {
            requestAllPlayers();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==event.KEYCODE_BACK){
            Intent intent1 = new Intent(this, TeamDetailsSignUpActivity.class);
            intent1.putExtra("matchId", matchId);
            startActivity(intent1);
            finish();
            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }
}
