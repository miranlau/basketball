package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.db.MySQLite;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupChooseTeamDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.match.ModifyMatchActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.TextUtil;
import com.fullnetworkbasketball.utils.Utility;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/20 0020.
 */
public class RacingActivity extends BaseActivity {

    @Bind(R.id.i_want_to_battle)
    TextView iWantToBattle;
    @Bind(R.id.launch_race_team_logo)
    ImageView launchRaceTeamLogo;
    @Bind(R.id.launch_race_address)
    TextView launchRaceAddress;
    @Bind(R.id.launch_race_time)
    TextView launchRaceTime;
    @Bind(R.id.launch_race_system)
    TextView launchRaceSystem;
    @Bind(R.id.launch_race_team_name)
    TextView launchRaceTeamName;
    @Bind(R.id.launch_race_team_combat)
    TextView launchRaceTeamCombat;
    @Bind(R.id.launch_race_age)
    TextView launchRaceAge;
    @Bind(R.id.launch_race_remark)
    TextView launchRaceRemark;
    @Bind(R.id.ll_launch_race_teams)
    LinearLayout llLaunchRaceTeams;
    @Bind(R.id.up)
    FrameLayout up;
    @Bind(R.id.has_visited_team)
    TextView hasVisitedTeam;
    @Bind(R.id.delete_match)
    TextView deleteMatch;
    @Bind(R.id.edit_match)
    TextView editMatch;
    private BottomPupChooseTeamDialog bottomPupDialog;
    private Match match;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;
    private ContactOpponentCaptainDialog dialog;
    private Team team_choose;

    private int matchId;

    private MySQLite mySQLite;

    @Override
    protected void setContentView() {

        setContentView(R.layout.racing_activity_layout);
//        match = (Match) getIntent().getSerializableExtra("match");

        matchId = getIntent().getIntExtra("matchId", 0);
        // mySQLite = new MySQLite(RacingActivity.this);
    }

    @Override
    protected void initializeViews() {
        iWantToBattle.setOnClickListener(this);
        deleteMatch.setOnClickListener(this);
        editMatch.setOnClickListener(this);
        launchRaceTeamLogo.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {
        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
    }

    @Override
    protected void onResume() {
        super.onResume();

        queryMatchDetails();
    }

    private void queryMatchDetails() {
        llLaunchRaceTeams.removeAllViews();
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>(this) {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    match = response.getData();
                    ImageLoader.loadCicleImage(RacingActivity.this, match.home.logo, R.mipmap.default_team, launchRaceTeamLogo);
                    launchRaceAddress.setText(match.addr);
                    launchRaceTeamName.setText(match.home.name);
                    launchRaceTime.setText(match.date);
                    launchRaceSystem.setText(match.type + "人制");
                    launchRaceTeamCombat.setText(match.power);

                     if (match.age==null){
                         launchRaceAge.setText("");
                     }  else {
                    launchRaceAge.setText(match.age + "");
                     }
                    launchRaceRemark.setText(match.desc);
                    if (UserManager.getIns().getUser() == null) {
                        up.setVisibility(View.GONE);
                        iWantToBattle.setVisibility(View.GONE);
                        getCustomActionBar().getRightText().setText("");
                    } else {
                        if (match.home.leader == UserManager.getIns().getUser().id) {
                            up.setVisibility(View.VISIBLE);
                            iWantToBattle.setVisibility(View.GONE);

                        } else {
                            if (match.gauntlet) {
                                up.setVisibility(View.GONE);
                                iWantToBattle.setVisibility(View.VISIBLE);
                                iWantToBattle.setText(getResources().getString(R.string.cancel_join_match));
                                iWantToBattle.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cancelJoinMatch();
                                    }
                                });
                            }
                        }
                        if (match.home.leader != UserManager.getIns().getUser().id || match.datings == 0) {
                            getCustomActionBar().getRightText().setText("");
                        }
                    }

                    if (match.dating == null || match.dating.size() == 0) {
                        hasVisitedTeam.setVisibility(View.GONE);
                    }


                    if (match.dating == null) {
                        return;
                    }
                    for (final Team team : match.dating) {
                        View view = LayoutInflater.from(RacingActivity.this).inflate(R.layout.team_item_layout, null);
                        ImageView my_team_header = (ImageView) view.findViewById(R.id.my_team_header);
                        ImageLoader.loadCicleImage(RacingActivity.this, team.logo, R.mipmap.default_team, my_team_header);
                        TextView my_name_name = (TextView) view.findViewById(R.id.my_name_name);
                        my_name_name.setText(team.name);
                        TextView my_team_level = (TextView) view.findViewById(R.id.my_team_level);
                        my_team_level.setText(team.level + getResources().getString(R.string.team_level));
                        RatingBar rb_evaluation_my_team = (RatingBar) view.findViewById(R.id.rb_evaluation_my_team);
                        rb_evaluation_my_team.setRating(Utility.formatInt(team.stars));
                        TextView my_team_record = (TextView) view.findViewById(R.id.my_team_record);
                        my_team_record.setText(record + team.win + win + team.deuce + decue + team.lose + lose);
                        TextView my_team_avg_age = (TextView) view.findViewById(R.id.my_team_avg_age);
                        my_team_avg_age.setText(avgAge + team.age);
                        llLaunchRaceTeams.addView(view);
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putInt("teamId", team.id);
                                startActivity(OtherTeamDetailsActivity.class, bundle);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void cancelJoinMatch() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().cancelJoinMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    //  mySQLite.DeleteYueData(matchId);
                    finish();
                } else {
                    T.showShort(RacingActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {

        if (UserManager.getIns().getUser() == null) {
            App.getInst().toLogin();
            return;
        }
        if (!TextUtil.isValidate(UserManager.getIns().getUser().token)) {
            App.getInst().toLogin();
            return;
        }
        switch (v.getId()) {
            case R.id.i_want_to_battle:
                getMyInfo();
                break;
            case R.id.delete_match:

                if (dialog == null) {
                    dialog = new ContactOpponentCaptainDialog(RacingActivity.this, R.style.callDialog, 5);
                    dialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                        @Override
                        public void onConfirmOrder(int i) {
                            if (i == 5) {
                                deleteMatch();
                            }
                        }
                    });
                }
                dialog.show();


                break;
            case R.id.edit_match:
                Intent intent = new Intent(RacingActivity.this, ModifyMatchActivity.class);
                intent.putExtra("match", match);
                startActivity(intent);

                break;
            case R.id.launch_race_team_logo:
                Intent intent1 = new Intent(RacingActivity.this, OtherTeamDetailsActivity.class);
                intent1.putExtra("teamId", match.home.id);
                startActivity(intent1);
                break;
        }
    }

    private void deleteMatch() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().deleteMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>(this) {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    mySQLite = new MySQLite(RacingActivity.this);
                    mySQLite.DeleteYueData(matchId);
                    SharePreHelper.getIns().putIsCreateMatch(true);
                    finish();
                } else {
                    T.showShort(RacingActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void getMyInfo() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        if (UserManager.getIns().getUser() != null) {
            params.put("userId", UserManager.getIns().getUser().id);
        }
        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    ArrayList<Team> myTeams = new ArrayList<Team>();
                    for (Team team : response.getData().teams) {
                        if (response.getData().id == team.leader) {
                            myTeams.add(team);
                        }
                    }

                    if (myTeams.size() == 0) {
                        getCustomActionBar().getRightText().setText("");
                        if (dialog == null) {
                            dialog = new ContactOpponentCaptainDialog(RacingActivity.this, R.style.callDialog, 4);
                        }
                        dialog.show();
                        return;
                    }
                    if (bottomPupDialog == null) {
                        bottomPupDialog = new BottomPupChooseTeamDialog(RacingActivity.this, R.style.callDialog, 1, myTeams);
                        bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
                        bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                        bottomPupDialog.setConfirmClickListener(new BottomPupChooseTeamDialog.OnConfirmOrderListener() {
                            @Override
                            public void onConfirmOrder(Team team) {
                                joinMatch(team);
                            }
                        });
                    }
                    bottomPupDialog.show();

                    WindowManager windowManager1 = RacingActivity.this.getWindowManager();
                    Display display1 = windowManager1.getDefaultDisplay();
                    WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
                    lp1.width = (int) (display1.getWidth()); //设置宽度
                    bottomPupDialog.getWindow().setAttributes(lp1);
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void joinMatch(Team team) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", team.id);
        Api.getRetrofit().joinMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    queryMatchDetails();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(getResources().getString(R.string.racing));
        getCustomActionBar().getRightText().setVisibility(View.VISIBLE);
        getCustomActionBar().getRightText().setTextColor(getResources().getColor(R.color.white));
        getCustomActionBar().getRightText().setText(getResources().getString(R.string.choose));
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_txt:
                popDialog();
                break;
        }
    }

    private void chooseOpponent() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", team_choose.id);
        Api.getRetrofit().chooseTeamForMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    SharePreHelper.getIns().putIsCreateMatch(true);
                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void popDialog() {
        if (bottomPupDialog == null) {
            bottomPupDialog = new BottomPupChooseTeamDialog(RacingActivity.this, R.style.callDialog, 0, match.dating);
            bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
            bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            bottomPupDialog.setConfirmClickListener(new BottomPupChooseTeamDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(Team team) {
                    team_choose = team;
                    chooseOpponent();

                }
            });
        }
        bottomPupDialog.show();

        WindowManager windowManager1 = RacingActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        bottomPupDialog.getWindow().setAttributes(lp1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
