package com.fullnetworkbasketball.ui.home;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/4/20 0020.
 */
public class RacingOfLaunchMatchActivity extends BaseActivity {
    @Bind(R.id.ll_go_to_team_details)
    LinearLayout llGoToTeamDetails;
    @Bind(R.id.edit_match)
    TextView editMatch;

    @Override
    protected void setContentView() {
        setContentView(R.layout.racing_activity_of_launch_match_layout);
    }

    @Override
    protected void initializeViews() {
        editMatch.setOnClickListener(this);
        llGoToTeamDetails.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_match:
                startActivity(LaunchMatchActivity.class);
                break;
            case R.id.ll_go_to_team_details:
                startActivity(OtherTeamDetailsActivity.class);

                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.racing_title);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

}
