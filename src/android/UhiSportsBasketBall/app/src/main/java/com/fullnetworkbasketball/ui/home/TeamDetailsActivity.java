package com.fullnetworkbasketball.ui.home;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/4/25 0025.
 */
public class TeamDetailsActivity extends BaseActivity {
    @Bind(R.id.player_item_header)
    LinearLayout playerItemHeader;
    @Bind(R.id.player_item_header_two)
    LinearLayout playerItemHeaderTwo;
    @Bind(R.id.live_interactive)
    TextView liveInteractive;
    private ImageView iv_leader;

    @Override
    protected void setContentView() {
        setContentView(R.layout.team_details_activity);
    }

    @Override
    protected void initializeViews() {

        for (int i = 0; i < 6; i++) {
            View view = getLayoutInflater().inflate(R.layout.team_details_player_item, null);
            iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
            if (i == 0) {
                iv_leader.setVisibility(View.VISIBLE);
            }
            playerItemHeader.addView(view);
        }
        for (int i = 0; i < 6; i++) {
            View view = getLayoutInflater().inflate(R.layout.team_details_player_item, null);
            iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
            playerItemHeaderTwo.addView(view);
        }
        liveInteractive.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
    switch (v.getId()){
        case R.id.live_interactive:
            startActivity(TeamDetailsOfLiveInteractive.class);
            break;
    }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.team_details_title);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                finish();
                break;
        }
    }



}
