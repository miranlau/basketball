package com.fullnetworkbasketball.ui.home;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.adapter.TeamDetailsInteractiveImgTxtAdapter;
import com.fullnetworkbasketball.ui.adapter.TeamDetailsInteractiveTimeAdapter;
import com.fullnetworkbasketball.ui.adapter.TechnicalStatisticsAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.views.ObserverHScrollView;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/4/25 0025.
 */
public class TeamDetailsOfLiveInteractive extends BaseActivity {
    @Bind(R.id.label_team)
    TextView labelTeam;
    @Bind(R.id.image_team)
    ImageView imageTeam;
    @Bind(R.id.filter_football_home_team)
    RelativeLayout filterFootballHomeTeam;
    @Bind(R.id.label_player)
    TextView labelPlayer;
    @Bind(R.id.image_player)
    ImageView imagePlayer;
    @Bind(R.id.filter_football_home_player)
    RelativeLayout filterFootballHomePlayer;
    @Bind(R.id.label_imgTxt)
    TextView labelImgTxt;
    @Bind(R.id.image_imgTxt)
    ImageView imageImgTxt;
    @Bind(R.id.filter_football_home_imgTxt)
    RelativeLayout filterFootballHomeImgTxt;
    @Bind(R.id.label_formation)
    TextView labelFormation;
    @Bind(R.id.image_formation)
    ImageView imageFormation;
    @Bind(R.id.filter_football_home_formation)
    RelativeLayout filterFootballHomeFormation;
    @Bind(R.id.label_time)
    TextView labelTime;
    @Bind(R.id.image_time)
    ImageView imageTime;
    @Bind(R.id.filter_football_home_time)
    RelativeLayout filterFootballHomeTime;
    @Bind(R.id.team_details_change)
    LinearLayout teamDetailsChange;
    private Typeface tf;
    private ListView technical_statistics;
    private TechnicalStatisticsAdapter adapter;
    private ArrayList<String> list_left = new ArrayList<String>();
    private ArrayList<String> list_right = new ArrayList<String>();
    private XRecyclerView xRecyclerView;
    private ListView listView;
    /**列表表头容器**/
    private RelativeLayout mListviewHead;
    private HorizontalScrollView mHorizontalScrollView;
    private BondSearchResultAdapter mListAdapter;
    private String[] one = {"10 梅西","80'55''","1","3/17","6/2/3","0","5"};
    private String[] two = {"11 C罗","70'55''","3","3/8","5/2/3","0","5"};
    private String[] three = {"16 本泽马","90'55''","2","3/9","4/2/3","0","1"};
    private String[] four = {"20 罗本","60'55''","0","3/10","3/2/3","0","2"};
    private String[] five = {"21 里贝里","50'55''","1","3/5","6/2/3","0","5"};
    private HashMap<Integer,String[]> map= new HashMap<Integer,String[]>();
    @Override
    protected void setContentView() {
        setContentView(R.layout.team_details_of_live_interactive);
    }

    protected String[] mParties = new String[]{
            "Party A", "Party B"
    };

    @Override
    protected void initializeViews() {
       map.put(0,one);
        map.put(1,two);
        map.put(2,three);
        map.put(3,four);
        map.put(4,five);
        filterFootballHomeTeam.setOnClickListener(this);
        filterFootballHomePlayer.setOnClickListener(this);
        filterFootballHomeImgTxt.setOnClickListener(this);
        filterFootballHomeFormation.setOnClickListener(this);
        filterFootballHomeTime.setOnClickListener(this);
        refreshState();
        labelTeam.setSelected(true);
        imageTeam.setVisibility(View.VISIBLE);
        View view = getLayoutInflater().inflate(R.layout.team_details_interactive_team_layout, null);
        teamDetailsChange.removeAllViews();
        teamDetailsChange.addView(view);

        list_left.add("68%");
        list_left.add("10");
        list_left.add("4");
        list_left.add("9");
        list_left.add("7");
        list_left.add("2");
        list_left.add("14");
        list_left.add("5");
        list_left.add("3");
        list_left.add("6");
        list_left.add("0");
        list_left.add("1");


        list_right.add("66%");
        list_right.add("10");
        list_right.add("8");
        list_right.add("4");
        list_right.add("11");
        list_right.add("4");
        list_right.add("17");
        list_right.add("3");
        list_right.add("4");
        list_right.add("8");
        list_right.add("1");
        list_right.add("0");
        technical_statistics = (ListView) view.findViewById(R.id.technical_statistics);
        adapter = new TechnicalStatisticsAdapter(this, list_left, list_right);
        technical_statistics.setAdapter(adapter);
    }



    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_home_team:
                refreshState();
                labelTeam.setSelected(true);
                imageTeam.setVisibility(View.VISIBLE);
                View view = getLayoutInflater().inflate(R.layout.team_details_interactive_team_layout, null);
                teamDetailsChange.removeAllViews();
                teamDetailsChange.addView(view);

                break;
            case R.id.filter_football_home_player:
                refreshState();
                labelPlayer.setSelected(true);
                imagePlayer.setVisibility(View.VISIBLE);
                teamDetailsChange.removeAllViews();
                View view2 = getLayoutInflater().inflate(R.layout.team_details_interactive_palyer, null);
                teamDetailsChange.addView(view2);

                //初始化列表表头
                mListviewHead = (RelativeLayout) findViewById(R.id.head);
                mListviewHead.setFocusable(true);
                mListviewHead.setClickable(true);
                mListviewHead.setBackgroundColor(getResources().getColor(R.color.item_bg));
                mListviewHead.setOnTouchListener(mHeadListTouchLinstener);
                mHorizontalScrollView = (HorizontalScrollView) mListviewHead.findViewById(R.id.horizontalScrollView1);

                //实例化listview适配器
                mListAdapter = new BondSearchResultAdapter();

                //初始化listview
                listView = (ListView)view2.findViewById(R.id.lv);
                listView.setOnTouchListener(mHeadListTouchLinstener);
                listView.setAdapter(mListAdapter);
                break;
            case R.id.filter_football_home_imgTxt:
                refreshState();
                labelImgTxt.setSelected(true);
                imageImgTxt.setVisibility(View.VISIBLE);
                teamDetailsChange.removeAllViews();
                View view3 = getLayoutInflater().inflate(R.layout.team_details_interactive_imgtxt, null);
                teamDetailsChange.addView(view3);
                xRecyclerView = (XRecyclerView) view3.findViewById(R.id.image_imgTxt_recyclerView);
                TeamDetailsInteractiveImgTxtAdapter adapter = new TeamDetailsInteractiveImgTxtAdapter(this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                xRecyclerView.setLayoutManager(layoutManager);
                xRecyclerView.setAdapter(adapter);
                break;
            case R.id.filter_football_home_formation:
                refreshState();
                labelFormation.setSelected(true);
                imageFormation.setVisibility(View.VISIBLE);
                teamDetailsChange.removeAllViews();
                View view4 = getLayoutInflater().inflate(R.layout.team_details_interactive_foemation, null);
                teamDetailsChange.addView(view4);
                break;
            case R.id.filter_football_home_time:
                refreshState();
                labelTime.setSelected(true);
                imageTime.setVisibility(View.VISIBLE);
                teamDetailsChange.removeAllViews();
                View view5 = getLayoutInflater().inflate(R.layout.team_details_interactive_time, null);
                teamDetailsChange.addView(view5);
                xRecyclerView = (XRecyclerView) view5.findViewById(R.id.image_imgTxt_recyclerView);
                TeamDetailsInteractiveTimeAdapter adapter1 = new TeamDetailsInteractiveTimeAdapter(this);
                LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
                layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
                xRecyclerView.setLayoutManager(layoutManager1);
                xRecyclerView.setAdapter(adapter1);
                xRecyclerView.setPullRefreshEnabled(false);
                xRecyclerView.setLoadingMoreEnabled(false);
                xRecyclerView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return false;
                    }
                });
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.team_details_title);

    }
    /**
     * 列头/Listview触摸事件监听器<br>
     * 当在列头 和 listView控件上touch时，将这个touch的事件分发给 ScrollView
     */
    private View.OnTouchListener mHeadListTouchLinstener = new View.OnTouchListener(){

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mHorizontalScrollView.onTouchEvent(event);
            return false;
        }
    };
    /**
     * listView滚动监听器
     */
    private AbsListView.OnScrollListener mListViewScrollListener = new AbsListView.OnScrollListener() {
        @Override
        /**
         * firstVisibleItem 当前页第一条记录的索引
         * visibleItemCount 当前页可见条数
         * totalItemCount 已加载总记录数
         **/
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    };
    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }

    }

    private void refreshState() {
        labelTeam.setSelected(false);
        imageTeam.setVisibility(View.GONE);
        labelPlayer.setSelected(false);
        imagePlayer.setVisibility(View.GONE);
        labelImgTxt.setSelected(false);
        imageImgTxt.setVisibility(View.GONE);
        labelFormation.setSelected(false);
        imageFormation.setVisibility(View.GONE);
        labelTime.setSelected(false);
        imageTime.setVisibility(View.GONE);

    }

    public class BondSearchResultAdapter extends BaseAdapter {
        private Context context;
        public BondSearchResultAdapter(){
        }

        @Override
        public int getCount() {
            return map.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // 查找控件
            ViewHolder holder = null;
            if (null == convertView) {
                convertView = LayoutInflater.from(TeamDetailsOfLiveInteractive.this).inflate(R.layout.activity_hvscorll_listview_item, null);
                holder = new ViewHolder();

                holder.txt1 = (TextView) convertView.findViewById(R.id.textView1);
                holder.textView_time = (TextView)convertView.findViewById(R.id.textView_time);
                holder.textView_goal = (TextView)convertView.findViewById(R.id.textView_goal);
                holder.textView_shoot_ = (TextView)convertView.findViewById(R.id.textView_shoot_);
                holder.textView_steal = (TextView)convertView.findViewById(R.id.textView_steal);
                holder.textView_pu = (TextView)convertView.findViewById(R.id.textView_pu);
                holder.textView_foul = (TextView)convertView.findViewById(R.id.textView_foul);
                //列表水平滚动条
                ObserverHScrollView scrollView1 = (ObserverHScrollView) convertView.findViewById(R.id.horizontalScrollView1);
                holder.scrollView = (ObserverHScrollView) convertView.findViewById(R.id.horizontalScrollView1);
                //列表表头滚动条
                ObserverHScrollView headSrcrollView = (ObserverHScrollView) mListviewHead.findViewById(R.id.horizontalScrollView1);
                headSrcrollView.AddOnScrollChangedListener(new OnScrollChangedListenerImp(scrollView1));

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            // 装填数据

            holder.txt1.setText(map.get(position)[0]);
            holder.textView_time.setText(map.get(position)[1]);
            holder.textView_goal.setText(map.get(position)[2]);
            holder.textView_shoot_.setText(map.get(position)[3]);
            holder.textView_steal.setText(map.get(position)[4]);
            holder.textView_pu.setText(map.get(position)[5]);
            holder.textView_foul.setText(map.get(position)[6]);
            if (position%2==0){
                convertView.setBackgroundColor(getResources().getColor(R.color.item_bg_other));
            }else {
                convertView.setBackgroundColor(getResources().getColor(R.color.item_bg));
            }
            return convertView;
        }
        private class ViewHolder {
            TextView txt1;
            TextView textView_time;
            TextView textView_goal;
            TextView textView_shoot_;
            TextView textView_steal;
            TextView textView_pu;
            TextView textView_foul;

            HorizontalScrollView scrollView;
        }
        private class OnScrollChangedListenerImp implements ObserverHScrollView.OnScrollChangedListener {
            ObserverHScrollView mScrollViewArg;

            public OnScrollChangedListenerImp(ObserverHScrollView scrollViewar) {
                mScrollViewArg = scrollViewar;
            }

            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                mScrollViewArg.smoothScrollTo(l, t);
            }
        }
    }


}


