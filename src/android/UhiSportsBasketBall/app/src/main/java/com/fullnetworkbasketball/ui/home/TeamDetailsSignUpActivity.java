package com.fullnetworkbasketball.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.TeamDetailGridAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupChooseTeamDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.match.BasketBallMatchScoreActivity;
import com.fullnetworkbasketball.ui.match.InteractiveLiveActivity;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.MyGridView;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import net.dr.qwzq.ConfirmFirstActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/4/26 0026.
 */
public class TeamDetailsSignUpActivity extends BaseActivity {
    //    @Bind(R.id.ll_other_team_details_header_of_player_home)
//    LinearLayout llOtherTeamDetailsHeaderOfPlayerHome;
//    @Bind(R.id.ll_other_team_details_header_of_player_visit)
//    LinearLayout llOtherTeamDetailsHeaderOfPlayerVisit;
    //    @Bind(R.id.contact_opponent_captain)
//    TextView contactOpponentCaptain;
//    @Bind(R.id.sign_up)
//    TextView signUp;
    @Bind(R.id.team_details_sign_home_header)
    ImageView teamDetailsSignHomeHeader;
    @Bind(R.id.team_details_sign_home_name)
    TextView teamDetailsSignHomeName;
    @Bind(R.id.team_details_sign_home_combat)
    TextView teamDetailsSignHomeCombat;
    @Bind(R.id.team_details_sign_home_age)
    TextView teamDetailsSignHomeAge;
    @Bind(R.id.team_details_sign_match_time)
    TextView teamDetailsSignMatchTime;
    @Bind(R.id.team_details_sign_match_venue)
    TextView teamDetailsSignMatchVenue;
    @Bind(R.id.team_details_sign_match_system)
    TextView teamDetailsSignMatchSystem;
    @Bind(R.id.team_details_sign_visit_header)
    ImageView teamDetailsSignVisitHeader;
    @Bind(R.id.team_details_sign_visit_name)
    TextView teamDetailsSignVisitName;
    @Bind(R.id.team_details_sign_visit_combat)
    TextView teamDetailsSignVisitCombat;
    @Bind(R.id.team_details_sign_visit_age)
    TextView teamDetailsSignVisitAge;
    @Bind(R.id.tv_home_player)
    TextView tvHomePlayer;
    @Bind(R.id.tv_visitor_player)
    TextView tvVisitorPlayer;
    @Bind(R.id.up) // 计分比赛
    FrameLayout up;
    @Bind(R.id.dowm) //报名参加
    TextView dowm;
    @Bind(R.id.match_score)
    TextView matchScore;
    @Bind(R.id.interactive_live)
    TextView interactiveLive;
    @Bind(R.id.stage)
    TextView stage;
    private ImageView iv_leader;
    private ImageView iv_leader1;
    private ContactOpponentCaptainDialog dialog;
    private int isScorer;
    private int matchId;
    private Match match;
    private int teamId;
    private Game game;
    private BottomPupChooseTeamDialog bottomPupDialog;
    private ArrayList<Team> teams;
    private ArrayList<Team> teams1;

    private MyGridView mHomeGrid;
    private MyGridView mVisitingGrid;

    @Override
    protected void setContentView() {
        setContentView(R.layout.team_details_sign_up_activity);
        matchId = getIntent().getIntExtra("matchId", 0);


        mHomeGrid = (MyGridView) findViewById(R.id.team_detail_sign_up_home_grid);
        mVisitingGrid = (MyGridView) findViewById(R.id.team_detail_sign_up_visiting_grid);

        Log.i("matchId", "赛事Id：" + matchId);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        llOtherTeamDetailsHeaderOfPlayerHome.removeAllViews();
//        llOtherTeamDetailsHeaderOfPlayerVisit.removeAllViews();
        queryMatchDetials();
    }

    private void queryMatchDetials() {

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>(this) {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    match = response.getData();
                    if (UserManager.getIns().getUser() == null) {
                        up.setVisibility(View.GONE);
                        dowm.setVisibility(View.GONE);

                        if (match.status == 10) {
                            up.setVisibility(View.GONE);
                            dowm.setVisibility(View.VISIBLE);

                            tvVisitorPlayer.setEnabled(false);
                            tvHomePlayer.setEnabled(false);

                            dowm.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(LoginActivity.class);
                                }
                            });
                        } else if (match.status == 20) {
                            up.setVisibility(View.VISIBLE);
                            dowm.setVisibility(View.GONE);
                            getCustomActionBar().setTitleText(R.string.middle_match);
                            matchScore.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(LoginActivity.class);
                                }
                            });
                            interactiveLive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(TeamDetailsSignUpActivity.this, InteractiveLiveActivity.class);
                                    intent.putExtra("matchId", match.id);
                                    startActivity(intent);
                                }
                            });
                        }
                    } else {
                        if (UserManager.getIns().getUser().id == match.home.leader || UserManager.getIns().getUser().id == match.visiting.leader) {


                            if (match.status == 10) {
//                                up.setVisibility(View.GONE);
//                                dowm.setVisibility(View.VISIBLE);
//                                dowm.setText(getResources().getString(R.string.contact_other_captain));
                                up.setVisibility(View.VISIBLE);
                                dowm.setVisibility(View.GONE);
                                matchScore.setText(getResources().getString(R.string.contact_other_captain));
                                //如果是主队队长，则联系客队队长
                                if (UserManager.getIns().getUser().id == match.home.leader) {
                                    matchScore.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (isScorer == 1) {
                                            } else {
                                                if (dialog == null) {
                                                    dialog = new ContactOpponentCaptainDialog(TeamDetailsSignUpActivity.this, R.style.callDialog, 9, match.visiting.mobile, match.visiting.leaderName);
                                                }
                                                dialog.show();
                                            }
                                        }
                                    });
                                } else {
                                    matchScore.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (isScorer == 1) {
                                            } else {
                                                if (dialog == null) {
                                                    dialog = new ContactOpponentCaptainDialog(TeamDetailsSignUpActivity.this, R.style.callDialog, 9, match.home.mobile, match.home.leaderName);
                                                }
                                                dialog.show();
                                            }
                                        }
                                    });
                                }
                                if (match.enroll) {
                                    interactiveLive.setText(getResources().getString(R.string.cancel_sign_up));
                                    interactiveLive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cacelSingUp();
                                        }
                                    });

                                } else if (!match.enroll) {
                                    interactiveLive.setText(getResources().getString(R.string.join_the_match));

                                    interactiveLive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            boolean isHome = false;
//                                            boolean isVosit = false;
//                                            for (Player player : match.home.players) {
//                                                if (player.id == UserManager.getIns().getUser().id) {
//                                                    isHome = true;
//                                                }
//                                            }
//                                            for (Player player : match.visiting.players) {
//                                                if (player.id == UserManager.getIns().getUser().id) {
//                                                    isVosit = true;
//                                                }
//                                            }
//                                            if (isHome && isVosit) {
//                                                myTeam();
//                                                popDialog();
//                                            } else {
//                                                singUp();
//                                            }
                                            myTeam();
                                        }
                                    });
                                }


                            } else {
                                getCustomActionBar().setTitleText(R.string.middle_match);
                                judgeRecord(match);
                            }

                        } else {
                            if (match.status == 10) {
                                if (match.enroll) {
                                    up.setVisibility(View.GONE);
                                    dowm.setVisibility(View.VISIBLE);
                                    tvVisitorPlayer.setEnabled(false);
                                    tvHomePlayer.setEnabled(false);
                                    dowm.setText(getResources().getString(R.string.cancel_sign_up));
                                    dowm.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cacelSingUp();
                                        }
                                    });
                                } else if (!match.enroll) {
                                    up.setVisibility(View.GONE);
                                    dowm.setVisibility(View.VISIBLE);
                                    tvVisitorPlayer.setEnabled(false);
                                    tvHomePlayer.setEnabled(false);
                                    dowm.setText(getResources().getString(R.string.join_the_match));
                                    dowm.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            boolean isHome = false;
//                                            boolean isVosit = false;
//                                            for (Player player : match.home.players) {
//                                                if (player.id == UserManager.getIns().getUser().id) {
//                                                    isHome = true;
//                                                }
//                                            }
//                                            for (Player player : match.visiting.players) {
//                                                if (player.id == UserManager.getIns().getUser().id) {
//                                                    isVosit = true;
//                                                }
//                                            }
//                                            if (isHome && isVosit) {
//                                                myTeam();
//                                                popDialog();
//                                            } else {
//                                                singUp();
//                                            }
                                            myTeam();
                                        }
                                    });
                                }
                            } else {
                                getCustomActionBar().setTitleText(R.string.middle_match);
                                judgeRecord(match);
                            }
                        }


                    }


                    ImageLoader.loadCicleImage(TeamDetailsSignUpActivity.this, match.home.logo, R.mipmap.default_team, teamDetailsSignHomeHeader);
                    teamDetailsSignHomeName.setText(match.home.name);
                    teamDetailsSignHomeCombat.setText(getResources().getString(R.string.combat) + match.home.stars);
                    teamDetailsSignHomeAge.setText(getResources().getString(R.string.age_sign) + match.home.age);

                    if (match.status == 10) {
                        teamDetailsSignMatchTime.setText(match.date);
                        teamDetailsSignMatchTime.setTextSize(15);
                        stage.setText(getResources().getString(R.string.before_match_));

                    } else {
                        teamDetailsSignMatchTime.setText(match.homescore + " : " + match.visitingscore);
                        stage.setText(getResources().getString(R.string.matching_));
                    }

                    teamDetailsSignMatchVenue.setText(match.addr);
                    teamDetailsSignMatchSystem.setText(match.type + getResources().getString(R.string.system));

                    ImageLoader.loadCicleImage(TeamDetailsSignUpActivity.this, match.visiting.logo, R.mipmap.default_team, teamDetailsSignVisitHeader);
                    teamDetailsSignVisitName.setText(match.visiting.name);
                    teamDetailsSignVisitCombat.setText(getResources().getString(R.string.combat) + match.visiting.stars);
                    teamDetailsSignVisitAge.setText(getResources().getString(R.string.age_sign) + match.visiting.age);


                    if (response.getData().homeusers == null || response.getData().homeusers.size() == 0) {
                        tvHomePlayer.setVisibility(View.GONE);

                    } else {
                        tvHomePlayer.setVisibility(View.VISIBLE);

                        TeamDetailGridAdapter teamDetailGridAdapter = new TeamDetailGridAdapter(TeamDetailsSignUpActivity.this, match.homeusers, match.home.leader);

                        mHomeGrid.setAdapter(teamDetailGridAdapter);

//                        for (final Player player : match.homeusers) {
//                            View view = getLayoutInflater().inflate(R.layout.team_details_player_item_final, null);
//                            ImageView player_header = (ImageView) view.findViewById(R.id.player_header);
//                            TextView number = (TextView) view.findViewById(R.id.number);
//                            TextView name = (TextView) view.findViewById(R.id.team_details_player_item_name);
//                            name.setText(player.name);
//                            number.setText(player.number + "");
//                            iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
//                            ImageLoader.loadCicleImage(TeamDetailsSignUpActivity.this, player.avatar, R.mipmap.default_person, player_header);
//                            if (match.home.leader == player.id) {
//                                iv_leader.setVisibility(View.VISIBLE);
//                            }
//                            llOtherTeamDetailsHeaderOfPlayerHome.addView(view);
//                            view.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    Bundle bundle = new Bundle();
//                                    bundle.putInt("playerId", player.id);
//                                    startActivity(PlayerPersonalActivity.class, bundle);
//                                }
//                            });
//                        }

                    }


                    if (response.getData().visitingusers == null || response.getData().visitingusers.size() == 0) {
                        tvVisitorPlayer.setVisibility(View.GONE);
                    } else {
                        tvVisitorPlayer.setVisibility(View.VISIBLE);

                        TeamDetailGridAdapter teamDetailGridAdapter = new TeamDetailGridAdapter(TeamDetailsSignUpActivity.this, match.visitingusers, match.visiting.leader);

                        mVisitingGrid.setAdapter(teamDetailGridAdapter);

//                        for (final Player player : match.visitingusers) {
//                            View view = getLayoutInflater().inflate(R.layout.team_details_player_item_final, null);
//                            iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
//                            TextView number = (TextView) view.findViewById(R.id.number);
//                            TextView name = (TextView) view.findViewById(R.id.team_details_player_item_name);
//                            name.setText(player.name);
//                            number.setText(player.number + "");
//                            ImageView player_header = (ImageView) view.findViewById(R.id.player_header);
//                            ImageLoader.loadCicleImage(TeamDetailsSignUpActivity.this, player.avatar, R.mipmap.default_person, player_header);
//                            if (match.visiting.leader == player.id) {
//                                iv_leader.setVisibility(View.VISIBLE);
//                            }
//                            llOtherTeamDetailsHeaderOfPlayerVisit.addView(view);
//                            view.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    Bundle bundle = new Bundle();
//                                    bundle.putInt("playerId", player.id);
//                                    startActivity(PlayerPersonalActivity.class, bundle);
//                                }
//                            });
//                        }

                    }


                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }

            }

            @Override
            public void onFinish() {

            }
        });
    }

    //获取自己所属球队，并取出自己创建的球队
    private void myTeam() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        if (UserManager.getIns().getUser() != null) {
            params.put("userId", UserManager.getIns().getUser().id);
        }
        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    teams = response.getData().teams;
                    teams1 = new ArrayList<Team>();
                    boolean isHome = false;
                    boolean isVosit = false;
                    for (Team team : teams) {
                        if (team.id == match.home.id) {
                            teamId = match.home.id;
                            isHome = true;
                            teams1.add(team);
                        }
                    }
                    for (Team team : teams) {
                        if (team.id == match.visiting.id) {
                            teamId = match.visiting.id;
                            isVosit = true;
                            teams1.add(team);
                        }
                    }
                    if (isHome && isVosit) {

                        popDialog();
                    } else {
                        singUp();
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    private void popDialog() {
        if (bottomPupDialog == null) {
            bottomPupDialog = new BottomPupChooseTeamDialog(TeamDetailsSignUpActivity.this, R.style.callDialog, 1, teams1);
            bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
            bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            bottomPupDialog.setConfirmClickListener(new BottomPupChooseTeamDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(Team team) {
                    teamId = team.id;
                    singUp();
                }
            });
        }
        bottomPupDialog.show();

        WindowManager windowManager1 = TeamDetailsSignUpActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        bottomPupDialog.getWindow().setAttributes(lp1);
    }

    private void judgeRecord(final Match match) {

        if (match.isrecord) {
            if (match.homerecord == 0) {
                judgeScore(teamId, match);
                return;
            }
            if (UserManager.getIns().getUser().id == match.homerecord) {
                teamId = match.home.id;
                judgeScore(teamId, match);
                return;
            }


            if (match.visitingrecord == 0) {
                judgeScore(teamId, match);
                return;
            }
            if (UserManager.getIns().getUser().id == match.visitingrecord) {
                teamId = match.visiting.id;
                judgeScore(teamId, match);
                return;
            }

        } else {
            if (match.homeusers == null) {
                judgeScore(teamId, match);
                return;
            }
            for (Player player : match.homeusers) {

                if (UserManager.getIns().getUser().id == player.id) {
                    teamId = match.home.id;
                    judgeScore(teamId, match);
                    return;
                }

            }
            if (match.visitingusers == null) {
                judgeScore(teamId, match);
                return;
            }
            for (Player player : match.visitingusers) {
                if (UserManager.getIns().getUser().id == player.id) {
                    teamId = match.visiting.id;
                    judgeScore(teamId, match);
                    return;
                }

            }

        }
        judgeScore(teamId, match);
        Log.i("teamId", teamId + "");
    }

    private void judgeScore(final int teamId, final Match match) {
        if (teamId == 0) {
            up.setVisibility(View.GONE);
            dowm.setVisibility(View.VISIBLE);
            tvVisitorPlayer.setEnabled(false);
            tvHomePlayer.setEnabled(false);
            dowm.setText(getResources().getString(R.string.interactive_live));
            dowm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TeamDetailsSignUpActivity.this, InteractiveLiveActivity.class);
                    intent.putExtra("matchId", match.id);
                    startActivity(intent);
//                                            startActivity(MatchScoreActivity.class);
                }
            });
        } else {
            up.setVisibility(View.VISIBLE);
            dowm.setVisibility(View.GONE);
            matchScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(TeamDetailsSignUpActivity.this, PreRacingActivity.class);
//                    intent.putExtra("matchId", match.id);
//                    intent.putExtra("teamId", teamId);
//
//                    startActivity(intent);

                    if (match.status != 100) {
                      isConfirmFirst(teamId,match);

//                        isStarting(teamId, match);
                    }

                }
            });
            interactiveLive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TeamDetailsSignUpActivity.this, InteractiveLiveActivity.class);
                    intent.putExtra("matchId", match.id);

                    startActivity(intent);
                }
            });

        }
    }

    private void isStarting(final int teamId, final Match match) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();

//
//                    if (game.statring.size() > 0 && game.statring != null) {
//                        Intent intent = new Intent(TeamDetailsSignUpActivity.this, MatchScoreActivity.class);
//                        intent.putExtra("matchId", match.id);
//                        intent.putExtra("teamId", teamId);
//                        intent.putExtra("game", game);
//                        startActivity(intent);
//                    } else {
//
//                    }


                    Intent intent = new Intent(TeamDetailsSignUpActivity.this, PreRacingActivity.class);
                    intent.putExtra("matchId", match.id);
                    intent.putExtra("teamId", teamId);
                    intent.putExtra("game", game);
                    intent.putExtra("type", game.type);
//                    if (game.homeStarting.size() > 0 && game.homeStarting != null) {
//                        intent.putExtra("isSureFirst", true);
//                    } else {
//                        intent.putExtra("isSureFirst", false);
//                    }
//
//
//                    if (game.visitingStarting.size() > 0 && game.visitingStarting != null) {
//                        intent.putExtra("isSureFirstVisit", true);
//                    } else {
//                        intent.putExtra("isSureFirstVisit", false);
//                    }
                    startActivity(intent);
                    finish();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    private void isConfirmFirst(final int teamId, final Match match) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();

//
//                    if (game.statring.size() > 0 && game.statring != null) {
//                        Intent intent = new Intent(TeamDetailsSignUpActivity.this, MatchScoreActivity.class);
//                        intent.putExtra("matchId", match.id);
//                        intent.putExtra("teamId", teamId);
//                        intent.putExtra("game", game);
//                        startActivity(intent);
//                    } else {
//
//                    }

                    if (game.homeStarting.size()==0||game.visitingStarting.size()==0){
                        Intent intent1 = new Intent(TeamDetailsSignUpActivity.this, ConfirmFirstActivity.class);
                        intent1.putExtra("playerList", match.homeusers);
                        intent1.putExtra("playerListForVisit", match.visitingusers);

                        intent1.putExtra("teamId", match.home.id);
                        intent1.putExtra("teamIdForVisit",match.visiting.id);


                        intent1.putExtra("matchId", matchId);
                        intent1.putExtra("match",match);
                        startActivity(intent1);

                    }else {
                        Intent intent = new Intent(TeamDetailsSignUpActivity.this, BasketBallMatchScoreActivity.class);
                        intent.putExtra("matchId",matchId);
                        intent.putExtra("teamId",teamId);
                        intent.putExtra("match",match);
                        startActivity(intent);
                    }


                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void singUp() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().signUpToMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    queryMatchDetials();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void cacelSingUp() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().cancelSignUpToMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    queryMatchDetials();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeViews() {
        teamDetailsSignHomeHeader.setOnClickListener(this);
        teamDetailsSignVisitHeader.setOnClickListener(this);
        tvHomePlayer.setOnClickListener(this);
        tvVisitorPlayer.setOnClickListener(this);
//        contactOpponentCaptain.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {
        isScorer = getIntent().getIntExtra("isScorer", 0);
        if (isScorer == 1) {
//            contactOpponentCaptain.setText(R.string.match_score);
//            signUp.setText(R.string.interactive_live);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.contact_opponent_captain:
//                if (isScorer == 1) {
//
//                } else {
//                    if (dialog == null) {
//                        dialog = new ContactOpponentCaptainDialog(TeamDetailsSignUpActivity.this, R.style.callDialog, 0);
//                    }
//                    dialog.show();
//                }
//
//                break;
            case R.id.team_details_sign_home_header:
                Intent intent = new Intent(TeamDetailsSignUpActivity.this, OtherTeamDetailsActivity.class);
                intent.putExtra("teamId", match.home.id);
                startActivity(intent);
                break;
            case R.id.team_details_sign_visit_header:
                Intent intent1 = new Intent(TeamDetailsSignUpActivity.this, OtherTeamDetailsActivity.class);
                intent1.putExtra("teamId", match.visiting.id);
                startActivity(intent1);
                break;

            case R.id.tv_home_player:
                isStarting(match.home.id,match);

                break;
            case R.id.tv_visitor_player:
                isStarting(match.visiting.id,match);

                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.before_match);

    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
