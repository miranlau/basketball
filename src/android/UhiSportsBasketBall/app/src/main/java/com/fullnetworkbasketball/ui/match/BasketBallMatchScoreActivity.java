package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.ActionListAdapter;
import com.fullnetworkbasketball.ui.adapter.MatchScoreTeamGridAdapter;
import com.fullnetworkbasketball.ui.dialog.BallStateDialog;
import com.fullnetworkbasketball.ui.dialog.ChooseBasketBallMatchStateDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.home.HomeActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.VibrateHelp;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/26 0026.
 */
public class BasketBallMatchScoreActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.abs_layout)
    AbsoluteLayout absLayout;
    @Bind(R.id.ll_change_team_match_score)
    LinearLayout llChangeTeamMatchScore;
    @Bind(R.id.court_left)
    View courtLeft;
    @Bind(R.id.team_score_right_grid)
    GridView teamScoreRightGrid;
    @Bind(R.id.min)
    TextView min;
    @Bind(R.id.sec)
    TextView sec;
    @Bind(R.id.court_right)
    View courtRight;
    @Bind(R.id.team_score_left_grid)
    GridView teamScoreLeftGrid;
    @Bind(R.id.ll_home_team)
    LinearLayout llHomeTeam;
    @Bind(R.id.ready_go)
    TextView readyGo;
    @Bind(R.id.ll_visit_team)
    LinearLayout llVisitTeam;
    private final int VIBRATE_TIME = 500;
    @Bind(R.id.home_logo)
    ImageView homeLogo;
    @Bind(R.id.home_name)
    TextView homeName;
    @Bind(R.id.visit_log)
    ImageView visitLog;
    @Bind(R.id.visit_name)
    TextView visitName;
    @Bind(R.id.tv_pause)
    TextView tvPause;
    @Bind(R.id.tv_end)
    TextView tvEnd;
    @Bind(R.id.which_section)
    TextView whichSection;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;


    private int matchId;
    private int teamId;
    private int actionType;
    private double perX;
    private int perY;
    private ArrayList<Display> displays;
    private int intWidth;
    private int intHeight;
    private Display display1;
    private ArrayList<FrameLayout> frameLayouts;
    private ArrayList<FrameLayout> frameLayoutsRight;
    private int playerId;
    private ContactOpponentCaptainDialog dialog;
    private ArrayList<Display> homeStartings;
    private ArrayList<Display> visitStartings;

    private MatchScoreTeamGridAdapter adapterRight;

    private Game game;

    private BallStateDialog ballStateDialog;
    private Match match;
    private ArrayList<String> actionLists = new ArrayList<>();
    private boolean isHome = true;

    //11 比赛开始
    //22 换人
    //31 罚球(进)
    //32 罚球(丢)
    //33 2分(命中)
    //34 2分(不中)
    //35 3分(命中)
    //36 3分(不中)
    //37 助攻
    //41 篮板
    //42 盖帽
    //43 抢断
    //44 失误
    //45 犯规
    //55 暂停
    //56 继续
    //61 下一节开始
    //62 本节结束
    //63 加时开始
    //64 加时结束
    //120 撤销

    private ActionListAdapter actionListAdapter;
    private int homeTeamId;
    private int visitTeamId;
    private boolean isPaused = false;
    private int timeUsedInsec;
    private String timeUsed;
    private Handler uiHandle = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (!isPaused) {
                        min.setTextColor(Color.WHITE);
                        sec.setTextColor(Color.WHITE);
                        addTimeUsed();
                        updateClockUI();
                    } else {
                        min.setTextColor(Color.RED);
                        sec.setTextColor(Color.RED);
//                        cannotOperate();
                    }
                    uiHandle.sendEmptyMessageDelayed(1, 1000);
                    break;
                default:
                    break;
            }
        }
    };
    private int click = 1;
    private boolean isOT = false;
    private ContactOpponentCaptainDialog dialogEnd;
    private ChooseBasketBallMatchStateDialog dialogOT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basketball_match_score_activity);
        matchId = getIntent().getIntExtra("matchId", 0);
        match = (Match) getIntent().getSerializableExtra("match");
//        teamId = getIntent().getIntExtra("teamId", 0);
//        displays = (ArrayList<Display>) getIntent().getSerializableExtra("playerList");

//        actionType = getIntent().getIntExtra("actionType", 0);
        ButterKnife.bind(this);
        tvPause.setOnClickListener(this);
        tvEnd.setOnClickListener(this);
        initView();
//        onLineData();
    }

    private void resetTimerState() {
        if (game.isStop) {
            click = 1;
        } else {
            click = 2;
        }


        uiHandle.removeMessages(1);
        startTime();
        if (click == 1) {
            updateClockUI();
            isPaused = true;
        } else {
            isPaused = false;
        }

    }

    private void chageState(final int actionType) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", teamId);
        params.put("status", actionType);
        Api.getRetrofit().basketballMatchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.code == 3005) {
                    onLineData();
                    return;
                }

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void refreshView() {
        if (actionLists!=null&&actionLists.size()>0){
            try{
                actionListAdapter.setNewData(actionLists);
            }catch (Exception NullPointerException){
                Log.i("tag","空指针异常");
            }
        }
        readyGo.setText(game.homeScore + " : " + game.visitingScore);
        if (game.isSuspend) {
            tvPause.setText(getResources().getString(R.string.keep));
        } else {
            tvPause.setText(getResources().getString(R.string.pause));
        }
        if (actionType == 61 || actionType == 63) {
            timeUsedInsec = 0;
        }

        if (game.status > 20 && game.status < 30) {
            isOT = true;
            whichSection.setText("加时" + (game.status - 20));
            timeUsedInsec = game.duration;
            tvEnd.setText("结束");
            resetTimerState();
            return;
        }

        switch (game.status) {
            //第一节比赛进行中
            case 11:
                whichSection.setText("第1节");
                timeUsedInsec = game.duration;
                tvEnd.setText("结束");
                resetTimerState();
                break;
            //第二节比赛进行中
            case 12:
                whichSection.setText("第2节");
//                timeUsedInsec = 0;
                timeUsedInsec = game.duration;
                tvEnd.setText("结束");
                resetTimerState();

                break;
            //第三节比赛进行中
            case 13:
                whichSection.setText("第3节");
//                timeUsedInsec = game.duration;
                timeUsedInsec = game.duration;
                tvEnd.setText("结束");
                resetTimerState();
                break;
            //第四节比赛进行中
            case 14:
                whichSection.setText("第4节");
//                timeUsedInsec = game.duration;
                timeUsedInsec = game.duration;
                tvEnd.setText("结束");
                resetTimerState();
                break;
            case 20:
                isOT = true;
                isPaused = true;
                whichSection.setText("加时赛");
                timeUsedInsec = 0;
                tvEnd.setText("开始");
                resetTimerState();
                break;
//            case 21:
//                isOT = true;
//                whichSection.setText("加时1");
////                timeUsedInsec = game.duration;
//                tvEnd.setText("结束");
//                resetTimerState();
//                break;
//            case 22:
//                isOT = true;
//                whichSection.setText("加时2");
////                timeUsedInsec = game.duration;
//                tvEnd.setText("结束");
//                resetTimerState();
//
//
//                break;
//            case 23:
//                isOT = true;
//                whichSection.setText("加时3");
////                timeUsedInsec = game.duration;
//                tvEnd.setText("结束");
//                resetTimerState();
//                break;
//            case 24:
//                isOT = true;
//                whichSection.setText("加时3");
////                timeUsedInsec = game.duration;
//                tvEnd.setText("结束");
//                resetTimerState();
//                break;
            case 30:
                timeUsedInsec = 0;
                isPaused = true;
                tvEnd.setText("下一节");

                resetTimerState();
                break;
            case 40:
                isOT = true;
                timeUsedInsec = 0;
                isPaused = true;
                tvEnd.setText("下一节");

                resetTimerState();
                break;
            case 100:
//                timeUsedInsec=0;
//                isPaused=true;
//                whichStateOfMatch.setText(getResources().getString(R.string.penalty_shootout));
                T.showShort(BasketBallMatchScoreActivity.this, "比赛已经结束！");
                Intent intent = new Intent(BasketBallMatchScoreActivity.this, HomeActivity.class);
                startActivity(intent);
                SharePreHelper.getIns().putIsCreateMatch(true);
                finish();
                break;
        }

    }

    private void resetState() {
        llVisitTeam.setSelected(false);
        llHomeTeam.setSelected(false);
    }

    private void startTime() {
        uiHandle.sendEmptyMessageDelayed(1, 1000);
    }

    public void addTimeUsed() {
        timeUsedInsec = timeUsedInsec + 1;
        timeUsed = this.getMin() + ":" + this.getSec();
    }

    public CharSequence getMin() {
        int sec = timeUsedInsec / 60;
        return sec < 10 ? "0" + sec : String.valueOf(sec);
    }

    public CharSequence getSec() {
        int sec = timeUsedInsec % 60;
        return sec < 10 ? "0" + sec : String.valueOf(sec);
    }

    /**
     * 更新时间的显示
     */
    private void updateClockUI() {
        min.setText(getMin() + "'");
        sec.setText(getSec() + "''");
    }

    private void onLineData() {

        homeName.setText(match.home.name);
        visitName.setText(match.visiting.name);
        ImageLoader.loadCicleImage(this, match.home.logo, R.mipmap.default_team, homeLogo);
        ImageLoader.loadCicleImage(this, match.visiting.logo, R.mipmap.default_team, visitLog);
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {

            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();
//                    players_down = game.players;
//                    initializeViews();
                    homeStartings = game.homeStarting;
                    visitStartings = game.visitingStarting;
                    homeTeamId = game.home.id;
                    visitTeamId = game.visiting.id;
                    actionLists = game.actions;

                    if (game.actions == null) {
                        actionLists = new ArrayList<String>();
                    }
                    refreshView();

                    if (actionType == 120) {
                        if (isHome == true) {
                            resetState();
                            clearRightPlayer();
                            llHomeTeam.setSelected(true);
                            courtRight.setVisibility(View.GONE);
                            courtLeft.setVisibility(View.VISIBLE);
                            teamScoreRightGrid.setVisibility(View.VISIBLE);
                            teamScoreLeftGrid.setVisibility(View.GONE);
                            teamScoreRightGrid.setAdapter(adapterRight);
                            getLineUp(homeStartings);
                            teamId = homeTeamId;
                            playerId = 0;
                        } else if (isHome == false) {
                            resetState();
                            clearLeftPlayer();
                            llVisitTeam.setSelected(true);
                            courtRight.setVisibility(View.VISIBLE);
                            courtLeft.setVisibility(View.GONE);
                            teamScoreRightGrid.setVisibility(View.GONE);
                            teamScoreLeftGrid.setVisibility(View.VISIBLE);
                            teamScoreLeftGrid.setAdapter(adapterRight);
                            getLineUpRight(visitStartings);
                            teamId = visitTeamId;
                            playerId = 0;
                        }

                    }
                }
                }

            @Override
            public void onFinish() {



            }
        });
    }

    private void popUpBallStateDialog(final int action) {
//        if (ballStateDialog == null) {
        ballStateDialog = new BallStateDialog(BasketBallMatchScoreActivity.this, R.style.callDialog);
        ballStateDialog.setConfirmClickListener(new BallStateDialog.OnConfirmOrderListener() {
            @Override
            public void onConfirmOrder(int i) {
                if (i == 1) {
                    if (action == 0) {
                        matchScore(35);
                    } else if (action == 1) {
                        matchScore(33);
                    } else if (action == 2) {
                        matchScore(31);
                    }


                } else if (i == 2) {
                    if (action == 0) {
                        matchScore(36);
                    } else if (action == 1) {
                        matchScore(34);
                    } else if (action == 2) {
                        matchScore(32);
                    }
                } else if (i == 3) {

                }
            }
        });
//        }
        ballStateDialog.show();

    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        llHomeTeam.setOnClickListener(this);
        llVisitTeam.setOnClickListener(this);
        llHomeTeam.setSelected(true);

        courtRight.setVisibility(View.GONE);
        teamScoreRightGrid.setVisibility(View.VISIBLE);
        adapterRight = new MatchScoreTeamGridAdapter(this);
        teamScoreRightGrid.setAdapter(adapterRight);
//        readyGo.setText(game.homeScore + " : " + game.visitingScore);
         /* 设置图片的宽高 */
        intWidth = ScreenUtils.dp2px(this, 70);
        intHeight = ScreenUtils.dp2px(this, 70);
        perX = App.getInst().getGridLengh();
        perY = (ScreenUtils.dp2px(BasketBallMatchScoreActivity.this, 300)) / 25;

        frameLayouts = new ArrayList<>();
        frameLayoutsRight = new ArrayList<>();

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();
//                    players_down = game.players;
//                    initializeViews();
                    homeStartings = game.homeStarting;
                    visitStartings = game.visitingStarting;
                    homeTeamId = game.home.id;
                    visitTeamId = game.visiting.id;
                    getLineUp(homeStartings);
                    actionListAdapter = new ActionListAdapter(R.layout.action_list_item_layout,game.actions);
                    recyclerView.setAdapter(actionListAdapter);
                    refreshView();
                    teamId = homeTeamId;

                }
            }

            @Override
            public void onFinish() {

            }
        });


        teamScoreLeftGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (playerId == 0 && i != 9&&i!=10) {
                    T.showLong(BasketBallMatchScoreActivity.this, "请选择客队球员!");
                    return;
                }
                VibrateHelp.vSimple(adapterView.getContext(), VIBRATE_TIME);
                switch (i) {
                    //客队3分
                    case 0:
                        popUpBallStateDialog(0);
                        break;
                    //客队2分
                    case 1:
                        popUpBallStateDialog(1);
                        break;
                    //客队罚球
                    case 2:
                        popUpBallStateDialog(2);
                        break;
                    //客队篮板
                    case 3:
                        matchScore(41);
                        break;
                    //客队盖帽
                    case 4:
                        matchScore(42);
                        break;
                    //客队助攻
                    case 5:
                        matchScore(37);
                        break;
                    //客队抢断
                    case 6:
                        matchScore(43);
                        break;
                    //客队失误
                    case 7:
                        matchScore(44);
                        break;
                    //客队犯规
                    case 8:
                        matchScore(45);
                        break;
                    //客队换人
                    case 9:
                        Intent intent_change = new Intent(BasketBallMatchScoreActivity.this, BasketBallChangePeopleActivity.class);
                        intent_change.putExtra("matchId", matchId);
                        startActivityForResult(intent_change, 10010);
                        break;
                    //客队撤销
                    case 10:
                        matchScore(120);
                        isHome = false;
                        break;

                }
            }
        });
        teamScoreRightGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (playerId == 0 && i != 9&&i!=10) {
                    T.showLong(BasketBallMatchScoreActivity.this, "请选择主队球员!");
                    return;
                }
                VibrateHelp.vSimple(adapterView.getContext(), VIBRATE_TIME);
                switch (i) {
                    //主队3分
                    case 0:
                        popUpBallStateDialog(0);
                        break;
                    //主队2分
                    case 1:
                        popUpBallStateDialog(1);
                        break;
                    //主队罚球
                    case 2:
                        popUpBallStateDialog(2);
                        break;
                    //主队篮板
                    case 3:
                        matchScore(41);
                        break;
                    //主队盖帽
                    case 4:
                        matchScore(42);
                        break;
                    //主队助攻
                    case 5:
                        matchScore(37);
                        break;
                    //主队抢断
                    case 6:
                        matchScore(43);
                        break;
                    //主队失误
                    case 7:
                        matchScore(44);
                        break;
                    //主队犯规
                    case 8:
                        matchScore(45);
                        break;
                    //主队换人
                    case 9:
                        Intent intent_change = new Intent(BasketBallMatchScoreActivity.this, BasketBallChangePeopleActivity.class);
                        intent_change.putExtra("matchId", matchId);
                        startActivityForResult(intent_change, 10010);
//                        matchScore(22);
                        break;
                    //主队撤销
                    case 10:
                        matchScore(120);
                        isHome = true;
                        break;

                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 10010:
//                    initView();
                    HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                    params.put("matchId", matchId);
                    Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
                        @Override
                        public void onSuccess(HttpResponseFWF2<Game> response) {
                            Logger.i("msg:" + response.toString());

                            if (response.isSuccess()) {

                                game = response.getData();
                                homeStartings = game.homeStarting;
                                visitStartings = game.visitingStarting;

                                if (llHomeTeam.isSelected()) {
                                    getLineUp(homeStartings);
                                } else {
                                    getLineUpRight(visitStartings);
                                }
                                refreshView();
                            }
                        }

                        @Override
                        public void onFinish() {

                        }
                    });

                    break;
                case 10020:
                    initView();
                    break;
            }
        }
    }

    private void getLineUp(ArrayList<Display> displays) {
        clearLeftPlayer();

        for (Display display : displays) {
            final FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(BasketBallMatchScoreActivity.this).inflate(R.layout.team_details_player_item_three, null);
            ImageView header = (ImageView) frameLayout.findViewById(R.id.player_header);
            TextView number = (TextView) frameLayout.findViewById(R.id.number);
            final ImageView choose = (ImageView) frameLayout.findViewById(R.id.iv_choose);
            ImageLoader.loadCicleImage(BasketBallMatchScoreActivity.this, display.avatar, R.mipmap.default_person, header);
            number.setText(display.number + "");
            absLayout.addView(frameLayout);
            frameLayout.setTag(display);
            frameLayouts.add(frameLayout);
            frameLayout.setLayoutParams
                    (
                            new AbsoluteLayout.LayoutParams
                                    (intWidth, intHeight, ((int) ((display.x) * perX)) - ScreenUtils.dp2px(BasketBallMatchScoreActivity.this, 30), ((int) ((display.y) * perX)) + App.getInst().getStatusBarHeight() + ScreenUtils.dp2px(BasketBallMatchScoreActivity.this, 10))

                    );
            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    for (FrameLayout frameLayout1 : frameLayouts) {
                        ImageView imageView = (ImageView) frameLayout1.findViewById(R.id.iv_choose);
                        imageView.setVisibility(View.GONE);
                    }
                    if (playerId ==((Display) frameLayout.getTag()).id){
                        playerId = 0;
                    }

                    else {
                        choose.setVisibility(View.VISIBLE);
                        playerId = ((Display) frameLayout.getTag()).id;
                    }

                }
            });
        }

    }
    private void getLineUpRight(ArrayList<Display> displays) {
        clearRightPlayer();

        for (Display display : displays) {
            display.x = display.x + App.getInst().rightCancelGrid();
            final FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(BasketBallMatchScoreActivity.this).inflate(R.layout.team_details_player_item_three, null);
            ImageView header = (ImageView) frameLayout.findViewById(R.id.player_header);
            TextView number = (TextView) frameLayout.findViewById(R.id.number);
            final ImageView choose = (ImageView) frameLayout.findViewById(R.id.iv_choose);
            ImageLoader.loadCicleImage(BasketBallMatchScoreActivity.this, display.avatar, R.mipmap.default_person, header);
            number.setText(display.number + "");
            absLayout.addView(frameLayout);
            frameLayout.setTag(display);
            frameLayoutsRight.add(frameLayout);
            frameLayout.setLayoutParams
                    (
                            new AbsoluteLayout.LayoutParams
                                    (intWidth, intHeight, ((int) ((display.x) * perX)) - ScreenUtils.dp2px(BasketBallMatchScoreActivity.this, 40), ((int) ((display.y) * perX)) + App.getInst().getStatusBarHeight() + ScreenUtils.dp2px(BasketBallMatchScoreActivity.this, 10))

                    );
            frameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    for (FrameLayout frameLayout1 : frameLayoutsRight) {
                        ImageView imageView = (ImageView) frameLayout1.findViewById(R.id.iv_choose);
                        imageView.setVisibility(View.GONE);
                    }
                    if (playerId == ((Display) frameLayout.getTag()).id) {
                        playerId = 0;
                    } else {
                        choose.setVisibility(View.VISIBLE);
                        playerId = ((Display) frameLayout.getTag()).id;
                    }

                }
            });
            display.x = display.x - App.getInst().rightCancelGrid();
        }


    }

    public void clearLeftPlayer() {
        for (FrameLayout frameLayout2 : frameLayouts) {
            absLayout.removeView(frameLayout2);
        }
    }

    public void clearRightPlayer() {
        for (FrameLayout frameLayout1 : frameLayoutsRight) {
            absLayout.removeView(frameLayout1);
        }
    }

    private void matchScore(final int actionType1) {
        final HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("status", actionType1);
        params.put("teamId", teamId);
        params.put("player", playerId);
        Api.getRetrofit().basketballMatchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    actionType = actionType1;
                    onLineData();
//                    initializeViews();
                    switch (actionType1) {

                        case 11:
                            isPaused = false;

                            uiHandle.removeMessages(1);

                            startTime();

                            T.showShort(getApplicationContext(), getResources().getString(R.string.start_match));

                            break;
                        case 100:

                            T.showShort(BasketBallMatchScoreActivity.this, "比赛结束！");
                            Intent intent = new Intent(BasketBallMatchScoreActivity.this, HomeActivity.class);
                            startActivity(intent);
                            SharePreHelper.getIns().putIsCreateMatch(true);
                            finish();
                            break;
                    }

                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        uiHandle.removeMessages(1);
        isPaused = true;
//        SharePreHelper.getIns().setTime(timeUsedInsec);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isPaused = false;
//        timeUsedInsec = SharePreHelper.getIns().getCourtTime();
        onLineData();


    }

    //比赛状态
    //0 未开始
    //10 常规时间
    //20 加时
    //30 节间休息

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.match_score_left_sure:
//                matchScore();
                break;
            case R.id.match_score_left_cancel:
                finish();
                break;
            case R.id.ll_home_team:
                resetState();
                clearRightPlayer();
                llHomeTeam.setSelected(true);
                courtRight.setVisibility(View.GONE);
                courtLeft.setVisibility(View.VISIBLE);
                teamScoreRightGrid.setVisibility(View.VISIBLE);
                teamScoreLeftGrid.setVisibility(View.GONE);
                teamScoreRightGrid.setAdapter(adapterRight);
                getLineUp(homeStartings);
                teamId = homeTeamId;
                playerId = 0;
                break;
            case R.id.ll_visit_team:
                resetState();
                clearLeftPlayer();
                llVisitTeam.setSelected(true);
                courtRight.setVisibility(View.VISIBLE);
                courtLeft.setVisibility(View.GONE);
                teamScoreRightGrid.setVisibility(View.GONE);
                teamScoreLeftGrid.setVisibility(View.VISIBLE);
                teamScoreLeftGrid.setAdapter(adapterRight);
                getLineUpRight(visitStartings);
                teamId = visitTeamId;
                playerId = 0;
                break;
            case R.id.tv_pause:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                if (game.isStop) {
//                    click = 2;
//                    chageState();
                    matchScore(56);
//                    timeUsedInsec = SharePreHelper.getIns().getCourtTime();
                } else {
//                    isPaused = false;
//                    click = 1;
//                    tvPause.setText(getResources().getString(R.string.pause));
                    matchScore(55);
//                    SharePreHelper.getIns().setTime(timeUsedInsec);
                }
                break;
            case R.id.tv_end:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                if (game.status == 30) {
                    matchScore(61);
              //      Toast.makeText(this,"game.status == 30",Toast.LENGTH_SHORT).show();
                    return;
                } else if (game.status == 40) {
                    dialogOT = new ChooseBasketBallMatchStateDialog(BasketBallMatchScoreActivity.this, R.style.callDialog, 3);
                    dialogOT.setConfirmClickListener(new ChooseBasketBallMatchStateDialog.OnConfirmOrderListener() {
                        @Override
                        public void onConfirmOrder(int i) {
                            //下一节
                            if (i == 1) {
                                matchScore(63);
                                return;
                            }
                            //取消
                            else if (i == 2) {
                                dialogOT.dismiss();
                                return;
                            }
                        }
                    });
                    dialogOT.show();
                    return;
                } else if (game.status == 20) {
            //        Toast.makeText(this,"game.status == 20",Toast.LENGTH_SHORT).show();
                    matchScore(63);
                    return;
                }
                if (game.homeScore == game.visitingScore) {
                    if (!isOT) {
                        dialogOT = new ChooseBasketBallMatchStateDialog(BasketBallMatchScoreActivity.this, R.style.callDialog, 1);
                        dialogOT.setConfirmClickListener(new ChooseBasketBallMatchStateDialog.OnConfirmOrderListener() {
                            @Override
                            public void onConfirmOrder(int i) {
                                //加时赛
                                if (i == 1) {
                                    matchScore(65);
                                }
                                //本节结束
                                else if (i == 2) {
                                    matchScore(62);
                                }
                            }
                        });
                        dialogOT.show();
                    } else {
                        //结束比赛调用
                        dialogOT = new ChooseBasketBallMatchStateDialog(BasketBallMatchScoreActivity.this, R.style.callDialog, 4);
                        dialogOT.setConfirmClickListener(new ChooseBasketBallMatchStateDialog.OnConfirmOrderListener() {
                            @Override
                            public void onConfirmOrder(int i) {
                                //下一节
                                if (i == 1) {
                                    matchScore(64);
                                }
                                //取消
                                else if (i == 2) {
                                    dialogOT.dismiss();
                                }
                            }
                        });
                        dialogOT.show();

                    }
                } else {
                    if (!isOT) {
                        dialogOT = new ChooseBasketBallMatchStateDialog(BasketBallMatchScoreActivity.this, R.style.callDialog, 2);
                        dialogOT.setConfirmClickListener(new ChooseBasketBallMatchStateDialog.OnConfirmOrderListener() {
                            @Override
                            public void onConfirmOrder(int i) {
                                //本节结束
                                if (i == 1) {
                                    matchScore(62);
                                }
                                //本场结束
                                else if (i == 2) {
                                    popEnd();
                                }
                            }
                        });
                        dialogOT.show();

                    } else {
                 //       Toast.makeText(this,"  popEnd();",Toast.LENGTH_SHORT).show();
                        popEnd();
                    }

                }

                break;
        }
    }

    private void popEnd() {
        if (dialogEnd == null) {
            dialogEnd = new ContactOpponentCaptainDialog(BasketBallMatchScoreActivity.this, R.style.callDialog, 11);
            dialogEnd.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(int i) {
                    if (i == 11) {
                        matchScore(100);
                    }
                }
            });
        }
        dialogEnd.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            adapterRight = new MatchScoreTeamGridAdapter(this);
            finish();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

}
