package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.ChangePlayerAdapter;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/31 0031.
 */
public class ChangePeopleActivity extends AppCompatActivity implements View.OnClickListener{

    @Bind(R.id.sure)
    TextView sure;
    @Bind(R.id.list_view_down_people)
    ListView listViewDownPeople;
    @Bind(R.id.cancel)
    TextView cancel;
    @Bind(R.id.rl_start_player)
    RelativeLayout rlStartPlayer;
    @Bind(R.id.abs_view)
    AbsoluteLayout absView;
    private int matchId;
    private int teamId;
    private int actionType;
    private int perX;
    private int perY;
    private ArrayList<Display> displays;
    private int intWidth;
    private int intHeight;
    private Display display1;
    private ArrayList<FrameLayout> frameLayouts;
    private int playerId;
    private ArrayList<Display> choosePlayer;
    private ChangePlayerAdapter adapter;
    private ArrayList<Player> players;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_change_people);
        players = (ArrayList<Player>)getIntent().getSerializableExtra("players");
        displays = (ArrayList<Display>)getIntent().getSerializableExtra("disPlayer");
        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        actionType = getIntent().getIntExtra("actionType",0);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
         /* 设置图片的宽高 */
        sure.setOnClickListener(this);
        intWidth = ScreenUtils.dp2px(this, 70);
        intHeight = ScreenUtils.dp2px(this, 70);
        perX = (ScreenUtils.dp2px(ChangePeopleActivity.this, 300))/25;
        perY = (ScreenUtils.dp2px(ChangePeopleActivity.this, 300))/25;
        frameLayouts = new ArrayList<>();
        choosePlayer = new ArrayList<>();
        adapter = new ChangePlayerAdapter(this,players);
        listViewDownPeople.setAdapter(adapter);
        cancel.setOnClickListener(this);
        getLineUp();

    }

    private void getLineUp() {

        for (Display display : displays) {
            final FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(ChangePeopleActivity.this).inflate(R.layout.team_details_player_item_three, null);
            ImageView header = (ImageView) frameLayout.findViewById(R.id.player_header);
            TextView number = (TextView) frameLayout.findViewById(R.id.number);
            final ImageView choose = (ImageView) frameLayout.findViewById(R.id.iv_choose);
            ImageLoader.loadCicleImage(ChangePeopleActivity.this, display.avatar, R.mipmap.default_person, header);
            number.setText(display.number + "");
            absView.addView(frameLayout);
            frameLayout.setTag(display);
            frameLayouts.add(frameLayout);
            frameLayout.setLayoutParams
                    (
                            new AbsoluteLayout.LayoutParams
                                    (intWidth, intHeight, ((int) (display.x) )* perX+ScreenUtils.dp2px(this, 10), ((int) (display.y)) * perY+ScreenUtils.dp2px(this, 10))

                    );
            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                                absLayout.removeAllViews();
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);
                    choosePlayer.clear();
                    for (FrameLayout frameLayout1:frameLayouts){
                        ImageView imageView = (ImageView)frameLayout1.findViewById(R.id.iv_choose);
                        imageView.setVisibility(View.GONE);
                    }
                    choose.setVisibility(View.VISIBLE);
                    playerId = ((Display)frameLayout.getTag()).id;
                    choosePlayer.add((Display)frameLayout.getTag());
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);
//                            }
                }
            });
        }


    }





    private void matchScore() {
        if (choosePlayer.size()==0){
            T.showShort(this,getResources().getString(R.string.choose_down_player));
            return;
        }
        if (adapter.getBeSelectedData().size()==0){
            T.showShort(this,getResources().getString(R.string.choose_up_player));
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", teamId);
        params.put("status", actionType);
        params.put("playerA",choosePlayer.get(0).id);
        params.put("playerB",(adapter.getBeSelectedData().get(0)).id) ;
        Api.getRetrofit().matchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    T.showShort(getApplicationContext(), "更换球员成功！");
                    Intent intent = new Intent();
                    intent.putExtra("gameScore","换人");
                    setResult(RESULT_OK,intent);
                     finish();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());

                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sure:
                finish();
                break;
            case R.id.cancel:
                matchScore();

                break;
        }
    }
}
