package com.fullnetworkbasketball.ui.match;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Player;
import com.uhisports.basketball.R;

import net.dr.qwzq.StagePlayer;
import net.dr.qwzq.StagePlayerActivity;
import net.dr.qwzq.StageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/7/4 0004.
 */
public class ChangePeopleImproveActivity extends StagePlayerActivity {
    @Bind(R.id.confirm)
    TextView confirm;
    private ArrayList<Player> players;
    private List<StagePlayer> playersNew;
    //球场
    private StageView mStageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stage_player_activity);
        ButterKnife.bind(this);
        mStageView = (StageView) findViewById(R.id.stage);
        mStageView.setOnRemoveCallbackListener(this);
        players = (ArrayList<Player>) getIntent().getSerializableExtra("playerList");
        playersNew = new ArrayList<>();
        for (int i = 0; i < players.size(); i++) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
            StagePlayer stagePlayer = new StagePlayer(players.get(i).number, mBitmap);
            stagePlayer.setName(players.get(i).name);
            stagePlayer.setRealName(players.get(i).realName);
            stagePlayer.setPicture(players.get(i).avatar);
            playersNew.add(stagePlayer);
        }
        initData(playersNew);
        confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.confirm:
                mStageView.calculateCoordinate();
                commitPlayerLocation(mStageView.getPlayerManager().getPlayerses());
                break;
        }
    }


}
