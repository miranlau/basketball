package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.utils.T;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import net.dr.qwzq.PlayerAdapter;
import net.dr.qwzq.PlayerManager;
import net.dr.qwzq.StageListView;
import net.dr.qwzq.StagePlayer;
import net.dr.qwzq.StageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 球场界面
 */
public class ChangePeopleNewActivity extends AppCompatActivity implements View.OnClickListener, StageListView.OnSelectCallbackListener, StageView.OnChangeCallbackListener ,PlayerAdapter.OnSelectCallbackListener{

    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.sure)
    TextView cancel;
    //球场
    private StageView mStageView;
    //备战区
    private StageListView list;
    //球员数据
    private PlayerAdapter mAdapter;
    private ArrayList<Player> players;
    private int matchId;
    private int teamId;
    private Match match;
    private int system;
    private Game game;
    private ArrayList<String> list_json;
    private ArrayList<Display> displays;
    private PlayerManager playerManager;
    //所有球员
    private List<StagePlayer> mPlayerses = new ArrayList<StagePlayer>();
    private int actionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stage_player_activity);
        ButterKnife.bind(this);
        players = (ArrayList<Player>) getIntent().getSerializableExtra("players");
        displays = (ArrayList<Display>) getIntent().getSerializableExtra("disPlayer");
        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        actionType = getIntent().getIntExtra("actionType", 0);
        mStageView = (StageView) findViewById(R.id.stage);
        mStageView.setOnRemoveCallbackListener(this);
        playerManager = mStageView.getPlayerManager();
        list = (StageListView) findViewById(R.id.list);
        list.setOnSelectCallbackListener(this);
        findViewById(R.id.confirm).setOnClickListener(this);
        cancel.setOnClickListener(this);
        mAdapter = new PlayerAdapter(this);
        list.setAdapter(mAdapter);
        list.setStageView(mStageView);
        getSystem();
        init();
    }

    private void init() {
        List<StagePlayer> players1 = new ArrayList<StagePlayer>();
        final List<StagePlayer> players2 = new ArrayList<StagePlayer>();
        for (Display display : displays) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
            StagePlayer stagePlayer = new StagePlayer(display.number, mBitmap);
            stagePlayer.setName(display.name);
            stagePlayer.setRealName(display.realName);
            stagePlayer.setPicture(display.avatar);
            stagePlayer.id = display.id;
            stagePlayer.setStateIn(true);
            if (display.x>22){
                stagePlayer.setLocationX(22);
            }else {
                stagePlayer.setLocationX((int) display.x);
            }
            if (display.y>22){
                stagePlayer.setLocationY(22);
            }else {
                stagePlayer.setLocationY((int) display.y);
            }

            players2.add(stagePlayer);
        }
        mStageView.setOnViewInitListener(new StageView.OnViewInitListener() {
            @Override
            public void onInited() {
                mStageView.initPlayers(players2);
            }
        });

        for (int i = 0; i < players.size(); i++) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
            StagePlayer stagePlayer = new StagePlayer(players.get(i).number, mBitmap);
            stagePlayer.setName(players.get(i).name);
            stagePlayer.setRealName(players.get(i).realName);
            stagePlayer.setPicture(players.get(i).avatar);
            stagePlayer.id = players.get(i).id;
            players1.add(stagePlayer);
        }
        initData(players1);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                displays = new ArrayList<>();
                mStageView.calculateCoordinate();
                mPlayerses = mStageView.getPlayerManager().getPlayerses();
                for (StagePlayer stagePlayer : mPlayerses) {
                    Display display = new Display();
                    display.avatar = stagePlayer.getPicture();
                    display.name = stagePlayer.getName();
                    display.realName = stagePlayer.getRealName();
                    display.number = stagePlayer.getNumber();
                    display.id = stagePlayer.id;
                    display.x = stagePlayer.getLocationX();
                    display.y = stagePlayer.getLocationY();
                    displays.add(display);
                }
                startPlayer();
                break;
            case R.id.sure:
                finish();
                break;
        }

    }

    @Override
    public void onSelectCallback(StagePlayer player) {
        mStageView.setReadyMovePlayer(player);
    }


    /**
     * 通过网络获得数据,需要重写
     *
     * @param players
     */
    protected void initData(List<StagePlayer> players) {
        mAdapter.refresh(players);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStageView.onDestory();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mStageView.onDestory();
    }

    /**
     * 移动处理之后,提交数据到服务器,需要重写
     *
     * @param players
     */
    protected void commitPlayerLocation(List<StagePlayer> players) {
        for (StagePlayer player : players) {
            Log.i("Main", "location x : " + player.getLocationX() + " y : " + player.getLocationY());
        }
    }

    /**
     * 移动过程中球员变化
     * @param pStageView 球场
     * @param pPlayers   被变化球员
     * @param moveOut
     */
    @Override
    public void onChangeCallback(StageView pStageView, StagePlayer pPlayers, boolean moveOut) {
//        Toast.makeText(ChangePeopleNewActivity.this, "player " + pPlayers.getNumber() + (moveOut ? " out" : " in"), Toast.LENGTH_SHORT).show();
        if (!moveOut) {
            mAdapter.remove(pPlayers);
        } else {
            mAdapter.add(pPlayers);
        }
    }

    private void getSystem() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    match = response.getData();
                    system = match.type;
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    private void startPlayer() {
        if (displays.size() == 0) {
            T.showShort(this, getResources().getString(R.string.start_player));
            return;
        }
        if (displays.size() > system) {
            T.showShort(this, getResources().getString(R.string.system_more));
            return;
        }
        list_json = new ArrayList<>();
        for (Display display : displays) {
            String json = new Gson().toJson(display);
            list_json.add(json);
        }

        String json = new Gson().toJson(displays);
        Log.i("list_to_string", json);
        Api.getRetrofit().changePeople(json, teamId, matchId).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    T.showShort(getApplicationContext(), "更换球员成功！");
                    Intent intent = new Intent();
                    intent.putExtra("gameScore","换人");
                    setResult(RESULT_OK,intent);
                    finish();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

}
