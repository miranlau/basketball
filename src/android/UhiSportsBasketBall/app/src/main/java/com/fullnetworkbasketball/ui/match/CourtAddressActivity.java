package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.UiSettings;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.poisearch.PoiItemDetail;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.fullnetworkbasketball.map.GeocodeSearchProxy;
import com.fullnetworkbasketball.map.MapLocationProxy;
import com.uhisports.basketball.R;

import java.util.List;

/**
 * Created by MagicBean on 2015/06/24 12:12:40
 */
public class CourtAddressActivity extends FragmentActivity implements View.OnClickListener, LocationSource, AMapLocationListener, GeocodeSearchProxy.OnGeocodeListener, TextWatcher, AMap.OnMapLongClickListener
        , MapLocationProxy.OnLocateListener, AMap.OnMarkerClickListener, AMap.OnInfoWindowClickListener, AMap.InfoWindowAdapter, GeocodeSearch.OnGeocodeSearchListener {
    private MapView mMapView;
    private AMap mMap;
    private OnLocationChangedListener mListener;
    private LocationManagerProxy mAMapLocationManager;
    private EditText mCurrentLocationText;
    private ListView mAddressListView;
    private AddressTipAdapter tipAdapter;
    private AMapLocation currentLocation;
    private PoiSearch.Query query;
    private PoiSearch poiSearch;
    private GeocodeSearchProxy geocodeSearchProxy;
    private LatLonPoint mCurrentLocation;
    private Marker mMarker;

    private TextView current;
    private MapLocationProxy locationProxy;
    private int infowindow = 2;
    private GeocodeSearch geocoderSearch;
    private TextView add_address_item_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address_layout);
        initializeView();
        initializeMap(savedInstanceState);
        mCurrentLocationText.setVisibility(View.GONE);
        current.setVisibility(View.GONE);



    }

    private void initializeMap(Bundle savedInstanceState) {
        geocodeSearchProxy = new GeocodeSearchProxy(this);
        geocodeSearchProxy.setOnGeocodeListener(this);

        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        locationProxy = new MapLocationProxy(this);
        locationProxy.addLocateListener(this);

        locationProxy.initMap(mMapView, this);
        if (mMap == null) {
            mMap = mMapView.getMap();
            setUpMap();
        }
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        mMap.setLocationSource(this);// 设置定位监听
        UiSettings mUiSetting = mMap.getUiSettings();
        mUiSetting.setZoomControlsEnabled(false);
        mUiSetting.setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式：定位（AMap.LOCATION_TYPE_LOCATE）、跟随（AMap.LOCATION_TYPE_MAP_FOLLOW）
        // 地图根据面向方向旋转（AMap.LOCATION_TYPE_MAP_ROTATE）三种模式
//        mMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
        geocoderSearch = new GeocodeSearch(this);
        geocoderSearch.setOnGeocodeSearchListener(this);
    }

    private void initializeView() {
        findViewById(R.id.back_btn).setOnClickListener(this);
//        findViewById(R.id.search_sure_btn).setOnClickListener(this);
        mCurrentLocationText = (EditText) findViewById(R.id.address_current_edit);
        mCurrentLocationText.addTextChangedListener(this);
        mAddressListView = (ListView) findViewById(R.id.tip_address_listview);
        mAddressListView.setOnItemClickListener(addressTipItemClick);
        tipAdapter = new AddressTipAdapter();
        mAddressListView.setAdapter(tipAdapter);
        current = (TextView) findViewById(R.id.current);
        current.setOnClickListener(this);
    }

    private void setUpMap() {

        mMap.setOnMarkerClickListener(this);// 设置点击marker事件监听器
        mMap.setOnInfoWindowClickListener(this);// 设置点击infoWindow事件监听器
        mMap.setInfoWindowAdapter(this);// 设置自定义InfoWindow样式
        mMap.setOnMapLongClickListener(this);
//        addMarkersToMap();// 往地图上添加marker
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
        deactivate();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                onBackPressed();
                break;


            case R.id.current:
                if (mCurrentLocation == null) {
                    return;
                }
                if (mMap == null || mMapView == null || currentLocation == null) {
                    return;
                }
                mCurrentLocationText.setText(current.getText().toString());
                latLng = new LatLonPoint(currentLocation.getLatitude(), currentLocation.getLatitude());
                submit();
                break;
        }
    }

    /**
     * 提交数据到服务器
     */
    private void submit() {
        if (latLng == null) {
            Toast.makeText(this, "请选择地址", Toast.LENGTH_SHORT).show();
            return;
        }
        String address = mCurrentLocationText.getText().toString();
        if (TextUtils.isEmpty(address)) {
            Toast.makeText(this, "地址信息不能空", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent();
        intent.putExtra("Latitude", mCurrentLocation.getLatitude());
        intent.putExtra("Longitude", mCurrentLocation.getLongitude());
        intent.putExtra("address", mCurrentLocationText.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
        if (mAMapLocationManager == null) {
            mAMapLocationManager = LocationManagerProxy.getInstance(this);
            //此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            //注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
            //在定位结束后，在合适的生命周期调用destroy()方法
            //其中如果间隔时间为-1，则定位只定一次
            mAMapLocationManager.requestLocationData(LocationProviderProxy.AMapNetwork, 60 * 1000, 10, this);
        }
    }

    @Override
    public void deactivate() {
        mListener = null;
        if (mAMapLocationManager != null) {
            mAMapLocationManager.removeUpdates(this);
            mAMapLocationManager.destroy();
        }
        mAMapLocationManager = null;
    }

    boolean isFrist = true;

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
//        currentLocation = aMapLocation;
//        if (mListener != null && aMapLocation != null) {
//            if (aMapLocation.getAMapException().getErrorCode() == 0) {
//                mListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
//                if (isFrist) {
//                    current.setText(aMapLocation.getDistrict() + aMapLocation.getStreet());
//                }
//            }
//            if (isFrist) {
//                isFrist = false;
//                geocodeSearchProxy.parsePoint(new LatLonPoint(aMapLocation.getLatitude(), aMapLocation.getLongitude()));
//            }
//        }
////        else {
////
////            T.showShort(AddAddressActivity.this, "定位失败，请到应用管理中心添加相应权限");
////            return;
////        }
//        if (aMapLocation != null) {
//            LatLng latLng = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());
//            // 定位成功后把地图移动到当前可视区域内
//            if (mMarker != null) mMarker.destroy();
////            if(circle!=null) circle.remove();
//            mMarker = mMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.find_address)));
//        }

        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.67, 104.06), 16f));
            mMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(30.67, 104.06)).anchor(0.5f, 0.5f).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.court_header)));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onGeocodeSearched(LatLonPoint point) {

    }

    @Override
    public void onRegeocodeSearched(String address, LatLonPoint latLng) {
        currentAddress = address;
        mCurrentLocation = latLng;
//        current.setText(address);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latLng.getLatitude(), latLng.getLongitude()), 16f));
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    boolean isFristSearch = true;

    @Override
    public void afterTextChanged(Editable s) {
//        if (isFristSearch) return;
        String newText = s.toString().trim();
        if (TextUtils.isEmpty(newText)) {
            tipAdapter.clearDatas();
            mAddressListView.setVisibility(View.GONE);
            return;
        } else {
            Log.i("TAG", "C:" + currentAddress + "k:" + newText);
            if (!TextUtils.equals(currentAddress, newText)) {
                if (mAddressListView.getVisibility() == View.GONE) {
                    mAddressListView.setVisibility(View.VISIBLE);
                }
                doSearchQuery(newText);
            }
        }
    }

    /**
     * 开始进行poi搜索
     */
    protected void doSearchQuery(String keyWord) {
        if (currentLocation == null) {
            Toast.makeText(this, "还未定位成功,请稍后再试", Toast.LENGTH_SHORT);
            tipAdapter.clearDatas();
            mAddressListView.setVisibility(View.GONE);
        } else {
            Log.i("TAG", "keywords:" + keyWord + "---city:" + currentLocation.getCity());
            query = new PoiSearch.Query(keyWord, "", "");// 第一个参数表示搜索字符串，第二个参数表示poi搜索类型，第三个参数表示poi搜索区域（空字符串代表全国）
            query.setPageSize(10);// 设置每页最多返回多少条poiitem
            query.setPageNum(0);// 设置查第一页
            Log.i("TAG", "keywords:" + keyWord);
            poiSearch = new PoiSearch(this, query);
            poiSearch.setOnPoiSearchListener(poiSearchListener);
            poiSearch.searchPOIAsyn();
        }
    }


    //poi搜索回调
    private PoiSearch.OnPoiSearchListener poiSearchListener = new PoiSearch.OnPoiSearchListener() {

        @Override
        public void onPoiSearched(PoiResult result, int rCode) {
            Log.i("TAG", "rCode:" + rCode);
            if (rCode == 0) {
                if (result != null && result.getQuery() != null) {// 搜索poi的结果
                    if (result.getQuery().equals(query)) {// 是否是同一条
                        // 取得搜索到的poiitems有多少页
                        List<PoiItem> poiItems = result.getPois();// 取得第一页的poiitem数据，页数从数字0开始
                        if (poiItems != null && poiItems.size() > 0) {
                            Log.i("TAG", "SIZE:" + poiItems.size());
                            tipAdapter.setDatas(poiItems);
                        } else {
                            Log.i("TAG", "null");
                        }
                    }
                } else {
                    Log.i("TAG", "无搜索结果");
                }
            }
        }

        @Override
        public void onPoiItemDetailSearched(PoiItemDetail poiItemDetail, int i) {

        }
    };

    private PoiItem poiItem;
    //当前地址
    private String currentAddress;

    private LatLonPoint latLng;
    //提示地址itemlistener
    private AdapterView.OnItemClickListener addressTipItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PoiItem poi = (PoiItem) parent.getAdapter().getItem(position);
            poiItem = poi;
            tipAdapter.clearDatas();
            mAddressListView.setVisibility(View.GONE);
            latLng = poiItem.getLatLonPoint();
            if (poi.getLatLonPoint() == null) {
                geocodeSearchProxy.parsePoint(new LatLonPoint(poi.getLatLonPoint().getLatitude(), poi.getLatLonPoint().getLongitude()));
            } else {
                // poi.getProvinceName() + poi.getCityName() +
                String address = poi.getAdName() + poi.getSnippet();
                currentAddress = address;
                mCurrentLocation = poi.getLatLonPoint();
                mCurrentLocationText.setText(poi.getTitle());
                if (mMap != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(poi.getLatLonPoint().getLatitude(), poi.getLatLonPoint().getLongitude()), 16f));
                }
                submit();
            }

        }
    };

    @Override
    public void onMapLongClick(LatLng latLng) {
        locationProxy.addMarkerM(latLng);

    }

    @Override
    public View getInfoWindow(Marker marker) {
        View infoWindow = getLayoutInflater().inflate(
                R.layout.add_address_item, null);

        render(marker, infoWindow);
        return infoWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    public void render(Marker marker, View view) {
        LatLng lat = (LatLng) marker.getObject();
        LatLonPoint l = new LatLonPoint(lat.latitude, lat.longitude);
        getAddress(l);
        add_address_item_name = (TextView) view.findViewById(R.id.add_address_item_name);
        mCurrentLocationText.setText(add_address_item_name.getText().toString());
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        LatLng la = (LatLng) marker.getObject();

        Intent intent = new Intent();
        intent.putExtra("Latitude", la.latitude);
        intent.putExtra("Longitude", la.longitude);
        intent.putExtra("address", add_address_item_name.getText().toString());

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (infowindow == 1) {
            marker.showInfoWindow();

            infowindow = 2;
        } else if (infowindow == 2) {
            marker.hideInfoWindow();
            infowindow = 1;
        }
        return true;
    }

    /**
     * 响应逆地理编码
     */
    public void getAddress(final LatLonPoint latLonPoint) {
        RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200,
                GeocodeSearch.AMAP);// 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
        geocoderSearch.getFromLocationAsyn(query);// 设置同步逆地理编码请求
    }

    @Override
    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
        add_address_item_name.setText(regeocodeResult.getRegeocodeAddress().getDistrict() + regeocodeResult.getRegeocodeAddress().getTownship() + regeocodeResult.getRegeocodeAddress().getNeighborhood());
    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }

    //地址适配器
    private class AddressTipAdapter extends BaseAdapter {

        private List<PoiItem> datas;

        public void setDatas(List<PoiItem> datas) {
            this.datas = datas;
            notifyDataSetChanged();
        }

        public void clearDatas() {
            if (datas != null) {
                datas.clear();
                notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            return this.datas == null ? 0 : datas.size();
        }

        @Override
        public Object getItem(int position) {
            return datas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TipViewHolder holder;
            if (convertView == null) {
                holder = new TipViewHolder();
                convertView = getLayoutInflater().inflate(R.layout.address_tip_listview_item, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.tip_title);
                holder.content = (TextView) convertView.findViewById(R.id.tip_content);
                convertView.setTag(holder);
            } else {
                holder = (TipViewHolder) convertView.getTag();
            }
            PoiItem tip = (PoiItem) getItem(position);
            holder.title.setText(tip.getTitle());
            holder.content.setText(tip.getSnippet());
            Log.i("TAG", "title:" + tip.getTitle());
            return convertView;
        }
    }

    static class TipViewHolder {
        public TextView title, content;
    }

    @Override
    public void onBackPressed() {
        if (mAddressListView.getVisibility() == View.VISIBLE) {
            mAddressListView.setVisibility(View.GONE);
            tipAdapter.clearDatas();
        } else {
            super.onBackPressed();
        }
    }
}
