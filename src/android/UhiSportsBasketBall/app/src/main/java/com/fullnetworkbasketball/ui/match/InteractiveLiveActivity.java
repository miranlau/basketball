package com.fullnetworkbasketball.ui.match;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.ShareData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.CameraDialog;
import com.fullnetworkbasketball.ui.dialog.ShareDialog;
import com.fullnetworkbasketball.ui.home.AfterGameActivity;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.ui.home.RacingActivity;
import com.fullnetworkbasketball.ui.home.TeamDetailsSignUpActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.BitmapUtil;
import com.fullnetworkbasketball.utils.CameraUtil;
import com.fullnetworkbasketball.utils.DisplayUtils;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2016/5/30 0030.
 */
public class InteractiveLiveActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.webView_interactive_live)
    WebView webViewInteractiveLive;
    @Bind(R.id.iv_up)
    ImageView ivUp;
    private int matchId;
    String path;

    private CameraDialog cameraDialog;
    private ArrayList<String> url = new ArrayList<>();
    private int height;
    private LoadingDialog loadingDialog;
    private Boolean isEnd;

    private SwipeRefreshLayout swipeRefreshLayout;

    private ShareDialog shareDialog;

    private ShareData shareData;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_interactive_live);
        matchId = getIntent().getIntExtra("matchId", 0);
        isEnd = getIntent().getBooleanExtra("isEnd", false);
        Log.i("matchId", "matchId的值为：" + matchId);


    }

    @Override
    protected void initializeViews() {


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setEnabled(false);


    }

    @Override
    protected void initializeData() {

        path = Api.getWebBaseUrl() + "ctl/game/detail?matchId=" + matchId;
        //  initwebView(path);

    }


    @Override
    protected void onResume() {
        super.onResume();
        initwebView(path);
        ShareRequest();
        // onRefresh();
    }

    public void initwebView(String path) {
        WindowManager windowManager = getWindowManager();
        height = windowManager.getDefaultDisplay().getHeight();

        webViewInteractiveLive.getSettings().setJavaScriptEnabled(true);
        webViewInteractiveLive.getSettings().setAppCacheEnabled(true);
        webViewInteractiveLive.getSettings().setDomStorageEnabled(true);
        webViewInteractiveLive.getSettings().setBlockNetworkImage(false);
        webViewInteractiveLive.getSettings().setUseWideViewPort(true);

        //不使用缓存
        webViewInteractiveLive.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        Map<String, String> extraHeaders = new HashMap<String, String>();
        int i = DisplayUtils.px2dip(this, height) - 80;
        path = path + "&height=" + i;

        if (UserManager.getIns().getUser() == null) {
            extraHeaders.put("token", "null");

        } else {
            extraHeaders.put("token", UserManager.getIns().getUser().token);
        }


        webViewInteractiveLive.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                loadingDialog = new LoadingDialog(InteractiveLiveActivity.this);
                loadingDialog.setCancelable(false);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                ivUp.setVisibility(View.GONE);
                loadingDialog.dismiss();

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // TODO Auto-generated method stub
                super.onReceivedError(view, errorCode, description, failingUrl);

            }
        });

        webViewInteractiveLive.loadUrl(path, extraHeaders);


        webViewInteractiveLive.addJavascriptInterface(new JavaScriptObject(), "JSBridge");
//        webViewInteractiveLive.loadUrl("http://192.168.0.157:8080/football/ctl/game/detail/202680");
        Log.i("web_url", Api.getWebBaseUrl() + "ctl/game/detail?matchId=" + matchId + "&height=" + DisplayUtils.px2dip(this, height));

        webViewInteractiveLive.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress >= 100) {
                    // 切换页面
//                    ivUp.setVisibility(View.GONE);
                }
            }

        });
    }

    @Override
    public void onRefresh() {
        initwebView(path);
    }


    class JavaScriptObject {
        /**
         * 图文直播发送文字
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void SendComment(String[] message) {

            int leagueId = Integer.parseInt(message[0]);
            String content = message[1];

            requestSendMessage(leagueId, content, 77);
        }

        /**
         * 图片的缩放
         * @param path
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void PhotoZoom(String path){
            Intent intent = new Intent(InteractiveLiveActivity.this, PhotoViewActivity.class);
            intent.putExtra("path",path);
            Log.i("5555555555555",path);
            startActivity(intent);
        }


        /**
         * 图文直播发送图片
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void SendPhotoAction(int leagueId) {
            selectPicture();
            Log.i("5555555555555", leagueId+"");

        }


        /**
         * 赛程 球队
         * <p/>
         * Integer.parseInt(send[1])
         */

        @JavascriptInterface //sdk17版本以上加上注解
        public void ActionGoToTeams(int[] send) {
            Intent intent = new Intent();
            intent.putExtra("teamId", send[1]);
            intent.putExtra("isFormHere", true);
            intent.putExtra("leagueId", send[0]);
            intent.setClass(InteractiveLiveActivity.this, OtherTeamDetailsActivity.class);
            InteractiveLiveActivity.this.startActivity(intent);

        }

        /**
         * 赛程 比赛
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void ActionGoToMatchs(String[] matchArray) {

            int status = Integer.parseInt(matchArray[0]);
            if (status == 100) {

//                Intent intent = new Intent(InteractiveLiveActivity.this, InteractiveLiveActivity.class);
//                intent.putExtra("matchId", Integer.parseInt(matchArray[1]));
//                startActivity(intent);
                return;
            } else if (status == 0) {
                Intent intent = new Intent();
                intent.putExtra("matchId", Integer.parseInt(matchArray[1]));
                intent.setClass(InteractiveLiveActivity.this, RacingActivity.class);
                startActivity(intent);
            } else if (status == 10 || status == 20) {
                Intent intent = new Intent();
                intent.putExtra("matchId", Integer.parseInt(matchArray[1]));
                intent.setClass(InteractiveLiveActivity.this, TeamDetailsSignUpActivity.class);
                InteractiveLiveActivity.this.startActivity(intent);
            }

        }

        /**
         * 赛程 比赛
         */
        @JavascriptInterface //sdk17版本以上加上注解
        public void commonJSBridge(String[] url) {
            String pathUrl = Api.getWebBaseUrl() + "ctl/" + url[0];

            String name = url[1];
            Intent intent = new Intent(InteractiveLiveActivity.this, AfterGameActivity.class);
            intent.putExtra("pathUrl", pathUrl);
            intent.putExtra("name", name);
            startActivity(intent);
        }
    }

    /**
     * 选择图片
     */
    public void selectPicture() {
        if (cameraDialog == null) {
            cameraDialog = new CameraDialog(this, R.style.callDialog);
            cameraDialog.getWindow().setGravity(Gravity.BOTTOM);
            cameraDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
        }
        cameraDialog.show();
        WindowManager windowManager1 = getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = cameraDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        cameraDialog.getWindow().setAttributes(lp1);
    }


    /**
     * 图片回传
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharePreHelper.getIns().setShouldShowNotification(false);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CameraUtil.TAKE_PHOTO:
                    String path = CameraUtil.getRealFilePath();
                    if (new File(path).exists()) {
                        //图片存在？
                        Logger.i("imagePath:" + path);
                        try {
                            Bitmap temp = BitmapUtil.revitionImageSize(path);
                            path = BitmapUtil.saveBitmap(this, temp, path);
//
                            if (uploadImage(path)) return;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }
                    break;
                case CameraUtil.PICK_PHOTO:
                    try {
                        String pickPath = CameraUtil.resolvePhotoFromIntent(InteractiveLiveActivity.this, data);
                        Uri uri = Uri.fromFile(new File(pickPath));

                        if (!new File(pickPath).exists()) {
                            Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                        Bitmap header = BitmapUtil.revitionImageSize(pickPath);
                        String finalPath = BitmapUtil.saveBitmap(this, header, pickPath);
//                        myHeader.setImageBitmap(header);
                        if (uploadImage(finalPath)) return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }


    /**
     * 图片上传
     */
    public boolean uploadImage(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) return true;

        RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody boy = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", "other")
                .addFormDataPart("files", file.getName(), fileBody)
                .build();


        Api.getRetrofit().SendPhoto(boy).enqueue(new RequestCallback<HttpResponseFWF<UploadResult>>() {
            @Override
            public void onSuccess(HttpResponseFWF<UploadResult> response) {

                if (response.isSuccess()) {

                    url = (ArrayList) response.getData();


                    requestSendPhoto(matchId, url.get(0), 88);


                } else {
                    T.show(InteractiveLiveActivity.this, "失败", 1000);
                }

            }

            @Override
            public void onFinish() {

            }
        });


        return false;
    }


    /**
     * 文字上传接口
     */

    public void requestSendMessage(int leagueId, String content, int status) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("matchId", leagueId);
        params.put("msg", content);
        params.put("status", status);
        Api.getRetrofit().SendMessage(params).enqueue(new RequestCallback<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {

                if (response.isSuccess()) {
                    //code 为 1006时表示没有登录

                    initwebView(path);

                } else {
                    if (response.code == 1006) {

                        App.getInst().toLogin();
                    }

                    T.showShort(InteractiveLiveActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    public void requestSendPhoto(int leagueId, String photoUrl, int status) {

        HashMap<String, Object> params = new HashMap<>();
        params.put("matchId", leagueId);
        params.put("msg", photoUrl);
        params.put("status", status);
        Api.getRetrofit().SendMessage(params).enqueue(new RequestCallback<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {

                if (response.isSuccess()) {
                    initwebView(path);
                }
                if (response.code == 1006) {

                    App.getInst().toLogin();
                }

                T.showShort(InteractiveLiveActivity.this, response.getMessage());
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setRightImageView(R.mipmap.share);
        if (isEnd) {
            getCustomActionBar().setTitleText("赛后");

        } else {
            getCustomActionBar().setTitleText(getResources().getString(R.string.interactive_live));
        }
    }


    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_image:


                if (shareDialog == null) {
                    shareDialog = new ShareDialog(InteractiveLiveActivity.this, R.style.callDialog, 1, shareData);
                    shareDialog.getWindow().setGravity(Gravity.BOTTOM);
                }

                shareDialog.show();

                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    public static class UploadResult {
        public String fid;
    }

    /**
     * 分享网络请求
     */
    public void ShareRequest() {

        HashMap<String, Object> params = new HashMap<>();

        params.put("matchId", matchId);

        Api.getRetrofit().share(params).enqueue(new RequestCallback<HttpResponseFWF2<ShareData>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<ShareData> response) {

                shareData = response.getData();


            }

            @Override
            public void onFinish() {

            }
        });


    }
}
