package com.fullnetworkbasketball.ui.match;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.dialog.ChooseMatchStateDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.home.HomeActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.MarqueeTextView;
import com.fullnetworkbasketball.views.VibrateHelp;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2016/5/17 0017.
 */
public class MatchScoreActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.tv_foul)
    TextView tvFoul;
    @Bind(R.id.tv_offside)
    TextView tvOffside;
    @Bind(R.id.tv_yellow_card)
    TextView tvYellowCard;
    @Bind(R.id.tv_red_card)
    TextView tvRedCard;
    @Bind(R.id.tv_fighting)
    TextView tvFighting;
    @Bind(R.id.tv_rescue)
    TextView tvRescue;
    @Bind(R.id.tv_steals)
    TextView tvSteals;
    @Bind(R.id.tv_intercept)
    TextView tvIntercept;
    @Bind(R.id.tv_shoot)
    TextView tvShoot;
    @Bind(R.id.tv_start_match)
    TextView tvStartMatch;
    @Bind(R.id.min)
    TextView min;
    @Bind(R.id.sec)
    TextView sec;
    @Bind(R.id.tv_pause)
    TextView tvPause;
    @Bind(R.id.ll_pause_half)
    LinearLayout llPauseHalf;
    @Bind(R.id.marquee)
    MarqueeTextView marquee;
    @Bind(R.id.which_state_of_match)
    TextView whichStateOfMatch;
    @Bind(R.id.match_score_host_header)
    ImageView matchScoreHostHeader;
    @Bind(R.id.match_score_host_name)
    TextView matchScoreHostName;
    @Bind(R.id.match_score_state)
    TextView matchScoreState;
    @Bind(R.id.match_score_visitor_header)
    ImageView matchScoreVisitorHeader;
    @Bind(R.id.match_score_vositor_name)
    TextView matchScoreVositorName;
    @Bind(R.id.back_to_last)
    TextView backToLast;
    @Bind(R.id.change_player)
    TextView changePlayer;
    @Bind(R.id.tv_corner)
    TextView tvCorner;
    @Bind(R.id.free_ball)
    LinearLayout freeBall;
    @Bind(R.id.pass_ball)
    TextView passBall;
    @Bind(R.id.tv_is_match_state)
    TextView tvIsMatchState;
    private boolean isPaused = false;
    private int click = 1;
    private int timeUsedInsec;
    private String timeUsed;
    private int teamId;
    private int matchId;
    private Game game;
    private int actionType;
    private boolean jiashi = false;
    private boolean dianqiu=false;
    private ArrayList<Player> players_down;
    private String tip="";
    private String gameScore;

    private Handler uiHandle = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (!isPaused) {
                        min.setTextColor(Color.WHITE);
                        sec.setTextColor(Color.WHITE);
                        addTimeUsed();
                        updateClockUI();
                    }else {
                        min.setTextColor(Color.RED);
                        sec.setTextColor(Color.RED);
                        cannotOperate();
                    }
                    uiHandle.sendEmptyMessageDelayed(1, 1000);
                    break;
                default:
                    break;
            }
        }
    };
    private ChooseMatchStateDialog dialog;
    private ContactOpponentCaptainDialog contactOpponentCaptainDialog;
    private ArrayList<String> cacheMatchState;
    private TextView textView;
    private BroadcastReceiver broadcastReceiver;
    private ContactOpponentCaptainDialog dialog1;
    private final int VIBRATE_TIME=500;

    private void startTime() {
        uiHandle.sendEmptyMessageDelayed(1, 1000);
    }

    public void addTimeUsed() {
        timeUsedInsec = timeUsedInsec + 1;
        timeUsed = this.getMin() + ":" + this.getSec();
    }

    public CharSequence getMin() {
        int sec = timeUsedInsec / 60;
        return sec < 10 ? "0" + sec : String.valueOf(sec);
    }

    public CharSequence getSec() {
        int sec = timeUsedInsec % 60;
        return sec < 10 ? "0" + sec : String.valueOf(sec);
    }

    /**
     * 更新时间的显示
     */
    private void updateClockUI() {
        min.setText(getMin() + "'");
        sec.setText(getSec() + "''");
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            click = savedInstanceState.getInt("click", 0);
        }
        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        game = (Game) getIntent().getSerializableExtra("game");
        //隐藏状态栏
        //定义全屏参数
        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        //获得当前窗体对象
        Window window = MatchScoreActivity.this.getWindow();
        //设置当前窗体为全屏显示
        window.setFlags(flag, flag);

        setContentView(R.layout.activity_match_score_new);
        ButterKnife.bind(this);
        initializeViews();
        cacheMatchState = new ArrayList<>();
        textView = (TextView)findViewById(R.id.tv_is_match_state);


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("refresh");
        broadcastReceiver= new Receiver();
        this.registerReceiver(broadcastReceiver, intentFilter);
    }
    public class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            onLineData();
        }

    }


    private void canOperate() {
        tvFoul.setEnabled(true);
        tvFoul.getBackground().setAlpha(255);
        tvOffside.setEnabled(true);
        tvOffside.getBackground().setAlpha(255);
        tvYellowCard.setEnabled(true);
        tvYellowCard.getBackground().setAlpha(255);
        tvRedCard.setEnabled(true);
        tvRedCard.getBackground().setAlpha(255);
        tvFighting.setEnabled(true);
        tvFighting.getBackground().setAlpha(255);
        tvRescue.setEnabled(true);
        tvRescue.getBackground().setAlpha(255);
        tvSteals.setEnabled(true);
        tvSteals.getBackground().setAlpha(255);
        tvIntercept.setEnabled(true);
        tvIntercept.getBackground().setAlpha(255);
        tvPause.setEnabled(true);
//        tvFoul.getBackground().setAlpha(255);
        tvShoot.setEnabled(true);
        tvShoot.getBackground().setAlpha(255);
        backToLast.setEnabled(true);
        backToLast.setAlpha(1);
        changePlayer.setEnabled(true);
        changePlayer.setAlpha(1);
        tvCorner.setEnabled(true);
        tvCorner.getBackground().setAlpha(255);
        freeBall.setEnabled(true);
        freeBall.getBackground().setAlpha(255);
        passBall.setEnabled(true);
        passBall.getBackground().setAlpha(255);
        tvIsMatchState.setEnabled(true);


        tvFoul.setOnClickListener(this);
        tvOffside.setOnClickListener(this);
        tvYellowCard.setOnClickListener(this);
        tvRedCard.setOnClickListener(this);
        tvFighting.setOnClickListener(this);
        tvRescue.setOnClickListener(this);
        tvSteals.setOnClickListener(this);
        tvIntercept.setOnClickListener(this);
        tvShoot.setOnClickListener(this);
        tvStartMatch.setOnClickListener(this);
        tvPause.setOnClickListener(this);
        backToLast.setOnClickListener(this);
        changePlayer.setOnClickListener(this);
        tvCorner.setOnClickListener(this);
        freeBall.setOnClickListener(this);
        passBall.setOnClickListener(this);
        tvIsMatchState.setOnClickListener(this);
    }

    private void cannotOperate() {
        tvFoul.setEnabled(false);
        tvFoul.getBackground().setAlpha(100);
        tvOffside.setEnabled(false);
        tvOffside.getBackground().setAlpha(100);
        tvYellowCard.setEnabled(false);
        tvYellowCard.getBackground().setAlpha(100);
        tvRedCard.setEnabled(false);
        tvRedCard.getBackground().setAlpha(100);
        tvFighting.setEnabled(false);
        tvFighting.getBackground().setAlpha(100);
        tvRescue.setEnabled(false);
        tvRescue.getBackground().setAlpha(100);
        tvSteals.setEnabled(false);
        tvSteals.getBackground().setAlpha(100);
        tvIntercept.setEnabled(false);
        tvIntercept.getBackground().setAlpha(100);
        tvShoot.setEnabled(false);
        tvShoot.getBackground().setAlpha(100);
//        tvStartMatch.setEnabled(false);
        backToLast.setEnabled(false);
        backToLast.setAlpha((float) 0.5);



        tvCorner.setEnabled(false);
        tvCorner.getBackground().setAlpha(100);
        freeBall.setEnabled(false);
        freeBall.getBackground().setAlpha(100);
        passBall.setEnabled(false);
        passBall.getBackground().setAlpha(100);

        if (isPaused){
            tvIsMatchState.setEnabled(true);
            tvPause.setEnabled(true);
            changePlayer.setEnabled(true);
            changePlayer.setAlpha(1);
        }else {
            tvIsMatchState.setEnabled(false);
//            changePlayer.setEnabled(false);
//            changePlayer.setAlpha((float) 0.5);

            if (game.status==11){
                changePlayer.setEnabled(true);
                changePlayer.setAlpha(1);
            }else if (game.status==30){
                changePlayer.setEnabled(true);
                changePlayer.setAlpha(1);
                tvPause.setAlpha((float) 0.5);
                tvPause.setEnabled(false);
                tvShoot.setEnabled(true);
                tvShoot.getBackground().setAlpha(255);
                backToLast.setEnabled(true);
                backToLast.setAlpha((float) 1);
                tvShoot.setOnClickListener(this);
                backToLast.setOnClickListener(this);
                changePlayer.setOnClickListener(this);
                tvIsMatchState.setEnabled(true);
                tvIsMatchState.setOnClickListener(this);
                tvStartMatch.setOnClickListener(this);
            }else if (game.status==11){
                backToLast.setEnabled(false);
                backToLast.setAlpha((float) 0.5);
            }
            else {
                changePlayer.setEnabled(false);
                changePlayer.setAlpha((float) 0.5);
            }
        }

        tvStartMatch.setOnClickListener(this);
//        tvStartMatch.setAlpha(1);
    }
    private void resetState(){
        if (game.isSuspend){
            click=1;
        }else {
            click=2;
        }
        tvStartMatch.setVisibility(View.GONE);
        llPauseHalf.setVisibility(View.VISIBLE);


        uiHandle.removeMessages(1);
        startTime();
        if (click==1){
            updateClockUI();
            isPaused=true;
        }else {
            isPaused = false;
        }

    }
    private void initializeViews() {


        ImageLoader.loadCicleImage(this, game.home.logo, R.mipmap.default_team, matchScoreHostHeader);
        matchScoreHostName.setText(game.home.name);
        if (game.visiting != null) {
            ImageLoader.loadCicleImage(this, game.visiting.logo, R.mipmap.default_team, matchScoreVisitorHeader);
            matchScoreVositorName.setText(game.visiting.name);
        } else {
//            ImageLoader.loadCicleImage(this, R.mipmap.default_header, matchScoreVisitorHeader);
            matchScoreVisitorHeader.setVisibility(View.INVISIBLE);
            matchScoreVositorName.setText("");
        }

        if (teamId==game.home.id){
            matchScoreHostName.setTextColor(getResources().getColor(R.color.red));
        }else if (teamId==game.visiting.id){
            matchScoreVositorName.setTextColor(getResources().getColor(R.color.red));
        }
        matchScoreState.setText(game.homeScore + ":" + game.visitingScore);
        if (game.isSuspend){
            tvPause.setText(getResources().getString(R.string.keep));
        }else {
            tvPause.setText(getResources().getString(R.string.pause));
        }
        switch (game.status) {
            //未开始
            case 0:
                cannotOperate();
                break;
            //上半场
            case 10:
                timeUsedInsec = game.duration;
                resetState();


                whichStateOfMatch.setText(getResources().getString(R.string.up_half));
//                tvIsMatchState.setText(getResources().getString());
                canOperate();
//                tvPause.setText(getResources().getString(R.string.half_rest));
                break;
            //中场休息
            case 11:
                tvStartMatch.setVisibility(View.GONE);
                llPauseHalf.setVisibility(View.VISIBLE);
                timeUsedInsec = 0;
                isPaused = true;
                whichStateOfMatch.setText(getResources().getString(R.string.half_rest));
                tvIsMatchState.setText(getResources().getString(R.string.start_down_half));
                cannotOperate();
                tvIsMatchState.setEnabled(true);
                changePlayer.setEnabled(true);
                tvIsMatchState.setOnClickListener(this);
                changePlayer.setOnClickListener(this);
                break;
            //下半场
            case 12:
                resetState();
                timeUsedInsec = game.duration;
                whichStateOfMatch.setText(getResources().getString(R.string.down_half));
                tvIsMatchState.setText(getResources().getString(R.string.over));
                canOperate();
                break;
            //加时赛上半场
            case 20:
                jiashi=true;

                if (game.isSuspend){
                    updateClockUI();
                    click=1;
                }else {
                    click=2;
                }

                if (click==1){
                    isPaused=true;
                }else {
                    isPaused = false;
                }
                timeUsedInsec = game.duration;

                uiHandle.removeMessages(1);

                tvStartMatch.setVisibility(View.GONE);
                llPauseHalf.setVisibility(View.VISIBLE);
//                uiHandle.sendEmptyMessageDelayed(1, 1000);
                startTime();
                whichStateOfMatch.setText(getResources().getString(R.string.up_over_time));
                tvIsMatchState.setText(getResources().getString(R.string.half_over_time));
                canOperate();
                break;
            //加时赛中场休息
            case 21:
                jiashi=true;
                timeUsedInsec = 0;
                updateClockUI();
                isPaused = true;
                tvStartMatch.setVisibility(View.GONE);
                llPauseHalf.setVisibility(View.VISIBLE);
                whichStateOfMatch.setText(getResources().getString(R.string.half_over_time));
                tvIsMatchState.setText(getResources().getString(R.string.down_over_time));
                cannotOperate();
                tvIsMatchState.setEnabled(true);
                tvIsMatchState.setOnClickListener(this);
                break;
            //加时赛下半场
            case 22:
                jiashi=true;
                timeUsedInsec = game.duration;
                uiHandle.removeMessages(1);
                startTime();
                if (game.isSuspend){
                    updateClockUI();
                    click=1;
                    isPaused=true;
                }else {
                    click=2;
                    isPaused = false;
                }


                if (click==1){

                }else {

                }
                tvStartMatch.setVisibility(View.GONE);
                llPauseHalf.setVisibility(View.VISIBLE);
                whichStateOfMatch.setText(getResources().getString(R.string.down_over_time));
                tvIsMatchState.setText(getResources().getString(R.string.over));
                canOperate();
                break;
            //点球大战
            case 30:
                dianqiu=true;
                tvStartMatch.setVisibility(View.GONE);
                llPauseHalf.setVisibility(View.VISIBLE);
                timeUsedInsec = 0;
                uiHandle.removeMessages(1);
//                startTime();
                if (game.isSuspend){
                    updateClockUI();
                    click=1;
                    isPaused=true;
                }else {
                    click=2;
                    isPaused = false;
                }

                whichStateOfMatch.setText(getResources().getString(R.string.penalty_shootout));
                tvIsMatchState.setText(getResources().getString(R.string.over));
                tvShoot.setText("点球");
//                tvFoul.setEnabled(false);
//                tvOffside.setEnabled(false);
//                tvYellowCard.setEnabled(false);
//                tvRedCard.setEnabled(false);
//                tvFighting.setEnabled(false);
//                tvRescue.setEnabled(false);
//                tvSteals.setEnabled(false);
//                tvIntercept.setEnabled(false);
//                tvCorner.setEnabled(false);
//                freeBall.setEnabled(false);
//                passBall.setEnabled(false);
//                tvShoot.setEnabled(true);
//                tvPause.setAlpha((float) 0.5);
//                tvPause.setEnabled(false);
//                tvShoot.setOnClickListener(this);
//                tvIsMatchState.setEnabled(true);
//                tvIsMatchState.setOnClickListener(this);
//                changePlayer.setEnabled(true);
//                changePlayer.setOnClickListener(this);
                cannotOperate();
                break;
            //比赛结束
            case 100:
//                timeUsedInsec=0;
//                isPaused=true;
//                whichStateOfMatch.setText(getResources().getString(R.string.penalty_shootout));
                T.showShort(MatchScoreActivity.this, "比赛已经结束！");
                Intent intent = new Intent(MatchScoreActivity.this, HomeActivity.class);
                startActivity(intent);
                SharePreHelper.getIns().putIsCreateMatch(true);
                finish();
                break;


        }


    }
    private void chageState(final int actionType){
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", teamId);
        params.put("status", actionType);
        Api.getRetrofit().matchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.code == 3005) {
                    onLineData();
                    return;
                }

                if (response.isSuccess()) {
                    if (actionType!=35&&actionType!=36&&actionType!=37){
                        onLineData();
                    }

//                    initializeViews();
                    switch (actionType) {

                        case 11:
                            isPaused = false;
                            tvStartMatch.setVisibility(View.GONE);
                            llPauseHalf.setVisibility(View.VISIBLE);
                            uiHandle.removeMessages(1);

                            startTime();

                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.start_match));
                            tip = getResources().getString(R.string.start_match);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 35:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = "任意球成功！";
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 36:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = "角球成功！";
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 37:
                            tip = "传球成功！";
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 44:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 42:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 41:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 43:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 27:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 28:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 29:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 31:
                            T.showShort(getApplicationContext(), response.getMessage());
                            tip = gameScore;
                            marquee.setText(tip);
                        case 55:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.pause_success));
                            tip = getResources().getString(R.string.pause_success);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 56:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.keep_success));
                            tip = getResources().getString(R.string.keep_success);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 57:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.half_rest));
                            tip = getResources().getString(R.string.half_rest);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 58:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.down_half_start_success));
                            tip = getResources().getString(R.string.down_half_start_success);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 60:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.up_over_time));
                            tip = getResources().getString(R.string.up_over_time);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 61:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.half_over_time));
                            tip = getResources().getString(R.string.half_over_time);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 62:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.down_over_time));
                            tip = getResources().getString(R.string.down_over_time);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 70:
                            T.showShort(MatchScoreActivity.this, getResources().getString(R.string.penalty_shootout));
                            tip = getResources().getString(R.string.penalty_shootout);
                            marquee.setText(tip);
                            cacheMatchState.add(tip);
                            break;
                        case 100:


                            T.showShort(MatchScoreActivity.this, "比赛结束！");
                            Intent intent = new Intent(MatchScoreActivity.this, HomeActivity.class);
                            startActivity(intent);
                            SharePreHelper.getIns().putIsCreateMatch(true);
                            finish();

                            break;
                        case 120:
                            T.showShort(getApplicationContext(), response.getMessage());
                            if (cacheMatchState.size() == 0) {
                                marquee.setText("");
                                return;
                            }
                            marquee.setText(cacheMatchState.get(cacheMatchState.size() - 1));
                            break;

                    }

                    textView.setEnabled(true);
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        uiHandle.removeMessages(1);
        isPaused = true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("click", click);
    }

    private void startMatch(){

    }
    private void onLineData() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();
                    players_down = game.homePlayers;
                    initializeViews();


                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isPaused = false;
        onLineData();


    }

    //    记分类型(Action Type)
//    11	比赛开始
//    22	换人
//    23	乌龙球
//    25	射偏
//    26	助攻
//    27	拦截
//    28	解围
//    29	扑救
//    30	头球
//    31	抢断
//    32	射正
//    33	进球
//    34	射偏
//    35	任意球
//    36	角球
//    37	传球
//    38	点球

//    41	越位
//    42	红牌
//    43	黄牌
//    44	犯规
//    55	暂停
//    56	继续
//    57	中场休息
//    58	下半场开始
//    60	加时赛上半场
//    61	加时赛中场休息
//    62	加时赛下半场
//    70	点球大战
//    77	直播信息
//    88	直播图片
//    100	比赛结束
//    110	比赛取消

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            //犯规
            case R.id.tv_foul:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 44;
                goLeftModel();
                break;
            //越位
            case R.id.tv_offside:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 41;
                goLeftModel();

                break;
            //黄牌
            case R.id.tv_yellow_card:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 43;
                goLeftModel();
                break;
            //红牌
            case R.id.tv_red_card:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 42;
                goLeftModel();
                break;
            //扑救
            case R.id.tv_fighting:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 29;
                goLeftModel();
                break;
            //解围
            case R.id.tv_rescue:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 28;
                goLeftModel();
                break;
            //抢断
            case R.id.tv_steals:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 31;
                goLeftModel();

                break;
            //拦截
            case R.id.tv_intercept:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                actionType = 27;
                goLeftModel();
                break;
            //射门
            case R.id.tv_shoot:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                Intent intent = new Intent(this, MatchScoreRightActivity.class);
                intent.putExtra("disPlayer",game.homeStarting);
                intent.putExtra("teamId", teamId);
                intent.putExtra("matchId", matchId);
                intent.putExtra("actionType", actionType);
                intent.putExtra("status",game.status);
                startActivityForResult(intent, 10009);
                break;
            //撤销操作
            case R.id.back_to_last:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                if (cacheMatchState.size()==0){
                    marquee.setText("");
                    return;
                }
                cacheMatchState.remove(cacheMatchState.get(cacheMatchState.size()-1));
               chageState(120);
                break;

            //换人
            case R.id.change_player:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                Intent intent1 = new Intent(this,ChangePeopleNewActivity.class);
                intent1.putExtra("players",players_down);
                intent1.putExtra("disPlayer",game.homeStarting);
                intent1.putExtra("teamId", teamId);
                intent1.putExtra("matchId", matchId);
                intent1.putExtra("actionType", 22);
                startActivityForResult(intent1, 10010);
                break;
            //角球
            case R.id.tv_corner:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                chageState(36);

                break;
            //任意球
            case R.id.free_ball:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                chageState(35);

                break;

            //传球
            case R.id.pass_ball:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                chageState(37);

                break;
            //
            //开始比赛
            case R.id.tv_start_match:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                click=2;
                chageState(11);

                break;
            //暂停比赛
            case R.id.tv_pause:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                if (game.isSuspend) {
//                    click = 2;

                    chageState(56);
                } else  {
//                    isPaused = false;
//                    click = 1;
//                    tvPause.setText(getResources().getString(R.string.pause));
                    chageState(55);
                }

//                timeUsedInsec = 0;
                break;
            case R.id.tv_is_match_state:
                VibrateHelp.vSimple(v.getContext(), VIBRATE_TIME);
                if (game.status>=12){
                    if (game.homeScore!=game.visitingScore){
                        textView.setEnabled(false);

                      popEnd();


                    }else if (jiashi&&dianqiu){
                        textView.setEnabled(false);
                      popEnd();
                    }else if (jiashi&&!dianqiu&&game.status==20){
                        chageState(61);

                    }else if (jiashi&&!dianqiu&&game.status==21){
                        chageState(62);
                    }else if (game.status==30){
                        textView.setEnabled(false);
                        popEnd();
                    }
                    else {
                        textView.setEnabled(false);

                            if (jiashi&&!dianqiu&&game.status==22){
                                dialog = new ChooseMatchStateDialog(MatchScoreActivity.this, R.style.callDialog, 1);
                            }else if (!jiashi&&!dianqiu){
                                dialog = new ChooseMatchStateDialog(MatchScoreActivity.this, R.style.callDialog, 2);
                            }else if(dianqiu&&game.status==30){
                                dialog = new ChooseMatchStateDialog(MatchScoreActivity.this, R.style.callDialog, 3);
                        }
                            dialog.setConfirmClickListener(new ChooseMatchStateDialog.OnConfirmOrderListener() {
                                @Override
                                public void onConfirmOrder(int i) {
//                                    textView.setEnabled(true);
                                    if (i == 1) {

                                      chageState(60);
                                    }else if (i==2){
                                        chageState(70);
                                    }else if (i==3){
                                        popEnd();
                                    }else if (i==4){
                                        textView.setEnabled(true);
                                    }
                                }
                            });
                        dialog.show();
                    }
                }
                else if (game.status==11){
                    chageState(58);
                }else if (game.status==10){
                    if (contactOpponentCaptainDialog==null){
                        contactOpponentCaptainDialog = new ContactOpponentCaptainDialog(MatchScoreActivity.this, R.style.callDialog, 14);
                        contactOpponentCaptainDialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                            @Override
                            public void onConfirmOrder(int i) {
                                if (i==14){
                                    chageState(57);
                                }
                            }
                        });
                    }
                  contactOpponentCaptainDialog.show();

                }
                break;


        }
    }
    private void  popEnd(){
        if (dialog1 == null) {
            dialog1 = new ContactOpponentCaptainDialog(MatchScoreActivity.this, R.style.callDialog, 11);
            dialog1.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(int i) {
                    if (i==11){
                        chageState(100);
                    }
                }
            });
        }
        dialog1.show();
        textView.setEnabled(true);
    }
    private void goLeftModel() {
        Intent intent = new Intent(MatchScoreActivity.this, MatchScoreLeftActivity.class);
        intent.putExtra("teamId", teamId);
        intent.putExtra("matchId", matchId);
        intent.putExtra("playerList", game.homeStarting);
        intent.putExtra("actionType", actionType);
        startActivityForResult(intent, 10008);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode){
            case RESULT_OK:
                if (requestCode==10008){
                    gameScore = data.getStringExtra("gameScore");
                    cacheMatchState.add(gameScore);
                    marquee.setText(gameScore);
                }else if (requestCode==10009){
                    gameScore = data.getStringExtra("gameScore");
                    cacheMatchState.add(gameScore);
                    marquee.setText(gameScore);
                }else if (requestCode==10010){
                    gameScore = data.getStringExtra("gameScore");
                    cacheMatchState.add(gameScore);
                    marquee.setText(gameScore);
                }

                break;
        }
    }
}
