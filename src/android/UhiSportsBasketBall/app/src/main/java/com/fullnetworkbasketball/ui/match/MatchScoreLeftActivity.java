package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/26 0026.
 */
public class MatchScoreLeftActivity extends AppCompatActivity implements View.OnClickListener {
    @Bind(R.id.match_score_left_sure)
    TextView matchScoreLeftSure;
    @Bind(R.id.match_score_left_cancel)
    TextView matchScoreLeftCancel;
    @Bind(R.id.abs_layout)
    AbsoluteLayout absLayout;

    //    记分类型(Action Type)
//    11	比赛开始
//    22	换人
//    23	乌龙球
//    25	射偏
//    26	助攻
//    27	拦截
//    28	解围
//    29	扑救
//    30	头球
//    31	抢断
//    32	射正
//    33	进球
//    34	射偏
//    35	任意球
//    36	角球
//    37	传球
//    38	点球
//    41	越位
//    42	红牌
//    43	黄牌
//    44	犯规
//    55	暂停
//    56	继续
//    57	中场休息
//    58	下半场开始
//    60	加时赛上半场
//    61	加时赛中场休息
//    62	加时赛下半场
//    70	点球大战
//    77	直播信息
//    88	直播图片
//    100	比赛结束
//    110	比赛取消
    private int matchId;
    private int teamId;
    private int actionType;
    private int perX;
    private int perY;
    private ArrayList<Display> displays;
    private int intWidth;
    private int intHeight;
    private Display display1;
    private ArrayList<FrameLayout> frameLayouts;
    private int playerId;
    private ContactOpponentCaptainDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_technical_statistics);
        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        displays = (ArrayList<Display>) getIntent().getSerializableExtra("playerList");

        actionType = getIntent().getIntExtra("actionType", 0);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
         /* 设置图片的宽高 */
        intWidth = ScreenUtils.dp2px(this, 70);
        intHeight = ScreenUtils.dp2px(this, 70);
        perX = (ScreenUtils.dp2px(MatchScoreLeftActivity.this, 300)) / 25;
        perY = (ScreenUtils.dp2px(MatchScoreLeftActivity.this, 300)) / 25;
        matchScoreLeftSure.setOnClickListener(this);
        matchScoreLeftCancel.setOnClickListener(this);
        frameLayouts = new ArrayList<>();
        getLineUp();

    }

    private void getLineUp() {

        for (Display display : displays) {
            final FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(MatchScoreLeftActivity.this).inflate(R.layout.team_details_player_item_three, null);
            ImageView header = (ImageView) frameLayout.findViewById(R.id.player_header);
            TextView number = (TextView) frameLayout.findViewById(R.id.number);
            final ImageView choose = (ImageView) frameLayout.findViewById(R.id.iv_choose);
            ImageLoader.loadCicleImage(MatchScoreLeftActivity.this, display.avatar, R.mipmap.default_header, header);
            number.setText(display.number + "");
            absLayout.addView(frameLayout);
            frameLayout.setTag(display);
            frameLayouts.add(frameLayout);
            frameLayout.setLayoutParams
                    (
                            new AbsoluteLayout.LayoutParams
                                    (intWidth, intHeight, ((int) (display.x)) * perX , ((int) (display.y)) * perY)

                    );
            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                                absLayout.removeAllViews();
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);

                    for (FrameLayout frameLayout1 : frameLayouts) {
                        ImageView imageView = (ImageView) frameLayout1.findViewById(R.id.iv_choose);
                        imageView.setVisibility(View.GONE);
                    }
                    choose.setVisibility(View.VISIBLE);
                    playerId = ((Display) frameLayout.getTag()).id;
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);
//                            }
                    matchScore();
                }
            });
        }


    }


    private void matchScore() {
        final HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", teamId);
        params.put("status", actionType);
        params.put("playerA", playerId);
        if (actionType == 42 || actionType == 43) {
            if (playerId == 0) {
                T.showShort(MatchScoreLeftActivity.this, "请选择球员！");
                return;
            }
        } else {
            if (playerId == 0) {


                if (dialog == null) {
                    dialog = new ContactOpponentCaptainDialog(MatchScoreLeftActivity.this, R.style.callDialog, 10);
                    dialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                        @Override
                        public void onConfirmOrder(int i) {
                            if (i == 10) {
                                Api.getRetrofit().matchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
                                    @Override
                                    public void onSuccess(HttpResponseFWF2 response) {
                                        Logger.i("msg:" + response.toString());
                                        T.showShort(getApplicationContext(), response.getMessage());
                                        if (response.isSuccess()) {
                                            playerId = 0;
                                            Intent intent = new Intent();
                                            if (actionType == 42) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.red_card));
                                            } else if (actionType == 43) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.yellow_card));
                                            } else if (actionType == 44) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.foul));
                                            } else if (actionType == 41) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.offside));
                                            } else if (actionType == 29) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.fighting));
                                            } else if (actionType == 28) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.rescue));
                                            } else if (actionType == 31) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.steals));
                                            } else if (actionType == 27) {
                                                intent.putExtra("gameScore", getResources().getString(R.string.intercept));
                                            }
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        } else {
                                        }
                                    }

                                    @Override
                                    public void onFinish() {

                                    }
                                });

                            }
                        }
                    });
                }
                dialog.show();
                return;
//                T.showShort(MatchScoreLeftActivity.this,"没有选择球员，该数据是否确认不记录到个人");
            }
        }

        Api.getRetrofit().matchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    playerId = 0;
                    Intent intent = new Intent();
                    if (actionType == 42) {
                        intent.putExtra("gameScore", getResources().getString(R.string.red_card));
                    } else if (actionType == 43) {
                        intent.putExtra("gameScore", getResources().getString(R.string.yellow_card));
                    } else if (actionType == 44) {
                        intent.putExtra("gameScore", getResources().getString(R.string.foul));
                    } else if (actionType == 41) {
                        intent.putExtra("gameScore", getResources().getString(R.string.offside));
                    } else if (actionType == 29) {
                        intent.putExtra("gameScore", getResources().getString(R.string.fighting));
                    } else if (actionType == 28) {
                        intent.putExtra("gameScore", getResources().getString(R.string.rescue));
                    } else if (actionType == 31) {
                        intent.putExtra("gameScore", getResources().getString(R.string.steals));
                    } else if (actionType == 27) {
                        intent.putExtra("gameScore", getResources().getString(R.string.intercept));
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.match_score_left_sure:
                matchScore();

                break;
            case R.id.match_score_left_cancel:

                finish();
                break;
        }
    }
}
