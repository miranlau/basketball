package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2016/5/26 0026.
 */
public class MatchScoreRightActivity extends AppCompatActivity implements View.OnClickListener {
    @Bind(R.id.shoot_right_not_score)
    TextView shootRightNotScore;
    @Bind(R.id.shoot_aside)
    TextView shootAside;
    @Bind(R.id.own_goal)
    TextView ownGoal;
    @Bind(R.id.goal_)
    TextView goal;
    @Bind(R.id.abs_layout)
    AbsoluteLayout absLayout;
    @Bind(R.id.cancel)
    TextView cancel;
    @Bind(R.id.have_assist)
    TextView haveAssist;
    @Bind(R.id.no_assist)
    TextView noAssist;
    @Bind(R.id.cancel_goal)
    TextView cancelGoal;
    @Bind(R.id.DQ)
    TextView DQ;
    private int matchId;
    private int teamId;
    private int actionType;
    private int perX;
    private int perY;
    private ArrayList<Display> displays;
    private int intWidth;
    private int intHeight;
    private Display display1;
    private ArrayList<FrameLayout> frameLayouts;
    private int playerId;
    private int playerId2;
    private boolean isAssist = false;
    private boolean isDQ =false;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_technical_statidtics_shoot);
        ButterKnife.bind(this);
        matchId = getIntent().getIntExtra("matchId", 0);//disPlayer
        teamId = getIntent().getIntExtra("teamId", 0);
        displays = (ArrayList<Display>) getIntent().getSerializableExtra("disPlayer");
        status = getIntent().getIntExtra("status",0);

        initView();
    }

    private void initView() {
        shootRightNotScore.setOnClickListener(this);
        shootAside.setOnClickListener(this);
        ownGoal.setOnClickListener(this);
        goal.setOnClickListener(this);
        cancel.setOnClickListener(this);
        haveAssist.setOnClickListener(this);
        noAssist.setOnClickListener(this);
        cancelGoal.setOnClickListener(this);
        DQ.setOnClickListener(this);
         /* 设置图片的宽高 */
        intWidth = ScreenUtils.dp2px(this, 70);
        intHeight = ScreenUtils.dp2px(this, 70);
        perX = (ScreenUtils.dp2px(MatchScoreRightActivity.this, 300) / 25);
        perY = (ScreenUtils.dp2px(MatchScoreRightActivity.this, 300) / 25);

        frameLayouts = new ArrayList<>();
        getLineUp();
        if (status==30){
            isDQ=true;
            shootRightNotScore.setVisibility(View.GONE);
            shootAside.setVisibility(View.GONE);
            ownGoal.setVisibility(View.GONE);
            goal.setVisibility(View.GONE);
            DQ.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);
            haveAssist.setVisibility(View.VISIBLE);
            noAssist.setVisibility(View.VISIBLE);
            haveAssist.setText("射进");
            noAssist.setText("射偏");
        }
    }
//    23	乌龙球
//    25	射偏
//    26	助攻

    //38点球射进
    //39点球射偏

    private void matchScore(final int actionType) {
        final HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", teamId);
        params.put("playerA", playerId);

        if (isAssist) {
            if (playerId2 == 0) {
                ContactOpponentCaptainDialog contactOpponentCaptainDialog = new ContactOpponentCaptainDialog(this, R.style.callDialog, 15);
                contactOpponentCaptainDialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                    @Override
                    public void onConfirmOrder(int i) {
                        if (i==15){
                            playerId2=-1;
                            params.put("playerB", playerId2);
                            params.put("status", actionType);
                            getDataFromSever(params,actionType);
                        }
                    }
                });
                contactOpponentCaptainDialog.show();
//                T.showShort(getApplicationContext(), "请选择助攻队员！");

            }else {
                params.put("playerB", playerId2);
                params.put("status", actionType);
                getDataFromSever(params,actionType);
            }
//            if (playerId==playerId2){
//                T.showShort(getApplicationContext(), "助攻不能选择进球队员！");
//                return;
//            }

            return;
        }else {
            if (actionType==32||actionType==34||actionType==23){
                if (playerId==0){
                    popDialogDQ(params, actionType, 10);

                }else {
                    params.put("status", actionType);
                    getDataFromSever(params,actionType);
                }
                return;
            }else if (actionType==33){
                if (playerId2==0){

                    params.put("status", actionType);
                    getDataFromSever(params, actionType);
                }
                return;
            }else if (playerId==0&&actionType==23){
                popDialogDQ(params,actionType,10);
                return;
            }

//            if (playerId==0&&playerId2!=0){
//                popDialogDQ(params,actionType,10);
//                return;
//            }else if (playerId!=0&&playerId2==0){
//                popDialogDQ(params,actionType,15);
//                return;
//            }else if (playerId==0&&playerId2==0){
//                if (isDQ){
//                    popDialogDQ(params,actionType,10);
//                }else {
//                    popDialogDQ(params,actionType,15);
//                }
//                              return;
//
//            }
            if (isDQ){
                if (playerId==0){
                    popDialogDQ(params,actionType,10);

                }else {
                    params.put("status", actionType);
                    getDataFromSever(params, actionType);
                }
                return;
            }

        }


        params.put("status", actionType);
        getDataFromSever(params, actionType);
    }
    private void getDataFromSever( HashMap<String, Object> params, final int actionType){
        Api.getRetrofit().matchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
//                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    Intent intent = new Intent();
                    if (actionType == 32) {
                        intent.putExtra("gameScore", getResources().getString(R.string.shoot_right_not_score));
                    } else if (actionType == 23) {
                        intent.putExtra("gameScore", getResources().getString(R.string.own_goal));
                    } else if (actionType == 34) {
                        intent.putExtra("gameScore", getResources().getString(R.string.shoot_aside));
                    } else if (actionType == 33) {
                        if (isAssist) {


                            intent.putExtra("gameScore", getResources().getString(R.string.shoot_right_have_assist));
                        } else {
                            intent.putExtra("gameScore", getResources().getString(R.string.shoot_right_have_no_assist));
                        }
                    }else if (actionType==38){
                        intent.putExtra("gameScore", "点球罚进");
                    }else if (actionType==39){
                        intent.putExtra("gameScore", "点球罚丢");
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    private void popDialogDQ(final HashMap<String, Object> params, final int actionType, final int typeForDQ){
        ContactOpponentCaptainDialog contactOpponentCaptainDialog = new ContactOpponentCaptainDialog(this, R.style.callDialog, typeForDQ);
        contactOpponentCaptainDialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
            @Override
            public void onConfirmOrder(int i) {
                if (i == typeForDQ) {
                    params.put("status", actionType);
                    getDataFromSever(params,actionType);
                }
            }
        });
        contactOpponentCaptainDialog.show();
    }
    private void getLineUp() {

        for (Display display : displays) {
            final FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(MatchScoreRightActivity.this).inflate(R.layout.team_details_player_item_three, null);
            ImageView header = (ImageView) frameLayout.findViewById(R.id.player_header);
            TextView number = (TextView) frameLayout.findViewById(R.id.number);
            final ImageView choose = (ImageView) frameLayout.findViewById(R.id.iv_choose);
            ImageLoader.loadCicleImage(MatchScoreRightActivity.this, display.avatar, R.mipmap.default_person, header);
            number.setText(display.number + "");
            absLayout.addView(frameLayout);
            frameLayout.setTag(display);
            frameLayouts.add(frameLayout);
            frameLayout.setLayoutParams
                    (
                            new AbsoluteLayout.LayoutParams
                                    (intWidth, intHeight, ((int) ((display.x)) * perX) -ScreenUtils.dp2px(this, 10), ((int) ((display.y)) * perY) -ScreenUtils.dp2px(this, 20))

                    );
            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                                absLayout.removeAllViews();
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);

                    for (FrameLayout frameLayout1 : frameLayouts) {
                        ImageView imageView = (ImageView) frameLayout1.findViewById(R.id.iv_choose);
                        imageView.setVisibility(View.GONE);
                    }
                    choose.setVisibility(View.VISIBLE);
                    playerId = ((Display) frameLayout.getTag()).id;
                    Log.i("playerId", "进球队员" + "------>" + ((Display) frameLayout.getTag()).name + "id" + "--->" + ((Display) frameLayout.getTag()).id);
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);
//                            }
                }
            });
        }


    }

    private void getLineUp2() {

        for (Display display : displays) {
            final FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(MatchScoreRightActivity.this).inflate(R.layout.team_details_player_item_three, null);
            ImageView header = (ImageView) frameLayout.findViewById(R.id.player_header);
            TextView number = (TextView) frameLayout.findViewById(R.id.number);
            final ImageView choose = (ImageView) frameLayout.findViewById(R.id.iv_choose);
            ImageLoader.loadCicleImage(MatchScoreRightActivity.this, display.avatar, R.mipmap.default_person, header);
            number.setText(display.number + "");
            absLayout.addView(frameLayout);
            frameLayout.setTag(display);
            frameLayouts.add(frameLayout);
            frameLayout.setLayoutParams
                    (
                            new AbsoluteLayout.LayoutParams
                                    (intWidth, intHeight, ((int) ((display.x)) * perX) - ScreenUtils.dp2px(this, 10), ((int) ((display.y)) * perY) - ScreenUtils.dp2px(this, 20))

                    );
            frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                                absLayout.removeAllViews();
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);

                    for (FrameLayout frameLayout1 : frameLayouts) {
                        ImageView imageView = (ImageView) frameLayout1.findViewById(R.id.iv_choose);
                        imageView.setVisibility(View.GONE);
                    }
                    choose.setVisibility(View.VISIBLE);
                    playerId2 = ((Display) frameLayout.getTag()).id;

//                    if (playerId==playerId2){
//                        T.showShort(getApplicationContext(), "助攻不能选择进球队员！");
//                        return;
//                    }
//                    matchScore(33);
                    Log.i("playerId2", "助攻队员" + "------>" + ((Display) frameLayout.getTag()).name);
//                                getLineUp();
//                                choose.setVisibility(View.VISIBLE);
//                            }
                }
            });
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shoot_right_not_score:
                matchScore(32);
                break;
            case R.id.shoot_aside:
                matchScore(34);
                break;
            case R.id.own_goal:
                matchScore(23);
                break;
            case R.id.goal_:
//                if (playerId == 0) {
//                    T.showShort(getApplicationContext(), "请选择进球队员！");
//                    return;
//                }
                shootRightNotScore.setVisibility(View.GONE);
                shootAside.setVisibility(View.GONE);
                ownGoal.setVisibility(View.GONE);
                goal.setVisibility(View.GONE);
                DQ.setVisibility(View.GONE);
                cancel.setVisibility(View.GONE);
                haveAssist.setVisibility(View.VISIBLE);
                noAssist.setVisibility(View.VISIBLE);
                for (FrameLayout frameLayout1 : frameLayouts) {

                    absLayout.removeView(frameLayout1);
                }
                getLineUp2();
                break;
            case R.id.cancel:
                onBackPressed();
                break;
            case R.id.have_assist:
                if (status==30){
//                    if (playerId == 0) {
//                        T.showShort(getApplicationContext(), "请选择进球队员！");
//                        return;
//                    }
                }
                if (isDQ){
                    matchScore(38);
                }else {
                    isAssist = true;
                    matchScore(33);
                }

                break;
            case R.id.no_assist:
                if (status==30){
//                    if (playerId == 0) {
//                        T.showShort(getApplicationContext(), "请选择进球队员！");
//                        return;
//                    }
                }
                if (isDQ){
                    matchScore(39);
                }else {
                    isAssist = false;
                    matchScore(33);
                }

                break;
            case R.id.cancel_goal:
                shootRightNotScore.setVisibility(View.VISIBLE);
                shootAside.setVisibility(View.VISIBLE);
                ownGoal.setVisibility(View.VISIBLE);
                goal.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.VISIBLE);
                haveAssist.setVisibility(View.GONE);
                noAssist.setVisibility(View.GONE);
                cancelGoal.setVisibility(View.GONE);
                for (FrameLayout frameLayout1 : frameLayouts) {

                    absLayout.removeView(frameLayout1);
                }
                getLineUp();
                break;
            case R.id.DQ:
                isDQ =true;
//                if (playerId == 0) {
//                    T.showShort(getApplicationContext(), "请选择进球队员！");
//                    return;
//                }

                shootRightNotScore.setVisibility(View.GONE);
                shootAside.setVisibility(View.GONE);
                ownGoal.setVisibility(View.GONE);
                goal.setVisibility(View.GONE);
                DQ.setVisibility(View.GONE);
                cancel.setVisibility(View.GONE);
                haveAssist.setVisibility(View.VISIBLE);
                noAssist.setVisibility(View.VISIBLE);
                haveAssist.setText("罚进");
                noAssist.setText("罚丢");
                break;

        }
    }
}
