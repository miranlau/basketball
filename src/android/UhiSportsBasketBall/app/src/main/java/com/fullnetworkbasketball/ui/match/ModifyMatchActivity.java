package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePopupWindow;
import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupChooseTeamDialog;
import com.fullnetworkbasketball.ui.dialog.ChooseMatchAssoaciationDialog;
import com.fullnetworkbasketball.ui.home.AvgAgeChoiceActivity;
import com.fullnetworkbasketball.ui.home.CombatChoiceActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.DateTimePickDialogUtil;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/23 0023.
 */
public class ModifyMatchActivity extends BaseActivity {


    TextView tvLaunchMatch;
    TextView launchMatchRecord;
    TextView launchMatchLevel;
    TextView launchMatchAge;
    TextView launchMatchName;
    RatingBar launchMatchEvaluation;
    ImageView launchMatchHeader;
    @Bind(R.id.ll_launch_match)
    LinearLayout llLaunchMatch;
    @Bind(R.id.tv_modify_match)
    TextView tvModifyMatch;
    private LinearLayout ll_change_team;
    private BottomPupChooseTeamDialog bottomPupDialog;
    private LinearLayout ll_opponent_battle;
    private LinearLayout ll_avg_age_choice;
    private int position = 1;
    private ArrayList<Team> myTeams;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;
    private TimePopupWindow pwTime;
    private LinearLayout launch_match_time;
    private TextView time;
    private String initEndDateTime = ""; // 初始化时间
    private LinearLayout ll_launch_match_system;
    private ChooseMatchAssoaciationDialog matchAssoaciationDialog;
    private int system;
    private TextView tv_launch_match_system;
    private LinearLayout ll_launch_match_address;
    private TextView tv_launch_match_address;
    private TextView tv_launch_match_battle;
    private String combat = "";
    private String chooseAge = "";
    private TextView tv_launch_match_age;
    private EditText launch_match_remark;
    private Team team1;
    private Match match;
    private ArrayList<Team> teams;
    private int singleMatch;

    private double  Longitude;
    private double  Latitude; //经纬度

    private String[] array;

    @Override
    protected void setContentView() {
        setContentView(R.layout.modify_match_activity);
        match = (Match) getIntent().getSerializableExtra("match");
        singleMatch = getIntent().getIntExtra("singleMatch", 0);
        system = match.type;
        array = this.getResources().getStringArray(R.array.select_system_array);
        team1 = match.home;
        myTeam();
    }

    @Override
    protected void initializeViews() {

    }
    private void modifyMatch() {
        if (time.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_date));
            return;
        }
        if (tv_launch_match_system.getText().toString().trim().isEmpty()) {
            T.showShort(this, getResources().getString(R.string.input_system));
            return;
        }
        if (singleMatch == 1) {

        } else {
            if (tv_launch_match_address.getText().toString().trim().isEmpty()) {
                T.showShort(this, getResources().getString(R.string.input_address));
                return;
            }
        }

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        //解决部分手机时间不传入秒的情况
        String times = time.getText().toString();
        String findText = ":";
        int count = 0;
        int index = 0;
        while (true) {
            index = times.indexOf(findText, index + 1);
            if (index > 0) {
                count++;
            } else {
                break;
            }
        }
        if (count==1){
            times = times+":00";
        }

        if (singleMatch == 1) {
            params.put("date", times);
            params.put("matchId", match.id);
            params.put("teamId", team1.id);
            params.put("type", system);
        } else {
            params.put("date", times);
            params.put("matchId", match.id);
            params.put("teamId", team1.id);
            params.put("type", system);
            params.put("power", combat);
            params.put("age", chooseAge);
            params.put("desc", launch_match_remark.getText().toString().trim());
            params.put("longitude", App.getInst().getCurrentAddress().latLng.getLongitude());
            params.put("latitude", App.getInst().getCurrentAddress().latLng.getLatitude());
            params.put("addr", tv_launch_match_address.getText().toString().trim());
        }

        Api.getRetrofit().modifyMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    finish();
                } else {

                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {
        if (singleMatch == 1) {
            changeRight(match.home);
        } else {
            changeVIew(match.home);
        }

    }

    //获取自己所属球队，并取出自己创建的球队
    private void myTeam() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        if (UserManager.getIns().getUser() != null) {
            params.put("userId", UserManager.getIns().getUser().id);
        }
        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {

                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    teams = response.getData().teams;
                    myTeams = new ArrayList<Team>();
                    for (Team team : teams) {
                        if (team.leader == UserManager.getIns().getUser().id) {
                            myTeams.add(team);
                        }
                    }

                } else {
                    T.showShort(ModifyMatchActivity.this, response.getMessage());
                }
            }
            @Override
            public void onFinish() {

            }
        });
    }

    private void changeVIew(Team team) {
        View view = LayoutInflater.from(this).inflate(R.layout.launch_match_left_layout, null);

        llLaunchMatch.addView(view);
        ll_change_team = (LinearLayout) view.findViewById(R.id.ll_change_team);
        ll_change_team.setVisibility(View.GONE);
//        ll_change_team.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popDialog();
//            }
//        });
        ll_opponent_battle = (LinearLayout) view.findViewById(R.id.ll_opponent_battle);
        tv_launch_match_battle = (TextView) view.findViewById(R.id.tv_launch_match_battle);
        tv_launch_match_battle.setText(match.power);
        ll_opponent_battle.setOnClickListener(this);

        ll_avg_age_choice = (LinearLayout) view.findViewById(R.id.ll_avg_age_choice);
        tv_launch_match_age = (TextView) view.findViewById(R.id.tv_launch_match_age);
        tv_launch_match_age.setText(match.age);
        ll_avg_age_choice.setOnClickListener(this);
        tvModifyMatch.setOnClickListener(this);
//        tvLaunchMatch.setOnClickListener(this);
        launchMatchRecord = (TextView) view.findViewById(R.id.launch_match_record);
        launchMatchLevel = (TextView) view.findViewById(R.id.launch_match_level);
        launchMatchAge = (TextView) view.findViewById(R.id.launch_match_age);
        launchMatchName = (TextView) view.findViewById(R.id.launch_match_name);
        launchMatchEvaluation = (RatingBar) view.findViewById(R.id.launch_match_evaluation);
        launchMatchHeader = (ImageView) view.findViewById(R.id.launch_match_header);
        launch_match_time = (LinearLayout) view.findViewById(R.id.launch_match_time);
        tv_launch_match_system = (TextView) view.findViewById(R.id.tv_launch_match_system);
        tv_launch_match_system.setText(match.type + getResources().getString(R.string.system));
        launch_match_time.setOnClickListener(this);

        ll_launch_match_system = (LinearLayout) view.findViewById(R.id.ll_launch_match_system);
        ll_launch_match_system.setOnClickListener(this);
        tv_launch_match_address = (TextView) view.findViewById(R.id.tv_launch_match_address);
        tv_launch_match_address.setText(match.addr);
        ll_launch_match_address = (LinearLayout) view.findViewById(R.id.ll_launch_match_address);
        ll_launch_match_address.setOnClickListener(this);
        time = (TextView) view.findViewById(R.id.time);
        time.setText(match.date);
        launch_match_remark = (EditText) view.findViewById(R.id.launch_match_remark);
        launch_match_remark.setText(match.desc);

        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
        ImageLoader.loadCicleImage(ModifyMatchActivity.this, team.logo, R.mipmap.default_header, launchMatchHeader);
        launchMatchName.setText(team.name);
        launchMatchEvaluation.setRating((float) team.stars);
        launchMatchRecord.setText(record + team.win + win + team.deuce + decue + team.lose + lose);
        launchMatchLevel.setText(level + team.level);
        launchMatchAge.setText(avgAge + team.age);
        chooseAge = match.age;
        combat = match.power;
    }

    private void changeRight(Team team) {
        View view = LayoutInflater.from(this).inflate(R.layout.launch_match_right_layout, null);

        llLaunchMatch.addView(view);
        tvModifyMatch.setOnClickListener(this);
        ll_change_team = (LinearLayout) view.findViewById(R.id.ll_launch_single_match_change_team);
        ll_change_team.setVisibility(View.GONE);
//        ll_change_team.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popDialog();
//            }
//        });
        launchMatchRecord = (TextView) view.findViewById(R.id.launch_single_match_record);
        launchMatchLevel = (TextView) view.findViewById(R.id.launch_single_match_level);
        launchMatchAge = (TextView) view.findViewById(R.id.launch_single_match_age);
        launchMatchName = (TextView) view.findViewById(R.id.launch_single_match_name);

        launchMatchEvaluation = (RatingBar) view.findViewById(R.id.launch_single_match_evaluation);
        launchMatchHeader = (ImageView) view.findViewById(R.id.launch_single_match_header);
        launch_match_time = (LinearLayout) view.findViewById(R.id.ll_launch_single_match_time);
        tv_launch_match_system = (TextView) view.findViewById(R.id.tv_launch_single_match_system);
        tv_launch_match_system.setText(match.type + getResources().getString(R.string.system));
        time = (TextView) view.findViewById(R.id.tv_launch_single_match_time);
        time.setText(match.date);
        launch_match_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseTime();
            }
        });
        ll_launch_match_system = (LinearLayout) view.findViewById(R.id.ll_launch_single_match_system);
        ll_launch_match_system.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popChooseSysttem();
            }
        });

        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
        ImageLoader.loadCicleImage(ModifyMatchActivity.this, team.logo, R.mipmap.default_header, launchMatchHeader);
        launchMatchName.setText(team.name);
        launchMatchEvaluation.setRating((float) team.stars);
        launchMatchRecord.setText(record + team.win + win + team.deuce + decue + team.lose + lose);
        launchMatchLevel.setText(level + team.level);
        launchMatchAge.setText(avgAge + team.age);
        system = match.type;

    }

    private void popDialog() {
        if (bottomPupDialog == null) {
            bottomPupDialog = new BottomPupChooseTeamDialog(ModifyMatchActivity.this, R.style.callDialog, 1, myTeams);
            bottomPupDialog.getWindow().setGravity(Gravity.BOTTOM);
            bottomPupDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            bottomPupDialog.setConfirmClickListener(new BottomPupChooseTeamDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(Team team) {
                    team1 = team;
                    llLaunchMatch.removeAllViews();
                    if (singleMatch == 1) {
                        changeRight(team);
                    } else {
                        changeVIew(team);
                    }

//                    refreshSate();
//                    if (position==1){
//                        labelLeft.setSelected(true);
//                        imageLeft.setVisibility(View.VISIBLE);
//                        changeVIew(team1);
//                    }else if (position==2){
//                        labelRight.setSelected(true);
//                        imageRight.setVisibility(View.VISIBLE);
//                        changeRight(team1);
//                    }

                }
            });
        }
        bottomPupDialog.show();

        WindowManager windowManager1 = ModifyMatchActivity.this.getWindowManager();
        Display display1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams lp1 = bottomPupDialog.getWindow().getAttributes();
        lp1.width = (int) (display1.getWidth()); //设置宽度
        bottomPupDialog.getWindow().setAttributes(lp1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.launch_match_time:
                chooseTime();
                break;
            case R.id.ll_launch_match_system:
                popChooseSysttem();
                break;
            case R.id.ll_launch_match_address:
                Intent intent_address = new Intent(ModifyMatchActivity.this,AddAddressActivity.class);
                startActivityForResult(intent_address,10003);
                break;
            case R.id.ll_opponent_battle:
                Intent intent = new Intent(ModifyMatchActivity.this, CombatChoiceActivity.class);
                startActivityForResult(intent, 10001);
                break;
            case R.id.ll_avg_age_choice:
                Intent intent_age = new Intent(ModifyMatchActivity.this, AvgAgeChoiceActivity.class);
                startActivityForResult(intent_age, 10002);
                break;
            case R.id.tv_modify_match: //修改比赛
                modifyMatch();
                break;
        }
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent data) {
        super.onActivityResult(arg0, arg1, data);
        if (arg1 == RESULT_OK) {
            switch (arg0) {
                case 10001:
                    combat = data.getStringExtra("lowest") + "-" + data.getStringExtra("highest");
                    tv_launch_match_battle.setText(combat);
                    break;
                case 10002:
                    chooseAge = data.getStringExtra("youngest") + "-" + data.getStringExtra("oldest");
                    tv_launch_match_age.setText(chooseAge);
                    break;
                case 10003: // TODO address
                    tv_launch_match_address.setText(data.getStringExtra("address"));
                    Longitude = data.getDoubleExtra("Longitude",0);
                    Latitude = data.getDoubleExtra("Latitude",0);

            }
        }
    }

    private void chooseTime() {
        if (Build.VERSION.SDK_INT > 20) {
            pwTime = new TimePopupWindow(this, TimePopupWindow.Type.ALL);
            pwTime.setOnTimeSelectListener(new TimePopupWindow.OnTimeSelectListener() {

                @Override
                public void onTimeSelect(Date date) {
                    Long l = date.getTime();
                    if (l < System.currentTimeMillis()) {
                        T.showShort(ModifyMatchActivity.this, "日期不能小于当前日期！");
                        return;
                    }
                    time.setText(getTime(date));


                }
            });
            pwTime.showAtLocation(time, Gravity.BOTTOM, 0, 0, new Date());

        } else {
            DateTimePickDialogUtil dateTimePicKDialog = new DateTimePickDialogUtil(
                    ModifyMatchActivity.this, initEndDateTime, 1);
            dateTimePicKDialog.dateTimePicKDialog(time);

        }

    }
      //选择赛制
    private void popChooseSysttem() {
        if (matchAssoaciationDialog == null) {
            matchAssoaciationDialog = new ChooseMatchAssoaciationDialog(ModifyMatchActivity.this, R.style.callDialog);
            matchAssoaciationDialog.getWindow().setGravity(Gravity.BOTTOM);
            matchAssoaciationDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
            matchAssoaciationDialog.setConfirmClickListener(new ChooseMatchAssoaciationDialog.OnConfirmOrderListener() {
                @Override
                public void onConfirmOrder(int i) {
                    system = i + 2;
                    tv_launch_match_system.setText(array[i]);
                }
            });
        }
        matchAssoaciationDialog.show();
    }

    public static String getTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(getResources().getString(R.string.modify_match));
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                finish();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
