package com.fullnetworkbasketball.ui.match;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;
public class PhotoViewActivity extends AppCompatActivity {
    /**
     * 图片缩放
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        uk.co.senab.photoview.PhotoView photo = (uk.co.senab.photoview.PhotoView) findViewById(R.id.iv_photo);
        String path = getIntent().getStringExtra("path");
        ImageLoader.loadImage(PhotoViewActivity.this, path, photo);
    }
}
