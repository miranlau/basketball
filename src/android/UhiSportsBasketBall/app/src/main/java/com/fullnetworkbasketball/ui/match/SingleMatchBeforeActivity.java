package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePopupWindow;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.TeamDetailGridAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.BottomPupChooseTeamDialog;
import com.fullnetworkbasketball.ui.dialog.ChooseMatchAssoaciationDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.ui.home.PreRacingActivity;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.MyGridView;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/24 0024.
 */
public class SingleMatchBeforeActivity extends BaseActivity {
    TextView tvLaunchMatch;
    TextView launchMatchRecord;
    TextView launchMatchLevel;
    TextView launchMatchAge;
    TextView launchMatchName;
    RatingBar launchMatchEvaluation;
    ImageView launchMatchHeader;

    @Bind(R.id.tvv_delete_match)
    TextView tvvDeleteMatch;
    @Bind(R.id.tv_edit_match)
    TextView tvEditMatch;
    @Bind(R.id.up)
    FrameLayout up;
    @Bind(R.id.tv_join_the_match)
    TextView tvJoinTheMatch;
    @Bind(R.id.tv_home_player)
    TextView tvHomePlayer;
//    @Bind(R.id.ll_other_team_details_header_of_player_home)
//    LinearLayout llOtherTeamDetailsHeaderOfPlayerHome;
    @Bind(R.id.tv_single_match_before_team_header)
    ImageView tvSingleMatchBeforeTeamHeader;
    @Bind(R.id.tv_single_match_before_stage)
    TextView tvSingleMatchBeforeStage;
    @Bind(R.id.tv_single_match_before_league)
    TextView tvSingleMatchBeforeLeague;
    @Bind(R.id.tv_single_match_before_system)
    TextView tvSingleMatchBeforeSystem;
    @Bind(R.id.tv_single_match_before_name)
    TextView tvSingleMatchBeforeName;
    private LinearLayout ll_change_team;
    private BottomPupChooseTeamDialog bottomPupDialog;
    private LinearLayout ll_opponent_battle;
    private LinearLayout ll_avg_age_choice;
    private int position = 1;
    private ArrayList<Team> myTeams;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;
    private TimePopupWindow pwTime;
    private LinearLayout launch_match_time;
    private TextView time;
    private String initEndDateTime = ""; // 初始化时间
    private LinearLayout ll_launch_match_system;
    private ChooseMatchAssoaciationDialog matchAssoaciationDialog;
    private int system;
    private TextView tv_launch_match_system;
    private LinearLayout ll_launch_match_address;
    private TextView tv_launch_match_address;
    private TextView tv_launch_match_battle;
    private String combat = "";
    private String chooseAge = "";
    private TextView tv_launch_match_age;
    private EditText launch_match_remark;
    private Team team1;
    private Match match;
    private int matchId;
    private ImageView iv_leader;
    private ContactOpponentCaptainDialog dialog;
    private int teamId;
    private Game game;

    private MyGridView myGridView;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_single_match_before);
        matchId = getIntent().getIntExtra("matchId", 0);
        myGridView = (MyGridView) findViewById(R.id.team_detail_sign_up_home_grid);
    }

    @Override
    protected void initializeViews() {
        tvvDeleteMatch.setOnClickListener(this);
        tvEditMatch.setOnClickListener(this);
        tvSingleMatchBeforeTeamHeader.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvv_delete_match:
                if (dialog == null) {
                    dialog = new ContactOpponentCaptainDialog(SingleMatchBeforeActivity.this, R.style.callDialog, 5);
                    dialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {
                        @Override
                        public void onConfirmOrder(int i) {
                            if (i == 5) {
                                deleteMatch();
                            }
                        }
                    });
                }
                dialog.show();

                break;
            case R.id.tv_edit_match:
                Intent intent = new Intent(SingleMatchBeforeActivity.this, ModifyMatchActivity.class);
                intent.putExtra("singleMatch", 1);
                intent.putExtra("match", match);
                startActivity(intent);

                break;
            case R.id.tv_single_match_before_team_header:
                Intent intent1 = new Intent(SingleMatchBeforeActivity.this, OtherTeamDetailsActivity.class);
                intent1.putExtra("teamId", match.home.id);
                startActivity(intent1);
                break;
        }
    }

    private void deleteMatch() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", match.id);
        Api.getRetrofit().deleteMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    finish();
                } else {
                    T.showShort(SingleMatchBeforeActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        queryMatchDetials();
    }

    private void singUp() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("teamId", match.home.id);
        Api.getRetrofit().signUpToMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    queryMatchDetials();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void cacelSingUp() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().cancelSignUpToMatch(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {
                    queryMatchDetials();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void queryMatchDetials() {
//        llOtherTeamDetailsHeaderOfPlayerHome.removeAllViews();
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    match = response.getData();
//                    changeRight(match.home);


                    if (match.status == 10) {
                        tvSingleMatchBeforeLeague.setText(match.date);
                        tvSingleMatchBeforeStage.setText(getResources().getString(R.string.before_match_));
                    } else {
                        if (tvSingleMatchBeforeLeague == null) {

                            return;
                        }
                        tvSingleMatchBeforeLeague.setText(match.homescore + " : " + match.visitingscore);
                        tvSingleMatchBeforeLeague.setTextSize(30);
                        tvSingleMatchBeforeStage.setText(getResources().getString(R.string.matching_));
                    }

                    ImageLoader.loadCicleImage(SingleMatchBeforeActivity.this, match.home.logo, R.mipmap.default_team, tvSingleMatchBeforeTeamHeader);
//                    tvSingleMatchBeforeStage.setText(getResources().getString(R.string.before_match_));
//                    tvSingleMatchBeforeLeague.setText(match.date);
                    tvSingleMatchBeforeSystem.setText(match.type + getResources().getString(R.string.system));
                    tvSingleMatchBeforeName.setText(match.home.name);
                    if (UserManager.getIns().getUser() == null) {
                        up.setVisibility(View.GONE);
                        tvJoinTheMatch.setVisibility(View.GONE);
                        if (match.status == 20) {
                            getCustomActionBar().setTitleText(getResources().getString(R.string.middle_match));
                            up.setVisibility(View.VISIBLE);
                            tvJoinTheMatch.setVisibility(View.GONE);
                            tvvDeleteMatch.setText(getResources().getString(R.string.match_score));
                            tvEditMatch.setText(getResources().getString(R.string.interactive_live));
                            tvvDeleteMatch.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(LoginActivity.class);
                                }
                            });
                            tvEditMatch.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(SingleMatchBeforeActivity.this, InteractiveLiveActivity.class);
                                    intent.putExtra("matchId", match.id);

                                    startActivity(intent);
                                }
                            });
                        }
                    } else {
                        if (match.home.leader == UserManager.getIns().getUser().id) {
                            up.setVisibility(View.VISIBLE);
                            tvJoinTheMatch.setVisibility(View.GONE);
                            if (match.status == 10) {
                                tvvDeleteMatch.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        deleteMatch();
                                    }
                                });
                                tvEditMatch.setText(getResources().getString(R.string.join_the_match));


                                if (match.enroll) {
                                    tvEditMatch.setText(getResources().getString(R.string.cancel_sign_up));
                                    tvEditMatch.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cacelSingUp();
                                        }
                                    });

                                } else if (!match.enroll) {
                                    tvEditMatch.setText(getResources().getString(R.string.join_the_match));
                                    tvEditMatch.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            singUp();
                                        }
                                    });
                                }
                            } else {
                                getCustomActionBar().setTitleText(R.string.middle_match);
                                judgeRecord(match);
                            }

                        } else {

                            if (match.status == 10) {
                                if (match.enroll) {
                                    up.setVisibility(View.GONE);
                                    tvJoinTheMatch.setVisibility(View.VISIBLE);
                                    tvJoinTheMatch.setText(getResources().getString(R.string.cancel_sign_up));
                                    tvJoinTheMatch.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cacelSingUp();
                                        }
                                    });

                                } else if (!match.enroll) {
                                    up.setVisibility(View.GONE);
                                    tvJoinTheMatch.setVisibility(View.VISIBLE);
                                    tvJoinTheMatch.setText(getResources().getString(R.string.join_the_match));
                                    tvJoinTheMatch.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            singUp();
                                        }
                                    });

                                }
                            } else {
                                getCustomActionBar().setTitleText(R.string.middle_match);
                                judgeRecord(match);
                            }

                        }
                    }


                    if (response.getData().homeusers == null || response.getData().homeusers.size() == 0) {
                        tvHomePlayer.setVisibility(View.GONE);

                    } else {
                        tvHomePlayer.setVisibility(View.VISIBLE);

                        TeamDetailGridAdapter adapter = new TeamDetailGridAdapter(SingleMatchBeforeActivity.this, match.homeusers, match.home.leader);
                        myGridView.setAdapter(adapter);
//                        for (final Player player : match.homeusers) {
//                            View view = getLayoutInflater().inflate(R.layout.team_details_player_item_final, null);
//                            ImageView iv_leader = (ImageView) view.findViewById(R.id.iv_leader);
//                            ImageView player_header = (ImageView) view.findViewById(R.id.player_header);
//                            TextView number = (TextView) view.findViewById(R.id.number);
//                            TextView name = (TextView) view.findViewById(R.id.team_details_player_item_name);
//                            name.setText(player.name);
//                            number.setText(player.number + "");
//                            ImageLoader.loadCicleImage(SingleMatchBeforeActivity.this, player.avatar, R.mipmap.default_person, player_header);
//                            if (match.home.leader == player.id) {
//                                iv_leader.setVisibility(View.VISIBLE);
//                            }
//                            llOtherTeamDetailsHeaderOfPlayerHome.addView(view);
//                            view.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    Bundle bundle = new Bundle();
//                                    bundle.putInt("playerId", player.id);
//                                    startActivity(PlayerPersonalActivity.class, bundle);
//                                }
//                            });
//                        }

                    }
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void judgeRecord(Match match) {
        if (match.isrecord) {
            if (match.homerecord == 0) {
                judgeScore(teamId, match);
                return;
            }
            if (UserManager.getIns().getUser().id == match.homerecord) {
                teamId = match.home.id;
                judgeScore(teamId, match);
                return;
            }


            if (match.visitingrecord == 0) {
                judgeScore(teamId, match);
                return;
            }
            if (UserManager.getIns().getUser().id == match.visitingrecord) {
                teamId = match.visiting.id;
                judgeScore(teamId, match);
                return;
            }
        } else {
            if (match.homeusers == null) {
                judgeScore(teamId, match);
                return;
            }
            for (Player player : match.homeusers) {

                if (UserManager.getIns().getUser().id == player.id) {
                    teamId = match.home.id;
                    judgeScore(teamId, match);
                    return;
                }

            }

            if (match.visitingusers == null) {
                judgeScore(teamId, match);
                return;
            }
            for (Player player : match.visitingusers) {
                if (UserManager.getIns().getUser().id == player.id) {
                    teamId = match.visiting.id;
                    judgeScore(teamId, match);
                    return;
                }

            }
        }

        Log.i("teamId", teamId + "");
    }

    private void judgeScore(final int teamId, final Match match) {
        if (teamId == 0) {
            up.setVisibility(View.GONE);
            tvJoinTheMatch.setVisibility(View.VISIBLE);
            tvJoinTheMatch.setText(getResources().getString(R.string.interactive_live));
            tvJoinTheMatch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleMatchBeforeActivity.this, InteractiveLiveActivity.class);
                    intent.putExtra("matchId", match.id);
                    startActivity(intent);
//                                            startActivity(MatchScoreActivity.class);
                }
            });
        } else {
            up.setVisibility(View.VISIBLE);
            tvJoinTheMatch.setVisibility(View.GONE);
            tvvDeleteMatch.setText(getResources().getString(R.string.match_score));
            tvEditMatch.setText(getResources().getString(R.string.interactive_live));
            tvEditMatch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleMatchBeforeActivity.this, InteractiveLiveActivity.class);
                    intent.putExtra("matchId", match.id);
                    startActivity(intent);
                }
            });
            tvvDeleteMatch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (match.status != 100) {
                        isStarting(teamId, match);
                    }

                }
            });

        }
    }

    private void isStarting(final int teamId, final Match match) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();

//                    if (game.statring.size() > 0 && game.statring != null) {
//                        Intent intent = new Intent(SingleMatchBeforeActivity.this, MatchScoreActivity.class);
//                        intent.putExtra("matchId", match.id);
//                        intent.putExtra("teamId", teamId);
//                        intent.putExtra("game", game);
//                        startActivity(intent);
//                    } else {
//                        Intent intent = new Intent(SingleMatchBeforeActivity.this, PreRacingActivity.class);
//                        intent.putExtra("matchId", match.id);
//                        intent.putExtra("teamId", teamId);
//                        intent.putExtra("type",game.type);
//                        startActivity(intent);
//                    }


                    Intent intent = new Intent(SingleMatchBeforeActivity.this, PreRacingActivity.class);
                    intent.putExtra("matchId", match.id);
                    intent.putExtra("teamId", teamId);
                    intent.putExtra("game", game);
                    intent.putExtra("type", game.type);
                    if (game.homeStarting.size() > 0 && game.homeStarting != null) {
                        intent.putExtra("isSureFirst", true);
                    } else {
                        intent.putExtra("isSureFirst", false);
                    }
                    startActivity(intent);

                }else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(getResources().getString(R.string.before_match));
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
