package com.fullnetworkbasketball.ui.match;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.PreRacingActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.T;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 2016/5/27 0027.
 */
public class StartPlayersBeforeMatchActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.ll_players)
    LinearLayout llPlayers;
    @Bind(R.id.scroll_view)
    ScrollView scrollView;
    @Bind(R.id.sure)
    TextView sure;
    @Bind(R.id.cancel)
    TextView cancel;
    /**
     * 当前触摸点相对于屏幕的坐标
     */
    private int mCurrentInScreenX;
    private int mCurrentInScreenY;

    /**
     * 触摸点按下时的相对于屏幕的坐标
     */
    private int mDownInScreenX;
    private int mDownInScreenY;
    @Bind(R.id.rl_start_player)
    RelativeLayout rlStartPlayer;
    @Bind(R.id.abs_view)
    AbsoluteLayout absView;
    /*声明ImageView变量*/
    /*声明相关变量作为存储图片宽高,位置使用*/
    private int intWidth, intHeight, intDefaultX, intDefaultY;
    private float mX, mY;
    /*声明存储屏幕的分辨率变量 */
    private int intScreenX, intScreenY;
    private ImageView header;
    private ArrayList<Player> list;
    private ArrayList<Display> displays;
    private Display display;
    private int perX;
    private int perY;
    private int maxX;
    private int matchId;
    private int teamId;
    private int system;
    private FrameLayout frameLayout;
    private boolean isDelete=false;
    private Game game;
    private Match match;
    private ArrayList<String> list_json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_starting_lineup);
        list = (ArrayList<Player>) getIntent().getSerializableExtra("playerList");
        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        ButterKnife.bind(this);
        initView();
        getSystem();
    }

    private void initView() {
        cancel.setOnClickListener(this);
        perX = (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 300));
        perY = (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 300));
          /* 取得屏幕对象 */
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

	    /* 取得屏幕解析像素 */
        intScreenX = dm.widthPixels;
        intScreenY = dm.heightPixels;

	    /* 设置图片的宽高 */
        intWidth = ScreenUtils.dp2px(this, 70);
        intHeight = ScreenUtils.dp2px(this, 70);
        header = (ImageView) findViewById(R.id.player_header);
        displays = new ArrayList<>();

        sure.setOnClickListener(this);
        refreshView();
    }

    private void refreshView() {
        llPlayers.removeAllViews();
        if (list.size()==0||list==null){
            return;
        }
        for (final Player player : list) {
            View view = LayoutInflater.from(StartPlayersBeforeMatchActivity.this).inflate(R.layout.start_player_before_match_item, null);
            final ImageView header = (ImageView) view.findViewById(R.id.player_header);
            ImageLoader.loadCicleImage(this, player.avatar, R.mipmap.default_person,header);
            TextView name = (TextView) view.findViewById(R.id.name);
            TextView number = (TextView)view.findViewById(R.id.number);
            if (player.realName==null){
                name.setText(player.name);
            }else {
                if (player.realName.isEmpty()){
                    name.setText(player.name);
                }else {
                    name.setText(player.realName);
                }
            }

            number.setText(player.number+"");
            llPlayers.addView(view);
            header.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    int[] location = new int[2];
                    v.getLocationOnScreen(location);
                    maxX = location[0] - ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 80);
                     /*通过findViewById构造器创建ImageView对象*/
//                    mImageView01 = new ImageView(StartPlayersBeforeMatchActivity.this);


                   final FrameLayout mImageView01 = (FrameLayout) LayoutInflater.from(StartPlayersBeforeMatchActivity.this).inflate(R.layout.player_item_new, null);
                    CircleImageView header = (CircleImageView) mImageView01.findViewById(R.id.player_header);
                    TextView number = (TextView) mImageView01.findViewById(R.id.number);
                    ImageView  choose = (ImageView) mImageView01.findViewById(R.id.iv_choose);

                    choose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                Player player1 = (Player) mImageView01.getTag();
                                list.add(player1);
                            Log.i("randy", "球员数量" + "---->" + displays.size()) ;

                            for (Display display:displays){
                                if (player1.id==display.id){
                                    displays.remove(display);
                                    absView.removeView(mImageView01);
                                    break;
                                }
                            }

                            refreshView();
                            isDelete=false;
//                            for (Display display:displays){
//                                if (display.userId==displays.get(displays.size()-1).userId)
//                            }
                        }
                    });
                    frameLayout = mImageView01;
//                    ImageLoader.loadCicleImage(StartPlayersBeforeMatchActivity.this, player.avatar, R.mipmap.default_header, header);
                    number.setText(player.number + "");
        /*将图片从Drawable赋值给ImageView来呈现*/
                    ImageLoader.loadCicleImage(StartPlayersBeforeMatchActivity.this, player.avatar,R.mipmap.default_person, header);
                    mImageView01.setTag(player);
                    absView.addView(mImageView01);
                    frameLayout.setLayoutParams
                            (
                                    new AbsoluteLayout.LayoutParams
                                            (intWidth, intHeight, location[0]-ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this,80) , location[1] )

                            );
                    display = new Display();
                    display.avatar = player.avatar;
                    display.name = player.name;
                    display.number = player.number;
                    display.id = player.id;
                    display.x = mX / (perX / 25);
                    display.y = mY / (perY / 25);
                    displays.add(display);
                    list.remove(player);
                    refreshView();
                    isDelete =true;
                    return false;
                }
            });
        }
    }

    /*自定义一发出信息的方法*/
    public void mMakeTextToast(String str, boolean isLong) {
        if (isLong == true) {
            Toast.makeText(StartPlayersBeforeMatchActivity.this, str, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(StartPlayersBeforeMatchActivity.this, str, Toast.LENGTH_SHORT).show();
        }
    }

    /* 还原ImageView位置的事件处理 */
    public void RestoreButton() {
        intDefaultX = ((intScreenX - intWidth) / 2);
        intDefaultY = ((intScreenY - intHeight) / 2);
        /*Toast还原位置坐标*/
        mMakeTextToast
                (
                        "(" +
                                Integer.toString(intDefaultX) +
                                "," +
                                Integer.toString(intDefaultY) + ")", true
                );

	    /* 以setLayoutParams方法，重新安排Layout上的位置 */
//        mImageView01.setLayoutParams
//                (
//                        new AbsoluteLayout.LayoutParams
//                                (intWidth, intHeight, intDefaultX, intDefaultY)
//
//                );
    }

    /*覆盖触控事件*/
    public boolean onTouchEvent(MotionEvent event) {
        /*取得手指触控屏幕的位置*/
        float x = event.getX();
        float y = event.getY();

        try {
          /*触控事件的处理*/
            switch (event.getAction()) {
            /*点击屏幕*/
                case MotionEvent.ACTION_DOWN:
                        /*通过log 来查看图片位置*/
                    picMove(x, y);
                    Log.i("jay", "按下");
                    break;
            /*移动位置*/
                case MotionEvent.ACTION_MOVE:
                    picMove(x, y);
                    Log.i("jay", "移动");
                    break;
            /*离开屏幕*/
                case MotionEvent.ACTION_UP:
                    picMove(x, y);
                    Log.i("jay", "松开");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /*移动图片的方法*/
    private void picMove(float x, float y) {
        for (Display display1 : displays) {
            if (display1.id == display.id) {
                displays.remove(display1);
            }
        }
        /*默认微调图片与指针的相对位置*/
        mX = x - (intWidth / 2);
        mY = y - (intHeight / 2);

	    /*防图片超过屏幕的相关处理*/
        /*防止屏幕向右超过屏幕*/
        if ((mX) > maxX) {
            mX = maxX;

            if (mY < ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 5)) {
                mY = ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 5);
            }

            if ((mY) > (intScreenY - (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 90)))) {
                mY = intScreenY - (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 90));
            }
        }
	    /*防止屏幕向左超过屏幕*/
        else if (mX < ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 25)) {
            mX = ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 25);
            if (mY < ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 5)) {
                mY = ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 5);
            }

            if ((mY) > (intScreenY - (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 90)))) {
                mY = intScreenY - (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 90));
            }
        }
	    /*防止屏幕向下超过屏幕*/
        else if ((mY) > (intScreenY - (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 90)))) {
            mY = intScreenY - (ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 90));
        }
	    /*防止屏幕向上超过屏幕*/
        else if (mY < ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 5)) {
            mY = ScreenUtils.dp2px(StartPlayersBeforeMatchActivity.this, 5);
        }
	    /*通过log 来查看图片位置*/
        Log.i("jay", Float.toString(mX) + "," + Float.toString(mY));
	    /* 以setLayoutParams方法，重新安排Layout上的位置 */
        frameLayout.setLayoutParams
                (
                        new AbsoluteLayout.LayoutParams
                                (intWidth, intHeight, (int) mX, (int) mY)
                );

        display.x = mX / (perX / 25);
        display.y = mY / (perY / 25);
        if (isDelete){

            displays.add(display);
        }


        for (int i = 0; i < displays.size(); i++) {
            Log.i("randy", "球员数量" + "--------->" + displays.size() + "----" + "X----》" + displays.get(i).x + "Y----->" + displays.get(i).y);
        }


    }

    @Override
    protected void onDestroy() {
//        closeLoadingDialog();
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sure:
                finish();
                break;
            case R.id.cancel:
                startPlayer();
                break;
        }
    }

    private void getSystem() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    match = response.getData();
                    system = match.type;
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    private void isStarting(final int teamId, final Match match){
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId",teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response.isSuccess()) {

                    game =  response.getData();

                    if (game.homeStarting.size()>0 &&game.homeStarting!=null) {
                        Intent intent = new Intent(StartPlayersBeforeMatchActivity.this, MatchScoreActivity.class);
                        intent.putExtra("matchId", match.id);
                        intent.putExtra("teamId", teamId);
                        intent.putExtra("game",game);
                        startActivity(intent);
                        finish();
                    } else  {
                        Intent intent = new Intent(StartPlayersBeforeMatchActivity.this, PreRacingActivity.class);
                        intent.putExtra("matchId", match.id);
                        intent.putExtra("teamId", teamId);
                        startActivity(intent);
                    }

                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    private void startPlayer() {
        if (displays.size() == 0) {
            T.showShort(this, getResources().getString(R.string.start_player));
            return;
        }
        if (displays.size() > system) {
            T.showShort(this, getResources().getString(R.string.system_more));
            return;
        }
        list_json = new ArrayList<>();
        for (Display display:displays){
            String json = new Gson().toJson(display);
            list_json.add(json);
        }

        String json = new Gson().toJson(displays);
        Log.i("list_to_string",json);
        Api.getRetrofit().startPlayerBeforeMatch(json, "", matchId).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    isStarting(teamId,match);

                }else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
}
