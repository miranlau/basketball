package com.fullnetworkbasketball.ui.message;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/6/24 0024.
 */
public class ModifyTeamBillActivity extends BaseActivity {
    @Bind(R.id.modify_team_bill_total)
    EditText modifyTeamBillTotal;
    @Bind(R.id.modify_team_bill_average)
    EditText modifyTeamBillAverage;
    @Bind(R.id.save_modify_team_bill)
    TextView saveModifyTeamBill;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_modify_team_bill);
    }

    @Override
    protected void initializeViews() {
        saveModifyTeamBill.setOnClickListener(this);
        modifyTeamBillTotal.setText(getIntent().getDoubleExtra("total", 0) + "");
        modifyTeamBillAverage.setText(getIntent().getDoubleExtra("averge",0)+"");
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.save_modify_team_bill:
                if (modifyTeamBillTotal.getText().toString().trim().isEmpty()){
                    T.showShort(this,"请输入总金额！");
                    return;
                }
                if (modifyTeamBillAverage.getText().toString().trim().isEmpty()){
                    T.showShort(this,"请输入人均价格！");
                    return;
                }
                Intent  intent=new Intent();
                intent.putExtra("total",Double.parseDouble(modifyTeamBillTotal.getText().toString().trim()));
                intent.putExtra("averge",Double.parseDouble(modifyTeamBillAverage.getText().toString().trim()));
                setResult(RESULT_OK,intent);
                finish();
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText("账本金额");

    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
