package com.fullnetworkbasketball.ui.message;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.db.MySQLite;
import com.fullnetworkbasketball.ui.adapter.PushListAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class PullToRefreshUseActivity extends BaseActivity implements BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private PushListAdapter mQuickAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private int TOTAL_COUNTER;

    private static final int PAGE_SIZE = 5;

    private int delayMillis = 1000;

    private int mCurrentCounter = 0;

    // ArrayList<DynamicReplayData> list = new ArrayList<>();

    MySQLite mySQLite;
    int pageNumber = 1;


    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_push_meaage_list);
        mRecyclerView = (RecyclerView) findViewById(R.id.push_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mySQLite = new MySQLite(PullToRefreshUseActivity.this);
        TOTAL_COUNTER = mySQLite.queryReplyAll().size();
        initAdapter();

        mRecyclerView.setAdapter(mQuickAdapter);
    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void onLoadMoreRequested() {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (mCurrentCounter >= TOTAL_COUNTER) {
                    mQuickAdapter.notifyDataChangedAfterLoadMore(false);
                    View view = getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) mRecyclerView.getParent(), false);
                    mQuickAdapter.addFooterView(view);
                } else {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mQuickAdapter.notifyDataChangedAfterLoadMore(mySQLite.queryReply(pageNumber), true);
                            mCurrentCounter = mQuickAdapter.getItemCount();
                            pageNumber++;
                        }
                    }, delayMillis);
                }
            }



    }

    );
}


    @Override
    public void onRefresh() {
        mySQLite.queryReply(pageNumber).clear();
        //  pageNumber = 1;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mQuickAdapter.setNewData(mySQLite.queryReply(pageNumber));
//                mQuickAdapter.openLoadMore(PAGE_SIZE, true);
                mCurrentCounter = PAGE_SIZE * pageNumber;
                // pageNumber++;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, delayMillis);
    }

    private void initAdapter() {
        mQuickAdapter = new PushListAdapter(PullToRefreshUseActivity.this, R.layout.include_message_item, mySQLite.queryReply(pageNumber));
        mQuickAdapter.openLoadAnimation();
        mRecyclerView.setAdapter(mQuickAdapter);
        mCurrentCounter = mQuickAdapter.getItemCount();
        mQuickAdapter.setOnLoadMoreListener(this);
        mQuickAdapter.openLoadMore(PAGE_SIZE, true);
        //or call mQuickAdapter.setPageSize(PAGE_SIZE);  mQuickAdapter.openLoadMore(true);

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(getResources().getString(R.string.push_reply));

    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }

    }

}
