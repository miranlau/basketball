package com.fullnetworkbasketball.ui.message;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.db.MySQLite;
import com.fullnetworkbasketball.models.ReservationData;
import com.fullnetworkbasketball.ui.adapter.PushYueListAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;

import java.util.ArrayList;

public class PushYueActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {


    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;

    public int pageNumber = 1;

    public PushYueListAdapter adapter;

    public MySQLite mySQLite;

    ArrayList<ReservationData> list = new ArrayList<>();

    public int total;
    private int total_count;
    private int mCurrentCounter = 0;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_push_yue);
    }

    @Override
    protected void initializeViews() {

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        recyclerView = (RecyclerView) findViewById(R.id.push_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(PushYueActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(this);
        mySQLite = new MySQLite(PushYueActivity.this);


    }


    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();

    }

    public void addLoacationSql() {


        if (mySQLite.queryYueAll() == null) {
            return;
        }

        list.addAll(mySQLite.queryYue(pageNumber));
        total_count = mySQLite.queryReplyAll().size();


        adapter = new PushYueListAdapter(PushYueActivity.this, R.layout.include_message_item, list);

        adapter.openLoadMore(5, true);
        adapter.openLoadAnimation();
        lodMore(adapter);
        recyclerView.setAdapter(adapter);
        mCurrentCounter = adapter.getItemCount()*pageNumber;
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

            }
        }, 500);
    }

    private void lodMore(final PushYueListAdapter adapter) {
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {


                        if (mCurrentCounter >= total_count) {
                            adapter.notifyDataChangedAfterLoadMore(false);
                            View view = PushYueActivity.this.getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) recyclerView.getParent(), false);
                            adapter.addFooterView(view);
                        } else {
                            list.clear();
                            try {
                                Thread.sleep(500);

                                adapter.notifyDataChangedAfterLoadMore(mySQLite.queryYue(pageNumber), true);

                                mCurrentCounter = adapter.getData().size();
                                pageNumber++;
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                });
            }
        });

    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRefresh() {
        list.clear();
        pageNumber = 1;
        addLoacationSql();
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(getResources().getString(R.string.event_manager));

    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }

    }
}
