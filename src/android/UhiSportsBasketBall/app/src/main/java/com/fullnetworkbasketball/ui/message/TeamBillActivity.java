package com.fullnetworkbasketball.ui.message;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.MatchBill;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/6/22 0022.
 */
public class TeamBillActivity extends BaseActivity {
    @Bind(R.id.ll_team_bill)
    LinearLayout llTeamBill;
    private int teamId;
    private ArrayList<MatchBill> bills;
    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_team_bill_layout);
        teamId = getIntent().getIntExtra("teamId",0);
    }

    @Override
    protected void initializeViews() {
        getTeamBill();
    }
    private void getTeamBill(){
        llTeamBill.removeAllViews();
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("token", UserManager.getIns().getUser().token);
        params.put("teamId",teamId);
        Api.getRetrofit().teamBillList(params).enqueue(new RequestCallback<HttpResponseFWFArray<MatchBill>>() {
            @Override
            public void onSuccess(HttpResponseFWFArray<MatchBill> response) {
                if (response.isSuccess()){
                    bills = response.getData();
                    for (final MatchBill bill:bills){
                        View view = LayoutInflater.from(TeamBillActivity.this).inflate(R.layout.team_bill_item_layout,null);
                        TextView date = (TextView)view.findViewById(R.id.team_bill_item_date);
                        date.setText(bill.date);

                        TextView address = (TextView)view.findViewById(R.id.team_bill_item_name);
                        address.setText(bill.addr);
                        llTeamBill.addView(view);
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(TeamBillActivity.this,TeamBillActivityTwo.class);
                                intent.putExtra("bill",bill);
                                intent.putExtra("teamId",teamId);
                                intent.putExtra("matchId",bill.id);
                                startActivity(intent);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText("球队账本");
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
