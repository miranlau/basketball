package com.fullnetworkbasketball.ui.message;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Bill;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.TeamBillGridAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/6/22 0022.
 */
public class TeamBillActivityTwo extends BaseActivity {
    @Bind(R.id.team_bill_total_money)
    TextView teamBillTotalMoney;
    @Bind(R.id.team_bill_per_pay)
    TextView teamBillPerPay;
    @Bind(R.id.gv_team_bill)
    GridView gvTeamBill;
    @Bind(R.id.save_team_bill)
    TextView saveTeamBill;
    @Bind(R.id.ll_add_team_bill)
    LinearLayout llAddTeamBill;
    private TeamBillGridAdapter gridAdapter;
    private ArrayList<Integer> arrayList;
    private int[] integer;
    private int matchId;
    private int teamId;
    private double total;
    private double averge;
    private int leaderId;
    private Bill bill;
    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_team_bill_two_layout);
        matchId = getIntent().getIntExtra("matchId",0);
        teamId = getIntent().getIntExtra("teamId",0);
        leaderId = getIntent().getIntExtra("leaderId",0);
        teamBillDetails();
    }
    private void teamBillDetails(){
        arrayList = new ArrayList<>();
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("token", UserManager.getIns().getUser().token);
        params.put("teamId",teamId);
        params.put("matchId",matchId);
        Api.getRetrofit().teamBillDetails(params).enqueue(new RequestCallback<HttpResponseFWF2<Bill>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Bill> response) {
                if (response.isSuccess()) {
                    bill = response.getData();

                    total = bill.total;
                    averge=bill.averge;
                    if (bill.billPayDtos!=null&&bill.billPayDtos.size()>0){
                        for (int i=0;i<bill.billPayDtos.size();i++){
                            if (bill.billPayDtos.get(i).ispay){
                                arrayList.add(bill.billPayDtos.get(i).id);
                            }

                        }
                    }
                    teamBillTotalMoney.setText("￥" + bill.total);
                    teamBillPerPay.setText("￥" + bill.averge);
                    gridAdapter = new TeamBillGridAdapter(TeamBillActivityTwo.this, bill.billPayDtos,leaderId);
                    gvTeamBill.setAdapter(gridAdapter);

                    if (bill.leader){
                        llAddTeamBill.setOnClickListener(TeamBillActivityTwo.this);
                        saveTeamBill.setOnClickListener(TeamBillActivityTwo.this);

                        gvTeamBill.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                ImageView imageView = (ImageView) view.findViewById(R.id.iv_choose);
                                if (imageView.VISIBLE == imageView.getVisibility()) {
                                    imageView.setVisibility(View.GONE);
                                    for (int i = 0; i < arrayList.size(); i++) {
                                        if (bill.billPayDtos.get(position).id == arrayList.get(i)) {
                                            arrayList.remove(i);
                                            break;
                                        }

                                    }


                                } else {
                                    imageView.setVisibility(View.VISIBLE);
                                    arrayList.add(bill.billPayDtos.get(position).id);
                                }
                                integer = new int[arrayList.size()];
                                for (int i = 0; i < arrayList.size(); i++) {

                                    integer[i] = arrayList.get(i);
                                }
//                T.showShort(TeamBillActivityTwo.this, "位置：" + position + "---->" + "数组：" + Arrays.toString(integer));
                            }
                        });
                    }else {
                        saveTeamBill.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }



    @Override
    protected void initializeViews() {




    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_add_team_bill:
                Intent intent = new Intent(this,ModifyTeamBillActivity.class);
                intent.putExtra("total",total);
                intent.putExtra("averge",averge);
                startActivityForResult(intent,10004);
                break;
            case R.id.save_team_bill:
                commit();
                break;
        }
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent data) {
        super.onActivityResult(arg0, arg1, data);
        if (arg1== Activity.RESULT_OK){
            switch (arg0){
                case 10004:
                    total = data.getDoubleExtra("total",0);
                    teamBillTotalMoney.setText(total+"");
                    averge = data.getDoubleExtra("averge",0);
                    teamBillPerPay.setText(averge+"");
                    break;
            }
        }
    }

    private void commit() {

        if (total == 0) {
            T.showShort(this, "请填写总金额！");
            return;
        }
        if (averge == 0) {
            T.showShort(this, "请填写人均价钱！");
            return;
        }


        Api.getRetrofit().commitTeamBill(arrayList, matchId, teamId, total, averge).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    finish();
                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText("球队账本");
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
