package com.fullnetworkbasketball.ui.mine;

import android.view.View;
import android.widget.LinearLayout;

import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.CalendarMonthView;
import com.uhisports.basketball.R;

import java.util.HashMap;

public class HistoryMonthActivity extends BaseActivity {


    private int year;
    private int month;
    // 存放日历控件的容器控件
    private LinearLayout calendarView;
    // 日历控件
    private CalendarMonthView calendarMonthView;

    // 用户有比赛数据的时间
    private HashMap<String, Long> dataMap;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_history_month);
    }

    @Override
    protected void initializeViews() {
        calendarView = (LinearLayout) findViewById(R.id.view_calendar);


        year = getIntent().getIntExtra("year", 2016);
        month = getIntent().getIntExtra("month", 1);
        dataMap = (HashMap<String, Long>) getIntent().getSerializableExtra("data");
        //calendarMonthView = new CalendarMonthView(this, 2016, 1, dataMap);
        calendarMonthView = new CalendarMonthView(this, year, month, dataMap);
        calendarView.addView(calendarMonthView);
    }

    @Override
    protected void initializeData() {
        initListener();
    }

    @Override
    public void onClick(View v) {

    }

    private void initListener() {


        /**
         * 每一天的点击事件
         */
        calendarMonthView.setOnDayClickListener(new CalendarMonthView.OnDayClickListener() {
            @Override
            public void onDayClick(boolean hasData, int day) {
                if (!hasData) {

                    return;
                }
                T.showShort(HistoryMonthActivity.this, "点击了第" + day + "天");
//                long startTime, endTime;
//                startTime = TimeUtils.getDayTime(year, month, day);
//                endTime = startTime + 24 * 60 * 60 * 1000 - 1;
//                getSkiDataListOfPeriod(startTime, endTime);
            }
        });
    }

    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.race_date);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }
}
