package com.fullnetworkbasketball.ui.mine;

import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.DayRaceDataDialog;
import com.fullnetworkbasketball.utils.TimeUtils;
import com.fullnetworkbasketball.views.calendar.ScrollerCalendar;
import com.fullnetworkbasketball.views.calendar.ScrollerCalendarController;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class HistoryRecordActivity extends BaseActivity implements ScrollerCalendarController {

    private DayRaceDataDialog dayRaceDataDialog;

    private ScrollerCalendar scrollerCalendar;
    private TextView monthTv;
    private TextView dayTv;
    // 用户有比赛数据的时间
    HashMap<String, Long> dataMap = new HashMap<>();

    //用户比赛数据时间列表
    ArrayList<Match> matches = new ArrayList<>();
    String date;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_history_record);
    }

    @Override
    protected void initializeViews() {

        monthTv = (TextView) findViewById(R.id.month_tv);
        dayTv = (TextView) findViewById(R.id.day_tv);


        scrollerCalendar = (ScrollerCalendar) findViewById(R.id.scrollerCalendar);

        Calendar calendar = Calendar.getInstance();

        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String[] array = getResources().getStringArray(R.array.array_month);

        monthTv.setText(array[month]);

        dayTv.setText((month + 1) + "-" + day);
        scrollerCalendar.setController(this);

        getHistorySkiTime();
    }

    /**
     * 获取用户哪些时间有比赛数据
     */
    private void getHistorySkiTime() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().queryMyMatchShedule(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {

                if (response.isSuccess()) {

                    ArrayList<Double> list = (ArrayList) response.getData();
                    if (list == null) {
                        return;
                    }

                    for (Double hour : list) {

                        Long time = new Double(hour).longValue();
                        String key = TimeUtils.getYearMonthDay(time);
                        dataMap.put(key, time);

                    }

                    scrollerCalendar.setSkiTime(dataMap);

                }


            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.race_date);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onMonthOfYearSelected(int year, int month, int day) {

        if ((month + 1) > 9 && day < 10) {
            date = year + "-" + month + "-0" + day;
        } else if ((month + 1) < 10 && day > 9) {
            date = year + "-0" + month + "-" + day;
        } else if ((month + 1) < 10 && day < 10) {
            date = year + "-0" + month + "-0" + day;
        } else {
            date = year + "-" + month + "-" + day;
        }
        requestData();


    }


    /**
     * 获取当前天的数据 网络请求
     */

    public void requestData() {
        showLoadingDialog();
        HashMap<String, Object> params = new HashMap<>();

        params.put("date", date);

        Api.getRetrofit().myMatchSheduleDetail(params).enqueue(new RequestCallbackFWF<HttpResponseFWFArray<Match>>() {
            @Override
            public void onSuccess(HttpResponseFWFArray<Match> response) {
                 closeLoadingDialog();
                if (response.isSuccess()) {
                    matches = response.data;

                    if (matches == null || matches.size() == 0) {
                        Toast.makeText(HistoryRecordActivity.this,"数据信息为空",Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        dayRaceDataDialog = new DayRaceDataDialog(HistoryRecordActivity.this, R.style.callDialog, matches, date);
                        dayRaceDataDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                        dayRaceDataDialog.show();
                        WindowManager windowManager1 = HistoryRecordActivity.this.getWindowManager();
                        Display display1 = windowManager1.getDefaultDisplay();
                        WindowManager.LayoutParams lp1 = dayRaceDataDialog.getWindow().getAttributes();
                        lp1.width = (int) (display1.getWidth()); //设置宽度
                        dayRaceDataDialog.getWindow().setAttributes(lp1);
                    }


                }
            }

            @Override
            public void onFinish() {

            }
        });

    }


}
