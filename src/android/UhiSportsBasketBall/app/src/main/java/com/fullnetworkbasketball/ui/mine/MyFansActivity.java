package com.fullnetworkbasketball.ui.mine;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.MyAttentionAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

public class MyFansActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;
    private ArrayList<Player> players;
    private MyAttentionAdapter myAttentionAdapter;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_my_fans);
    }

    @Override
    protected void initializeViews() {

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        recyclerView = (RecyclerView) findViewById(R.id.rv_my_fans);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    public void request() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().myFans(params).enqueue(new RequestCallbackFWF<HttpResponseFWFArray<Player>>(this) {
            @Override
            public void onSuccess(HttpResponseFWFArray<Player> response) {

                if (response.isSuccess()) {

                    players = response.getData();


                    Log.i("player", players.toString());

                    myAttentionAdapter = new MyAttentionAdapter(MyFansActivity.this, R.layout.my_attention_player_item, players);
                    recyclerView.setAdapter(myAttentionAdapter);

                } else {
                    T.showShort(MyFansActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        request();
    }

    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.my_fans);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }
}
