package com.fullnetworkbasketball.ui.mine;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fullnetworkbasketball.models.AllCircleData;
import com.fullnetworkbasketball.models.CircleDynamicData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.CircleFragmentAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

public class MyPublishActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    public int pageNumber = 1;

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;
    private ArrayList<CircleDynamicData> list;
    private int mCurrentCounter = 0;
    private int total;

    public int flag = 0;
    private int total_count;
    private CircleFragmentAdapter adapter;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_my_publish);

    }

    @Override
    protected void initializeViews() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        recyclerView = (RecyclerView) findViewById(R.id.rv_my_publish);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();

    }

    /**
     * cht
     * 网络请求
     */


    public void requestAllDynamicData() {

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("pageNumber", pageNumber);
        Api.getRetrofit().myDynamicList(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllCircleData>>(this) {
            @Override
            public void onSuccess(HttpResponseFWF2<AllCircleData> response) {

                if (response.isSuccess()) {

                    list = response.getData().content;
                    total_count = response.getData().total;
                    total = list.size();
                    if (total > 0) {
                        pageNumber++;
                    }
                    adapter = new CircleFragmentAdapter(MyPublishActivity.this, R.layout.match_details_over_adapter_item_img_txt, list,0);


                    adapter.openLoadMore(list.size(), true);
                    adapter.openLoadAnimation();
                    lodMore(adapter);
                    recyclerView.setAdapter(adapter);
                    mCurrentCounter = adapter.getData().size();

                } else {
                    T.showShort(MyPublishActivity.this, response.getMessage());

                }

            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    public void lodMore(final CircleFragmentAdapter adapter) {

        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {

                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mCurrentCounter >= total_count) {
                            adapter.notifyDataChangedAfterLoadMore(false);
                            View view = MyPublishActivity.this.getLayoutInflater().inflate(R.layout.not_loading, (ViewGroup) recyclerView.getParent(), false);
                            adapter.addFooterView(view);
                        } else {

                            HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                            params.put("pageNumber", pageNumber);


                            Api.getRetrofit().getAllDynamicPager(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AllCircleData>>(MyPublishActivity.this) {
                                @Override
                                public void onSuccess(HttpResponseFWF2<AllCircleData> response) {

                                    if (response.isSuccess()) {

                                        if (response.getData().content.size() > 0) {
                                            adapter.notifyDataChangedAfterLoadMore(response.getData().content, true);
                                            mCurrentCounter = adapter.getData().size();
                                            pageNumber++;
                                        }
                                    }
                                }

                                @Override
                                public void onFinish() {

                                }
                            });
                        }
                    }

                });

            }

        });
    }


    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.my_publish);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onRefresh() {

        pageNumber = 1;
        requestAllDynamicData();
    }
}
