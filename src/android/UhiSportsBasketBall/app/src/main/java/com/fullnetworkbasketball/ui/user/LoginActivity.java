package com.fullnetworkbasketball.ui.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fullnetworkbasketball.models.User;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.home.HomeActivity;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chatuidemo.DemoApplication;
import com.hyphenate.chatuidemo.DemoHelper;
import com.hyphenate.chatuidemo.db.DemoDBManager;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;

public class LoginActivity extends BaseActivity {

    @Bind(R.id.register)
    TextView register;
    @Bind(R.id.account)
    EditText account;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.login)
    TextView login;
    @Bind(R.id.forget_password)
    TextView forgetPassword;
    private String mobile;
    private boolean progressShow;
    private String TAG="huanxin";
    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void initializeViews() {
        register.setOnClickListener(this);
        login.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    private void verification() {
        if (account.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_account);
            return;
        }
        if (password.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_password);
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("username", account.getText().toString().trim());
        params.put("password", password.getText().toString().trim());
        Api.getRetrofit().loginFB(params).enqueue(new RequestCallback<HttpResponseFWF2<User>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<User> response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    if (response.flag == 0) {
                        if (response.code == -1) {
                            T.showShort(getApplicationContext(), response.getMessage());
                        }
                    }
                } else {
                    User user = response.getData();
                    UserManager.getIns().saveUserInfo(user);

                    upDeviceToken();
                    SharePreHelper.getIns().savePassWord(password.getText().toString().trim());
                    T.showShort(getApplicationContext(), response.getMessage());
                    login(user);
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    /**
     * 登录环信
     *
     * @param
     */
    public void login(User user) {
        if (!EaseCommonUtils.isNetWorkConnected(this)) {
            Toast.makeText(this, R.string.network_isnot_available, Toast.LENGTH_SHORT).show();
            return;
        }


        progressShow = true;
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setCanceledOnTouchOutside(false);
        pd.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                Log.d(TAG, "EMClient.getInstance().onCancel");
                progressShow = false;
            }
        });
        pd.setMessage(getString(R.string.Is_landing));
//        pd.show();

        // After logout，the DemoDB may still be accessed due to async callback, so the DemoDB will be re-opened again.
        // close it before login to make sure DemoDB not overlap
        DemoDBManager.getInstance().closeDB();

        // reset current user name before login
        DemoHelper.getInstance().setCurrentUserName(user.username);

        final long start = System.currentTimeMillis();
        // 调用sdk登陆方法登陆聊天服务器
        Log.d(TAG, "EMClient.getInstance().login");
        EMClient.getInstance().login(user.username, user.username, new EMCallBack() {

            @Override
            public void onSuccess() {
                Log.d(TAG, "login: onSuccess");

                if (!LoginActivity.this.isFinishing() && pd.isShowing()) {
                    pd.dismiss();
                }

                // ** 第一次登录或者之前logout后再登录，加载所有本地群和回话
                // ** manually load all local groups and
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();

                // 更新当前用户的nickname 此方法的作用是在ios离线推送时能够显示用户nick
                boolean updatenick = EMClient.getInstance().updateCurrentUserNick(
                        DemoApplication.currentUserNick.trim());
                if (!updatenick) {
                    Log.e("LoginActivity", "update current user nick fail");
                }
                //异步获取当前用户的昵称和头像(从自己服务器获取，demo使用的一个第三方服务)
//				DemoHelper.getInstance().getUserProfileManager().asyncGetCurrentUserInfo();

                // 进入主页面
                startActivity(HomeActivity.class);
                finish();
            }

            @Override
            public void onProgress(int progress, String status) {
                Log.d(TAG, "login: onProgress");
            }

            @Override
            public void onError(final int code, final String message) {
                Log.d(TAG, "login: onError: " + code);
                if (!progressShow) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        pd.dismiss();
                        Toast.makeText(getApplicationContext(), getString(R.string.Login_failed) + message,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:
                Bundle bundle1 = new Bundle();
                bundle1.putInt("isFind", 0);
                startActivity(RegisterActivity.class, bundle1);
                finish();
                break;
            case R.id.login:
                verification();
                break;
            case R.id.forget_password:
                Bundle bundle = new Bundle();
                bundle.putInt("isFind", 1);
                startActivity(RegisterActivity.class, bundle);
                finish();
                break;
        }
    }

    /**
     * 上传deviceToken网络请求
     */
    public void upDeviceToken() {

        //deviceToken
        String deviceToken = JPushInterface.getRegistrationID(getApplicationContext());

        //设备id
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = "" + tm.getDeviceId();
        int deviceType = 0;

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", UserManager.getIns().getUser().id);
        params.put("deviceId", deviceId);
        params.put("deviceToken", deviceToken);
        params.put("deviceType", deviceType);
        Api.getRetrofit().upDeviceToken(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {

            }

            @Override
            public void onFinish() {

            }
        });
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
