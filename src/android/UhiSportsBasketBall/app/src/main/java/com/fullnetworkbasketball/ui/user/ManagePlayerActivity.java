package com.fullnetworkbasketball.ui.user;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.fullnetworkbasketball.models.TeamDetails;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.ManagePlayerAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/12 0012.
 */
public class ManagePlayerActivity extends BaseActivity {
    @Bind(R.id.manage_team_player_listView)
    ListView manageTeamPlayerListView;
    private int teamId;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_manage_player);
    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        teamId = getIntent().getExtras().getInt("teamId", 0);
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        Api.getRetrofit().getTeamDtails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<TeamDetails>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<TeamDetails> response) {
                if (!response.isSuccess()) {

                } else {
                    TeamDetails teamDetails = response.getData();
                    ManagePlayerAdapter managePlayerAdapter = new ManagePlayerAdapter(ManagePlayerActivity.this, teamDetails.players, teamId);
                    manageTeamPlayerListView.setAdapter(managePlayerAdapter);
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.manage_player);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
