package com.fullnetworkbasketball.ui.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fullnetworkbasketball.models.TeamDetails;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.CameraDialog;
import com.fullnetworkbasketball.utils.BitmapUtil;
import com.fullnetworkbasketball.utils.CameraUtil;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2016/5/12 0012.
 */
public class ManageTeamActivity extends BaseActivity {
    @Bind(R.id.manage_team_name)
    TextView manageTeamName;
    @Bind(R.id.manage_player)
    LinearLayout managePlayer;
    @Bind(R.id.modify_code)
    LinearLayout modifyCode;
    @Bind(R.id.broke_team)
    LinearLayout brokeTeam;
    @Bind(R.id.manage_team_header)
    ImageView manageTeamHeader;
    @Bind(R.id.ll_modify_name)
    LinearLayout llModifyName;
    private int teamId;
    private TeamDetails teamDetails;
    private CameraDialog cameraDialog;
    private LoadingDialog loadingDialog;
    private ArrayList<String> url;

    String imagePath;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_manage_team);
    }

    @Override
    protected void initializeViews() {
        managePlayer.setOnClickListener(this);
        modifyCode.setOnClickListener(this);
        brokeTeam.setOnClickListener(this);
        llModifyName.setOnClickListener(this);
        manageTeamHeader.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestData();
        // uploadImage(imagePath);
    }

    @Override
    protected void initializeData() {


    }


    /**
     * cht 网络请求
     */

    public void requestData() {
        teamId = getIntent().getExtras().getInt("teamId", 0);
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        Api.getRetrofit().getTeamDtails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<TeamDetails>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<TeamDetails> response) {
                if (!response.isSuccess()) {

                } else {
                    teamDetails = response.getData();
                    manageTeamName.setText(teamDetails.name);
                    ImageLoader.loadCicleImage(ManageTeamActivity.this, teamDetails.logo, R.mipmap.default_team, manageTeamHeader);
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.manage_player:
                Bundle bundle = new Bundle();
                bundle.putInt("teamId", teamDetails.id);
                startActivity(ManagePlayerActivity.class, bundle);
                break;
            case R.id.modify_code:
                Bundle bundle1 = new Bundle();
                bundle1.putInt("modify", 2);
                bundle1.putInt("teamId", teamDetails.id);
                startActivity(ManageTeamModifyActivity.class, bundle1);
                break;
            case R.id.broke_team:
                Bundle bundle3 = new Bundle();
                bundle3.putInt("modify", 4);
                bundle3.putInt("teamId", teamDetails.id);
                startActivity(ManageTeamModifyActivity.class, bundle3);
                finish();
                break;
            case R.id.manage_team_header:
                if (cameraDialog == null) {
                    cameraDialog = new CameraDialog(this, R.style.callDialog);
                    cameraDialog.getWindow().setGravity(Gravity.BOTTOM);
                    cameraDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                }
                cameraDialog.show();
                WindowManager windowManager1 = getWindowManager();
                Display display1 = windowManager1.getDefaultDisplay();
                WindowManager.LayoutParams lp1 = cameraDialog.getWindow().getAttributes();
                lp1.width = (int) (display1.getWidth()); //设置宽度
                cameraDialog.getWindow().setAttributes(lp1);
                break;
            case R.id.ll_modify_name:
                Bundle bundle2 = new Bundle();
                bundle2.putInt("modify", 3);
                bundle2.putInt("teamId", teamDetails.id);
                startActivity(ManageTeamModifyActivity.class, bundle2);
                break;
        }
    }


    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharePreHelper.getIns().setShouldShowNotification(false);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CameraUtil.TAKE_PHOTO:
                    String path = CameraUtil.getRealFilePath();
                    if (new File(path).exists()) {
                        //图片存在？
                        Logger.i("imagePath:" + path);
                        try {
                            Bitmap temp = BitmapUtil.revitionImageSize(path);
                            imagePath = BitmapUtil.saveBitmap(this, temp, path);
//                            manageTeamHeader.setImageBitmap(temp);
                            //ImageLoader.loadCicleImage(ManageTeamActivity.this, takePath, manageTeamHeader);
                            if (uploadImage(imagePath)) return;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }
                    break;
                case CameraUtil.PICK_PHOTO:
                    try {
                        String pickPath = CameraUtil.resolvePhotoFromIntent(ManageTeamActivity.this, data);
                        Uri uri = Uri.fromFile(new File(pickPath));

                        if (!new File(pickPath).exists()) {
                            Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                        Bitmap header = BitmapUtil.revitionImageSize(pickPath);
                        imagePath = BitmapUtil.saveBitmap(this, header, pickPath);
//                        manageTeamHeader.setImageBitmap(header);
                        // ImageLoader.loadCicleImage(ManageTeamActivity.this, finalPath, manageTeamHeader);
                        if (uploadImage(imagePath)) return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }

    private boolean uploadImage(String imagePath) {
        if (!checkNetWork()) return false;
        File file = new File(imagePath);
        if (!file.exists()) return true;
        RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody boy = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", "logo")
                .addFormDataPart("files", file.getName(), fileBody)

                .build();
        loadingDialog = new LoadingDialog(this);
        loadingDialog.show();
        Api.getRetrofit().uploadFacePicTeam(boy).enqueue(new RequestCallback<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response != null && response.isSuccess()) {
//                    App.getInst().getUser().image = response.getDataFrist().fid;
//                    modifyUserInfo(response.getDataFrist().fid);

                    url = (ArrayList) response.getData();

                    Log.i("url", "网址：" + url.toString());
                    HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                    params.put("logo", url.get(0));
                    params.put("teamId", teamId);
                    Api.getRetrofit().modifyTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
                        @Override
                        public void onSuccess(HttpResponseFWF response) {
                            if (!response.isSuccess()) {

                            } else {
                                ImageLoader.loadCicleImage(ManageTeamActivity.this, url.get(0), R.mipmap.default_team, manageTeamHeader);
                                T.showShort(ManageTeamActivity.this, response.getMessage());
                            }
                        }

                        @Override
                        public void onFinish() {

                        }
                    });


                }
            }

            @Override
            public void onFinish() {
                loadingDialog.dismiss();
            }

        });
        return false;
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.manage_team);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
