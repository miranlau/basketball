package com.fullnetworkbasketball.ui.user;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/13 0013.
 */
public class ManageTeamModifyActivity extends BaseActivity {
    @Bind(R.id.tv_title)
    TextView tvTitle;
    @Bind(R.id.et_input_team_code)
    EditText etInputTeamCode;

    @Bind(R.id.next_step)
    TextView nextStep;
    private int modify;
    private int teamId;
    private int userId;
    //比赛球队 赛事id
    private int matchId;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_manage_team_modify);
        modify = getIntent().getIntExtra("modify", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        userId = getIntent().getIntExtra("userId", 0);


    }

    @Override
    protected void initializeViews() {
        nextStep.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void initializeData() {
        if (modify == 6) {
            etInputTeamCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
            etInputTeamCode.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        if (modify == 5) {
            etInputTeamCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});

            etInputTeamCode.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
    }

    private void join() {

        if (etInputTeamCode.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_anhao);
            return;
        }

        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        params.put("password", etInputTeamCode.getText().toString().trim());
        Api.getRetrofit().joinTeam(params).enqueue(new RequestCallback<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (!response.isSuccess()) {

                } else {


                    finish();
                }

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_step:
                if (modify == 1) {

                    join();
                } else if (modify == 2) {
                    modifyCode();
                } else if (modify == 3) {
                    modifyName();
                } else if (modify == 4) {
                    brokeTeam();
                } else if (modify == 5) {

                    modifyNumber();
                } else if (modify == 6) {
                    racingModifyNumber();
                }
                break;
        }
    }

    /**
     * by  cht
     * <p/>
     * 修改赛前球员 球衣号
     */
    private void racingModifyNumber() {


        if (etInputTeamCode.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_modify_player_number);
            return;
        }
//        if (number.length() > 3) {
//            return;
//        }
//        int nun = Integer.valueOf(number).intValue();
//
//        if (nun < 1 || nun > 100) {
//
//            T.showShort(ManageTeamModifyActivity.this, "请输入1到99之间的号码");
//            return;
//        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("teamId", teamId);

        params.put("userId", userId);
        Log.i("number", "球衣号码：" + etInputTeamCode.getText().toString().trim());
        params.put("number", etInputTeamCode.getText().toString().trim());

        Api.getRetrofit().modifyRacingPlayerNumber(params).enqueue(new RequestCallback<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                if (response.isSuccess()) {

                    T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                    ManageTeamModifyActivity.this.finish();
                } else {
                    T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });


    }

    private void modifyNumber() {
//        etInputTeamCode.setVisibility(View.GONE);
//        etInputTeamCodeNew.setVisibility(View.VISIBLE);

        if (etInputTeamCode.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_modify_player_number);
            return;
        }
//        int nun = Integer.valueOf(etInputTeamCode.getText().toString().trim()).intValue();
//        if (nun < 1 || nun > 100) {
//
//            T.showShort(ManageTeamModifyActivity.this, "请输入1到99之间的号码");
//            return;
//        }
//        if (etInputTeamCode.getText().toString().trim().length() > 3) {
//            return;
//        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        params.put("userId", userId);
        params.put("number", etInputTeamCode.getText().toString().trim());
        Api.getRetrofit().modifyTeamPlayerNumber(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                if (!response.isSuccess()) {

                } else {

                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void modifyName() {

        if (etInputTeamCode.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_anhao);
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        params.put("name", etInputTeamCode.getText().toString().trim());
        Api.getRetrofit().modifyTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                if (!response.isSuccess()) {

                } else {


                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void brokeTeam() {

        if (etInputTeamCode.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_sure_broke_team_to_broke);
            return;
        }
        if (!etInputTeamCode.getText().toString().trim().equals(getResources().getString(R.string.sure_broke))) {
            T.showShort(this, R.string.input_sure_broke_team_to_broke);
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);

        Api.getRetrofit().brokeTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                if (!response.isSuccess()) {

                } else {

                    startActivity(MineActivity.class);
                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void modifyCode() {

        if (etInputTeamCode.getText().toString().trim().isEmpty()) {
            T.showShort(this, R.string.input_anhao);
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("teamId", teamId);
        params.put("password", etInputTeamCode.getText().toString().trim());
        Api.getRetrofit().modifyTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                if (!response.isSuccess()) {

                } else {

                    T.showShort(ManageTeamModifyActivity.this, response.getMessage());
                    finish();
                }
            }

            @Override
            public void onFinish() {

            }
        });

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        String title = "";
        if (modify == 1) {
            title = getResources().getString(R.string.title);
        } else if (modify == 2) {
            title = getResources().getString(R.string.modify_code);
            tvTitle.setText(R.string.input_team_new_code);
            etInputTeamCode.setHint(R.string.input_team_new_code_hint);
            nextStep.setText(R.string.sure);
        } else if (modify == 3) {
            title = getResources().getString(R.string.modify_team_name);
            tvTitle.setText(R.string.input_team_new_name_title);
            etInputTeamCode.setHint(R.string.input_team_new_name);
            nextStep.setText(R.string.sure);
        } else if (modify == 4) {
            title = getResources().getString(R.string.broke_team);
            tvTitle.setText(R.string.input_sure_broke_team_to_broke);
            etInputTeamCode.setHint(R.string.input_sure_broke);
            nextStep.setText(R.string.think_good);
        } else if (modify == 5) {
            title = getResources().getString(R.string.modify_number);
            tvTitle.setText(R.string.input_this_player_number);
            etInputTeamCode.setHint(R.string.input_player_number);
            nextStep.setText(R.string.sure);
        } else if (modify == 6) {
            title = getResources().getString(R.string.modify_number);
            tvTitle.setText(R.string.input_this_player_number);
            etInputTeamCode.setHint(R.string.input_player_number);
            nextStep.setText(R.string.sure);
        }
        getCustomActionBar().setTitleText(title);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
