package com.fullnetworkbasketball.ui.user;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.ShareDialog;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.ui.mine.HistoryRecordActivity;
import com.fullnetworkbasketball.ui.mine.MyFansActivity;
import com.fullnetworkbasketball.ui.mine.MyPublishActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.Utility;
import com.fullnetworkbasketball.views.RadarView;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/4 0004.
 */
public class MineActivity extends BaseActivity {
    @Bind(R.id.mine_my_attention)
    LinearLayout mineMyAttention;
    @Bind(R.id.mine_my_fans)
    LinearLayout mineMyFans;
    @Bind(R.id.mine_my_publish)
    LinearLayout mineMyPublish;
    @Bind(R.id.my_setting)
    TextView mySetting;
    @Bind(R.id.player_header)
    ImageView playerHeader;
    @Bind(R.id.player_name)
    TextView playerName;
    @Bind(R.id.rb_evaluation_mine)
    RatingBar rbEvaluationMine;
    @Bind(R.id.my_team)
    LinearLayout myTeam;
    @Bind(R.id.mine_info_join_match)
    TextView mineInfoJoinMatch;
    @Bind(R.id.mine_info_join_goal) //总得分
    TextView mineInfoJoinGoal;
    @Bind(R.id.mine_info_join_level) //总篮板
    TextView mineInfoJoinLevel;

    @Bind(R.id.tv_scoreCount)
    TextView minScoreCount;
    @Bind(R.id.tv_avgHelp)
    TextView minAvgHelp;
    @Bind(R.id.tv_avgBackboard)
    TextView minAvgBackboard;
   /* @Bind(R.id.tv_two_rate)
    TextView minTworate;*/
    /*@Bind(R.id.tv_three_rate)
    TextView minThreerate;*/
    @Bind(R.id.tv_Freethrow_rate)
    TextView minFreethrowrate;
    @Bind(R.id.tv_avgCover)
    TextView minAvgCover;
    @Bind(R.id.tv_avgSteals)
    TextView minAvgSteals;


    private double[] data;
    RadarView teamDetailsRadar;
      private String attack;
    private String defense;
    private String credit;
    private String physics_fitness;
    private String evaliation;
    private String speed;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;

    private TextView focues;
    private TextView publish;
    private TextView fans;

    private LinearLayout schedule;

    private ShareDialog shareDialog;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_mine_layout);
    }

    @Override
    protected void initializeViews() {
        mineMyAttention.setOnClickListener(this);
        mineMyFans.setOnClickListener(this);
        mineMyPublish.setOnClickListener(this);
        mySetting.setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.schedule_ll)).setOnClickListener(this);

        focues = (TextView) findViewById(R.id.focus_tv);
        publish = (TextView) findViewById(R.id.publish_tv);
        fans = (TextView) findViewById(R.id.fans_tv);
    }

    @Override
    protected void initializeData() {
        attack = getResources().getString(R.string.attack);
        defense = getResources().getString(R.string.defense);
        credit = getResources().getString(R.string.credit);
        evaliation = getResources().getString(R.string.evaluation);
        speed = getResources().getString(R.string.speed);
        physics_fitness = getResources().getString(R.string.physics_fitness);
        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
    }

    @Override
    protected void onResume() {
        super.onResume();

        myTeam.removeAllViews();
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        if (UserManager.getIns().getUser() != null) {
            params.put("userId", UserManager.getIns().getUser().id);
        }

        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallback<HttpResponseFWF2<Player>>(this) {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {
            //    T.showShort(getApplicationContext(), response.getMessage());
                if (!response.isSuccess()) {
                    if (response.flag == 0) {
                        if (response.code == -1) {
                            T.showShort(getApplicationContext(), response.getMessage());
                        }
                    }
                } else {
                    Player player = response.getData();
                    if (player == null || playerName == null) {
                        return;
                    }

                    focues.setText(player.myFocus + "");
                    publish.setText(player.myPublish + "");
                    fans.setText(player.myFans + "");
                    playerName.setText(player.name);
                    Logger.i("name", player.name);
                    rbEvaluationMine.setRating(Utility.formatInt(player.stars));
                    ImageLoader.loadCicleImage(MineActivity.this, player.avatar, R.mipmap.default_person, playerHeader);
                    /*teamDetailsAttack.setText(attack + player.radarDto.jingong);
                    teamDetailsCredit.setText(credit + player.radarDto.credit);
                    teamDetailsDefense.setText(defense + player.radarDto.fangshou);
                    teamDetailsEvaluation.setText(evaliation + player.radarDto.stars);
                    teamDetailsPhysicalFitness.setText(physics_fitness + player.radarDto.tineng);
                    teamDetailsSpeed.setText(speed + player.radarDto.sudu);*/


                    minScoreCount.setText(player.scoreCount+"");
                    minAvgHelp.setText(player.avgHelp+"");
                    minAvgBackboard.setText(player.avgBackboard+"");
                    minFreethrowrate.setText(player.avgSteals+"");
                    minAvgCover.setText(player.avgCover+"");
                    minAvgSteals.setText(player.avgAnError+"");


                    mineInfoJoinMatch.setText(getResources().getString(R.string.join_match) + player.matches);
                    mineInfoJoinGoal.setText(getResources().getString(R.string.goal_player_details) + player.score);
                    mineInfoJoinLevel.setText(getResources().getString(R.string.level) + player.backboard);
                   /* data = new double[6];
                    data[0] = player.radarDto.fangshou;

                    data[1] = player.radarDto.stars;

                    data[2] = player.radarDto.sudu;
                    data[3] = player.radarDto.jingong;
                    data[4] = player.radarDto.credit;
                    data[5] = player.radarDto.tineng;
                    teamDetailsRadar = new RadarView(MineActivity.this, data);
                    *//** 设置旋转动画 *//*
                    final RotateAnimation animation = new RotateAnimation(0f, 30f, Animation.RELATIVE_TO_SELF,
                            0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    animation.setDuration(100);//设置动画持续时间
                    *//** 常用方法 *//*
                    animation.setRepeatCount(0);//设置重复次数
                    animation.setFillAfter(true);//动画执行完后是否停留在执行完的状态
                    teamDetailsRadar.startAnimation(animation);
                    llRadar.addView(teamDetailsRadar);*/
                    for (final Team team : player.teams) {
                        View view = LayoutInflater.from(MineActivity.this).inflate(R.layout.team_item_layout, null);
                        ImageView my_team_header = (ImageView) view.findViewById(R.id.my_team_header);
                        ImageLoader.loadCicleImage(MineActivity.this, team.logo, R.mipmap.default_team, my_team_header);
                        TextView my_name_name = (TextView) view.findViewById(R.id.my_name_name);
                        my_name_name.setText(team.name);
                        TextView my_team_level = (TextView) view.findViewById(R.id.my_team_level);
                        my_team_level.setText(team.level + getResources().getString(R.string.team_level));
                        RatingBar rb_evaluation_my_team = (RatingBar) view.findViewById(R.id.rb_evaluation_my_team);
                        rb_evaluation_my_team.setRating(Utility.formatInt(team.stars));
                        TextView my_team_record = (TextView) view.findViewById(R.id.my_team_record);
                        my_team_record.setText(record + team.win + win + "  " + team.lose + lose);
                        TextView my_team_avg_age = (TextView) view.findViewById(R.id.my_team_avg_age);
                        my_team_avg_age.setText(avgAge + team.age);
                        myTeam.addView(view);
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putInt("teamId", team.id);
                                startActivity(OtherTeamDetailsActivity.class, bundle);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFinish() {

            }

        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mine_my_attention:
                startActivity(MyAttentionActivity.class);
                break;
            case R.id.mine_my_fans:
                startActivity(MyFansActivity.class);
                break;
            case R.id.mine_my_publish:
                startActivity(MyPublishActivity.class);
                break;
            case R.id.my_setting:
                startActivity(MyInfoSettingActivity.class);
                break;
            case R.id.schedule_ll:
                startActivity(HistoryRecordActivity.class);
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setRightImageView(R.mipmap.share);
        getCustomActionBar().setTitleText(R.string.mine);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_image:


                String path = Api.getWebBaseUrl() + "ctl/player/share?userId=" + UserManager.getIns().getUser().id;
                if (shareDialog == null) {

                    shareDialog = new ShareDialog(MineActivity.this, R.style.callDialog, 0, path);
                    shareDialog.getWindow().setGravity(Gravity.BOTTOM);
                }

                shareDialog.show();


                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}


