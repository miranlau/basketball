package com.fullnetworkbasketball.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.IdCardUtils;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/9 0009.
 */
public class ModifyDataActivity extends BaseActivity {
    @Bind(R.id.input_data)
    EditText inputData;
    @Bind(R.id.delete)
    ImageView delete;
    private int type;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_modify_data);
        type = getIntent().getIntExtra("type", 0);

    }

    @Override
    protected void initializeViews() {
        delete.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {
        inputData.setText(getIntent().getStringExtra("data"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delete:
                inputData.setText("");
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().getRightText().setVisibility(View.VISIBLE);
        getCustomActionBar().getRightText().setTextColor(getResources().getColor(R.color.white));
        getCustomActionBar().getRightText().setText(R.string.save);
        if (type == 1) {
            getCustomActionBar().setTitleText(R.string.modify_username);
        } else if (type == 2 || type == 3 || type == 4) {
            getCustomActionBar().setTitleText(R.string.modify_data);
        } else if (type == 5) {
            getCustomActionBar().setTitleText(R.string.modify_id_card);
        } else if (type==6){
            getCustomActionBar().setTitleText(R.string.modify_username);
        }
        if (type == 2) {
            inputData.setInputType(InputType.TYPE_CLASS_NUMBER);
            inputData.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});

        } else if (type == 3) {
            inputData.setInputType(InputType.TYPE_CLASS_NUMBER);
            inputData.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});

        } else if (type == 4) {
            inputData.setInputType(InputType.TYPE_CLASS_NUMBER);
            inputData.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        } else if (type == 1) {
            inputData.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        } else if (type == 5) {
        }
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
            case R.id.actionbar_right_txt:
                if (inputData.getText().toString().trim().isEmpty()) {
                    T.showShort(this, R.string.input_data_id_card);
                    return;
                }
                try {


                    if (!IdCardUtils.IDCardValidate(inputData.getText().toString().trim()) && type == 5) {

                        T.showShort(this, R.string.input_invalid_card);
                        return;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

                String dataType = "";
                if (type == 2) {
                    dataType = "height";
                } else if (type == 3) {
                    dataType = "weight";
                } else if (type == 4) {
                    dataType = "age";
                } else if (type == 1) {
                    dataType = "name";
                } else if (type == 5) {
                    dataType = "idcard";
                } else if (type==6){
                    dataType = "realName";
                }
                params.put(dataType, inputData.getText().toString().trim());
                Api.getRetrofit().modifyData(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
                    @Override
                    public void onSuccess(HttpResponseFWF response) {
                        Logger.i("msg:" + response.toString());
                        if (!response.isSuccess()) {
                            T.showShort(ModifyDataActivity.this, response.getMessage());
                        } else {
//                            T.showShort(ModifyDataActivity.this, getResources().getString(R.string.modify_card_true));
                            Intent intent = new Intent();
                            intent.putExtra("modifyData", inputData.getText().toString().trim());
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFinish() {

                    }
                });
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
