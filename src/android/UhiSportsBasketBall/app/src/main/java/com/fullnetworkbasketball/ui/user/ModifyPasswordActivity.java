package com.fullnetworkbasketball.ui.user;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/9 0009.
 */
public class ModifyPasswordActivity extends BaseActivity {
    @Bind(R.id.set_pwd_account)
    TextView setPwdAccount;
    @Bind(R.id.set_old_password)
    EditText setOldPassword;
    @Bind(R.id.set_pwd_new)
    EditText setPwdNew;
    @Bind(R.id.set_pwd_again)
    EditText setPwdAgain;
    @Bind(R.id.forget_password)
    TextView forgetPassword;
    @Bind(R.id.set_pwd_sure)
    TextView setPwdSure;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_modify_password);
    }

    @Override
    protected void initializeViews() {
        forgetPassword.setOnClickListener(this);
        setPwdSure.setOnClickListener(this);
        setPwdAccount.setText(UserManager.getIns().getUser().mobile);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forget_password:
                Bundle bundle = new Bundle();
                bundle.putInt("isFind",1);
                startActivity(RegisterActivity.class,bundle);
                break;
            case R.id.set_pwd_sure:
                verifycation();
                break;

        }
    }
    private void verifycation(){
        if (setOldPassword.getText().toString().trim().isEmpty()){
            T.showShort(this, R.string.input_old_passwprd);
            return;
        }
        if (setPwdNew.getText().toString().trim().isEmpty()){
            T.showShort(this, R.string.input_new_password);
            return;
        }
        if (setPwdAgain.getText().toString().trim().isEmpty()){
            T.showShort(this, R.string.input_new_password_again);
            return;
        }
        if (!TextUtils.equals(setPwdNew.getText().toString().trim(),setPwdAgain.getText().toString().trim())){
            T.showShort(this, R.string.pwd_error);
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("mobile",setPwdAccount.getText().toString().trim() );
        params.put("oldpsd",setOldPassword.getText().toString().trim());
        params.put("newpsd",setPwdNew.getText().toString().trim());
        Api.getRetrofit().modifyPWD(params).enqueue(new RequestCallback<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    T.showShort(getApplicationContext(), response.getMessage());

                } else {

                    SharePreHelper.getIns().savePassWord(setPwdNew.getText().toString().trim());
                    T.showShort(getApplicationContext(), response.getMessage());
                    finish();

                }
            }

            @Override
            public void onFinish() {

            }
        });
    }
    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.modify_password);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
