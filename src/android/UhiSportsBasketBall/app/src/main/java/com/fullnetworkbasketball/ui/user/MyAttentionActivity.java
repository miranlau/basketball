package com.fullnetworkbasketball.ui.user;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.models.League;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWFArray;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.adapter.CompetitionTeamAdapter;
import com.fullnetworkbasketball.ui.adapter.FindRecyclerViewAdapter;
import com.fullnetworkbasketball.ui.adapter.MyAttentionAdapter;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/4 0004.
 */
public class MyAttentionActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.label_mine_player)
    TextView labelMinePlayer;
    @Bind(R.id.image_mine_player)
    ImageView imageMinePlayer;
    @Bind(R.id.filter_football_mine_player)
    RelativeLayout filterFootballMinePlayer;
    @Bind(R.id.label_mine_league_match)
    TextView labelMineLeagueMatch;
    @Bind(R.id.image_mine_league_match)
    ImageView imageMineLeagueMatch;
    @Bind(R.id.filter_football_mine_league_match)
    RelativeLayout filterFootballMineLeagueMatch;
    //    @Bind(R.id.recyclerView_my_attention)
//    XRecyclerView recyclerViewMyAttention;
    private MyAttentionAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;

    private RelativeLayout filterFootballMineTeam;

    private TextView labelMineTeam;
    private ImageView imageMineTeam;

    public int flag = 0;

    //我关注的球员
    public ArrayList<Player> players;

    public ArrayList<League> leagues;

    public ArrayList<Team> teams;

    public MyAttentionAdapter myAttentionAdapter;

    public FindRecyclerViewAdapter findRecyclerViewAdapter;

    public CompetitionTeamAdapter competitionTeamAdapter;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_my_attention);
    }

    @Override
    protected void initializeViews() {
        filterFootballMineTeam = (RelativeLayout) findViewById(R.id.filter_football_mine_team);
        labelMineTeam = (TextView) findViewById(R.id.label_mine_team);
        imageMineTeam = (ImageView) findViewById(R.id.image_mine_team);

        filterFootballMineTeam.setOnClickListener(this);
        filterFootballMinePlayer.setOnClickListener(this);
        filterFootballMineLeagueMatch.setOnClickListener(this);
        refreshState();
        labelMinePlayer.setSelected(true);
        imageMinePlayer.setVisibility(View.VISIBLE);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        recyclerView = (RecyclerView) findViewById(R.id.rv_my_attention);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    protected void initializeData() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_football_mine_player:
                flag = 0;
                refreshState();
                recyclerView.removeAllViews();
                labelMinePlayer.setSelected(true);
                imageMinePlayer.setVisibility(View.VISIBLE);
                onRefresh();
                break;
            case R.id.filter_football_mine_team:
                flag = 1;

                refreshState();
                recyclerView.removeAllViews();
                labelMineTeam.setSelected(true);
                imageMineTeam.setVisibility(View.VISIBLE);
                onRefresh();
                break;
            case R.id.filter_football_mine_league_match:
                flag = 2;
                refreshState();
                recyclerView.removeAllViews();
                labelMineLeagueMatch.setSelected(true);
                imageMineLeagueMatch.setVisibility(View.VISIBLE);

                onRefresh();
                break;
        }
    }

    private void refreshState() {
        labelMinePlayer.setSelected(false);
        imageMinePlayer.setVisibility(View.GONE);
        labelMineLeagueMatch.setSelected(false);
        imageMineLeagueMatch.setVisibility(View.GONE);
        labelMineTeam.setSelected(false);
        imageMineTeam.setVisibility(View.GONE);

    }

    /**
     * cht
     * 网络请求
     * 我关注的球员
     */

    public void requestMyFoucesPlayer() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().myFocusPlayer(params).enqueue(new RequestCallbackFWF<HttpResponseFWFArray<Player>>(this) {
            @Override
            public void onSuccess(HttpResponseFWFArray<Player> response) {

                if (response.isSuccess()) {

                    players = response.getData();

                    Log.i("player", players.toString());

                    myAttentionAdapter = new MyAttentionAdapter(MyAttentionActivity.this, R.layout.my_attention_player_item, players);
                    recyclerView.setAdapter(myAttentionAdapter);

                } else {
                    T.showShort(MyAttentionActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    /**
     * 我关注的联赛
     */

    public void requestMyFoucesMatch() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().myFocusLeague(params).enqueue(new RequestCallbackFWF<HttpResponseFWFArray<League>>(this) {
            @Override
            public void onSuccess(HttpResponseFWFArray<League> response) {

                if (response.isSuccess()) {

                    leagues = response.getData();


                    findRecyclerViewAdapter = new FindRecyclerViewAdapter(MyAttentionActivity.this, R.layout.fragment_find_adapter_item, leagues);
                    recyclerView.setAdapter(findRecyclerViewAdapter);

                } else {
                    T.showShort(MyAttentionActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 我关注的球队
     */

    public void requestMyFoucesTeam() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().myFocusTeam(params).enqueue(new RequestCallbackFWF<HttpResponseFWFArray<Team>>(this) {
            @Override
            public void onSuccess(HttpResponseFWFArray<Team> response) {

                if (response.isSuccess()) {

                    teams = response.getData();


                    competitionTeamAdapter = new CompetitionTeamAdapter(MyAttentionActivity.this, R.layout.competition_team_adapter_item, teams);
                    recyclerView.setAdapter(competitionTeamAdapter);

                } else {
                    T.showShort(MyAttentionActivity.this, response.getMessage());
                }
            }

            @Override
            public void onFinish() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.my_attention);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onRefresh() {

        if (flag == 0) {
            requestMyFoucesPlayer();
        } else if (flag == 1) {

            requestMyFoucesTeam();
        } else {
            requestMyFoucesMatch();
        }
    }
}
