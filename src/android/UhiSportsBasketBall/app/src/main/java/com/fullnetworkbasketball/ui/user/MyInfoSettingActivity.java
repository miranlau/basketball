package com.fullnetworkbasketball.ui.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fullnetworkbasketball.models.User;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.CameraDialog;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.utils.BitmapUtil;
import com.fullnetworkbasketball.utils.CameraUtil;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.LoadingDialog;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2016/5/9 0009.
 */
public class MyInfoSettingActivity extends BaseActivity {
    @Bind(R.id.my_header)
    ImageView myHeader;
    @Bind(R.id.modify_password)
    LinearLayout modifyPassword;
    @Bind(R.id.modify_username)
    LinearLayout modifyUsername;
    @Bind(R.id.modify_height)
    LinearLayout modifyHeight;
    @Bind(R.id.modify_weight)
    LinearLayout modifyWeight;
    @Bind(R.id.modify_age)
    LinearLayout modifyAge;
    @Bind(R.id.modify_id_card)
    LinearLayout modifyIdCard;
    @Bind(R.id.tv_userName)
    TextView tvUserName;
    @Bind(R.id.tv_height)
    TextView tvHeight;
    @Bind(R.id.tv_weight)
    TextView tvWeight;
    @Bind(R.id.tv_age)
    TextView tvAge;
    @Bind(R.id.tv_id_card)
    TextView tvIdCard;
    @Bind(R.id.login_out)
    LinearLayout loginOut;
    @Bind(R.id.mobile)
    TextView mobile;
    @Bind(R.id.tv_realName)
    TextView tvRealName;
    @Bind(R.id.modify_realName)
    LinearLayout modifyRealName;
    private CameraDialog cameraDialog;
    private LoadingDialog loadingDialog;
    private ArrayList<String> url;
    private ContactOpponentCaptainDialog dialog;

    private CheckBox seeCb;
    private CheckBox chatCb;
    private CheckBox updataCb;
    private CheckBox updataPhoneCb;

    private boolean isCanview;
    private boolean isCanUpdata;
    private boolean isChatpush;
    private boolean isUpdataPhone;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_my_info_setting);
    }

    @Override
    protected void initializeViews() {
        myHeader.setOnClickListener(this);
        modifyPassword.setOnClickListener(this);
        modifyUsername.setOnClickListener(this);
        modifyHeight.setOnClickListener(this);
        modifyWeight.setOnClickListener(this);
        modifyAge.setOnClickListener(this);
        modifyIdCard.setOnClickListener(this);
        loginOut.setOnClickListener(this);
        modifyRealName.setOnClickListener(this);


        seeCb = (CheckBox) findViewById(R.id.see_my_data_cb);
        chatCb = (CheckBox) findViewById(R.id.chat_notify_cb);
        updataCb = (CheckBox) findViewById(R.id.updata_cb);
        updataPhoneCb = (CheckBox) findViewById(R.id.telephone_directory_cb);


        isUpdataPhone = SharePreHelper.getIns().getIsUpDataPhone();

        updataPhoneCb.setChecked(isUpdataPhone);

    }

    public void request() {
        url = new ArrayList<String>();
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("userId", UserManager.getIns().getUser().id);

        Log.i("userId", "userId:" + UserManager.getIns().getUser().id);
        Api.getRetrofit().getUserInfo(params).enqueue(new RequestCallback<HttpResponseFWF2<User>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<User> response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    if (response.flag == 0) {
                        if (response.code == -1) {
                            T.showShort(getApplicationContext(), response.getMessage());
                        }
                    }
                } else {

                    Log.i("dataMy", response.getData().toString());
                    User user = response.getData();


                    tvUserName.setText(user.name);
                    tvHeight.setText(user.height + "");
                    tvWeight.setText(user.weight + "");
                    tvAge.setText(user.age + "");
                    tvRealName.setText(user.realName);

                    isCanUpdata = user.autoupdate;
                    isCanview = user.canview;
                    isChatpush = user.chatpush;


                    seeCb.setChecked(isCanview);
                    chatCb.setChecked(isChatpush);
                    updataCb.setChecked(isCanUpdata);
                    if (isChatpush){
                        SharePreHelper.getIns().setChatOpen(true);
                    }else {
                        SharePreHelper.getIns().setChatOpen(false);
                    }

                    if (TextUtils.equals("null", user.idcard)) {
                        tvIdCard.setText("");
                    } else {
                        tvIdCard.setText(user.idcard + "");
                    }

                    mobile.setText(user.mobile);
                    ImageLoader.loadCicleImage(MyInfoSettingActivity.this, user.avatar, R.mipmap.default_person, myHeader);

                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {

        request();

        seeCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isCanview != isChecked) {
                    changeCanView(isChecked, 0);
                }
//
            }
        });
        chatCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChatpush != isChecked) {
                    changeCanView(isChecked, 1);
                }

            }
        });
        updataCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {


                if (isCanUpdata != isChecked) {
                    changeCanView(isChecked, 2);
                }

            }
        });

        updataPhoneCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {


                if (isUpdataPhone != isChecked) {
                    SharePreHelper.getIns().setIsUpDataPhone(updataPhoneCb.isChecked());
                    T.showShort(MyInfoSettingActivity.this, getResources().getString(R.string.save_success));
                } else {
                    SharePreHelper.getIns().setIsUpDataPhone(updataPhoneCb.isChecked());
                    T.showShort(MyInfoSettingActivity.this, getResources().getString(R.string.save_success));
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.my_header:
                if (cameraDialog == null) {
                    cameraDialog = new CameraDialog(this, R.style.callDialog);
                    cameraDialog.getWindow().setGravity(Gravity.BOTTOM);
                    cameraDialog.getWindow().setWindowAnimations(R.style.dialogAnimation);
                }
                cameraDialog.show();
                WindowManager windowManager1 = getWindowManager();
                Display display1 = windowManager1.getDefaultDisplay();
                WindowManager.LayoutParams lp1 = cameraDialog.getWindow().getAttributes();
                lp1.width = (int) (display1.getWidth()); //设置宽度
                cameraDialog.getWindow().setAttributes(lp1);
                break;
            case R.id.modify_password:
                startActivity(ModifyPasswordActivity.class);
                break;
            case R.id.modify_username:

                Intent intent_name = new Intent(this, ModifyDataActivity.class);
                intent_name.putExtra("type", 1);
                intent_name.putExtra("data", tvUserName.getText().toString().trim());
                startActivityForResult(intent_name, 10001);
                break;
            case R.id.modify_height:

                Intent intent_height = new Intent(this, ModifyDataActivity.class);
                intent_height.putExtra("type", 2);
                intent_height.putExtra("data", tvHeight.getText().toString().trim());
                startActivityForResult(intent_height, 10002);
                break;
            case R.id.modify_weight:
                Intent intent_weight = new Intent(this, ModifyDataActivity.class);
                intent_weight.putExtra("type", 3);

                intent_weight.putExtra("data", tvWeight.getText().toString().trim());
                startActivityForResult(intent_weight, 10003);
                break;
            case R.id.modify_age:
                Intent intent_age = new Intent(this, ModifyDataActivity.class);
                intent_age.putExtra("type", 4);
                intent_age.putExtra("data", tvAge.getText().toString().trim());

                startActivityForResult(intent_age, 10004);
                break;
            case R.id.modify_id_card:
                Intent intent_id_card = new Intent(this, ModifyDataActivity.class);
                intent_id_card.putExtra("type", 5);
                intent_id_card.putExtra("data", tvIdCard.getText().toString().trim());
                startActivityForResult(intent_id_card, 10005);
                break;
            case R.id.login_out:
                if (dialog == null) {
                    dialog = new ContactOpponentCaptainDialog(MyInfoSettingActivity.this, R.style.callDialog, 3);
                }
                dialog.show();
                break;
            case R.id.modify_realName:
                Intent intent_realName = new Intent(this, ModifyDataActivity.class);
                intent_realName.putExtra("type", 6);
                intent_realName.putExtra("data", tvRealName.getText().toString().trim());
                startActivityForResult(intent_realName, 10006);
                break;
        }
    }


    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharePreHelper.getIns().setShouldShowNotification(false);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CameraUtil.TAKE_PHOTO:
                    String path = CameraUtil.getRealFilePath();
                    if (new File(path).exists()) {
                        //图片存在？
                        Logger.i("imagePath:" + path);
                        try {
                            Bitmap temp = BitmapUtil.revitionImageSize(path);
                            path = BitmapUtil.saveBitmap(this, temp, path);
//                            myHeader.setImageBitmap(temp);
                            if (uploadImage(path)) return;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }
                    break;
                case CameraUtil.PICK_PHOTO:
                    try {
                        String pickPath = CameraUtil.resolvePhotoFromIntent(MyInfoSettingActivity.this, data);
                        Uri uri = Uri.fromFile(new File(pickPath));

                        if (!new File(pickPath).exists()) {
                            Toast toast = Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                        Bitmap header = BitmapUtil.revitionImageSize(pickPath);
                        String finalPath = BitmapUtil.saveBitmap(this, header, pickPath);
//                        myHeader.setImageBitmap(header);
                        if (uploadImage(finalPath)) return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 10001:
                    tvUserName.setText(data.getStringExtra("modifyData"));
                    break;
                case 10002:
                    tvHeight.setText(data.getStringExtra("modifyData"));
                    break;
                case 10003:
                    tvWeight.setText(data.getStringExtra("modifyData"));
                    break;
                case 10004:
                    tvAge.setText(data.getStringExtra("modifyData"));
                    break;
                case 10005:
                    tvIdCard.setText(data.getStringExtra("modifyData"));
                    break;
                case 10006:
                    tvRealName.setText(data.getStringExtra("modifyData"));
                    break;
            }
        }
    }

    private boolean uploadImage(String path) {
        if (!checkNetWork()) return false;
        File file = new File(path);

//        ImageLoader.loadCicleImage(MyInfoSettingActivity.this, path, myHeader);
        if (!file.exists()) return true;
        RequestBody fileBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody boy = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", "avatar")
                .addFormDataPart("files", file.getName(), fileBody)

                .build();
        loadingDialog = new LoadingDialog(this);
        loadingDialog.show();
        Api.getRetrofit().uploadFacePic(boy).enqueue(new RequestCallback<HttpResponseFWF<UploadImageResult>>() {
            @Override
            public void onSuccess(HttpResponseFWF<UploadImageResult> response) {
                Logger.i("msg:" + response.toString());
                T.showShort(getApplicationContext(), response.getMessage());
                if (response != null && response.isSuccess()) {
//                    App.getInst().getUser().image = response.getDataFrist().fid;
//                    modifyUserInfo(response.getDataFrist().fid);

                    url = (ArrayList) response.getData();
                    ImageLoader.loadCicleImage(MyInfoSettingActivity.this, url.get(0), R.mipmap.default_person, myHeader);

                    Logger.i("url:" + url.get(0));

                    HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                    params.put("avatar", url.get(0));
                    Api.getRetrofit().modifyData(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
                        @Override
                        public void onSuccess(HttpResponseFWF response) {
                            Logger.i("msg:" + response.toString());
                            if (!response.isSuccess()) {
                                T.showShort(getApplicationContext(), response.getMessage());

                            } else {

                                SharePreHelper.getIns().saveHeaderUrl(url.get(0));
                                SharePreHelper.getIns().isModifyHeader(true);
                            }
                        }

                        @Override
                        public void onFinish() {

                        }
                    });


                }
            }

            @Override
            public void onFinish() {
                loadingDialog.dismiss();
            }

        });
        return false;
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        getCustomActionBar().setTitleText(R.string.personal_setting);
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                finish();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    /**
     * 修改是否允许他人查看我的资料
     */

    public void changeCanView(final boolean visible, final int type) {

        HashMap<String, Object> params = new HashMap<>();
        if (type == 0) {
            params.put("canview", visible);

        } else if (type == 1) {
            params.put("chatpush", visible);

        } else if (type == 2) {
            params.put("autoupdate", visible);


        }

        Api.getRetrofit().modifyData(params).enqueue(new RequestCallbackFWF<HttpResponseFWF>() {
            @Override
            public void onSuccess(HttpResponseFWF response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    T.showShort(MyInfoSettingActivity.this, response.getMessage());
                } else {
                    T.showShort(MyInfoSettingActivity.this, getResources().getString(R.string.save_success));
                    if (type == 0) {


                        request();
                    } else if (type == 1) {

                        request();

                    } else {


                        request();
                    }

                }
            }

            @Override
            public void onFinish() {

            }
        });

    }


    public static class UploadImageResult {
        public String fid;
    }
}
