package com.fullnetworkbasketball.ui.user;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.models.Team;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.home.OtherTeamDetailsActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.utils.Utility;
import com.fullnetworkbasketball.views.RadarView;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/4 0004.
 */
public class PlayerPersonalActivity extends BaseActivity {

    @Bind(R.id.player_details_header)
    ImageView playerDetailsHeader;
    @Bind(R.id.player_details_name)
    TextView playerDetailsName;
    @Bind(R.id.rb_evaluation_mine)
    RatingBar rbEvaluationMine;
    @Bind(R.id.player_details_join_match)
    TextView playerDetailsJoinMatch;
    @Bind(R.id.player_details_goal)
    TextView playerDetailsGoal;
    @Bind(R.id.player_details_level)
    TextView playerDetailsLevel;
    @Bind(R.id.team_details_attack)
    TextView teamDetailsAttack;
    @Bind(R.id.team_details_speed)
    TextView teamDetailsSpeed;
    @Bind(R.id.team_details_credit)
    TextView teamDetailsCredit;
    @Bind(R.id.ll_radar)
    LinearLayout llRadar;
    @Bind(R.id.team_details_evaluation)
    TextView teamDetailsEvaluation;
    @Bind(R.id.team_details_physical_fitness)
    TextView teamDetailsPhysicalFitness;
    @Bind(R.id.team_details_defense)
    TextView teamDetailsDefense;
    @Bind(R.id.player_details_height)
    TextView playerDetailsHeight;
    @Bind(R.id.player_height)
    LinearLayout playerHeight;
    @Bind(R.id.player_details_weight)
    TextView playerDetailsWeight;
    @Bind(R.id.player_weight)
    LinearLayout playerWeight;
    @Bind(R.id.player_details_age)
    TextView playerDetailsAge;
    @Bind(R.id.player_age)
    LinearLayout playerAge;
    @Bind(R.id.player_details_bmi)
    TextView playerDetailsBmi;
    @Bind(R.id.player_bmi)
    LinearLayout playerBmi;
    @Bind(R.id.my_team)
    LinearLayout myTeam;
    RadarView teamDetailsRadar;
    @Bind(R.id.attention_player)
    TextView attentionPlayer;

    @Bind(R.id.tv_scoreCount)
    TextView minScoreCount;
    @Bind(R.id.tv_avgHelp)
    TextView minAvgHelp;
    @Bind(R.id.tv_avgBackboard)
    TextView minAvgBackboard;
    @Bind(R.id.tv_Freethrow_rate)
    TextView minFreethrowrate;
    @Bind(R.id.tv_avgCover)
    TextView minAvgCover;
    @Bind(R.id.tv_avgSteals)
    TextView minAvgSteals;

    private String attack;
    private String defense;
    private String credit;
    private String physics_fitness;
    private String evaliation;
    private String speed;
    private String record;
    private String win;
    private String decue;
    private String lose;
    private String level;
    private String avgAge;
    private double[] data;
    private int playerId;
    private Player player;
    private double BMI;

    public boolean isCanView;
    public ContactOpponentCaptainDialog captainDialog;

    public LinearLayout person_ll;

    private int userId;

    //判断当前用户是否与当前球员相同
    private boolean flag;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_personal_layout);
        playerId = getIntent().getExtras().getInt("playerId", 0);

        person_ll = (LinearLayout) findViewById(R.id.person_ll);
    }

    @Override
    protected void initializeViews() {
        attentionPlayer.setOnClickListener(this);
    }

    private void attentionPlayer() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("focusId", playerId);
        Api.getRetrofit().attationPlayer(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    T.showShort(getApplicationContext(), response.getMessage());
                    attentionPlayer.setText(getResources().getString(R.string.cancel_attention_player));
                    initializeData();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void cancelAttentionPlayer() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("focusId", playerId);
        Api.getRetrofit().cancelAttationPlayer(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {
                    T.showShort(getApplicationContext(), response.getMessage());
                    attentionPlayer.setText(getResources().getString(R.string.attention));
                    initializeData();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void initializeData() {
        attack = getResources().getString(R.string.attack);
        defense = getResources().getString(R.string.defense);
        credit = getResources().getString(R.string.credit);
        evaliation = getResources().getString(R.string.evaluation);
        speed = getResources().getString(R.string.speed);
        physics_fitness = getResources().getString(R.string.physics_fitness);
        record = getResources().getString(R.string.record);
        win = getResources().getString(R.string.win);
        decue = getResources().getString(R.string.deuce);
        lose = getResources().getString(R.string.lose);
        level = getResources().getString(R.string.team_details_level);
        avgAge = getResources().getString(R.string.avg_age);
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("userId", playerId);
        Api.getRetrofit().getMyInfo(params).enqueue(new RequestCallback<HttpResponseFWF2<Player>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Player> response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    if (response.flag == 0) {
                        if (response.code == -1) {
                            T.showShort(getApplicationContext(), response.getMessage());
                        }
                    }
                } else {
                    player = response.getData();

                    isCanView = player.canview;

//


                    if (UserManager.getIns().getUser().id == player.id) {
                        flag = true;
                    } else {
                        flag = false;
                    }

                    userId = player.id;
                    if (UserManager.getIns().getUser().id == player.id || isCanView) {


                        if (player.height == 0 || player == null) {

                        } else {
                            BMI = player.weight / ((player.height / 100) * (player.height / 100));
                        }

                        playerDetailsBmi.setText(Utility.formatFloat(BMI));
                        playerDetailsName.setText(player.name);
                        getCustomActionBar().setTitleText(player.name);
                        ImageLoader.loadCicleImage(PlayerPersonalActivity.this, player.avatar, R.mipmap.default_person, playerDetailsHeader);
                        teamDetailsAttack.setText(attack + player.radarDto.jingong);
                        teamDetailsCredit.setText(credit + player.radarDto.credit);
                        teamDetailsDefense.setText(defense + player.radarDto.fangshou);
                        teamDetailsEvaluation.setText(evaliation + player.radarDto.stars);
                        teamDetailsPhysicalFitness.setText(physics_fitness + player.radarDto.tineng);
                        teamDetailsSpeed.setText(speed + player.radarDto.sudu);
                        rbEvaluationMine.setRating(Utility.formatInt(player.stars));

                        playerDetailsJoinMatch.setText(getResources().getString(R.string.join_match) + player.matches);
                        playerDetailsGoal.setText(getResources().getString(R.string.goal_player_details) + player.score);
                        playerDetailsLevel.setText(getResources().getString(R.string.level) + player.backboard);

                        playerDetailsHeight.setText(player.height + "");
                        playerDetailsWeight.setText(player.weight + "");
                        playerDetailsAge.setText(player.age + "");

                        minScoreCount.setText(player.scoreCount+"");
                        minAvgHelp.setText(player.avgHelp+"");
                        minAvgBackboard.setText(player.avgBackboard+"");
                        minFreethrowrate.setText(player.avgSteals+"");
                        minAvgCover.setText(player.avgCover+"");
                        minAvgSteals.setText(player.avgAnError+"");


                        data = new double[6];


                        data[0] = player.radarDto.fangshou;
                        data[1] = player.radarDto.stars;
                        data[2] = player.radarDto.sudu;
                        data[3] = player.radarDto.jingong;
                        data[4] = player.radarDto.credit;
                        data[5] = player.radarDto.tineng;
                        teamDetailsRadar = new RadarView(PlayerPersonalActivity.this, data);
                        /** 设置旋转动画 */
                        final RotateAnimation animation = new RotateAnimation(0f, 30f, Animation.RELATIVE_TO_SELF,
                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        animation.setDuration(100);//设置动画持续时间
                        /** 常用方法 */
                        animation.setRepeatCount(0);//设置重复次数
                        animation.setFillAfter(true);//动画执行完后是否停留在执行完的状态
                        teamDetailsRadar.startAnimation(animation);
                        llRadar.addView(teamDetailsRadar);
//
                        for (final Team team : player.teams) {
                            View view = LayoutInflater.from(PlayerPersonalActivity.this).inflate(R.layout.team_item_layout, null);
                            ImageView my_team_header = (ImageView) view.findViewById(R.id.my_team_header);
                            ImageLoader.loadCicleImage(PlayerPersonalActivity.this, team.logo,R.mipmap.default_team, my_team_header);
                            TextView my_name_name = (TextView) view.findViewById(R.id.my_name_name);
                            my_name_name.setText(team.name);
                            TextView my_team_level = (TextView) view.findViewById(R.id.my_team_level);
                            my_team_level.setText(team.level + getResources().getString(R.string.team_level));
                            RatingBar rb_evaluation_my_team = (RatingBar) view.findViewById(R.id.rb_evaluation_my_team);
                            rb_evaluation_my_team.setRating((float) team.stars);
                            TextView my_team_record = (TextView) view.findViewById(R.id.my_team_record);
                            my_team_record.setText(record + team.win + win +"  " + team.lose + lose);
                            TextView my_team_avg_age = (TextView) view.findViewById(R.id.my_team_avg_age);
                            my_team_avg_age.setText(avgAge + team.age);
                            myTeam.addView(view);
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("teamId", team.id);
                                    startActivity(OtherTeamDetailsActivity.class, bundle);
                                }
                            });
                        }
                        if (player.id == UserManager.getIns().getUser().id) {
                            attentionPlayer.setVisibility(View.INVISIBLE);
                        } else {
                            attentionPlayer.setVisibility(View.VISIBLE);
                        }
                        if (player.follow) {
                            attentionPlayer.setText(getResources().getString(R.string.cancel_attention_player));
                        } else {
                            attentionPlayer.setText(getResources().getString(R.string.attention));
                        }

                    } else {

                        person_ll.setVisibility(View.GONE);
                        closeDialog();
                        return;
                    }


                }
            }

            @Override
            public void onFinish() {

            }
        });

    }


    public void closeDialog() {

        if (captainDialog == null) {
            captainDialog = new ContactOpponentCaptainDialog(PlayerPersonalActivity.this, R.style.callDialog, 12);
            captainDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            captainDialog.setConfirmClickListener(new ContactOpponentCaptainDialog.OnConfirmOrderListener() {

                @Override
                public void onConfirmOrder(int i) {
                    if (i == 12) {
                        finish();
                    }
                }

            });

        }
        captainDialog.show();

        captainDialog.setCancelable(false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.attention_player:
                if (UserManager.getIns().getUser() == null) {
                    App.getInst().toLogin();
                    return;
                }
                if (player.follow) {
                    cancelAttentionPlayer();
                } else {
                    attentionPlayer();
                }
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);


        if (UserManager.getIns().getUser() != null) {
            if (flag) {
                getCustomActionBar().setRightImageView(R.mipmap.share);
            }

        }
    }

    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                finish();
                break;
            case R.id.actionbar_right_image:
                break;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}