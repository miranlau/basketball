package com.fullnetworkbasketball.ui.user;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponse;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.ui.base.BaseActivity;
import com.fullnetworkbasketball.utils.T;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity implements TextWatcher {


    @Bind(R.id.phone_number)
    EditText phoneNumber;
    @Bind(R.id.time_)
    TextView time;
    @Bind(R.id.next_step_register)
    TextView nextStepRegister;
    @Bind(R.id.verifyCode)
    EditText verifyCode;
    private String phone = "";
    private int isFind;
    private String strVerifyCode="";

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_register);
        isFind = getIntent().getExtras().getInt("isFind",0);
    }

    @Override
    protected void initializeViews() {
        phoneNumber.addTextChangedListener(this);
        time.setOnClickListener(this);
        nextStepRegister.setOnClickListener(this);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.time_:
                phone = phoneNumber.getText().toString().trim();
                if (phone.isEmpty() || !isMobileNO(phone)) {
                    T.showShort(RegisterActivity.this, R.string.input_right_phone_number);
                    return;
                }

                HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
                params.put("mobile", phone);

                if (isFind==1){
                    Api.getRetrofit().findVerCode(params).enqueue(new RequestCallback<HttpResponse>() {
                        @Override
                        public void onSuccess(HttpResponse response) {
                            Logger.i("msg:" + response.toString());
                            if (!response.isSuccess()) {
                                T.showShort(getApplicationContext(), response.getMessage());
                            } else {
                                runTime();
//                                verifyCode.setText(response.data);
                                strVerifyCode = response.data;

                                T.showShort(getApplicationContext(), response.getMessage());
                            }
                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }else {
                    Api.getRetrofit().loginVerCode(params).enqueue(new RequestCallback<HttpResponse>() {
                        @Override
                        public void onSuccess(HttpResponse response) {
                            Logger.i("msg:" + response.toString());
                            if (!response.isSuccess()) {
                                T.showShort(getApplicationContext(), response.getMessage());
                            } else {
                                runTime();
//                                verifyCode.setText(response.data);
                                strVerifyCode = response.data;
                                T.showShort(getApplicationContext(), response.getMessage());
                            }
                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }



                break;
            case R.id.next_step_register:
               verification();
                break;
        }
    }
    private void verification(){
        if (phoneNumber.getText().toString().trim().isEmpty()){
            T.showShort(this,R.string.phone_is_empty);
            return;
        }
        if (verifyCode.getText().toString().trim().isEmpty()){
            T.showShort(this,R.string.input_code);
            return;
        }
        if (!verifyCode.getText().toString().trim().equals(strVerifyCode)){
            T.showShort(this,R.string.code_error);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("mobile",phoneNumber.getText().toString().trim());
        bundle.putString("code", verifyCode.getText().toString().trim());
        bundle.putInt("isFind", isFind);
        handler.removeMessages(1);
        startActivity(SettingPassWordActivity.class,bundle);

//        handler.removeMessages();
//        finish();
    }
    @Override
    public void onActionBarClick(View view) {
        super.onActionBarClick(view);
        switch (view.getId()) {
            case R.id.actionbar_left_image:
                onBackPressed();
                handler.removeMessages(1);
                break;
        }
    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        getCustomActionBar().setLeftImageView(R.mipmap.back);
        if (isFind==1){
            getCustomActionBar().setTitleText(R.string.find_pwd);
        }else {
            getCustomActionBar().setTitleText(R.string.register);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.getTrimmedLength(s.toString()) == 11 && TextUtils.isDigitsOnly(s.toString())) {
            time.setEnabled(true);
        } else {
        }
    }

    public boolean isMobileNO(String mobiles) {
        Pattern p = Pattern
                .compile("^((13[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$");
        Matcher m = p.matcher(mobiles);

        return m.matches();
    }

    /**
     * 倒数时间器
     */
    public void runTime() {
        time.setEnabled(false);
        handler.sendEmptyMessageDelayed(1, 1000);
    }

    private String code = "";
    private Handler handler = new Handler() {

        private int count = 60;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            time.setText(getResources().getString(R.string.time_format, count));
            time.setBackgroundColor(Color.parseColor("#cccccc"));
            count--;

            if (count > 0) {
                handler.sendEmptyMessageDelayed(1, 1000);
            } else {
                count = 60;
                code = "";
                time.setEnabled(true);
                time.setText(R.string.send_code_again);
                time.setBackgroundColor(getResources().getColor(R.color.blue_register));
            }
        }
    };
}
