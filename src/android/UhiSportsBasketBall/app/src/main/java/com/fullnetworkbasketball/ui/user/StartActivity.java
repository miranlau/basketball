package com.fullnetworkbasketball.ui.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullnetworkbasketball.models.AdData;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.HomeActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.uhisports.basketball.R;

import java.util.HashMap;

public class StartActivity extends Activity implements View.OnClickListener {

    private ImageView imageView;

    private TextView time;
    private boolean isFirstLoad;

    private String ImagePath;

    private String LinKUrl;

    private boolean Open = true;

    //用于跳转问题的处理，倒计时跳转和点击跳转的冲突
    public boolean flag = true;
    public int count = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start);
        imageView = (ImageView) findViewById(R.id.ad_iv);
        time = (TextView) findViewById(R.id.time_tv);

        isFirstLoad = SharePreHelper.getIns().getIsFirstLoad();
        getAd();

        initView();
    }


    public void initView() {


        imageView.setOnClickListener(this);
        time.setOnClickListener(this);
        time.setBackgroundColor(Color.parseColor("#cccccc"));


    }

    /**
     * 倒数时间器
     */
    public void runTime() {
        //time.setEnabled(false);

        handler.sendEmptyMessageDelayed(1, 1000);


//
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            time.setText(getResources().getString(R.string.skip_time, count));
            // time.setBackgroundColor(Color.parseColor("#cccccc"));
            count--;

            if (count > 0) {
                handler.sendEmptyMessageDelayed(1, 1000);
            } else {
                Log.i("Flag", "数据：" + count);

                if (flag) {

                    if (isFirstLoad) {
                        Intent time = new Intent();
                        time.setClass(StartActivity.this, HomeActivity.class);
                        startActivity(time);
                        finish();
                    } else {
                        Intent time = new Intent();
                        time.setClass(StartActivity.this, GuideActivity.class);
                        startActivity(time);
                        finish();
                    }
                } else {

                    return;
                }
            }

        }


    };


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ad_iv:

                if (LinKUrl == null) {

                    return;
                }
                flag = false;

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(LinKUrl));
                startActivity(intent);

                break;
            case R.id.time_tv:


                flag = false;

                if (isFirstLoad) {
                    Intent time = new Intent();
                    time.setClass(StartActivity.this, HomeActivity.class);
                    startActivity(time);
                    finish();
                } else {
                    Intent time = new Intent();
                    time.setClass(StartActivity.this, GuideActivity.class);
                    startActivity(time);
                    finish();
                }

                break;
        }

    }

    /**
     * 广告网络请求
     */

    public void getAd() {

        HashMap<String, Object> params = new HashMap<>();

        Api.getRetrofit().getAd(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<AdData>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<AdData> response) {

                AdData adData = response.getData();

                Log.i("adData", "数据：" + adData.srcPath);


                Open = adData.isOpen;



                Open = adData.isOpen;

                if (!Open) {
                    if (isFirstLoad) {
                        Intent time = new Intent();
                        time.setClass(StartActivity.this, HomeActivity.class);
                        startActivity(time);
                        finish();
                    } else {
                        Intent time = new Intent();
                        time.setClass(StartActivity.this, GuideActivity.class);
                        startActivity(time);
                        finish();
                    }

                } else {


                    initView();
                    ImagePath = adData.srcPath;
                    LinKUrl = adData.urlPath;

                    ImageLoader.loadImage(StartActivity.this, ImagePath, imageView);
                    runTime();
                }

            }

            @Override
            public void onFinish() {

            }
        });
    }
}
