package com.fullnetworkbasketball.ui.user;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.User;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallback;
import com.fullnetworkbasketball.utils.JsonParser;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.TextUtil;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chatuidemo.DemoApplication;
import com.hyphenate.chatuidemo.DemoHelper;
import com.hyphenate.chatuidemo.db.DemoDBManager;
import com.orhanobut.logger.Logger;

import java.util.HashMap;


/**
 * Created by MagicBean on 2016/03/05 14:14:53
 */
public class UserManager {
    private static UserManager mUserManager;


    private boolean isRegisterToServer;
    private String TAG = "huanxin";


    private UserManager() {

    }

    public static UserManager getIns() {
        if (mUserManager == null) {
            synchronized (UserManager.class) {
                mUserManager = new UserManager();
            }
        }
        return mUserManager;
    }

    public void logout() {

    }

    /**
     * 清除session
     */
    public void clearToken() {
        SharedPreferences mPreferences = App.getInst().getSharedPreferences("UserInfo", 0);
        mPreferences.edit().clear().commit();
    }

    /**
     * 刷新用户数据
     */
//    public synchronized void refreshUserInfo() {
//        if (getUser() == null) return;
//        if (getUser() != null && !TextUtil.isValidate(getUser().token)) {
//            return;
//        }
//        HashMap<String, Object> params = HttpParamsHelper.createParams();
//        params.put("token", getUser().token);
//        Api.getRetrofit().refreshUserInfo(params).enqueue(new Callback<HttpResponse<User>>() {
//            @Override
//            public void onResponse(Response<HttpResponse<User>> response) {
//                if (response.isSuccess()) {
//                    if (response.body() != null) {
//                        //token 过期或者 其他社保登录
//                        if (response.body().code == 401 || response.body().code == 403) {
//                            App.getInst().toLogin();
//                        } else {
//                            User user = response.body().getDataFrist();
//                            if (user != null) {
//                                App.getInst().setUser(user);
//                                saveUserInfo(user);
//                                if (!isRegisterToServer) {
//                                    isRegisterToServer = true;
//                                    PushManager.getInstance().initialize(App.getInst().getApplicationContext());
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//
//            }
//        });
//    }

    /***
     * 自动登录
     *
     * @return
     */
    public synchronized void autoLogin() {
        if (getUser() == null) return;
        if (getUser() != null && !TextUtil.isValidate(getUser().token)) {
            return;
        }
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("username", getUser().username);
        params.put("password", SharePreHelper.getIns().getPassword());
        Api.getRetrofit().loginFB(params).enqueue(new RequestCallback<HttpResponseFWF2<User>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<User> response) {
                Logger.i("msg:" + response.toString());
                if (!response.isSuccess()) {
                    if (response.flag == 0) {
                        if (response.code == -1) {
                        }
                    }
                } else {
                    User user = response.getData();
                    UserManager.getIns().saveUserInfo(user);
                    login(user);
                    // upDeviceToken();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    /**
     * 登录环信
     *
     * @param
     */
    public void login(User user) {
//        if (!EaseCommonUtils.isNetWorkConnected(getIns)) {
//            Toast.makeText(this, R.string.network_isnot_available, Toast.LENGTH_SHORT).show();
//            return;
//        }
//


        // After logout，the DemoDB may still be accessed due to async callback, so the DemoDB will be re-opened again.
        // close it before login to make sure DemoDB not overlap
        DemoDBManager.getInstance().closeDB();

        // reset current user name before login
        DemoHelper.getInstance().setCurrentUserName(user.username);

        final long start = System.currentTimeMillis();
        // 调用sdk登陆方法登陆聊天服务器
        Log.d(TAG, "EMClient.getInstance().login");
        EMClient.getInstance().login(user.username, user.username, new EMCallBack() {

            @Override
            public void onSuccess() {
                Log.d(TAG, "login: onSuccess");

//                if (!LoginActivity.this.isFinishing() && pd.isShowing()) {
//                    pd.dismiss();
//                }

                // ** 第一次登录或者之前logout后再登录，加载所有本地群和回话
                // ** manually load all local groups and
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();

                // 更新当前用户的nickname 此方法的作用是在ios离线推送时能够显示用户nick
                boolean updatenick = EMClient.getInstance().updateCurrentUserNick(
                        DemoApplication.currentUserNick.trim());
                if (!updatenick) {
                    Log.e("LoginActivity", "update current user nick fail");
                }
                //异步获取当前用户的昵称和头像(从自己服务器获取，demo使用的一个第三方服务)
//				DemoHelper.getInstance().getUserProfileManager().asyncGetCurrentUserInfo();

                // 进入主页面
//                startActivity(HomeActivity.class);
//                finish();
            }

            @Override
            public void onProgress(int progress, String status) {
                Log.d(TAG, "login: onProgress");
            }

            @Override
            public void onError(final int code, final String message) {
                Log.d(TAG, "login: onError: " + code);
//                if (!progressShow) {
//                    return;
//                }
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        pd.dismiss();
//                        Toast.makeText(getApplicationContext(), getString(R.string.Login_failed) + message,
//                                Toast.LENGTH_SHORT).show();
//                    }
//                });
            }
        });
    }


    public synchronized User getUser() {
        SharedPreferences mPreferences = App.getInst().getSharedPreferences("UserInfo", 0);
        String temp = mPreferences.getString("_user", "");
        byte[] base64Bytes = Base64.decode(temp, Base64.DEFAULT);
        String retStr = new String(base64Bytes);
        return JsonParser.deserializeByJson(retStr, User.class);
    }

    public synchronized void saveUserInfo(User user) {
        if (user == null) return;
        SharedPreferences mPreferences = App.getInst().getSharedPreferences("UserInfo", 0);
        String json = JsonParser.serializeToJson(user);
        String temp = new String(Base64.encode(json.getBytes(), Base64.DEFAULT));
        mPreferences.edit().putString("_user", temp).commit();
    }


}
