package com.fullnetworkbasketball.updataVersion;

public class UpdateInfo  {
    //版本名字
    public  String versionNumber;

    //后台系统版本号
    public int code;

    //要求的最低版本号,如果本机小于这个版本号则必须下载

    public int minCode;
    //版本名字
    public String name;
    //下载地址
    public String downloadURL;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getMinCode() {
        return minCode;
    }

    public void setMinCode(int minCode) {
        this.minCode = minCode;
    }



    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }


}
