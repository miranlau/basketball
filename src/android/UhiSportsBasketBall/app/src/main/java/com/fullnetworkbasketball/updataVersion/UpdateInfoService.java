package com.fullnetworkbasketball.updataVersion;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;

import com.fullnetworkbasketball.utils.SharePreHelper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class UpdateInfoService {

    ProgressDialog progressDialog;

    Context context;

    UpdateInfo updateInfo;


    public UpdateInfoService(Context context, UpdateInfo updateInfo) {
        this.context = context;
        this.updateInfo = updateInfo;
    }


    //判断是不是需要版本更新

    public boolean isNeedUpdate() {

        //服务器的版本号
        int new_version = updateInfo.code;

        //服务器最低版本号

        int min_version = updateInfo.minCode;

        //本机App 的版本号
        int now_version = 0;

        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            //比较版本号
            now_version = packageInfo.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (min_version <= now_version) {
            if (new_version <= now_version) {
                return false;
            } else {

                //判断是不是第一次进来 点击了确定更新

                SharePreHelper.getIns().setIsCancelUpdata(false);
                return true;
            }
        } else {
            return true;
        }
    }


    //下载文件
    public void downLoadFile(final String url, final ProgressDialog pDialog) {
        progressDialog = pDialog;
        new Thread() {
            public void run() {
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(url);
                HttpResponse response;
                try {
                    response = client.execute(get);
                    HttpEntity entity = response.getEntity();
                    int length = (int) entity.getContentLength();
                    progressDialog.setMax(length);
                    InputStream is = entity.getContent();
                    FileOutputStream fileOutputStream = null;
                    if (is != null) {

                        File file = new File(Environment.getExternalStorageDirectory(), "football.apk");
                        fileOutputStream = new FileOutputStream(file);

                        byte[] buf = new byte[100];
                        int ch = -1;
                        int process = 0;
                        while ((ch = is.read(buf)) != -1) {
                            fileOutputStream.write(buf, 0, ch);
                            process += ch;
                            progressDialog.setProgress(process);
                        }
                    }
                    fileOutputStream.flush();
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    down();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }.start();
    }

    //下载
    public void down() {
        progressDialog.cancel();
        update();
    }

    //
    public void update() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "football.apk")),
                "application/vnd.android.package-archive");
        context.startActivity(intent);
    }


}
