package com.fullnetworkbasketball.utils;

/**
 * Created by Administrator on 2015/12/30 0030.
 */
public class File {

    //表一 评论回复
    public static String TABLE_NAME = "dynamic";

    public static int VERSON = 1;
    public static String DB_NAME = "Jpush";

    //发送者头像
    public static String SENDER_HEADER = "senderHeader";
    //发送内容
    public static String CONTENT = "content";

    //发送时间
    public static String SEND_TIME = "sendTime";
    //推送类型
    public static String PUSH_TYPE = "pushType";
    //发送者名称
    public static String SENDER_NAME = "senderName";
    //动态id
    public static String DYNAMIC_ID = "dynamicId";
    //发送者id
    public static String SENDER_ID = "senderId";
    //表二 赛事管家
    public static String TABLE_NAME_ONE = "housekeeper";

    //赛事id
    public static String MATCH_ID = "matchId";

    //用户id
    public static String USER_ID = "userId";
}
