package com.fullnetworkbasketball.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.fullnetworkbasketball.ui.dialog.ContactOpponentCaptainDialog;
import com.fullnetworkbasketball.ui.find.CompetitionTeamActivity;
import com.fullnetworkbasketball.ui.find.LeagueDynamicActivity;
import com.fullnetworkbasketball.ui.find.ListActivity;
import com.fullnetworkbasketball.ui.find.RaceCalendarActivity;
import com.fullnetworkbasketball.ui.user.LoginActivity;
import com.fullnetworkbasketball.ui.user.UserManager;


/**
 * Created by cht on 2016/5/27 0027.
 */
public class JavaScriptObject {

    ContactOpponentCaptainDialog dialog;
    public Context context;
    public int matchId;

    public JavaScriptObject(Context context) {
        this.context = context;
    }

    public JavaScriptObject(Context context, int matchId) {
        this.context = context;
        this.matchId = matchId;
    }

    @JavascriptInterface //sdk17版本以上加上注解
    public void ActionCheckInfo(int infoId) {


        if (UserManager.getIns().getUser() == null || UserManager.getIns().getUser().token.isEmpty()) {

//            if (dialog == null) {
//                dialog = new ContactOpponentCaptainDialog(context, R.style.callDialog, 8);
//            }
//            dialog.show();
//
//            return;

            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);

        }
        Log.i("infold", "key:" + infoId);
        Intent intent = new Intent();
        intent.putExtra("infoId", infoId);
        intent.setClass(context, LeagueDynamicActivity.class);
        context.startActivity(intent);
    }

    @JavascriptInterface //sdk17版本以上加上注解

    //球队
    public void ActionTeams() {

        Intent intent = new Intent();
        intent.putExtra("leagueId", matchId);
        intent.setClass(context, CompetitionTeamActivity.class);
        context.startActivity(intent);
    }

    @JavascriptInterface //sdk17版本以上加上注解
    public void ActionTeamsProcess() {
        Intent intent = new Intent();
        intent.putExtra("leagueId", matchId);
        intent.setClass(context, RaceCalendarActivity.class);
        context.startActivity(intent);
    }


    /**
     * 榜单
     */
    @JavascriptInterface //sdk17版本以上加上注解
    public void ActionTeamsList() {
        Intent intent = new Intent();
        intent.putExtra("leagueId", matchId);
        intent.setClass(context, ListActivity.class);
        context.startActivity(intent);
    }

}
