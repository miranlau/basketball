package com.fullnetworkbasketball.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uhisports.basketball.R;


public class CalendarDayView extends LinearLayout {

    // 整个布局
    private View view;

    private LinearLayout dayLl;
    // 显示日期控件
    private TextView dayTv;
    // 显示农历控件
    private TextView lunarTv;

    public CalendarDayView(Context context) {
        super(context);
        view = LinearLayout.inflate(context, R.layout.item_calendar_day, this);

        dayLl = (LinearLayout) view.findViewById(R.id.linearLayout_day);
        dayTv = (TextView) view.findViewById(R.id.tv_calendar_day);
        lunarTv = (TextView) view.findViewById(R.id.tv_lunar);
    }

    public CalendarDayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public CalendarDayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setDay(String day, String lunar) {
        dayTv.setText(day);
        lunarTv.setText(lunar);
    }

    /**
     * 设置控件的选中状态来控制控件字体颜色和背景变化
     * @param selected 选中状态
     */
    public void setSelecteState(boolean selected){
        dayLl.setSelected(selected);
        dayTv.setSelected(selected);
        lunarTv.setSelected(selected);
    }
}