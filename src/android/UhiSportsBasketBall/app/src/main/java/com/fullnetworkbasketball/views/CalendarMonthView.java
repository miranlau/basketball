package com.fullnetworkbasketball.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.utils.CalendarUtil;
import com.fullnetworkbasketball.utils.DisplayUtil;
import com.uhisports.basketball.R;

import java.util.Calendar;
import java.util.HashMap;

/**
 * 每月日历的自定义控件
 */
public class CalendarMonthView extends LinearLayout implements View.OnClickListener {

    int year, month;

    Calendar calendar;
    CalendarDayView[][] dateViews;
    boolean thisMonthEnd = false;

    // 用来记录选中展示的天
    private CalendarDayView chooseView = null;
    // 存放的有数据的天的时间
    private HashMap<String, Long> dataMap;
    // 农历计算工具
    private CalendarUtil calendarUtil = new CalendarUtil();

    private OnDayClickListener onDayClickListener;

    public CalendarMonthView(Context context, int year, int month, HashMap<String, Long> dataMap) {
        super(context);
        this.setOrientation(VERTICAL);
        this.year = year;
        this.month = month;
        this.dataMap = dataMap;

        initData();
        initView(context);
    }

    public CalendarMonthView(Context context, int year, int month) {
        super(context);
        this.setOrientation(VERTICAL);
        this.year = year;
        this.month = month;


        initData();
        initView(context);
    }
    private void initData() {
        calendar = Calendar.getInstance();
        // 月份是从0开始，所以要-1
        calendar.set(year, month - 1, 1);
        int week = calendar.getMaximum(Calendar.WEEK_OF_MONTH);
        dateViews = new CalendarDayView[week][7];
    }


    public CalendarMonthView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CalendarMonthView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    private void initView(Context context) {
        this.removeAllViews();

        int week = calendar.get(Calendar.DAY_OF_WEEK);

        int height = DisplayUtil.dip2px(context, 40);
        LinearLayout monthLl = new LinearLayout(context);
        LayoutParams monthLP = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        monthLl.setLayoutParams(monthLP);
        monthLl.setOrientation(HORIZONTAL);
        monthLl.setBackgroundColor(getResources().getColor(android.R.color.white));

        // 绘制月份
        DisplayMetrics metric = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(metric);
        TextView monthView = new TextView(context);
        LayoutParams monthLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        monthLayoutParams.leftMargin = metric.widthPixels / 7 * (week - 1);
        monthView.setLayoutParams(monthLayoutParams);
        monthView.setGravity(Gravity.CENTER);
        monthView.setText(month + "月");
        monthView.setTextSize(22);
        monthView.setTextColor(getResources().getColor(R.color.text_color_orange));
        monthLl.addView(monthView);
        this.addView(monthLl);

        for (int i = 0; i < dateViews.length; i++) {

            if (thisMonthEnd) {
                break;
            }

            LinearLayout weekView = new LinearLayout(context);
            LayoutParams weekLP = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            weekView.setLayoutParams(weekLP);
            weekView.setOrientation(HORIZONTAL);
            weekView.setBackgroundColor(getResources().getColor(android.R.color.white));


            for (int g = 0; g < week - 1; g++) {
                View view = new View(context);
                LayoutParams layoutParams = new LayoutParams(0, 1, 1.0f);
                view.setLayoutParams(layoutParams);
                weekView.addView(view);
            }


            for (int j = week - 1; j < dateViews[i].length; j++) {
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                dateViews[i][j] = new CalendarDayView(context);
                LayoutParams lp = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                dateViews[i][j].setLayoutParams(lp);
                // TODO 计算农历
                String lunar = calendarUtil.getChineseDay(year, month, day);
                if(lunar.equals("初一")){
                    // 如果是初一，就把初一替换为月份
                    lunar = calendarUtil.getChineseMonth(year, month, day);
                }
                dateViews[i][j].setDay(day + "", lunar);
                // 判断当前天有没有数据
                String key = year+"."+month+"."+day;
                if (checkSkiData(key)) {
                    dateViews[i][j].setSelecteState(true);
                }
                weekView.addView(dateViews[i][j]);

                dateViews[i][j].setTag(day);
                dateViews[i][j].setOnClickListener(this);

                if (thisMonthEnd) {
                    dateViews[i][j].setVisibility(View.INVISIBLE);
                }

                if (calendar.get(Calendar.DAY_OF_MONTH) == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                    thisMonthEnd = true;
                }
                // 设置星期六和星期天的字体颜色
//                if (j == 0 || j == 6) {
//                    dateViews[i][j].setDayTextColor();
//                }
                // 日期进入下一天
                calendar.roll(Calendar.DAY_OF_MONTH, true);
            }
            week = 1;
            this.addView(weekView);
        }

    }

    /**
     * 判断某年某月某日是否有滑雪数据
     *
     * @param key
     * @return
     */
    private boolean checkSkiData(String key) {

        return dataMap.containsKey(key);

    }

    /**
     * 每一天的点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        CalendarDayView dayView = (CalendarDayView) view;
        if(chooseView != null){
            // 前面被选中的天的日期
            int chooseDay = (int) chooseView.getTag();
            String chooseKey = year+"."+month+"."+chooseDay;
            if(!checkSkiData(chooseKey)){
                chooseView.setSelecteState(false);
            }
        }
        chooseView = dayView;
        dayView.setSelecteState(true);
        int day = (int) dayView.getTag();
        String key = year+"."+month+"."+day;
        boolean hasData = checkSkiData(key);
        onDayClickListener.onDayClick(hasData, day);
    }


    public interface OnDayClickListener {
        // item点击事件
        void onDayClick(boolean hasData, int day);
    }

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.onDayClickListener = onDayClickListener;
    }
}