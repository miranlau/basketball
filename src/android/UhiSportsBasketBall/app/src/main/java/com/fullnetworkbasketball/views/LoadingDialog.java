package com.fullnetworkbasketball.views;

import android.app.Dialog;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.uhisports.basketball.R;


/**
 * Created by MagicBean on 2016/03/02 10:10:06
 */
public class LoadingDialog extends Dialog {
    private ImageView ball;
    public LoadingDialog(Context context) {
        super(context, R.style.dialogTheme3);
        initializeView();
    }

    private void initializeView() {
        setContentView(R.layout.loading_dialog_layout);
        ball = (ImageView) findViewById(R.id.ball);

        //上下摇摆
        TranslateAnimation alphaAnimation2 = new TranslateAnimation(0, 0, 0, 60F);  //同一个x轴 (开始结束都是50f,所以x轴保存不变)  y轴开始点50f  y轴结束点80f
        alphaAnimation2.setDuration(200);  //设置时间
        alphaAnimation2.setRepeatCount(Animation.INFINITE);  //为重复执行的次数。如果设置为n，则动画将执行n+1次。INFINITE为无限制播放
        alphaAnimation2.setRepeatMode(Animation.REVERSE);  //为动画效果的重复模式，常用的取值如下。
//        alphaAnimation2.setInterpolator(new BounceInterpolator());//动画结束的时候弹起
        ball.setAnimation(alphaAnimation2);
        setCanceledOnTouchOutside(false);
        alphaAnimation2.start();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
