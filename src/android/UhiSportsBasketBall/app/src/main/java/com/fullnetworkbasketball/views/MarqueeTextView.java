package com.fullnetworkbasketball.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * @author 袁鹏
 * 自定义TextView 跑马灯
 */
public class MarqueeTextView extends TextView {
    public MarqueeTextView(Context context) {
        super(context);
    }

    /**
     *
     * MarqueeTextView  构造函数
     * @param context
     * @param attrs
     */
    public MarqueeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     *MarqueeTextView  构造函数
     * @param context
     * @param attrs
     * @param defStyle
     */
    public MarqueeTextView(Context context, AttributeSet attrs,
                           int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    /**
     *设置Focused 为真
     */
    public boolean isFocused() {
        return true;
    }

}
