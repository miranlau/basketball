package com.fullnetworkbasketball.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import com.uhisports.basketball.R;


/**
 * @ClassName ProgressTips
 * @Description 进度提示
 * @author 袁鹏
 * @date 2015-4-11
 */
public class ProgressTips {

    private static ProgressTips instance;

    private ProgressDialog dialog;

    public ProgressTips() {
    }

    public static ProgressTips getInstance() {
        if (instance == null) {
            instance = new ProgressTips();
        }
        return instance;
    }

    public void show(Context context, String message){

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }

        if (((Activity) context).isFinishing()){
            return;
        }

        dialog = getDialog(context);
        dialog.setMessage(message);
        dialog.show();
    }

    private ProgressDialog getDialog(Context context){


        ProgressDialog dialog = new ProgressDialog(context, R.style.dialogTheme3);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public void dismiss(){
        if (dialog != null){
            dialog.dismiss();
            dialog = null;
        }

    }

    public void destory(){
        instance = null;
    }

}
