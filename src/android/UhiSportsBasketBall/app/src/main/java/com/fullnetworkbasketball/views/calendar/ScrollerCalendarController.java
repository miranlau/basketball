package com.fullnetworkbasketball.views.calendar;

public interface ScrollerCalendarController {

	void onMonthOfYearSelected(int year, int month,int day);

}