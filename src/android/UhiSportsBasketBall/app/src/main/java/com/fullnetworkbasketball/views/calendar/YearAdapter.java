package com.fullnetworkbasketball.views.calendar;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class YearAdapter extends RecyclerView.Adapter<YearAdapter.ViewHolder> implements YearView.OnMonthClickListener, YearView.OnDayClickListener {
    private final TypedArray typedArray;
    private final Context context;
    private final ScrollerCalendarController scrollerCalendarController;
    private final Calendar calendar;
    private CalendarMonth selectedMonth;

    private final int yearRange;

    private HashMap<String, Long> dataMap = new HashMap<>();

    public YearAdapter(Context context, ScrollerCalendarController datePickerController, TypedArray typedArray) {
        yearRange = 20;
        this.typedArray = typedArray;
        this.context = context;
        calendar = Calendar.getInstance();
        selectedMonth = new CalendarMonth();
        scrollerCalendarController = datePickerController;
    }

    public void setSkiTime(HashMap<String, Long> dataMap) {
        this.dataMap = dataMap;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final YearView yearView = new YearView(context, typedArray);
        return new ViewHolder(yearView, this, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final YearView yearView = viewHolder.yearView;
        final HashMap<String, Integer> drawingParams = new HashMap<>();
        int currentYear = calendar.get(Calendar.YEAR) + (position - yearRange / 2);
        yearView.reuse();
        drawingParams.put(YearView.VIEW_PARAMS_YEAR_CURRENT, currentYear);
        drawingParams.put(YearView.VIEW_PARAMS_WEEK_START, calendar.getFirstDayOfWeek());
        yearView.setSkiTime(dataMap);
        yearView.setYearParams(drawingParams);
        yearView.invalidate();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return yearRange;
    }

    public void onMonthClick(YearView yearView, CalendarMonth calendarMonth) {
        if (calendarMonth != null) {
            onMonthTapped(calendarMonth);
        }
    }

    protected void onMonthTapped(CalendarMonth calendarMonth) {
        scrollerCalendarController.onMonthOfYearSelected(calendarMonth.year, calendarMonth.month, calendarMonth.day);
        setSelectedMonth(calendarMonth);
        notifyDataSetChanged();
    }

    public void setSelectedMonth(CalendarMonth calendarMonth) {
        selectedMonth = calendarMonth;
        notifyDataSetChanged();
    }

    public CalendarMonth getSelectedMonths() {
        return selectedMonth;
    }

    public int getYearRange() {
        return yearRange;
    }

    @Override
    public void onDayClick(YearView simpleMonthView, CalendarMonth key) {
        if (key == null) {
            return;
        }
        onMonthTapped(key);

        //打印  消息数据
       // Toast.makeText(context, "day:" + key.year + "-" + key.month + "-" + key.day, Toast.LENGTH_SHORT).show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final YearView yearView;

        public ViewHolder(View itemView, YearView.OnMonthClickListener onMonthClickListener, YearView.OnDayClickListener onDayClickListener) {
            super(itemView);
            yearView = (YearView) itemView;
            yearView.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            yearView.setClickable(true);
            yearView.setOnDayClickListener(onDayClickListener);
//            yearView.setOnMonthClickListener(onMonthClickListener);
        }
    }


    public static class CalendarMonth implements Serializable {
        private static final long serialVersionUID = -5456695978688356202L;
        private Calendar calendar;

        int month;
        int year;

        int day;

        public CalendarMonth() {
            setTime(System.currentTimeMillis());
        }

//        public CalendarMonth(int year, int month) {
//            setDay(year, month);
//        }

        public CalendarMonth(int year, int month, int day) {
            setDay(year, month, day);
        }

        public CalendarMonth(long timeInMillis) {
            setTime(timeInMillis);
        }

        public CalendarMonth(Calendar calendar) {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(calendar.DAY_OF_MONTH);
        }

        private void setTime(long timeInMillis) {
            if (calendar == null) {
                calendar = Calendar.getInstance();
            }
            calendar.setTimeInMillis(timeInMillis);
            month = this.calendar.get(Calendar.MONTH);
            year = this.calendar.get(Calendar.YEAR);
            day = this.calendar.get(Calendar.DAY_OF_MONTH);
        }

        public void set(CalendarMonth calendarDay) {
            year = calendarDay.year;
            month = calendarDay.month;
            day = calendarDay.day;
        }

        public void setDay(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        public Date getDate() {
            if (calendar == null) {
                calendar = Calendar.getInstance();
            }
            calendar.set(year, month, day);
            return calendar.getTime();
        }


        @Override
        public String toString() {
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{ year: ");
            stringBuilder.append(year);
            stringBuilder.append(", month: ");
            stringBuilder.append(month);
            stringBuilder.append(", day: ");
            stringBuilder.append(day);
            stringBuilder.append(" }");

            return stringBuilder.toString();
        }
    }


}