package com.fullnetworkbasketball.views.calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.uhisports.basketball.R;

import java.security.InvalidParameterException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class YearView extends View {
    public static final String VIEW_PARAMS_YEAR_CURRENT = "current";
    public static final String VIEW_PARAMS_WEEK_START = "week_start";

    protected static int DAY_LABEL_CIRCLE_RADIUS;
    protected static int DAY_LABEL_LINE_RADIUS;
    protected static int MONTH_HEADER_HEIGHT;
    protected static int YEAR_HEADER_TEXT_HEIGHT;


    protected static int YEAR_HEADER_TEXT_SIZE;
    protected static int MONTH_LABEL_TEXT_SIZE;
    protected static int DAY_LABEL_TEXT_SIZE;

    protected int padding = 0;
    protected int lineSpacingBetweenYearAndMonth = 0;
    protected int lineSpacingBetweenDayAndDay = 0;


    protected Paint yearHeaderTextPaint;
    protected Paint dividerPaint;
    protected Paint monthLabelTextPaint;
    protected Paint dayLabelTextPaint;
    protected Paint dayLabelCircleBgPaint;


    // 年份标题的字体颜色
    protected int yearHeaderTextColor;
    // 年份分割线颜色
    protected int dividerColor;
    // 月份标题字体颜色（橙色）
    protected int monthTextColor;
    // 未选中天的字体颜色
    protected int dayLabelTextColor;
    // 被选中天的字体颜色
    protected int dayLabelTextTodayColor;
    // 被选中天的背景颜色
    protected int dayLabelCircleBgColor;

    protected int today = -1;
    protected int weekStart = 1;
    protected int numDays = 7;
    protected int numCells = 0;
    private int dayOfWeekStart = 0;
    protected int month;
    protected int year;
    protected int day;

    protected int rowMonthHeight;
    protected int mWidth;

    protected final Time currentTime;
    // 屏幕宽度
    private int width;

    private Context mContext;
    private final Calendar calendar;

    private OnMonthClickListener mOnMonthClickListener;
    private OnDayClickListener mOnDayClickListener;

    // 滑雪数据
    private HashMap<String, Long> dataMap;

    private HashMap<YearAdapter.CalendarMonth, RectF> rects;

    public YearView(Context context, TypedArray typedArray) {
        super(context);
        mContext = context;
        rects = new HashMap<YearAdapter.CalendarMonth, RectF>();
        Resources resources = context.getResources();
        calendar = Calendar.getInstance();
        currentTime = new Time(Time.getCurrentTimezone());
        currentTime.setToNow();

        dividerColor = typedArray.getColor(
                R.styleable.ScrollerCalendar_dividerColor,
                resources.getColor(R.color.line_color_grey));
        yearHeaderTextColor = typedArray.getColor(
                R.styleable.ScrollerCalendar_yearHeaderTextColor,
                resources.getColor(R.color.white));
        monthTextColor = typedArray.getColor(
                R.styleable.ScrollerCalendar_monthLabelTextColor,
                resources.getColor(R.color.text_color_orange));
        dayLabelTextColor = typedArray.getColor(
                R.styleable.ScrollerCalendar_dayLabelTextColor,
                resources.getColor(R.color.white));
        dayLabelTextTodayColor = typedArray.getColor(
                R.styleable.ScrollerCalendar_dayLabelTextTodayColor,
                resources.getColor(android.R.color.white));
        dayLabelCircleBgColor = typedArray.getColor(
                R.styleable.ScrollerCalendar_dayLabelCircleBgColor,
                resources.getColor(R.color.text_color_orange));

        YEAR_HEADER_TEXT_SIZE = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_yearHeaderTextSize,
                resources.getDimensionPixelSize(R.dimen.year_header_text_size));
        DAY_LABEL_TEXT_SIZE = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_dayLabelTextSize,
                resources.getDimensionPixelSize(R.dimen.year_header_lunar_text_size));
        MONTH_LABEL_TEXT_SIZE = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_monthLabelTextSize,
                resources.getDimensionPixelSize(R.dimen.month_label_text_size));

        YEAR_HEADER_TEXT_HEIGHT = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_yearHeaderTextHeight,
                resources.getDimensionPixelOffset(R.dimen.year_header_text_height));
        MONTH_HEADER_HEIGHT = typedArray.getDimensionPixelOffset(
                R.styleable.ScrollerCalendar_monthLabelTextHeight,
                resources.getDimensionPixelSize(R.dimen.month_label_text_height));
        DAY_LABEL_CIRCLE_RADIUS = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_dayLabelCircleRadius,
                resources.getDimensionPixelSize(R.dimen.day_label_circle_radius));

        DAY_LABEL_LINE_RADIUS = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_dayLabelCircleRadius,
                resources.getDimensionPixelSize(R.dimen.day_label_circle_line_radius));

        rowMonthHeight = typedArray.getDimensionPixelSize(
                R.styleable.ScrollerCalendar_monthDayRowHeight,
                resources.getDimensionPixelSize(R.dimen.month_day_row_height));
        lineSpacingBetweenDayAndDay = resources.getDimensionPixelSize(R.dimen.padding_between_day_and_day);
        padding = CommonUtils.dp2px(mContext, 5);

        lineSpacingBetweenYearAndMonth = CommonUtils.dp2px(mContext, 30);
        initView();

    }

    protected void initView() {
        monthLabelTextPaint = new Paint();
        monthLabelTextPaint.setFakeBoldText(true);
        monthLabelTextPaint.setAntiAlias(true);
        monthLabelTextPaint.setTextSize(MONTH_LABEL_TEXT_SIZE);
        monthLabelTextPaint.setColor(monthTextColor);
        monthLabelTextPaint.setTextAlign(Align.CENTER);
        monthLabelTextPaint.setStyle(Style.FILL);

        yearHeaderTextPaint = new Paint();
        yearHeaderTextPaint.setAntiAlias(true);
        yearHeaderTextPaint.setTextAlign(Align.LEFT);
        yearHeaderTextPaint.setTextSize(YEAR_HEADER_TEXT_SIZE);
        yearHeaderTextPaint.setColor(yearHeaderTextColor);
        yearHeaderTextPaint.setStyle(Style.FILL);
        yearHeaderTextPaint.setFakeBoldText(false);

        dayLabelTextPaint = new Paint();
        dayLabelTextPaint.setAntiAlias(true);
        dayLabelTextPaint.setTextSize(DAY_LABEL_TEXT_SIZE);
        dayLabelTextPaint.setColor(dayLabelTextColor);
        dayLabelTextPaint.setStyle(Style.FILL);
        dayLabelTextPaint.setTextAlign(Align.CENTER);
        dayLabelTextPaint.setFakeBoldText(false);


        dayLabelCircleBgPaint = new Paint();
        dayLabelCircleBgPaint.setAntiAlias(true);
        dayLabelCircleBgPaint.setTextSize(DAY_LABEL_TEXT_SIZE);
        dayLabelCircleBgPaint.setColor(dayLabelCircleBgColor);
        dayLabelCircleBgPaint.setStyle(Style.FILL);
        dayLabelCircleBgPaint.setTextAlign(Align.CENTER);
        dayLabelCircleBgPaint.setFakeBoldText(false);

        dividerPaint = new Paint();
        dividerPaint.setAntiAlias(true);
        dividerPaint.setColor(dividerColor);
        dividerPaint.setStyle(Style.FILL);
        dividerPaint.setTextAlign(Align.CENTER);
        dividerPaint.setFakeBoldText(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        drawMonthTitle(canvas);
        // drawYearHeaderLabels(canvas);
        drawAllMonthNums(canvas);
    }

    private void drawYearHeaderLabels(Canvas canvas) {
        int y = (4 * YEAR_HEADER_TEXT_HEIGHT) / 5;
        canvas.drawText(year + "年", 2 * padding, y, yearHeaderTextPaint);

        //canvas.drawLine(2 * padding, YEAR_HEADER_TEXT_HEIGHT, width, YEAR_HEADER_TEXT_HEIGHT, dividerPaint);
    }

//    private void drawYearHeaderLabels(Canvas canvas) {
//        int y = (4 * YEAR_HEADER_TEXT_HEIGHT) / 5;
//        LunarYear lunar = new LunarYear(year);
//
//        canvas.drawText(year + "年", 2 * padding, y, yearHeaderTextPaint);
//
//        if(showYearLunarLabel){
//            yearHeaderDashPaint.setStrokeWidth((float) 4.0);
//            canvas.drawLine(width - 5 * YEAR_HEADER_LUNAR_TEXT_SIZE - 2
//                    * padding, (3 * y) / 8, width - 5
//                    * YEAR_HEADER_LUNAR_TEXT_SIZE, (3 * y) / 8, yearHeaderDashPaint);
//            canvas.drawText(lunar.cyclical() + lunar.animalsYear() + "年", width - 2
//                    * padding, y / 2, yearHeaderLunarTextPaint);
//
//            yearHeaderDashPaint.setStrokeWidth((float) 2.0);
//            canvas.drawLine(width - 5 * YEAR_HEADER_LUNAR_TEXT_SIZE - 2
//                    * padding, (7 * y) / 8, width - 5
//                    * YEAR_HEADER_LUNAR_TEXT_SIZE, (7 * y) / 8, yearHeaderDashPaint);
//            canvas.drawText("农历初一", width - 2 * padding, y,
//                    yearHeaderLunarTextPaint);
//        }
//
//        canvas.drawLine(2 * padding, YEAR_HEADER_TEXT_HEIGHT, width,
//                YEAR_HEADER_TEXT_HEIGHT, dividerPaint);
//    }

    private void drawMonthTitle(Canvas canvas) {
        int paddingDay = (mWidth - 2 * padding) / (2 * numDays);

        int x = padding;
        int y = 0;

        for (int i = 1; i <= 12; i++) {

            switch (i) {
                case 1:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth;
                    break;
                case 2:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 1;
                    break;
                case 3:
                    // x = paddingDay + (width / 3) * (i - 1);
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 2;
                    break;
                case 4:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 3;
                    break;
                case 5:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 4;
                    break;
                case 6:
                    //  x = paddingDay + (width / 3) * (i - 4);
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 5;
                    break;
                case 7:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 6;
                    break;
                case 8:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 7;
                    break;
                case 9:
                    // x = paddingDay + (width / 3) * (i - 7);
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 8;
                    break;
                case 10:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 9;
                    break;
                case 11:
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 10;
                    break;
                case 12:
                    // x = paddingDay + (width / 3) * (i - 10);
                    y = (MONTH_HEADER_HEIGHT) / 2 + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 11;
                    break;
            }

            canvas.drawText(i + "月", getMeasuredWidth() / 2 + (mContext.getResources().getDimensionPixelSize(R.dimen.reduce)), y, monthLabelTextPaint);

            canvas.drawText(year + "年", getMeasuredWidth() / 2 - (mContext.getResources().getDimensionPixelSize(R.dimen.reduce)), y, yearHeaderTextPaint);
        }
    }

    private void drawAllMonthNums(Canvas canvas) {
        int y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT + lineSpacingBetweenYearAndMonth;
        int paddingDay = (mWidth - 2 * padding) / (2 * numDays);

        for (int i = 0; i < 12; i++) {

            switch (i) {
                case 0:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth;
                    break;
                case 1:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 1;

                    break;
                case 2:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 2;

                    break;

                case 3:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 3;

                    break;
                case 4:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 4;

                    break;
                case 5:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 5;
                    break;
                case 6:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 6;
                    break;
                case 7:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 7;
                    break;

                case 8:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 8;
                    break;
                case 9:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 9;
                    break;
                case 10:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 10;
                    break;
                case 11:
                    y = MONTH_HEADER_HEIGHT + YEAR_HEADER_TEXT_HEIGHT
                            + lineSpacingBetweenYearAndMonth + rowMonthHeight * 11;
                    break;
            }

            setYearParams(year, i);
            int dayOffset = findDayOffset();
            day = 1;

            while (day <= numCells) {
                int column = 0;
                switch (i) {
                    case 0:
                    case 3:
                    case 6:
                    case 9:
                        column = 0;
                        break;
                    case 1:
                    case 4:
                    case 7:
                    case 10:
                        column = 0;
                        break;
                    case 2:
                    case 5:
                    case 8:
                    case 11:
                        column = 0;
                        break;
                }

                int x = paddingDay * (1 + dayOffset * 2) + (column * width) / 3;
                // + paddingDay;

                canvas.drawText(String.format("%d", day), x, y, dayLabelTextPaint);
                String key = year + "." + (month + 1) + "." + day;
                boolean has = checkSkiData(key);
                if (has) {
//                    canvas.drawCircle(x + CommonUtils.dp2px(mContext, 1), y - CommonUtils.dp2px(mContext, 4),
//                            DAY_LABEL_CIRCLE_RADIUS, dayLabelCircleBgPaint);

                    if (day < 10) {
                        canvas.drawCircle(x - CommonUtils.dp2px(mContext, 1) + 12, y - CommonUtils.dp2px(mContext, 4) - 22,
                                DAY_LABEL_CIRCLE_RADIUS, dayLabelCircleBgPaint);
                    } else {
                        canvas.drawCircle(x - CommonUtils.dp2px(mContext, 1) + 19, y - CommonUtils.dp2px(mContext, 4) - 22,
                                DAY_LABEL_CIRCLE_RADIUS, dayLabelCircleBgPaint);
                    }


                    dayLabelTextPaint.setColor(dayLabelTextTodayColor);
                    canvas.drawText(String.format("%d", day), x, y,
                            dayLabelTextPaint);
                    dayLabelTextPaint.setColor(dayLabelTextColor);

                    canvas.drawLine(x + CommonUtils.dp2px(mContext, 1) - DAY_LABEL_LINE_RADIUS, y - CommonUtils.dp2px(mContext, 4) + DAY_LABEL_LINE_RADIUS, x + CommonUtils.dp2px(mContext, 1) + DAY_LABEL_LINE_RADIUS, y - CommonUtils.dp2px(mContext, 4) + DAY_LABEL_LINE_RADIUS, monthLabelTextPaint);
                }

                int left = x + CommonUtils.dp2px(mContext, 1) - DAY_LABEL_LINE_RADIUS;
                int top = y - CommonUtils.dp2px(mContext, 4) - DAY_LABEL_LINE_RADIUS;
                int right = left + DAY_LABEL_LINE_RADIUS * 2;
                int bottom = top + DAY_LABEL_LINE_RADIUS * 2;
                RectF rect = new RectF(left, top, right, bottom);
                YearAdapter.CalendarMonth calendarKey = new YearAdapter.CalendarMonth(year, month + 1, day);
                rects.put(calendarKey, rect);

                dayOffset++;

                if (dayOffset == numDays) {
                    dayOffset = 0;
                    y += lineSpacingBetweenDayAndDay;
                }
                day++;
            }// end of while

        }// end of for
    }

    public void setSkiTime(HashMap<String, Long> dataMap) {
        this.dataMap = dataMap;
    }


    /**
     * 判断某年某月某日是否有滑雪数据
     *
     * @param key
     * @return
     */
    private boolean checkSkiData(String key) {
        return dataMap.containsKey(key);
        //return false;
    }

    private int findDayOffset() {
        return (dayOfWeekStart < weekStart ? (dayOfWeekStart + numDays)
                : dayOfWeekStart) - weekStart;
    }

    private void onDayClick(YearAdapter.CalendarMonth calendarMonth) {
        if (mOnMonthClickListener != null) {
            mOnMonthClickListener.onMonthClick(this, calendarMonth);
        }
    }

    private boolean isToday(int monthDay, Time time) {
        return (year == time.year) && (month == time.month)
                && (monthDay == time.monthDay);
    }

    public YearAdapter.CalendarMonth getMonthFromLocation(
            MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        int month = 0;
        // int columnWidth = width / 3;

        if (y < YEAR_HEADER_TEXT_HEIGHT) {
            return null;
        }

        if ((y <= YEAR_HEADER_TEXT_HEIGHT + rowMonthHeight)) {

            month = 0;


        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 2 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + rowMonthHeight)) {

            month = 1;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 3 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 2 * rowMonthHeight)) {

            month = 2;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 4 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 3 * rowMonthHeight)) {

            month = 3;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 5 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 4 * rowMonthHeight)) {

            month = 4;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 6 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 5 * rowMonthHeight)) {

            month = 5;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 7 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 6 * rowMonthHeight)) {

            month = 6;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 8 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 7 * rowMonthHeight)) {

            month = 7;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 9 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 8 * rowMonthHeight)) {

            month = 8;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 10 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 9 * rowMonthHeight)) {

            month = 9;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 11 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 10 * rowMonthHeight)) {

            month = 10;
        }
        if ((y <= YEAR_HEADER_TEXT_HEIGHT + 12 * rowMonthHeight) && (y > YEAR_HEADER_TEXT_HEIGHT + 11 * rowMonthHeight)) {

            month = 11;
        }


        return new YearAdapter.CalendarMonth(year, month, day);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        setMeasuredDimension(width, rowMonthHeight * 12 + YEAR_HEADER_TEXT_HEIGHT
                + lineSpacingBetweenYearAndMonth);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w;
    }

    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            YearAdapter.CalendarMonth calendarDay = getMonthFromLocation(event);
//            if (calendarDay != null) {
////                onDayClick(calendarDay);
//            }
            if (calendarDay == null) {
                return false;
            }


            YearAdapter.CalendarMonth key = checkContains(event.getX(), event.getY());

            if (day == -1) {
                Log.d("YearView", "not found day");
                return false;
            }

            if (mOnDayClickListener != null) {
                mOnDayClickListener.onDayClick(this, key);
            }
            return true;

        }

        return true;
    }

    private YearAdapter.CalendarMonth checkContains(float x, float y) {
        for (Map.Entry<YearAdapter.CalendarMonth, RectF> entry : rects.entrySet()) {
            if (entry.getValue().contains(x, y)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void reuse() {
        requestLayout();
    }

    public void setYearParams(int year, int month) {
        this.month = month;
        this.year = year;

        today = -1;

        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        dayOfWeekStart = calendar.get(Calendar.DAY_OF_WEEK);

        weekStart = calendar.getFirstDayOfWeek();
        numCells = CalendarUtils.getDaysInMonth(month, year);

        for (int i = 0; i < numCells; i++) {
            final int day = i + 1;
            if (isToday(day, currentTime)) {
                today = day;
            }
        }
    }


    @SuppressWarnings("deprecation")
    public void setYearParams(HashMap<String, Integer> params) {
        if (!params.containsKey(VIEW_PARAMS_YEAR_CURRENT)) {
            throw new InvalidParameterException(
                    "You must specify current_year for this view");
        }
        setTag(params);

        year = params.get(VIEW_PARAMS_YEAR_CURRENT);

        today = -1;

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        dayOfWeekStart = calendar.get(Calendar.DAY_OF_WEEK);

        if (params.containsKey(VIEW_PARAMS_WEEK_START)) {
            weekStart = params.get(VIEW_PARAMS_WEEK_START);
        } else {
            weekStart = calendar.getFirstDayOfWeek();
        }

        numCells = CalendarUtils.getDaysInMonth(month, year);

        for (int i = 0; i < numCells; i++) {
            final int day = i + 1;
            if (isToday(day, currentTime)) {
                today = day;
            }

        }

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        width = wm.getDefaultDisplay().getWidth();
    }

    public void setOnMonthClickListener(OnMonthClickListener onMonthClickListener) {
        mOnMonthClickListener = onMonthClickListener;
    }


    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        mOnDayClickListener = onDayClickListener;
    }

    public interface OnMonthClickListener {
        void onMonthClick(YearView simpleMonthView, YearAdapter.CalendarMonth calendarDay);
    }

    public interface OnDayClickListener {
        void onDayClick(YearView simpleMonthView, YearAdapter.CalendarMonth key);
    }

}