package com.hyphenate.easeui.utils;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.fullnetworkbasketball.models.User;
import com.fullnetworkbasketball.ui.user.UserManager;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.controller.EaseUI;
import com.hyphenate.easeui.controller.EaseUI.EaseUserProfileProvider;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.exceptions.HyphenateException;
import com.uhisports.basketball.R;

public class EaseUserUtils {
    
    static EaseUserProfileProvider userProvider;
    private static String avater;

    static {
        userProvider = EaseUI.getInstance().getUserProfileProvider();
    }
    
    /**
     * 根据username获取相应user
     * @param username
     * @return
     */
    public static EaseUser getUserInfo(String username){
        if(userProvider != null)
            return userProvider.getUser(username);
        
        return null;
    }
    
    /**
     * 设置用户头像
     * @param username
     */
    public static void setUserAvatar(Context context, String username, ImageView imageView){
    	EaseUser user = getUserInfo(username);

       User user1= UserManager.getIns().getUser();
//
//        if(user != null && user.getAvatar() != null){
//            try {
//                int avatarResId = Integer.parseInt(user.getAvatar());
//                Glide.with(context).load(avatarResId).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);
//            } catch (Exception e) {
//                //正常的string路径
//                Glide.with(context).load(user1.avatar).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);
//
////                ImageLoader.loadCicleImage(context,user.getAvatar(),R.mipmap.default_person,imageView);
//            }
//        }else{
//            Glide.with(context).load(R.mipmap.default_person).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);
//        }

        Glide.with(context).load(user.getAvatar()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);

    }
    public static void setUserAvatar2(Context context, String username, ImageView imageView,EMMessage message){
        EaseUser user = getUserInfo(username);

        User user1= UserManager.getIns().getUser();
//
//        if(user != null && user.getAvatar() != null){
//            try {
//                int avatarResId = Integer.parseInt(user.getAvatar());
//                Glide.with(context).load(avatarResId).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);
//            } catch (Exception e) {
//                //正常的string路径
//                Glide.with(context).load(user1.avatar).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);
//
////                ImageLoader.loadCicleImage(context,user.getAvatar(),R.mipmap.default_person,imageView);
//            }
//        }else{
//            Glide.with(context).load(R.mipmap.default_person).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);
//        }
        try {
          avater = message.getStringAttribute("avatar");
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
        Glide.with(context).load(avater).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.mipmap.default_person).bitmapTransform(new ImageLoader.CropCircleTransformation(context)).into(imageView);

    }
    /**
     * 设置用户昵称
     */
    public static void setUserNick(String username,TextView textView){
        if(textView != null){
        	EaseUser user = getUserInfo(username);
        	if(user != null && user.getNick() != null){
        		textView.setText(user.getNick());
        	}else{

        		textView.setText(username);
        	}
        }
    }
    /**
     * 设置用户昵称
     */
    public static void setUserNick2(String username,TextView textView,EMMessage message){
        if(textView != null){
            EaseUser user = getUserInfo(username);
            if(user != null && user.getNick() != null){
                textView.setText(user.getNick());
            }else{
                try {
                    username = message.getStringAttribute("userName");
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
                textView.setText(username);
            }
        }
    }
}
