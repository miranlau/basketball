package net.dr.qwzq;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.match.BasketBallMatchScoreActivity;
import com.fullnetworkbasketball.utils.ImageLoader;
import com.fullnetworkbasketball.utils.SharePreHelper;
import com.fullnetworkbasketball.utils.T;
import com.fullnetworkbasketball.views.VibrateHelp;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 球场界面
 */
public class ConfirmFirstActivity extends AppCompatActivity implements View.OnClickListener, StageListView.OnSelectCallbackListener, StageView.OnChangeCallbackListener {

    //    @Bind(R.id.title)
//    TextView title;
//    @Bind(R.id.sure)
//    TextView sure;
    @Bind(R.id.ll_home_team)
    LinearLayout llHomeTeam;
    @Bind(R.id.ll_visit_team)
    LinearLayout llVisitTeam;
    //    @Bind(R.id.ll_listView_right)
//    LinearLayout llListViewRight;
//    @Bind(R.id.footboall)
//    LinearLayout footboall;
//    @Bind(R.id.ll_listView_left)
//    LinearLayout llListViewLeft;
//    @Bind(R.id.list_left)
//    StageListView listLeft;
    @Bind(R.id.ll_change)
    LinearLayout llChange;
    @Bind(R.id.ready_go)
    TextView readyGo;
    @Bind(R.id.home_head)
    ImageView homeHead;
    @Bind(R.id.home_name)
    TextView homeName;
    @Bind(R.id.visit_head)
    ImageView visitHead;
    @Bind(R.id.visit_name)
    TextView visitName;
    //球场
    private StageView mStageView;

    //备战区
    private StageListView list;

    //球员数据
    private PlayerAdapter mAdapter;

    private PlayerAdapter mLeftAdapter;

    private ArrayList<Player> players;

    private ArrayList<Player> playersForVisit;

    private int matchId;
    private int teamId;
    //    private Match match;
    private int system;
    private Game game;
    private ArrayList<String> list_json;
    private ArrayList<Display> displays;


    List<StagePlayer> players1;

    List<StagePlayer> players2;


    List<StagePlayer> playersHome;

    List<StagePlayer> playersVisit;


    List<StagePlayer> playersAll = new ArrayList<StagePlayer>();

    private ArrayList<Display> displays_home = new ArrayList<Display>();

    private ArrayList<Display> displays_visit = new ArrayList<Display>();

    //所有球员
    private List<StagePlayer> mPlayerses = new ArrayList<StagePlayer>();

    private Match match;

    private boolean isHome=true;

    private int mHomePlayers = 0;//球场上主队人数
    private int mVIsitPlayers = 0; //球场上客队人数

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_first_activity);
        ButterKnife.bind(this);

        players = (ArrayList<Player>) getIntent().getSerializableExtra("playerList");

        playersForVisit = (ArrayList<Player>) getIntent().getSerializableExtra("playerListForVisit");

        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        match = (Match) getIntent().getSerializableExtra("match");

        SharePreHelper.getIns().setChangeTeam(true);
        SharePreHelper.getIns().setStageTouchable(true);
        llHomeTeam.setOnClickListener(this);
        llVisitTeam.setOnClickListener(this);
        readyGo.setOnClickListener(this);
        llHomeTeam.setSelected(true);

//        players = (ArrayList<Player>) getIntent().getSerializableExtra("playerList");
//        matchId = getIntent().getIntExtra("matchId", 0);
//        teamId = getIntent().getIntExtra("teamId", 0);
//        title.setText("确认首发");
        View view = LayoutInflater.from(this).inflate(R.layout.confirm_first_left_layout, null);
        llChange.addView(view);
        mStageView = (StageView) view.findViewById(R.id.stageView);
        mStageView.setOnRemoveCallbackListener(this);
        list = (StageListView) view.findViewById(R.id.list);
        list.setOnSelectCallbackListener(this);
        mAdapter = new PlayerAdapter(this);
        list.setAdapter(mAdapter);
        list.setStageView(mStageView);
        mLeftAdapter = new PlayerAdapter(this);
        getSystem();
        test();
    }

    private void resetState() {
        if (mStageView != null) {
            mStageView.onDestory();
        }

        llChange.removeAllViews();

        llVisitTeam.setSelected(false);
        llHomeTeam.setSelected(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mStageView.onDestory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStageView.onDestory();
    }

    private void test() {
        players1 = new ArrayList<StagePlayer>();
        for (int i = 0; i < players.size(); i++) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
            StagePlayer stagePlayer = new StagePlayer(players.get(i).number, mBitmap);
            stagePlayer.setName(players.get(i).name);
            stagePlayer.setRealName(players.get(i).realName);
            stagePlayer.setPicture(players.get(i).avatar);
            stagePlayer.id = players.get(i).id;
            players1.add(stagePlayer);
        }
        initData(players1);


//
//        players1 = new ArrayList<StagePlayer>();
//        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
//        for (int i = 0; i < 15; i++) {
//            StagePlayer player = new StagePlayer(i, mBitmap);
//            player.setRealName("test" + i);
//            players1.add(player);
//        }

        players2 = new ArrayList<StagePlayer>();
        for (int i = 0; i < playersForVisit.size(); i++) {
            Bitmap mBitmap2 = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
            StagePlayer stagePlayer = new StagePlayer(playersForVisit.get(i).number, mBitmap2);
            stagePlayer.setName(playersForVisit.get(i).name);
            stagePlayer.setRealName(playersForVisit.get(i).realName);
            stagePlayer.setPicture(playersForVisit.get(i).avatar);
            stagePlayer.id = playersForVisit.get(i).id;
            players2.add(stagePlayer);
        }
//
        homeName.setText(match.home.name);
        visitName.setText(match.visiting.name);
        ImageLoader.loadCicleImage(this,match.home.logo,R.mipmap.default_team,homeHead);
        ImageLoader.loadCicleImage(this,match.visiting.logo,R.mipmap.default_team,visitHead);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                displays = new ArrayList<>();
                mStageView.calculateCoordinate();

                mPlayerses = mStageView.getPlayerManager().getPlayerses();

                for (StagePlayer stagePlayer : mPlayerses) {
                    Display display = new Display();
                    display.avatar = stagePlayer.getPicture();
                    display.name = stagePlayer.getName();
                    display.realName = stagePlayer.getRealName();
                    display.number = stagePlayer.getNumber();
                    display.id = stagePlayer.id;
                    display.x = stagePlayer.getLocationX();
                    display.y = stagePlayer.getLocationY();
                    displays.add(display);
                }
                startPlayer();
//                commitPlayerLocation(mStageView.getPlayerManager().getPlayerses());
                break;
            case R.id.sure:
                finish();
                break;
            case R.id.ll_home_team:
                if (llHomeTeam.isSelected()) {
                    return;
                }
                SharePreHelper.getIns().setChangeTeam(true);
                SharePreHelper.getIns().setStageTouchable(true);

                recordVisitPlayer(mStageView);
                resetState();

                llHomeTeam.setSelected(true);
                View view = LayoutInflater.from(this).inflate(R.layout.confirm_first_left_layout, null);
                llChange.addView(view);
                mStageView = (StageView) view.findViewById(R.id.stageView);
                mStageView.setOnRemoveCallbackListener(this);
                list = (StageListView) view.findViewById(R.id.list);
                list.setOnSelectCallbackListener(this);
                mAdapter = new PlayerAdapter(this);
                list.setAdapter(mAdapter);
                list.setStageView(mStageView);
                initData(players1);
                mStageView.setOnViewInitListener(new StageView.OnViewInitListener() {
                    @Override
                    public void onInited() {
                        if (playersHome == null || playersHome.size() == 0) {
                            return;
                        }
                        playersHome.clear();
                        for (Display display : displays_home) {
                            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
                            StagePlayer stagePlayer = new StagePlayer(display.number, mBitmap);
                            stagePlayer.setName(display.name);
                            stagePlayer.setRealName(display.realName);
                            stagePlayer.setPicture(display.avatar);
                            stagePlayer.id = display.id;
                            stagePlayer.setStateIn(true);

                            stagePlayer.setLocationX((int) display.x);

                            stagePlayer.setLocationY((int) display.y);


                            playersHome.add(stagePlayer);
                        }

                        mStageView.initPlayers(playersHome);
                    }
                });
                break;
            case R.id.ll_visit_team:
                if (llVisitTeam.isSelected()) {
                    return;
                }
                SharePreHelper.getIns().setChangeTeam(false);
                SharePreHelper.getIns().setStageTouchable(true);
                boolean isHome = SharePreHelper.getIns().getIsChangeTeam();
//                if (playersHome==null||playersHome.size()==0){
//                    T.show(this,"请选择主队上场队员!",1000);
//                    return;
//                }
                recordHomePlayer(mStageView);
                resetState();
                llVisitTeam.setSelected(true);
                View view_visit = LayoutInflater.from(this).inflate(R.layout.confirm_first_right_layout, null);
                llChange.addView(view_visit);
                mStageView = (StageView) view_visit.findViewById(R.id.stageView_right);
                mStageView.setOnRemoveCallbackListener(this);
                list = (StageListView) view_visit.findViewById(R.id.list_right);
                list.setOnSelectCallbackListener(this);
                mAdapter = new PlayerAdapter(this);
                list.setAdapter(mAdapter);
                list.setStageView(mStageView);
                initData(players2);
                mStageView.setOnViewInitListener(new StageView.OnViewInitListener() {
                    @Override
                    public void onInited() {
                        if (playersVisit == null || playersVisit.size() == 0) {
                            return;
                        }
                        playersVisit.clear();
                        for (Display display : displays_visit) {
                            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
                            StagePlayer stagePlayer = new StagePlayer(display.number, mBitmap);
                            stagePlayer.setName(display.name);
                            stagePlayer.setRealName(display.realName);
                            stagePlayer.setPicture(display.avatar);
                            stagePlayer.id = display.id;
                            stagePlayer.setStateIn(true);

                            stagePlayer.setLocationX((int) display.x);

                            stagePlayer.setLocationY((int) display.y);


                            playersVisit.add(stagePlayer);
                        }

                        mStageView.initPlayers(playersVisit);
                    }
                });
                break;
            case R.id.ready_go:

                if (displays_visit == null) {
                    return;
                }

                if (mHomePlayers == 0 ) {
                    T.show(this, "请选择主队上场队员!", 1000);
                    return;
                }
                if (mVIsitPlayers == 0 ) {
                    T.show(this, "请选择客队上场队员!", 1000);
                    return;
                }

                if (mHomePlayers>system || mVIsitPlayers >system){
                    T.showShort(this, getResources().getString(R.string.system_more));
                    return;
                }

//                if (playersVisit == null||playersVisit.size()==0) {
//                    recordVisitPlayer(mStageView);
//                    playersVisit.clear();
//                    for (Display display : displays_visit) {
//                        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
//                        StagePlayer stagePlayer = new StagePlayer(display.number, mBitmap);
//                        stagePlayer.setName(display.name);
//                        stagePlayer.setRealName(display.realName);
//                        stagePlayer.setPicture(display.avatar);
//                        stagePlayer.id = display.id;
//                        stagePlayer.setStateIn(true);
//
//                        stagePlayer.setLocationX((int) display.x);
//
//                        stagePlayer.setLocationY((int) display.y);
//
//
//                        playersVisit.add(stagePlayer);
//                    }
//                }
                if (llHomeTeam.isSelected()){
                    recordHomePlayer(mStageView);
                    //   playersHome.clear();
                    for (Display display : displays_home) {
                        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
                        StagePlayer stagePlayer = new StagePlayer(display.number, mBitmap);
                        stagePlayer.setName(display.name);
                        stagePlayer.setRealName(display.realName);
                        stagePlayer.setPicture(display.avatar);
                        stagePlayer.id = display.id;
                        stagePlayer.setStateIn(true);

                        stagePlayer.setLocationX((int) display.x);

                        stagePlayer.setLocationY((int) display.y);

                        playersHome.add(stagePlayer);
                    }
                }else {
                    recordVisitPlayer(mStageView);
                    //    playersVisit.clear();
                    for (Display display : displays_visit) {
                        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
                        StagePlayer stagePlayer = new StagePlayer(display.number, mBitmap);
                        stagePlayer.setName(display.name);
                        stagePlayer.setRealName(display.realName);
                        stagePlayer.setPicture(display.avatar);
                        stagePlayer.id = display.id;
                        stagePlayer.setStateIn(true);

                        stagePlayer.setLocationX((int) display.x);

                        stagePlayer.setLocationY((int) display.y);


                        playersVisit.add(stagePlayer);
                    }
                }
//                if (playersHome == null||playersHome.size()==0) {
//
//                }


                startPlayer();
                resetState();
                llHomeTeam.setEnabled(false);
                llVisitTeam.setEnabled(false);
                View view_jump = LayoutInflater.from(this).inflate(R.layout.jump_ball_layout, null);
                llChange.addView(view_jump);
                mStageView = (StageView) view_jump.findViewById(R.id.stageView_jump_ball);
                playersAll.clear();
                for (StagePlayer stagePlayer : playersHome) {
                    playersAll.add(stagePlayer);
                }

                for (StagePlayer stagePlayer : playersVisit) {
                    playersAll.add(stagePlayer);
                }

                mStageView.setOnViewInitListener(new StageView.OnViewInitListener() {
                    @Override
                    public void onInited() {
                        mStageView.initPlayers(playersAll);
                    }
                });


                break;
        }

    }

    private void recordHomePlayer(StageView mStageView) {
        mAdapter.notifyDataSetChanged();
        isHome =false;
        displays_home.clear();
        mStageView.calculateCoordinate();
        playersHome = mStageView.getPlayerManager().getPlayerses();

        for (StagePlayer player : playersHome) {
            players1.remove(player);
        }
        for (StagePlayer stagePlayer : playersHome) {
            Display display = new Display();
            display.avatar = stagePlayer.getPicture();
            display.name = stagePlayer.getName();
            display.realName = stagePlayer.getRealName();
            display.number = stagePlayer.getNumber();
            display.id = stagePlayer.id;
            display.x = stagePlayer.getLocationX();
            display.y = stagePlayer.getLocationY();
            displays_home.add(display);

        }

//        T.show(this, "主队阵型" + "X--->" + displays_home.get(0).x + "Y--->" + displays_home.get(0).y + "  右边需要减掉的格子数" + App.getInst().rightCancelGrid(), 1000);
    }

    private void recordVisitPlayer(StageView mStageView) {
        mAdapter.notifyDataSetChanged();
        isHome = true;
        displays_visit.clear();
        mStageView.calculateCoordinate();
        playersVisit = mStageView.getPlayerManager().getPlayerses();

        for (StagePlayer player : playersVisit) {
            players2.remove(player);
        }
        for (StagePlayer stagePlayer : playersVisit) {
            Display display = new Display();
            display.avatar = stagePlayer.getPicture();
            display.name = stagePlayer.getName();
            display.realName = stagePlayer.getRealName();
            display.number = stagePlayer.getNumber();
            display.id = stagePlayer.id;
            display.x = stagePlayer.getLocationX();
            display.y = stagePlayer.getLocationY();
            displays_visit.add(display);
        }



//        T.show(this, "客队阵型" + "X--->" + displays_visit.get(0).x + "Y--->" + displays_visit.get(0).y + "  右边需要减掉的格子数" + App.getInst().rightCancelGrid(), 1000);
    }

    @Override
    public void onSelectCallback(StagePlayer player) {
        mStageView.setReadyMovePlayer(player);
    }

    /**
     * 通过网络获得数据,需要重写
     *
     * @param players
     */
    protected void initData(List<StagePlayer> players) {
        mAdapter.refresh(players);
        mLeftAdapter.refresh(players);
    }


    /**
     * 移动处理之后,提交数据到服务器,需要重写
     *
     * @param players
     */
    protected void commitPlayerLocation(List<StagePlayer> players) {
        for (StagePlayer player : players) {
            Log.i("Main", "location x : " + player.getLocationX() + " y : " + player.getLocationY());
        }
    }

    /**
     * 移动过程中球员变化 TODO
     *
     * @param pStageView 球场
     * @param pPlayers   被变化球员
     * @param moveOut
     */
    @Override
    public void onChangeCallback(StageView pStageView, StagePlayer pPlayers, boolean moveOut) {

        if (!moveOut) { //移进球场
            mAdapter.remove(pPlayers);
          if (isHome == true){
              mHomePlayers++;
          }else if (isHome ==false){
              mVIsitPlayers++;
          }
        } else {
            mAdapter.add(pPlayers);
            if (isHome ==true ){
                mHomePlayers--;
            }else if (isHome ==false){
                mVIsitPlayers--;
            }
        }
    }

    private void getSystem() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    match = response.getData();
                    system = match.type;
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void isStarting(final int teamId, final Match match) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();

//                    if (game.statring.size() > 0 && game.statring != null) {
//                        Intent intent = new Intent(ConfirmFirstActivity.this, MatchScoreActivity.class);
//                        intent.putExtra("matchId", match.id);
//                        intent.putExtra("teamId", teamId);
//                        intent.putExtra("game", game);
//                        startActivity(intent);
//                        finish();
//                    } else {
//                        Intent intent = new Intent(ConfirmFirstActivity.this, PreRacingActivity.class);
//                        intent.putExtra("matchId", match.id);
//                        intent.putExtra("teamId", teamId);
//                        startActivity(intent);
//                    }

                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void startPlayer() {
        if (displays_home.size() == 0 || displays_visit.size() == 0) {
            T.showShort(this, getResources().getString(R.string.start_player));
            return;
        }
        if (displays_home.size() > system) {//所选人数超过了比赛赛制人数
            T.showShort(this, getResources().getString(R.string.system_more));
            return;
        }

        if (displays_visit.size() > system) {
            T.showShort(this, getResources().getString(R.string.system_more));
            return;
        }
//        list_json = new ArrayList<>();
//        for (Display display : displays_home) {
//            String json = new Gson().toJson(display);
//            list_json.add(json);
//        }


        for (Display display:displays_visit){
            display.x = display.x-App.getInst().rightCancelGrid()-2;
        }
        String json = new Gson().toJson(displays_home);
        String jsonVisit = new Gson().toJson(displays_visit);
        Log.i("list_to_string", jsonVisit);
        Api.getRetrofit().startPlayerBeforeMatch(json, jsonVisit, matchId).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            public final int VIBRATE_TIME=500;

            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    SharePreHelper.getIns().setStageTouchable(false);
                    SharePreHelper.getIns().setConfirmFirstSuccess(true);
                    T.showShort(getApplicationContext(), response.getMessage());
                    if (SharePreHelper.getIns().getConfirmFirstSuccess()) {
                        readyGo.setText("跳球开始");

                        readyGo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                VibrateHelp.vSimple(view.getContext(), VIBRATE_TIME);
                                matchScore(11);

                            }
                        });
                    }
//                    isStarting(teamId, match);

                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void matchScore(int actionType) {
        final HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        params.put("status", actionType);
        Api.getRetrofit().basketballMatchScore(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    Intent intent = new Intent(ConfirmFirstActivity.this, BasketBallMatchScoreActivity.class);
                    intent.putExtra("matchId", matchId);
                    intent.putExtra("teamId", teamId);
                    intent.putExtra("match",match);
                    startActivity(intent);
                    finish();

                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

}
