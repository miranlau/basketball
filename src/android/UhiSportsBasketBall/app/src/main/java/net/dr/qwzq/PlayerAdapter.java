package net.dr.qwzq;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fullnetworkbasketball.utils.ImageLoader;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther Kalen
 * @time 16/7/1 下午7:15
 * @des 球员备战区数据适配器
 */
public class PlayerAdapter extends BaseAdapter {

    //用来存放选中的对象
    private List<StagePlayer> players;
    //供adapter获取选中数据的方法
    private Activity context;

    public PlayerAdapter(Activity context) {
        this.context = context;
        this.players = new ArrayList<StagePlayer>();
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.start_players_item, parent, false);
            holder = new ViewHolder();
            holder.choose = (ImageView) convertView.findViewById(R.id.iv_choose);
            holder.number = (TextView) convertView.findViewById(R.id.number);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.iv_leader = (ImageView) convertView.findViewById(R.id.player_header);
            holder.itelRL = (LinearLayout) convertView.findViewById(R.id.lt);
            holder.player_header = (ImageView) convertView.findViewById(R.id.player_header);
            holder.layout_header = (FrameLayout)convertView.findViewById(R.id.layout_header);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.init((StagePlayer) getItem(position));
        return convertView;
    }

    public void remove(StagePlayer pPlayers) {
        players.remove(pPlayers);
        notifyDataSetChanged();
    }

    public void add(StagePlayer pPlayers) {
        players.add(pPlayers);
        notifyDataSetChanged();
    }


    public class ViewHolder {
        public ImageView choose;
        public LinearLayout itelRL;
        public TextView number;
        public ImageView player_header;
        public ImageView iv_leader;
        public TextView name;
        public FrameLayout layout_header;

        public void init(final StagePlayer stagePlayer) {
            layout_header.setFocusable(false);
            layout_header.setFocusableInTouchMode(false);
            layout_header.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
            name.setText(stagePlayer.getFullName());
            number.setText(stagePlayer.getNumber() + "");
            ImageLoader.loadCicleImage(context, stagePlayer.getPicture(), R.mipmap.default_person, player_header);
            player_header.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (mOnSelectCallbackListener != null) {

                        mOnSelectCallbackListener.onSelectCallback(stagePlayer);
                    }
                    return true;
                }
            });
        }
    }

    /**
     * 刷新球员列表
     * @param pList
     */
    public void refresh(List<StagePlayer> pList){
        if (pList == null){
            return;
        }
        players.clear();
        players.addAll(pList);
        notifyDataSetChanged();
    }

    private OnSelectCallbackListener mOnSelectCallbackListener;

    public void setOnSelectCallbackListener(OnSelectCallbackListener pOnSelectCallbackListener) {
        this.mOnSelectCallbackListener = pOnSelectCallbackListener;
    }


    public interface OnSelectCallbackListener {
        public void onSelectCallback(StagePlayer player);

    }
}