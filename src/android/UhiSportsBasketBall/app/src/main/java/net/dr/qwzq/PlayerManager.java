package net.dr.qwzq;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther Kalen
 * @time 16/7/1 下午7:15
 * @des 球场管理
 */
public class PlayerManager {

    /**
     * 球场横线格子数
     */
    public static final int GRID_X = 95;
    /**
     * 球场纵向格子数
     */
    public static final int GRID_Y = 39;

    //所有球员
    private List<StagePlayer> mPlayerses = new ArrayList<StagePlayer>();

    //球员衣服
    private Bitmap mBitmap = null;

    //球场背景,暂时没有使用
    private Bitmap mBackground = null;

    //资源类
    private Resources mResources;


    public PlayerManager(Resources pResources) {
        this.mResources = pResources;
        initBitmap();
    }


    /**
     * 获得球场衣服
     *
     * @return
     */
    public Bitmap getBackgroundBitmap() {
        return mBackground;
    }

    /**
     * 初始化球场图片
     */
    private void initBitmap() {
        mBitmap = BitmapFactory.decodeResource(mResources, R.mipmap.player_clothes);
        mBackground = BitmapFactory.decodeResource(mResources, R.mipmap.basketball_court);
    }

    /**
     * 绘制球员
     *
     * @param canvas
     */
    public void onDraw(Canvas canvas) {
        float textSize = mResources.getDisplayMetrics().density * 24;
        for (StagePlayer players : getPlayers()) {
            players.onDraw(canvas, textSize);
        }
    }

    /**
     * 判断此坐标是否选中了某球员
     *
     * @param x
     * @param y
     * @return
     */
    public StagePlayer containsPlayer(float x, float y) {
        for (StagePlayer player : getPlayers()) {
            if (player.contains(x, y)) {
                return player;
            }
        }
        return null;
    }

    /**
     * 判断此坐标是否选中了某球员,排除自身外
     *
     * @param filter
     * @param x
     * @param y
     * @return
     */
    public StagePlayer containsPlayer(StagePlayer filter, float x, float y) {
        for (StagePlayer player : getPlayers()) {
            if (player.getNumber() != filter.getNumber() && player.contains(x, y)) {
                return player;
            }
        }
        return null;
    }

    /**
     * 删除球员
     *
     * @param player
     */
    public void removePlayers(StagePlayer player) {
        mPlayerses.remove(player);
    }

    /**
     * 计算球员坐标系
     *
     * @param width
     * @param height
     */
    public void calculateCoordinate(int width, int height) {
        for (StagePlayer players : getPlayers()) {
            players.calculateCoordinate(width, height);
        }
    }

    /**
     * 返回所有球员
     *
     * @return
     */
    public List<StagePlayer> getPlayerses() {
        return mPlayerses;
    }

    public void removeAllPlayers(){
        mPlayerses.clear();
    }
    /**
     * 初始化球场球员
     *
     * @param pList
     */
    public void initPlayers(List<StagePlayer> pList) {
        if (pList == null) {
            return;
        }

        mPlayerses.clear();
        mPlayerses.addAll(pList);
    }

    /**
     * 球场添加球员
     *
     * @param player
     */
    public void addPlayers(StagePlayer player) {
        mPlayerses.add(player);
    }

    private StagePlayer[] getPlayers() {
        StagePlayer[] players = new StagePlayer[0];
        return mPlayerses.toArray(players);
    }
}
