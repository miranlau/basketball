package net.dr.qwzq;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * @auther Kalen
 * @time 16/7/1 下午9:25
 * @des ${TODO}
 */
public class StageListView extends ListView {
    private int mCurrentPosition = 0;
    private static final String TAG = "dragPlayer";
    private float DownX;
    private float DownY;

    public StageListView(Context context) {
        super(context);
    }

    public StageListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StageListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StageListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    private StageView mStageView;

    public void setStageView(StageView pStageView) {
        this.mStageView = pStageView;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "ListView dispatchTouchEvent");
        getParent().requestDisallowInterceptTouchEvent(true);
        int action = ev.getAction();
        float x = ev.getX();
        float y = ev.getY();

        MotionEvent event2 = MotionEvent.obtain(ev);
        event2.setLocation(ev.getRawX(), ev.getRawY() - getStatusBarHeight()- 160);
        mStageView.dispatchTouchEvent(event2);
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                handleTouchDown(pointToPosition((int) x, (int) y));
                Log.e(TAG, "ListView dispatchTouchEvent ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                if (handleTouchMove(x)) {
                    return super.dispatchTouchEvent(ev);
                }
                if (mOnSelectCallbackListener != null) {
                    StagePlayer player = (StagePlayer) getItemAtPosition(mCurrentPosition);
                    mOnSelectCallbackListener.onSelectCallback(player);
                }
                return false;
            case MotionEvent.ACTION_UP:
                handleTouchUp(mCurrentPosition);
                mCurrentPosition = -1;
                Log.e(TAG, "ListView dispatchTouchEvent ACTION_UP");
                break;
            case MotionEvent.ACTION_CANCEL:

                break;


            default:
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d(TAG, "ListView onInterceptTouchEvent");
        return true;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        getParent().requestDisallowInterceptTouchEvent(true);
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                Log.e(TAG, "ListView onTouchEvent ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                requestDisallowInterceptTouchEvent(true);
                Log.e(TAG, "ListView onTouchEvent ACTION_MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.e(TAG, "ListView onTouchEvent ACTION_UP");
                break;

            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    private void handleTouchDown(int position) {
        mCurrentPosition = position;
        if (mOnSelectCallbackListener != null) {
            StagePlayer player = (StagePlayer) getItemAtPosition(position);
            mOnSelectCallbackListener.onSelectCallback(player);
        }
    }
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    private void handleTouchUp(int position) {

    }

    private boolean handleTouchMove(float x) {
        return x > 0;
    }
    private OnSelectCallbackListener mOnSelectCallbackListener;

    public void setOnSelectCallbackListener(OnSelectCallbackListener pOnSelectCallbackListener) {
        this.mOnSelectCallbackListener = pOnSelectCallbackListener;
    }


    public interface OnSelectCallbackListener {
        public void onSelectCallback(StagePlayer player);

    }
}
