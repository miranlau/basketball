package net.dr.qwzq;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.fullnetworkbasketball.models.Display;
import com.fullnetworkbasketball.models.Game;
import com.fullnetworkbasketball.models.Match;
import com.fullnetworkbasketball.models.Player;
import com.fullnetworkbasketball.net.Api;
import com.fullnetworkbasketball.net.HttpParamsHelper;
import com.fullnetworkbasketball.net.HttpResponseFWF2;
import com.fullnetworkbasketball.net.RequestCallbackFWF;
import com.fullnetworkbasketball.ui.home.PreRacingActivity;
import com.fullnetworkbasketball.ui.match.MatchScoreActivity;
import com.fullnetworkbasketball.utils.T;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.uhisports.basketball.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 球场界面
 */
public class StagePlayerActivity extends AppCompatActivity implements View.OnClickListener, StageListView.OnSelectCallbackListener, StageView.OnChangeCallbackListener {

    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.sure)
    TextView sure;
    //球场
    private StageView mStageView;

    //备战区
    private StageListView list;

    //球员数据
    private PlayerAdapter mAdapter;
    private ArrayList<Player> players;
    private int matchId;
    private int teamId;
    private Match match;
    private int system;
    private Game game;
    private ArrayList<String> list_json;
    private ArrayList<Display> displays;

    //所有球员
    private List<StagePlayer> mPlayerses = new ArrayList<StagePlayer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stage_player_activity);
        ButterKnife.bind(this);
        players = (ArrayList<Player>) getIntent().getSerializableExtra("playerList");
        matchId = getIntent().getIntExtra("matchId", 0);
        teamId = getIntent().getIntExtra("teamId", 0);
        title.setText("确认首发");
        mStageView = (StageView) findViewById(R.id.stage);
        mStageView.setOnRemoveCallbackListener(this);
        list = (StageListView) findViewById(R.id.list);
        list.setOnSelectCallbackListener(this);
        sure.setOnClickListener(this);
        findViewById(R.id.confirm).setOnClickListener(this);
        mAdapter = new PlayerAdapter(this);
        list.setAdapter(mAdapter);
        list.setStageView(mStageView);
        getSystem();
        test();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mStageView.onDestory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mStageView.onDestory();
    }

    private void test() {
        List<StagePlayer> players1 = new ArrayList<StagePlayer>();
//        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
//        StagePlayer players1 = new StagePlayer(2, mBitmap);
//        players1.setRealName("Kalen");
//        StagePlayer players2 = new StagePlayer(3, mBitmap);
//        players2.setRealName("Holen");
//        players.add(players1);
//        players.add(players2);


        for (int i = 0; i < players.size(); i++) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
            StagePlayer stagePlayer = new StagePlayer(players.get(i).number, mBitmap);
            stagePlayer.setName(players.get(i).name);
            stagePlayer.setRealName(players.get(i).realName);
            stagePlayer.setPicture(players.get(i).avatar);
            stagePlayer.id = players.get(i).id;
            players1.add(stagePlayer);
        }
        initData(players1);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                displays = new ArrayList<>();
                mStageView.calculateCoordinate();

                mPlayerses = mStageView.getPlayerManager().getPlayerses();

                for (StagePlayer stagePlayer : mPlayerses) {
                    Display display = new Display();
                    display.avatar = stagePlayer.getPicture();
                    display.name = stagePlayer.getName();
                    display.realName = stagePlayer.getRealName();
                    display.number = stagePlayer.getNumber();
                    display.id = stagePlayer.id;
                    display.x = stagePlayer.getLocationX();
                    display.y = stagePlayer.getLocationY();
                    displays.add(display);
                }
                startPlayer();
//                commitPlayerLocation(mStageView.getPlayerManager().getPlayerses());
                break;
            case R.id.sure:
                finish();
                break;
        }

    }


    @Override
    public void onSelectCallback(StagePlayer player) {
        mStageView.setReadyMovePlayer(player);
    }


    /**
     * 通过网络获得数据,需要重写
     *
     * @param players
     */
    protected void initData(List<StagePlayer> players) {
        mAdapter.refresh(players);
    }


    /**
     * 移动处理之后,提交数据到服务器,需要重写
     *
     * @param players
     */
    protected void commitPlayerLocation(List<StagePlayer> players) {
        for (StagePlayer player : players) {
            Log.i("Main", "location x : " + player.getLocationX() + " y : " + player.getLocationY());
        }
    }

    /**
     * 移动过程中球员变化
     *
     * @param pStageView 球场
     * @param pPlayers   被变化球员
     * @param moveOut
     */
    @Override
    public void onChangeCallback(StageView pStageView, StagePlayer pPlayers, boolean moveOut) {
//        Toast.makeText(StagePlayerActivity.this, "player " + pPlayers.getNumber() + (moveOut ? " out" : " in"), Toast.LENGTH_SHORT).show();
        if (!moveOut) {
            mAdapter.remove(pPlayers);
            Log.i("1111111111111111", "StagePlayerActivity");
        } else {
            mAdapter.add(pPlayers);
        }
    }

    private void getSystem() {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();
        params.put("matchId", matchId);
        Api.getRetrofit().queryMatchDetails(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Match>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Match> response) {
                Logger.i("msg:" + response.toString());
                if (response.isSuccess()) {

                    match = response.getData();
                    system = match.type;
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void isStarting(final int teamId, final Match match) {
        HashMap<String, Object> params = HttpParamsHelper.createParamsFWF();

        params.put("matchId", matchId);
        params.put("teamId", teamId);
        Api.getRetrofit().gaming(params).enqueue(new RequestCallbackFWF<HttpResponseFWF2<Game>>() {
            @Override
            public void onSuccess(HttpResponseFWF2<Game> response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {

                    game = response.getData();

                    if (game.homeStarting.size() > 0 && game.homeStarting != null) {
                        Intent intent = new Intent(StagePlayerActivity.this, MatchScoreActivity.class);
                        intent.putExtra("matchId", match.id);
                        intent.putExtra("teamId", teamId);
                        intent.putExtra("game", game);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(StagePlayerActivity.this, PreRacingActivity.class);
                        intent.putExtra("matchId", match.id);
                        intent.putExtra("teamId", teamId);
                        startActivity(intent);
                    }

                }else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void startPlayer() {
        if (displays.size() == 0) {
            T.showShort(this, getResources().getString(R.string.start_player));
            return;
        }
        if (displays.size() > system) {
            T.showShort(this, getResources().getString(R.string.system_more));
            return;
        }
        list_json = new ArrayList<>();
        for (Display display : displays) {
            String json = new Gson().toJson(display);
            list_json.add(json);
        }

        String json = new Gson().toJson(displays);
        Log.i("list_to_string", json);
        Api.getRetrofit().startPlayerBeforeMatch(json, "", matchId).enqueue(new RequestCallbackFWF<HttpResponseFWF2>() {
            @Override
            public void onSuccess(HttpResponseFWF2 response) {
                Logger.i("msg:" + response.toString());

                if (response.isSuccess()) {
                    isStarting(teamId, match);

                } else {
                    T.showShort(getApplicationContext(), response.getMessage());
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }


}
