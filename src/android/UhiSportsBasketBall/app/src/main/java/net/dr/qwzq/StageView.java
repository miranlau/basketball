package net.dr.qwzq;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import com.fullnetworkbasketball.App;
import com.fullnetworkbasketball.utils.ScreenUtils;
import com.fullnetworkbasketball.utils.SharePreHelper;

import java.util.List;

/**
 * @auther Kalen
 * @time 16/7/1 下午5:53
 * @des 足球舞台
 */
public class StageView extends SurfaceView implements Callback {

    //当前被移动球员
//    private StagePlayer mCurrentPlayer;

    //球场上球员管理
    private PlayerManager mPlayerManager;

    //球场绘制线程
    private StageCanvasThread mStageCanvasThread;

    private static final String TAG = "dragPlayer";

    //球员变化回调句柄
    private OnChangeCallbackListener mOnChangeCallbackListener;

    private RectF mVisibleRect;

    private StagePlayer readyMovePlayer;
    private float DownX;
    private float DownY;

    public StageView(Context context) {
        super(context);

    }

    /**
     * 设置球员变化回调句柄
     *
     * @param pOnChangeCallbackListener
     */
    public void setOnRemoveCallbackListener(OnChangeCallbackListener pOnChangeCallbackListener) {
        this.mOnChangeCallbackListener = pOnChangeCallbackListener;
    }

    public StageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //初始化球员管理类,初始化双缓冲SurfaceView,初始化绘制线程
        mPlayerManager = new PlayerManager(getResources());
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        setZOrderOnTop(true);
        getHolder().setFormat(PixelFormat.TRANSLUCENT);
    }

    public StageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        int specWidthMode = MeasureSpec.getMode(widthMeasureSpec);
        int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);
        int specHeightMode = MeasureSpec.getMode(heightMeasureSpec);
        Log.d(TAG, "Stage width: " + specWidthSize);
        Log.d(TAG, "Stage height: " + specWidthSize);
        setMeasuredDimension(specWidthSize, specHeightSize);
        setVisibleRect(null);
        Log.d(TAG, "onMeasure");
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                Log.e(TAG, "Stage dispatchTouchEvent ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.e(TAG, "Stage dispatchTouchEvent ACTION_MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.e(TAG, "Stage dispatchTouchEvent ACTION_UP");
                break;

            default:
                break;
        }

        return super.dispatchTouchEvent(event);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (!SharePreHelper.getIns().getStageTouchable()){
                    return true;
                }

                if (mVisibleRect.contains(x, y)) {
                    readyMovePlayer = mPlayerManager.containsPlayer(x, y);
                }

                Log.e(TAG, "Stage onTouchEvent ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                if (readyMovePlayer == null) {
                    return super.onTouchEvent(event);
                }
                Log.e(TAG, "Stage onTouchEvent ACTION_MOVE");
//                if (!mVisibleRect.contains(x, y)){
//                    return false;
//                }
                if (!isMoveable(readyMovePlayer, x, y)) {
                    return true;
                }
                handleTouchMove(readyMovePlayer, x, y);
                break;
            case MotionEvent.ACTION_UP:
                if (readyMovePlayer == null) {
                    return super.onTouchEvent(event);
                }
                Log.e(TAG, "Stage onTouchEvent ACTION_UP");
                handleTouchUp(readyMovePlayer, x, y);
                readyMovePlayer = null;
                break;

            default:
                break;
        }
        //如果当前没有移动球员则不用拦截事件,若正在移动球员则应该拦截事件用于移动
        if (readyMovePlayer == null) {
            return super.onTouchEvent(event);
        }
        return true;
    }

    /**
     * 用于一定球员时改变球员坐标
     *
     * @param player
     * @param x
     * @param y
     */
    private void handleTouchMove(StagePlayer player, float x, float y) {
        if (player == null) {
            return;
        }
        if (x < 0){
            x = player.getWidth()/2;
        }
        if (y< 0){
            y = player.getHeight()/2;
        }
        if (y>mVisibleRect.height()){
            y = mVisibleRect.height() - player.getHeight()/2;
        }
        player.setCoordinateX(x);
        player.setCoordinateY(y);
    }


    /**
     * 当手势抬起,判断球员是否移动出界或是否覆盖在某球员,则操作删除球员
     *
     * @param player 当前球员
     * @param x
     * @param y
     */
    private void handleTouchUp(StagePlayer player, float x, float y) {
        if (player == null) {
            return;
        }
        if (player.getCoordinateX() < 0 || player.getCoordinateY() <0 || player.getCoordinateY()>mVisibleRect.height()||(App.getInst().getScreenWidth()-player.getCoordinateX()<player.getWidth()/2)){
            readyMovePlayer = null;
            return;
        }

        if (isBorder(x)) {
            if (player.isStateIn()) {
                removePlayer(player);
            } else {
                readyMovePlayer.reset();
            }
        } else {
//            player.setStateIn(true);
                StagePlayer other = mPlayerManager.containsPlayer(player, x, y);
                if (other != null) {
                    removePlayer(other);
//                    addPlayer(readyMovePlayer);
                }

                if (!player.isStateIn()){
                    addPlayer(readyMovePlayer);
                }


        }
    }

    private void addPlayer(StagePlayer player) {
        player.setStateIn(true);
        mPlayerManager.addPlayers(player);
        if (mOnChangeCallbackListener != null) {
            mOnChangeCallbackListener.onChangeCallback(this, player, false);
        }
    }

    /**
     * 删除球员并回调通知监听者
     *
     * @param player
     */
    private void removePlayer(StagePlayer player) {
        player.setStateIn(false);
        mPlayerManager.removePlayers(player);
        player.reset();
        if (mOnChangeCallbackListener != null) {
            mOnChangeCallbackListener.onChangeCallback(this, player, true);
        }
    }

    /**
     * 判断是否移动出界
     *
     * @param x
     * @return
     */
    private boolean isBorder(float x) {
           if (SharePreHelper.getIns().getIsChangeTeam()){

               return x > mVisibleRect.width();
           }else {
               return x < ScreenUtils.getScreenW(getContext())-mVisibleRect.width();
           }



    }

    /**
     * 判断是否可以移动
     *
     * @param player
     * @param x
     * @param y
     * @return
     */
    private boolean isMoveable(StagePlayer player, float x, float y) {
        boolean moveable = true;

        if (player.isStateIn()) {
            moveable = !((x - player.getWidth() / 2) < 0 || (y - player.getHeight() / 2 < 0) || (y + player.getHeight() / 2) > mVisibleRect.height());
        } else {
            moveable = !((x - player.getWidth() / 2) < 0 || (y - player.getHeight() / 2 < 0) || (y + player.getHeight() / 2) > getMeasuredHeight() || x + player.getWidth() / 2 > getMeasuredWidth());
        }

        return moveable;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");
        mStageCanvasThread = new StageCanvasThread(this, holder);
        mStageCanvasThread.startDraw();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed");
        mStageCanvasThread.pauseDraw();

    }

    public void onDestory(){
        mStageCanvasThread.pauseDraw();
    }


    /**
     * 绘制球场
     *
     * @param c
     */
    public void drawPlayers(Canvas c) {
        c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        //绘制球员
        mPlayerManager.onDraw(c);
        drawReadPlayer(c);
        //...若球场还有其他可需要绘制
    }

    /**
     * 绘制移动球员
     *
     * @param c
     */
    public void drawReadPlayer(Canvas c) {
        if (readyMovePlayer == null) {
            return;
        }
        if (!mVisibleRect.contains(readyMovePlayer.getCoordinateX(), readyMovePlayer.getCoordinateY())){
            return;
        }


        float textSize = getResources().getDisplayMetrics().density * 24;
        readyMovePlayer.onDraw(c, textSize);
    }

    /**
     * 绘制网格,用于测试使用
     *
     * @param c
     */
    public void drawGrid(Canvas c) {
        float w = (mVisibleRect.width()*2) / PlayerManager.GRID_X;
        float H = mVisibleRect.height() / PlayerManager.GRID_Y;
        Paint paint = new Paint();
        paint.setStrokeWidth(2);
        paint.setColor(Color.YELLOW);
        for (int i = 0; i < PlayerManager.GRID_X + 1; i++) {
            c.drawLine(0, H * i, mVisibleRect.width()*2, H * i, paint);
        }
        for (int i = 0; i < PlayerManager.GRID_Y + 1; i++) {
            c.drawLine(w * i, 0, w * i, mVisibleRect.height(), paint);
        }
    }

    /**
     * 计算所有球员真实二维坐标系,以(0,0)开始
     * 不要频繁调用此方法,只有待你确实需要球员二维坐标系坐标时调用,不然影响效率
     */
    public void calculateCoordinate() {
        mPlayerManager.calculateCoordinate((int) mVisibleRect.width(), (int) mVisibleRect.height());
    }

    /**
     * 获得球场管理
     *
     * @return
     */
    public PlayerManager getPlayerManager() {
        return mPlayerManager;
    }

    /**
     * 当球员被变化时回调接口
     */
    public interface OnChangeCallbackListener {
        /**
         * 当被从球场变化时回调此方法
         *
         * @param pStageView 球场
         * @param pPlayers   被变化球员
         */
        public void onChangeCallback(StageView pStageView, StagePlayer pPlayers, boolean moveOut);
    }

    public void setVisibleRect(RectF pRect) {
        float width = getMeasuredWidth();
        if (pRect == null) {

            if (SharePreHelper.getIns().getIsChangeTeam()){
                pRect = new RectF(0, 0, App.getInst().getScreenWidth() *2/5, getMeasuredHeight());
            }else {
                pRect = new RectF(getMeasuredWidth() *3/5, 0, getMeasuredWidth(), getMeasuredHeight());
            }

        }
        mVisibleRect = pRect;
        if (mOnViewInitListener != null) {
            mOnViewInitListener.onInited();
        }
    }

    public void setReadyMovePlayer(StagePlayer pReadyMovePlayer) {
        this.readyMovePlayer = pReadyMovePlayer;
    }


    /**
     * 初始化球场球员
     *
     * @param pList
     */
    public void initPlayers(List<StagePlayer> pList) {
        if (pList == null) {
            return;
        }

        for (StagePlayer player : pList) {
            player.convertCoordinate(mVisibleRect.width(), mVisibleRect.height());
        }
        mPlayerManager.initPlayers(pList);
    }

    private OnViewInitListener mOnViewInitListener;

    /**
     * 设置初始化回调
     *
     * @param pOnViewInitListener
     */
    public void setOnViewInitListener(OnViewInitListener pOnViewInitListener) {
        this.mOnViewInitListener = pOnViewInitListener;
    }


    /**
     * 初始化回调接口
     */
    public interface OnViewInitListener {
        public void onInited();
    }
}
