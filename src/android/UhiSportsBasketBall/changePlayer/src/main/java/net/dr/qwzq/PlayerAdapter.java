package net.dr.qwzq;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther Kalen
 * @time 16/7/1 下午7:15
 * @des 球员备战区数据适配器
 */
public class PlayerAdapter extends BaseAdapter {

    //用来存放选中的对象
    private List<StagePlayer> players;
    //供adapter获取选中数据的方法
    private Context context;

    public PlayerAdapter(Context context) {
        this.context = context;
        this.players = new ArrayList<StagePlayer>();
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.start_players_item, parent, false);
            holder = new ViewHolder();
            holder.choose = (ImageView) convertView.findViewById(R.id.iv_choose);
            holder.number = (TextView) convertView.findViewById(R.id.number);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.iv_leader = (ImageView) convertView.findViewById(R.id.player_header);
            holder.itelRL = (LinearLayout) convertView.findViewById(R.id.lt);
            holder.player_header = (ImageView) convertView.findViewById(R.id.player_header);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.init((StagePlayer) getItem(position));
        return convertView;
    }

    public void remove(StagePlayer pPlayers) {
        players.remove(pPlayers);
        notifyDataSetChanged();
    }

    public void add(StagePlayer pPlayers) {
        players.add(pPlayers);
        notifyDataSetChanged();
    }


    public class ViewHolder {
        public ImageView choose;
        public LinearLayout itelRL;
        public TextView number;
        public ImageView player_header;
        public ImageView iv_leader;
        public TextView name;

        public void init(StagePlayer stagePlayer) {
            name.setText(stagePlayer.getFullName());
            number.setText(stagePlayer.getNumber() + "");
//            ImageLoader.loadCicleImage(context, stagePlayer.getPicture(), R.mipmap.default_person, player_header);
        }
    }

    /**
     * 刷新球员列表
     * @param pList
     */
    public void refresh(List<StagePlayer> pList){
        if (pList == null){
            return;
        }
        players.clear();
        players.addAll(pList);
        notifyDataSetChanged();
    }
}