package net.dr.qwzq;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * @auther Kalen
 * @time 16/7/2 下午12:14
 * @des 球场绘制线程
 */
public class StageCanvasThread extends Thread {
    //双缓冲句柄
    private SurfaceHolder surfaceHolder;
    //球场
    private StageView mStageView;
    //是否需要继续绘制
    private boolean isRunning;

    /**
     * 绘制间隔时间,时间间隔会影响绘制效率
     */
    public static final long delay = 50;

    public StageCanvasThread(StageView pStageView) {
        this.mStageView = pStageView;
    }

    public StageCanvasThread(StageView pStageView, SurfaceHolder surfaceHolder) {
        this(pStageView);
        this.surfaceHolder = surfaceHolder;
    }

    @Override
    public void run() {
        Canvas c = null;
        while (isRunning) {
            try {
                synchronized (surfaceHolder) {
                    c = surfaceHolder.lockCanvas(null);
                    doDraw(c);
                    Thread.sleep(delay);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                surfaceHolder.unlockCanvasAndPost(c);
            }
        }

    }

    /**
     * 结束绘制
     */
    public void pauseDraw() {
        isRunning = false;
    }

    /**
     * 准备开始绘制
     */
    public void startDraw() {
        isRunning = true;
        start();
    }

    /**
     * 允许绘制,开始绘制
     *
     * @param c
     */
    public void doDraw(Canvas c) {
        mStageView.drawPlayers(c);
//        mStageView.drawGrid(c);
    }

}
