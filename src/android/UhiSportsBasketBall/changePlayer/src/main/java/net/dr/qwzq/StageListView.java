package net.dr.qwzq;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * @auther Kalen
 * @time 16/7/1 下午9:25
 * @des ${TODO}
 */
public class StageListView extends ListView {
    private int mCurrentPosition = 0;
    private static final String TAG = StageListView.class.getSimpleName();

    public StageListView(Context context) {
        super(context);
    }

    public StageListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StageListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StageListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "ListView dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d(TAG, "ListView onInterceptTouchEvent");
        return false;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                handleTouchDown(pointToPosition((int) x, (int) y));
                Log.d(TAG, " position : " + mCurrentPosition);
                Log.e(TAG, "ListView onTouchEvent ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                handleTouchMove(mCurrentPosition);
                Log.e(TAG, "ListView onTouchEvent ACTION_MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.e(TAG, "ListView onTouchEvent ACTION_UP");
                handleTouchUp(mCurrentPosition);
                mCurrentPosition = -1;
                break;

            default:
                break;
        }
        return false;
    }

    private void handleTouchDown(int position) {
        mCurrentPosition = position;
        if (mOnSelectCallbackListener != null) {
            StagePlayer player = (StagePlayer) getItemAtPosition(position);
            mOnSelectCallbackListener.onSelectCallback(player);
        }
    }

    private void handleTouchUp(int position) {

    }

    private void handleTouchMove(int position) {

    }

    private OnSelectCallbackListener mOnSelectCallbackListener;

    public void setOnSelectCallbackListener(OnSelectCallbackListener pOnSelectCallbackListener) {
        this.mOnSelectCallbackListener = pOnSelectCallbackListener;
    }


    public interface OnSelectCallbackListener {
        public void onSelectCallback(StagePlayer player);

    }
}
