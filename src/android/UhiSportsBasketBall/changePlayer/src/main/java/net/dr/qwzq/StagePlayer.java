package net.dr.qwzq;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;

/**
 * @auther Kalen
 * @time 16/7/1 下午6:28
 * @des 球员
 */
public class StagePlayer {

    //球场X坐标
    private float coordinateX;
    //球场Y坐标
    private float coordinateY;
    //二维坐标系X坐标
    private int locationX;
    //二维坐标系Y坐标
    private int locationY;
    //球员号码
    private int number;
    //球员头像
    private String picture;
    //球员名称
    private String name;
    //球员真实名称
    private String realName;
    //球员衣服
    private Bitmap mBitmap;

    private boolean isStateIn;




    public StagePlayer(int number, Bitmap pBitmap) {
        this.number = number;
        this.mBitmap = pBitmap;
        reset();
    }

    public void reset(){
        coordinateX = coordinateY = -getWidth();
        locationX = locationY = -getHeight();
    }

    public boolean isStateIn() {
        return isStateIn;
    }

    public void setStateIn(boolean pStateIn) {
        isStateIn = pStateIn;
    }

    public float getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(float pCoordinateX) {
        coordinateX = pCoordinateX;
    }

    public float getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(float pCoordinateY) {
        coordinateY = pCoordinateY;
    }

    public int getLocationX() {
        return locationX;
    }

    public void setLocationX(int pLocationX) {
        locationX = pLocationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public void setLocationY(int pLocationY) {
        locationY = pLocationY;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String pPicture) {
        picture = pPicture;
    }

    public String getName() {
        return name;
    }

    public void setName(String pName) {
        name = pName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String pRealName) {
        realName = pRealName;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap pBitmap) {
        mBitmap = pBitmap;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int pNumber) {
        number = pNumber;
    }

    public String getFullName() {
        if (TextUtils.isEmpty(realName)) {
            return name;
        }
        return realName;
    }


    public int getWidth() {
        return mBitmap.getWidth();
    }

    public int getHeight() {
        return mBitmap.getHeight();
    }

    public RectF getRect() {
        RectF rect = new RectF(getCoordinateX() - getWidth() / 2, getCoordinateY() - getHeight() / 2, getCoordinateX() + getWidth() / 2, getCoordinateY() + getHeight() / 2);
        return rect;
    }

    /**
     * 判断坐标是否是自己
     * @param x
     * @param y
     * @return
     */
    public boolean contains(float x, float y) {
        RectF rectF = getRect();
        if (rectF.contains(x, y)) {
            return true;
        }

        return false;
    }

    /**
     * 球员绘制
     * @param canvas
     * @param size
     */
    public void onDraw(Canvas canvas, float size) {
        Paint paint = new Paint();
        paint.setTextSize(size);
        paint.setStrokeWidth(4);
        paint.setColor(Color.WHITE);
        Rect bound = new Rect();
        paint.getTextBounds(Integer.toString(getNumber()), 0, 1, bound);
        canvas.drawBitmap(mBitmap, getCoordinateX() - mBitmap.getWidth() / 2, getCoordinateY() - mBitmap.getHeight() / 2, null);
        canvas.drawText("" + getNumber(), getCoordinateX() - (bound.left + bound.right) / 2, getCoordinateY() - (bound.top + bound.bottom) / 2, paint);
    }

    /**
     * 根据球场坐标转换二维坐标系
     * @param width
     * @param height
     */
    public void calculateCoordinate(int width, int height) {
        float r_w = width / PlayerManager.GRID_X;
        int r = (int) (getCoordinateX() / r_w);
        if (getCoordinateX() % r_w > 0) {
            r += 1;
        }
        setLocationX(r - 1);

        float c_h = height / PlayerManager.GRID_Y;
        int c = (int) (getCoordinateY() / c_h);
        if (getCoordinateY() % c_h > 0) {
            c += 1;
        }
        setLocationY(c - 1);
    }
}
