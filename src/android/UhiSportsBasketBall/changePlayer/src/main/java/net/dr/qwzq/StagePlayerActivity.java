package net.dr.qwzq;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * 球场界面
 */
public class StagePlayerActivity extends AppCompatActivity implements View.OnClickListener, StageListView.OnSelectCallbackListener, StageView.OnChangeCallbackListener {

    //球场
    private StageView mStageView;

    //备战区
    private StageListView list;

    //球员数据
    private PlayerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stage_player_activity);
        mStageView = (StageView) findViewById(R.id.stage);
        mStageView.setOnRemoveCallbackListener(this);
        list = (StageListView) findViewById(R.id.list);
        list.setOnSelectCallbackListener(this);
        findViewById(R.id.confirm).setOnClickListener(this);
        mAdapter = new PlayerAdapter(this);
        list.setAdapter(mAdapter);
        test();
    }

    private void test() {
        List<StagePlayer> players = new ArrayList<StagePlayer>();
        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.player_clothes);
        StagePlayer players1 = new StagePlayer(2, mBitmap);
        players1.setRealName("Kalen");
        StagePlayer players2 = new StagePlayer(3, mBitmap);
        players2.setRealName("Holen");
        players.add(players1);
        players.add(players2);
        initData(players);
    }


    @Override
    public void onClick(View v) {
        mStageView.calculateCoordinate();
        commitPlayerLocation(mStageView.getPlayerManager().getPlayerses());
    }


    @Override
    public void onSelectCallback(StagePlayer player) {
        mStageView.setReadyMovePlayer(player);
    }


    /**
     * 通过网络获得数据,需要重写
     *
     * @param players
     */
    protected void initData(List<StagePlayer> players) {
        mAdapter.refresh(players);
    }


    /**
     * 移动处理之后,提交数据到服务器,需要重写
     *
     * @param players
     */
    protected void commitPlayerLocation(List<StagePlayer> players) {
        for (StagePlayer player : players) {
            Log.d("Main", "location x : " + player.getLocationX() + " y : " + player.getLocationY());
        }
    }

    /**
     * 移动过程中球员变化
     *
     * @param pStageView 球场
     * @param pPlayers   被变化球员
     * @param moveOut
     */
    @Override
    public void onChangeCallback(StageView pStageView, StagePlayer pPlayers, boolean moveOut) {
        Toast.makeText(StagePlayerActivity.this, "player " + pPlayers.getNumber() + (moveOut ? " out" : " in"), Toast.LENGTH_SHORT).show();
        if (!moveOut) {
            mAdapter.remove(pPlayers);
        } else {
            mAdapter.add(pPlayers);
        }
    }
}
