//
//  AppDelegate.h
//  FootballApp
//
//  Created by MagicBeans2 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

