//
//  AppDelegate.m
//  FootballApp
//
//  Created by MagicBeans2 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AppDelegate.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "JPUSHService.h"
#import "ChatHelper.h"
#import "messageTuishong.h"
#import <AudioToolbox/AudioToolbox.h>

#import "DynamicDeatilController.h"
#import "JoinWarViewController.h"
#import "JoinViewController.h"

//分享
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>

#define ShareAppKey @"14d21ed941512"
#define WeboAppkey @"wxeba4cdc4b8f19d65" //老的可用
#define weboSecret @"wxeba4cdc4b8f19d65" //老的可用

#define WXAppID @"wx35ae0d64b9945b87"
#define WXAppSecret @"3fb15027cf3759ecbf8ad94db481ac56"

#define QQAppKey @"1105611036"
#define QQAppSecret @"cDxI9hws694WVJO0"



#import "ShareView.h"


@interface AppDelegate ()
@property(nonatomic,strong)NSMutableArray *array0;
@property(nonatomic,strong)NSMutableArray *array1;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.array0 = [NSMutableArray arrayWithCapacity:0];
    self.array1 = [NSMutableArray arrayWithCapacity:0];
    //可以添加自定义categories
    
    //可以添加自定义categories
    [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                      UIUserNotificationTypeSound |
                                                      UIUserNotificationTypeAlert)
                                          categories:nil];
    
    //如不需要使用IDFA，advertisingIdentifier 可为nil
    [JPUSHService setupWithOption:launchOptions appKey:JP_KEY channel:@"App Store" apsForProduction:[AccountHelper shareAccount ].isPublic];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNOti:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getPushInfo) name:@"pushnewmessage" object:nil];
    
    [[AccountHelper shareAccount] autoLogin];//自动登录
    
    
    //     [AMapLocationServices sharedServices].apiKey =MAP_KEY;//地图定位
    [AMapServices sharedServices].apiKey =MAP_KEY;//地图搜索
    
    [[AccountHelper shareAccount] autoLocation];//自动定位
    
    
    [ChatHelper initEMCha];//初始化换新
    
    [[ChatHelper chatService] resginChatDelegate];
    
    
    /**社会化分享*/
    [ShareSDK registerApp:ShareAppKey];
    
    //1.添加微信应用  http://open.weixin.qq.com/
    [ShareSDK connectWeChatWithAppId:WXAppID
                           appSecret:WXAppSecret
                           wechatCls:[WXApi class]];
//
//    //2.微信朋友圈    http://open.weixin.qq.com/
    [ShareSDK connectWeChatTimelineWithAppId:WXAppID appSecret:WXAppSecret wechatCls:[WXApi class]];
    
    //3.QQ   http://connect.qq.com/
//    [ShareSDK connectQQWithAppId:QQAppID qqApiCls:[QQApiInterface class]]; //这里扣扣好友能用
   // 3.新浪微博
    [ShareSDK connectSinaWeiboWithAppKey:WeboAppkey
                               appSecret:weboSecret
                             redirectUri:@"http://sns.whalecloud.com/sina2/callback"];
//    [ShareSDK connectSinaWeiboWithAppKey:@"271603247840288" //f7058f1b7c20
//                               appSecret:@"206d7a2bd222b6441f20421b6e4cc1b4"
//                             redirectUri:@"http://dashboard.mob.com/#/share/index"];
    
    //4.QQ空间
    [ShareSDK connectQZoneWithAppKey:QQAppKey
                           appSecret:QQAppSecret
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    
    
    return YES;
}


//下面两个为分享回调地址
- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    ShareView *shareView = [[ShareView alloc]init];
    [shareView dismiss];
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}



//将变为非活动状态
- (void)applicationWillResignActive:(UIApplication *)application {
    
    [[LoadingView shareView] stop];
    
    [SVProgressHUD dismiss];
}
//进入后台

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[EMClient sharedClient] applicationDidEnterBackground:application];
}
//将要进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    
    [[EMClient sharedClient] applicationWillEnterForeground:application];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"appBecomeActive" object:nil];
    
    
}
//活动状态
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}
//程序完全退出
- (void)applicationWillTerminate:(UIApplication *)application {
    
    
    
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    
    [JPUSHService registerDeviceToken:deviceToken];
    
    [AccountHelper PushRegistration];
    
    [[EMClient sharedClient] bindDeviceToken:deviceToken];
    

    
    
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [JPUSHService handleRemoteNotification:userInfo];
  
    
}
- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
   
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    [JPUSHService handleRemoteNotification:userInfo];

    //这里设置app的图片的角标为0，红色但角标就会消失
    [UIApplication sharedApplication].applicationIconBadgeNumber  =  0;
    completionHandler(UIBackgroundFetchResultNewData);
    
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {  //此时app在前台运行，我的做法是弹出一个alert，告诉用户有一条推送，用户可以选择查看或者忽略
        //声音和震动
        AudioServicesPlaySystemSound(1007);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

        

    }
    else
    {
        //这里是app未运行或者在后台，通过点击手机通知栏的推送消息打开app时可以在这里进行处理，比如，拿到推送里的内容或者附加      字段(假设，推送里附加了一个url为 www.baidu.com)，那么你就可以拿到这个url，然后进行跳转到相应店web页，当然，不一定必须是web页，也可以是你app里的任意一个controll，跳转的话用navigation或者模态视图都可以
      
        if ([userInfo[@"pushType"] intValue] == 0)
        {
            NSString *dynamicId = userInfo[@"dynamicId"];
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
           DynamicDeatilController *vcc = [storyboard instantiateViewControllerWithIdentifier:@"DynamicDeatilController"];
             vcc.dynamicId = dynamicId;
             vcc.dynamicIdSymbol = @"动态推送跳转";
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vcc];
           
            self.window.rootViewController = nc;
            
            
        }
        else if ([userInfo[@"pushType"] intValue] == 1)
        {

            NSString *matchID = userInfo[@"matchId"];
             NSString *matchStatus = userInfo[@"matchStatus"];
            [self gotosomethings:matchStatus And:matchID];
        }
        else
        {
            
        }
        
    }
    
  
    
    
}
//收到自定义消息
-(void)receiveNOti:(NSNotification *)noti{
    
    //声音和震动
//    AudioServicesPlaySystemSound(1007);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    NSDictionary *dicc = [NSDictionary dictionary];
    dicc = noti.userInfo;

    if([dicc[@"extras"][@"pushType"] intValue]==3){
    
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"matchChange" object:nil];
        
    
    }else{
    
    NSString *strkey = [NSString stringWithFormat:@"%d",MINEId];

  
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *array=[NSMutableArray array];
    [array addObject:dicc];
    [array addObjectsFromArray:[defaults objectForKey:strkey]];
    [defaults setObject:array forKey:strkey];
    [defaults synchronize];
  
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnewmessage" object:nil];
        
    }
}

//收到推送消息就解析
- (void)getPushInfo
{
    self.array0  = [NSMutableArray arrayWithCapacity:0];
    self.array1  = [NSMutableArray arrayWithCapacity:0];
   
    NSString *strkey = [NSString stringWithFormat:@"%d",MINEId];
    self.array0 = [Defaults objectForKey:strkey];
   
    for (int i = 0; i < self.array0.count; i++)
    {
        messageTuishong *arInfo = [[messageTuishong alloc]init];
        NSDictionary *dic = [NSDictionary dictionary];
        dic = self.array0[i];
        
        arInfo.senderHeader     = dic[@"extras"][@"senderHeader"];
        arInfo.pushType         = dic[@"extras"][@"pushType"];
        arInfo.content          = dic[@"extras"][@"content"];
        arInfo.dynamicId        = dic[@"extras"][@"dynamicId"];
        arInfo.senderName       = dic[@"extras"][@"senderName"];
        arInfo.sendTime         = dic[@"extras"][@"sendTime"];
        arInfo.matchId          = dic[@"extras"][@"matchId"];
        arInfo.contentinfo      = dic[@"content"];
        [self.array1 addObject:arInfo];
    }
    [AccountHelper shareAccount].tuisongArray = [NSMutableArray arrayWithCapacity:0];
    [AccountHelper shareAccount].tuisongArray = self.array1;
 
}

//约赛推送中跳转
- (void)gotosomethings:(NSString *)status And:(NSString *)matchId
{
    //約赛中
    if ([status intValue]==0) {
        
    
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        JoinWarViewController *vcc = [storyboard instantiateViewControllerWithIdentifier:@"JoinWarViewController"];
        vcc.navigationItem.title = @"约赛中";
        vcc.matchId = matchId;
        vcc.matchpushInfo = @"约赛推送跳转";
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vcc];
        self.window.rootViewController = nc;
        
        
    }
    //以匹配（赛前）
    if ([status intValue]==10) {
        
     
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        JoinWarViewController *vcc = [storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        vcc.navigationItem.title = @"赛前";
        vcc.matchId = matchId;
        vcc.matchpushInfo = @"约赛推送跳转";
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vcc];
        self.window.rootViewController = nc;
        
    }
    //比赛中
    if ([status intValue]==20) {
      
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        JoinWarViewController *vcc = [storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        vcc.navigationItem.title = @"赛中";
        vcc.matchId = matchId;
        vcc.matchpushInfo = @"约赛推送跳转";
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vcc];
        self.window.rootViewController = nc;
        
    }
   
    
    
}

@end
