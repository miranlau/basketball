//
//  CircleViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CircleViewController.h"
#import "FriendModle.h"
#import "DynamicDeatilController.h"
#import <UITableView+FDTemplateLayoutCell.h>
#import "CupMatchCell.h"

#import "AppDelegate.h"

#import "TeamHelper.h"

@interface CircleViewController ()
{

    UIButton *leftImageView;
    __weak IBOutlet UIView *headView;
    
    __weak IBOutlet UITableView *circleTableView;
    UIButton *lastBtn;
    
    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet UIButton *firstBtn;
    __weak IBOutlet UIImageView *line;
    
     int pageNumber;
    
    
}
@property(nonatomic,strong)NSMutableArray *array0;
@property(nonatomic,strong)NSMutableArray *array1;

@end

@implementation CircleViewController

//获取全部动态列表(分页)
//dynamic/getAllDynamicPager
//参数	名称	可选值	备注
//pageNumber	页码，初始化1

- (void)reloadtheViewdata
{
    [self viewDidLoad];
     [circleTableView.mj_header beginRefreshing];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [circleTableView reloadData]; //强制重载，来自适应行高
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"圈子";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadtheViewdata) name:@"reloadtheViewdata" object:nil];
    
    
    [self loadNavBarItmes];
    
    [self loadHeadView];
     [self loadTableView];

    [self headBtnClick:firstBtn];
    

}


-(void)loadNavBarItmes{
    
    leftImageView=[[UIButton alloc]initWithFrame:CGRectMake(-10, 0, 39, 39)];
    leftImageView.layer.cornerRadius=19.5;
    leftImageView.layer.masksToBounds=YES;
    leftImageView.userInteractionEnabled=YES;
    [leftImageView addTarget:self action:@selector(headImageClick:) forControlEvents:UIControlEventTouchUpInside];
     [self headImageChange];
    
    
    //去除边线
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftImageView];
    
    
    UIBarButtonItem *right=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"发动态.png"] style:UIBarButtonItemStylePlain target:self action:@selector(sendNew:)];
    
    
    self.navigationItem.rightBarButtonItem=right;
    
    //右侧添加按钮
  
}
-(void)loadHeadView{
    
    headView.backgroundColor=BarColor;
    
}

-(void)headImageChange{
    
    if([AccountHelper shareAccount].isLogin ==YES){
        
        
        
        [leftImageView setBackgroundImageForState:0 withURL:[NSURL URLWithString:[AccountHelper shareAccount].player.avatar] placeholderImage:HeadImage];
        [leftImageView setTitle:@"" forState:0];
    }else{
        [leftImageView setBackgroundImage:HeadImage forState:0];
        [leftImageView setTitle:@"未登录" forState:0];
        [leftImageView setTitleColor:[UIColor grayColor] forState:0];
         leftImageView.titleLabel.font=[UIFont systemFontOfSize:12];
        
    }
    
}


-(void)loadTableView{
    
    circleTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    circleTableView.rowHeight=UITableViewAutomaticDimension;
    circleTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]init];
    bgImageView.image=BGIMG;
    circleTableView.backgroundView=bgImageView;
    

    
  
   
    circleTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 进入刷新状态后会自动调用这个block
          pageNumber=1;
        [self  loadNetWorkDatas:lastBtn.tag];
    
        
    }];
    
//    [circleTableView.mj_header beginRefreshing];
    
    circleTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        pageNumber++;
        
        [self  loadNetWorkDatas:lastBtn.tag];
        
    }];
    
}
-(IBAction)headBtnClick:(UIButton *)butn{
    
    //防止点击多次
    if (butn==lastBtn) {
        return;
    }
    
    lastBtn.selected=NO;
    butn.selected=!butn.selected;
    
    lastBtn=butn;
    
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/2*(butn.tag-1001);
        [line layoutIfNeeded];
        
        
    }];
    pageNumber = 1;
  [circleTableView.mj_header beginRefreshing]; //注意这个玩意放的位置应该在block后面之类的
}
#pragma mark 头像按钮点击事件
-(void)headImageClick:(id)sender{
    
     [self headImageViewClick];
    
    
}
-(void)sendNew:(id)sender{
    
    if ([AccountHelper shareAccount].isLogin ==YES) {
        
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SendNewViewController"] animated:YES];
        
    }else{
    
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }

    
    

}


-(void)loadNetWorkDatas:(NSInteger )tag{
    
    if (pageNumber==1) {
        
        [self.dataArr removeAllObjects];
        
        [circleTableView reloadData];
    }
    
    [AccountHelper shareAccount].isShow=YES;
    
    if (tag==1001) {
      
        [HttpRequestHelper postWithURL:Url(@"dynamic/getAllDynamicPager") params:@{@"pageNumber":@(pageNumber)} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
  
                for (NSDictionary *dic  in responseObject[@"content"]) {
                    
                    FriendModle *model=[[FriendModle alloc]initWithDic:dic];
                    
                    BaseCellModel *base=nil;
         
                   base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"CircleCell"];
                    [self.dataArr addObject:base];
    
                }
                
                [circleTableView reloadData];
                [circleTableView.mj_header endRefreshing];
                [circleTableView.mj_footer endRefreshing];
                
                
            }else{


                
                 [circleTableView reloadData];
                   [circleTableView.mj_header endRefreshing];
                 [circleTableView.mj_footer endRefreshing];

                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
            
        } failure:^(NSError *error) {
             [circleTableView reloadData];
            [circleTableView.mj_header endRefreshing];
            [circleTableView.mj_footer endRefreshing];
            
        }];
  
    }
    
    
    if (tag==1002) {
        
        
        
//        NSLog(@"mun = %d",pageNumber);
        [HttpRequestHelper postWithURL:Url(@"dynamic/getFriendDynamicPager") params:@{@"pageNumber":@(pageNumber)} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                for (NSDictionary *dic  in responseObject[@"content"]) {
                    
                    FriendModle *model=[[FriendModle alloc]initWithDic:dic];
                    
                    BaseCellModel *base=nil;
                    
                    base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"CircleCell"];
                    [self.dataArr addObject:base];
                    
                }
                [circleTableView reloadData];
                
                [circleTableView.mj_header endRefreshing];
                [circleTableView.mj_footer endRefreshing];
                
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
                 [self.dataArr removeAllObjects];
                [circleTableView reloadData];
                [circleTableView.mj_header endRefreshing];
                [circleTableView.mj_footer endRefreshing];

            }
            
            
        } failure:^(NSError *error) {
            
            [circleTableView.mj_header endRefreshing];
            [circleTableView.mj_footer endRefreshing];
            
        }];
        
    }
}
//分区数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//每个分区的行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.dataArr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
      BaseCellModel *model = self.dataArr[indexPath.row];
    
    return [tableView fd_heightForCellWithIdentifier:model.reuseIdentifier cacheByIndexPath:indexPath configuration:^(UITableViewCell *cell) {
        
        
        cell.fd_enforceFrameLayout = NO;
        
          [cell setValue:model forKey:@"data"];
        
       
    }];


}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BaseCellModel *model = self.dataArr[indexPath.row];
 
    CupMatchCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier];

    cell.headButton.tag = indexPath.row;
    [cell.headButton addTarget:self action:@selector(headImageClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.fd_enforceFrameLayout = NO;
    [cell setValue:model forKey:@"data"];
    
    if (indexPath.row%2) {

        
        cell.contentView.backgroundColor=BGCOLOR1;
       

    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }

    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BaseCellModel *model = self.dataArr[indexPath.row];
    FriendModle *fridenmo = model.value;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DynamicDeatilController* vc = [storyboard instantiateViewControllerWithIdentifier:@"DynamicDeatilController"];
    vc.dynamicId = [NSString stringWithFormat:@"%@",fridenmo.id];
  
    if([AccountHelper shareAccount].isLogin==YES)
    {
        if([fridenmo.userId intValue] == MINEId)
            {
                  
                vc.MinePublishSymbol = @"这里进去可以删除";
                        
            }
        else
            {
                vc.MinePublishSymbol = @"这里进去不能删除";
            }
    }
    else
    {
         vc.MinePublishSymbol = @"这里进去不能删除";
    }
    [self.navigationController pushViewController:vc animated:YES];
}

//点击头像跳转，我在头像UIImageView 上面放了一个UIbutton
- (IBAction)headImageClicked:(UIButton *)sender {
    
    NSInteger number = (NSInteger)sender.tag;
    
    BaseCellModel *model = self.dataArr[number];
    FriendModle *fridenmo = model.value;
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
    if([fridenmo.user[@"id"] intValue] == MINEId)
    {
         controller.navigationItem.title= @"我的";
         [controller setValue:fridenmo.user[@"id"] forKey:@"userId"];
    }
    else
    {
        controller.navigationItem.title= fridenmo.user[@"name"];
        [controller setValue:fridenmo.user[@"id"] forKey:@"userId"];
    }
    [self.navigationController pushViewController:controller animated:YES];

    
}


@end
