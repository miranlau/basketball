//
//  DynamicDeatilController.m
//  FootballApp
//
//  Created by zj on 16/6/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "DynamicDeatilController.h"
#import "CupMatchCell1.h"
#import "CupCommentCell.h"
#import "FriendModle.h"
#import "BaseCell.h"
#import "CupMatchCell.h"
#import "JPUSHService.h"
#import "TabBarViewController.h"
#import "messageTuishong.h"
#import "UITableView+FDTemplateLayoutCell.h"

@interface DynamicDeatilController ()
{
    NSMutableArray *commentArray;
    NSMutableArray *arrayss;
    BOOL isSucess;
    dispatch_once_t onceToken;
    NSMutableArray * tmpArray;
}
@property (weak, nonatomic) IBOutlet UITableView *dynamicViewTableView;
@property (weak, nonatomic) IBOutlet UITextField *mytext;
@property(nonatomic,strong) FriendModle *friend;
@property(nonatomic,strong) MyUserModle *userInfo;
@property(nonatomic,strong) MyCommentModle *commentInfo;

@property(nonatomic,copy)NSString *String; //评论内容
@property(nonatomic,copy)NSString *receivedID; //接收者ID
@property (nonatomic,assign)BOOL *ApperWhenData;


@end

@implementation DynamicDeatilController
- (void)reloadAllDatahaha
{
     [_dynamicViewTableView reloadData];
    [_dynamicViewTableView.mj_header beginRefreshing];

}
- (void)doClickBackAction
{

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabBarViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [Defaults setObject:@"动态推送跳转" forKey:@"dynamicDetailPush"];
    [Defaults synchronize];
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [_dynamicViewTableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    if([self.dynamicIdSymbol isEqualToString: @"动态推送跳转"])
    {
    
        UIImage* backImage = [UIImage imageNamed:@"back.png"];
        CGRect backframe = CGRectMake(0,0,54,30);
        UIButton* backButton= [[UIButton alloc] initWithFrame:backframe];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)]   ;
        [backButton setImage:backImage forState:UIControlStateNormal];
        
        [backButton addTarget:self action:@selector(doClickBackAction) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }
    if([self.MinePublishSymbol isEqual:@"这里进去可以删除"])
    {
         [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnewmessage" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeTuisongInfoAction) name:@"removetuisongInfoss" object:nil];
    }
    
    _ApperWhenData = false;
    commentArray = [[NSMutableArray alloc]initWithCapacity:0];
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadAllDatahaha) name:@"reloadAllDatahaha" object:nil];
    
    self.navigationItem.title = @"动态详情";
    self.mytext.placeholder = @"说点什么...";
    UIImageView *bgImageView=[[UIImageView alloc]init];
    bgImageView.image=BGIMG;
    _dynamicViewTableView.backgroundView=bgImageView;
   
    _dynamicViewTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    _dynamicViewTableView.rowHeight = UITableViewAutomaticDimension;
    _dynamicViewTableView.estimatedRowHeight = 125.0; // 设置为一个接近“平均”行高的值
    
    _dynamicViewTableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        
        [self loadData];
         [_dynamicViewTableView reloadData];
   }];
    
    
    [_dynamicViewTableView.mj_header beginRefreshing];
   

    
}
- (void)loadData
{
    [HttpRequestHelper postWithURL:Url(@"dynamic/dynamicDetail") params:@{@"dynamicId":_dynamicId} success:^(BOOL sucess, id responseObject) {
       
        if (sucess) {
            isSucess =YES;
            NSDictionary *dic = responseObject;
          
                FriendModle *model=[[FriendModle alloc]initWithDic:dic];
            self.friend  = model;
                self.userInfo = [[MyUserModle alloc]initWithDic:model.user];
            NSArray *array = model.commentDtos;
            [commentArray removeAllObjects];
        for (int i = 0; i < array.count; i++)
            {
//                self.commentInfo = [[MyCommentModle alloc]initWithDic:array[i]];
//                [commentArray addObject:self.commentInfo];
                
                MyCommentModle *model=[[MyCommentModle alloc]initWithDic:array[i]];
                
                BaseCellModel *base=nil;
                
                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"CircleCell2"];
                [commentArray addObject:base];
                
                
                
                
                
                
            }
    
            
        
            
            
            
            
            dispatch_barrier_async(dispatch_get_main_queue(), ^{
                
                [_dynamicViewTableView reloadData];
                _ApperWhenData = YES;
                [_dynamicViewTableView.mj_header endRefreshing];
            });
            
        }else{
            [_dynamicViewTableView.mj_header endRefreshing];
            
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
        
    } failure:^(NSError *error) {
        
        [_dynamicViewTableView.mj_header endRefreshing];
       
        
    }];
}
- (IBAction)CommentClicked:(id)sender {
    if ([self.mytext.text isEqual:@""])
    {
        [SVProgressHUD showInfoWithStatus:@"评论内容不能为空"];
    }
    else
    {
        [self loadComment];
    }
    
}
//点赞
- (IBAction)LaudClicked:(id)sender {
    
    if ([AccountHelper shareAccount].isLogin == NO) {
        [[[FBAlertView alloc ]initWithTitle:@"您还未登陆，是否去登陆？" butnTitlt:@"去登陆!" block:^(id data) {
            [AccountHelper shareAccount].isReg=NO;
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
            controller.navigationItem.title=@"登录";
            [self.navigationController pushViewController:controller animated:YES];
        }]show];
        return;
    }
    
    
    [HttpRequestHelper postWithURL:Url(@"dynamic/clickLaud") params:@{@"dynamicId":_dynamicId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
             [self viewDidLoad];
            [SVProgressHUD showInfoWithStatus:@"赞成功"];
            
        }else{
            
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

//发送评论
- (void)loadComment
{
    [_mytext resignFirstResponder];
   
    if ([AccountHelper shareAccount].isLogin == NO) {
    
        [[[FBAlertView alloc ]initWithTitle:@"您还未登陆，是否去登陆？" butnTitlt:@"去登陆!" block:^(id data) {
            [AccountHelper shareAccount].isReg=NO;
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
            controller.navigationItem.title=@"登录";
            [self.navigationController pushViewController:controller animated:YES];
        }]show];
        return;
    }
    
    
    
    NSDictionary *dic= [NSDictionary dictionary];
    if ( [self.mytext.placeholder isEqualToString: @"说点什么..."])
    {
        self.mytext.text =   self.mytext.text;
        if (_dynamicId != nil)
        {
            dic = @{@"dynamicid":_dynamicId,@"content":self.mytext.text};
        }
        else
        {
            
        }
    }
    else
    {
        self.mytext.text = [_String stringByAppendingString: self.mytext.text];
        if (_dynamicId != nil && _receivedID != nil)
        {
            dic = @{@"dynamicid":_dynamicId,@"content":self.mytext.text,@"recieverid":_receivedID};

        }
        else
        {
            
        }
        
    }
    
    [HttpRequestHelper postWithURL:Url(@"dynamic/commentDynamic") params:dic success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
           
             [SVProgressHUD showInfoWithStatus:@"评论成功"];
            self.mytext.text = @"";
        
            [self viewDidLoad];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadAllDatahaha" object:nil];
            
        }else{
        
            [SVProgressHUD showErrorWithStatus:responseObject];

        }
      
        
    } failure:^(NSError *error) {
        
    }];
}


-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section ==0 )
    {
        return 1;
    }
    else
    {
        return commentArray.count;
    }
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 7;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view=[[UIView alloc]init];
    view.backgroundColor=[UIColor clearColor];
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
  
    if (indexPath.section == 0)
    {
        // _ApperWhenData为真时表示取到了数据再显示。否则隐藏数据为空的cell
        if (_ApperWhenData)
        {
            CupMatchCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CircleCell" forIndexPath:indexPath];
        
            cell.headButton.tag = indexPath.row;
            [cell.headButton addTarget:self action:@selector(headImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            BaseCellModel *model=[[BaseCellModel alloc]init];
            model.value=self.friend;
            
            [cell setData:model];
            cell.contentView.backgroundColor=BGCOLOR1;
            for(UIView *view in cell.contentView.subviews)
            {
                view.hidden = NO;
            }
            
            if([self.MinePublishSymbol isEqual:@"这里进去不能删除"])
            {
                cell.delbutton.hidden = YES;
                cell.delbutton.enabled = NO;
                cell.delImage.hidden = YES;
            }
           else if([self.MinePublishSymbol isEqual:@"这里进去可以删除"])
            {
                cell.delbutton.hidden = NO;
                cell.delbutton.enabled = YES;
                cell.delImage.hidden = NO;
                cell.delbutton.layer.cornerRadius = 4;
                cell.delbutton.layer.masksToBounds = YES;
            }
            else
            {
                cell.delbutton.hidden = YES;
                cell.delbutton.enabled = NO;
                cell.delImage.hidden = YES;
            }
            
            return cell;
        }
        else
        {
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CircleCell" forIndexPath:indexPath];
             cell.contentView.backgroundColor=[UIColor clearColor];
            
            for(UIView *view in cell.contentView.subviews)
            {
                view.hidden = YES;
            }
       
            return cell;
        }

    }
    else
    {
    
    
        if(commentArray.count > 0)
        {
            BaseCellModel *model = commentArray[indexPath.row];
            
            CupCommentCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier];

            if (indexPath.row%2)
            {
                cell.contentView.backgroundColor=BGCOLOR1;
            }
            else
            {
                cell.contentView.backgroundColor=BGCOLOR2;
            }
         [cell setValue:model forKey:@"data"];
            
            return cell;
        }
        else
        {
            CupCommentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CircleCell2" forIndexPath:indexPath];
            
            
            cell.buttonImage.tag = indexPath.row;
            [cell.buttonImage addTarget:self action:@selector(CommentheadImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            if (indexPath.row%2)
            {
                cell.contentView.backgroundColor=BGCOLOR1;
            }
            else
            {
                cell.contentView.backgroundColor=BGCOLOR2;
            }
            
            return cell;
        }
    }
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section == 1)
    {

         BaseCellModel *models = commentArray[indexPath.row];
        MyCommentModle *model=models.value;
        MyUserModle *userinfo = [[MyUserModle alloc]initWithDic:model.sender];
        self.mytext.placeholder = [NSString stringWithFormat:@"回复 %@" ,userinfo.name];
        _String =  [NSString stringWithFormat:@"回复 %@:" ,userinfo.name];
        _receivedID = userinfo.id;
    }
    else
    {
          self.mytext.placeholder = @"说点什么...";
    }
    
    
    
    
}

-(void)viewDidLayoutSubviews{
 
    dispatch_once(&onceToken, ^{
        
        if (isSucess) {
            [_dynamicViewTableView reloadData];
        }
        
    });

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//删除我的动态


- (IBAction)DeleteMyPublic:(id)sender {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"提示"  message: @"确认删除该动态?"    preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        
    }]];
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    
        [HttpRequestHelper postWithURL:Url(@"dynamic/deleteDynamic") params:@{@"dynamicId":_dynamicId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                //           [SVProgressHUD showInfoWithStatus:@"删除信息成功"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadtheDeleveViewdata" object:nil];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadtheViewdata" object:nil];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"removetuisongInfoss" object:nil]; //动态删除后发出通知删除我的跟这条动态有关的推送
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                [_dynamicViewTableView.mj_header endRefreshing];
                
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
            
        } failure:^(NSError *error) {
            
            [_dynamicViewTableView.mj_header endRefreshing];
            
            
        }];

        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
  
}

//点击头像跳转，我在头像UIImageView 上面放了一个UIbutton
- (IBAction)headImageClicked:(UIButton *)sender {
    
    FriendModle *fridenmo = self.friend;
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
    if([fridenmo.user[@"id"] intValue] == MINEId)
    {
        controller.navigationItem.title= @"我的";
        [controller setValue:fridenmo.user[@"id"] forKey:@"userId"];
    }
    else
    {
        controller.navigationItem.title= fridenmo.user[@"name"];
        [controller setValue:fridenmo.user[@"id"] forKey:@"userId"];
    }
    [self.navigationController pushViewController:controller animated:YES];
    
}

//点击评论者 头像跳转，我在头像UIImageView 上面放了一个UIbutton
- (IBAction)CommentheadImageClicked:(UIButton *)sender {
    
    NSInteger number = (NSInteger)sender.tag;
    
    BaseCellModel *models = commentArray[number];
    MyCommentModle *model=models.value;
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
    controller.navigationItem.title= model.sender[@"name"];
    [controller setValue:model.sender[@"id"] forKey:@"userId"];
    [self.navigationController pushViewController:controller animated:YES];
}


//取出所有存在本地的通知，删除特定的后再重新保存
- (void)removeTuisongInfoAction
{
    
     NSString *strkey = [NSString stringWithFormat:@"%d",MINEId];
     arrayss = [[NSMutableArray alloc]initWithCapacity:0];
     arrayss  =[NSMutableArray arrayWithArray: [Defaults objectForKey:strkey]];
 
     tmpArray = [NSMutableArray arrayWithCapacity:0];
    
    //遍历删除
    [arrayss enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        if([obj[@"extras"][@"dynamicId"] isEqualToString:_dynamicId]){
 
            [tmpArray addObject:obj];
        }
    }];
    
    for (id obj in tmpArray) {
        [arrayss removeObject:obj];
    }
    [Defaults setObject:arrayss forKey:strkey];
    [Defaults synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnewmessage" object:nil];

}
@end
