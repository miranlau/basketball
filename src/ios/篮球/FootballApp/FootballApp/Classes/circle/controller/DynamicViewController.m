//
//  DynamicViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "DynamicViewController.h"

@interface DynamicViewController ()
{

    __weak IBOutlet UITableView *dynamicViewTableView;
    
    NSMutableDictionary *rowDic;
    

}
@end

@implementation DynamicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
    
    rowDic=[self witchCellForRow:0];
    
    [dynamicViewTableView reloadData];
   
}

-(void)loadTableView{

    dynamicViewTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    dynamicViewTableView.rowHeight=UITableViewAutomaticDimension;
    dynamicViewTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]init];
    bgImageView.image=BGIMG;
    dynamicViewTableView.backgroundView=bgImageView;
    
    
   

}

-(NSMutableDictionary *)witchCellForRow:(NSInteger)tag{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    
    
    [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                       
                                                       [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"CircleCell"],
                                                      
                                                       ]];
    
    
    
    [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                       
                                                       
                                                       [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"CircleCell2"],
                                                       [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"CircleCell2"],
                                                       [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"CircleCell2"]
                                                       ]];
    
    
    
    
    
    
    
    
    
    
    
    return dic;
    
}
#pragma mark tableView 委托协议

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return [[rowDic allKeys]count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view=[[UIView alloc]init];
    view.backgroundColor=[UIColor clearColor];
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    //回调事件
    
    
    
    if (indexPath.row%2) {

        cell.contentView.backgroundColor=BGCOLOR1;
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }


    [cell setValue:@"" forKey:@"data"];
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    
    
    
}


@end
