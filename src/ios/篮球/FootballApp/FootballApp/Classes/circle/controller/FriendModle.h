//
//  FriendModle.h
//  FootballApp
//
//  Created by zj on 16/6/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface FriendModle : BaseModel
@property(strong,nonatomic)id user; //用户信息
@property(strong,nonatomic)id commentDtos; //评论信息
@property(strong,nonatomic)NSString *pubTime;//发布时间
@property(strong,nonatomic)NSString *content;//发布内容
@property(strong,nonatomic)NSString *images;//发布图片

@property(strong,nonatomic)NSString *id;//动态id
@property(strong,nonatomic)NSString *userId;//用户id


@property(strong,nonatomic)NSString *readNum;//阅读数
@property(strong,nonatomic)NSString *laudNum;//赞数
@property(strong,nonatomic)NSString *commentNum;//评论数
@end



@interface MyUserModle : BaseModel
@property(strong,nonatomic)NSString *id;//用户id
@property(strong,nonatomic)NSString *avatar;//头像
@property(strong,nonatomic)NSString *name;  //姓名

@end


@interface MyCommentModle : BaseModel

@property(strong,nonatomic)id sender; //用户信息
@property(strong,nonatomic)NSString *id;//评论内容id
@property(strong,nonatomic)NSString *content;//评论内容
@property(strong,nonatomic)NSString *pubTime;  //发表时间

@end





