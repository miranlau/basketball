//
//  SendNewViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface SendNewViewController : BaseViewController
-(void)gotoLogin;  //是否去登录

@property(nonatomic,copy)NSString *shareContext;
@property(nonatomic,copy)NSString *shareMarkText;
@end
