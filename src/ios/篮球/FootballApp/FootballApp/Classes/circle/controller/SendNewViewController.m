//
//  SendNewViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "SendNewViewController.h"
#import "OnePhotoSelecter.h"
#import "ZZPhotoKit.h"
#import "PicsCell.h"
#import "MBPhotoBrowser.h"
#import "UIImageHandle.h"
@interface SendNewViewController ()<UITextViewDelegate,UIImagePickerControllerDelegate>
{

    __weak IBOutlet UILabel *message;

    __weak IBOutlet UITextView *texView;
    __weak IBOutlet UIView *bgView;
    
     NSMutableArray *imageArr;
    NSMutableArray *imageViewArr;
    
     NSMutableArray *compressArrayImage;
}
@end

@implementation SendNewViewController
-(NSMutableArray *)imageArr
{
    if (!imageArr) {
        imageArr = [NSMutableArray array];
    }
    return imageArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    imageArr = [NSMutableArray arrayWithCapacity:0];
    compressArrayImage = [NSMutableArray arrayWithCapacity:0];
    self.navigationItem.title = @"发布动态";
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"发布" style:UIBarButtonItemStylePlain target:self action:@selector(publish)];
    
    [self showImageList:imageArr];
    
    
    if ([_shareMarkText isEqualToString:@"分享到圈子"])
    {
        texView.text = _shareContext;
        message.hidden = YES;
    }
    else
    {
        message.hidden = NO;
    }
    
}
//发表按钮进行判断
-(void)publish{

    [self.view endEditing:YES];
    

    
    
    if ([imageArr count] >0) {
        
        if([texView.text isEqualToString:@""])
        {
            [SVProgressHUD showSuccessWithStatus:@"发布内容不能为空"];
            return;
        }
        
        
         [SVProgressHUD showWithStatus:@"上传图片中"];
        
        [self onLineBothPhotoAndText];
        
        
    }else{
        
        
        if([texView.text isEqualToString:@""])
        {
                    [SVProgressHUD showSuccessWithStatus:@"发布内容不能为空"];
                    return;
        }
        
        [self onLineOnlyText];
    
    }
    
    
    
}
//有内容没有图片
- (void)onLineOnlyText
{
    
    [ HttpRequestHelper postWithURL:Url(@"dynamic/publish") params:@{@"content":texView.text,@"longitude":[AccountHelper shareAccount].longitude,@"latitude":[AccountHelper shareAccount].latitude,@"addr":[AccountHelper shareAccount].addr} success:^(BOOL sucess, id responseObject2) {
        
        if (sucess) {
            
            [SVProgressHUD showSuccessWithStatus:@"信息发布成功"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadtheViewdata" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            
            [SVProgressHUD showErrorWithStatus:responseObject2];
            
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showSuccessWithStatus:@"信息发布失败"];
    }];
}
//有内容有图片
- (void)onLineBothPhotoAndText
{
    NSLog(@"datacount = %lu",(unsigned long)imageArr.count);
    [HttpRequestHelper postWithURL:Url(@"resource/upload") params:@{@"type":@"other"} formDataArray:imageArr success:^(BOOL sucess, id responseObject1) {
        if (sucess) {
            NSMutableArray *photoArray = responseObject1;
            NSString *str = [photoArray componentsJoinedByString:@","];
            
            
            [ HttpRequestHelper postWithURL:Url(@"dynamic/publish") params:@{@"images":str,@"content":texView.text,@"longitude":[AccountHelper shareAccount].longitude,@"latitude":[AccountHelper shareAccount].latitude,@"addr":[AccountHelper shareAccount].addr} success:^(BOOL sucess, id responseObject2) {
                
                if (sucess) {
                    
                    
                    [SVProgressHUD showSuccessWithStatus:@"信息发布成功"];
                     [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadtheViewdata" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                   
                    
                }else{
                    
                    [SVProgressHUD showErrorWithStatus:responseObject2];
                    
                }
                
            } failure:^(NSError *error) {
                [SVProgressHUD showSuccessWithStatus:@"信息发布失败"];
            }];
            
            
        }else{
            
            [SVProgressHUD showErrorWithStatus:responseObject1];
            
        }
        
        
    } failure:^(NSError *error) {
        [SVProgressHUD showSuccessWithStatus:@"上传图片失败"];
        
    }];
}

//这里外面点击预览要崩溃
-(void)imageViewClick:(UITapGestureRecognizer*)tap{

    MBPhotoBrowser *mbp=[[MBPhotoBrowser alloc]init];
    
    mbp.viewArr=imageViewArr;
    
    [mbp Show:(UIImageView *)tap.view];

}

//这里点击图片删除，
- (void)deleteSinglePhoto:(UIButton *)sender
{
    
    for (int i = 0; i < imageViewArr.count; i++)
    {
        UIImageView *imageview = imageViewArr[i];
        if (imageview.tag == sender.tag)
        {
            [imageArr removeObjectAtIndex:sender.tag];
                [self showImageList:imageArr];
        }
    }
}
-(void)showImageList:(id)list{

    for (UIView *view  in bgView.subviews) {
        [view removeFromSuperview];
    }
    
    NSInteger imageW=(kScreenWidth-20-15)/3;
    
    NSInteger spsce =5;
    
    [imageViewArr removeAllObjects];
    for (int i=0; i<[list count]; i++) {
        
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(i%3*(imageW+spsce), i/3*(imageW+spsce), imageW, imageW)];
        imageView.image = list[i];
        imageView.tag = i;
        imageView.userInteractionEnabled=YES;
        
        [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewClick:)]];

        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(imageW - 20, 0, 20, 20)];
        button.tag = i;
        [button addTarget:self action:@selector(deleteSinglePhoto:) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"xx按钮.png"] forState:UIControlStateNormal];
        [imageView addSubview:button];
        
        
        [bgView addSubview:imageView];
        
        if (!imageViewArr) {
            imageViewArr=[NSMutableArray array];
        }
        
        [imageViewArr addObject:imageView];
        
    }
    
    //超过9张按钮+号图片按钮消失
    if ([list count]<9) {
        
        NSInteger i= [list count];
        
        UIButton *buton=[[UIButton alloc]initWithFrame:CGRectMake(i%3*(imageW+spsce), i/3*(imageW+spsce), imageW, imageW)];
        [buton addTarget:self action:@selector(addImageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [buton setBackgroundImage:[UIImage imageNamed:@"添加图片"] forState:0];
        
        [bgView addSubview:buton];
 
    }
}


- (void)selectPhotoAlbumPhotos
{
    ZZPhotoController *photoController = [[ZZPhotoController alloc]init];
    photoController.selectPhotoOfMax = 9;
    
    [photoController showIn:self result:^(id responseObject){
        
        NSArray *array = (NSArray *)responseObject;
        
        
        //对图片进行压缩
        [compressArrayImage removeAllObjects];
        for(int i = 0;i < array.count;i++)
        {
            UIImage *imagee = [[UIImageHandle alloc] compressImage:array[i]];
           [compressArrayImage addObject: imagee];
        }
    
        [imageArr addObjectsFromArray:compressArrayImage];
        
        
        
        
        if (imageArr.count <= 9)
        {
             [self showImageList:imageArr];
        }
        else
        {
            [imageArr removeObjectsInArray:array];
            [SVProgressHUD showInfoWithStatus:@"图片不能超过9张"];
        }
        
        
    }];

}

- (void)actionSheetView
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选择图片" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"从相册选图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self selectPhotoAlbumPhotos];
        
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"现在拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self selectCamera];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消"
                                                        style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          
                                                      }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void)addImageBtnClick:(id)sendeer{

    [self actionSheetView];

}


-(void)textViewDidBeginEditing:(UITextView *)textView{

    message.hidden=YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView{

    if ([textView.text isEqualToString:@""]) {
        message.hidden=NO;
    }else{
     message.hidden=YES;
    }

}


//打开相机
- (void)selectCamera
{
    //获取支持的媒体格式
    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    //判断是否支持需要设置的sourceType
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        //1.初始化图片拾取器
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        //2.设置图片拾取器上得sourceType
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        //3.设置支持的媒体格式
        imagePickerController.delegate = self;
        imagePickerController.mediaTypes = @[mediaTypes[0]];
        //默认打开后置摄像头
        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //        前置摄像头
        //        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        //4.其他设置
        //        imagePickerController.allowsEditing = YES;
        //5.推送图片拾取器控制器
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    
}
//当用户选择了某一张图片或编辑使用了某张图片后触发此方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *editedImage = info[@"UIImagePickerControllerOriginalImage"];
    UIImage *image=[self sizedImage:editedImage withMaxValue:480];
//    myBlock(image);
    [imageArr addObject:image];
    //这里得到照片后要在屏幕显示出来
    [self showImageList:imageArr];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
//图片压缩处理
- (UIImage *)sizedImage:(UIImage *)originalImage withMaxValue:(float)maxValue
{
    UIImage *image = originalImage;
    CGFloat var1;
    CGFloat var2;
    
    if (image.size.width > image.size.height){
        // Landscape
        CGFloat aspect = image.size.width / image.size.height;
        CGFloat height = ceilf(maxValue/aspect);
        var1 = maxValue;
        var2 = height;
    }
    else if (image.size.height > image.size.width){
        // Portrait
        CGFloat aspect = image.size.height / image.size.width;
        CGFloat width = ceilf(maxValue/aspect);
        var1 = width;
        var2 = maxValue;
    }
    else{
        // Square
        var1 = maxValue;
        var2 = maxValue;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(var1, var2), NO, 1);
    [image drawInRect:CGRectMake(0, 0, var1, var2)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
