//
//  messageTuishong.h
//  FootballApp
//
//  Created by zj on 16/6/7.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface messageTuishong : BaseModel

@property(strong,nonatomic)NSString *senderHeader;  //发表时间
@property(strong,nonatomic)NSString *pushType;//评论内容
@property(strong,nonatomic)NSString *content;  //发表时间
@property(strong,nonatomic)NSString *dynamicId;//评论内容
@property(strong,nonatomic)NSString *senderName;  //发表时间
@property(strong,nonatomic)NSString *sendTime;  //发表时间
@property(strong,nonatomic)NSString *contentinfo;
@property(strong,nonatomic)NSString *matchId;
@end
