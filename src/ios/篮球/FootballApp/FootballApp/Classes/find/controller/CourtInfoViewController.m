//
//  CourtInfoViewController.m
//  FootballApp
//
//  Created by zj on 16/7/5.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CourtInfoViewController.h"

#import <JavaScriptCore/JavaScriptCore.h>
#import "CourtMapAddressController.h"
@protocol JsCourtProtocol <JSExport>

- (void)takePhone:(NSString *)phone; //联系电话

- (void)takeMap:(NSArray *)infos; //地图
@end


@interface CourtInfoViewController ()<JsCourtProtocol>
{
     JSContext *context;
    __weak IBOutlet UIWebView *myWebView;
}

@end

@implementation CourtInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [[LoadingView shareView]start]; //开始提示
    
    myWebView.delegate = self;
    myWebView.scrollView.delegate = self;
    
    [self loadPage];
    
    //初始化refreshView，添加到webview 的 scrollView子视图中
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0 - myWebView.scrollView.bounds.size.height, myWebView.scrollView.frame.size.width,myWebView.scrollView.bounds.size.height)];
        _refreshHeaderView.delegate = self;
        [myWebView.scrollView addSubview:_refreshHeaderView];
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    
    [[LoadingView shareView]stop];  //结束提示，为了下面的 viewDidLoad
    
}
- (void)loadPage
{
    NSString *urlstr0 = Url(@"arenas/detail");
    NSString *urlStr1 = [NSString stringWithFormat:@"%@?arenasId=%@",urlstr0,self.CourtId];
    
    
    NSURL *url = [NSURL URLWithString:urlStr1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [myWebView loadRequest:request];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
    // 通过UIWebView获得网页中的JavaScript执行环境
    context = [myWebView valueForKeyPath:
               @"documentView.webView.mainFrame.javaScriptContext"];
    // 设置处理异常的block回调
    [context setExceptionHandler:^(JSContext *ctx, JSValue *value) {
        NSLog(@"error: %@", value);
    }];
    context[@"JSBridge"] = self;
    
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:myWebView.scrollView];
}

- (void)takePhone:(NSString *)phone
{
    [self openTel:phone];
    
}
- (BOOL)openTel:(NSString *)tel
{
  
    NSString *telString = [NSString stringWithFormat:@"tel://%@",tel];
    return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString]];
    
}
- (void)takeMap:(NSArray *)infos
{
 
    if ([infos[0] doubleValue] > 0 || [infos[1] doubleValue] > 0) {
        
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CourtMapAddressController"];
        controller.navigationItem.title=@"球场位置";
        [controller setValue:infos[0]  forKey:@"lat"];
        [controller setValue:infos[1]  forKey:@"lon"];
        [controller setValue:infos[2]  forKey:@"CourtName"];
        [controller setValue:infos[3]  forKey:@"Courtaddress"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@"球场位置(经纬度)出错!"];
    }
    
    
  
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//下面是加上网页下拉刷新的第三方
- (void)webViewDidStartLoad:(UIWebView *)webView {
    _reloading = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
//    NSLog(@"load page error:%@", [error description]);
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:myWebView.scrollView];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self loadPage];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
    return [NSDate date]; // should return date data source was last changed
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
