//
//  CourtMapAddressController.h
//  FootballApp
//
//  Created by zj on 16/7/5.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface CourtMapAddressController : BaseViewController
@property(nonatomic,copy)NSString *lat;//纬度
@property(nonatomic,copy)NSString *lon;//经度
@property(nonatomic,copy)NSString *CourtName;//球场名字
@property(nonatomic,copy)NSString *Courtaddress;//球场地址
@end
