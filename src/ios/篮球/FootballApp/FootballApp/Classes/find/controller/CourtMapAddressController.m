//
//  CourtMapAddressController.m
//  FootballApp
//
//  Created by zj on 16/7/5.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CourtMapAddressController.h"

#import <MapKit/MapKit.h>
#import "DisplayMap.h"

@interface CourtMapAddressController ()<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation CourtMapAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
   _mapView.delegate=self;
    
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake([self.lon doubleValue],[self.lat doubleValue]);
    
    float zoomLevel = 0.02;
    MKCoordinateRegion region = MKCoordinateRegionMake(coords, MKCoordinateSpanMake(zoomLevel, zoomLevel));
    [_mapView setRegion:[_mapView regionThatFits:region] animated:YES];
    [_mapView setZoomEnabled:YES];
    [_mapView setScrollEnabled:YES];
    DisplayMap *ann = [[DisplayMap alloc] init];
    ann.title    = self.CourtName;
    ann.subtitle = self.Courtaddress;
    //地点名字
    ann.coordinate = region.center;
    [_mapView addAnnotation:ann];
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation> )annotation
{
    
    
    MKAnnotationView *newAnnotation=[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"annotation1"];
   
    newAnnotation.image = [UIImage imageNamed:@"icon_location.png"];
    newAnnotation.canShowCallout=YES;
    
    return newAnnotation;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
