//
//  CourtViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CourtViewController.h"

@interface CourtViewController ()
{

    __weak IBOutlet UITableView *tabView;
    NSMutableDictionary *rowDic;


}
@end

@implementation CourtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
    
}
-(void)loadTableView{
    
    tabView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    tabView.rowHeight=UITableViewAutomaticDimension;
    tabView.estimatedRowHeight=125;
    tabView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    
    
    rowDic =[self witchCellForRow:0];
    [tabView reloadData];
    
             
    
}
-(NSMutableDictionary *)witchCellForRow:(NSInteger)tag{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
   
   
        
        [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"CourtCell1"],
                                                           [BaseCellModel setModel:@"球场联系电话" key:@"" value:@"" image:@"电话.png" reuseIdentifier:@"CourtCell2"],
                                                           [BaseCellModel setModel:@"球场地址" key:@"" value:@"" image:@"比赛地点.png" reuseIdentifier:@"CourtCell2"]
                                                           ]];
   
    
   
        [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                       
                                                      
                                                       [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"CourtCell3"]
                                                       ]];
    
    
    
    
    
    
    
    
    
    
    
    return dic;
    
}
#pragma mark tableView 委托协议

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return [[rowDic allKeys]count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return section==0?0:25;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view=[[UIView alloc]init];
    
    
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 4, 55, 17)];
    label.text=@"球场状态";
    label.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
    label.font=[UIFont systemFontOfSize:10];
    label.layer.cornerRadius=3;
    label.textColor=[UIColor whiteColor];
    label.textAlignment=1;
    label.layer.masksToBounds=YES;
    [view addSubview:label];
    
    return view;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
    [cell setValue:model forKey:@"data"];
    
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
   
    
    
}
@end
