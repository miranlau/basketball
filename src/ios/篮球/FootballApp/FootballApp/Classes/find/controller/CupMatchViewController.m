//
//  CupMatchViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CupMatchViewController.h"
static  NSInteger height=0;
@interface CupMatchViewController ()
{
    __weak IBOutlet UIView *headView;

    __weak IBOutlet NSLayoutConstraint *top;
    __weak IBOutlet UITableView *tabView;
    NSMutableArray *dataArr;


}
@end

@implementation CupMatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataArr=[[NSMutableArray alloc]initWithArray:@[@"1",@"1",]];
    [tabView reloadData];
    
    [self initHeadView];
    
    [self loadTableView];
    
}
-(void)loadTableView{
    
    tabView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    tabView.rowHeight=UITableViewAutomaticDimension;
    tabView.estimatedRowHeight=125;
}
-(void)initHeadView{
    
    
    UIPanGestureRecognizer *pang=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panViewBegin:)];
    
    pang.minimumNumberOfTouches = 1;
    pang.maximumNumberOfTouches = 1;
    
    
    [headView addGestureRecognizer:pang];



}

-(IBAction)headBtnClick:(UIButton *)btn{
    
    
    if (btn.tag==1003||btn.tag==1002) {
        
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamListViewController"];
        [controller setValue:@(btn.tag) forKey:@"tag"];
        
        [self.navigationController pushViewController:controller animated:YES
         ];
        
        
    }else{
        
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ListViewController"] animated:YES
         ];
    
    }



}
-(void)panViewBegin:(UIPanGestureRecognizer *)pan{
    
    CGPoint translation = [pan translationInView:pan.view];

    
    if (translation.x == 0 && fabs(top.constant)<=height&&top.constant<=0) {

        top.constant=top.constant+translation.y;

        
        if (top.constant>0) {
            top.constant=0;
           
        }
    }


}


//每个分区的行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [dataArr count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CupMatchCell" forIndexPath:indexPath];
    
    [cell setValue:@"" forKey:@"data"];
    
    if (indexPath.row%2) {
        
        
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    
}
-(void)viewDidLayoutSubviews{

    height=headView.frame.size.height;
    

}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 保证是我们的tableivew
    
    
    if (scrollView == tabView) {
        // 保证我们是垂直方向滚动，而不是水平滚动
        if (scrollView.contentOffset.x == 0) {
            CGFloat y = scrollView.contentOffset.y;
            
            // 这个是非常关键的变量，用于记录上一次滚动到哪个偏移位置
            static CGFloat previousOffsetY = 0;
            
            // 向上滚动
            if (y > 0) {
                if (top.constant+height <= 0) {
                    return;
                }
                // 计算两次回调的滚动差:fabs(y - previousOffsetY)值
                CGFloat bottomY = top.constant+height - fabs(y - previousOffsetY);
                bottomY = bottomY >= 0 ? bottomY : 0;
                
                top.constant=bottomY-height;
                previousOffsetY = y;
                
                // 如果一直不松手滑动，重复向上向下滑动时，如果没有设置还原为0，则会出现马上到顶的情况。
                if (previousOffsetY >= height) {
                    previousOffsetY = 0;
                }
            }
            // 向下滚动
            else if (y < 0) {
                if (top.constant >= 0) {
                    return;
                }
                CGFloat bottomY = top.constant+height + fabs(y);
                bottomY = bottomY <= height ? bottomY : height;
                
                top.constant=bottomY-height;
            }
        }
        
        
    }
    
}

@end
