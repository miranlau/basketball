//
//  DisplayMap.h
//  map大头针
//
//  Created by zj on 16/7/5.
//  Copyright © 2016年 zj. All rights reserved.
//

#import <Foundation/Foundation.h>


#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface DisplayMap : NSObject

{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;


@end
