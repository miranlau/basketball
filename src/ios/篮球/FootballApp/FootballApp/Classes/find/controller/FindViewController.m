//
//  FindViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "FindViewController.h"
#import "FindTableViewDelegate.h"
#import "TeamModel.h"
#import "RaceEventModel.h"
#import "CourtModel.h"
@interface FindViewController ()<UISearchBarDelegate>
{
    
    FindTableViewDelegate *findShowDelegate;
    FindTableViewDelegate *searchShowDelegate;
    
    UIButton *leftImageView;
    __weak IBOutlet UIView *headView;
    
    __weak IBOutlet UITableView *findTableView;
    UIButton *lastBtn;
    
    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet UIButton *firstBtn;
    __weak IBOutlet UIImageView *line;
    
    UISearchController *_searchController;
    UISearchBar *_searchBar;
    
    NSInteger page;
    NSInteger searchPage;
    
    
    NSMutableArray *searchList;
    
    
    
    
    
    
    
}
@end

@implementation FindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadNavBarItmes];
    
    [self loadHeadView];
    
    [self loadTableView];
    
    [self headBtnClick:firstBtn];
    
}
-(void)loadNavBarItmes{
    
    leftImageView=[[UIButton alloc]initWithFrame:CGRectMake(-10, 0, 39, 39)];
    leftImageView.layer.cornerRadius=19.5;
    leftImageView.layer.masksToBounds=YES;
    leftImageView.userInteractionEnabled=YES;
    [leftImageView addTarget:self action:@selector(headImageClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self headImageChange];
    
    
    
    //去除边线
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftImageView];
    //右侧添加按钮
    
    
    _searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(60, 5, kScreenWidth-80, 35)];
    _searchBar.placeholder=@"搜索";
    _searchBar.delegate=self;
    
    
    
    [self.navigationController.navigationBar addSubview:_searchBar];
    
    
    
    
    
    
    
}
-(void)loadHeadView{
    
    
    
    
    
    headView.backgroundColor=BarColor;
    
}
-(void)headImageChange{
    
    if([AccountHelper shareAccount].isLogin ==YES){
        
        
        
        [leftImageView setBackgroundImageForState:0 withURL:[NSURL URLWithString:[AccountHelper shareAccount].player.avatar] placeholderImage:HeadImage];
        [leftImageView setTitle:@"" forState:0];
    }else{
        [leftImageView setBackgroundImage:HeadImage forState:0];
        [leftImageView setTitle:@"未登录" forState:0];
        [leftImageView setTitleColor:[UIColor grayColor] forState:0];
        leftImageView.titleLabel.font=[UIFont systemFontOfSize:12];
        
    }
    
}
-(void)loadTableView{
    
    findTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    findTableView.rowHeight=UITableViewAutomaticDimension;
    findTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    findTableView.backgroundView=bgImageView;
    
    findShowDelegate=[[FindTableViewDelegate alloc]initWithData:nil block:^(id data) {
        
        BaseCellModel *model=(BaseCellModel *)data;
        RaceEventModel *raceModle = model.value;
        CourtModel *modelC = model.value; //球场
        [_searchBar resignFirstResponder];
        
        if ([model.reuseIdentifier isEqualToString:@"MatchCell"]) {

            UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
            
            [controller setValue:raceModle.id     forKey:@"MatchId"];
            [controller setValue:raceModle.status forKey:@"status"];
            [self.navigationController pushViewController:controller animated:YES];

        }
        if ([model.reuseIdentifier isEqualToString:@"TeamCell"]) {
            
            
            TeamModel *tm=model.value;
            
            UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
            
            controller.navigationItem.title=tm.name;
            [controller setValue:tm.id forKey:@"teamId"];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        //球场（）
        if ([model.reuseIdentifier isEqualToString:@"CourtCell"]) {
            UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CourtInfoViewController"];
            controller.navigationItem.title=modelC.name;
            [controller setValue:modelC.id forKey:@"CourtId"];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
   
    }];
    
    
    findTableView.delegate=findShowDelegate;
    findTableView.dataSource=findShowDelegate;
    
    
    findTableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        
        
        if (_searchBar.showsCancelButton) {
            
            searchPage=1;
            [self  searchText:_searchBar.text];
            return;
        }
        
        page=1;
        
        [self  loadNetworkData:lastBtn.tag];
        
        
    }];
    
    
    findTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        
        if (_searchBar.showsCancelButton) {
            
            searchPage++;
            [self  searchText:_searchBar.text];
            return;
        }
        
        page++;
        
        [self  loadNetworkData:lastBtn.tag];
        
    }];
    
    
    
}
-(IBAction)headBtnClick:(UIButton *)butn{
    
    
    if (_searchBar.showsCancelButton) {
        
        return;
    }
    
    //防止点击多次
    if (butn == lastBtn) {
        return;
    }
    
    lastBtn.selected=NO;
    butn.selected=!butn.selected;
    
    lastBtn=butn;
    
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/3*(butn.tag-1001);
        [line layoutIfNeeded];
        
        
    }];
    
    page=1;
    
    [findTableView.mj_header beginRefreshing];
    
}

-(void)loadNetworkData:(NSInteger )sender{
    
    if (page==1) {
        
        [self.dataArr removeAllObjects];
        
    }
    
    [AccountHelper shareAccount].isShow=YES;
    
    
    if (sender==1001) {
        
        NSLog(@"url = %@",Url(@"league/query"));
        
        [HttpRequestHelper postWithURL:Url(@"league/query") params:@{@"pageNumber":@(page)} success:^(BOOL sucess, id responseObject) {
            
        if (sucess)
        {
                if (page==1) {
                    [self.dataArr removeAllObjects];
                }
                
                
                
                for(NSDictionary *dic in responseObject[@"content"]){
                    
                    RaceEventModel *model=[[RaceEventModel alloc]initWithDic:dic];
                    
                    [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell"]];
                }
                findShowDelegate.list=self.dataArr;
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
                [findTableView reloadData];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
            
        } failure:^(NSError *error) {
            
            [findTableView.mj_header endRefreshing];
            [findTableView.mj_footer endRefreshing];
        }];
        
    }
    
    
    if (sender==1002) {
        
        
        [HttpRequestHelper postWithURL:Url(@"team/query") params:@{@"pageNumber":@(page)} success:^(BOOL sucess, id responseObject) {
            
            
            
            
            
            if (sucess) {
                if (page==1) {
                    [self.dataArr removeAllObjects];
                }
                
                
                for(NSDictionary *dic in responseObject[@"content"]){
                    
                    TeamModel *model=[[TeamModel alloc]initWithDic:dic];
                    
                    [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"TeamCell"]];
                    
                    
                    
                }
                
                
                findShowDelegate.list=self.dataArr;
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
                [findTableView reloadData];
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
                
            }
            
            
            
            
        } failure:^(NSError *error) {
            
            [findTableView.mj_header endRefreshing];
            [findTableView.mj_footer endRefreshing];
            
            
        }];
        
        
    }
    
    if (sender==1003) {
        
        //CourtCell
        
        [HttpRequestHelper postWithURL:Url(@"arenas/list") params:@{@"pageNumber":@(page)} success:^(BOOL sucess, id responseObject) {
            
            
            
            
            
            if (sucess) {
                if (page==1) {
                    [self.dataArr removeAllObjects];
                }
                
                
                for(NSDictionary *dic in responseObject[@"content"]){
                    
                    CourtModel *model=[[CourtModel alloc]initWithDic:dic];
                    
                    [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"CourtCell"]];
                    
                    
                    
                }
                
                
                findShowDelegate.list=self.dataArr;
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
                [findTableView reloadData];
            }else{
                
                
                findShowDelegate.list=self.dataArr;
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
                [findTableView reloadData];
                [SVProgressHUD showErrorWithStatus:responseObject];
                
            }
            
            
            
            
        } failure:^(NSError *error) {
            findShowDelegate.list=self.dataArr;
            
            [findTableView.mj_header endRefreshing];
            [findTableView.mj_footer endRefreshing];
            
            [findTableView reloadData];
            
            
        }];
        
    }
    
    
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchPage=1;
    _searchBar.showsCancelButton=YES;
    [searchList removeAllObjects];
    findShowDelegate.list=searchList;
    
    [findTableView  reloadData];
    
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [_searchBar resignFirstResponder];
    _searchBar.text=@"";
    _searchBar.showsCancelButton=NO;
    
    findShowDelegate.list=self.dataArr;
    
    [findTableView  reloadData];
    
    
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
 
    [self searchText:searchBar.text];
  
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar resignFirstResponder];
    
    [self searchText:searchBar.text];
    
    
}

-(void)searchText:(NSString *)text{
    
    
    //如果是足球
    
    if ([text isEqualToString:@""]) {
        
        return;
    }
    if (searchPage==1) {
        [searchList removeAllObjects];
    }
    
    
    
    if (!searchList) {
        searchList=[NSMutableArray array];
    }
     [AccountHelper shareAccount].isShow=YES;
    if(lastBtn.tag==1001){
        
        [HttpRequestHelper postWithURL:Url(@"league/query") params:@{@"keyword":text,@"pageNumber":@(searchPage)} success:^(BOOL sucess, id responseObject) {
            
            if(sucess){
                
                for(NSDictionary *dic in responseObject[@"content"]){
                    
                    RaceEventModel *model=[[RaceEventModel alloc]initWithDic:dic];
                    
                    [searchList addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell"]];
                    
                }
                findShowDelegate.list=searchList;
                [findTableView  reloadData];
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
            }
        } failure:^(NSError *error) {
            
        }];
    }
    
    
    if(lastBtn.tag==1002){
        
        [HttpRequestHelper postWithURL:Url(@"team/query") params:@{@"keyword":text,@"pageNumber":@(searchPage)} success:^(BOOL sucess, id responseObject) {
            
            if(sucess){
                
                for(NSDictionary *dic in responseObject[@"content"]){
                    
                    TeamModel *model=[[TeamModel alloc]initWithDic:dic];
                    
                    [searchList addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"TeamCell"]];
                    
                    
                    
                }
                
                findShowDelegate.list=searchList;
                
                [findTableView  reloadData];
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
            }
        } failure:^(NSError *error) {
            
        }];
    }
    if(lastBtn.tag==1003){
        
        [HttpRequestHelper postWithURL:Url(@"arenas/list") params:@{@"keyword":text,@"pageNumber":@(searchPage)} success:^(BOOL sucess, id responseObject) {
            
            if(sucess){
                
                for(NSDictionary *dic in responseObject[@"content"]){
                    
                    CourtModel *model=[[CourtModel alloc]initWithDic:dic];
                    
                    [searchList addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"CourtCell"]];

                }
                
                findShowDelegate.list=searchList;
                
                [findTableView  reloadData];
                
                [findTableView.mj_header endRefreshing];
                [findTableView.mj_footer endRefreshing];
                
            }
        } failure:^(NSError *error) {
            
        }];
    }
    
}
#pragma mark 头像按钮点击事件
-(void)headImageClick:(id)sender{
    
    if (_searchBar.showsCancelButton) {
        
        return;
    }
    
    
    
    [self headImageViewClick];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:NO];
    
  
    _searchBar.hidden=NO;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:NO];
    
    _searchBar.hidden=YES;
    
}

@end
