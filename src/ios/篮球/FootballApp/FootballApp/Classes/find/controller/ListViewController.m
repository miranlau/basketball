//
//  ListViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ListViewController.h"
#import "MenberNumModel.h"
@interface ListViewController ()
{


    __weak IBOutlet UIView *headView;

    __weak IBOutlet UIButton *fristBtn;
    
    UIButton *lastBtn;
    
    __weak IBOutlet NSLayoutConstraint *left;
  
    __weak IBOutlet UIImageView *line;
}

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadHeadView];
    
    [self headBtnClick:fristBtn];
}
-(IBAction)headBtnClick:(UIButton *)butn{
    
    //防止点击多次
    if (butn==lastBtn) {
        return;
    }
    
    lastBtn.selected=NO;
    butn.selected=!butn.selected;
    
    lastBtn=butn;
    
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/3*(butn.tag-1001);
        [line layoutIfNeeded];
        
        
    }];
    
    [self loadNumbersViewew:butn.tag];
    
    
}
-(void)loadHeadView{
    
    //去除边线
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    headView.backgroundColor=BarColor;
    
}
-(void)loadNumbersViewew:(NSInteger )tag{
    
    
    for ( UIView *vew in _numbersView.subviews) {
        [vew removeFromSuperview];
    }


    NSMutableArray *array=[NSMutableArray array];
    
    
    
    for (int i=0; i<4; i++) {
        
        MenberNumModel *model=[[MenberNumModel alloc]init];
        
        model.name=[NSString stringWithFormat:@"球员%d",i];
        model.type1=@"1";
        model.type2=@"2";
        model.type3=@"3";
        model.type4=@"4";
        model.type5=@"3";
        model.type6=@"2";
        [array addObject:model];
        
    }
    
   
    
    
    
    
   BSNumbersView *view =[[BSNumbersView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-109)];
    [_numbersView addSubview:view];
    
    view.bodyData = array;
    view.headerTextColor=[UIColor whiteColor];
    view.headerBackgroundColor=[UIColor clearColor];
    view.bodyTextColor=[UIColor whiteColor];
    view.bodyBackgroundColor=[UIColor clearColor];
    
    view.freezeBodyTextColor=[UIColor clearColor];
    view.freezeBodyTextColor=[UIColor whiteColor];
    
    
    if (tag==1001) {
         view.headerData = @[@"名次", @"球队", @"胜/负/平", @"进/失/净", @"积分", @"扑救", @"犯规"];
    }
    
    if (tag==1002) {
        view.headerData = @[@"名次",@"球员", @"球队", @"进球", @"射正/射门", @"抢断/拦截/解围", @"扑救"];
    }
    
    if (tag==1003) {
        view.headerData = @[@"名次",@"球员", @"进球", @"红", @"黄", @"双簧", @"犯规"];
    }
   
    view.freezeColumn = 1;
    view.itemHeight=20;
    //    self.numbersView.maxItemWidth=150;
    
    
    
    view.bodyFont = [UIFont systemFontOfSize:12];
    view.headerFont=[UIFont systemFontOfSize:12];
    view.freezeBodyFont=[UIFont systemFontOfSize:12];
    
    
    
    [view reloadData];

}


@end
