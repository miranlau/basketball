//
//  SignUpViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "SignUpViewController.h"
#import "TeamModel.h"
#import "RaceEventModel.h"
#import <JavaScriptCore/JavaScriptCore.h>

#import <AFNetworking.h>

@protocol JsProtocol <JSExport>

- (void)PayAttentionAction; //关注
- (void)ActionTeams;        //球队
- (void)ActionTeamsList;    //榜单
- (void)ActionTeamsProcess;  //赛程
- (void)ActionCheckInfo:(NSString *)infoId; //动态详情


@end


@interface SignUpViewController ()<JsProtocol>
{
    NSMutableArray *teams;
    JSContext *context;
    int number;  //因为这里我点击了一下 js那边会调用我三次，所以我用number标记只调用一次

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mywebBottomConstant;

@property (weak, nonatomic) IBOutlet UIButton *SignInStartbutton;

@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@property(nonatomic,assign)int tag;
@property(nonatomic,assign)int isFollow;
@property(nonatomic,assign)int signInOrNot;
@property(nonatomic,assign)int statuss;
@end

@implementation SignUpViewController

//网页中关注的方法
- (void)PayAttentionAction
{
    
    if ([AccountHelper shareAccount].isLogin == NO) {
        [[[FBAlertView alloc ]initWithTitle:@"您还未登陆，是否去登陆？" butnTitlt:@"去登陆!" block:^(id data) {
            [AccountHelper shareAccount].isReg=NO;
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
            controller.navigationItem.title=@"登录";
            [self.navigationController pushViewController:controller animated:YES];
        }]show];
        return;
    }
    else
    {
        if (self.isFollow == 1)
        {
            [self CancelpayAttentionClicked];
        }
        else
        {
            [self payAttentionClicked];
        }
    }
    
    
}

- (void)ActionTeams
{
    self.tag = 10;
    [self gotoWebView:self.tag];
}
- (void)ActionTeamsList
{
    self.tag = 11;
    [self gotoWebView:self.tag];
}
- (void)ActionTeamsProcess
{
    self.tag = 12;
    [self gotoWebView:self.tag];
}
- (void)gotoWebView:(int)tag
{
     __weak typeof(self)weakSelf = self;
    if (tag == 10)
    {
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamDetail"];
        controller.navigationItem.title=@"参赛球队";
        [controller setValue:self.MatchId forKey:@"MatchId"];
          [controller setValue:Url(@"league/queryTeamsByLeagueId") forKey:@"url"];
        dispatch_async(dispatch_get_main_queue(), ^{ //返回主线程执行，否则抛出异常，可能会有内存溢出
        [weakSelf.navigationController pushViewController:controller animated:YES];
        });
    }
    else if (tag == 11)
    {
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamDetail"];
        controller.navigationItem.title=@"榜单";
        [controller setValue:self.MatchId forKey:@"MatchId"];
           [controller setValue:Url(@"league/leagueBang") forKey:@"url"];
        dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.navigationController pushViewController:controller animated:YES];
        });
    }
    else
    {
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamDetail"];
        controller.navigationItem.title=@"赛程";
        [controller setValue:self.MatchId forKey:@"MatchId"];
        [controller setValue:Url(@"league/queryMatchByLeagueId") forKey:@"url"];       dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.navigationController pushViewController:controller animated:YES];
        });

    }
        
}



//进入动态详情
- (void)ActionCheckInfo:(NSString *)infoId
{
    __weak typeof(self) weakself = self;
 
    if (number == 0)
    {
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"leagueInfoDetailController"];
        controller.navigationItem.title=@"动态详情";
        [controller setValue:infoId forKey:@"InfoDetail"]; //这里传的是赛事用户详情
        [controller setValue:@"1" forKey:@"flags"];
        dispatch_async(dispatch_get_main_queue(), ^{ //返回主线程执行，否则抛出异常，可能会有内存溢出
            [weakself.navigationController pushViewController:controller animated:YES];
        });
        number ++;
    }
  
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    number = 0;

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[LoadingView shareView]stop];  //结束提示
}
- (void)loadPage
{
    NSString *urlstr0 = Url(@"league/queryLeagueView");
    NSString *urlStr1 = [NSString stringWithFormat:@"%@?leagueId=%@",urlstr0,self.MatchId];
    
    
    NSURL *url = [NSURL URLWithString:urlStr1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebView loadRequest:request];
}
- (void)viewDidLoad {
    [super viewDidLoad];
      [[LoadingView shareView]start]; //开始提示
    number = 0;
    self.statuss = [self.status intValue];
    self.title = @"赛事详情";

    teams = [[NSMutableArray alloc]initWithCapacity:0];
    
        self.myWebView.delegate = self;
        self.myWebView.scrollView.delegate = self;
    
        [self.myWebView setUserInteractionEnabled:YES];
        [self loadPage];
    
    //初始化refreshView，添加到webview 的 scrollView子视图中
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-self.myWebView.scrollView.bounds.size.height, self.myWebView.scrollView.frame.size.width, self.myWebView.scrollView.bounds.size.height)];
        _refreshHeaderView.delegate = self;
        [self.myWebView.scrollView addSubview:_refreshHeaderView];
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    
     [[LoadingView shareView]stop];  //结束提示，为了下面的 viewDidLoad
    
    [self getInfoDetails];
    
}
- (void)getInfoDetails
{
    [HttpRequestHelper postWithURL:Url(@"league/queryById") params:@{@"leagueId":self.MatchId} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
                NSDictionary *dic = responseObject;
                RaceEventModel *model=[[RaceEventModel alloc]initWithDic:dic];
            
                self.signInOrNot = model.gauntlet;
                self.isFollow    = model.isfollow;
            
            if (self.statuss == 0)
            {   self.SignInStartbutton.hidden = NO;
                 self.mywebBottomConstant.constant = 40;
                self.SignInStartbutton.enabled = YES;
                if (self.signInOrNot == 1)
                    {
                        [self.SignInStartbutton setTitle:@"取消报名" forState:UIControlStateNormal];
                        self.SignInStartbutton.backgroundColor = [UIColor colorWithRed:251.0/255.0 green:88.0/255.0 blue:109.0/255.0 alpha:1.0];
                        self.SignInStartbutton.tag = 100;
                    }
                else
                    {
                        [self.SignInStartbutton setTitle:@"报名参赛" forState:UIControlStateNormal];
                        self.SignInStartbutton.backgroundColor = [UIColor colorWithRed:28.0/255.0 green:154.0/255.0 blue:190.0/255.0 alpha:1.0];
                        self.SignInStartbutton.tag = 101;
                    }
            }
            else if (self.statuss == 1)
            {
                self.mywebBottomConstant.constant = 0;
                self.SignInStartbutton.hidden = YES;
                self.SignInStartbutton.enabled = YES;
               

            }
            else if (self.statuss == 2)
            {
                 self.SignInStartbutton.enabled = YES;
                 self.SignInStartbutton.hidden = YES;
                 self.mywebBottomConstant.constant = 0;
            }
            else
            {
                 self.SignInStartbutton.hidden = YES;
                 self.SignInStartbutton.enabled = YES;
                 self.mywebBottomConstant.constant = 0;

            }
            
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
   
    // 通过UIWebView获得网页中的JavaScript执行环境
    context = [self.myWebView valueForKeyPath:
                @"documentView.webView.mainFrame.javaScriptContext"];
    // 设置处理异常的block回调
    [context setExceptionHandler:^(JSContext *ctx, JSValue *value) {
        NSLog(@"error: %@", value);
    }];
     context[@"JSBridge"] = self;
    
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.myWebView.scrollView];
}

- (void)payAttentionClicked
{
    [HttpRequestHelper postWithURL:Url(@"league/follow") params:@{@"leagueId":self.MatchId} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
            
            [[[FBAlertView alloc]initWithTitle:@"关注成功!" butnTitlt:@"好!" block:^(id data) {
                [self viewDidLoad];
            }] show];
            
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)CancelpayAttentionClicked
{
    [HttpRequestHelper postWithURL:Url(@"league/unfollow") params:@{@"leagueId":self.MatchId} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
            
            [[[FBAlertView alloc]initWithTitle:@"取消关注成功!" butnTitlt:@"好!" block:^(id data) {
                [self viewDidLoad];
            }] show];
            
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
}



- (IBAction)signUpBtnClick:(UIButton *)sender {
   
    if (sender.tag == 100)
    {
        [[[FBAlertView alloc]initWithTitle:@"确定要取消报名吗?机会难得哟!" butnTitlt:@"我要取消" block:^(id data) {
        [self CancleSignINAndOut];
            
        }] show];
    }
    else
    {
        if ([AccountHelper shareAccount].isLogin == NO) {
            [[[FBAlertView alloc ]initWithTitle:@"您还未登陆，是否去登陆？" butnTitlt:@"去登陆!" block:^(id data) {
                [AccountHelper shareAccount].isReg=NO;
                UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
                            controller.navigationItem.title=@"登录";
                            [self.navigationController pushViewController:controller animated:YES];
                        }]show];
                        return;
                    }
        [[AccountHelper shareAccount] getPlayer:^(id data) {
                teams=[self getMyLeaderTeams];
       
                    if([teams count]<=0){
                        [[[FBAlertView alloc ]initWithTitle:@"您当前没有球队，暂不能报名参赛，是否去创建球队？" butnTitlt:@"去创建!" block:^(id data) {
                        [AccountHelper shareAccount].isReg=NO;
                        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamNameViewController"] animated:YES];
                        }]show];
                    }else{
                    [[[TeamView alloc]initWithArr:teams clickBlock:^(id data) {
            
                        TeamModel *model=teams[[data intValue]];
                     
                      
                        [self gotoSignInTheMatch:self.MatchId And:model.id];
                    }]show];
                    }
        }];
    }
    
}
- (void)CancleSignINAndOut
{

            [HttpRequestHelper postWithURL:Url(@"league/cancelEnroll") params:@{@"leagueId":self.MatchId} success:^(BOOL sucess, id responseObject) {
                        if (sucess)
                        {
                      
                            [[[FBAlertView alloc]initWithTitle:@"您已成功取消报名" butnTitlt:@"好!" block:^(id data) {
                                [self viewDidLoad];
                            }] show];

                        }
                        else
                        {
                            [SVProgressHUD showErrorWithStatus:responseObject];
                        }
    
                    } failure:^(NSError *error) {
    
                    }];
}

- (void)gotoSignInTheMatch:(NSString *)matchId And:(NSString *)teamId
{
   
    [HttpRequestHelper postWithURL:Url(@"league/enroll") params:@{@"leagueId":matchId,@"teamId":teamId} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {

                [[[FBAlertView alloc]initWithTitle:@"您已成功报名,请等待比赛开始!" butnTitlt:@"好!" block:^(id data) {
                    [self viewDidLoad];
                }] show];

        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
    _reloading = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"load page error:%@", [error description]);
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.myWebView.scrollView];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self loadPage];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
    return [NSDate date]; // should return date data source was last changed
}


@end
