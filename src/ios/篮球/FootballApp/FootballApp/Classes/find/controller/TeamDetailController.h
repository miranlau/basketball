//
//  TeamDetailController.h
//  FootballApp
//
//  Created by zj on 16/5/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"
#import "EGORefreshTableHeaderView.h" //UIWebView添加刷新头，像UITableView一样

@interface TeamDetailController : BaseViewController<UIWebViewDelegate, UIScrollViewDelegate, EGORefreshTableHeaderDelegate>
{
    //下拉视图
    EGORefreshTableHeaderView * _refreshHeaderView;
    //刷新标识，是否正在刷新过程中
    BOOL _reloading;
}
@property(nonatomic,copy)NSString *MatchId;
@property(nonatomic,copy)NSString *url;
@end
