//
//  TeamDetailController.m
//  FootballApp
//
//  Created by zj on 16/5/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamDetailController.h"
#import <JavaScriptCore/JavaScriptCore.h>

//这个协议名字不能重名，重名了的话不能够调用
@protocol FindProtocol <JSExport>

- (void)ActionGoToMatch:(NSArray *)info;        //比赛详情
- (void)ActionGoToTeam:(NSString *)infoId;      //球队详情
- (void)ActionAccessTeam:(NSString *)infoIds;    //参赛球队 进入 球队详情

@end
//@protocol FindProtocols <JSExport>
//
//- (void)ActionGoToTeams:(NSString *)infoId; //参赛球队 进入 球队详情
//@end

@interface TeamDetailController ()<UIWebViewDelegate,FindProtocol>
{
    JSContext *contexts;

}
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;

@end

@implementation TeamDetailController

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
    // 通过UIWebView获得网页中的JavaScript执行环境
    contexts = [self.myWebview valueForKeyPath:
               @"documentView.webView.mainFrame.javaScriptContext"];
    // 设置处理异常的block回调
    [contexts setExceptionHandler:^(JSContext *ctx, JSValue *value) {
        NSLog(@"error: %@", value);
    }];
    contexts[@"JSBridge"] = self;
    
    
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_myWebview.scrollView];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
     [[LoadingView shareView]stop];  //结束提示
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _myWebview.delegate = self;
    _myWebview.scrollView.delegate = self;
     [self loadPage];
    
    //初始化refreshView，添加到webview 的 scrollView子视图中
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-_myWebview.scrollView.bounds.size.height, _myWebview.scrollView.frame.size.width, _myWebview.scrollView.bounds.size.height)];
        _refreshHeaderView.delegate = self;
        [_myWebview.scrollView addSubview:_refreshHeaderView];
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    
     [[LoadingView shareView]start]; //开始提示
    

    
   
    
}
//加载网页
- (void)loadPage {
    
    NSString *urlStr1 = [NSString stringWithFormat:@"%@?leagueId=%@",self.url,self.MatchId];
    NSURL *url = [NSURL URLWithString:urlStr1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebview loadRequest:request];
    
}

- (void)ActionGoToMatch:(NSArray *)info
{
      NSString *str = info[0];
      NSString *str1 = info[1];

    //赛前
    if ([str intValue]==10) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        controller.navigationItem.title=@"赛前";
        [controller setValue:str1 forKey:@"matchId"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    //比赛中
    if ([str intValue]==20) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        controller.navigationItem.title=@"赛中";
        [controller setValue:str1 forKey:@"matchId"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    //比赛结束
    if ([str intValue]==100) {

        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
        controller.navigationItem.title=@"赛后";
        [controller setValue:str1 forKey:@"matchId"];
        [self.navigationController pushViewController:controller animated:YES];

    }
 
}
//赛程里面
- (void)ActionGoToTeam:(NSString *)infoId
{
    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    [controller setValue:infoId forKey:@"teamId"];
    [self.navigationController pushViewController:controller animated:YES];
}
//球队里面
- (void)ActionAccessTeam:(NSString *)infoIds
{
    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    [controller setValue:infoIds forKey:@"teamId"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    _reloading = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"load page error:%@", [error description]);
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_myWebview.scrollView];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self loadPage];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
    return [NSDate date]; // should return date data source was last changed
}



@end
