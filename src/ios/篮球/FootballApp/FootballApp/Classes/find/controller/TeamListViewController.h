//
//  TeamListViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface TeamListViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *teamtableView;
@property(assign,nonatomic)NSInteger  tag;

@end
