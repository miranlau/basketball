//
//  TeamListViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamListViewController.h"

@interface TeamListViewController ()

@end

@implementation TeamListViewController
@synthesize teamtableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
   
}
-(void)loadTableView{
    
    teamtableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    teamtableView.rowHeight=UITableViewAutomaticDimension;
    teamtableView.estimatedRowHeight=100;
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    teamtableView.backgroundView=bgImageView;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 10;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
   
    
    
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:self.tag==1002?@"TeamCell":@"WatchCell" forIndexPath:indexPath];
    
    cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
    
    
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
