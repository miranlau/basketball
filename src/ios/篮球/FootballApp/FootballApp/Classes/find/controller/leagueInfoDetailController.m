//
//  leagueInfoDetailController.m
//  FootballApp
//
//  Created by zj on 16/5/27.
//  Copyright © 2016年 North. All rights reserved.
//

#import "leagueInfoDetailController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "AccountHelper.h"
@protocol DetailProtocol <JSExport>

- (void)ActionComment:(NSArray *)comment; //评论动态方法
- (void)ActionLaud:(NSString *)infoId;  //点赞

@end





@interface leagueInfoDetailController ()<DetailProtocol>
{
      JSContext *context;
}
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;
@property(nonatomic,assign)int flag;

@end

@implementation leagueInfoDetailController
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[LoadingView shareView]stop];  //结束提示
}
- (void)loadPage
{
    NSString *str = Url(@"league/leagueInfoDetail");
    NSString *strDetail = [str stringByAppendingString:[NSString stringWithFormat:@"?infoId=%@",self.InfoDetail]];
    [self.myWebview setUserInteractionEnabled:YES];
    NSURL *url = [NSURL URLWithString:strDetail];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    if ([AccountHelper shareAccount].isLogin==YES) {
        
        [request setValue:[AccountHelper shareAccount].token forHTTPHeaderField:@"token"];
        
    }
    [self.myWebview loadRequest:request];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
        [[LoadingView shareView]start]; //开始提示
    
    self.myWebview.delegate = self;
    self.myWebview.scrollView.delegate = self;
    [self loadPage];
    
    //初始化refreshView，添加到webview 的 scrollView子视图中
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-self.myWebview.scrollView.bounds.size.height, self.myWebview.scrollView.frame.size.width, self.myWebview.scrollView.bounds.size.height)];
        _refreshHeaderView.delegate = self;
        [self.myWebview.scrollView addSubview:_refreshHeaderView];
    }
    [_refreshHeaderView refreshLastUpdatedDate];
  
    [[LoadingView shareView]stop];  //结束提示,这句话的目的是评论后不会再执行viewDidAppear了
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    _reloading = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
//    NSLog(@"load page error:%@", [error description]);
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.myWebview.scrollView];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self loadPage];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
    return [NSDate date]; // should return date data source was last changed
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
    // 通过UIWebView获得网页中的JavaScript执行环境
    context = [self.myWebview valueForKeyPath:
               @"documentView.webView.mainFrame.javaScriptContext"];
    // 设置处理异常的block回调
    [context setExceptionHandler:^(JSContext *ctx, JSValue *value) {
//        NSLog(@"error: %@", value);
    }];
    context[@"JSBridge"] = self;
    
    
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.myWebview.scrollView];
    
}

//动态详情评论
- (void)ActionComment:(NSArray *)comment
{
//    NSLog(@"commment = %@",comment);
//因为传到服务器的数据的编码是UTF-8，在服务器看到是乱码，那说明服务器解析数据采用的可能不是UTF-8，使用中文操作系统，默认的编码是GBK,
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSString *str = comment[0];
    NSData *data = [str dataUsingEncoding:enc];
    NSString *retStr = [[NSString alloc] initWithData:data encoding:enc];

    [HttpRequestHelper postWithURL:Url(@"league/commentLeagueInfo") params:@{@"content":retStr,@"infoId":comment[1]} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
            
            [[[FBAlertView alloc]initWithTitle:@"评论成功!" butnTitlt:@"好!" block:^(id data) {
              [self viewDidLoad];
            }] show];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

//点赞
- (void)ActionLaud:(NSString *)infoId
{
    [HttpRequestHelper postWithURL:Url(@"league/addLaud") params:@{@"infoId":infoId} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
            //这里点赞成功不提示
            [[[FBAlertView alloc]initWithTitle:@"点赞成功!" butnTitlt:@"好!" block:^(id data) {
                [self viewDidLoad];
            }] show];
           
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
