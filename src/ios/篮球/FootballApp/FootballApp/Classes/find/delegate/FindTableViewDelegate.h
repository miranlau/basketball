//
//  FindTableViewDelegate.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FindTableViewDelegate : NSObject<UITableViewDelegate,UITableViewDataSource>
{
    MutableBlock block;

}
@property(copy,nonatomic) id list;

-(id)initWithData:(NSMutableArray *)data block:(MutableBlock)clickrowBlock;


@end
