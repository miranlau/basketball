//
//  FindTableViewDelegate.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "FindTableViewDelegate.h"

@implementation FindTableViewDelegate
-(id)initWithData:(NSMutableArray *)data block:(MutableBlock)clickrowBlock;{

    self=[super init];
    
    if (self) {
        block=clickrowBlock;
        self.list=data;
    }
    return self;
}
//分区数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
//每个分区的行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [self.list count];

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BaseCellModel *model=self.list[indexPath.row];
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    if (indexPath.row%2) {
        
        
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
 
        [cell setValue:model forKey:@"data"];
    
  
    
   
    
    return cell;
    

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (block) {
        block(self.list[indexPath.row]);
    }
    
    
}


@end
