//
//  CourtCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CourtCell.h"

#import "CourtModel.h"


@implementation CourtCell

-(void)getdata:(id)data{

    BaseCellModel *model=(BaseCellModel *)data;
    
    if ([model.reuseIdentifier isEqualToString:@"CourtCell2"]) {
        self.cellImageView.image=[UIImage imageNamed:model.image];
        self.cellName.text=model.name;
    }
    
    if ([model.reuseIdentifier isEqualToString:@"CourtCell"]) {
       
        CourtModel *court =model.value;
        
        [self.cellImageView setImageWithURL:[NSURL URLWithString:court.icon] placeholderImage:[UIImage imageNamed:@"比赛默认图片.png"]];
        self.cellName.text=court.name;
        self.tel.text = court.telephone_number_1;
        self.address.text=court.address;
        _score.show_star=[court.stars intValue]/20;
        
        [self layoutIfNeeded];
    }
    
}

@end
