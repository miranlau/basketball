//
//  CourtModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/6/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface CourtModel : BaseModel
@property(strong,nonatomic) NSString *address;
@property(strong,nonatomic) NSString *company_id;
@property(strong,nonatomic) NSString *icon;
@property(strong,nonatomic) NSString *id;
@property(strong,nonatomic) NSString *latitude;
@property(strong,nonatomic) NSString *longitude;
@property(strong,nonatomic) NSString *name;
@property(strong,nonatomic) NSString *status;
@property(strong,nonatomic) NSString *telephone_name_1;
@property(strong,nonatomic) NSString *telephone_number_1;
@property(strong,nonatomic) NSString *url;
@property(strong,nonatomic) NSString *stars;

@end
