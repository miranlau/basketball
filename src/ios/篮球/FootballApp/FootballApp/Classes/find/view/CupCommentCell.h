//
//  CupCommentCell.h
//  FootballApp
//
//  Created by zj on 16/6/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "FriendModle.h"
#import "KZLinkLabel.h"
@interface CupCommentCell : BaseCell<UIActionSheetDelegate>

@property (nonatomic,strong) NSDictionary *selectedLinkDic;

@property (weak, nonatomic) IBOutlet UIImageView *myAvatr;
@property (weak, nonatomic) IBOutlet UILabel *myName;

@property (weak, nonatomic) IBOutlet UILabel *myTime;

@property (weak, nonatomic) IBOutlet KZLinkLabel *mycoment;

@property(nonatomic,strong) MyCommentModle *commentInfo1;
@property(nonatomic,strong) MyUserModle *userInfo1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage;
@end
