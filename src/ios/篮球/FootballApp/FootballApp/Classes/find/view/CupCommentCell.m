//
//  CupCommentCell.m
//  FootballApp
//
//  Created by zj on 16/6/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CupCommentCell.h"

@implementation CupCommentCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)getdata:(id)data{
    
    BaseCellModel *model=data;
    MyCommentModle *tm=model.value;
    MyUserModle *user = [[MyUserModle alloc]initWithDic:tm.sender];

    [self.myAvatr setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:HeadImage];
    self.myAvatr.layer.cornerRadius = 25;
    self.myAvatr.layer.masksToBounds = YES;
    self.myAvatr.clipsToBounds = YES;
    self.myName.text=[NSString stringWithFormat:@"%@",user.name];
    self.myTime.text=[NSString stringWithFormat:@"%@",tm.pubTime];
   
    //将文本内容进行链接判断
    [self changeMyLabelTextLinkColorAndClicked:tm.content];

}
- (void)changeMyLabelTextLinkColorAndClicked:(NSString *)text_label_text
{
    UIFont *font = [UIFont systemFontOfSize:13];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:text_label_text attributes:attributes];
    self.mycoment.textColor = [UIColor whiteColor];
    self.mycoment.attributedText = attributedString;
    self.mycoment.linkColor = [UIColor lightGrayColor];
    self.mycoment.linkHighlightColor = [UIColor blueColor];
    
    self.mycoment.linkTapHandler = ^(KZLinkType linkType, NSString *string, NSRange range){
        if (linkType == KZLinkTypeURL) {
            [self openURL:[NSURL URLWithString:string]];
        } else if (linkType == KZLinkTypePhoneNumber) {
            [self openTel:string];
        } else {
            NSLog(@"Other Link");
        }
    };
    self.mycoment.linkLongPressHandler = ^(KZLinkType linkType, NSString *string, NSRange range){
        NSMutableDictionary *linkDictionary = [NSMutableDictionary dictionaryWithCapacity:3];
        [linkDictionary setObject:@(linkType) forKey:@"linkType"];
        [linkDictionary setObject:string forKey:@"link"];
        [linkDictionary setObject:[NSValue valueWithRange:range] forKey:@"range"];
        self.selectedLinkDic = linkDictionary;
        
        NSString *openTypeString;
        if (linkType == KZLinkTypeURL) {
            openTypeString = @"在Safari中打开";
        } else if (linkType == KZLinkTypePhoneNumber) {
            openTypeString = @"直接拨打";
        }
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"取消"
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:@"拷贝",openTypeString, nil];
        [sheet showInView:self];
    };
    
}

//下面的几个方法都为链接的这个
- (BOOL)openURL:(NSURL *)url
{
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
        return YES;
    } else {
        return NO;
    }
}
- (BOOL)openTel:(NSString *)tel
{
    //    [[[FBAlertView alloc]initWithTitle:@"是否拨打该电话？" butnTitlt:@"拨打" block:^(id data) {
    //        NSString *telString = [NSString stringWithFormat:@"tel://%@",tel];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString]];
    //
    //    }] show];
    NSString *telString = [NSString stringWithFormat:@"tel://%@",tel];
    return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString]];
    
}
#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (!self.selectedLinkDic) {
        return;
    }
    switch (buttonIndex)
    {
        case 0:
        {
            [UIPasteboard generalPasteboard].string = self.selectedLinkDic[@"link"];
            break;
        }
        case 1:
        {
            KZLinkType linkType = [self.selectedLinkDic[@"linkType"] integerValue];
            if (linkType == KZLinkTypeURL) {
                NSURL *url = [NSURL URLWithString:self.selectedLinkDic[@"link"]];
                [self openURL:url];
            } else if (linkType == KZLinkTypePhoneNumber) {
                [self openTel:self.selectedLinkDic[@"link"]];
                
                //                [[[FBAlertView alloc]initWithTitle:@"是否拨打该电话？" butnTitlt:@"拨打" block:^(id data) {
                //                    NSString *telString = [NSString stringWithFormat:@"tel://%@",self.selectedLinkDic[@"link"]];
                //                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString]];
                //
                //                }] show];
                
            }
            break;
        }
    }
}

@end
