//
//  CupMatchCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

#import "KZLinkLabel.h"

typedef void (^ABlock)(void); //定义一个简单的Block

@interface CupMatchCell : BaseCell<UIActionSheetDelegate>
{
    NSMutableArray *viewArr;
    ABlock _block;
}
@property (nonatomic,strong) NSDictionary *selectedLinkDic;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@property (weak, nonatomic) IBOutlet UIImageView *myAvatar;
@property (weak, nonatomic) IBOutlet UILabel *myname;
@property (weak, nonatomic) IBOutlet UILabel *mytime;


@property (weak, nonatomic) IBOutlet KZLinkLabel *mycontent0;


@property (weak, nonatomic) IBOutlet UIButton *myreadnum;
@property (weak, nonatomic) IBOutlet UIButton *mylaidnum;

@property (weak, nonatomic) IBOutlet UIButton *mycommentnum;

@property (weak, nonatomic) IBOutlet UIButton *delbutton;
@property (weak, nonatomic) IBOutlet UIImageView *delImage;
@property (weak, nonatomic) IBOutlet UIButton *headButton;
@property (readwrite, nonatomic, strong, setter = af_setActiveImageDownloadReceipt:) AFImageDownloadReceipt *af_activeImageDownloadReceipt;
@end
