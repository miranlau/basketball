//
//  CupMatchCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CupMatchCell.h"
#import "MBPhotoBrowser.h"
#import "FriendModle.h"

#import "KZLinkLabel.h"

#define FPS 2  //帧率,1秒30张图片

@implementation CupMatchCell

-(void)getdata:(id)data{

    BaseCellModel *model=data;
    FriendModle *tm=model.value;
    MyUserModle *user = [[MyUserModle alloc]initWithDic:tm.user];
    
    
    NSArray *arr=[tm.images componentsSeparatedByString:@","];
    
    [self.myAvatar setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:HeadImage];
    self.myAvatar.layer.cornerRadius = 25;
    self.myAvatar.layer.masksToBounds = YES;
    self.myAvatar.clipsToBounds = YES;
    self.myname.text=[NSString stringWithFormat:@"%@",user.name];
    self.mytime.text=[NSString stringWithFormat:@"%@",tm.pubTime];

    //将文本内容进行链接判断
    [self changeMyLabelTextLinkColorAndClicked:tm.content];
    
    [self.myreadnum setTitle:[NSString stringWithFormat:@"%@",tm.readNum] forState:UIControlStateNormal];
    [self.mylaidnum setTitle:[NSString stringWithFormat:@"%@",tm.laudNum] forState:UIControlStateNormal];
    [self.mycommentnum setTitle:[NSString stringWithFormat:@"%@",tm.commentNum] forState:UIControlStateNormal];
    
    for (UIView *vview in self.bgView.subviews) {
        [vview removeFromSuperview];
    }
    
    int sps=5;
    int ViewWidth = kScreenWidth-30-15;
    
 
     viewArr=[NSMutableArray array];
    
    for (int i=0; i<[arr count]; i++) {
        
        UIImageView * imageView= [[UIImageView alloc]initWithFrame:CGRectMake(i%3*(ViewWidth/3+sps)+sps, i/3*(ViewWidth/3+sps), ViewWidth/3, ViewWidth/3.0)];
        
         __weak typeof (UIImageView)*weakImageView = imageView;//【这样写就ok了】；防止下面的循环引用
        

        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:arr[i]]];
        [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        
        //几张图片连续动画加载
        UIImage *imagee = [UIImage animatedImageNamed:@"加载中aa" duration:0.75];
        
        
        UIImageView *imageA = [[UIImageView alloc]initWithImage:imagee];
//        imageA.backgroundColor = [UIColor redColor];
        imageA.frame = CGRectMake(ViewWidth/6 -10, ViewWidth/6 - 10, 20, 20);
        [imageView addSubview:imageA];
        
        //为图片添加加载失败的图片
        [weakImageView setImageWithURLRequest:request placeholderImage:PlaceHold success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
//            UIActivityIndicatorView *iView = [weakImageView viewWithTag:1000];
            //imageView.image = image;   //这句话要提示循环引用，故而用下面一句
                 weakImageView.image=image;
            [imageA removeFromSuperview];

            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
            weakImageView.image=[UIImage imageNamed:@"加载失败2"];
             [imageA removeFromSuperview];

            
        }];
        
        weakImageView.userInteractionEnabled=YES;
        
        [weakImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewClick:)]];
        
        
        [self.bgView addSubview:weakImageView];
        
        [viewArr addObject:weakImageView];
        
        if (i==[arr count]-1) {
            self.height.constant=CGRectGetMaxY(imageView.frame); //CGRectGetMaxY 获取Y坐标的值以及高度的值
            [self layoutIfNeeded];
        }
    }
    
    if ([arr count]==0) {
        self.height.constant=0;
        
        [self layoutIfNeeded];
    }


}
-(void)imageViewClick:(UITapGestureRecognizer*)tap{
         
    MBPhotoBrowser *mbp=[[MBPhotoBrowser alloc]init];
    mbp.viewArr=viewArr;
   [mbp Show:(UIImageView *)tap.view];
    
}
- (void)changeMyLabelTextLinkColorAndClicked:(NSString *)text_label_text
{
    UIFont *font = [UIFont systemFontOfSize:13];
    NSDictionary *attributes = @{NSFontAttributeName: font};
    NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:text_label_text attributes:attributes];
    self.mycontent0.textColor = [UIColor whiteColor];
    self.mycontent0.attributedText = attributedString;
    self.mycontent0.linkColor = [UIColor lightGrayColor];
    self.mycontent0.linkHighlightColor = [UIColor blueColor];
   
    self.mycontent0.linkTapHandler = ^(KZLinkType linkType, NSString *string, NSRange range){
        if (linkType == KZLinkTypeURL) {
            [self openURL:[NSURL URLWithString:string]];
        } else if (linkType == KZLinkTypePhoneNumber) {
            [self openTel:string];
        } else {
            NSLog(@"Other Link");
        }
    };
    self.mycontent0.linkLongPressHandler = ^(KZLinkType linkType, NSString *string, NSRange range){
        NSMutableDictionary *linkDictionary = [NSMutableDictionary dictionaryWithCapacity:3];
        [linkDictionary setObject:@(linkType) forKey:@"linkType"];
        [linkDictionary setObject:string forKey:@"link"];
        [linkDictionary setObject:[NSValue valueWithRange:range] forKey:@"range"];
        self.selectedLinkDic = linkDictionary;
        
        NSString *openTypeString;
        if (linkType == KZLinkTypeURL) {
            openTypeString = @"在Safari中打开";
        } else if (linkType == KZLinkTypePhoneNumber) {
            openTypeString = @"直接拨打";
        }
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"取消"
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:@"拷贝",openTypeString, nil];
        [sheet showInView:self];
    };
  
}

//下面的几个方法都为链接的这个
- (BOOL)openURL:(NSURL *)url
{
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
        return YES;
    } else {
        return NO;
    }
}
- (BOOL)openTel:(NSString *)tel
{
//    [[[FBAlertView alloc]initWithTitle:@"是否拨打该电话？" butnTitlt:@"拨打" block:^(id data) {
//        NSString *telString = [NSString stringWithFormat:@"tel://%@",tel];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString]];
//        
//    }] show];
    NSString *telString = [NSString stringWithFormat:@"tel://%@",tel];
    return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telString]];
  
}
#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (!self.selectedLinkDic) {
        return;
    }
    switch (buttonIndex)
    {
        case 0:
        {
            [UIPasteboard generalPasteboard].string = self.selectedLinkDic[@"link"];
            break;
        }
        case 1:
        {
            KZLinkType linkType = [self.selectedLinkDic[@"linkType"] integerValue];
            if (linkType == KZLinkTypeURL) {
                NSURL *url = [NSURL URLWithString:self.selectedLinkDic[@"link"]];
                [self openURL:url];
            } else if (linkType == KZLinkTypePhoneNumber) {
                [self openTel:self.selectedLinkDic[@"link"]];
                
            }
            break;
        }
    }
}




@end
