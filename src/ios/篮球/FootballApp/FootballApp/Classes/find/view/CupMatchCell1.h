//
//  CupMatchCell1.h
//  FootballApp
//
//  Created by zj on 16/6/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "FriendModle.h"
@interface CupMatchCell1 : BaseCell
{
    NSMutableArray *viewArr;
    
}
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@property (weak, nonatomic) IBOutlet UIImageView *myAvatar;
@property (weak, nonatomic) IBOutlet UILabel *myname;
@property (weak, nonatomic) IBOutlet UILabel *mytime;
@property (weak, nonatomic) IBOutlet UILabel *mycontent;
@property (weak, nonatomic) IBOutlet UIButton *myreadnum;
@property (weak, nonatomic) IBOutlet UIButton *mylaidnum;

@property (weak, nonatomic) IBOutlet UIButton *mycommentnum;

@property(nonatomic,strong) FriendModle *friend1;
@property(nonatomic,strong) MyUserModle *userInfo1;


@end
