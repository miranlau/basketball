//
//  CupMatchCell1.m
//  FootballApp
//
//  Created by zj on 16/6/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CupMatchCell1.h"
#import "MBPhotoBrowser.h"
#import "FriendModle.h"
@implementation CupMatchCell1

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews
{
//    BaseCellModel *model=data;
//    FriendModle *tm=model.value;
//    MyUserModle *user = [[MyUserModle alloc]initWithDic:tm.user];
    
//    [self performSelector:@selector(getMydata) withObject:nil afterDelay:1.5];
    [self getMydata];
}
- (void)getMydata
{
    NSArray *arr=[self.friend1.images componentsSeparatedByString:@","];
    
    [self.myAvatar setImageWithURL:[NSURL URLWithString:self.userInfo1.avatar] placeholderImage:HeadImage];
    self.myAvatar.layer.cornerRadius = 25;
    self.myAvatar.layer.masksToBounds = YES;
    self.myAvatar.clipsToBounds = YES;
    self.myname.text=[NSString stringWithFormat:@"%@",self.userInfo1.name];
    self.mytime.text=[NSString stringWithFormat:@"%@",self.friend1.pubTime];
    self.mycontent.text = self.friend1.content;
    
    
    
    [self.myreadnum setTitle:[NSString stringWithFormat:@"%@",self.friend1.readNum] forState:UIControlStateNormal];
    [self.mylaidnum setTitle:[NSString stringWithFormat:@"%@",self.friend1.laudNum] forState:UIControlStateNormal];
    [self.mycommentnum setTitle:[NSString stringWithFormat:@"%@",self.friend1.commentNum] forState:UIControlStateNormal];
    
    for (UIView *vview in self.bgView.subviews) {
        [vview removeFromSuperview];
    }
    
    int sps=5;
    int ViewWidth=kScreenWidth-30-15;
    
    
    viewArr=[NSMutableArray array];
    
    for (int i=0; i<[arr count]; i++) {
        
        UIImageView * imageView= [[UIImageView alloc]initWithFrame:CGRectMake(i%3*(ViewWidth/3+sps)+sps, i/3*(ViewWidth/3+sps), ViewWidth/3, ViewWidth/3.0)];
        
        
        
        [imageView setImageWithURL:[NSURL URLWithString:arr[i]] placeholderImage:HeadImage];
        
        imageView.userInteractionEnabled=YES;
        
        [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewClick:)]];
        
        
        [self.bgView addSubview:imageView];
        
        
        
        
        [viewArr addObject:imageView];
        
        
        
        if (i==[arr count]-1) {
            self.height.constant=CGRectGetMaxY(imageView.frame);
            
            [self layoutIfNeeded];
        }
    }

}

-(void)imageViewClick:(UITapGestureRecognizer*)tap{
    
    MBPhotoBrowser *mbp=[[MBPhotoBrowser alloc]init];
    mbp.viewArr=viewArr;
    [mbp Show:(UIImageView *)tap.view];
    
}


@end
