//
//  RaceEventCell.h
//  FootballApp
//
//  Created by zj on 16/5/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

@interface RaceEventCell : BaseCell

@property (weak, nonatomic) IBOutlet UIImageView *myImage;
@property (weak, nonatomic) IBOutlet UILabel *myName;
@property (weak, nonatomic) IBOutlet UILabel *myTime;
@property (weak, nonatomic) IBOutlet UILabel *myArea;
@property (weak, nonatomic) IBOutlet UILabel *myNumber;
@property (weak, nonatomic) IBOutlet UILabel *myStatus;


@end
