//
//  RaceEventCell.m
//  FootballApp
//
//  Created by zj on 16/5/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "RaceEventCell.h"
#import "RaceEventModel.h"
#import "TeamHelper.h"
@implementation RaceEventCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)getdata:(id)data{
    
    BaseCellModel *model=data;
    RaceEventModel *tm=model.value;
    [self.myImage setImageWithURL:[NSURL URLWithString:tm.image] placeholderImage:[UIImage imageNamed:@"banner.png"]];
    self.myName.text=[NSString stringWithFormat:@"%@",tm.name];
    
    NSArray *arraybegin = [tm.begindate componentsSeparatedByString:@" "];
    NSArray *arrayend   = [tm.enddate   componentsSeparatedByString:@" "];
    self.myTime.text=[NSString stringWithFormat:@"%@ 至 %@",arraybegin[0],arrayend[0]];
    self.myArea.text=[NSString stringWithFormat:@"%@",tm.areaname];
    
    self.myNumber.text=[NSString stringWithFormat:@"%@人制",tm.matchtype];
    self.myStatus.layer.cornerRadius = 3;
    self.myStatus.layer.masksToBounds = YES;
    self.myStatus.clipsToBounds = YES;
    if([tm.status intValue] == 0)
    {
         self.myStatus.text=[NSString stringWithFormat:@"报名中"];
         self.myStatus.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:151.0/255.0 blue:56.0/255.0 alpha:1.0];
    }
    else if ([tm.status intValue] == 1)
    {
         self.myStatus.text=[NSString stringWithFormat:@"报名结束"];
         self.myStatus.backgroundColor = [UIColor colorWithRed:23.0/255.0 green:154.0/255.0 blue:181.0/255.0 alpha:1.0];
    }
    else if ([tm.status intValue] == 2)
    {
        self.myStatus.text=[NSString stringWithFormat:@"比赛中"];
         self.myStatus.backgroundColor = [COLOR colorWithHexString:@"1c7a25"];
    }
    else
    {
         self.myStatus.text=[NSString stringWithFormat:@"比赛结束"];
         self.myStatus.backgroundColor = [UIColor colorWithRed:45.0/255.0 green:74.0/255.0 blue:89.0/255.0 alpha:1.0];
    }
  
    [self layoutIfNeeded];
}
@end
