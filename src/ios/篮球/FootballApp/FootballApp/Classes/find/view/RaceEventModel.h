//
//  RaceEventModel.h
//  FootballApp
//
//  Created by zj on 16/5/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface RaceEventModel : BaseModel
//赛事
@property(copy,nonatomic)NSString* areacode;
@property(copy,nonatomic)NSString* areaname;
@property(copy,nonatomic)NSString* begindate;
@property(copy,nonatomic)NSString* deadline;
@property(copy,nonatomic)NSString* details;

@property(copy,nonatomic)NSString* enddate;
@property(copy,nonatomic)NSString* follows;
@property(copy,nonatomic)NSString* id;
@property(copy,nonatomic)NSString* image;
@property(copy,nonatomic)NSString* matchtype;

@property(copy,nonatomic)NSString* name;
@property(copy,nonatomic)NSString* status;
@property(copy,nonatomic)NSString* teams;
@property(assign,nonatomic)BOOL gauntlet;
@property(assign,nonatomic)BOOL isfollow;

@end
