//
//  TeamCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/11.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "MBStarScore.h"
@interface TeamCell : BaseCell

@property (weak, nonatomic) IBOutlet UIImageView *teamLogo;
@property (weak, nonatomic) IBOutlet UILabel *teamName;
@property (weak, nonatomic) IBOutlet MBStarScore *teamSorce;
@property (weak, nonatomic) IBOutlet UILabel *teamRecord;
@property (weak, nonatomic) IBOutlet UILabel *teamLevel;
@property (weak, nonatomic) IBOutlet UILabel *teamAge;





@end
