//
//  TeamCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/11.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamCell.h"
#import "TeamModel.h"
@implementation TeamCell

-(void)getdata:(id)data{

    BaseCellModel *model=data;
    TeamModel *tm=model.value;
    
    [self.teamLogo setImageWithURL:[NSURL URLWithString:tm.logo] placeholderImage:TeamLogo];
    
    self.teamLogo.layer.cornerRadius=74/2;
    self.teamLogo.layer.masksToBounds=YES;
    
    self.teamLogo.layer.borderColor=[UIColor whiteColor].CGColor;
    self.teamLogo.layer.borderWidth=1;
    
    self.teamAge.text=[NSString stringWithFormat:@"平均年龄:%@",tm.age];
    self.teamName.text=tm.name;
    
    self.teamSorce.show_star= [tm.stars intValue]/20;
    
    
    self.teamRecord.text=[NSString stringWithFormat:@"战绩:%@胜%@负",tm.win,tm.lose];
    
    
    self.teamLevel.text=[NSString stringWithFormat:@" %@级球队 ",tm.level];
    self.teamLevel.layer.cornerRadius=2;
    self.teamLevel.layer.masksToBounds=YES;
    
    
    
   
    
    

}
- (IBAction)yesBtnClick:(id)sender {
    
    
    if(self.cellBlock){
    
        self.cellBlock(@"Yes");
    
    }
    
}

@end
