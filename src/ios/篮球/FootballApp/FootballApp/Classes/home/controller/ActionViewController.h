//
//  ActionViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/29.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_OPTIONS(NSUInteger, ActionType) {
    
    ACFoul = 1000,//犯规
    ACOffside,//越位
    ACYellowCard,//黄牌
    ACRedCard,//红牌
    ACFighting,//扑救
    ACRescue,//解围
    ACSteals,//抢断
    ACIntercept,//拦截
    ACShoot,//射门
    ACChange,//换人
    TheFirst//首发
    
    
};

typedef NS_OPTIONS(NSUInteger, TagType) {
    TTDetermine = 1000 ,//确定
    TTCancel,//取消
    TTPositiveDirection,//射正
    TTUnPositiveDirection,//射偏
    TTOolong,//乌龙
    TTGoal,//进球
    TTNoHelp, //无助攻
    TTDian//进球
    
};





@interface ActionViewController : BaseViewController


@property(assign,nonatomic)ActionType actionType;

@property(strong ,nonatomic)NSString *matchId;
@property(strong ,nonatomic)NSString *teamId;



@end


