//
//  ActionViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/29.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ActionViewController.h"
#import "MenberView.h"
#import "MatchHelper.h"
#import "PlayerModel.h"
#import "PlayerView.h"
#import "MatchModel.h"
#import "XHToast.h"



@interface ActionViewController ()
{
    
    __weak IBOutlet UIView *rightView;
    
    MatchModel *match;
    MenberView *menberView;
    
    
    NSString *playerA;
    NSString *playerB;//助攻人员
    
    
    NSMutableArray *fristList;//场上人员表
    
    
    PlayerView *selectPlayer;//当前选中的球员
    
    PlayerView *selectPlayer2;//助攻球员
    
    BOOL isGoal;//是否进球
    
    PlayerView *lastView;
    PlayerView *lastView2;
    
    NSString *leader;
    
    BOOL isDian;
    
    
    
    
}
@end

@implementation ActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //因为屏幕还没有转过来
    if (_actionType!=TheFirst) {
        
        
        
        
        
        [self loadViewFromType:_actionType];
        
        [self loadLocation];
        
        
        if (_actionType==ACChange) {
            
            [self loadOtherPeople];
        }
        
    }else{
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissToRoot) name:@"dismissToRoot" object:nil];
        
        
        NSString *sss=[MatchHelper shareMatch].match.home[@"id"];
        if ([_teamId intValue]==[sss intValue]) {
            
            leader=[MatchHelper shareMatch].match.home[@"leader"];
        }else{
            leader=[MatchHelper shareMatch].match.visiting[@"leader"];
        }
        [self loadAllmenber];
    }
    
    
    
    
}
-(void)dismissToRoot{
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissToRoot2" object:nil];
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:NO];
    
    if (_actionType==TheFirst) {
        [self loadViewFromType:_actionType];
        
        
        
    }
    
}


#pragma mark 加载场上人员和位置信息
-(void)loadLocation{
    
    
    
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.statring) {
        PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
        
        PlayerView *player=[[PlayerView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        player.play=play;
        player.center=[self locationfromPosition:CGPointMake([play.x doubleValue] , [play.y doubleValue])];
        
        player.isSelect=YES;
        [player selectBtnClick:^(id data) {
            
            
            if(_actionType !=ACShoot&& _actionType !=ACChange){
                
                
                playerA=player.play.id;
                selectPlayer=player;
                lastView=player;
                
                playerB=nil;
                [self playerClick];
                
                
                
            }else{
                
                //是否点击了进球
                if (!isGoal) {
                    
                    if (player != selectPlayer) {
                        
                        if (lastView){
                            
                            lastView.isSelected=NO;
                        }
                        
                        
                        playerA=player.play.id;
                        selectPlayer=player;
                        lastView=player;
                        
                    }else{
                        playerA=nil;
                        selectPlayer=nil;
                        lastView=nil;
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }else{
                    
                    
                    
                    
                    if (player != selectPlayer2) {
                        
                        if (lastView2 && playerB) {
                            
                            lastView2.isSelected=NO;
                        }
                        playerB=player.play.id;
                        selectPlayer2=player;
                        
                        lastView2=selectPlayer2;
                        
                    }else{
                        
                        playerB=nil;
                        selectPlayer2=nil;
                        lastView2=nil;
                        
                    }
                    
                    
                    
                    
                }
                
            }
            
            
            
            
            
            
            
            
            
            
        }];
        
        
        
        
        [self.view addSubview:player];
        
    }
    //判断当前队伍是主队还是客队
    
    
    
    
}

#pragma mark 加载场下人员信息
-(void)loadOtherPeople{
    
    
    
    [self.dataArr removeAllObjects];
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.players) {
        PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
        [self.dataArr addObject:play];
        
    }
    
    
    if ([self.dataArr count]>0) {
        
        if (menberView!=nil) {
            menberView.list=self.dataArr;
        }
        
    }
    
    
    
    
}
//加载赛前确认过的球员
-(void)loadAllmenber{
    
    
    [HttpRequestHelper postWithURL:Url(@"game/queryAll") params:@{@"teamId":_teamId,@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess)
        {
            [self.dataArr removeAllObjects];
            
            
            for (NSDictionary *dic  in responseObject) {
                PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
                [self.dataArr addObject:play];
                
            }
            
            
            if ([self.dataArr count]>0) {
                
                if (menberView!=nil) {
                    menberView.list=self.dataArr;
                }
                
            }
            
            
        }
        else
        {
            
            [XHToast showBottomWithText:responseObject];
            
            
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
    
    
    [HttpRequestHelper postWithURL:Url(@"match/queryById") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            match=[[MatchModel alloc]initWithDic:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
    
    
}


@synthesize actionType=_actionType;

-(void)setActionType:(ActionType)actionType{
    
    _actionType=actionType;
    
    
    
}
#pragma mark 从位置计算坐标
-(CGPoint)locationfromPosition:(CGPoint)position{
    
    //视图大小320*320
    
    double top=(kScreenHeight-320)/2.0;
    CGFloat width=320/25.0;
    CGPoint point=CGPointMake(position.x *width, position.y*width+top);
    
    return point;
    
}
#pragma mark 从坐标计算位置
-(CGPoint)positionFromloction:(CGPoint)point{
    
    //视图大小320*320
    
    double top=(kScreenHeight-320)/2;
    CGFloat width=320/25.0;
    
    int x=point.x/width;
    int y=(point.y-top)/width;
    return CGPointMake(x, y);
    
}

-(void)loadViewFromType:(ActionType)actionType{
    
    
    if (actionType==ACOffside||actionType==ACYellowCard||actionType==ACRedCard||actionType==ACFighting||actionType==ACRescue||actionType==ACSteals||actionType==ACIntercept||actionType==ACShoot||actionType==ACFoul) {
        
        //不是射门
        if (actionType!=ACShoot) {
            
            
            [self addButn:[BaseCode getBtnList1]];
            
            
        }else{
            
            [self addButn:[BaseCode getBtnList2]];
            
            
        }
        
        
        
    }else{
        
        
        
        
        
        menberView =[[MenberView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth-280, kScreenHeight) isgest:_actionType == TheFirst?YES:NO  rowBlock:^(id data) {
            
            //点击某一行的回调
            
            PlayerModel *model=data;
            
            //            selectPlayer.play=model;
            
            
            playerB=model.id;
            
            
        } panBlock:^(id data) {
            
            //手势回调
            
            PlayerView *headImage=data;
            
            CGRect oldframe=[headImage convertRect:headImage.bounds toView:self.view];
            
            headImage.frame=oldframe;
            
            [headImage removeFromSuperview];
            PlayerView *playerView=[[PlayerView alloc ]initWithFrame:oldframe];
            playerView.play=headImage.play;
            playerView.isSelect=NO;
            [playerView deleteBtnBlock:^(id data) {
                
                [self.dataArr addObject:data];
                menberView.list=self.dataArr;
                [playerView removeFromSuperview];
                [fristList removeObject:playerView];
                
                
            }];
            
            
            [self.view addSubview:playerView];
            
            
            if (!fristList) {
                fristList =[NSMutableArray array];
                
            }
            
            [fristList addObject:playerView];
            
        }];
        __weak ActionViewController *weakSelf = self;
        
        [menberView cancelBlock:^(id data) {
            
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }];
        
        menberView.leader=leader;
        
        //如果是确认首发
        if(actionType==TheFirst){
            
            
            
            [menberView setTitle:@"首发阵容选择" doneBlock:^(id data) {
                
                if ([fristList count]==0) {
                    
                    
                    [XHToast showBottomWithText:@"请选择首发人员"];
                    return ;
                    
                }else{
                    
                    if ([fristList count]>[match.type  intValue]) {
                        
                        [XHToast showBottomWithText:@"超过允许上场人数"];
                        
                        return ;
                        
                    }else{
                        
                        
                        [[[FBAlertView alloc]initWithTitle:@"确认首发完毕？" butnTitlt:@"完毕" block:^(id data) {
                            
                            
                            NSMutableDictionary *dic =[NSMutableDictionary dictionary];
                            
                            [dic setValue:weakSelf.matchId forKey:@"matchId"];
                            
                            [dic setValue:weakSelf.teamId forKey:@"teamId"];
                            
                            NSMutableArray *arr=[ weakSelf getLocation];
                            
                            NSData *jsonData = [NSJSONSerialization
                                                dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
                            
                            
                            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                            
                            
                            
                            
                            
                            
                            [dic setObject:jsonString forKey:@"display"];
                            
                            
                            [HttpRequestHelper postWithURL:Url(@"game/starting") params:dic success:^(BOOL sucess, id responseObject) {
                                
                                if(sucess){
                                    
                                    
                                    [MatchHelper MatchInfo:@{@"matchId":dic[@"matchId"],@"teamId":dic[@"teamId"]} success:^(id data) {
                                        
                                        
                                        UIViewController *controller=[weakSelf.storyboard instantiateViewControllerWithIdentifier:@"OnLiveViewController"];
                                        [controller setValue:weakSelf.matchId forKey:@"matchId"];
                                        
                                        [controller setValue:weakSelf.teamId forKey:@"teamId"];
                                        
                                        [controller setValue:@YES forKey:@"fromFristView"];
                                        
                                        [weakSelf presentViewController:controller animated:YES completion:nil];
                                        
                                    } failure:^(id data) {
                                        
                                    }];
                                    
                                    
                                    
                                    
                                    
                                }else{
                                    
                                    
                                    [XHToast showBottomWithText:responseObject];
                                    
                                }
                                
                                
                            } failure:^(NSError *error) {
                                
                                
                                
                                
                                
                            }];
                            
                        }] show];
                        
                    }
                }
                
                
            }];
            if ([self.dataArr count]>0) {
                menberView.list=self.dataArr;
            }
            
            
            
        }else{
            
            
            __weak ActionViewController *weakSelf = self;
            
            [menberView setTitle:@"换人" doneBlock:^(id data) {
                
                
                
                
                if ([playerA intValue]<=0) {
                    
                    [XHToast showBottomWithText:@"请先选择需要被换下的球员"];
                    
                    return ;
                }
                
                if ([playerB intValue]<=0) {
                    
                    
                    [XHToast showBottomWithText:@"请先选择需要被换上的球员"];
                    return ;
                }
                
                NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:playerB status:@"22" matchId:weakSelf.matchId teamId:weakSelf.teamId];
                
                [[[FBAlertView alloc]initWithTitle:@"确认换人完毕？" butnTitlt:@"完毕" block:^(id data) {
                    
                    
                    
                    
                    
                    [MatchHelper matchActionRequest:dic success:^(id data) {
                        
                        [MatchHelper MatchInfo:dic success:^(id data4) {
                            
                            
                            UIViewController *controller=[weakSelf.storyboard instantiateViewControllerWithIdentifier:@"OnLiveViewController"];
                            [controller setValue:weakSelf.matchId forKey:@"matchId"];
                            
                            [controller setValue:weakSelf.teamId forKey:@"teamId"];
                            
                            [self dismissViewControllerAnimated:NO completion:nil];
                            
                        } failure:^(id data2) {
                            [XHToast showBottomWithText:data2];
                        }];
                        
                    } failure:^(id data) {
                        
                        
                        
                        [XHToast showBottomWithText:data];
                    }];
                    
                    
                    
                    
                    
                    
                    
                    
                }] show];
                
                
                
            }];
            
            
            if ([self.dataArr count]>0) {
                menberView.list=self.dataArr;
            }
            
        }
        
        
        
        [rightView addSubview:menberView];
        
        menberView.hidden=NO;
        
    }
    
}
#pragma mark 获取首发人员位置
-(NSMutableArray *)getLocation{
    NSMutableArray *arr=[NSMutableArray array];
    
    
    for(PlayerView *view in fristList){
        NSMutableDictionary *dic =[NSMutableDictionary dictionary];
        
        
        [dic setObject:view.play.id forKey:@"id"];
        
        CGPoint point = [self positionFromloction:view.center];
        [dic setObject:@(point.x) forKey:@"x"];
        [dic setObject:@(point.y) forKey:@"y"];
        [dic setObject:view.play.avatar forKey:@"avatar"];
        [dic setObject:view.play.number forKey:@"number"];
        
        [arr addObject:dic];
        
        
        
    }
    
    
    return arr;
    
    
    
    
}
-(void)addButn:(NSArray *)arr{
    
    for (UIView *view  in rightView.subviews) {
        if (![view isKindOfClass:[MenberView class]]) {
            [view removeFromSuperview];
        }
    }
    
    menberView.hidden=YES;
    
    
    int btnH=50;
    
    int x=20;
    
    int spece=(kScreenHeight-[arr count] *btnH)/([arr count]+1);
    int btnW=(kScreenWidth-320-40);
    
    
    
    for (int i=0; i<[arr count]; i++) {
        
        
        
        
        
        UIButton *butn=[[UIButton alloc]initWithFrame:CGRectMake(x, i*(spece+btnH)+spece, btnW, btnH)];
        
        if ([arr count]==6) {
            
            if (i==3) {
                
                butn.frame= CGRectMake(x, i*(spece+btnH)+spece, btnW/2-10, btnH);
            }
            
            if (i==4) {
                
                butn.frame=CGRectMake(x+btnW/2+10, (i-1)*(spece+btnH)+spece, btnW/2-10, btnH);
            }
            
            
        }
        
        
        BaseCode *code=arr[i];
        
        butn.titleLabel.font=[UIFont systemFontOfSize:14];
        [butn setTitle:code.title forState:0];
        
        butn.layer.cornerRadius=5;
        butn.layer.masksToBounds=YES;
        
        [butn setTitleColor:[UIColor whiteColor] forState:0];
        butn.tag=code.tagType;
        if (code.tagType == TTCancel || code.tagType == TTNoHelp||code.tagType == 1009) {
            
            
            [butn setBackgroundColor:[COLOR colorWithHexString:@"e74459"]];
            
            
        }else{
            
            [butn setBackgroundColor:[COLOR colorWithHexString:@"3e87b4"]];
            
            
        }
        
        [butn addTarget:self action:@selector(butncllick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [rightView addSubview:butn];
    }
    
}

-(void)playerClick{
    
    NSString *status=nil;
    switch (_actionType) {
        case ACFoul:
            status=@"44";
            break;
        case ACOffside:
            status=@"41";
            break;
        case ACYellowCard:
            status=@"43";
            break;
        case ACRedCard:
            status=@"42";
            break;
        case ACFighting:
            status=@"29";
            break;
        case ACRescue:
            status=@"28";
            break;
        case ACSteals:
            status=@"31";
            break;
        case ACIntercept:
            status=@"27";
            break;
        case ACChange:
            status=@"22";
            break;
            
        case ACShoot:
            status=@"33";
            break;
            
        default:
            break;
    }
    
    
    
    NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:playerB status:status matchId:_matchId teamId:_teamId];
    
    [MatchHelper matchActionRequest:dic success:^(id data) {
        
        
        
        if (self.cblock) {
            self.cblock(@"");
        }
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    } failure:^(id data) {
        
        
        
        [XHToast showTopWithText:data];
        
        
    }];
    
}

-(void)butncllick:(UIButton *)butn{
    
    NSString *status=nil;
    switch (_actionType) {
        case ACFoul:
            status=@"44";
            break;
        case ACOffside:
            status=@"41";
            break;
        case ACYellowCard:
            status=@"43";
            break;
        case ACRedCard:
            status=@"42";
            break;
        case ACFighting:
            status=@"29";
            break;
        case ACRescue:
            status=@"28";
            break;
        case ACSteals:
            status=@"31";
            break;
        case ACIntercept:
            status=@"27";
            break;
        case ACChange:
            status=@"22";
            break;
            
        case ACShoot:
            status=@"33";
            break;
            
        default:
            break;
    }
    
    if (butn.tag!=TTGoal && butn.tag!=TTDian) {
        
        if ([butn.titleLabel.text isEqualToString:@"确定"]||[butn.titleLabel.text isEqualToString:@"助攻"]) {
            
            NSString *play=[NSString stringWithFormat:@"%@",playerA];
            
          
            
            if (_actionType == ACYellowCard||_actionType == ACRedCard) {
                if (!play || [play isEqualToString:@""]||[play isEqualToString:@"(null)"]) {
                    [SVProgressHUD showErrorWithStatus:@"请选择球员"];
                    return;
                }
            }
            
            BOOL help=NO;
            if([butn.titleLabel.text isEqualToString:@"助攻"]&&_actionType==ACShoot){
                
                NSString *play2=[NSString stringWithFormat:@"%@",playerB];
                if (!play2 || [play2 isEqualToString:@""]||[play2 isEqualToString:@"(null)"]) {
                    playerB=@"-1";
                    help=YES;
                }
                
            }
            
            
            
            //
            //            //如果是射门的确定
            //            if (_actionType==ACShoot) {
            //
            //                NSString *play2=[NSString stringWithFormat:@"%@",playerB];
            //                if (!play2 || [play2 isEqualToString:@""]||[play2 isEqualToString:@"(null)"]) {
            //                    [SVProgressHUD showErrorWithStatus:@"请选择助攻球员"];
            //                    return;
            //                }
            //
            //            }
            
            
            NSString *string=@"";
            
            if ([butn.currentTitle isEqualToString:@"助攻"]) {
                string=@"进球有助攻";
            }
            
            if ([butn.currentTitle isEqualToString:@"无助攻"]) {
                string=@"进球无助攻";
            }
            
            if ((_actionType!=ACShoot&&_actionType!=ACChange&&_actionType!=TheFirst)||help==YES){
                
                [[[FBAlertView alloc]initWithTitle:@"是否未看清球员" butnTitlt:@"确定" block:^(id data) {
                    
                    NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:playerB status:status matchId:_matchId teamId:_teamId];
                    
                    [MatchHelper matchActionRequest:dic success:^(id data) {
                        
                        
                        
                        
                        if (self.cblock) {
                            self.cblock(string);
                        }
                        
                        [self dismissViewControllerAnimated:NO completion:nil];
                        
                    } failure:^(id data) {
                        
                        
                        
                        [XHToast showTopWithText:data];
                        
                        
                    }];
                    
                }]show];
                
                
                
            }else{
                
                
                
                
                NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:playerB status:status matchId:_matchId teamId:_teamId];
                
                [MatchHelper matchActionRequest:dic success:^(id data) {
                    
                    
                    
                    if (self.cblock) {
                        self.cblock(string);
                    }
                    
                    [self dismissViewControllerAnimated:NO completion:nil];
                    
                } failure:^(id data) {
                    
                    
                    
                    [XHToast showTopWithText:data];
                    
                    
                }];
                
            }
            
            
        }else{
            
            if ([butn.titleLabel.text isEqualToString:@"取消"]) {
                
                
                
                
                if (butn.tag == 1009) {
                    
                    [self addButn:[BaseCode getBtnList2]];
                    
                    [self.view addSubview:selectPlayer];
                    
                    return;
                }else{
                    
                    
                    [self dismissViewControllerAnimated:NO completion:nil];
                    return;
                }
            }
            
            if ([butn.titleLabel.text isEqualToString:@"射正未进"]) {
                
                status=@"32";
                
            }
            
            if ([butn.titleLabel.text isEqualToString:@"射偏"]) {
                
//                if (butn.tag==1009) {
//                    
//                    status=@"39";
//                    
//                }else{
                    status=@"34";
//                    
//                }
                
                
                
                
            }
            
            
            
            if ([butn.titleLabel.text isEqualToString:@"罚进"]||[butn.titleLabel.text isEqualToString:@"射进"]) {
                status=@"38";
            }
            
            if ([butn.titleLabel.text isEqualToString:@"罚丢"]) {
                status=@"39";
            }
            
            if ([butn.titleLabel.text isEqualToString:@"乌龙球"]) {
 
              status=@"23";
 
                
            }
            
            
            //            NSString *play=[NSString stringWithFormat:@"%@",playerA];
            //
            //
            //            if (!play || [play isEqualToString:@""]||[play isEqualToString:@"(null)"]) {
            //                [SVProgressHUD showErrorWithStatus:@"请选择球员"];
            //                return;
            //            }
            
            if (isDian) {
                
                NSString *play=[NSString stringWithFormat:@"%@",playerA];
                
                
                if (!play || [play isEqualToString:@""]||[play isEqualToString:@"(null)"]) {
                    [SVProgressHUD showErrorWithStatus:@"请选择球员"];
                    return;
                }
                
                
                
                
                
                
            }
            
            //这改过
            if ([butn.titleLabel.text isEqualToString:@"乌龙球"]||[butn.titleLabel.text isEqualToString:@"射正未进"]||[butn.titleLabel.text isEqualToString:@"射偏"]) {
                
               
                
                 NSString *play=[NSString stringWithFormat:@"%@",playerA];
                if (!play || [play isEqualToString:@""]||[play isEqualToString:@"(null)"]) {
                    [[[FBAlertView alloc]initWithTitle:@"是否未看清球员" butnTitlt:@"确定" block:^(id data) {
                        NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:nil status:status matchId:_matchId teamId:_teamId];
                        
                        [MatchHelper matchActionRequest:dic success:^(id data) {
                            
                            
                            if (_actionType==ACShoot) {
                                
                                NSString *string=butn.titleLabel.text;
                                
                                if ([string isEqualToString:@"无助攻"]) {
                                    string=@"进球无助攻";
                                }
                                
                                if([status isEqualToString:@"38"]){
                                    string =@"点球罚进";
                                    
                                }
                                
                                if([status isEqualToString:@"39"]){
                                    string =@"点球罚丢";
                                    
                                }
                                
                                
                                
                                if (self.cblock) {
                                    self.cblock(string);
                                }
                            }
                            
                            [self dismissViewControllerAnimated:NO completion:nil];
                            
                        } failure:^(id data) {
                            
                            
                            
                            [XHToast showBottomWithText:data];
                            
                            
                        }];
                    }]show];
                }else{
                
                    NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:nil status:status matchId:_matchId teamId:_teamId];
                    
                    [MatchHelper matchActionRequest:dic success:^(id data) {
                        
                        
                        if (_actionType==ACShoot) {
                            
                            NSString *string=butn.titleLabel.text;
                            
                            if ([string isEqualToString:@"无助攻"]) {
                                string=@"进球无助攻";
                            }
                            
                            if([status isEqualToString:@"38"]){
                                string =@"点球罚进";
                                
                            }
                            
                            if([status isEqualToString:@"39"]){
                                string =@"点球罚丢";
                                
                            }
                            
                            
                            
                            if (self.cblock) {
                                self.cblock(string);
                            }
                        }
                        
                        [self dismissViewControllerAnimated:NO completion:nil];
                        
                    } failure:^(id data) {
                        
                        
                        
                        [XHToast showBottomWithText:data];
                        
                        
                    }];
                }
                
            }else{
            
                NSMutableDictionary *dic=[MatchHelper playerA:playerA playerB:nil status:status matchId:_matchId teamId:_teamId];
                
                [MatchHelper matchActionRequest:dic success:^(id data) {
                    
                    
                    if (_actionType==ACShoot) {
                        
                        NSString *string=butn.titleLabel.text;
                        
                        if ([string isEqualToString:@"无助攻"]) {
                            string=@"进球无助攻";
                        }
                        
                        if([status isEqualToString:@"38"]){
                            string =@"点球罚进";
                            
                        }
                        
                        if([status isEqualToString:@"39"]){
                            string =@"点球罚丢";
                            
                        }
                        
                        
                        
                        if (self.cblock) {
                            self.cblock(string);
                        }
                    }
                    
                    [self dismissViewControllerAnimated:NO completion:nil];
                    
                } failure:^(id data) {
                    
                    
                    
                    [XHToast showBottomWithText:data];
                    
                    
                }];
            
            
            
            }
            
            
            
           
            
            
            
            
            
            
        }
        
        
        
        
        
    }
    
    else{
        
        //            NSString *play=[NSString stringWithFormat:@"%@",playerA];
        //
        //
        //            if (!play || [play isEqualToString:@""]||[play isEqualToString:@"(null)"]) {
        //                [SVProgressHUD showErrorWithStatus:@"请选择球员"];
        //                return;
        //            }
        
        
        if(butn.tag==TTGoal){
            if (selectPlayer) {
                PlayerModel *goldPlay=selectPlayer.play;
                
                selectPlayer.isSelected=NO;
                
                playerA=goldPlay.id;
            }
            
            
            
            //有进球人员
            
            if (playerA) {
                isGoal=YES;
                [self addButn:[BaseCode getBtnList3]];
            }else{
                [[[FBAlertView alloc]initWithTitle:@"是否未看清球员" butnTitlt:@"确定" block:^(id data) {
                    isGoal=YES;
                    [self addButn:[BaseCode getBtnList3]];
                }]show];
            }
            
            
            
          
            
            
            
            
            
        }
        
        if (butn.tag==TTDian) {
            
//            if (playerA) {
                isDian=YES;
                
                [self addButn:[BaseCode getBtnList4]];
//            }else{
//                [[[FBAlertView alloc]initWithTitle:@"是否未看清球员" butnTitlt:@"确定" block:^(id data) {
//                    isDian=YES;
//                    
//                    [self addButn:[BaseCode getBtnList4]];
//                }]show];
//            }
            
           
            
        }
        
        
        
        
        
    }
    
}



-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersStatusBarHidden
{
    
    
    return YES;
}
@end
