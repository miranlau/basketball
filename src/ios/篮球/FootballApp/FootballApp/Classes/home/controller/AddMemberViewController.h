//
//  AddMemberViewController.h
//  FootballApp
//
//  Created by zj on 16/5/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface AddMemberViewController : BaseViewController
@property(nonatomic,copy)NSString *teamId;
@property(nonatomic,copy)NSString *matchId;
@end
