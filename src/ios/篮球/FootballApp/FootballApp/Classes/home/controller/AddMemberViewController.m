//
//  AddMemberViewController.m
//  FootballApp
//
//  Created by zj on 16/5/25.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AddMemberViewController.h"
#import "MatchModel1.h"
#import "MenberCell.h"
@interface AddMemberViewController ()
{
    __weak IBOutlet UITableView *AddMemberTableview;
 
   
    
    NSMutableArray *list;
    NSMutableArray *listID;
    BOOL BStatus;
    NSString *memberID;
    UIButton *right;
}
@property(nonatomic,copy)NSString *modleid;
@property (nonatomic, strong) NSMutableArray *selectArray;//记录选择的所有选项
@end

@implementation AddMemberViewController
/**
 *  懒加载
 */
- (NSMutableArray *)selectArray {
    if (!_selectArray) {
        _selectArray = [NSMutableArray array];
    }
    return _selectArray;
}

//当我选择了球队后，不想再添加球员，那么直接返回上一界面，需要删除选择的数据
- (void)viewWillDisappear:(BOOL)animated
{
    if (list.count > 0)
    {
        
        for(int i = 0; i < listID.count; i++)
        {
            [Defaults removeObjectForKey:listID[i]];
        }
    }
}
//数组去重
- (NSMutableArray *)arrayWithMemberIsOnly:(NSArray *)array
{
    NSMutableArray *categoryArray = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < [array count]; i++)
    {
      
        if (![categoryArray containsObject:array[i]])
        {
             [categoryArray addObject:array[i]];
        }
    }
                                     
    return categoryArray;
}

//全选功能
- (void)selectedAllMembers
{
  
    if (right.tag == 100)
    {
   
        NSMutableArray *array0 = [NSMutableArray arrayWithCapacity:0];
        for (int i = 0; i < self.dataArr.count; i++)
        {
            BaseCellModel *modelB=self.dataArr[i];
            MatchModel1 *modle = modelB.value;
            NSString *IDString = [NSString stringWithFormat:@"%@",modle.id];
        
            [array0 addObject:IDString];
     
        }
            self.selectArray = [self arrayWithMemberIsOnly:array0];
        
            right.tag = 101;
            [right setTitle:@"取消" forState:UIControlStateNormal];
            [AddMemberTableview reloadData];
 
    }
    else
    {
         right.tag = 100;
        [right setTitle:@"全选" forState:UIControlStateNormal];
        
        for (int i = 0; i < self.dataArr.count; i++)
        {
            BaseCellModel *modelB=self.dataArr[i];
            MatchModel1 *modle = modelB.value;
            NSString *IDString = [NSString stringWithFormat:@"%@",modle.id];
            if ([self.selectArray containsObject:IDString])
            {
                [self.selectArray removeObject:IDString];
            }
            
        }
        [AddMemberTableview reloadData];

    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//     self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"全选" style:UIBarButtonItemStylePlain target:self action:@selector(selectedAllMembers)];
    
    right = [UIButton buttonWithType:UIButtonTypeCustom];
    right.frame = CGRectMake(0, 0, 50, 50);
    [right addTarget:self action:@selector(selectedAllMembers) forControlEvents:UIControlEventTouchUpInside];
    [right setTitle:@"全选" forState:UIControlStateNormal];
    right.tag = 100;
    right.titleLabel.font = [UIFont systemFontOfSize:15];
    [right setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightBut = [[UIBarButtonItem alloc]initWithCustomView:right];
    self.navigationItem.rightBarButtonItem = rightBut;
    
    BStatus = YES;

     list=[[NSMutableArray alloc]initWithCapacity:0];
      listID=[[NSMutableArray alloc]initWithCapacity:0];
    
    [self loadTableView];
    [self loadnetUrlInfo];
    
   
    
}
- (void)loadnetUrlInfo
{


    [HttpRequestHelper postWithURL:Url(@"game/queryOthers") params:@{@"teamId":self.teamId,@"matchId":self.matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess)
        {
            [self.dataArr removeAllObjects];
            NSArray *array = responseObject;
            for (int i = 0; i < array.count; i++)
            {
              
                MatchModel1 *model=[[MatchModel1 alloc]initWithDic:array[i]];
                [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MenberCell"]];
            }
            
            [AddMemberTableview.mj_header endRefreshing];
            [AddMemberTableview.mj_footer endRefreshing];
            [AddMemberTableview reloadData];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
        [AddMemberTableview.mj_header endRefreshing];
        [AddMemberTableview.mj_footer endRefreshing];
    }];
}
-(void)loadTableView{

    AddMemberTableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    AddMemberTableview.rowHeight=UITableViewAutomaticDimension;
    
    AddMemberTableview.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    AddMemberTableview.backgroundView=bgImageView;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BaseCellModel *modelB=self.dataArr[indexPath.row];
    MatchModel1 *modle = modelB.value;
    MenberCell *cell=[tableView dequeueReusableCellWithIdentifier:modelB.reuseIdentifier forIndexPath:indexPath];

    if (indexPath.row%2) {
        
        
        
        cell.backgroundColor=BGCOLOR1;
        
        
    }else{
        cell.backgroundColor=BGCOLOR2;
    }
    
    [cell.headImageView setImageWithURL:[NSURL URLWithString:modle.avatar] placeholderImage:HeadImage];
    cell.headImageView.layer.cornerRadius = 76 / 2;
    cell.headImageView.layer.masksToBounds = YES;
    cell.name.text = modle.name;
    if (modle.realName) {
        cell.name.text = modle.realName;
    }
    
    
    cell.num.text = [NSString stringWithFormat:@"球衣号码:%@",modle.number];
    
    NSString *IDString = [NSString stringWithFormat:@"%@",modle.id];
    if ([self.selectArray containsObject:IDString]) {
        
        cell.button.selected = YES;
        cell.widthC.constant = 20;
        cell.heigH.constant  = 35;
    }else {
        
        cell.button.selected = NO;
    }
    
    //给队长的成员加上旗子
    
        cell.leaderImage.hidden=YES;
    
        if([modle.isLeader intValue] == 1)
        {
            cell.leaderImage.hidden=NO;
        }
    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     MenberCell *cell =[tableView cellForRowAtIndexPath:indexPath];

        BaseCellModel *modelB=self.dataArr[indexPath.row];
        MatchModel1 *modle = modelB.value;

        NSString *IDString = [NSString stringWithFormat:@"%@",modle.id];
   
    if ([self.selectArray containsObject:IDString]) {
        [self.selectArray removeObject:IDString];
        
    }else {
        [self.selectArray addObject:IDString];
    }
    cell.widthC.constant = 20;
    cell.heigH.constant  = 35;
//    [AddMemberTableview reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    
    [tableView reloadData];

    
}

- (IBAction)addMemberConfirmClicked:(id)sender {
    
    if (self.selectArray.count > 0)
    {
        NSString *str0 = Url(@"game/addPlayer");
        NSString *str1 = [NSString stringWithFormat:@"?teamId=%@&matchId=%@",self.teamId,self.matchId];
        NSString *str2 =  [str0 stringByAppendingString:str1];
        NSString *str3 = [self.selectArray componentsJoinedByString:@"&userId="];
        NSString *str4 = [NSString stringWithFormat:@"&userId=%@",str3];
        NSString *str5 = [str2 stringByAppendingString:str4];
   
        [HttpRequestHelper getWithURL:str5 params:nil success:^(BOOL sucess, id responseObject) {
            
            if (sucess)
                {
                        //返回上一个页面并刷新上一页；
                        [self.navigationController popViewControllerAnimated:YES];
                        [AddMemberTableview reloadData];
                    }
                    else
                        {
                        [SVProgressHUD showErrorWithStatus:responseObject];
                }
            
        } failure:^(NSError *error) {
           
            [AddMemberTableview.mj_header endRefreshing];
            [AddMemberTableview.mj_footer endRefreshing];
            
        }];
        
    }
    else
    {
         [SVProgressHUD showErrorWithStatus:@"您还未选择球员"];
    }
}



@end
