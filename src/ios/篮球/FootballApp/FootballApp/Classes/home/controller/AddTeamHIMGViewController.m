//
//  AddTeamHIMGViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AddTeamHIMGViewController.h"
#import "TeamHelper.h"
#import "OnePhotoSelecter.h"
@interface AddTeamHIMGViewController (){


    __weak IBOutlet UIImageView *headImageView;

    __weak IBOutlet UIButton *button1;
    __weak IBOutlet UIButton *button;
}

@end

@implementation AddTeamHIMGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    button1.layer.cornerRadius = 4;
    button1.layer.masksToBounds = YES;
    self.navigationItem.title=@"全网篮球";
    
    [self setHeadImageView];
    
}

-(void)setHeadImageView{

    headImageView.layer.cornerRadius=70.0;
    headImageView.layer.masksToBounds=YES;
    headImageView.layer.borderColor=[UIColor whiteColor].CGColor;
    headImageView.layer.borderWidth=1;
    [headImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addImage:)]];
    
    
    if ([TeamHelper shareTeam].logo) {
        [headImageView setImageWithURL:[NSURL URLWithString:[TeamHelper shareTeam].logo] placeholderImage:[UIImage imageNamed:@"添加头像.png"]];
    }
    


}
-(void)addImage:(id)sender{
    
    [[OnePhotoSelecter shareInstance] selectImage:self imageBlock:^(id data) {
    
        headImageView.image=data;
        
        [HttpRequestHelper postWithURL:Url(@"resource/upload") params:@{@"type":@"team"} formDataArray:@[data] success:^(BOOL sucess, id responseObject1) {
            
            
            if (sucess) {

                [TeamHelper shareTeam].logo=responseObject1[0];
                
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject1];
                
            }
            
            
        } failure:^(NSError *error) {
            
            
        }];

        
    
    
    }];
    



}

- (IBAction)nextBtnClick:(id)sender {
    
    
    
    if (![TeamHelper shareTeam].logo) {
        [SVProgressHUD showErrorWithStatus:@"球队图片不能为空"];
        return;
    }
    
    
    UIViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamPwdViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
  
}
-(IBAction)lastBtnClick:(id)sender{
    
   [TeamHelper shareTeam].logo=@"";

    UIViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamPwdViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
