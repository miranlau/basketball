//
//  AddTeamNameViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AddTeamNameViewController.h"
#import "TeamHelper.h"
@interface AddTeamNameViewController ()<UITextFieldDelegate>
{


    __weak IBOutlet UIButton *button;
    __weak IBOutlet UITextField *teamName;

}

@end

@implementation AddTeamNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    [teamName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
   
    [teamName becomeFirstResponder];
}
-(void)textFieldDidChange:(UITextField *)textfield{
    
    
    
    if (textfield.text.length > 16) {
        textfield.text = [textfield.text substringToIndex:16];
    }
    
    
    
}

- (IBAction)nextBtnClick:(id)sender {
    
    if ([teamName.text isEqualToString:@""]) {
         [SVProgressHUD showErrorWithStatus:@"请输入球队名"];
        return;
        
        
    }
    
    [TeamHelper shareTeam].name=teamName.text;
    
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamHIMGViewController"];
    
    
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

@end
