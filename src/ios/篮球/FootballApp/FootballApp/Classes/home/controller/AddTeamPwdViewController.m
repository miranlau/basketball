//
//  AddTeamPwdViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AddTeamPwdViewController.h"

#import "TeamHelper.h"
@interface AddTeamPwdViewController ()
{

    __weak IBOutlet UITextField *password;

}
@end

@implementation AddTeamPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"全网篮球";
    
      [password becomeFirstResponder];
}

- (IBAction)creatBtnClick:(id)sender {
    
    
    if ([password.text isEqualToString:@""]) {
        
     [SVProgressHUD showErrorWithStatus:@"请输入球队暗号"];
        
        return;
    }
    
    
    [TeamHelper shareTeam].password=password.text;
    
    [TeamHelper creatTeam:^(id data) {
        
         [SVProgressHUD showSuccessWithStatus:@"创建球队成功"];
        [TeamHelper shareTeam].logo=nil;
        
        [self loadPlayerData:^(id data) {
            
            [self performSelector:@selector(back) withObject:nil afterDelay:2];
        }];
        
        
        
       
        
    } failure:^(id data) {
        
         [SVProgressHUD showErrorWithStatus:@"创建球队失败"];
        
    }];
    
}
-(void)back{
    
    [self.navigationController popToRootViewControllerAnimated:YES];


}
@end
