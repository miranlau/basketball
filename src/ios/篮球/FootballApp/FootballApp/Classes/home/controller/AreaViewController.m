//
//  AreaViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/18.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AreaViewController.h"

@interface AreaViewController()
{

    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UITextField *minTextFiled;
    __weak IBOutlet UITextField *maxTextFiled;

}

@end
@implementation AreaViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    
    [minTextFiled becomeFirstResponder];
    
    [minTextFiled addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    
    [maxTextFiled addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    if ([self.navigationItem.title isEqualToString:@"对手战斗力"]) {
       titleLabel.text=@"选择对手战斗力区间";
        if (_value && ![_value isEqualToString:@""]) {
            NSArray *array = [_value componentsSeparatedByString:@"-"];
            minTextFiled.text=array[0];
            maxTextFiled.text=array[1];
        }
        
        
        
    }else{
        titleLabel.text=@"选择对手年龄区间";
        if (_value&& ![_value isEqualToString:@""]) {
            NSArray *array = [_value componentsSeparatedByString:@"-"];
            minTextFiled.text=array[0];
            maxTextFiled.text=array[1];
        }
    
    }
    
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnClick:)];
    

}
-(void)textChange:(UITextField *)textFile{
    
    if (textFile == minTextFiled) {
        
        if ([self.navigationItem.title isEqualToString:@"对手战斗力"]) {
            
            if (textFile.text.length >3) {
                 textFile.text = [textFile.text substringToIndex:3];
            }
            
           
            
        }else{
        
            if (textFile.text.length >2) {
                textFile.text = [textFile.text substringToIndex:2];
            }
        
        }
        
    }else{
    
        if ([self.navigationItem.title isEqualToString:@"对手战斗力"]) {
            
            if (textFile.text.length >3) {
                textFile.text = [textFile.text substringToIndex:3];
            }
            
            
            
        }else{
            
            if (textFile.text.length >2) {
                textFile.text = [textFile.text substringToIndex:2];
            }
            
        }
    
    }



}
-(void)saveBtnClick:(id)sender{
    
    [self.view endEditing:YES];
    
    
    if ([minTextFiled.text isEqualToString:@""]) {
        
        [SVProgressHUD showErrorWithStatus:@"请选择最小值"];
        return;
        
    }
    if ([maxTextFiled.text isEqualToString:@""]) {
         [SVProgressHUD showErrorWithStatus:@"请选择最大值"];
        return;
    }
    
    if ([maxTextFiled.text intValue]<[minTextFiled.text intValue]) {
        
        [SVProgressHUD showErrorWithStatus:@"最大值不能小于最小值"];
        return;
    }
    if ([self.navigationItem.title isEqualToString:@"对手战斗力"]){
        
        
        
        if ([maxTextFiled.text intValue] >100||[minTextFiled.text intValue]>100) {
             [SVProgressHUD showErrorWithStatus:@"战斗力范围1-100"];
             return;
        }
        
       
    
    }else{
    
        if ([maxTextFiled.text intValue] >60||[minTextFiled.text intValue]>60) {
            [SVProgressHUD showErrorWithStatus:@"年龄范围13-60"];
             return;
        }
        
        if ([maxTextFiled.text intValue] <13||[minTextFiled.text intValue]<13) {
            [SVProgressHUD showErrorWithStatus:@"年龄范围13-60"];
             return;
        }
    
    
    }
    
    NSString *data=[minTextFiled.text stringByAppendingFormat:@"-%@",maxTextFiled.text];
    
   
    
    self.cblock(data);
    
    [self.navigationController popViewControllerAnimated:YES];


}
@end
