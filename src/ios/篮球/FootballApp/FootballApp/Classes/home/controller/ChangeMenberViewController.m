//
//  ChangeMenberViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChangeMenberViewController.h"
#import "TeamHelper.h"
#import "PlayerModel.h"
#import "MenberCell.h"
#import "LoadingView.h"
@interface ChangeMenberViewController ()
{

    __weak IBOutlet UITableView *menberTableView;

}
@end

@implementation ChangeMenberViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
   
    [self loadTableView];
}

-(void)loadTableView{
    
    
    
    menberTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    menberTableView.rowHeight=UITableViewAutomaticDimension;
    
    menberTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    menberTableView.backgroundView=bgImageView;
    
}
-(void)reloadTableView{
    
    [menberTableView reloadData];
    [[LoadingView shareView] stop];

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.dataArr count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MenberCell" forIndexPath:indexPath];
    
    if (indexPath.row%2) {
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    [cell setValue:self.dataArr[indexPath.row] forKey:@"data"];
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MenberCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    
    
    PlayerModel *model =[[PlayerModel alloc]initWithDic:self.dataArr[indexPath.row]];
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    [dic setObject:_teamId forKey:@"teamId"];
     [dic setObject:model.id forKey:@"userId"];
    
    BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamNameViewController"];
    controller.navigationItem.title=@"修改球员号码";
    
    
    controller.dataDic=dic;
    
    [controller setControllerBlock:^(id data) {
        
        [[LoadingView shareView] start];
        
        if (self.cblock) {
            
              self.cblock(@"");
        }
      
        

       
        
    }];
    
    
    [controller setValue:@"3" forKey:@"type"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
    
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath

{
    
    return  UITableViewCellEditingStyleDelete;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
    }
}
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //设置删除按钮
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction*action,NSIndexPath *indexPath) {
         PlayerModel *model =[[PlayerModel alloc]initWithDic:self.dataArr[indexPath.row]];
        
        if([model.id intValue]==[[AccountHelper shareAccount].player.id intValue]){
            
         [SVProgressHUD showErrorWithStatus:@"不能踢出自己"];
            
            return ;
        
        }
        [[[FBAlertView alloc]initWithTitle:@"是否移出该成员" butnTitlt:@"确定" block:^(id data) {
            
            
            [SVProgressHUD showWithStatus:@"移出中..."];

            [TeamHelper quitTeam:@{@"teamId":_teamId,@"userId":model.id} success:^(id data) {
                
                [SVProgressHUD showSuccessWithStatus:@"踢人成功"];
                
                [self.dataArr removeObjectAtIndex:indexPath.row];
                
                [menberTableView beginUpdates];
                
                [menberTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                
                [menberTableView endUpdates];
                
                
                if (self.cblock) {
                    self.cblock(@"");
                }
                
                
                
            } failure:^(id data) {
                
                [SVProgressHUD showErrorWithStatus:data];
                
                
            }];
            
        }]show];
        
       
        
    }];
    
   
    
    
    
    return  @[deleteRowAction];
}


@end
