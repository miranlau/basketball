//
//  ChangePlayerNumber.h
//  FootballApp
//
//  Created by zj on 16/5/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface ChangePlayerNumber : BaseViewController
@property(nonatomic,copy)NSString *teamId;
@property(nonatomic,copy)NSString *matchId;
@property(nonatomic,copy)NSString *userId;
@end
