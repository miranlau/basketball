//
//  ChangePlayerNumber.m
//  FootballApp
//
//  Created by zj on 16/5/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChangePlayerNumber.h"
#import "TeamHelper.h"
@interface ChangePlayerNumber ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *myNumber;
@property (weak, nonatomic) IBOutlet UIButton *myConfirm;

@end

@implementation ChangePlayerNumber

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myNumber.delegate = self;
    self.myConfirm.layer.cornerRadius = 4;
    self.myConfirm.layer.masksToBounds = YES;
 
}
- (IBAction)ConfirmChangeClicked:(id)sender {
        if([self.myNumber.text isEqualToString:@""] || [self.myNumber.text intValue] > 99){
            [SVProgressHUD showErrorWithStatus:@"请输入0-99球衣号"];
            return;
        }
    
    [TeamHelper changeNum:@{@"teamId":self.teamId,@"number":self.myNumber.text,@"userId":self.userId} success:^(id data) {
        self.cblock(self.myNumber.text);
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(id data) {

    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    return [self validateNumber:string];
//}
//这里只能输数字
- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.myNumber) {
        if (string.length == 0) return YES;
        
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if (existedLength - selectedLength + replaceLength > 2) {
            return NO;
        }
    }
    
    return [self validateNumber:string];
}
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.myNumber) {
        if (textField.text.length > 2) {
            textField.text = [textField.text substringToIndex:2];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
