//
//  ChangeViewController.h
//  FootballApp
//
//  Created by North on 16/6/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface ChangeViewController : BaseViewController
@property(strong ,nonatomic)NSString *matchId;
@property(strong ,nonatomic)NSString *teamId;

@property(assign,nonatomic)BOOL isChange;//是否换人

@end
