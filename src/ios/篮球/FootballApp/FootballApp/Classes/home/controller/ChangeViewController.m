//
//  ChangeViewController.m
//  FootballApp
//
//  Created by North on 16/6/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChangeViewController.h"
#import "MatchHelper.h"
#import "MenberView.h"
#import "PlayersView.h"
#import "XHToast.h"
#import "MatchModel.h"
#define bgWight 280
#define bgHeight 320

@interface ChangeViewController ()<UIGestureRecognizerDelegate>
{

    __weak IBOutlet UITableView *menberTableView;

    __weak IBOutlet UIButton *doneBtn;
    __weak IBOutlet UILabel *typeLabel;
    

    NSMutableArray *otherList;//替补队员列表
    NSMutableArray *other2List;//替补队员Copy
    NSMutableArray *fristLocationList;//首发队员在父视图中的view列表
    NSMutableArray *fristList;//首发人员列表
    
    PlayersView *playView;
    NSIndexPath * _indexPath;
    
    NSString *leader;
    
    MatchModel *match;

}
@end

@implementation ChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //tableView 自动适应行高
    menberTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    menberTableView.rowHeight=UITableViewAutomaticDimension;
    menberTableView.estimatedRowHeight=10;
    ViewRadius(doneBtn, 5);
    
    
    
    if (_isChange) {
        typeLabel.text=@"换人";
        
        //如果是首发就加载人员到场上
        
        [self loadFristList];
        
    }else{
        
        typeLabel.text=@"首发阵容选择";
        
        NSString *sss=[MatchHelper shareMatch].match.home[@"id"];
        if ([_teamId intValue]==[sss intValue]) {
            
            leader=[MatchHelper shareMatch].match.home[@"leader"];
        }else{
            leader=[MatchHelper shareMatch].match.visiting[@"leader"];
        }
        [self loadOtherList];
        
    }
    //给tableView增加拖拽手势
        
      UIPanGestureRecognizer * pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(cellPanAction:)];
        pan.delegate=self;
    [menberTableView addGestureRecognizer:pan];
    
    
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}
#pragma mark 拖拽手势
-(void)cellPanAction:(UIPanGestureRecognizer *)pan{
    
    
     CGPoint pointInSelf = [pan locationInView:self.view];//在父视图中的位置
     CGPoint pointIntableView = [pan locationInView:menberTableView];//在tableView中的位置
    
    

    
    if (pan.state == UIGestureRecognizerStateBegan) {
        
        playView =[[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        
        _indexPath = [menberTableView indexPathForRowAtPoint:pointIntableView];
        
        if (!_indexPath|| _indexPath.row+1 >[otherList count]) {
            _indexPath=nil;
             playView=nil;
            
            return;
        }
        
        
        
        
        PlayerModel *model=otherList[_indexPath.row];
        
        playView.play=model;
        
        playView.center=pointInSelf;
        
        
        [playView addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(viewPanAction:)]];

        
        [self.view addSubview:playView];
        
        
    }else if (pan.state == UIGestureRecognizerStateChanged) {
        
        
        
        //设置移动边界
       CGRect bgRect1=   CGRectMake(0, (kScreenHeight-320)/2.0, 280, 320);
        CGRect bgRect2=   CGRectMake(320, 0, kScreenWidth, kScreenHeight);
        
        //如果还在在右边视图内移动
        if (CGRectContainsPoint(bgRect2, pointInSelf)) {
            playView.center=pointInSelf;
        }else{
            
            //还在在左边视图内移动
            if (CGRectContainsPoint(bgRect1, pointInSelf)) {
                playView.center=pointInSelf;
                
            }else{
                
                return;
            }
        }
        
        
        

        
    }else if (pan.state == UIGestureRecognizerStateEnded) {
        
        if (!playView) {
            return;
        }
        //左边背景
        CGRect bgRect1=   CGRectMake(0, (kScreenHeight-320)/2.0, 280, 320);
        //如果在左边视图内
        if (CGRectContainsPoint(bgRect1, pointInSelf)) {
 
            //判断有没有覆盖场上人员列表
            PlayersView *downView=nil;
            
            for(PlayersView *view in fristLocationList){
                //如果本次拖拽的视图中心在存在的场上人员的坐标范围系统内
                if (CGRectContainsPoint(view.frame, playView.center)&&view !=pan.view){
                    
                    
                    //将模型增加到数组中
                    
                    NSInteger index=[otherList count];
                    
                    if (view.play) {
                        [otherList insertObject:view.play atIndex:index];
                        
                        
                        [menberTableView beginUpdates];
                        [menberTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        [menberTableView endUpdates];
                        
                      
                        downView=view;
                        break;
                    }

                }
            
            }
            
            if (downView){
                [downView removeFromSuperview];
                [fristLocationList removeObject:downView];
            }
            
            
            
            if (_indexPath) {
                
                //增加到场上人员视图列表里面
                
                if(!fristLocationList){
                    fristLocationList =[NSMutableArray array];
                }
                
                [fristLocationList addObject:playView];
                //从替补阵容中删除
                [otherList removeObject:otherList[_indexPath.row]];
                
                [menberTableView beginUpdates];
                [menberTableView deleteRowsAtIndexPaths:@[_indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [menberTableView endUpdates];
            }
            
            
            
          
            
            //判断有没有重叠 有就代表替换
  
            
        }else{
            
           [playView removeFromSuperview];
            playView=nil;
           _indexPath=nil;
        
        }
        
       
        
        
        
        
    }
    


}
 -(void)viewPanAction:(UIPanGestureRecognizer *)pan{
     
       CGPoint pointInSelf = [pan locationInView:self.view];//在父视图中的位置
     //左边背景
       CGRect bgRect1=   CGRectMake(0, (kScreenHeight-320)/2.0, 280, 320);
     if (pan.state==UIGestureRecognizerStateChanged) {
         
         pan.view.center=pointInSelf;
         
         
         
     }else if(pan.state==UIGestureRecognizerStateEnded){
     
         //左边背景
       
         //如果在左边视图内
         if (CGRectContainsPoint(bgRect1, pan.view.center)){
             
             
             
//             //判断场上人员数量有没有被删除干净
//             NSMutableArray *list=[NSMutableArray array];
//             
//             int count=0;
//             
//             for (UIView *view in self.view.subviews) {
//                 
//                 if ([view isKindOfClass:[PlayersView class]]&&view.hidden==NO) {
//                     count++;
//                     [list addObject:view];
//                 }
//                 
//             }
//             
//             if (count!=[fristLocationList count]) {
//                 
//                 fristLocationList=list;
//             }
             
             //判断有没有覆盖场上人员列表
             PlayersView *downView=nil;
             
             
             
             
             for(PlayersView *view in fristLocationList){
                 //如果本次拖拽的视图中心在存在的场上人员的坐标范围系统内
                 if (CGRectContainsPoint(view.frame, pan.view.center)&&view !=pan.view){
                     
                     
                     //将模型增加到数组中
                     
                     NSInteger index=[otherList count];
                     
                     [otherList insertObject:view.play atIndex:index];
                     
                     
                     [menberTableView beginUpdates];
                     [menberTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                     [menberTableView endUpdates];
                     
                    
                     downView=view;
                     break;
                     
                 }
                 
             }
             
             if (downView) {
                 [downView removeFromSuperview];
                 [fristLocationList removeObject:downView];
             }

         }else{
             
             
             PlayersView *view=(PlayersView *)pan.view;
             
             //将模型增加到数组中
             
             NSInteger index=[otherList count];
             
             [otherList insertObject:view.play atIndex:index];
             
             
             [menberTableView beginUpdates];
             [menberTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
             [menberTableView endUpdates];
             
             [fristLocationList removeObject:pan.view];
             [view removeFromSuperview];
             
         }
       
         
     }
         
         
 }
//取消按钮
- (IBAction)cancelBtnClick:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
//完成按钮选择
- (IBAction)doneBtnCllick:(id)sender {
    
    //判断场上人员数量有没有被删除干净
    NSMutableArray *list=[NSMutableArray array];
    
    int count=0;
    
    for (UIView *view in self.view.subviews) {
        
        if ([view isKindOfClass:[PlayersView class]]&&view.hidden==NO) {
            count++;
            [list addObject:view];
        }
        
    }

    fristLocationList=list;
    
    //是换人
    if (_isChange) {
        
        if ([fristLocationList count]==0) {
            [XHToast showBottomWithText:@"请选择上场人员"];
            return;
        }
        
        
        if ([fristLocationList count]>[match.type  intValue]) {
            
            [XHToast showBottomWithText:@"超过允许上场人数"];
            
            return ;
            
        }
        
        [[[FBAlertView alloc]initWithTitle:@"确认换人完毕？" butnTitlt:@"完毕" block:^(id data) {
            
            
            NSMutableDictionary *dic =[NSMutableDictionary dictionary];
            
            [dic setValue:self.matchId forKey:@"matchId"];
            
            [dic setValue:self.teamId forKey:@"teamId"];
            
            NSMutableArray *arr=[ self getLocation];
            
            NSData *jsonData = [NSJSONSerialization
                                dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
            
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            [dic setObject:jsonString forKey:@"display"];
            
            
            [HttpRequestHelper postWithURL:Url(@"game/change") params:dic success:^(BOOL sucess, id responseObject) {
                
                if(sucess){
                    
                    
                    if(self.cblock){
                        
                        self.cblock(@"换人");
                    }
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    
                }else{
                    
                    
                    [XHToast showBottomWithText:responseObject];
                    
                }
                
                
            } failure:^(NSError *error) {
   
                
            }];
            
        }] show];
        
        
    }else{
    //首发
        
        if ([fristLocationList count]==0) {
             [XHToast showBottomWithText:@"请选择首发人员"];
            return;
        }
        
        
        if ([fristLocationList count]>[match.type  intValue]) {
            
            [XHToast showBottomWithText:@"超过允许上场人数"];
            
            return ;
            
        }
        
        
        [[[FBAlertView alloc]initWithTitle:@"确认首发完毕？" butnTitlt:@"完毕" block:^(id data) {
            
            
            NSMutableDictionary *dic =[NSMutableDictionary dictionary];
            
            [dic setValue:self.matchId forKey:@"matchId"];
            
            [dic setValue:self.teamId forKey:@"teamId"];
            
            NSMutableArray *arr=[ self getLocation];
            
            NSData *jsonData = [NSJSONSerialization
                                dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
            
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            
            
            
            
            
            [dic setObject:jsonString forKey:@"display"];
            
            
            [HttpRequestHelper postWithURL:Url(@"game/starting") params:dic success:^(BOOL sucess, id responseObject) {
                
                if(sucess){
                    
                    
                    [MatchHelper MatchInfo:@{@"matchId":dic[@"matchId"],@"teamId":dic[@"teamId"]} success:^(id data) {
                        
                        
                        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"OnLiveViewController"];
                        [controller setValue:self.matchId forKey:@"matchId"];
                        
                        [controller setValue:self.teamId forKey:@"teamId"];
                        
                        [controller setValue:@YES forKey:@"fromFristView"];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        
                    } failure:^(id data) {
                        
                    }];
                    
                    
                    
                    
                    
                }else{
                    
                    
                    [XHToast showBottomWithText:responseObject];
                    
                }
                
                
            } failure:^(NSError *error) {
                
                
                
                
                
            }];
            
        }] show];

    
    }
    
    
    
    
}
#pragma mark 获取首发人员位置
-(NSMutableArray *)getLocation{
    NSMutableArray *arr=[NSMutableArray array];
    
    
    for(PlayerView *view in fristLocationList){
        NSMutableDictionary *dic =[NSMutableDictionary dictionary];
        
        
        [dic setObject:view.play.id forKey:@"id"];
        
        CGPoint point = [self positionFromloction:view.center];
        [dic setObject:@(point.x) forKey:@"x"];
        [dic setObject:@(point.y) forKey:@"y"];
        [dic setObject:view.play.avatar forKey:@"avatar"];
        [dic setObject:view.play.number forKey:@"number"];
        [dic setValue:view.play.realName forKey:@"realName"];
        [dic setObject:view.play.name forKey:@"name"];
        
        [arr addObject:dic];
        
        
        
    }
    
    
    return arr;
    
    
    
    
}
#pragma mark 加载场上人员
-(void)loadFristList{
    
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.statring) {
        PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
        
        PlayersView *view = [[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        view.play=model;
        
        [view addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(viewPanAction:)]];
        
        view.center=[self locationfromPosition:CGPointMake([model.x doubleValue] , [model.y doubleValue])];
       
        [self.view addSubview:view];
        
        if (!fristLocationList) {
            fristLocationList =[NSMutableArray array];
        }
        
        [fristLocationList addObject:view];
        
        
       
        
    }
    
    
    
    otherList =[NSMutableArray array];
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.players) {
        PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
        [otherList addObject:play];
        
    }
    
    
    if ([self.dataArr count]>0) {
        
        [menberTableView reloadData];
        
    }
    
    

    [HttpRequestHelper postWithURL:Url(@"match/queryById") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            match=[[MatchModel alloc]initWithDic:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
    

}
#pragma mark 加载替补人员
-(void)loadOtherList{
    
    
    
    [HttpRequestHelper postWithURL:Url(@"game/queryAll") params:@{@"teamId":_teamId,@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess)
        {
            otherList=[NSMutableArray array];
            
            
            for (NSDictionary *dic  in responseObject) {
                PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
                [otherList addObject:play];
                
            }
            
            
             [menberTableView reloadData];
            
            
        }
        else
        {
            
            [XHToast showBottomWithText:responseObject];
            
            
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
    
    
    [HttpRequestHelper postWithURL:Url(@"match/queryById") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            match=[[MatchModel alloc]initWithDic:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
}

#pragma mark 从位置计算坐标
-(CGPoint)locationfromPosition:(CGPoint)position{
    
    //视图大小320*320
    
    double top=(kScreenHeight-320)/2.0;
    CGFloat width=320/25.0;
    CGPoint point=CGPointMake(position.x *width, position.y*width+top);
    
    return point;
    
}
#pragma mark 从坐标计算位置
-(CGPoint)positionFromloction:(CGPoint)point{
    
    //视图大小320*320
    
    double top=(kScreenHeight-320)/2;
    CGFloat width=320/25.0;
    int x=point.x/width;
    int y=(point.y-top)/width;
    return CGPointMake(x, y);
    
}
#pragma mark TableDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    return [otherList count];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MenberViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MenberViewCell" forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=indexPath.row%2==0?BGCOLOR1:BGCOLOR2;
    
    [cell setValue:leader forKey:@"leader"];
    PlayerModel *model=otherList[indexPath.row];
    [cell setValue:model forKey:@"data"];
    
    return cell;
    
    
}
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersStatusBarHidden
{
    
    
    return YES;
}
@end
