//
//  CreatMtchViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CreatMtchViewController.h"
#import "BaseCell.h"
#import "TeamModel.h"
#import "MBDatePicker.h"
#import "MatchInfoCell.h"




@interface CreatMtchViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIView *headView;
    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet UIImageView *line;
    __weak IBOutlet UIButton *fristBtn;
    __weak IBOutlet UIButton *secndBtn;
    UIButton *lastBtn;
    __weak IBOutlet UITableView *teamTableView;
    NSMutableDictionary *rowDic;
    
    __weak IBOutlet NSLayoutConstraint *top;
    
    
    __weak IBOutlet UIButton *buttonBtn;
    
    
    
    NSMutableArray *teams;
    
    TeamModel *team;
    
  
    NSString *date;//比赛时间
    NSString *type;//比赛人制
    NSString *typeString;
    NSString *areaCode;//地域编号6位
    NSString *power;//战斗力范围
    NSString *age;//年龄范围
    NSString *desc;//描述
    
    NSString *addr;
    NSString *latitude;
    NSString *longitude;
   
}

@end

@implementation CreatMtchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadHeadView];
    [self loadTableView];
    
     teams=[self getMyLeaderTeams];
    
    if (_match) {
        
        top.constant=-43;
        
        for (TeamModel *model in teams) {
            if ([_match.home[@"id"] intValue]==[model.id intValue]) {
                team=model;
                break;
            }
        }

        if (!team) {
            team=teams[0];
        }
        
        
        
        [buttonBtn setTitle:@"修改比赛" forState:0];
        date=_match.date;
        type=[NSString stringWithFormat:@"%@",_match.type];
        typeString=[NSString stringWithFormat:@"%@人制",_match.type];
        
       
            
            power=_match.power;
            age=_match.age;
            desc=_match.desc;
            addr=_match.addr;
            latitude=_match.latitude;
            longitude=_match.longitude;
        
       
        
    }else{
    
    team=teams[0];

     
        
    }
    
    rowDic=[self witchCellForRow:1001];
    
    
    [teamTableView reloadData];
        
}

-(void)loadHeadView{
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    headView.backgroundColor=BarColor;
    
   
    
    
    
}
-(void)loadTableView{
    
    rowDic =[NSMutableDictionary dictionary];
    
    teamTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    teamTableView.rowHeight=UITableViewAutomaticDimension;
    teamTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    teamTableView.backgroundView=bgImageView;
    
    
    
}


- (IBAction)headBtnClick:(UIButton *)butn {
    
    //防止点击多次
    if (butn==lastBtn) {
        return;
    }
    
    
    if (!_match) {
        
        date=@"";
        typeString=@"";
        type=@"";
        areaCode=@"";
        power=@"";
        age=@"";
        desc=@"";
    }
    
   
    
    
    lastBtn.selected=NO;
    butn.selected=!butn.selected;
    
    lastBtn=butn;
    
    
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/2*(butn.tag-1001);
        [line layoutIfNeeded];
        
        
    }];

    rowDic=[self witchCellForRow:1001];
    
    
    [teamTableView reloadData];
    
    
    
    
    
}
#pragma mark tableView 委托协议
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [[rowDic allKeys] count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return section==0?0:10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *view=[[UIView alloc]init];
    
    return view;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    [cell setValue:model forKey:@"data"];
    
    [((BaseCell *)cell) setCellBlock:^(id data) {
        
        
        if(!_match){
            
            if ([data isEqualToString:@"changeteam"]) {
                
                [[[TeamView alloc]initWithArr:teams clickBlock:^(id num) {
                    
                    NSInteger index=[num integerValue];
                    
                    team =teams[index];
                    model.value=team;
                    
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    
                    
                }]show];
                
            }
        
        }
        
        
 
        
    }];
    
   
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=indexPath.row%2==0?BGCOLOR1:BGCOLOR2;

    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    

    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    if ([model.name isEqualToString:@"比赛时间"]) {
        
      MBDatePicker *datePicker= [[MBDatePicker  alloc]initWithBlock:^(id data) {
            date=data;
            model.value=date;
            
            ((MatchInfoCell *)cell).cellInfo.text=date;
            
        }];
        [datePicker setMinDate:[NSDate date]];
        [datePicker setForMartString:@"yyyy-MM-dd HH:mm"];
        
        [datePicker show];
        
    }
    
    if ([model.name isEqualToString:@"比赛人制"]) {
        
        
        
        [self showSelectAlertView:^(id data) {
            
            typeString=data;
            model.value=typeString;
            
            ((MatchInfoCell *)cell).cellInfo.text=typeString;
       
            
        }];

   
   
    }
    if ([model.name isEqualToString:@"对手战斗力"]||[model.name isEqualToString:@"对手平均年龄"]) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"AreaViewController"];
        controller.navigationItem.title =model.name;
        [controller setControllerBlock:^(id data) {
            
            
            if([model.name isEqualToString:@"对手战斗力"]){
                power=data;
                model.value=power;
                ((MatchInfoCell *)cell).cellInfo.text=power;
                
            }else{
                
                age=data;
                model.value=age;
                ((MatchInfoCell *)cell).cellInfo.text=age;

            }

            
        }];
        
        
        if (model.value) {
            
            [controller setValue:model.value forKey:@"value"];
        }
        
        
       
        [self.navigationController pushViewController:controller animated:YES];

        
    }
    
    if ([model.name isEqualToString:@"比赛地点"]) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        
        [controller setControllerBlock:^(id data) {
           
                addr=data[@"addr"];
                latitude=data[@"latitude"];
                longitude=data[@"longitude"];
                model.value=addr;
                ((MatchInfoCell *)cell).cellInfo.text=addr;
                
        }];
        
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    if ([model.name isEqualToString:@"比赛留言"]) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"NoteViewController"];
        controller.navigationItem.title =model.name;
        [controller setControllerBlock:^(id data) {

                desc=data;
                model.value=desc;
                ((MatchInfoCell *)cell).cellInfo.text=desc;
            
             [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            

        }];
        if (model.value) {
            
            [controller setValue:model.value forKey:@"value"];
        }
        [self.navigationController pushViewController:controller animated:YES];
  
    }
    

}
- (IBAction)creatBtnClick:(UIButton *)sender {
    
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];

    NSString *service=@"";
    
    
    if (!team) {
        [SVProgressHUD showErrorWithStatus:@"请先选择球队"];
        return;
    }
    if (!date||[date isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"请选择比赛时间"];
        return;
    }
    if (!type||[type isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"请选择比赛人制"];
        return;
    }
    
    
    [dic setValue:team.id forKey:@"teamId"];
    [dic setValue:type forKey:@"type"];
    [dic setValue:[NSString stringWithFormat:@"%@:00",date] forKey:@"date"];
   
    
    
  
        service=Url(@"match/createMatching");

        
        if (!addr||[addr isEqualToString:@""]) {
             [SVProgressHUD showErrorWithStatus:@"请选择比赛地点"];
            return;
        }
       
        [dic setValue:addr forKey:@"addr"];
        [dic setValue:latitude forKey:@"latitude"];
        [dic setValue:longitude forKey:@"longitude"];
        [dic setValue:power forKey:@"power"];
        [dic setValue:age forKey:@"age"];
        [dic setValue:desc forKey:@"desc"];
        
    
    
    
    if (_match) {
        [dic setValue:_match.id forKey:@"matchId"];
        service=Url(@"match/modify");
    }
    
    
    [HttpRequestHelper postWithURL:service params:dic success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            self.cblock(@"");
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
        
         [SVProgressHUD showErrorWithStatus:responseObject];
        
        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
    
    
    
}



-(NSMutableDictionary *)witchCellForRow:(NSInteger)tag{
    
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    if (tag==1001) {
        
        
        
        
        
        
        [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                  
                                                  [BaseCellModel setModel:@"" key:_match?@"0":@"1" value:team image:@"" reuseIdentifier:@"TeamInfoCell"],
                                                  [BaseCellModel setModel:@"比赛时间" key:@"" value:date image:@"比赛时间" reuseIdentifier:@"MatchInfoCell"],
                                                  [BaseCellModel setModel:@"比赛人制" key:@"" value:typeString image:@"比赛人制" reuseIdentifier:@"MatchInfoCell"],
                                                  [BaseCellModel setModel:@"比赛地点" key:@"" value:addr image:@"比赛地点" reuseIdentifier:@"MatchInfoCell"]
                                                  ]];
        
        
        
        
        [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                  
                                                  [BaseCellModel setModel:@"对手战斗力" key:@"" value:power image:@"对手战斗力" reuseIdentifier:@"MatchInfoCell"],
                                                  [BaseCellModel setModel:@"对手平均年龄" key:@"" value:age image:@"比赛时间" reuseIdentifier:@"MatchInfoCell"],
                                                  [BaseCellModel setModel:@"比赛留言" key:@"" value:desc image:@"比赛留言" reuseIdentifier:@"MatchNoteCell"]
                                                  ]];
        
        
        
        
       
        
        
        
    }else{
        
        [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                  
                                                  [BaseCellModel setModel:@"" key:_match?@"0":@"1" value:team image:@"" reuseIdentifier:@"TeamInfoCell"],
                                                  [BaseCellModel setModel:@"比赛时间" key:@"" value:date image:@"比赛时间" reuseIdentifier:@"MatchInfoCell"],
                                                  [BaseCellModel setModel:@"比赛人制" key:@"" value:typeString image:@"比赛人制" reuseIdentifier:@"MatchInfoCell"]
                                                  ]];
        
    }
    
    
    
    
    
    
    
    return dic;
    
}
-(void)showSelectAlertView:(MutableBlock)block{
    
   
    
    

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"比赛人制" message:@"" preferredStyle:[[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ?UIAlertControllerStyleAlert: UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"2人制"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          block(@"2人制");
                                                          type=@"2";
                                                      }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"3人制"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          block(@"3人制");
                                                          type=@"3";
                                                      }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"4人制"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          block(@"4人制");
                                                          type=@"4";
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"5人制"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          block(@"5人制");
                                                          type=@"5";
                                                      }]];
   
    
    
   
    
    
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消"
                                                        style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          
                                                      }]];
    [self presentViewController:alertController animated:YES completion:nil];









}
@end
