//
//  HomeViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "HomeViewController.h"
#import "AddWatchView.h"
#import "MatchModel.h"
#import "TeamModel.h"
#import "GuidePageView.h"
#import "DataFormatHelper.h"
#import "AdvertWebController.h"
#import "AdvertPageView.h"
#import "XHToast.h"
#import "TeamHelper.h"
#import <AddressBook/AddressBook.h>
#import "ContactTool.h"
@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>

{
    UIButton *leftImageView;
    __weak IBOutlet UIView *headView;
    
    __weak IBOutlet UITableView *watchTableView;
    UIButton *lastBtn;

    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet UIButton *allBtn;
    __weak IBOutlet UIImageView *line;
    
    int pageNumber;
    UIButton*rightButton;
    NSArray *sortArr;
    NSString *myCurrentVersion;
    
    BOOL isLock;
    
       ContactTool * contactTool; //手机通讯录
}

@end

@implementation HomeViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    rightButton.hidden=NO;

}

//设置访问手机通讯录权限
- (void)getMineAccesssAuthPhone
{
    [TeamHelper CheckAddressBookAuthorization:^(bool isAuthorized) {
        if (isAuthorized)
        {
            NSLog(@"同意了APP访问该手机通讯录");
        }
        else
        {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"提示"  message: @"请到 设置>隐私>通讯录 打开通讯录权限"    preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
    
}


//上传手机通讯录
- (void)UpLoadPhoneFriends:(NSString *)contactList
{
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSLog(@"token = %@",[AccountHelper shareAccount].token);
    if ([AccountHelper shareAccount].isLogin==YES) {
        
        [manager.requestSerializer setValue:[AccountHelper shareAccount].token forHTTPHeaderField:@"token"];
    }
    NSDictionary *dic = @{@"contactList":contactList};
    [manager POST:Url(@"address/update") parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
   
        NSLog(@"上传通讯录成功");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
       
    }];
}
-(void)viewDidLoad{
    [super viewDidLoad];

    
    
    if ( [AccountHelper shareAccount].isBackHome!=YES) {
        
        if ([[Defaults objectForKey:@"dynamicDetailPush"] isEqualToString:@"动态推送跳转"])
        {
            
        }
        else
        {
            //广告页
            [self getMyAdvertPages];
        }
    }else{
        [AccountHelper shareAccount].isBackHome=NO;
    
    }
    
   
  
    
    
    //引导页
    NSString *string=[Defaults objectForKey:@"GuidePages"];
    if (!string ||[string isEqualToString:@""]) {
        [[[GuidePageView alloc]initView] show];
        [Defaults setObject:@"GuidePages" forKey:@"GuidePages"];
        [Defaults synchronize];
    }

    
    

    [self loadNavBarItmes];
    [self loadHeadView];
    [self loadTableView];
    [self headBtnClick:allBtn];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissToRoot) name:@"dismissToRoot2" object:nil];
    
    
    NSString *str = [Defaults objectForKey:@"canuploadmyphones"];
    if ([str isEqualToString:@"canuploadmyphones"])
    {
        
    }
    else
    {
        //设置访问手机通讯录权限
        [self getMineAccesssAuthPhone];
        //注册去上传通讯录的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactToolphoneArray) name:@"HomeLocalPhoneArray" object:nil];
    }
    
 
    
    
}

- (void)contactToolphoneArray
{
    contactTool = [ContactTool new];
    if (contactTool.phoneArray !=nil && contactTool.phoneArray.count > 0)
    {
        NSString *contactList0 = [self  jsonStringFromDictionaryOrArray:contactTool.phoneArray];
        NSString *contactList1 = [contactList0 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *contactList2 = [contactList1 stringByReplacingOccurrencesOfString:@"+86" withString:@""];
        NSString *contactList3 = [contactList2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString *contactList4 = [contactList3 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *contactList5 = [contactList4 stringByReplacingOccurrencesOfString:@"[" withString:@""];
        NSString *contactList6 = [contactList5 stringByReplacingOccurrencesOfString:@"]" withString:@""];
        [self UpLoadPhoneFriends:contactList6];
    
        
    }
}
-(NSString *)jsonStringFromDictionaryOrArray:(id)object{
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
    if ([jsonData length] > 0 && error == nil) {
        NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
       
        return str;
    }
    return nil;
}


-(void)dismissToRoot{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [SVProgressHUD dismiss];
          [watchTableView.mj_header beginRefreshing];
    });

  

}

- (void)getMyCurrentVersonAction
{
    //当前版本号
   if([AccountHelper shareAccount].isLogin ==YES){
        
//        NSLog(@"%@",[AccountHelper shareAccount].player.id);
       
        if ([AccountHelper shareAccount].player.id)
        {
            
            [self loadUserData:^(id data) {
                
                
                if([AccountHelper shareAccount].user.autoupdate) //自动下载更新并提示安装
                {
                    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
                    myCurrentVersion =  [infoDic objectForKey:@"CFBundleVersion"];
                    [self getCurrentVersionUrl];
                }
                
                else
                {
                    
                }
            }];
        }
    }
}


//这里是注册，登录成功后发的通知,声明在父类中
-(void)accountLogined{

    [self  getMyCurrentVersonAction];


}
-(void)loadNavBarItmes{
    
    //左侧头像按钮
    leftImageView=[[UIButton alloc]initWithFrame:CGRectMake(-10, 0, 39, 39)];
    leftImageView.layer.cornerRadius=19.5;
    leftImageView.layer.masksToBounds=YES;
    leftImageView.userInteractionEnabled=YES;
    [leftImageView addTarget:self action:@selector(headImageClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self headImageChange];
    
    
    
    
    //去除边线
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];

    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftImageView];
    rightButton = [[UIButton alloc]initWithFrame:CGRectMake(kScreenWidth-40,0,30,30)];
    [rightButton setImage:[UIImage imageNamed:@"添加"]forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(addBtnClick:)forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationController.navigationBar addSubview:rightButton];
   
    
}
-(void)loadHeadView{
    
    headView.backgroundColor=BarColor;
   
}

-(void)loadTableView{
    
    watchTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    watchTableView.rowHeight=UITableViewAutomaticDimension;
    watchTableView.estimatedRowHeight=125;

    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    watchTableView.backgroundView=bgImageView;
    watchTableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        pageNumber=1;
        [self  loadNetWorkData:lastBtn.tag];
    }];
    watchTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        pageNumber++;
        [self  loadNetWorkData:lastBtn.tag];
        
    }];
    
    

    
}
-(IBAction)headBtnClick:(UIButton *)butn{
    
    //防止点击多次
    if (butn==lastBtn) {
        return;
    }
    
    
    [self.dataDic removeAllObjects];
    [watchTableView reloadData];
    
    
    lastBtn.selected=NO;
    butn.selected=!butn.selected;
    
    lastBtn=butn;
 
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/3*(butn.tag-1001);
        [line layoutIfNeeded];
        
    }];
    
    
    [watchTableView.mj_header beginRefreshing];
 
}

-(void)loadNetWorkData:(NSInteger )tag{
    
    
    
    static NSString *MatchCell1=@"MatchCell1";
    static NSString *MatchCell2=@"MatchCell2";
    static NSString *MatchCell3=@"MatchCell3";
    static NSString *MatchCell4=@"MatchCell4";
    
    
    [AccountHelper shareAccount].isShow=YES; //不显示url请求提示
    
    if (tag==1001) {
        
        [HttpRequestHelper postWithURL:Url(@"match/query") params:@{@"pageNumber":@(pageNumber)} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                if (pageNumber==1) {
                    
                    [self.dataDic removeAllObjects];
                }

                for (NSDictionary *dic  in responseObject[@"content"]) {
                    
                    MatchModel *model=[[MatchModel alloc]initWithDic:dic];

                    
                    NSString *date=[DataFormatHelper timeChangeFormart:@"MM月dd日 EEEE" times:model.date];
                  
                    BaseCellModel *base=nil;
            
                    if ([model.playerType intValue]==0) {
                        
                        
                        
                        if ([model.status intValue]==0) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell1];
                            
                        }
                        
                        //赛前
                        if ([model.status intValue]==10) {
                            
                           base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell1];
                            
                        }
                        //比赛中
                        if ([model.status intValue]==20) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell2];
                            
                        }
                        //比赛中
                        if ([model.status intValue]==21) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell2];
                            
                        }
                        //比赛结束
                        if ([model.status intValue]==100) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell2];
                            
                        }
                        
                    }else{
                        //約赛中
                        if ([model.status intValue]==0) {
                            
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell1];
                        }
                        //以匹配（赛前）
                        if ([model.status intValue]==10) {
                            
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell4];
                        }
                        //比赛中
                        if ([model.status intValue]==20) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell3];
                        }
                        if ([model.status intValue]==21) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell3];
                        }
                        //比赛结束
                        if ([model.status intValue]==100) {
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:MatchCell3];
                        }
                    
                    
                    }
                    
                    
                    if(base){
                    
                    if ([self.dataDic objectForKey:date]) {
                        
                        NSMutableArray *arr=[NSMutableArray array];
                        [arr addObjectsFromArray:[self.dataDic objectForKey:date]];
                        [arr addObject:base];
                        
                        [self.dataDic setObject:arr forKey:date];
                        
                        
                    }else{
                        
                        NSMutableArray *arr=[NSMutableArray array];
                        
                        [arr addObject:base];
                        
                        [self.dataDic setObject:arr forKey:date];
                    
                    }
                    
                    }
                    

                }
                [watchTableView.mj_header endRefreshing];
                [watchTableView.mj_footer endRefreshing];
                
                [watchTableView reloadData];
                
                
                

                
                
                
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
                [watchTableView.mj_header endRefreshing];
                [watchTableView.mj_footer endRefreshing];
                
                
                  [watchTableView reloadData];
            
            }
            
            
        } failure:^(NSError *error) {
            
            [watchTableView.mj_header endRefreshing];
            [watchTableView.mj_footer endRefreshing];
            
        }];
        
        
        
    }
    if (tag==1002) {
        
        if([AccountHelper shareAccount].isLogin==YES){
            
            
        
            [HttpRequestHelper postWithURL:Url(@"match/queryByUser") params:@{@"pageNumber":@(pageNumber)} success:^(BOOL sucess, id responseObject) {
                
                if (sucess) {
                    
                    if (pageNumber==1) {
                        
                        [self.dataDic removeAllObjects];
                    }
                    if ([responseObject[@"content"] count]==0) {
                        
                        pageNumber--;
                        [watchTableView reloadData];
                        [watchTableView.mj_header endRefreshing];
                        [watchTableView.mj_footer endRefreshing];
                        
                        return ;
                    }
                    
                    
                    
                    
                    for (NSDictionary *dic  in responseObject[@"content"]) {
                        
                        MatchModel *model=[[MatchModel alloc]initWithDic:dic];
                        
                        
                        NSString *date=[DataFormatHelper timeChangeFormart:@"MM月dd日 EEEE" times:model.date];
                        
                        BaseCellModel *base=nil;
                        
                        if ([model.playerType intValue]==0) {
                            
                            
                            
                            if ([model.status intValue]==0) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                                
                            }
                            
                            //赛前
                            if ([model.status intValue]==10) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                                
                            }
                            //比赛中
                            if ([model.status intValue]==20) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                                
                            }
                            //比赛中
                            if ([model.status intValue]==21) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                                
                            }
                            //比赛结束
                            if ([model.status intValue]==100) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                                
                            }
                            
                        }else{
                            //約赛中
                            if ([model.status intValue]==0) {
                                
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                            }
                            //以匹配（赛前）
                            if ([model.status intValue]==10) {
                                
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell4"];
                            }
                            //比赛中
                            if ([model.status intValue]==20) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                            }
                            if ([model.status intValue]==21) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                            }
                            //比赛结束
                            if ([model.status intValue]==100) {
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                            }
                            
                            
                        }
                        
                        if(base){
                            
                            if ([self.dataDic objectForKey:date]) {
                                
                                NSMutableArray *arr=[NSMutableArray array];
                                [arr addObjectsFromArray:[self.dataDic objectForKey:date]];
                                [arr addObject:base];
                                
                                [self.dataDic setObject:arr forKey:date];
                                
                                
                            }else{
                                
                                NSMutableArray *arr=[NSMutableArray array];
                                
                                [arr addObject:base];
                                
                                [self.dataDic setObject:arr forKey:date];
                                
                            }
                            
                        }
        
                        
                    }
                    
                    [watchTableView.mj_header endRefreshing];
                    [watchTableView.mj_footer endRefreshing];
                    
                    [watchTableView reloadData];
                    
                    
                    
                    
                    
                    
                    
                    
                }else{
                    
                    [SVProgressHUD showErrorWithStatus:responseObject];
                    [watchTableView.mj_header endRefreshing];
                    [watchTableView.mj_footer endRefreshing];
                    
                }
                
                
            } failure:^(NSError *error) {
                
                [watchTableView.mj_header endRefreshing];
                [watchTableView.mj_footer endRefreshing];
                
            }];
        }else{
            
            [watchTableView.mj_header endRefreshing];
            [watchTableView.mj_footer endRefreshing];
            
            
            [[[FBAlertView alloc]initWithTitle:@"当前信息需要登录才能查看,是否登录" butnTitlt:@"去登录" block:^(id data) {
                
                
                UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
                controller.navigationItem.title=@"登录";
                [self.navigationController pushViewController:controller animated:YES];
                
                
            }] show];

        }
    }
    if (tag==1003) {
       
        
        [[AccountHelper shareAccount] getLocation:^(id data) {
            
            
            if ([data intValue]==1000) {
                 [SVProgressHUD  showErrorWithStatus:@"定位失败，请检查定位权限"];
               
            }else{
            
           
            NSMutableDictionary *dic=[NSMutableDictionary dictionary];
            
            [dic setObject:@(pageNumber) forKey:@"pageNumber"];
            [dic setValue:[AccountHelper shareAccount].latitude forKey:@"latitude"];
            [dic setValue:[AccountHelper shareAccount].longitude forKey:@"longitude"];
            
            [HttpRequestHelper postWithURL:Url(@"match/queryNear") params:dic success:^(BOOL sucess, id responseObject) {
                
                if (sucess) {
                    
                    if (pageNumber==1) {
                        
                        [self.dataDic removeAllObjects];
                    }
                    if ([responseObject[@"content"] count]==0) {
                        
                        pageNumber--;
                        [watchTableView reloadData];
                        [watchTableView.mj_header endRefreshing];
                        [watchTableView.mj_footer endRefreshing];
                        
                        return ;
                    }
                    
                    
                    
                    
                    for (NSDictionary *dic  in responseObject[@"content"]) {
                        
                        MatchModel *model=[[MatchModel alloc]initWithDic:dic];
                        
                        
                        NSString *date=[DataFormatHelper timeChangeFormart:@"MM月dd日 EEEE" times:model.date];
                        
                        BaseCellModel *base=nil;
                        
                        if ([model.playerType intValue]==0) {
                            
                            
                            
                            if ([model.status intValue]==0) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                                
                            }
                            
                            //赛前
                            if ([model.status intValue]==10) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                                
                            }
                            //比赛中
                            if ([model.status intValue]==20) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                                
                            }
                            //比赛中
                            if ([model.status intValue]==21) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                                
                            }
                            //比赛结束
                            if ([model.status intValue]==100) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                                
                            }
                            
                        }else{
                            //約赛中
                            if ([model.status intValue]==0) {
                                
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                            }
                            //以匹配（赛前）
                            if ([model.status intValue]==10) {
                                
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell4"];
                            }
                            //比赛中
                            if ([model.status intValue]==20) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                            }
                            if ([model.status intValue]==21) {
                                
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                            }
                            //比赛结束
                            if ([model.status intValue]==100) {
                                base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                            }
                            
                            
                        }
                        
                        
                        if(base){
                            
                            if ([self.dataDic objectForKey:date]) {
                                
                                NSMutableArray *arr=[NSMutableArray array];
                                [arr addObjectsFromArray:[self.dataDic objectForKey:date]];
                                [arr addObject:base];
                                
                                [self.dataDic setObject:arr forKey:date];
                                
                                
                            }else{
                                
                                NSMutableArray *arr=[NSMutableArray array];
                                
                                [arr addObject:base];
                                
                                [self.dataDic setObject:arr forKey:date];
                                
                            }
                            
                        }
                        
                        
                    }
                    [watchTableView.mj_header endRefreshing];
                    [watchTableView.mj_footer endRefreshing];
                    
                    [watchTableView reloadData];
                    
                    
                    
                }else{
                    
                    [SVProgressHUD showErrorWithStatus:responseObject];
                    [watchTableView.mj_header endRefreshing];
                    [watchTableView.mj_footer endRefreshing];
                    
                }
                
                
            } failure:^(NSError *error) {
                
                [watchTableView.mj_header endRefreshing];
                [watchTableView.mj_footer endRefreshing];
                
            }];
            
        }
         
        }];
        
        
        
    }



}
-(void)headImageChange{
    
    if([AccountHelper shareAccount].isLogin ==YES){

        [leftImageView setBackgroundImageForState:0 withURL:[NSURL URLWithString:[AccountHelper shareAccount].player.avatar] placeholderImage:HeadImage];
        [leftImageView setTitle:@"" forState:0];

    }else{
        
        [leftImageView setBackgroundImage:HeadImage forState:0];
        [leftImageView setTitle:@"未登录" forState:0];
        [leftImageView setTitleColor:[UIColor grayColor] forState:0];
        leftImageView.titleLabel.font=[UIFont systemFontOfSize:12];
    
    }

}
#pragma mark 头像按钮点击事件
-(void)headImageClick:(id)sender{
    
    
   
    
//    
//    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"] animated:YES completion:nil];
    
    [self headImageViewClick];
    
    
    
}


#pragma mark 添加按钮点击事件
-(void)addBtnClick:(id)sender{
    
    //如果没有登录
    if([AccountHelper shareAccount].isLogin==YES){
    
        [[[AddWatchView alloc]initWithBlock:^(id data) {
            
            if ([data intValue]==1) {
                
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamNameViewController"] animated:YES];
            }else{
                
                [[AccountHelper shareAccount] getPlayer:^(id data) {
                        
                        if ([[self getMyLeaderTeams] count]<=0) {
                            
                            [[[FBAlertView alloc ]initWithTitle:@"您当前没有球队，暂不能发起比赛，是否去创建球队？" butnTitlt:@"去创建!" block:^(id data) {
                                [AccountHelper shareAccount].isReg=NO;
                                
                                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamNameViewController"] animated:YES];
        
                            }]show];
                           
                        }else
                            
                            
                        {
                            
                            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatMtchViewController"];
                            controller.navigationItem.title=@"发起比赛";
                            
                            [controller setControllerBlock:^(id data) {
                                
                                [watchTableView.mj_header beginRefreshing];
                            }];
                            
                            [self.navigationController pushViewController:controller animated:YES];
                            
                        }
                    }];
                    
                
                
            }
            
        }]show];
    
    
    
    }else{
    
     [self headImageViewClick];
    
    
    }
    
   

}
#pragma mark tableView 委托协议

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    
    [self sortDate];
    
    
    return [[self.dataDic allKeys] count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    NSString *sectionTitle=sortArr[section];
    

    NSArray *arr=[self.dataDic objectForKey:sectionTitle];
    
    return arr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 25;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    
    
    
    
    UIView *view=[[UIView alloc]init];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 4, 50, 17)];
    
    
    NSString *string =[DataFormatHelper compareDate :sortArr[section]];
    string= [string stringByReplacingOccurrencesOfString:@"Monday" withString:@"星期一"];
    string= [string stringByReplacingOccurrencesOfString:@"Tuesday" withString:@"星期二"];
    string= [string stringByReplacingOccurrencesOfString:@"Wednesday" withString:@"星期三"];
    string= [string stringByReplacingOccurrencesOfString:@"Thursday" withString:@"星期四"];
    string= [string stringByReplacingOccurrencesOfString:@"Friday" withString:@"星期五"];
    string= [string stringByReplacingOccurrencesOfString:@"Saturday" withString:@"星期六"];
    string= [string stringByReplacingOccurrencesOfString:@"Sunday" withString:@"星期天"];
    
    label.text=string;

    
    if([label.text rangeOfString:@"今天"].location != NSNotFound)//_roaldSearchText
    {
       label.backgroundColor=TODAYCOLOR;
    }
    else
    {
       label.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
    }

    label.font=[UIFont systemFontOfSize:10];
    [label sizeToFit];
    label.frame=CGRectMake(0, 4, label.frame.size.width+2, 17);
    label.layer.cornerRadius=3;
    label.textColor=[UIColor whiteColor];
    label.textAlignment=1;
    label.layer.masksToBounds=YES;
    [view addSubview:label];
    
    return view;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSString *sectionTitle=sortArr[indexPath.section];

    NSArray *arr=[self.dataDic objectForKey:sectionTitle];
    
   
    BaseCellModel *model=arr[indexPath.row];

    BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    //左边和右边头像的跳转
    MatchModel *match=model.value;
    
    if(indexPath.row == 0)
    {
        [self getshareInfo:match.id];   //这里仅仅是为了用比赛id去网络请求得到分享时的icon图标的url，因为每个Icon图标都一样好，所以随便用一个比赛id就可以了，保存本地后面去用
    }
    //回调事件
    
    [cell setCellBlock:^(id data) {
        
     

        if ([data isEqualToString:@"left"]) {
            TeamModel *team=[[TeamModel alloc]initWithDic:match.home];
            [self teamHeadImageClick:team.id];
        }
        if ([data isEqualToString:@"right"]) {
            TeamModel *team=[[TeamModel alloc]initWithDic:match.visiting];
            [self teamHeadImageClick:team.id];
            
        }

        
    }];
    
    if (indexPath.row%2) {
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
       
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    [cell setValue:model forKey:@"data"];
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
   
   
    NSString *sectionTitle=sortArr[indexPath.section];
    
    
    NSArray *arr=[self.dataDic objectForKey:sectionTitle];
    BaseCellModel *model=arr[indexPath.row];
     MatchModel *match=model.value;
    
    
    
    if ([match.playerType  intValue]==0 ) {
        
        //赛前
        if ([match.status intValue]==10) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            controller.navigationItem.title=@"赛前";
            [controller setValue:match.id forKey:@"matchId"];
            
            [controller setControllerBlock:^(id data) {
                
                [watchTableView.mj_header beginRefreshing];
                
            }];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        //比赛中
        if ([match.status intValue]==20 || [match.status intValue]==21) {
            
            
            
            if ([AccountHelper shareAccount].isLogin!=YES) {
                BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
                controller.navigationItem.title=@"赛后";
                [controller setValue:match.id forKey:@"matchId"];
                [self.navigationController pushViewController:controller animated:YES];
                
                return;
            }
            
            if (isLock==YES) {
                return;
            }
            
            isLock=YES;
            
            [AccountHelper shareAccount].isShow=YES;
            
            
          
            
            
            
            [HttpRequestHelper postWithURL:Url(@"game/isScoring") params:@{@"matchId":match.id} success:^(BOOL sucess, id responseObject) {
                 isLock=NO;
                if (sucess){
                    
                 
                    
                    if ([responseObject[@"isScoring"] intValue]==0) {
                        
                        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
                        controller.navigationItem.title=@"赛中";
                        [controller setValue:match.id forKey:@"matchId"];
                        [self.navigationController pushViewController:controller animated:YES];
                        
                        
                    }else{
                        
                        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
                        controller.navigationItem.title=@"赛中";
                        [controller setValue:match.id forKey:@"matchId"];
                        [self.navigationController pushViewController:controller animated:YES];
                        
                    }
                    
                   
                }
                
                
            } failure:^(NSError *error) {
                isLock=NO;
            }];
            
        }
        
        //比赛结束
        if ([match.status intValue]==100) {
            
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
            controller.navigationItem.title=@"赛后";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }
        
    }else{
        //約赛中
        if ([match.status intValue]==0) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinWarViewController"];
            
            controller.navigationItem.title=@"约赛中";
            [controller setValue:match.id forKey:@"matchId"];
        
            [controller setControllerBlock:^(id data) {
                
                [watchTableView.mj_header beginRefreshing];
                
            }];
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        //以匹配（赛前）
        if ([match.status intValue]==10) {
            
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            
            controller.navigationItem.title=@"赛前";
            [controller setValue:match.id forKey:@"matchId"];
            
            [self.navigationController pushViewController:controller animated:YES];
            
           
        }
      
        //比赛中
        if ([match.status intValue]==20 || [match.status intValue]==21) {
            
            
            if ([AccountHelper shareAccount].isLogin!=YES) {
                BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
                controller.navigationItem.title=@"赛后";
                [controller setValue:match.id forKey:@"matchId"];
                [self.navigationController pushViewController:controller animated:YES];
                
                return;
            }
            [AccountHelper shareAccount].isShow=YES;
            
            
           
            
            
            
            if (isLock==YES) {
                return;
            }
            
            isLock=YES;
            
            [AccountHelper shareAccount].isShow=YES;
             NSDate* tmpStartData = [NSDate date];
            
            [HttpRequestHelper postWithURL:Url(@"game/isScoring") params:@{@"matchId":match.id} success:^(BOOL sucess, id responseObject) {
                isLock=NO;
                if (sucess) {
                    
                    double deltaTime = [[NSDate date] timeIntervalSinceDate:tmpStartData];
                    
                    
                  //  [XHToast showBottomWithText:[NSString stringWithFormat:@"%f",deltaTime]];
                    
                    if ([responseObject[@"isScoring"] intValue]==0) {
                        
                        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
                        controller.navigationItem.title=@"赛中";
                        [controller setValue:match.id forKey:@"matchId"];
                        [self.navigationController pushViewController:controller animated:YES];
                        
                        
                    }else{
                        
                        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
                        controller.navigationItem.title=@"赛中";
                        [controller setValue:match.id forKey:@"matchId"];
                        [self.navigationController pushViewController:controller animated:YES];
                        
                    }
                    
                    
                }
                
                
            } failure:^(NSError *error) {
                isLock=NO;
            }];
           
           
            
        }
        //比赛结束
        if ([match.status intValue]==100) {
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
            controller.navigationItem.title=@"赛后";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }

}
-(void)teamHeadImageClick:(NSString *)string{

    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    [controller setValue:string forKey:@"teamId"];
    [self.navigationController pushViewController:controller animated:YES];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:NO];
      
    //让登陆一次才提示一次升级
    if([AccountHelper shareAccount].isLogin==YES){
       NSString *str =  [Defaults objectForKey:@"gotoupdateNewVersion"];
        if ([str isEqualToString:@"gotoupdateNewVersion"])
        {
            
        }
        else
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"logined" object:nil];
            [Defaults setObject:@"gotoupdateNewVersion" forKey:@"gotoupdateNewVersion"];
            [Defaults synchronize];
        }
   
    }
    
    if ([AccountHelper shareAccount].isReg==YES) {
        
        [[[FBAlertView alloc ]initWithTitle:@"欢迎使用全网篮球\n现在去完善资料？" butnTitlt:@"好!" block:^(id data) {
            [AccountHelper shareAccount].isReg=NO;
            
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"] animated:YES];
  
        }]show];
    }

}
//这里是进行日期排序
-(void)sortDate{
    
    if ([sortArr count]!=[self.dataDic allKeys].count) {
        
        NSArray  *keyArr=[self.dataDic allKeys];
        
        sortArr = (NSMutableArray *)[keyArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MM月dd日 EEEE"];
            
            NSDate *date1 = [formatter dateFromString:obj1];
            NSDate *date2 = [formatter dateFromString:obj2];
            NSComparisonResult result = [date1 compare:date2];
            return result == NSOrderedAscending;
        }];
        
    }
}
-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:NO];
    [Defaults removeObjectForKey:@"dynamicDetailPush"]; //移除关于动态推送的key
     rightButton.hidden=YES;
}

//通过服务器得到我当前是否有广告
- (void)getMyAdvertPages
{
    [AccountHelper shareAccount].isShow=YES;
    [HttpRequestHelper postWithURL:Url(@"advert/get") params:nil success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            NSDictionary *dic = responseObject;
//            NSLog(@"%@",dic[@"isOpen"]);
            if (dic[@"isOpen"])     //有广告
            {
                NSString *srcPath    = dic[@"srcPath"];
                NSString *urlPath    = dic[@"urlPath"];
                [[[AdvertPageView alloc]initViewImage:srcPath And:urlPath] show];
            }
        }
        
    } failure:^(NSError *error) {
        
    }];
}


//得到服务器当前的app最新版本
- (void)getCurrentVersionUrl
{
    [AccountHelper shareAccount].isShow=YES;
    
    
    [HttpRequestHelper postWithURL:Url(@"version/getVersion") params:@{@"type":[NSNumber numberWithInt:1]} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            NSDictionary *dic = responseObject;
            [AccountHelper shareAccount].versionNumber  = dic[@"versionNumber"];
            [AccountHelper shareAccount].downloadURL    = dic[@"downloadURL"];
            NSString *strVerson = dic[@"versionNumber"];
            NSString *strUrl    = dic[@"downloadURL"];
            NSString *minCode   = dic[@"minCode"];
            if ([myCurrentVersion intValue] < [minCode intValue]) {
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"当前版本太低"  message: @"需要升级才能使用"    preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //跳1 一般用于跳到外部链接
                    UIApplication *application = [UIApplication sharedApplication];
                    [application openURL:[NSURL URLWithString:strUrl]];
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];

            }
            
            if ([myCurrentVersion intValue] < [strVerson intValue])//105 103，当前版本小于服务器版本时提示跳转
            {
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"有新的版本了"  message: @"去商店升级"    preferredStyle:UIAlertControllerStyleAlert];
                
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"暂不升级" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                    
                    
                }]];
                
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //跳1 一般用于跳到外部链接
//                    UIApplication *application = [UIApplication sharedApplication];
//                    [application openURL:[NSURL URLWithString:strUrl]];
                    //上面那个strUrl不用，我这里直接跳到appstore里面去
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1052580535"]];
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            
            
        }else{
   
        }
        
    } failure:^(NSError *error) {
        
    }];
}


- (void)getshareInfo:(NSString *)matchid
{
    [AccountHelper shareAccount].isShow=YES;
    [HttpRequestHelper postWithURL:Url(@"match/share") params:@{@"matchId":matchid} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
          
            NSDictionary *dic = responseObject;
            NSString *stricon = dic[@"icon"];
            [Defaults setObject:stricon forKey:@"strICON"];
            [Defaults synchronize];
        }
        else
        {
            
        }
        
    } failure:^(NSError *error) {
       
    }];
}

@end
