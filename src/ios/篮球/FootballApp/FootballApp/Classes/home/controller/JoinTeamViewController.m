//
//  JoinTeamViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "JoinTeamViewController.h"
#import "TeamHelper.h"
@interface JoinTeamViewController ()
{

    __weak IBOutlet UITextField *password;


}
@end

@implementation JoinTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [password becomeFirstResponder];
}

- (IBAction)joinBtnClick:(id)sender {
    
    
    if ([password .text isEqualToString:@""]) {
         [SVProgressHUD showErrorWithStatus:@"球队暗号不能为空"];
        return;
        
    }
    
    [TeamHelper joinTeam:@{@"teamId":_teamId,@"password":password.text} success:^(id data) {
        
        
         [SVProgressHUD showSuccessWithStatus:@"加入成功"];
        
            self.cblock(@"");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"joinTeam" object:nil];
        
        
        [self.navigationController popViewControllerAnimated:YES];
        
        
    } failure:^(id data) {
        
         [SVProgressHUD showErrorWithStatus:@"球队口号错误，加入失败"];
        
        
    }];
    
    
    
}


@end
