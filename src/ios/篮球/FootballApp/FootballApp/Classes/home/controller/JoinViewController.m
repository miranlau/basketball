//
//  JoinViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "JoinViewController.h"
#import "MatchModel.h"
#import "TeamModel.h"
#import "MatchModel1.h"
#import "MatchHelper.h"
#import "TeamView.h"
#import "TeamView.h"

#import "TabBarViewController.h"

@interface JoinViewController ()
{

    __weak IBOutlet UITableView *teamTableView;

    __weak IBOutlet NSLayoutConstraint *buttom;
    __weak IBOutlet UIButton *buttom1Btn;
    __weak IBOutlet UIButton *buttom2Btn;
    __weak IBOutlet UIButton *buttom3Btn;
    
    __weak IBOutlet NSLayoutConstraint *widthConst;
    MatchModel *match;
    
    NSString *mobile;
}
@property(nonatomic,strong)MatchModel1 *model1;
@property(nonatomic,copy)NSString *teamId;
@property(nonatomic,assign)int teamStatus;
@property(nonatomic,copy) NSString *mobilePhone;
@end

@implementation JoinViewController

- (void)doClickBackAction
{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabBarViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [Defaults setObject:@"动态推送跳转" forKey:@"dynamicDetailPush"];
    [Defaults synchronize];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self.matchpushInfo isEqualToString:@"约赛推送跳转"])
    {
        UIImage* backImage = [UIImage imageNamed:@"back.png"];
        CGRect backframe = CGRectMake(0,0,54,30);
        UIButton* backButton= [[UIButton alloc] initWithFrame:backframe];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)]   ;
        [backButton setImage:backImage forState:UIControlStateNormal];
        
        [backButton addTarget:self action:@selector(doClickBackAction) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }

    
     buttom1Btn.enabled = YES;
    [self loadTableView];
   
    [self loadNetWorkData];
    
   
    
    
    
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNetWorkData) name:@"matchChange" object:nil];
   
}
-(void)dismissToRoot{

    [self.navigationController popToRootViewControllerAnimated:NO];
    
}
-(void)loadTableView{

  
    
    teamTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    teamTableView.rowHeight=UITableViewAutomaticDimension;
    teamTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    teamTableView.backgroundView=bgImageView;

}
-(void)loadNetWorkData{
    
    [self.dataDic removeAllObjects];
    
    [HttpRequestHelper postWithURL:Url(@"match/queryById") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {

            match=[[MatchModel alloc]initWithDic:responseObject];
            
            _matchId=match.id;
            
            if ([match.status intValue]==10) {
                self.navigationItem.title=@"赛前";
            }
            if ([match.status intValue]==20||[match.status intValue]==21) {
                self.navigationItem.title=@"赛中";
            }
   
            
        //比赛类型:0:单对赛 1:匹配赛
            if ([match.playerType intValue]==0) {
                
                 [BaseCellModel insetKeyForDic:self.dataDic key:@"0" value:@[[BaseCellModel setModel:@"" key:@"" value:match image:@"" reuseIdentifier:@"MatchCell5"]]];
                
                if ([match.homeusers count]>0) {

                    [BaseCellModel insetKeyForDic:self.dataDic key:@"1" value:@[[BaseCellModel setModel:@"" key:match.home[@"leader"] value:match.homeusers image:@"" reuseIdentifier:@"TeamMenberCell"]]];

                }
   
                if ([self.navigationItem.title isEqualToString:@"赛前"]) {
                    
                    
                    //如果我是主队队长
                    if (MINEId == [match.home[@"leader"] intValue] ) {
                        
                        buttom3Btn.hidden=YES;
                        [buttom1Btn setTitle:@"删除比赛" forState:0];
                        
                        
                        if ([match.enroll intValue]==1) {
                            [buttom2Btn setTitle:@"取消报名" forState:0];
                        }else{
                            
                            [buttom2Btn setTitle:@"报名参赛" forState:0];
                            
                        }
                        
                        
                    }else{
                        buttom3Btn.hidden=NO;
                        
                        
                        if ([match.enroll intValue]==1) {
                           [buttom3Btn setTitle:@"取消报名" forState:0];
                        }else{
                            
                            [buttom3Btn setTitle:@"报名参赛" forState:0];
                        
                        }
                    }
   
                    if ([AccountHelper shareAccount].isLogin==NO) {
                        
                        buttom.constant=-48;
                    }else{
                        buttom.constant=0;
                    }
                    
                }
                
                if ([self.navigationItem.title isEqualToString:@"赛中"]) {
                    
                    buttom.constant=0;
                    
                    if ([self isAppearAccount:responseObject]){
                    buttom3Btn.hidden=YES;
                    
                    
                    [buttom1Btn setTitle:@"比赛计分" forState:0];
                    [buttom2Btn setTitle:@"直播互动" forState:0];
                    }
                    else{
                    
                     buttom3Btn.hidden=NO;
                        
                    [buttom3Btn setTitle:@"直播互动" forState:0];
                    }
                   
                }
                
            }else{
            
                [BaseCellModel insetKeyForDic:self.dataDic key:@"0" value:@[[BaseCellModel setModel:@"" key:@"" value:match image:@"" reuseIdentifier:@"MatchCell6"]]];
                if ([match.homeusers count]>0) {
                    [BaseCellModel insetKeyForDic:self.dataDic key:@"1" value:@[[BaseCellModel setModel:@"" key:match.home[@"leader"] value:match.homeusers image:@"" reuseIdentifier:@"TeamMenberCell"]]];
                }
               
                
                if ([match.visitingusers count]>0) {
                    
                    NSString *hv=@"2";
                    if ([match.homeusers count]==0) {
                        hv=@"1";
                    }
                    
                    [BaseCellModel insetKeyForDic:self.dataDic key:hv value:@[[BaseCellModel setModel:@"" key:match.visiting[@"leader"] value:match.visitingusers image:@"" reuseIdentifier:@"TeamMenberCell"]]];
                }
                
               
            
                if ([self.navigationItem.title isEqualToString:@"赛前"]) {

                    // 判断我是不是队长
                     buttom3Btn.hidden=YES;
                    
                    if([match.home[@"leader"] intValue]==MINEId||[match.visiting[@"leader"] intValue]==MINEId){
                        
                        if ([match.home[@"leader"] intValue] == MINEId)
                        {
                            //如果我是主队队长,就去拿客队里面的电话
                            _mobilePhone  = [NSString stringWithFormat:@"%@ %@", match.visiting[@"leaderName"], match.visiting[@"mobile"]];
                            mobile=[NSString stringWithFormat:@"%@",match.visiting[@"mobile"]];
                        }
                        else
                        {
                            _mobilePhone   =  [ NSString stringWithFormat:@"%@ %@", match.home[@"leaderName"], match.home[@"mobile"]];
                            
                            mobile=[NSString stringWithFormat:@"%@",match.home[@"mobile"]];
                        }
                       
                        buttom3Btn.hidden=YES;
                        [buttom1Btn setTitle:@"联系对方队长" forState:0];
                        
                        if ([match.enroll intValue]==1) {
                            [buttom2Btn setTitle:@"取消报名" forState:0];
                        }else{
                            
                            [buttom2Btn setTitle:@"报名参赛" forState:0];
                            
                        }
                        
                       
                    }else{
                            buttom3Btn.hidden=NO;
//                         [buttom1Btn setTitle:@"联系对方队长" forState:0];
                        if ([match.enroll intValue]==1) {
                            
                            [buttom3Btn setTitle:@"取消报名" forState:0];
                        }else{
                            
                            [buttom3Btn setTitle:@"报名参赛" forState:0];
                            
                        }
                    
                    }
              
                    if ([AccountHelper shareAccount].isLogin==NO) {
                        
                        buttom.constant=-48;
                    }else{
                        buttom.constant=0;
                    }

                }
                
                if ([self.navigationItem.title isEqualToString:@"赛中"]) {
                    buttom.constant=0;
                    
                    if ([self isAppearAccount:responseObject]){
                        buttom3Btn.hidden=YES;
                        
                        
                        [buttom1Btn setTitle:@"比赛计分" forState:0];
                        [buttom2Btn setTitle:@"直播互动" forState:0];
                    }
                    else{
                        
                        buttom3Btn.hidden=NO;
                        
                        [buttom3Btn setTitle:@"直播互动" forState:0];
                    }
                    
                }
                
            
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
               [teamTableView reloadData];
            });
        
        }else{
            
            [SVProgressHUD showErrorWithStatus:responseObject];
            
        }
     
    } failure:^(NSError *error) {
       
    }];
    
}


//是否显示计分按钮
- (BOOL)isAppearAccount:(id)responseObject
{
   //有专业记分员
    if([match.isrecord intValue]==1){
        //代表我是主队记分员
        if ([match.homerecord intValue]==MINEId) {
            
            
            return YES;
 
        }
 
        if ([match.playerType intValue]>=1) {
            
            if ([match.visitingrecord intValue]==MINEId) {
               
                return YES;
            }
        }
        
        return NO;
    
    
    }else{
        
        
        for (NSDictionary *dic in match.homeusers ) {
            
            
            PlayerModel *palyer =[[PlayerModel alloc]initWithDic:dic];
            
            if ([palyer.id intValue]==MINEId) {
                
                
                
                return YES;
                
            
            }

        }
        
        if ([match.playerType intValue]>=1){
        
        for (NSDictionary *dic in match.visitingusers ) {
            
            
            PlayerModel *palyer =[[PlayerModel alloc]initWithDic:dic];
            
            if ([palyer.id intValue]==MINEId) {
                

                return YES;
                
                
            }
            
        }
            
        }
        
        
        return NO;
    
    
    }
    
}

#pragma mark tableView 委托协议
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [[self.dataDic allKeys] count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[self.dataDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return section==0?5:25;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view=[[UIView alloc]init];
    
    
    
    if (section>0) {

        UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(0, 4, 80, 17)];
        button.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
        button.titleLabel.font=[UIFont systemFontOfSize:14];
        [button setTitleColor:[UIColor whiteColor] forState:0];
        
        
        if (section==1) {
            
            [button setTitle:@" 主队球员 》" forState:0];
            button.tag=10001;

            if ([match.homeusers count]==0) {
                
                [button setTitle:@" 客队球员 》" forState:0];
                 button.tag=10002;
                
            }
        }else{
            [button setTitle:@" 客队球员 》" forState:0];
            button.tag=10002;
        }
        
        ViewRadius(button, 3.0);
        
        [button addTarget:self action:@selector(addMenberBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
    }
    
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[self.dataDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    [cell setValue:model forKey:@"data"];
    
    [cell setCellBlock:^(id data) {
        
        
        if ([data isKindOfClass:[PlayerModel class]]) {
            
            PlayerModel *player=data;
            
            if ([player.id intValue]==MINEId) {
                
                
                UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
                controller.navigationItem.title=@"我的";
                [self.navigationController pushViewController:controller animated:YES];
                
            }else{
            
                
                UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
                controller.navigationItem.title=player.name;
                [controller setValue:player.id forKey:@"userId"];
                
                [self.navigationController pushViewController:controller animated:YES];
            }
    
        }else{
            
            //点击进入球队详情
            if ([data isEqualToString:@"left"]) {
                
                
                
                UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
                [controller setValue:match.home[@"id"] forKey:@"teamId"];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
            
            //点击进入球队详情
            if ([data isEqualToString:@"right"]) {
                
                
                
                UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
                [controller setValue:match.visiting[@"id"] forKey:@"teamId"];
                [self.navigationController pushViewController:controller animated:YES];
                
            }
           
        
        
        
        }
        
    }];
    
    return cell;
    
}
-(void)addMenberBtnClick:(UIButton *)button{
    
    if (buttom3Btn.hidden==NO) {
        
        return;
        
    }
    
     BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MenberViewController"];
     [controller setValue:self.matchId forKey:@"matchId"];
    //主队
    if (button.tag==10001) {
        [controller setValue:match.home[@"id"] forKey:@"teamId"];
    }
    //客队
    if (button.tag==10002) {
       [controller setValue:match.visiting[@"id"] forKey:@"teamId"];
    }
    
    [controller setControllerBlock:^(id data) {
        
        [self loadNetWorkData];
        
    }];
   
   
    
    
    controller.navigationItem.title = @"参赛队员确认";
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark 比赛计分


-(IBAction)ButtomBtnClick:(UIButton *)butn{
    
    NSString *name=butn.titleLabel.text;
    
    if ([name isEqualToString:@"删除比赛"]) {
        
        [HttpRequestHelper postWithURL:Url(@"match/delete") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                [SVProgressHUD showSuccessWithStatus:@"删除成功"];
                
                if(self.cblock){
                    
                    self.cblock(@"");
                    
                }
    
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                
                [SVProgressHUD showSuccessWithStatus:responseObject];
            }
       
        } failure:^(NSError *error) {
            
        }];
    }
    
    if ([name isEqualToString:@"编辑比赛"]) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatMtchViewController"];
        
        controller.navigationItem.title=@"修改比赛";
        
        [controller setControllerBlock:^(id data) {
            
            
            if(self.cblock){
                
                self.cblock(@"");
                
            }
            
            [self loadNetWorkData];
            
        }];
        
        
        [controller setValue:match forKey:@"match"];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    if ([name isEqualToString:@"报名参赛"]) {
        
        if([AccountHelper shareAccount].isLogin==YES){
            
            
            NSMutableArray *arr= [AccountHelper shareAccount].player.teams;
            
            NSMutableArray *array=[NSMutableArray array];
            BOOL  isHome=NO;
            
            BOOL isVis= NO;
            
            for (NSDictionary *dic in arr) {
                
                TeamModel *team =[[TeamModel alloc]initWithDic:dic];
                
                if ([team.id intValue]==[match.home[@"id"] intValue]) {
                    [array addObject:team];
                    isHome=YES;
                }
                
                if ([team.id intValue]==[match.visiting[@"id"] intValue]) {
                    [array addObject:team];
                    isVis=YES;
                }
                
                
            }
            
            //如果不是匹配赛
            

                //我是两个队伍的球员
                if (isVis == YES && isHome == YES) {

                    
                    [[[TeamView alloc]initWithArr:array clickBlock:^(id data) {
                        
                        TeamModel *team = array[[data intValue]];

                        [HttpRequestHelper postWithURL:Url(@"match/enroll") params:@{@"matchId":_matchId,@"teamId":team.id} success:^(BOOL sucess, id responseObject) {
                            
                            if (sucess) {
                                
                                [SVProgressHUD showSuccessWithStatus:@"报名成功"];
                                
                                [self loadNetWorkData];
                                
                            }else{
                                
                                [SVProgressHUD showErrorWithStatus:responseObject];
                                
                            }
                            
                        } failure:^(NSError *error) {
                            
                            
                            
                        }];
                        
                        
                        
                    }]show];
                    
                    
                    
                    
                }else{
                    
                    //如果是游客
                    if (isHome==NO&&isVis==NO) {
                        
                        isHome=YES;
                    }
                    
                    
                    [HttpRequestHelper postWithURL:Url(@"match/enroll") params:@{@"matchId":_matchId,@"teamId":isHome==YES?match.home[@"id"]:match.visiting[@"id"]} success:^(BOOL sucess, id responseObject) {
                        
                        if (sucess) {
                            
                            [SVProgressHUD showSuccessWithStatus:@"报名成功"];
                            
                            [self loadNetWorkData];
                            
                        }else{
                            
                            [SVProgressHUD showErrorWithStatus:responseObject];
                            
                        }
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                    }];
                    
                }
                
            
        }else{
        
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
            controller.navigationItem.title=@"登录";
            [self.navigationController pushViewController:controller animated:YES];
        
        
        }
        
        
        
        
    }
    if ([name isEqualToString:@"取消报名"]) {
        [HttpRequestHelper postWithURL:Url(@"match/cancelEnroll") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
            
            
            
            if (sucess) {
                
                [SVProgressHUD showSuccessWithStatus:@"取消成功"];
                
                [self loadNetWorkData];
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
                
            }
            
        } failure:^(NSError *error) {
            
            
            
        }];
    }
    if ([name isEqualToString:@"比赛计分"]) {
        
        [MatchHelper shareMatch].match=nil;
        
        [MatchHelper  MatchInfo:@{@"matchId":_matchId} success:^(id data) {
            
            if ([[MatchHelper shareMatch].match.homeStarting count]>0) {
                
                
                UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"];
                [controller setValue:self.matchId forKey:@"matchId"];
                
                [self presentViewController:controller animated:YES completion:nil];
                
                
            }else{
            
                UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SureLineUpViewController"];
                [controller setValue:self.matchId forKey:@"matchId"];
                
                
                
                [self presentViewController:controller animated:YES completion:nil];
            }
            
            

           
  
            
        } failure:^(id data) {
            
             [SVProgressHUD showErrorWithStatus:data];
        }];
        
        
        
        
  
       
    }
    
    if ([name isEqualToString:@"直播互动"]) {
        
       
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
        controller.navigationItem.title=@"赛中";
        [controller setValue:match.id forKey:@"matchId"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    if ([name isEqualToString:@"联系对方队长"]) {
        if (mobile&&![mobile isEqualToString:@"(null)"]&&![mobile isEqualToString:@""])
        {
            
            [[[FBAlertView alloc]initWithTitle:_mobilePhone butnTitlt:@"拨打电话" block:^(id data) {
                
               
        
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",mobile]]];
                
            }]show];
        }
        else
        {
            [[[FBAlertView alloc]initWithTitle:@"对方队长没有留下电话号码" butnTitlt:@"好" block:^(id data) {
      
                
            }]show];
        }
     
        
        
    }




}
















@end
