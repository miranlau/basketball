//
//  JoinWarViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "JoinWarViewController.h"
#import "MatchModel.h"
#import "TeamModel.h"
#import "TabBarViewController.h"
@interface JoinWarViewController ()

{
    
    __weak IBOutlet UIButton *beginMatchBtn;
    __weak IBOutlet NSLayoutConstraint *buttom;
    __weak IBOutlet UITableView *warTableView;
     MatchModel *match;
    
}

@end


@implementation JoinWarViewController


//删除约赛时一起删除这个越晒的所有推送
- (void)DeleteMatchTuiSonfgs
{
    NSMutableArray *arrayss;
    NSMutableArray *tmpArray;
    
    NSString *strkey = [NSString stringWithFormat:@"%d",MINEId];
    arrayss = [[NSMutableArray alloc]initWithCapacity:0];
    arrayss  =[NSMutableArray arrayWithArray: [Defaults objectForKey:strkey]];

    tmpArray = [NSMutableArray arrayWithCapacity:0];
     //遍历删除
    [arrayss enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    
        NSString *str = [NSString stringWithFormat:@"%@",obj[@"extras"][@"matchId"]];
        if([str intValue] == [_matchId intValue]){
            
            [tmpArray addObject:obj];
        }
        else
        {

        }
        
    }];
    
    for (id obj in tmpArray) {
        
        [arrayss removeObject:obj];
        
    }
    [Defaults setObject:arrayss forKey:strkey];
    [Defaults synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnewmessage" object:nil];
    
}
- (void)doClickBackAction
{

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabBarViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [Defaults setObject:@"动态推送跳转" forKey:@"dynamicDetailPush"];
    [Defaults synchronize];
    [self presentViewController:vc animated:YES completion:nil];
}
-(void)viewDidLoad{

    [super viewDidLoad];
       [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(DeleteMatchTuiSonfgs) name:@"DeleteMatchTuiSonfg" object:nil];
    
    if([self.matchpushInfo isEqualToString:@"约赛推送跳转"])
    {
        UIImage* backImage = [UIImage imageNamed:@"back.png"];
        CGRect backframe = CGRectMake(0,0,54,30);
        UIButton* backButton= [[UIButton alloc] initWithFrame:backframe];
        [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)]   ;
        [backButton setImage:backImage forState:UIControlStateNormal];
        
        [backButton addTarget:self action:@selector(doClickBackAction) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }
    [self loadTableView];
    
    [self loadNetWorkData];

}

-(void)loadTableView{
    
    warTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    warTableView.rowHeight=UITableViewAutomaticDimension;
    warTableView.estimatedRowHeight=125;
    warTableView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    
    
    
}

-(void)loadNetWorkData{

    [self.dataDic removeAllObjects];
    
    
    [HttpRequestHelper postWithURL:Url(@"match/queryById") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
              match=[[MatchModel alloc]initWithDic:responseObject];
            
            
            
            
            
            [BaseCellModel insetKeyForDic:self.dataDic key:@"0" value:@[[BaseCellModel setModel:@"" key:@"" value:match image:@"" reuseIdentifier:@"TeamInfo3Cell"]]];
            
            [BaseCellModel insetKeyForDic:self.dataDic key:@"1" value:@[[BaseCellModel setModel:@"" key:match.power value:match.age image:@"" reuseIdentifier:@"InfoCell1"],
                [BaseCellModel setModel:@"" key:@"" value:match.desc image:@"" reuseIdentifier:@"InfoCell2"]
             ]];
            
            
            if ([match.dating count]>0) {
                
                NSMutableArray *list=[NSMutableArray array];
                
                for (NSDictionary *dic in match.dating) {
                    
                    TeamModel *team=[[TeamModel alloc]initWithDic:dic];
                    
                    [list addObject:[BaseCellModel setModel:@"" key:match.home[@"leader"] value:team image:@"" reuseIdentifier:@"TeamCell"]];
                }
                
                [BaseCellModel insetKeyForDic:self.dataDic key:@"2" value:list];
                
            }
            

            
            
            if ([AccountHelper shareAccount].isLogin==YES &&[match.home[@"leader"] intValue]==[[AccountHelper shareAccount].player.id intValue]) {
                
                
                if([match.datings intValue]>0){
                self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"选择" style:UIBarButtonItemStylePlain target:self action:@selector(selectTeam:)];
                
                }
                
                beginMatchBtn.hidden=YES;
                
                
            }else{
           
                buttom.constant=0;
                if([match.gauntlet intValue]==1){
                
                    [beginMatchBtn setTitle:@"取消应约" forState:0];
                
                }else{
                    [beginMatchBtn setTitle:@"我要应战" forState:0];
                }
            }

            
            [warTableView reloadData];
 
            
        }else{
        
            [SVProgressHUD showErrorWithStatus:responseObject];
            
        }
        
        
      
        
        
        
        
    } failure:^(NSError *error) {
        
        
        
    }];
    
    



}
-(void)selectTeam:(id)sender{
    
    
    NSMutableArray *arr=[NSMutableArray array];
    
    for (NSDictionary *dic in match.dating) {
        
        TeamModel *team=[[TeamModel alloc]initWithDic:dic];
        
        [arr addObject:team];
        
    }
    
    
    [[[TeamView alloc]initWithArr:arr clickBlock:^(id data) {
        
            TeamModel *team=arr[[data intValue]];
            
            [self yesBtnClick:team.id];
            
        
        
        
        
    }] show];



}
#pragma mark tableView 委托协议
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [[self.dataDic allKeys] count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[self.dataDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return section==2?25:5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view=[[UIView alloc]init];
    
    
    
    
    if (section==2) {
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 4, 55, 17)];
        label.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
        label.font=[UIFont systemFontOfSize:10];
        label.layer.cornerRadius=3;
        label.textColor=[UIColor whiteColor];
        label.textAlignment=1;
        label.layer.masksToBounds=YES;
        label.text=@" 应约球队 " ;
        [label sizeToFit];
        [view addSubview:label];
    }
    
    
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[self.dataDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    
    
    
    BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    [cell setCellBlock:^(id data) {
        
        
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
        [controller setValue:match.home[@"id"] forKey:@"teamId"];
        [self.navigationController pushViewController:controller animated:YES];
        
       
        
    }];
    
    if (indexPath.row%2) {
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    [cell setValue:model forKey:@"data"];
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSArray *arr=[self.dataDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    if ([model.reuseIdentifier isEqualToString:@"TeamCell"]) {
        
         TeamModel *team=model.value;
        
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
        [controller setValue:team.id forKey:@"teamId"];
    
        [self.navigationController pushViewController:controller animated:YES];
        
    }
  
    
}
-(void)yesBtnClick:(NSString *)teamId{
    
    
    [HttpRequestHelper postWithURL:Url(@"match/chooseTeam") params:@{@"teamId":teamId,@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            [SVProgressHUD showSuccessWithStatus:@"选择成功"];
            
            if(self.cblock){
            
                self.cblock(@"");
            
            }
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            
            [SVProgressHUD showSuccessWithStatus:responseObject];
        }
        
        
        
    } failure:^(NSError *error) {
        
    }];
    



}
- (IBAction)beginMatchBtnClick:(UIButton *)sender {
    
    
    if ([sender.titleLabel.text isEqualToString:@"我要应战"]) {
        
        
        
        
        
        if ([AccountHelper shareAccount].isLogin!=YES) {
            

            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
            controller.navigationItem.title=@"登录";
            [self.navigationController pushViewController:controller animated:YES];
            
            
            
        }else{
            //这里即使登录了我也要再访问获取一次数据，保证我的确有值
              [[AccountHelper shareAccount] getPlayer:^(id data) {
                        NSMutableArray *teams=[self getMyLeaderTeams];
            
            
            if ([teams count]==0) {
                
                [[[FBAlertView alloc ]initWithTitle:@"您当前没有球队，暂不能发起比赛，是否去创建球队？" butnTitlt:@"去创建!" block:^(id data) {
                    [AccountHelper shareAccount].isReg=NO;
                    
                    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"AddTeamNameViewController"] animated:YES];

                }]show];
                
                
            }else{
                
                [[[TeamView alloc]initWithArr:teams clickBlock:^(id data) {
                    
                    TeamModel *model=teams[[data intValue]];
                    
                    
                    
                    
                    [HttpRequestHelper postWithURL:Url(@"match/gauntlet") params:@{@"teamId":model.id,@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
                        
                        if (sucess) {

                            [SVProgressHUD showSuccessWithStatus:@"应战成功"];
                            
                            [self loadNetWorkData];
                            
                        }else{
                        
                          [SVProgressHUD showErrorWithStatus:responseObject];
                        }
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                    }];
                    
                }]show];
                
            }
            
        }];
            
        }
    }
    
    if ([sender.titleLabel.text isEqualToString:@"删除比赛"]){
    
        [HttpRequestHelper postWithURL:Url(@"match/delete") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                [SVProgressHUD showSuccessWithStatus:@"删除成功"];
                
                
                if(self.cblock){
                    
                    self.cblock(@"");
                    
                }
                //发出通知去删除比赛的推送
                [[NSNotificationCenter defaultCenter]postNotificationName:@"DeleteMatchTuiSonfg" object:nil];
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                
                [SVProgressHUD showSuccessWithStatus:responseObject];
            }
            
            
            
        } failure:^(NSError *error) {
            
        }];
    
    
    }
    
    if ([sender.titleLabel.text isEqualToString:@"编辑比赛"]){
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatMtchViewController"];
        
        controller.navigationItem.title=@"修改比赛";
        
        [controller setControllerBlock:^(id data) {
            
            
            if(self.cblock){
                
                self.cblock(@"");
                
            }
            
            [self loadNetWorkData];
            
        }];
        [controller setValue:match forKey:@"match"];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    if ([sender.titleLabel.text isEqualToString:@"取消应约"]){
        
        [HttpRequestHelper postWithURL:Url(@"match/cancelGauntlet") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                [SVProgressHUD showSuccessWithStatus:@"取消成功"];
                
                if(self.cblock){
                    self.cblock(@"");
                
                }
                
                
                [self loadNetWorkData];
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
            
            
        } failure:^(NSError *error) {
            
        }];
    }
   
    
    
    
    
}



@end
