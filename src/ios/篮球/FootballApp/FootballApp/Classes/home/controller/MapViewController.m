//
//  MapViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MapViewController.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <MAMapKit/MAMapKit.h>

@interface MapViewController ()<MAMapViewDelegate,AMapLocationManagerDelegate,AMapSearchDelegate,UISearchBarDelegate>
{
    
   dispatch_once_t onceToken;
    __weak IBOutlet MAMapView *_mapView;
    UISearchBar *_searchBar;
     AMapSearchAPI *searchApi;
    UITableView *_tableView;
    AMapLocationManager *locationManager;
    MAPointAnnotation *pointAnnotaiton;
    UIImageView * locationImageView;
    
    UIView *centerView;
    
    UIButton *submitBtn;
    
    UIActivityIndicatorView *activityView;
    UILabel *locationLabel;
    UILabel *addressLabel;
    
    AMapReGeocode *curnnAddress;//当前地址
}

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mapView.delegate = self;
    _mapView.showsUserLocation=NO;
    [_mapView removeAnnotations:_mapView.annotations];
    
    [self startLocation];
     searchApi=[[AMapSearchAPI alloc]init];
    searchApi.delegate=self;
   
    _searchBar=[[UISearchBar alloc]init];
    
    self.navigationItem.titleView=_searchBar;
    
    [_searchBar sizeToFit];
    _searchBar.delegate=self;
    
    
    UIImage *image=[UIImage imageNamed:@"icon_location.png"];
    locationImageView=[[UIImageView alloc]initWithImage:image];
//    locationImageView.backgroundColor=[UIColor redColor];
    
    locationImageView.frame=CGRectMake(kScreenWidth/2.0-image.size.width/2.0, (kScreenHeight-64)/2.0-image.size.height/2.0, image.size.width, image.size.height);
    
    [_mapView addSubview:locationImageView];
  
    locationImageView.hidden=YES;
    
    
    
   
    
    
    
}
-(void)startLocation{

    
    locationManager= [[AMapLocationManager alloc] init];
    
    [locationManager setDelegate:self];
    
    [locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    
    [locationManager setPausesLocationUpdatesAutomatically:NO];

    [locationManager setLocationTimeout:6];
    
    [locationManager setReGeocodeTimeout:3];
    
    [locationManager requestLocationWithReGeocode:NO completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        
        if (location) {

            [pointAnnotaiton setCoordinate:location.coordinate];
            
            [_mapView setCenterCoordinate:location.coordinate];
            locationImageView.hidden=NO;
            [_mapView setZoomLevel:15.1 animated:NO];
        }
        
        
    }];
}


#pragma mark SerchBarDelegate
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{

    searchBar.showsCancelButton=YES;
    
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        [self.view addSubview:_tableView];
        
    }
    _tableView.hidden=NO;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    [self searchBegin:searchBar.text];

}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _tableView.hidden=YES;
    searchBar.showsCancelButton=NO;
    searchBar.text=@"";
    [searchBar resignFirstResponder];

}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

    [self searchBegin:searchBar.text];
    

}
#pragma mark poi搜索
-(void)searchBegin:(NSString *)keyword{
    
    [self.dataArr removeAllObjects];
    [_tableView reloadData];
    
    if ([keyword isEqualToString:@""]) {
        return;
    }
    AMapPOIKeywordsSearchRequest *request=[[AMapPOIKeywordsSearchRequest alloc]init];
    request.keywords=keyword;
    request.city=[AccountHelper shareAccount].city;
    
   //poi搜索
    [searchApi AMapPOIKeywordsSearch:request];

}//poi搜索回调

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    
    if(response.pois.count == 0)
    {
        return;
    }
    [self.dataArr addObjectsFromArray:response.pois];
    [_tableView reloadData];
}
#pragma mark tableView delegate 
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return  self.dataArr.count;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier=@"cellIdentifier";

    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    AMapPOI * geoCode=self.dataArr[indexPath.row];
    
    cell.textLabel.text=[NSString stringWithFormat:@"%@-%@",geoCode.name,geoCode.address];
    
    return cell;
    


}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    AMapPOI * geoCode=self.dataArr[indexPath.row];
    
    NSDictionary *dic=@{@"addr":geoCode.name,@"latitude":@(geoCode.location.latitude),@"longitude":@(geoCode.location.longitude)};
    
    
    [self.view endEditing:YES];
    self.cblock(dic);
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
   

}
#pragma mark mapView delegate

//地图委托
/**
 *  地图将要发生移动时调用此接口
 *
 *  @param mapView       地图view
 *  @param wasUserAction 标识是否是用户动作
 */
- (void)mapView:(MAMapView *)mapView mapWillMoveByUser:(BOOL)wasUserAction{
    
    int w=200;
    int h=50;
    if (!centerView) {
        
        
        centerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, w, h)];
        [mapView addSubview:centerView];
        centerView.layer.cornerRadius=h/2.0;
        centerView.layer.masksToBounds=YES;
        centerView.backgroundColor=BGCOLOR2;
        
        //选择按钮
        submitBtn=[[UIButton alloc]initWithFrame:CGRectMake(w-h, 0, h, h)];
        submitBtn.backgroundColor=WERED;
        submitBtn.titleLabel.font=[UIFont systemFontOfSize:12];
        [submitBtn setTitle:@"选择" forState:UIControlStateNormal];
        [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        submitBtn.layer.cornerRadius=h/2.0;
        submitBtn.layer.masksToBounds=YES;
        submitBtn.layer.borderColor=TODAYCOLOR.CGColor;
        submitBtn.layer.borderWidth=1;
        [submitBtn addTarget:self action:@selector(submitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        activityView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        CGPoint center2=CGPointMake(h/2.0, h/2.0);
        activityView.center=center2;
        
        [centerView addSubview:activityView];
        
        [activityView startAnimating];
        
        ;
        
        locationLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, h, h)];
        locationLabel.text=@"位置";
        locationLabel.textAlignment=1;
        locationLabel.layer.cornerRadius=h/2.0;
        locationLabel.layer.masksToBounds=YES;
        locationLabel.font=[UIFont systemFontOfSize:12];
        locationLabel.textColor=[UIColor whiteColor];
        locationLabel.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
        [centerView addSubview:locationLabel];
        
        addressLabel=[[UILabel alloc]initWithFrame:CGRectMake(h, 0, w-2*h, h)];
        addressLabel.textAlignment = 1;
        //自动折行设置
        addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        addressLabel.numberOfLines = 0;
        addressLabel.text=@"定位中.....";
        
        addressLabel.font=[UIFont systemFontOfSize:12];
        addressLabel.textColor=[UIColor whiteColor];
       
        [centerView addSubview:addressLabel];
        
    
        [centerView addSubview:submitBtn];
    }
    
    centerView.alpha=.3;

    CGPoint  point =CGPointMake(locationImageView.center.x, CGRectGetMinY(locationImageView.frame)-h/2.0);
    
    centerView.center=point;
    
    locationLabel.hidden=YES;
    [activityView startAnimating];
    activityView.hidden=NO;
    addressLabel.text=@"定位中.....";
    submitBtn.userInteractionEnabled=NO;
    
    
    
    
    
}

/**
 *  地图移动结束后调用此接口
 *
 *  @param mapView       地图view
 *  @param wasUserAction 标识是否是用户动作
 */
- (void)mapView:(MAMapView *)mapView mapDidMoveByUser:(BOOL)wasUserAction{
    
    centerView.alpha=1;

    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    regeo.location = [AMapGeoPoint locationWithLatitude:_mapView.centerCoordinate.latitude  longitude:_mapView.centerCoordinate.longitude];
    regeo.requireExtension=YES;
        //发起逆地理编码
     [searchApi AMapReGoecodeSearch: regeo];
    

}

//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        curnnAddress=response.regeocode;
        submitBtn.userInteractionEnabled=YES;
        locationLabel.hidden=NO;
        [activityView stopAnimating];
        addressLabel.text=response.regeocode.formattedAddress;
        
    }
}
-(void)submitBtnClick:(id)sender{

    
    NSString *address=[curnnAddress.formattedAddress stringByReplacingOccurrencesOfString:curnnAddress.addressComponent.city withString:@""];
    
    address=[address stringByReplacingOccurrencesOfString:curnnAddress.addressComponent.province withString:@""];
    
    NSDictionary *dic=@{@"addr":address,@"latitude":@(_mapView.centerCoordinate.latitude),@"longitude":@(_mapView.centerCoordinate.longitude)};

    self.cblock(dic);
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
