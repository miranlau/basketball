//
//  MatchInfoViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MatchInfoViewController.h"
#import "ChangeView.h"
#define HEADHEIGHT 71
@interface MatchInfoViewController ()
{

    __weak IBOutlet UITableView *matchTableView;
    
    __weak IBOutlet UIImageView *leftHeadView;
    __weak IBOutlet UIImageView *rightHeadView;
    __weak IBOutlet UIButton *typeBtn1;
    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet UIImageView *line;
    UIButton *lastBtn;
    NSMutableDictionary *rowDic;

    __weak IBOutlet NSLayoutConstraint *top;
}
@end

@implementation MatchInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
    
   
    
     [self addTapToImageView:leftHeadView action:@selector(leftImageViewClick)];
     [self addTapToImageView:rightHeadView action:@selector(rightImageViewClick)];
  
    rowDic= [self witchCellForRow:2];
    
    [matchTableView reloadData];
    
    
    
    self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back" ] style:UIBarButtonItemStylePlain target:self action:@selector(backToLast:)];
}
-(void)backToLast:(id)sender{

    [self dismissViewControllerAnimated:YES completion:nil];

}
-(void)addTapToImageView:(UIImageView *)view action :(SEL)action{

    view.userInteractionEnabled=YES;
    [view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:action]];
}


-(void)leftImageViewClick{

    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)rightImageViewClick{
    
    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)loadTableView{

    matchTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    matchTableView.rowHeight=UITableViewAutomaticDimension;
    matchTableView.estimatedRowHeight=100;

}
- (IBAction)btnClick:(UIButton *)sender {
    
    //防止多次点击
    if (lastBtn==sender) {
        return;
    }
    
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/5*(sender.tag-1001);
        [line layoutIfNeeded];
        
        
    }];
    lastBtn=sender;
   rowDic= [self witchCellForRow:sender.tag];
    
    [matchTableView reloadData];
    
    
   
    
}
-(NSMutableDictionary *)witchCellForRow:(NSInteger)tag{
    
     NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    

    
        [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:@"begin" image:@"" reuseIdentifier:@"ChartCell5"],
                                                           [BaseCellModel setModel:@"" key:@"" value:@"goal" image:@"" reuseIdentifier:@"ChartCell5"]
                                                           ,
                                                           [BaseCellModel setModel:@"" key:@"" value:@"card" image:@"" reuseIdentifier:@"ChartCell5"]
                                                           ,
                                                           [BaseCellModel setModel:@"" key:@"" value:@"goal" image:@"" reuseIdentifier:@"ChartCell5"]
                                                           ,
                                                           [BaseCellModel setModel:@"" key:@"" value:@"change" image:@"" reuseIdentifier:@"ChartCell5"]
                                                           ,
                                                           [BaseCellModel setModel:@"" key:@"" value:@"end" image:@"" reuseIdentifier:@"ChartCell5"]
                                                           
                                                           ]];
//    }
//    
   
    
    
    
    
    
    
    
    
    
    
    return dic;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];

    cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
     [cell setValue:model forKey:@"data"];
    
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];

    
    if([model.reuseIdentifier isEqualToString:@"ChartCell5"]){
        
        if (![model.value isEqualToString:@"begin"]&&![model.value isEqualToString:@"end"]) {
            
            [[[ChangeView alloc]initWithBlock:^(id data) {
                
                if ( [data intValue]==1) {
                    
                    
                    
                    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MenberViewController"];
                    
                    [self.navigationController pushViewController:controller animated:YES];
                    
                }else{
                    
                    
                    
                }
                
                
                
            }] show];
            
        }
    
        
    
    
    }
    
    
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 保证是我们的tableivew
    
    
    if (scrollView == matchTableView) {
        // 保证我们是垂直方向滚动，而不是水平滚动
        if (scrollView.contentOffset.x == 0) {
            CGFloat y = scrollView.contentOffset.y;
            
            // 这个是非常关键的变量，用于记录上一次滚动到哪个偏移位置
            static CGFloat previousOffsetY = 0;
            
            // 向上滚动
            if (y > 0) {
                if (top.constant+HEADHEIGHT <= 0) {
                    return;
                }
                // 计算两次回调的滚动差:fabs(y - previousOffsetY)值
                CGFloat bottomY = top.constant+HEADHEIGHT - fabs(y - previousOffsetY);
                bottomY = bottomY >= 0 ? bottomY : 0;
                
                top.constant=bottomY-HEADHEIGHT;
                previousOffsetY = y;
                
                // 如果一直不松手滑动，重复向上向下滑动时，如果没有设置还原为0，则会出现马上到顶的情况。
                if (previousOffsetY >= HEADHEIGHT) {
                    previousOffsetY = 0;
                }
            }
            // 向下滚动
            else if (y < 0) {
                if (top.constant >= 0) {
                    return;
                }
                CGFloat bottomY = top.constant+HEADHEIGHT + fabs(y);
                bottomY = bottomY <= HEADHEIGHT ? bottomY : HEADHEIGHT+10;
                
                top.constant=bottomY-HEADHEIGHT;
            }
        }
        
        
    }
    
}
@end
