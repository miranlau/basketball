//
//  MenberViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MenberViewController.h"
#import "MatchModel1.h"
#import "MenberCell.h"

#import "MatchHelper.h"
@interface MenberViewController ()
{

    __weak IBOutlet UITableView *menberTableView;


}
@end

@implementation MenberViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
     [self loadnetUrlInfo];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTableView];
    [self loadnetUrlInfo];
}
- (void)loadnetUrlInfo
{

//    NSLog(@"%@,%@,%@",_teamId,_matchId,[AccountHelper shareAccount].token);
    
    [HttpRequestHelper postWithURL:Url(@"game/queryAll") params:@{@"teamId":_teamId,@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {

        
        if (sucess)
        {
            [self.dataArr removeAllObjects];
            NSArray *array = responseObject;
            for (int i = 0; i < array.count; i++)
            {
                MatchModel1 *model=[[MatchModel1 alloc]initWithDic:array[i]];
                [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MenberCell"]];
            }
            [menberTableView.mj_header endRefreshing];
            [menberTableView.mj_footer endRefreshing];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                  [menberTableView reloadData];
                
            });
            
          
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
        [menberTableView.mj_header endRefreshing];
        [menberTableView.mj_footer endRefreshing];
    }];
    

}
-(void)loadTableView{
    
   
    
    menberTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    menberTableView.rowHeight=UITableViewAutomaticDimension;
    
    menberTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    menberTableView.backgroundView=bgImageView;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    BaseCellModel *modelB=self.dataArr[indexPath.row];
    MatchModel1 *modle = modelB.value;
    MenberCell *cell=[tableView dequeueReusableCellWithIdentifier:modelB.reuseIdentifier forIndexPath:indexPath];
    
    if (indexPath.row%2) {
        
        
        
        cell.backgroundColor=BGCOLOR1;
        
        
    }else{
        cell.backgroundColor=BGCOLOR2;
    }
 
    [cell.headImageView setImageWithURL:[NSURL URLWithString:modle.avatar] placeholderImage:HeadImage];
    cell.headImageView.layer.cornerRadius = 76 / 2;
    cell.headImageView.layer.masksToBounds = YES;
    cell.headImageView.clipsToBounds = YES;
    if (modle.realName)
    {
        cell.name.text = modle.realName;
    }
    else if(modle.name)
    {
        cell.name.text = modle.name;
    }
    else
    {
         cell.name.text = @"";
    }
   
    cell.num.text = [NSString stringWithFormat:@"球衣号码:%@",modle.number];

    cell.leaderImage.hidden=YES;
    
    if([modle.isLeader intValue] == 1)
    {
        cell.leaderImage.hidden=NO;
    }
    
   
    
  
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BaseCellModel *modelB=self.dataArr[indexPath.row];
    MatchModel1 *modle = modelB.value;
   
    
    if (self.teamId)
    {
        BaseViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangePlayerNumber"];
        [controller setValue:self.teamId  forKey:@"teamId"];
        [controller setValue:self.matchId  forKey:@"matchId"];
        [controller setValue:modle.id  forKey:@"userId"];
        controller.navigationItem.title = @"修改号码";

        //这里返回修改后的球衣号
        [controller setControllerBlock:^(id data) {
            
            [self loadnetUrlInfo];
        }];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath

{
    
    return  UITableViewCellEditingStyleDelete;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
       
    }
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseCellModel *modelB=self.dataArr[indexPath.row];
    MatchModel1 *modle = modelB.value;
    
    //设置删除按钮
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction*action,NSIndexPath *indexPath) {
//        NSLog(@"userId = %@",modle.id);
        [self DeleMemberPlayer:modle.id];

        [tableView setEditing:NO animated:YES];
           }];
    deleteRowAction.backgroundColor = [UIColor redColor];
    return  @[deleteRowAction];
}
//http://192.168.1.100:8080/football-api/ctl/game/queryAll?matchId=203408&teamId=200607
//http://192.168.1.100:8080/football-api/ctl/game/deletePlayer?matchId=203408&teamId=200607&userId=1018209

- (void)DeleMemberPlayer:(NSString *)userId
{
//    NSLog(@"url = %@ ,%@",Url(@"game/deletePlayer"),userId);
    [HttpRequestHelper postWithURL:Url(@"game/deletePlayer") params:@{@"teamId":self.teamId,@"matchId":self.matchId,@"userId":userId} success:^(BOOL sucess, id responseObject) {
        
        if (sucess)
        {
            [self loadnetUrlInfo];
            [menberTableView reloadData];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
        [menberTableView.mj_header endRefreshing];
        [menberTableView.mj_footer endRefreshing];
    }];
    
    
}

- (IBAction)AddNewPlayerClicked:(id)sender {
    if (self.teamId)
    {
   
        BaseViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"AddMemberViewController"];
        [controller setValue:self.teamId  forKey:@"teamId"];
        [controller setValue:self.matchId  forKey:@"matchId"];
        controller.navigationItem.title = @"选择球员";
        
        [controller setControllerBlock:^(id data) {
            
            [self loadnetUrlInfo];
        }];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
         [SVProgressHUD showErrorWithStatus:@"球队ID为空"];
    }
}
- (IBAction)nextBtnClick:(id)sender {
    
    
//    [MatchHelper  MatchInfo:@{@"matchId":_matchId,@"teamId":_teamId} success:^(id data) {
    
//        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ActionViewController"];
//        [controller setValue:self.matchId forKey:@"matchId"];
//        
//        [controller setValue:_teamId forKey:@"teamId"];
//        
//        [controller setValue:@(1010) forKey:@"actionType"];
        
        
//        if ([[MatchHelper shareMatch].match.statring count]>0) {
//            
//            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"OnLiveViewController"];
//            [controller setValue:self.matchId forKey:@"matchId"];
//            
//            [controller setValue:_teamId forKey:@"teamId"];
//            
////            [controller setValue:@NO forKey:@"isChange"];
//            
//            
//            
//            [self presentViewController:controller animated:YES completion:nil];
//            
//            
//        }else{
//        
//        
//            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SureLineUpViewController"];
//            [controller setValue:self.matchId forKey:@"matchId"];
//            
//            [controller setValue:_teamId forKey:@"teamId"];
//            
//            [controller setValue:@NO forKey:@"isChange"];
//            
//            
//            
//            [self presentViewController:controller animated:YES completion:nil];
//        }
    
    if (self.cblock) {
        self.cblock(@"");
    }
    
        
        [self.navigationController popViewControllerAnimated:YES];
       
        
//    } failure:^(id data) {
//        
//        [SVProgressHUD showErrorWithStatus:data];
//    }];
 
}

@end
