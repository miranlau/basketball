//
//  NoteViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/18.
//  Copyright © 2016年 North. All rights reserved.
//

#import "NoteViewController.h"
@interface NoteViewController()<UITextViewDelegate>
{
    
    __weak IBOutlet UILabel *messageLabel;
    __weak IBOutlet UITextView *notetextView;
    
}

@end
@implementation NoteViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    
    notetextView.text=_value;
    [notetextView becomeFirstResponder];
    if ([notetextView.text isEqualToString:@""]) {
        messageLabel.hidden=NO;
    }else{
        messageLabel.hidden=YES;
    }
    
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(textViewChange:) name:UITextViewTextDidChangeNotification object:notetextView];

    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnClick:)];
    
    
}
-(void)textViewChange:(NSNotification *)sender{
    
    
    UITextView *textView = (UITextView *)sender.object;

    NSString *toBeString = textView.text;
   // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {

            if (toBeString.length > 200) {
                
                textView.text = [toBeString substringToIndex:200];
  
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
            
        }
  
    

}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    messageLabel.hidden=YES;

}
-(void)textViewDidEndEditing:(UITextView *)textView{

    if ([textView.text isEqualToString:@""]) {
         messageLabel.hidden=NO;
    }else{
        messageLabel.hidden=YES;
    
    }

}
-(void)saveBtnClick:(id)sender{
    
    [self.view endEditing:YES];
    
    
    if ([notetextView.text isEqualToString:@""]) {
        
        [SVProgressHUD showErrorWithStatus:@"留言不能为空"];
        return;
        
    }
    
    
    
    self.cblock(notetextView.text);
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
@end
