//
//  OnLiveViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface OnLiveViewController : BaseViewController
@property(strong ,nonatomic)NSString *matchId;
@property(strong ,nonatomic)NSString *teamId;
@property(assign ,nonatomic)BOOL fromFristView;//是否从首发页面过来
@end
