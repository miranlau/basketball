//
//  OnLiveViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "OnLiveViewController.h"
#import "EndView.h"
#import "MatchHelper.h"
#import "TeamModel.h"
#import "XHToast.h"
//#import "MXMarqueeView.h"
#import "NavViewController.h"
#import "messageTuishong.h"
#import <AudioToolbox/AudioToolbox.h>

@interface OnLiveViewController ()
{
    
    __weak IBOutlet UILabel *actitle;
    __weak IBOutlet UIButton *beginBtn;
    __weak IBOutlet UIButton *stopBtn;
    
    __weak IBOutlet NSLayoutConstraint *right1;
    __weak IBOutlet NSLayoutConstraint *right2;
    
    __weak IBOutlet UILabel *timeLabel;
    __weak IBOutlet UILabel *sorce;
    
    __weak IBOutlet UIImageView *homeImage;
    __weak IBOutlet UILabel *homeName;
    __weak IBOutlet UIImageView *visImage;
    
    __weak IBOutlet UILabel *visName;
    
    __weak IBOutlet UILabel *stuName;
    
     dispatch_once_t onceToken;
    NSTimer *timer;
    
    __weak IBOutlet UIButton *shotBtn;
    BOOL isLong;//是否加时赛
    BOOL isShot;//是否点球大战
    int currnTime;//当前时间
    
//    MXMarqueeView *paoMaView;
    
    
    
    
}

@end

@implementation OnLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [MatchHelper shareMatch].match.id=_matchId;
    
    
    
    timeLabel.text=@"00'00\"";
    for(UIView *view in self.view.subviews){
        view.layer.cornerRadius=5;
        view.layer.masksToBounds=YES;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecomeActive:) name:@"appBecomeActive" object:nil];
    
    [self loadMatchLiveInfo];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecomeActive:) name:@"matchChange" object:nil];
    
    
}

-(void)matchEndAction{
    
    
    for (UIView *view  in [UIApplication sharedApplication].keyWindow.subviews) {
        
        if ([view isKindOfClass:[EndView class]]) {
            
            [view removeFromSuperview];
        }
    }
    
    
    [AccountHelper shareAccount].isBackHome=YES;
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];

    [UIApplication sharedApplication].keyWindow.rootViewController =controller;

    



}
-(void)loadnetWorkData:(MutableBlock)block{
    
    [MatchHelper shareMatch].match=nil;

    [MatchHelper  MatchInfo:@{@"matchId":[MatchHelper shareMatch].match.id,@"teamId":_teamId} success:^(id data) {
        
        [self loadMatchLiveInfo];
        
        block(@"");
        
    } failure:^(id data) {
        
        [XHToast showTopWithText:@"data"];
    }];

}

//加载直播中的信息

-(void)loadMatchLiveInfo{
   
    dispatch_async(dispatch_get_main_queue(), ^{
        
        sorce.text=[NSString stringWithFormat:@"%@:%@",[MatchHelper shareMatch].match.homeScore,[MatchHelper shareMatch].match.visitingScore];
        
        TeamModel *home=[[ TeamModel alloc]initWithDic:[MatchHelper shareMatch].match.home];
        TeamModel *vis=[[ TeamModel alloc]initWithDic:[MatchHelper shareMatch].match.visiting];
        
        
        homeName.text=[NSString stringWithFormat:@"%@",home.name];
        
        [visImage setImageWithURL:[NSURL URLWithString:vis.logo] placeholderImage:TeamLogo];
        visName.text=[NSString stringWithFormat:@"%@",vis.name];
        
        [homeImage setImageWithURL:[NSURL URLWithString:home.logo] placeholderImage:TeamLogo];
        
        if (vis.name ==nil) {
            visImage.hidden=YES;
            visName.hidden=YES;
        }
        
        //记分员所在的队伍或者计分的队伍为红色
        if ([_teamId intValue]== [[MatchHelper shareMatch].match.home[@"id"] intValue]) {
            homeName.textColor=[UIColor redColor];
            visName.textColor=[UIColor whiteColor];
        }else{
            visName.textColor=[UIColor redColor];
            homeName.textColor=[UIColor whiteColor];
            
        }
        
        
        
        
        homeImage.layer.cornerRadius=20;
        homeImage.layer.masksToBounds=YES;
        visImage.layer.cornerRadius=20;
        visImage.layer.masksToBounds=YES;
        
        
        if ([MatchHelper shareMatch].match.status ==100) {
            
            
            [self matchEndAction];
            
            
        }
        
        
        if ([MatchHelper shareMatch].match.status == 0) {
            
            [self beginPaoMa:@"比赛还没开始"];
            
            
            for(UIView *view in self.view.subviews){
                if([view isKindOfClass:[UIButton class]] && view.tag !=1010){
                    
                    UIButton *butn =(UIButton *)view;
                    butn.userInteractionEnabled=NO;
                    
                    
                    butn.backgroundColor=[butn.backgroundColor colorWithAlphaComponent:.5];
                    
                    
                }
                
            }
            
        }else{
            
            
            for(UIView *view in self.view.subviews){
                if([view isKindOfClass:[UIButton class]] && view.tag !=1010){
                    
                    UIButton *butn =(UIButton *)view;
                    butn.userInteractionEnabled=YES;
                    butn.backgroundColor=[butn.backgroundColor colorWithAlphaComponent:1];
                    
                    
                    
                }
                
            }
            
        }
        
        
        //比赛已经开始
        if ([MatchHelper shareMatch].match.status >0) {
            
            if ([self.dataArr count]==0) {
                
                [self beginPaoMa:@"比赛中"];
                
            }
            
            
            
            
            
            
            right1.priority=250;
            right2.priority=750;
            
            currnTime=[MatchHelper shareMatch].match.duration;
            
            //如果比赛暂停
            if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
                [UIView animateWithDuration:.3 animations:^{
                    right1.priority=250;
                    right2.priority=750;
                    
                    [self.view layoutIfNeeded];
                    
                    
                    [beginBtn setTitle:@"继续" forState:0];
                    
                    timeLabel.textColor=[UIColor redColor];
                    
                    
                }];
                
                
                [timer invalidate];
                timer =nil;
                
                switch ([MatchHelper shareMatch].match.status) {
                    case 10://上半场
                        
                        stuName.text=@"上半场";
                        
                        [stopBtn setTitle:@"中场休息" forState:0];
                        
                        
                        break;
                        
                    case 11://中场
                        
                        [stopBtn setTitle:@"下半场开始" forState:0];
                        stuName.text=@"中场休息";
                        [timer invalidate];
                        timer =nil;
//                        timeLabel.textColor=[UIColor redColor];
                        
                        break;
                        
                    case 12://下场
                        stuName.text=@"下半场";
                        [stopBtn setTitle:@"结束比赛" forState:0];
//                        timeLabel.textColor=[UIColor whiteColor];
                        
                        break;
                    case 20://上半场
                        
                        stuName.text=@"加时赛上半场";
                        
                        [stopBtn setTitle:@"加时赛中场休息" forState:0];
                        isLong=YES;
                        
                        break;
                        
                    case 21://中场
                        stuName.text=@"加时赛中场休息";
                        [stopBtn setTitle:@"加时赛下半场开始" forState:0];
//                        timeLabel.textColor=[UIColor redColor];
                        [timer invalidate];
                        timer =nil;
                        isLong=YES;
                        
                        break;
                        
                    case 22://下场
                        stuName.text=@"加时赛下半场";
                        [stopBtn setTitle:@"结束比赛" forState:0];
//                        timeLabel.textColor=[UIColor whiteColor];
                        isLong=YES;
                        break;
                        
                        
                    case 30://下场
                        stuName.text=@"点球大战";
                        
                        [shotBtn setTitle:@"点球" forState:0];
                        [stopBtn setTitle:@"结束比赛" forState:0];
                        
                        
                        isShot=YES;
                        break;
                        
                    default:
                        break;
                }
                
                
                
                
            }else{
                
                [beginBtn setTitle:@"暂停" forState:0];
                timeLabel.textColor=[UIColor whiteColor];
                [timer invalidate];
                timer =nil;
                
                timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange:) userInfo:nil repeats:YES];
                
                
                [UIView animateWithDuration:.3 animations:^{
                    right1.priority=250;
                    right2.priority=750;
                    
                    [self.view layoutIfNeeded];
                    [beginBtn setTitle:@"暂停" forState:0];
                    
                }];
                
                
                switch ([MatchHelper shareMatch].match.status) {
                    case 10://上半场
                        
                        stuName.text=@"上半场";
                        
                        [stopBtn setTitle:@"中场休息" forState:0];
                         timeLabel.textColor=[UIColor whiteColor];
                        
                        break;
                        
                    case 11://中场
                        
                        [stopBtn setTitle:@"下半场开始" forState:0];
                        stuName.text=@"中场休息";
                        timeLabel.textColor=[UIColor redColor];
                        
                        [timer invalidate];
                        timer =nil;
                        break;
                        
                    case 12://下场
                        stuName.text=@"下半场";
                        [stopBtn setTitle:@"结束比赛" forState:0];
                        timeLabel.textColor=[UIColor whiteColor];
                        
                        break;
                    case 20://上半场
                        
                        stuName.text=@"加时赛上半场";
                        isLong=YES;
                        [stopBtn setTitle:@"加时赛中场休息" forState:0];
                         timeLabel.textColor=[UIColor whiteColor];
                        
                        
                        break;
                        
                    case 21://中场
                        stuName.text=@"加时赛中场休息";
                        [stopBtn setTitle:@"加时赛下半场开始" forState:0];
                        timeLabel.textColor=[UIColor redColor];
                        isLong=YES;
                        [timer invalidate];
                        timer =nil;
                        
                        break;
                        
                    case 22://下场
                        stuName.text=@"加时赛下半场";
                        [stopBtn setTitle:@"结束比赛" forState:0];
                        timeLabel.textColor=[UIColor whiteColor];
                        isLong=YES;
                        break;
                        
                        
                    case 30://下场
                        stuName.text=@"点球大战";
                        [shotBtn setTitle:@"点球" forState:0];
                        [stopBtn setTitle:@"结束比赛" forState:0];
                        isShot=YES;
                        
                        [timer invalidate];
                        timer =nil;
                        
                        break;
                        
                    default:
                        break;
                }
                
            }
            
        }
        
        if ([MatchHelper shareMatch].match.status>0) {
            if ([MatchHelper shareMatch].match.status!=30) {
                 [self setTime:[MatchHelper shareMatch].match.duration];
            }else{
                currnTime=0;
                [self setTime:currnTime];
            }
            
           
            
            
            
        }else{
            
            stuName.text=@"未开始";
        }
    });
    
   
        
   
    
}

-(void)beginPaoMa:(NSString *)text{
    actitle.text=text;


}

- (IBAction)btnClick:(UIButton *)sender {
    
    
//    [self beginPaoMa:sender.currentTitle];
    
    
      NSString *currnTtile= sender.currentTitle;
    
    if (sender.tag<1010) {
        
         AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        //换人
        if (sender.tag==1009) {
            
         
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangeViewController"];
            [controller setValue:self.matchId forKey:@"matchId"];
            
            [controller setValue:_teamId forKey:@"teamId"];
            
            [controller setValue:@YES forKey:@"isChange"];
            
            
            [controller setControllerBlock:^(id data) {
                
                [self.dataArr addObject:data];
                
                [self beginPaoMa:[self.dataArr lastObject]];
                
                [self loadnetWorkData:^(id data2) {
                    
                    
                    
                }];
            }];
            
            
            
            
            
            
            
            [self presentViewController:controller animated:YES completion:nil];
            
            
            return;
        }
        
        
        if ([currnTtile isEqualToString:@"点球"]) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ShotViewController"];
            [controller setValue:self.matchId forKey:@"matchId"];
            
            [controller setValue:_teamId forKey:@"teamId"];

            [controller setControllerBlock:^(id data) {
                
                [self.dataArr addObject:data];
                
                [self beginPaoMa:[self.dataArr lastObject]];
                
                [self loadnetWorkData:^(id data2) {
                    
                    currnTime=0;
                    
                    [self setTime:currnTime];
                    [timer invalidate];
                    timer =nil;
                    
                    
                    
                }];
            }];
            
            
            
            
            
            
            
            [self presentViewController:controller animated:YES completion:nil];
            
            
            return;
        }
        //点球大战不允许
        
        if([MatchHelper shareMatch].match.status == 30){
        
            return;
        }
        
       
        
        if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
            if (sender.tag!=1009) {
                 return;
            }
           
        }
        
        if ([MatchHelper shareMatch].match.status==0) {
            if (sender.tag!=1009) {
                return;
            }
        }
        
        if ([MatchHelper shareMatch].match.status==11||[MatchHelper shareMatch].match.status==11) {
            if (sender.tag!=1009) {
                return;
            }
        }
        
        
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ActionViewController"];
        
        [controller setValue:@(sender.tag) forKey:@"actionType"];
        
        [controller setValue:_matchId forKey:@"matchId"];
        
        [controller setValue:_teamId forKey:@"teamId"];
        
        
        
        [controller setControllerBlock:^(id data) {
            
            if ([currnTtile isEqualToString:@"射门"]) {
                [self.dataArr addObject:data];
            }else{
            
                [self.dataArr addObject:currnTtile];
            
            }
            
            
            
            
            [self beginPaoMa:[self.dataArr lastObject]];
            
            if ([sender.currentTitle isEqualToString:@"黄牌"]||[sender.currentTitle isEqualToString:@"红牌"]||[sender.currentTitle isEqualToString:@"射门"]||[sender.currentTitle isEqualToString:@"换人"]) {
                
                [self loadnetWorkData:^(id data2) {
                    
 
                    
                }];
            }
            
        }];
        
        
       
        
 
        [self presentViewController:controller animated:YES completion:nil];
        
        
    }else{
        
        
        [AccountHelper shareAccount].isShow=YES;
        
//        if ([currnTtile isEqualToString:@"传球"]) {
//             [AccountHelper shareAccount].isLive=YES;
//        }
       
        
        
      
        if (![currnTtile isEqualToString:@"撤销操作"]) {
              [self.dataArr addObject:sender.currentTitle];
        }
        
      
        
        if (![sender.currentTitle isEqualToString:@"结束比赛"]) {
            
//            //点球大战不允许点
//            if([MatchHelper shareMatch].match.status == 30){
//                
//                return;
//            }
            
            
            
            
            NSString *status=nil;
            NSString *message=sender.currentTitle;
            
            if( [sender.currentTitle isEqualToString:@"开始比赛"]){
                
                stuName.text=@"上半场";
              
                [MatchHelper shareMatch].match.status=10;
                status=@"11";
                
                [UIView animateWithDuration:.3 animations:^{
                    right1.priority=250;
                    right2.priority=750;
                    
                    [self.view layoutIfNeeded];
                    [beginBtn setTitle:@"暂停" forState:0];
                    timeLabel.textColor=[UIColor whiteColor];
                    
                }];
                
                for(UIView *view in self.view.subviews){
                    if([view isKindOfClass:[UIButton class]] && view.tag !=1010){
                        
                        UIButton *butn =(UIButton *)view;
                        butn.userInteractionEnabled=YES;
                        butn.backgroundColor=[butn.backgroundColor colorWithAlphaComponent:2];
  
                    }
                    
                }
              
                
                
                
            }
            else
            if( [sender.currentTitle isEqualToString:@"暂停"]){
                
                
                
                if ([MatchHelper shareMatch].match.status==0||[MatchHelper shareMatch].match.status==30) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==11||[MatchHelper shareMatch].match.status==21) {
                    return;
                }
                
                timeLabel.textColor=[UIColor redColor];
                
                status=@"55";
                
                [MatchHelper shareMatch].match.isSuspend=@"1";
                [beginBtn setTitle:@"继续" forState:0];
                
                
                
            }
            else
            if( [sender.currentTitle isEqualToString:@"继续"]){
                
                status=@"56";
                timeLabel.textColor=[UIColor whiteColor];
                [MatchHelper shareMatch].match.isSuspend=@"0";
                [beginBtn setTitle:@"暂停" forState:0];
                
                
            } else
                
            if( [sender.currentTitle isEqualToString:@"下半场开始"]){
               
                status=@"58";
               
                
                
            }else
            //待定------------
            if([sender.currentTitle isEqualToString:@"加时赛中场休息"]){
               
                status=@"61";
                
                
                
            }else
            if( [sender.currentTitle isEqualToString:@"加时赛下半场开始"]){
               
                status=@"62";
                
                
                
            }
            //待定------------
            
            else
            if( [sender.currentTitle isEqualToString:@"角球"]){
                
                if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==0) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==11||[MatchHelper shareMatch].match.status==11) {
                    return;
                }
                status=@"36";
                
                
            }
            else
            if( [sender.currentTitle isEqualToString:@"传球"]){
                
                if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==0) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==11||[MatchHelper shareMatch].match.status==11) {
                    return;
                }
                status=@"37";
                
                
            }
            else
            if( [sender.currentTitle isEqualToString:@"任意球"]){
                
                if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==0) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==11||[MatchHelper shareMatch].match.status==11) {
                    return;
                }
                status=@"35";
                
                
            }else if( [sender.currentTitle isEqualToString:@"撤销操作"]){
                
                if ([MatchHelper shareMatch].match.status==0) {
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==11) {
                    
                    return;
                }
                
                if ([MatchHelper shareMatch].match.status==21) {
                    return;
                }
                
//                if ([MatchHelper shareMatch].match.status==30) {
//                    return;
//                }
                
                 status=@"120";
  
                
            }else if ([sender.currentTitle isEqualToString:@"中场休息"]) {
                status=@"57";
            }
            
            if( [sender.currentTitle isEqualToString:@"中场休息"]||[sender.currentTitle isEqualToString:@"加时赛中场休息"]||[sender.currentTitle isEqualToString:@"加时赛下半场开始"]||[sender.currentTitle isEqualToString:@"下半场开始"]){
                
               
                
                
                NSString *titl=[NSString stringWithFormat:@"是否确定%@",sender.currentTitle];
                
                
                [[[FBAlertView alloc]initWithTitle:titl butnTitlt:@"确定" block:^(id data) {
                    
                    if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
                        return;
                    }
                    
                    
                    if ([currnTtile isEqualToString:@"中场休息"]) {
                        timeLabel.textColor=[UIColor redColor];
                        
                        
                        stuName.text=@"中场休息";
                        [MatchHelper shareMatch].match.status=11;
                        
                        [stopBtn setTitle:@"下半场开始" forState:0];
                    }
                    
                    if( [currnTtile isEqualToString:@"加时赛下半场开始"]){
                        timeLabel.textColor=[UIColor whiteColor];
                        
                        stuName.text=@"加时赛下半场";
                        [MatchHelper shareMatch].match.status=22;
                        
                        [stopBtn setTitle:@"结束比赛" forState:0];
                        
                        
                    }
                    if([currnTtile isEqualToString:@"加时赛中场休息"]){
                        stuName.text=@"加时赛中场休息";
                        
                        timeLabel.textColor=[UIColor redColor];
                        [MatchHelper shareMatch].match.status=21;
                        
                        [stopBtn setTitle:@"加时赛下半场开始" forState:0];
                        
                        
                    }
                    if( [currnTtile isEqualToString:@"下半场开始"]){
                        stuName.text=@"下半场";
                        
                        timeLabel.textColor=[UIColor whiteColor];
                        [MatchHelper shareMatch].match.status=12;
                        
                        [stopBtn setTitle:@"结束比赛" forState:0];
                        
                        
                    }
                    
                    NSDictionary  * dic =[MatchHelper playerA:nil playerB:nil status:status matchId:_matchId teamId:_teamId];
                    
                    [MatchHelper matchActionRequest:dic success:^(id data) {
                        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                        if([message isEqualToString:@"撤销操作"]){
                            
                            [self.dataArr removeLastObject];
                            
                            if([self.dataArr count]==0){
                                
                                [self.dataArr addObject: @"比赛中"];
                                
                            }
                            
                            
                            
                            [self  beginPaoMa:[self.dataArr lastObject]];
                            
                            [self loadnetWorkData:^(id data) {
                                
                            }];
                        }
                        
                        [self  beginPaoMa:[self.dataArr lastObject]];
                        
                        
                        if ([message isEqualToString:@"暂停"]||[message isEqualToString:@"中场休息"]||[message isEqualToString:@"加时赛中场休息"]||[message isEqualToString:@"点球大战"]){
                            
                            [timer invalidate];
                            timer =nil;
                            
                        }else{
                            
                            
                            
                            
                            if ([MatchHelper shareMatch].match.status>0) {
                                [timer invalidate];
                                timer =nil;
                                timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange:) userInfo:nil repeats:YES];
                            }
                            
                            if ([message isEqualToString:@"下半场开始"]||[message isEqualToString:@"加时赛下半场开始"]) {
                                
                                currnTime=0;
                                
                                [self setTime:currnTime];
                                
                            }
                            
                            
                        }
                        
                        if(![message isEqualToString:@"传球"]){
                            [XHToast showBottomWithText:[NSString stringWithFormat:@"%@成功",message]];
                            
                        }
                        
                        
                    } failure:^(id data) {
                        
                        [XHToast showBottomWithText:[NSString stringWithFormat:@"%@失败",data]];
                        
                    }];
                    
                    
                }]show];
                
               
                
            }else{
            
            
            
            
            NSDictionary  * dic =[MatchHelper playerA:nil playerB:nil status:status matchId:_matchId teamId:_teamId];
            
            [MatchHelper matchActionRequest:dic success:^(id data) {
                
                  AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                if([message isEqualToString:@"撤销操作"]){
                    
                    [self.dataArr removeLastObject];
                    
                    if([self.dataArr count]==0){
                    
                        [self.dataArr addObject: @"比赛中"];
                    
                    }
                    
                    
                
                    [self  beginPaoMa:[self.dataArr lastObject]];
                    
                    [self loadnetWorkData:^(id data) {
                        
                    }];
                }
                
                    [self  beginPaoMa:[self.dataArr lastObject]];
                
                
                if ([message isEqualToString:@"暂停"]||[message isEqualToString:@"中场休息"]||[message isEqualToString:@"加时赛中场休息"]||[message isEqualToString:@"点球大战"]){
                    
                      [timer invalidate];
                       timer =nil;
                
                }else{
                    
                    
                    
                    
                    if ([MatchHelper shareMatch].match.status>0) {
                        [timer invalidate];
                        timer =nil;
                         timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange:) userInfo:nil repeats:YES];
                    }
                    
                    if ([message isEqualToString:@"下半场开始"]||[message isEqualToString:@"加时赛下半场开始"]) {
                        
                        currnTime=0;
                        
                        [self setTime:currnTime];
                        
                    }
               
                
                }
                
              if(![message isEqualToString:@"传球"]){
                    [XHToast showBottomWithText:[NSString stringWithFormat:@"%@成功",message]];
                
              }
             
                
            } failure:^(id data) {
                
                 [XHToast showBottomWithText:[NSString stringWithFormat:@"%@失败",data]];
                
            }];
            
            }
            
            
            
            
            
        }else{
            
            
            
            if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
                return;
            }
            
            if ([MatchHelper shareMatch].match.status==0) {
                return;
            }
            
            if ([MatchHelper shareMatch].match.status==11||[MatchHelper shareMatch].match.status==11) {
                return;
            }
            
            //如果主队分不等于客队分
            
          
            
            
            
            if([[MatchHelper shareMatch].match.homeScore intValue]!=[[MatchHelper shareMatch].match.visitingScore intValue]||isShot==YES){
                
                [[[FBAlertView alloc]initWithTitle:@"是否确认结束比赛" butnTitlt:@"确认" block:^(id data) {
                    
                      [SVProgressHUD showWithStatus:@"结束比赛中...."];
                    [MatchHelper matchActionRequest:@{@"matchId":_matchId,@"teamId":_teamId,@"status":@"100"} success:^(id data) {
                        
                        [timer invalidate];
                         AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                        [AccountHelper shareAccount].isLive=NO;
                        
                        [SVProgressHUD dismiss];
                          [self matchEndAction];
                        
                        
                        
                        
                        
                        
                        
                    } failure:^(id data) {
                        
                        
                        
                        [XHToast showBottomWithText:@"结束比赛失败"];
                    }];
                    
                }]show];
                
                
                
                
               
                
                
                
            
            }else{
            
                
                if (isLong==YES&& isShot==YES) {
                    [[[FBAlertView alloc]initWithTitle:@"是否确认结束比赛" butnTitlt:@"确认" block:^(id data) {
                        
                        [MatchHelper matchActionRequest:@{@"matchId":_matchId,@"teamId":_teamId,@"status":@"100"} success:^(id data) {
                            
                            [timer invalidate];
                            
                            [AccountHelper shareAccount].isLive=NO;
                            
                            [self matchEndAction];
                            
                            //如果是从首发界面过来
                            //                             [SVProgressHUD showWithStatus:@"结束中...."];
                            //                                if (_fromFristView ==YES) {
                            //
                            //                                    [self dismissViewControllerAnimated:NO completion:nil];
                            //
                            //                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissToRoot" object:nil];
                            //                                }
                            //                                else{
                            //
                            //
                            //                                    [self dismissViewControllerAnimated:NO completion:nil];
                            //
                            //                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissToRoot2" object:nil];
                            //
                            //                                }
                            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                            
                        } failure:^(id data) {
                            
                            
                            
                            [XHToast showBottomWithText:@"结束比赛失败"];
                        }];
                        
                        
                    }]show];
                    
                }else{
                
                 
                 
                 
                    EndView *endView =   [[EndView alloc]initWithBlock:^(id data) {
                    
                    if ([data intValue]==1001) {
                        
                        
                        [[[FBAlertView alloc]initWithTitle:@"是否确认加时赛" butnTitlt:@"确认" block:^(id data) {
                            [MatchHelper matchActionRequest:@{@"matchId":_matchId,@"teamId":_teamId,@"status":@"60"} success:^(id data) {
                                
                                stuName.text=@"加时赛上半场";
                                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                                //                            [XHToast showBottomWithText:@"加时赛开始"];
                                currnTime=0;
                                
                                [self setTime:currnTime];
                                [stopBtn setTitle:@"加时赛中场休息" forState:0];
                                
                                
                                
                                isLong=YES;//已经加时赛
                                
                            } failure:^(id data) {
                                
                                [XHToast showBottomWithText:@"加时赛失败"];
                                
                                
                                
                            }];
                        }]show];
                        
                        
                        
                    }
                    
                    if ([data intValue]==1002) {
                        
                        
                        [[[FBAlertView alloc]initWithTitle:@"是否确认点球大战" butnTitlt:@"确认" block:^(id data) {
                            
                            [MatchHelper matchActionRequest:@{@"matchId":_matchId,@"teamId":_teamId,@"status":@"70"} success:^(id data) {
                                
                                [MatchHelper shareMatch].match.status=30;
                                stuName.text=@"点球大战";
                                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                                [shotBtn setTitle:@"点球" forState:0];
                                isShot=YES;//已经点球大战
                                isLong=YES;
                                
                                
                                currnTime=0;
                                
                                [self setTime:currnTime];
                                [timer invalidate];
                                timer =nil;
                                
                                //                            [XHToast showBottomWithText:@"点球大战开始"];
                            } failure:^(id data) {
                                
                                
                                
                                [XHToast showBottomWithText:@"点球大战失败"];
                                
                              
                            }];
                        }]show];
                        
                       
                    }
                    
                    if ([data intValue]==1003) {
                        
                        [[[FBAlertView alloc]initWithTitle:@"是否确认结束比赛" butnTitlt:@"确认" block:^(id data) {
                            
                            [MatchHelper matchActionRequest:@{@"matchId":_matchId,@"teamId":_teamId,@"status":@"100"} success:^(id data) {
                                
                                [timer invalidate];
                                
                                [AccountHelper shareAccount].isLive=NO;
                                
                                  [self matchEndAction];
                                
                                //如果是从首发界面过来
                                //                             [SVProgressHUD showWithStatus:@"结束中...."];
//                                if (_fromFristView ==YES) {
//                                    
//                                    [self dismissViewControllerAnimated:NO completion:nil];
//                                    
//                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissToRoot" object:nil];
//                                }
//                                else{
//                                    
//                                    
//                                    [self dismissViewControllerAnimated:NO completion:nil];
//                                    
//                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissToRoot2" object:nil];
//                                    
//                                }
                                 AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                                
                            } failure:^(id data) {
                                
                                
                                
                                [XHToast showBottomWithText:@"结束比赛失败"];
                            }];
                            
                            
                        }]show];
                        
                        
                        
                        
                        
                    }
                    
                    
                    
         }];
                //如果加时赛已经加过了
                if (isLong==YES) {
                    [endView setHideTag:1001];
                }
                //已经点球大战
                if (isShot==YES) {
                    [endView setHideTag:1002];
                }
                
                
                
                [endView   show];
            
            }
            
            }
            
            
        }
        
    }
    
}

-(void)timeChange:(id)sender{
    
    
    currnTime++;
    int min=currnTime/60;
    int sec=currnTime%60;
    
    
    NSString *minS=nil;
    NSString *secS=nil;
    
    if (min<10) {
        minS=[NSString stringWithFormat:@"0%d",min];
    }else{
        minS=[NSString stringWithFormat:@"%d",min];
        
    }
    
    if (sec<10) {
        secS=[NSString stringWithFormat:@"0%d",sec];
    }else{
        secS=[NSString stringWithFormat:@"%d",sec];
        
    }
    
    timeLabel.text =[NSString stringWithFormat:@"%@'%@\"",minS,secS];
    
    
    
    
    
    
}

-(void)setTime:(int)times{

    int min=times/60;
    int sec=times%60;
    
    
    NSString *minS=nil;
    NSString *secS=nil;
    
    if (min<10) {
        minS=[NSString stringWithFormat:@"0%d",min];
    }else{
        minS=[NSString stringWithFormat:@"%d",min];
        
    }
    
    if (sec<10) {
        secS=[NSString stringWithFormat:@"0%d",sec];
    }else{
        secS=[NSString stringWithFormat:@"%d",sec];
        
    }
    
    timeLabel.text =[NSString stringWithFormat:@"%@'%@\"",minS,secS];

}
-(void)appBecomeActive:(id)sender{
    
    [self loadnetWorkData:^(id data) {
        
    }];
    
  
}
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:NO];
    
    [self.view layoutIfNeeded];
  
   
    
    

}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}
- (BOOL)prefersStatusBarHidden
{
    
    
    return YES;
}

@end
