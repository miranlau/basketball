//
//  ScoreViewController.m
//  FootballApp
//
//  Created by North on 16/7/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ScoreViewController.h"
#import "MatchHelper.h"
#import "MenberView.h"
#import "PlayersView.h"
#import "XHToast.h"
#import "MatchModel.h"
#import "ActionView.h"
#import "messageTuishong.h"
#import <AudioToolbox/AudioToolbox.h>
#import "EndView.h"
#import "STableViewCell.h"

@interface ScoreViewController ()
{
    
    __weak IBOutlet UIButton *homeTeamBtn;
    __weak IBOutlet UIButton *visitTeamBtn;
    __weak IBOutlet UIView *homeView;
    __weak IBOutlet UIView *visitView;
    __weak IBOutlet UIButton *socreBtn;
    __weak IBOutlet UIView *socreView;
    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet NSLayoutConstraint *right;
    __weak IBOutlet UIImageView *bgLogo;
    __weak IBOutlet UIView *topView;
    __weak IBOutlet UIButton *stopBtn;//暂停按钮
    
    __weak IBOutlet UIScrollView *actionScrollView;
    __weak IBOutlet UIButton *nextBtn;//下一节按钮
    __weak IBOutlet UITableView *histroyTableView;
    __weak IBOutlet UILabel *seccionNumLaber;//节数
    __weak IBOutlet UILabel *secionTimeLabel;//时间
    
    __weak IBOutlet UIImageView *hImageView;
    __weak IBOutlet UIImageView *vImageView;
    __weak IBOutlet UILabel *hName;
    __weak IBOutlet UILabel *VName;
    PlayersView *lastHomePlayer;
    PlayersView *lastVisitPlayer;
    UIButton *teamBtn;
    
    NSTimer *timer;
    int bgWidth;
    
}
@end

@implementation ScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    ViewRadius(homeTeamBtn, 5.0);
    ViewRadius(visitTeamBtn, 5.0);
    ViewRadius(socreBtn, 5.0);
    ViewRadius(topView, 5.0);
    ViewRadius(stopBtn, 3.0);
    ViewRadius(nextBtn, 3.0);
    ViewRadius(histroyTableView, 5.0);
    
    topView.layer.borderColor=[UIColor whiteColor].CGColor;
    topView.layer.borderWidth=.5;
    histroyTableView.layer.borderColor=[UIColor whiteColor].CGColor;
    histroyTableView.layer.borderWidth=.5;
    
    
    
    socreView.hidden=YES;
    
    bgLogo.hidden=NO;
    topView.hidden=YES;
    histroyTableView.hidden=YES;
    
    
    if (kScreenWidth>kScreenHeight) {
         bgWidth=(kScreenWidth-150)/2;
    }else{
    
        bgWidth=(kScreenHeight-150)/2;
    }
    
     [self loadMatchInfo];
    
    
    
    
  
    
    
}
#pragma mark 加载球队信息

-(void)loadMatchInfo{
    
    
    
    hName.text=[MatchHelper shareMatch].match.home[@"name"];
    VName.text=[MatchHelper shareMatch].match.visiting[@"name"];
    
    NSString *homeLogo=[MatchHelper shareMatch].match.home[@"logo"];
    NSString *visLogo=[MatchHelper shareMatch].match.visiting[@"logo"];
    
    [hImageView setImageWithURL:[NSURL URLWithString:homeLogo] placeholderImage:TeamLogo];
    [vImageView setImageWithURL:[NSURL URLWithString:visLogo] placeholderImage:TeamLogo];
    
    ViewRadius(hImageView, 22.5);
    ViewRadius(vImageView, 22.5);
    
    
    
    MatchHelper *match=[MatchHelper shareMatch].match;
    //比赛已经开始
    if (match.status>0) {
        socreBtn.userInteractionEnabled=NO;
        
        [socreBtn setTitle:[NSString stringWithFormat:@"%@:%@",match.homeScore,match.visitingScore] forState:0];
        
        bgLogo.hidden=YES;
        topView.hidden=NO;
        histroyTableView.hidden=NO;
        
        seccionNumLaber.text=[NSString stringWithFormat:@"第%@节",match.season];
        [self setTime:match.duration];
        
        if([match.isSuspend intValue]==1){
            [stopBtn setTitle:@"继续" forState:0];
            [self timeStop];
            
            if(match.status >=20&& match.status <30){
             seccionNumLaber.text=@"加时赛";
            }
            
            if(match.status ==40){
                
                seccionNumLaber.text=@"加时赛";
            }
            
        }else{
            
            [stopBtn setTitle:@"暂停" forState:0];
            
            if(match.status >=20&& match.status <30){
                
                seccionNumLaber.text=@"加时赛";
            }
            
            if(match.status ==40){
                
                seccionNumLaber.text=@"加时赛";
            }
            //正常时间
            if (match.status==30 ||match.status==40) {
                
                [self timeStop];
                //小节休息
            }else{
                
                [self timeBegin];
            }
            
        }
        
        
        
        
        
        
        
    }else{
        
        socreBtn.userInteractionEnabled=YES;
        
        bgLogo.hidden=NO;
        topView.hidden=YES;
        histroyTableView.hidden=YES;
        
        [self timeStop];
        [self setTime:0];
    }
    
    if ([match.isSuspend integerValue]==1) {
        
    }
    
    
    [self loadFristList];
    
    
}
#pragma mark 点击上方球队
- (IBAction)teamBtnClick:(id)sender {
    
    //比赛未开始，不允许点击操作
    if ([MatchHelper shareMatch].match.status <=0) {
        return;
    }
    //    //暂停不准计分
    //    if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
    //        return;
    //    }
    
    
    
    //如果是切换主客队
    
    if (sender!=teamBtn) {
        
        if (sender==homeTeamBtn) {
            
            lastVisitPlayer.isSelected=NO;
            lastVisitPlayer=nil;
            
        }
        
        if (sender==visitTeamBtn) {
            lastHomePlayer.isSelected=NO;
            lastHomePlayer=nil;
        }
        
    }
    
    
    
    
    if(sender== homeTeamBtn){
        
        //如果已经点击一次
        if (teamBtn==homeTeamBtn) {
            teamBtn=nil;
            socreView.hidden=YES;
            
            visitView.hidden=NO;
            homeTeamBtn.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            homeView.hidden=NO;
            
            //取消操作
            
            lastHomePlayer.isSelected=NO;
            lastHomePlayer=nil;
            
            
        }else{
            socreView.hidden=NO;
            left.priority=UILayoutPriorityDefaultHigh;
            right.priority=UILayoutPriorityDefaultLow;
            
            teamBtn=sender;
            
            
             homeTeamBtn.backgroundColor=[COLOR colorWithHexString:@"4D98C0"];
             visitTeamBtn.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            
            //视图影藏
            visitView.hidden=YES;
            
            homeView.hidden=NO;
            
            
            
            
        }
        
    }
    
    
    if(sender== visitTeamBtn){
        
        //如果已经点击一次
        if (teamBtn==visitTeamBtn) {
            teamBtn=nil;
            visitTeamBtn.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            socreView.hidden=YES;
            visitView.hidden=NO;
            homeView.hidden=NO;
            //取消操作
            lastVisitPlayer.isSelected=NO;
            lastVisitPlayer=nil;
        }else{
            teamBtn=sender;
             visitTeamBtn.backgroundColor=[COLOR colorWithHexString:@"4D98C0"];
             homeTeamBtn.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            socreView.hidden=NO;
            
            
            right.priority=UILayoutPriorityDefaultHigh;
            left.priority=UILayoutPriorityDefaultLow;
            //视图影藏
            visitView.hidden=NO;
            homeView.hidden=YES;
            
            
            
        }
        
    }
    
    
    
    
    
    
    
}


-(void)viewDidLayoutSubviews{
    
    [self loadActionBtn:[self getActionList]];
    
    
}

-(void)loadActionBtn:(id)list{
    
    
    for (UIView *view  in actionScrollView.subviews) {
        [view removeFromSuperview];
    }
    
    int speace=5;//每个之间的间隔
    
    
    int width= (WIDTH(actionScrollView)-4*speace)/3;//每个按钮宽度和高度
    
    for (int i=0;i<[list count];i++) {
        
        NSDictionary *dic=list[i];
        
        UIButton *acBtn=[[UIButton alloc]initWithFrame:CGRectMake(i%3*(width+speace)+speace, i/3*(width+speace)+speace, width, width)];
        [acBtn setTitle:dic[@"name"] forState:0];
        [acBtn setTintColor:[UIColor redColor]];
        acBtn.titleLabel.font=[UIFont systemFontOfSize:15];
        [actionScrollView addSubview:acBtn];
        acBtn.tag=[dic[@"code"] integerValue];
        
        if (i<3) {
            [acBtn setBackgroundColor:ACCOLOR1];
        }
        
        if (i==3|| i==4||i==6) {
            [acBtn setBackgroundColor:ACCOLOR2];
        }
        
        
        if (i==5|| i==7||i==8) {
            [acBtn setBackgroundColor:ACCOLOR3];
        }
        
        if (i==9||i==10) {
            [acBtn setBackgroundColor:ACCOLOR4];
        }
        
        [acBtn addTarget:self action:@selector(actionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        ViewRadius(acBtn, 5.0);
        if (i==[list count]-1) {
            [actionScrollView setContentSize:CGSizeMake(WIDTH(actionScrollView), MaxY(acBtn)+5)];
        }
        
    }
    
}
#pragma mark 点击跳球开始
- (IBAction)scoreBtn:(id)sender {
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    
    
    
    [MatchHelper matchActionRequest:@{@"matchId":_matchId,@"status":@"11"} success:^(id data) {
        
        [socreBtn setTitle:@"0:0" forState:0];
        bgLogo.hidden=YES;
        topView.hidden=NO;
        histroyTableView.hidden=NO;
        socreBtn.userInteractionEnabled=NO;
        seccionNumLaber.text=@"第1节";
        [MatchHelper shareMatch].match.season=@"1";
        [MatchHelper shareMatch].match.duration=0;
        [MatchHelper shareMatch].match.status=10;
        [self timeBegin];
        
        
        [MatchHelper shareMatch].match.actions=data[@"actions"];
        [histroyTableView reloadData];
        
        [self tableViewScrollToBottom];
        
        
        
        
        
    } failure:^(id data) {
        
        [XHToast showBottomWithText:data];
        
    }];
    
    
}
//暂停按钮
-(IBAction)stopBtnClick:(id)sender{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    
    
    //比赛已经暂停
    if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
        
        [stopBtn setTitle:@"暂停" forState:0];
        [self actionRequest:@"56" teamId:nil];
        [self timeBegin];
        [MatchHelper shareMatch].match.isSuspend=@"0";
        
        
        
    }else{
        
        [stopBtn setTitle:@"继续" forState:0];
        
        [self actionRequest:@"55" teamId:nil];
        [self timeStop];
        [MatchHelper shareMatch].match.isSuspend=@"1";
    }
    
    
    
    
    
    
    
}
//下一节按钮
-(IBAction)nextBtnClick:(UIButton *)sender{
    
    
    
    
    //暂停不准点
    if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
        return;
    }
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    
    if ([sender.currentTitle isEqualToString:@"结束"]) {
        //判断比分是否相等
        BOOL isAdd=NO;
        if ([[MatchHelper shareMatch].match.homeScore intValue]==[[MatchHelper shareMatch].match.visitingScore intValue]) {
            isAdd=YES;
        }
        //是否进行加时赛中
       
        
        //加时赛结束
        if(([MatchHelper shareMatch].match.status >=20 && [MatchHelper shareMatch].match.status<30)||[MatchHelper shareMatch].match.status==40){
           //如果比分不相等 结束本场比赛
            if (!isAdd) {
                
                [[[FBAlertView alloc]initWithTitle:@"是否结束全场比赛" butnTitlt:@"确定" block:^(id data) {
                    
                    [self actionRequest:@"100" teamId:nil];
                    
                    
                }]show];
                
            }else{
                
                [[[FBAlertView alloc]initWithTitle:@"是否确定结束" butnTitlt:@"确定" block:^(id data) {
                    
                    [self actionRequest:@"64" teamId:nil];
                    
                    [self timeStop];
                    [MatchHelper shareMatch].match.duration=0;
                    [self setTime:[MatchHelper shareMatch].match.duration];
                    [nextBtn setTitle:@"下一节" forState:0];
                }]show];
                
             
            
            }
            
            
        }else{
        
        
        
        
        [[[EndView alloc]initWithAddTime:isAdd block:^(id data) {
            
            //本节结束
            if([data isEqualToString:@"本节结束"] ){
                
                [self timeStop];
                [MatchHelper shareMatch].match.duration=0;
                [self setTime:[MatchHelper shareMatch].match.duration];
                [nextBtn setTitle:@"下一节" forState:0];
                
                [self actionRequest:@"62" teamId:nil];
                
                
            }
            //全场结束
            if([data isEqualToString:@"全场结束"]){
                
                [[[FBAlertView alloc]initWithTitle:@"是否结束全场比赛" butnTitlt:@"确定" block:^(id data) {
                    
                    [self actionRequest:@"100" teamId:nil];
                    
                   
                }]show];
                
               
            }
            //加时赛
            if([data isEqualToString:@"加时赛"]){
                
                [MatchHelper shareMatch].match.season=@"0";
                seccionNumLaber.text=@"加时赛";
                [MatchHelper shareMatch].match.status=20;
                [self actionRequest:@"65" teamId:nil];
                [self timeStop];
                [MatchHelper shareMatch].match.duration=0;
                [self setTime:[MatchHelper shareMatch].match.duration];
                [nextBtn setTitle:@"开始" forState:0];
                
                
            }
            
            
        }]show];
        }
        
    }else if([sender.currentTitle isEqualToString:@"下一节"]){
        
         [MatchHelper shareMatch].match.duration=0;
        
        if(([MatchHelper shareMatch].match.status >=20 && [MatchHelper shareMatch].match.status<30)||[MatchHelper shareMatch].match.status==40){
            
            
            [[[FBAlertView alloc]initWithTitle:@"是否确认进入下节比赛" butnTitlt:@"确认" block:^(id data) {
                [self actionRequest:@"63" teamId:nil];
                [self timeBegin];
                [nextBtn setTitle:@"结束" forState:0];
                [MatchHelper shareMatch].match.status=40;//加时赛节间休息
                int season=[[MatchHelper shareMatch].match.season intValue];
                season++;
                [MatchHelper shareMatch].match.season=[NSString stringWithFormat:@"%d",season];
                seccionNumLaber.text=[NSString stringWithFormat:@"加时赛%d",season];
                
            }] show];
        
 
        
        }else{
            
            int season=[[MatchHelper shareMatch].match.season intValue];
            season++;
            [MatchHelper shareMatch].match.season=[NSString stringWithFormat:@"%d",season];
            seccionNumLaber.text=[NSString stringWithFormat:@"第%d节",season];
           
            [self timeBegin];
            [nextBtn setTitle:@"结束" forState:0];
            [self actionRequest:@"61" teamId:nil];
            [MatchHelper shareMatch].match.status=30;//常规时间节间休息
            
        }
        
       
        
        
    }
    else if([sender.currentTitle isEqualToString:@"开始"]){
        
        [self actionRequest:@"63" teamId:nil];
        [MatchHelper shareMatch].match.duration=0;
        [self timeBegin];
        [nextBtn setTitle:@"结束" forState:0];
        int season=[[MatchHelper shareMatch].match.season intValue];
        season++;
        [MatchHelper shareMatch].match.season=[NSString stringWithFormat:@"%d",season];
        seccionNumLaber.text=[NSString stringWithFormat:@"加时赛%d",season];
        
        
    }
   
    
    
    
}


#pragma mark 点击计分按钮
-(void)actionBtnClick:(UIButton *)butn{
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    
    
    
    if (teamBtn==homeTeamBtn) {
        
        
        
        if (butn.tag==22) {
            
                //暂停不准计分
            
            
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SureLineUpViewController"];
            [controller setValue:self.matchId forKey:@"matchId"];
            
            
            [controller setValue:@YES forKey:@"isChange"];
            
            
            [controller setControllerBlock:^(id data) {
                
                [MatchHelper MatchInfo:@{@"matchId":self.matchId} success:^(id data2) {
                     [self loadMatchInfo];
                    
                    
                    [MatchHelper shareMatch].match.actions=data2[@"actions"];
                    [histroyTableView reloadData];

                } failure:^(id data) {
                    
                }];
                
                
                
               
                
            }];
            
            
            [self teamBtnClick:homeTeamBtn];
            
            
            [self  presentViewController:controller animated:YES completion:nil];
            
            return;
            
        }
        
        if (butn.tag==120) {
            
            [self actionRequest:@"120" teamId:[MatchHelper shareMatch].match.home[@"id"]];
            
            [self teamBtnClick:homeTeamBtn];
            
            return;
            
            
            
        }
        
        
//        if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
//            return;
//        }
//        
        
        if(lastHomePlayer){
            
            [self actionDetail:butn];
            
            
        }else{
            
            [XHToast showBottomWithText:@"请选择球员"];
           
            
        }
        
        
    }else{
        
        if (butn.tag==22) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SureLineUpViewController"];
            [controller setValue:self.matchId forKey:@"matchId"];
            
            
            [controller setValue:@YES forKey:@"isChange"];
            
            
            [controller setControllerBlock:^(id data) {
                
                [MatchHelper MatchInfo:@{@"matchId":self.matchId} success:^(id data2) {
                    [self loadMatchInfo];
                    
                    
                    [MatchHelper shareMatch].match.actions=data2[@"actions"];
                    [histroyTableView reloadData];
                } failure:^(id data) {
                    
                }];
                
            }];
            
            
            [self teamBtnClick:visitTeamBtn];
            
            
            [self  presentViewController:controller animated:YES completion:nil];
            return;
            
        }
        
        if (butn.tag==120) {
            
            [self actionRequest:@"120" teamId:[MatchHelper shareMatch].match.home[@"id"]];
            
            [self teamBtnClick:visitTeamBtn];
            
            return;
            
            
            
        }
//        if ([[MatchHelper shareMatch].match.isSuspend intValue]==1) {
//            return;
//        }
        if(lastVisitPlayer){
            [self actionDetail:butn];
            
        }else{
            
            [XHToast showBottomWithText:@"请选择球员"];
            
            
        }
        
    }
    
    
    
    
    
    
    
}

-(void)actionDetail:(UIButton *)butn{
    
    
    if (butn.tag <=3) {
        
        [[[ActionView alloc]initWithBlock:^(id data) {
            //命中
            NSString *status=nil;
            
            if ([data intValue]==1000) {
                //
                if (butn.tag ==1) {
                    status=@"35";
                }
                if (butn.tag ==2) {
                    status=@"33";
                }
                if (butn.tag ==3) {
                    status=@"31";
                }
                
                
            }else{
                //未命中
                if (butn.tag ==1) {
                    status=@"36";
                }
                if (butn.tag ==2) {
                    status=@"34";
                }
                if (butn.tag ==3) {
                    status=@"32";
                }
                
            }
            
            
            if (teamBtn==homeTeamBtn) {
                
                [self actionRequest:status teamId:[MatchHelper shareMatch].match.home[@"id"]];
                
                [self teamBtnClick:homeTeamBtn];
                
            }else{
                
                [self actionRequest:status teamId:[MatchHelper shareMatch].match.visiting[@"id"]];
                
                [self teamBtnClick:visitTeamBtn];
            }
            
            
        }]show];
        
    }else{
        
        if (teamBtn==homeTeamBtn) {
            
            [self actionRequest:[NSString stringWithFormat:@"%ld",butn.tag] teamId:[MatchHelper shareMatch].match.home[@"id"]];
            
            [self teamBtnClick:homeTeamBtn];
        }else{
            
            [self actionRequest:[NSString stringWithFormat:@"%ld",butn.tag] teamId:[MatchHelper shareMatch].match.visiting[@"id"]];
            
            [self teamBtnClick:visitTeamBtn];
        }
        
    }
    
}

-(void)actionRequest:(NSString *)status teamId:(NSString *)teamId{
    
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    [dic setValue:status forKey:@"status"];
    [dic setValue:teamId forKey:@"teamId"];
    [dic setValue:_matchId forKey:@"matchId"];
    
    if(lastHomePlayer){
    [dic setValue:lastHomePlayer.play.id forKey:@"player"];
    
    }
    
    if(lastVisitPlayer){
        [dic setValue:lastVisitPlayer.play.id forKey:@"player"];
        
    }
    
    
    
    
    
    [MatchHelper matchActionRequest:dic success:^(id data) {
        
        if(([status intValue]>=31&&[status intValue]<=36)||[status intValue]==120){
            
            [MatchHelper MatchInfo:@{@"matchId":dic[@"matchId"]} success:^(id data) {
                
                [self loadMatchInfo];
                
            } failure:^(id data) {
                
            }];
            
        }
        //结束全场比赛
        if ([status intValue]==100) {
             [self matchEndAction];
        }
        
        
        [MatchHelper shareMatch].match.actions=data[@"actions"];
        [histroyTableView reloadData];
        
        [self tableViewScrollToBottom];
        
        
        
    } failure:^(id data) {
        
        [XHToast showBottomWithText:data];
        
    }];
    
    
}
-(void)timeBegin{
    [timer invalidate];
    timer =nil;
    timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange:) userInfo:nil repeats:YES];
    
}
-(void)timeStop{
    [timer invalidate];
    timer =nil;
    
    
}

-(void)timeChange:(id)sender{
    
    [MatchHelper shareMatch].match.duration ++;
    
    
    
    [self setTime:[MatchHelper shareMatch].match.duration];
    
}

-(void)setTime:(int)times{
    
    int min=times/60;
    int sec=times%60;
    
    
    NSString *minS=nil;
    NSString *secS=nil;
    
    if (min<10) {
        minS=[NSString stringWithFormat:@"0%d",min];
    }else{
        minS=[NSString stringWithFormat:@"%d",min];
        
    }
    
    if (sec<10) {
        secS=[NSString stringWithFormat:@"0%d",sec];
    }else{
        secS=[NSString stringWithFormat:@"%d",sec];
        
    }
    
    secionTimeLabel.text=[NSString stringWithFormat:@"%@'%@\"",minS,secS];
    
    
}
#pragma mark 加载场上人员
-(void)loadFristList{
    
    
    for (UIView *view  in homeView.subviews) {
        if ([view isKindOfClass:[PlayersView class]]) {
            [view removeFromSuperview];
        }
    }
    
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.homeStarting) {
        PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
        
        PlayersView *view = [[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        view.play=model;
        
        view.center=[self locationfromPosition:CGPointMake([model.x doubleValue] , [model.y doubleValue])];
        
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(homePlayerClick:)]];
        
        [homeView addSubview:view];
        
        
        
        
        
    }
    for (UIView *view  in visitView.subviews) {
        if ([view isKindOfClass:[PlayersView class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.visitingStarting) {
        
        
        PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
        
        PlayersView *view = [[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        view.play=model;
        
        view.center=[self locationfromPosition:CGPointMake([model.x doubleValue] , [model.y doubleValue])];
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(visitPlayerClick:)]];
        [visitView addSubview:view];
        
    }
    
}
-(void)homePlayerClick:(UITapGestureRecognizer *)tap{
    
    
    PlayersView *view =(PlayersView *)tap.view;
    
    
    if (lastHomePlayer!=view) {
        
        lastHomePlayer.isSelected=NO;
        
        view.isSelected=YES;
        lastHomePlayer=view;
        teamBtn=nil;
        [self teamBtnClick:homeTeamBtn];
        
        
    }else{
        lastHomePlayer=nil;
        ViewRadius(view, 22.5);
        view.layer.borderWidth=0;
        view.layer.borderColor=[UIColor greenColor].CGColor;
        
    }
    
    
}
-(void)visitPlayerClick:(UITapGestureRecognizer *)tap{
    PlayersView *view =(PlayersView *)tap.view;
    
    if (lastVisitPlayer!=view) {
        
        
        
        lastVisitPlayer.isSelected=NO;
        
        view.isSelected=YES;
        lastVisitPlayer=view;
        
        teamBtn=nil;
        [self teamBtnClick:visitTeamBtn];
        
        
    }else{
        lastVisitPlayer=nil;
        ViewRadius(view, 22.5);
        view.layer.borderWidth=0;
        view.layer.borderColor=[UIColor greenColor].CGColor;
        
    }
    
}
-(NSMutableArray *)getActionList{
    
    NSMutableArray *list=[NSMutableArray array];
    
    [list addObject:@{@"name":@"3分",@"code":@"1"}];
    [list addObject:@{@"name":@"2分",@"code":@"2"}];
    [list addObject:@{@"name":@"罚球",@"code":@"3"}];
    
    [list addObject:@{@"name":@"篮板",@"code":@"41"}];
    [list addObject:@{@"name":@"盖帽",@"code":@"42"}];
    [list addObject:@{@"name":@"助攻",@"code":@"37"}];
    
    [list addObject:@{@"name":@"抢断",@"code":@"43"}];
    [list addObject:@{@"name":@"失误",@"code":@"44"}];
    [list addObject:@{@"name":@"犯规",@"code":@"45"}];
    
    [list addObject:@{@"name":@"换人",@"code":@"22"}];
    [list addObject:@{@"name":@"撤销",@"code":@"120"}];
    
    return list;
    
    
    
}
-(void)matchEndAction{
    
    
    for (UIView *view  in [UIApplication sharedApplication].keyWindow.subviews) {
        
        if ([view isKindOfClass:[EndView class]]) {
            
            [view removeFromSuperview];
        }
    }
    
    
    [AccountHelper shareAccount].isBackHome=YES;
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
    
    [UIApplication sharedApplication].keyWindow.rootViewController =controller;
    
    
    
    
    
}
- (void)tableViewScrollToBottom
{
//    if ([[MatchHelper shareMatch].match.actions count]==0){
//        return;
//    }
//    
//    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.dataArr.count-1 inSection:0];
//    [histroyTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    
    
}
#pragma mark tableView 委托 
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    
    return [[MatchHelper shareMatch].match.actions count];
}
-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 15;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *idf=@"tableViewCell";
    STableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:idf];
    if (!cell) {
        cell=[[STableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idf];
    }
     cell.showLabel.text=[MatchHelper shareMatch].match.actions[indexPath.row];
     cell.backgroundColor=[UIColor clearColor];
   
    
    return cell;
}

#pragma mark 从位置计算坐标
-(CGPoint)locationfromPosition:(CGPoint)position{
    
    //视图大小320*320
    
    CGFloat width=bgWidth/37;
    CGPoint point=CGPointMake(position.x *width, position.y*width);
    
    return point;
    
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersStatusBarHidden
{
    
    
    return YES;
}
@end
