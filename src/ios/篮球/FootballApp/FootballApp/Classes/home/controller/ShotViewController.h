//
//  ShotViewController.h
//  FootballApp
//
//  Created by North on 16/7/1.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface ShotViewController : BaseViewController
@property(strong ,nonatomic)NSString *matchId;
@property(strong ,nonatomic)NSString *teamId;
@end
