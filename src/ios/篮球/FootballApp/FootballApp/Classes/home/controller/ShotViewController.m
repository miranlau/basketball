//
//  ShotViewController.m
//  FootballApp
//
//  Created by North on 16/7/1.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ShotViewController.h"
#import "MatchHelper.h"
#import "MenberView.h"
#import "PlayerView.h"
#import "XHToast.h"
#import "MatchModel.h"
@interface ShotViewController ()
{
    NSString *playA;
    PlayerView *lastPlayView;

    __weak IBOutlet UIButton *btn1;
    __weak IBOutlet UIButton *btn2;
}
@end

@implementation ShotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ViewRadius(btn1, 6);
    ViewRadius(btn2, 6);
    
    [self loadFristList];
    
}
-(void)loadFristList{

    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.statring) {
        PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
        
        PlayerView *player=[[PlayerView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        player.play=play;
        player.center=[self locationfromPosition:CGPointMake([play.x doubleValue] , [play.y doubleValue])];
        
        player.isSelect=YES;
        [player selectBtnClick:^(id data) {
            
            if (lastPlayView!=player) {
                if (lastPlayView) {
                    
                    lastPlayView.isSelected=NO;
                }

                playA=player.play.id;
                lastPlayView=player;
                
            }else{
                lastPlayView=nil;
                playA=nil;
            
            }
            
        }];
        
        
        
        
        [self.view addSubview:player];
        
    }
    //判断当前队伍是主队还是客队


}
#pragma mark 从位置计算坐标
-(CGPoint)locationfromPosition:(CGPoint)position{
    
    //视图大小320*320
    
    double top=(kScreenHeight-320)/2.0;
    CGFloat width=320/25.0;
    CGPoint point=CGPointMake(position.x *width, position.y*width+top);
    
    return point;
    
}
- (IBAction)shotBtnClick:(UIButton *)sender {
    
    
    if (!playA) {
    [XHToast showBottomWithText:@"请先选择点球人员"];
        return;
    }
    
    
    
    NSString *status=nil;
    NSString *string=@"";
    //射正
    if (sender.tag==1001) {
        
        status=@"38";
        string=@"点球罚进";
        
    }else{
        
        status=@"39";
        string=@"点球罚丢";
    
    }
    
    
    NSMutableDictionary *dic=[MatchHelper playerA:playA playerB:nil status:status matchId:_matchId teamId:_teamId];
    
    [MatchHelper matchActionRequest:dic success:^(id data) {
        
        if (self.cblock) {
            self.cblock(string);
        }
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    } failure:^(id data) {
        
        
        
        [XHToast showTopWithText:data];
        
        
    }];
    
    
}
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersStatusBarHidden
{
    
    
    return YES;
}

@end
