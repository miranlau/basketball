//
//  SureLineUpViewController.m
//  FootballApp
//
//  Created by North on 16/7/27.
//  Copyright © 2016年 North. All rights reserved.
//

#import "SureLineUpViewController.h"
#import "MatchHelper.h"
#import "MenberView.h"
#import "PlayersView.h"
#import "XHToast.h"
#import "MatchModel.h"

@interface SureLineUpViewController ()
{
    __weak IBOutlet UIButton *homeTeam;//主队
    
    __weak IBOutlet UIButton *visitTeam;//客队
    
    __weak IBOutlet UIButton *submitBtn;//确认阵容
    __weak IBOutlet UIView *homeView;
    __weak IBOutlet UIView *visitView;
    
    __weak IBOutlet UIView *homeTableView;
    __weak IBOutlet UIView *visitTableView;
    
    __weak IBOutlet UITableView *homeTableList;
    __weak IBOutlet UITableView *visitTableList;
    
    __weak IBOutlet UIImageView *hImageView;
    __weak IBOutlet UIImageView *vImageView;
    __weak IBOutlet UILabel *hName;
    __weak IBOutlet UILabel *VName;
    UIButton *teamBtn;
    int bGwidth;
    
    NSMutableArray *homeOtherList;//主队替补队员列表 //如果不是换人就是赛前确认过的人员
    NSMutableArray *visitOtherList;//客队替补队员列表
    NSMutableArray *homeLocationList;//主队首发队员在父视图中的view列表
    NSMutableArray *visitLocationList;//主队首发队员在父视图中的view列表
    NSMutableArray *homeFristList;//首发人员列表
    NSMutableArray *visitFristList;//首发人员列表
    
    PlayersView *playView;
    NSIndexPath * _indexPath;
    NSString *homeLeader;//主队队长
    NSString *visitLeader;//客队队长
    MatchModel *match;
    
}
@end

@implementation SureLineUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    ViewRadius(homeTeam, 5.0);
    ViewRadius(visitTeam, 5.0);
    ViewRadius(submitBtn, 5.0);
    
    
    homeTableList.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    homeTableList.rowHeight=UITableViewAutomaticDimension;
    homeTableList.estimatedRowHeight=10;
    
    
    visitTableList.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    visitTableList.rowHeight=UITableViewAutomaticDimension;
    visitTableList.estimatedRowHeight=10;
    
    
    
    NSString *homeLogo=[MatchHelper shareMatch].match.home[@"logo"];
    NSString *visLogo=[MatchHelper shareMatch].match.visiting[@"logo"];
    
    hName.text=[MatchHelper shareMatch].match.home[@"name"];
    VName.text=[MatchHelper shareMatch].match.visiting[@"name"];
    
    [hImageView setImageWithURL:[NSURL URLWithString:homeLogo] placeholderImage:TeamLogo];
    [vImageView setImageWithURL:[NSURL URLWithString:visLogo] placeholderImage:TeamLogo];
    
    ViewRadius(hImageView, 22.5);
    ViewRadius(vImageView, 22.5);
    

    
    //给tableView增加拖拽手势
    
    UIPanGestureRecognizer * pan1=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(cellPanAction:)];
    pan1.delegate=self;
    [homeTableList addGestureRecognizer:pan1];
    
    
    UIPanGestureRecognizer * pan2=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(cellPanAction:)];
    pan2.delegate=self;
    [visitTableList addGestureRecognizer:pan2];
    
    if (kScreenWidth>kScreenHeight) {
        bGwidth=(kScreenWidth-150)/2;
    }else{
        
        bGwidth=(kScreenHeight-150)/2;
    }
    
    
    //如果是首发就加载人员到场上
    if (_isChange) {
        [submitBtn setTitle:@"换人完毕" forState:0];
        [self loadFristList];
        [self loadOtherList];
        
    }else{
        
        homeLeader=[MatchHelper shareMatch].match.home[@"leader"];
        
        visitLeader=[MatchHelper shareMatch].match.visiting[@"leader"];
        
        [self loadOtherList];
        
    }
    
    
}

- (IBAction)teamBtnClick:(UIButton *)sender {
    
    if(sender== homeTeam){
        
        //如果已经点击一次
        if (teamBtn==homeTeam) {
            teamBtn=nil;
            homeTeam.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            
            visitView.hidden=NO;
            visitTableView.hidden=YES;
            homeView.hidden=NO;
            homeTableView.hidden=YES;
        }else{
            
            teamBtn=sender;
            //边框
            homeTeam.backgroundColor=[COLOR colorWithHexString:@"4D98C0"];
            visitTeam.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            
            
            
            //视图影藏
            visitView.hidden=YES;
            visitTableView.hidden=NO;
            homeView.hidden=NO;
            homeTableView.hidden=YES;
            
        }
        
    }
    
    
    if(sender== visitTeam){
        
        //如果已经点击一次
        if (teamBtn==visitTeam) {
            teamBtn=nil;
            
            visitTeam.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            
            visitView.hidden=NO;
            visitTableView.hidden=YES;
            homeView.hidden=NO;
            homeTableView.hidden=YES;
            
        }else{
            teamBtn=sender;
            visitTeam.backgroundColor=[COLOR colorWithHexString:@"4D98C0"];
            homeTeam.backgroundColor=[COLOR colorWithHexString:@"2d2e30"];
            
            
            
            
            //视图影藏
            visitView.hidden=NO;
            visitTableView.hidden=YES;
            homeView.hidden=YES;
            homeTableView.hidden=NO;
            
        }
        
    }
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}
#pragma mark 拖拽手势
-(void)cellPanAction:(UIPanGestureRecognizer *)pan{
    
    if (!teamBtn) {
        return;
    }
    UITableView *tableView=nil;
    NSMutableArray *otherList;
    UIView *fatherView;
    NSMutableArray *fristLocationList;
    
    if (!homeLocationList) {
        homeLocationList=[NSMutableArray array];
        visitLocationList=[NSMutableArray array];
    }
    
    //如果当前是主队
    if (teamBtn==homeTeam) {
        tableView=visitTableList;
        otherList=homeOtherList;
        fatherView=homeView;
        fristLocationList=homeLocationList;
        
        
    }else{
        
        tableView=homeTableList;
        otherList=visitOtherList;
        fatherView=visitView;
        fristLocationList=visitLocationList;
    }
    
    CGPoint pointInSelf = [pan locationInView:self.view];//在父视图中的位置
    CGPoint pointIntableView = [pan locationInView:tableView];//在tableView中的位置
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        
        playView =[[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        
        _indexPath = [tableView indexPathForRowAtPoint:pointIntableView];
        
        if (!_indexPath|| _indexPath.row+1 >[otherList count]) {
            _indexPath=nil;
            playView=nil;
            
            return;
        }
        
        
        PlayerModel *model=otherList[_indexPath.row];
        
        playView.play=model;
        
        playView.center=pointInSelf;
        
        
        [playView addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(viewPanAction:)]];
        
        
        [self.view addSubview:playView];
        
        
    }else if (pan.state == UIGestureRecognizerStateChanged) {
        
        
        //设置移动边界
        CGRect bgRect1= CGRectZero;//放球员的位置
        CGRect bgRect2= CGRectZero;//放球员的外部位置
        
        
        if (teamBtn==homeTeam) {
            bgRect1=homeTableView.frame;
            bgRect2=CGRectMake(MaxX(homeTableView), MinY(visitTableView), kScreenWidth-MaxX(homeTableView), kScreenHeight-MinY(visitTableView));
            
            
        }else{
            
            bgRect1=visitTableView.frame;
            
            bgRect2=CGRectMake(0, MinY(visitTableView), kScreenWidth-WIDTH(visitTableView), kScreenHeight-MinY(visitTableView));
            
        }
        
        //如果还在在右边视图内移动
        if (CGRectContainsPoint(bgRect2, pointInSelf)) {
            playView.center=pointInSelf;
        }else{
            
            //还在在左边视图内移动
            if (CGRectContainsPoint(bgRect1, pointInSelf)) {
                playView.center=pointInSelf;
                
            }else{
                
                return;
            }
        }
    }else if (pan.state == UIGestureRecognizerStateEnded) {
        
        if (!playView) {
            return;
        }
        
        //设置移动边界
        CGRect bgRect1= CGRectZero;
        
        
        if (teamBtn==homeTeam) {
            bgRect1=homeTableView.frame;
            
        }else{
            
            bgRect1=visitTableView.frame;
            
        }
        //如果在左边视图内
        if (CGRectContainsPoint(bgRect1, pointInSelf)) {
            
            
            
            //换算到superView里的frame
            CGRect newFrame=[playView convertRect:playView.bounds toView:fatherView];
            playView.frame=newFrame;
            [fatherView addSubview:playView];
            
            
            
            
            
            //判断有没有覆盖场上人员列表
            PlayersView *downView=nil;
            
            for(PlayersView *view in fristLocationList){
                //如果本次拖拽的视图中心在存在的场上人员的坐标范围系统内
                if (CGRectContainsPoint(view.frame, playView.center)&&view !=pan.view){
                    
                    
                    //将模型增加到数组中
                    
                    NSInteger index=[otherList count];
                    
                    if (view.play) {
                        
                        
                        [otherList insertObject:view.play atIndex:index];
                        
                        
                        [tableView beginUpdates];
                        [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        [tableView endUpdates];
                        
                        
                        downView=view;
                        break;
                    }
                    
                }
                
            }
            
            if (downView){
                [downView removeFromSuperview];
                [fristLocationList removeObject:downView];
            }
            
            
            
            if (_indexPath) {
                
                //增加到场上人员视图列表里面
                
                if(!fristLocationList){
                    fristLocationList =[NSMutableArray array];
                }
                
                [fristLocationList addObject:playView];
                //从替补阵容中删除
                [otherList removeObject:otherList[_indexPath.row]];
                
                [tableView beginUpdates];
                [tableView deleteRowsAtIndexPaths:@[_indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [tableView endUpdates];
            }
            
            
            
            
            
            //判断有没有重叠 有就代表替换
            
            
        }else{
            
            [playView removeFromSuperview];
            playView=nil;
            _indexPath=nil;
            
        }
        
        
        
        
        
        
    }
    
    
    
}
-(void)viewPanAction:(UIPanGestureRecognizer *)pan{
    
    
    if (!teamBtn) {
        return;
    }
    
    //如果当前是主队
    
    
    CGPoint pointInSelf = [pan locationInView:self.view];//在父视图中的位置
    //设置移动边界
    
    CGRect bgRect1= CGRectZero;
    UITableView *tableView=nil;
    NSMutableArray *otherList;
    UIView *fatherView;
    NSMutableArray *fristLocationList;
    
    //如果当前是主队
    if (teamBtn==homeTeam) {
        tableView=visitTableList;
        otherList=homeOtherList;
        fatherView=homeView;
        fristLocationList=homeLocationList;
        bgRect1=homeTableView.frame;
        
    }else{
        
        tableView=homeTableList;
        otherList=visitOtherList;
        fatherView=visitView;
        fristLocationList=visitLocationList;
        bgRect1=visitTableView.frame;
    }
    
    if (pan.state==UIGestureRecognizerStateBegan) {
        //换算坐标
        
        //换算到superView里的frame
        CGRect newFrame=[pan.view convertRect:pan.view.bounds toView:self.view];
        pan.view.frame=newFrame;
        [self.view addSubview:pan.view];
        
        
    }else if (pan.state==UIGestureRecognizerStateChanged) {
        
        pan.view.center=pointInSelf;
        
    }else if(pan.state==UIGestureRecognizerStateEnded){
        
        //左边背景
        
        //如果在左边视图内
        if (CGRectContainsPoint(bgRect1, pan.view.center)){
            
            
            CGRect newFrame=[pan.view convertRect:pan.view.bounds toView:fatherView];
            pan.view.frame=newFrame;
            [fatherView addSubview:pan.view];
            
            
            //判断有没有覆盖场上人员列表
            PlayersView *downView=nil;
            
            for(PlayersView *view in fristLocationList){
                //如果本次拖拽的视图中心在存在的场上人员的坐标范围系统内
                if (CGRectContainsPoint(view.frame, pan.view.center)&&view !=pan.view){
                    
                    
                    //将模型增加到数组中
                    
                    NSInteger index=[otherList count];
                    
                    [otherList insertObject:view.play atIndex:index];
                    
                    
                    [tableView beginUpdates];
                    [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [tableView endUpdates];
                    
                    
                    downView=view;
                    break;
                    
                }
                
            }
            
            if (downView) {
                [downView removeFromSuperview];
                [fristLocationList removeObject:downView];
            }
            
        }else{
            
            
            PlayersView *view=(PlayersView *)pan.view;
            
            //将模型增加到数组中
            
            NSInteger index=[otherList count];
            
            [otherList insertObject:view.play atIndex:index];
            
            
            [tableView beginUpdates];
            [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [tableView endUpdates];
            
            [fristLocationList removeObject:pan.view];
            [view removeFromSuperview];
            
        }
        
        
    }
    
    
}

//完成按钮选择
- (IBAction)doneBtnCllick:(id)sender {
    
    //判断场上人员数量有没有被删除干净
    NSMutableArray *hlist=[NSMutableArray array];
    
    //主队首发人员
    
    for (UIView *view in homeView.subviews) {
        
        if ([view isKindOfClass:[PlayersView class]]&&view.hidden==NO) {
            
            [hlist addObject:view];
        }
        
    }
    
    homeLocationList=hlist;
    
    
    //判断场上人员数量有没有被删除干净
    NSMutableArray *vlist=[NSMutableArray array];
    
    //主队首发人员
    
    for (UIView *view in visitView.subviews) {
        
        if ([view isKindOfClass:[PlayersView class]]&&view.hidden==NO) {
            
            [vlist addObject:view];
        }
        
    }
    
    visitLocationList=vlist;
    
    //是换人
    if (_isChange) {
        
        if ([homeLocationList count]==0) {
            [XHToast showBottomWithText:@"请选择主队上场人员"];
            return;
        }
        
        if ([visitLocationList count]==0) {
            [XHToast showBottomWithText:@"请选择客队上场人员"];
            return;
        }
        
        
        if ([homeLocationList count]>[[MatchHelper shareMatch].match.type  intValue]) {
            
            [XHToast showBottomWithText:@"主队超过允许上场人数"];
            
            return ;
            
        }
        if ([visitLocationList count]>[[MatchHelper shareMatch].match.type  intValue]) {
            
            [XHToast showBottomWithText:@"客队超过允许上场人数"];
            
            return ;
            
        }
        
        [[[FBAlertView alloc]initWithTitle:@"确认换人完毕？" butnTitlt:@"完毕" block:^(id data) {
            
            
            NSMutableDictionary *dic =[NSMutableDictionary dictionary];
            
            [dic setValue:self.matchId forKey:@"matchId"];
            
            [dic setValue:[self getLocation:homeLocationList] forKey:@"homeDisplay"];
            
            [dic setValue:[self getLocation:visitLocationList] forKey:@"visitingDisplay"];
            
            [HttpRequestHelper postWithURL:Url(@"game/change") params:dic success:^(BOOL sucess, id responseObject) {
                
                if(sucess){
                    
                    [MatchHelper MatchInfo:@{@"matchId":dic[@"matchId"]} success:^(id data) {
                        
                        
                        if(self.cblock){
                            
                            self.cblock(@"换人");
                        }
                        
                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                    } failure:^(id data) {
                        
                        
                        
                    }];
                    
                    
                    
                    
                }else{
                    
                    
                    [XHToast showBottomWithText:responseObject];
                    
                }
                
                
            } failure:^(NSError *error) {
                
                
            }];
            
        }] show];
        
        
    }else{
        //首发
        
        if ([homeLocationList count]==0) {
            [XHToast showBottomWithText:@"请选择主队上场人员"];
            return;
        }
        
        if ([visitLocationList count]==0) {
            [XHToast showBottomWithText:@"请选择客队上场人员"];
            return;
        }
        
        
        if ([homeLocationList count]>[[MatchHelper shareMatch].match.type  intValue]) {
            
            [XHToast showBottomWithText:@"主队超过允许上场人数"];
            
            return ;
            
        }
        if ([visitLocationList count]>[[MatchHelper shareMatch].match.type  intValue]) {
            
            [XHToast showBottomWithText:@"客队超过允许上场人数"];
            
            return ;
            
        }
        
        
        [[[FBAlertView alloc]initWithTitle:@"确认首发完毕？" butnTitlt:@"完毕" block:^(id data) {
            
            NSMutableDictionary *dic =[NSMutableDictionary dictionary];
            
            [dic setValue:self.matchId forKey:@"matchId"];
            
            [dic setValue:[self getLocation:homeLocationList] forKey:@"homeDisplay"];
            
            [dic setValue:[self getLocation:visitLocationList] forKey:@"visitingDisplay"];
            
            
            [HttpRequestHelper postWithURL:Url(@"game/starting") params:dic success:^(BOOL sucess, id responseObject) {
                
                if(sucess){
                    
                    
                    [MatchHelper MatchInfo:@{@"matchId":dic[@"matchId"]} success:^(id data) {
                        
                        
                        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"];
                        [controller setValue:self.matchId forKey:@"matchId"];
                        
                        [self presentViewController:controller animated:YES completion:nil];
                        
                    } failure:^(id data) {
                        
                    }];
                    
                    
                    
                    
                    
                }else{
                    
                    
                    [XHToast showBottomWithText:responseObject];
                    
                }
                
                
            } failure:^(NSError *error) {
                
                
                
                
                
            }];
            
        }] show];
        
        
    }
    
    
    
    
}
#pragma mark 获取首发人员位置
-(NSString *)getLocation:(NSMutableArray *)array{
    
    
    
    
    NSMutableArray *arr=[NSMutableArray array];
    
    
    for(PlayerView *view in array){
        NSMutableDictionary *dic =[NSMutableDictionary dictionary];
        
        
        [dic setObject:view.play.id forKey:@"id"];
        
        CGPoint point = [self positionFromloction:view.center];
        [dic setObject:@(point.x) forKey:@"x"];
        [dic setObject:@(point.y) forKey:@"y"];
        [dic setObject:view.play.avatar forKey:@"avatar"];
        [dic setObject:view.play.number forKey:@"number"];
        [dic setValue:view.play.realName forKey:@"realName"];
        [dic setObject:view.play.name forKey:@"name"];
        
        [arr addObject:dic];
        
        
        
    }
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
    
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
    
    
    
    
}
#pragma mark 加载场上人员
-(void)loadFristList{
    
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.homeStarting) {
        PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
        
        PlayersView *view = [[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        view.play=model;
        
        [view addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(viewPanAction:)]];
        
        view.center=[self locationfromPosition:CGPointMake([model.x doubleValue] , [model.y doubleValue])];
        
        [homeView addSubview:view];
        
        if (!homeLocationList) {
            homeLocationList =[NSMutableArray array];
        }
        
        [homeLocationList addObject:view];
        
        
        
        
    }
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.visitingStarting) {
        PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
        
        PlayersView *view = [[PlayersView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
        
        view.play=model;
        
        [view addGestureRecognizer:[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(viewPanAction:)]];
        
        view.center=[self locationfromPosition:CGPointMake([model.x doubleValue] , [model.y doubleValue])];
        
        [visitView addSubview:view];
        
        if (!visitLocationList) {
            visitLocationList =[NSMutableArray array];
        }
        
        [visitLocationList addObject:view];
        
        
        
        
    }
    
    
    
    
    
    [self teamBtnClick:homeTeam];
    
    
    
    
    
    
}
#pragma mark 加载替补人员
-(void)loadOtherList{
    
    
    homeOtherList=[NSMutableArray array];
    visitOtherList=[NSMutableArray array];
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.homePlayers) {
        PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
        [homeOtherList addObject:play];
        
    }
    
    
    for (NSDictionary *dic  in [MatchHelper shareMatch].match.visitingPlayers) {
        PlayerModel *play=[[PlayerModel alloc]initWithDic:dic];
        
        [visitOtherList addObject:play];
        
        
    }
    [homeTableList reloadData];
    [visitTableList reloadData];
    
    [self teamBtnClick:homeTeam];
    
    
}

#pragma mark 从位置计算坐标
-(CGPoint)locationfromPosition:(CGPoint)position{
    
    //视图大小320*320
    
    CGFloat width=bGwidth/37;
    CGPoint point=CGPointMake(position.x *width, position.y*width);
    //CGPoint point=CGPointMake(position.x *width, position.y >39?39:position.y *width);
    return point;
    
}
#pragma mark 从坐标计算位置
-(CGPoint)positionFromloction:(CGPoint)point{
    
    CGFloat width=bGwidth/37;
    int x=point.x/width;
    int y=(point.y)/width;
    
    //    if(x>37){
    //        x=37;
    //    }
    //    if(y>39){
    //        x=39;
    //    }
    
    return CGPointMake(x, y);
    
}
#pragma mark TableDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    if(tableView !=homeTableList){
        
        return [homeOtherList count];
        
    }else{
        return [visitOtherList count];
        
    }
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menberViewCell=@"MenberViewCell";
    
    
    MenberViewCell *cell=[tableView dequeueReusableCellWithIdentifier:menberViewCell forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=indexPath.row%2==0?BGCOLOR1:BGCOLOR2;
    
    [cell setValue:tableView !=homeTableList?homeLeader:visitLeader forKey:@"leader"];
    
    PlayerModel *model=tableView !=homeTableList?homeOtherList[indexPath.row]:visitOtherList[indexPath.row];
    [cell setValue:model forKey:@"data"];
    
    return cell;
    
    
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)prefersStatusBarHidden
{
    
    
    return YES;
}

@end
