//
//  TeamInfoViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamInfoViewController.h"
#import "TeamHelper.h"
#import "TeamModel.h"
#import "MatchModel.h"
#import "DataFormatHelper.h"

#import "ShareView.h"
#import "SendNewViewController.h"

@interface TeamInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    dispatch_once_t onceToken;
    __weak IBOutlet UITableView *teamTableView;

    __weak IBOutlet NSLayoutConstraint *btnWidth;
    
    NSMutableDictionary *rowDic;
    TeamModel *teamModel;
    __weak IBOutlet UIButton *bottom1Btn;
    __weak IBOutlet UIButton *bottom2Btn;
    __weak IBOutlet UIButton *bottom3Btn;
    __weak IBOutlet NSLayoutConstraint *buttom;

     NSString *shareText;
    
}
@end

@implementation TeamInfoViewController
- (void)payAttentionSuccess0
{
     [self loadTeamData];
}
- (void)payAttentionSuccess1
{
     [self loadTeamData];
}
- (void)shareClicked
{
     NSString *iconurl = [Defaults objectForKey:@"strICON"];
    ShareView *view = [[ShareView alloc] initWithFrame:CGRectMake(0, kScreenHeight - 160, kScreenWidth, 160)];
    NSString *shareURL = [NSString stringWithFormat:@"%@?teamId=%@",Url(@"team/share"),_teamId];
    NSString *strmessage = @"我在使用全网篮球，这是我们球队的数据";
    view.title = @"全网篮球";//一般是你的App名字
    view.message = strmessage;//需要展示的信息简介
    view.shareUrl = shareURL;//分享的内容地址(点击跳转的地址)
    if(iconurl && ![iconurl isEqualToString:@"(null)"])
    {
        view.pictureName =iconurl;
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@"分析图标未获取到"];
    }
//    view.pictureName = iconurl;//分享的图片名称（必须在左侧项目栏里，且为.png格式，因为shareView进行了配置）
    shareText = [strmessage stringByAppendingString:[NSString stringWithFormat:@"\n%@",shareURL]];
    view.weretogo = @"球队分享";
    /**举个例子
     
     假如是京东某件商品分享
     *title 可以写京东
     *message 商品的介绍（一行字，如促销信息）
     *shareUrl 则为该商品的地址
     *pictureName 该商品的图片
     
     
     以上数据在开发中后端都会有相应的数据
     
     */
    
    [view show];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"分享.png"] style:UIBarButtonItemStylePlain target:self action:@selector(shareClicked)];
    self.navigationItem.rightBarButtonItem = item;
    
    
     btnWidth.constant=kScreenWidth/2.0;
    [self loadTableView];
    
    
    [self loadTeamData];
    
    self.navigationItem.title=@"球队详情";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payAttentionSuccess0) name:@"payAttentionSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payAttentionSuccess1) name:@"unpayAttentionSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotologins) name:@"gotologins" object:nil];
 
    
    //这里是分享到圈子的注册
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoSendControllerTeam) name:@"gotoSendControllerTeam" object:nil];
    
}
- (void)gotoSendControllerTeam
{
    
    
    if ([AccountHelper shareAccount].isLogin ==YES) {
        
        
        SendNewViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SendNewViewController"];
        controller.shareMarkText = @"分享到圈子";
        controller.shareContext = shareText;
        [self.navigationController pushViewController:controller animated:YES];
        
    }else{
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
    
    
    
}
-(void)loadTableView{

    teamTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    teamTableView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    teamTableView.rowHeight=UITableViewAutomaticDimension;
    teamTableView.estimatedRowHeight=100;
  
}
-(void)loadTeamData{

    
    [TeamHelper getTeam:@{@"teamId":self.teamId} success:^(id data) {
        
        teamModel=[[TeamModel alloc]initWithDic:data];
        
        
        self.navigationItem.title=teamModel.name;
        
        [self showButtomView];
        
        //获取历史球队记录
        [self LoadMatchTeamInfo:^(id data) {
            rowDic= [self witchCellForRow];
 
            dispatch_async(dispatch_get_main_queue(), ^{
                 [teamTableView reloadData];
            });
            
           


        }];
        
        
        
    } failure:^(id data) {
        
    }];
}

- (void)LoadMatchTeamInfo:(MutableBlock)block
{
    
        [HttpRequestHelper postWithURL:Url(@"match/queryMatchByTeamId") params:@{@"teamId":self.teamId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
    
                for (NSDictionary *dic  in responseObject) {
                    
                    MatchModel *model = [[MatchModel alloc]initWithDic:dic];
 
                     NSString *date=[DataFormatHelper timeChangeFormart:@"MM月dd日 EEEE" times:model.date];

                    BaseCellModel *base=nil;
   
                    if ([model.lid intValue] == 10000) {
                        
                        
                        if ([model.status intValue]==0) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                            
                        }
                        
                        //赛前
                        if ([model.status intValue]==10) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                            
                        }
                        //比赛中
                        if ([model.status intValue]==20||[model.status intValue]==21) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                            
                        }
                        //比赛结束
                        if ([model.status intValue]==100) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                            
                        }
                        
                    }else{
                        //約赛中
                        if ([model.status intValue]==0) {
                            
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                        }
                        //以匹配（赛前）
                        if ([model.status intValue]==10) {
                            
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell4"];
                        }
                        //比赛中
                        if ([model.status intValue]==20) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                        }
                        if ([model.status intValue]==20||[model.status intValue]==21) {
                            
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                        }
                        //比赛结束
                        if ([model.status intValue]==100) {
                            base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                        }
                    }
              
//                    [self.dataArr addObject:base];
                    
                    
                    
                    if ([self.dataDic objectForKey:date]) {
                        
                        NSMutableArray *arr=[NSMutableArray array];
                        [arr addObjectsFromArray:[self.dataDic objectForKey:date]];
                        [arr addObject:base];
                        
                        [self.dataDic setObject:arr forKey:date];
                        
                        
                    }else{
                        
                        NSMutableArray *arr=[NSMutableArray array];
                        
                        [arr addObject:base];
                        
                        [self.dataDic setObject:arr forKey:date];
                        
                    }
                   
                }
                
                block(@"");
           }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
               
            }
        } failure:^(NSError *error) {
            
            
        }];
    
}

-(void)showButtomView{
    
    //游客模式 没有登录 可以点击加入球队
    
    if([AccountHelper shareAccount].isLogin!=YES){
    
        bottom3Btn.hidden=NO;
        [bottom3Btn setTitle:@"加入球队" forState:0];
//        [bottom2Btn setTitle:@"发起约战" forState:0];

    
    }else{
        
       //如果我是这个球队队长
        if ([teamModel.leader intValue]== [[AccountHelper shareAccount].player.id intValue]) {
            bottom3Btn.hidden=NO;
            [bottom3Btn setTitle:@"管理球队" forState:0];
        }else{
        
        //判断我是不是球队成员
            
            BOOL isMenber=NO;
            
            for(NSDictionary *dic in teamModel.players){
            
                if ([dic[@"id"] intValue] == [[AccountHelper shareAccount].player.id intValue]) {
                    
                    isMenber=YES;
                    break;
                    
                }
            
            }

            if (isMenber) {
                
                bottom3Btn.hidden=NO;
                [bottom3Btn setTitle:@"退出球队" forState:0];
            }else{
            
                bottom3Btn.hidden=NO;
                [bottom3Btn setTitle:@"加入球队" forState:0];
//                [bottom2Btn setTitle:@"发起约战" forState:0];
            
            }

        }
        
        
        
    
    }




}
-(IBAction)buttomBtnClick:(UIButton *)butn{
    

    if ([butn.titleLabel.text isEqualToString:@"加入球队"]) {
        
        
        if([AccountHelper shareAccount].isLogin!=YES){
            
            
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
            controller.navigationItem.title=@"登录";
            [self.navigationController pushViewController:controller animated:YES];
            
           
            
            
        }else{
        
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinTeamViewController"];
            controller.navigationItem.title=@"全网篮球";
            [controller setValue:teamModel.id forKey:@"teamId"];
            
            [controller setControllerBlock:^(id data) {
                
                [self loadTeamData];
                
            }];
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }
 
    }
    
    if ([butn.titleLabel.text isEqualToString:@"管理球队"]) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamManageViewController"];
        controller.navigationItem.title=@"管理球队";
          [controller setValue:teamModel forKey:@"team"];
        
        [controller setControllerBlock:^(id data) {
            
             [self loadTeamData];
            
//            self.cblock(@"");
            
        }];
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
    if ([butn.titleLabel.text isEqualToString:@"退出球队"]) {
        
        
        
        [[[FBAlertView alloc]initWithTitle:@"是否确认退出球队" butnTitlt:@"退出" block:^(id data) {
            
            [TeamHelper kickoutTeam:@{@"teamId":_teamId,@"userId":[AccountHelper shareAccount].player.id} success:^(id data) {
                
                [SVProgressHUD showSuccessWithStatus:@"退出成功"];
                
                [self  loadTeamData];
                
                
                
            } failure:^(id data) {
                
                
                
                
            }];
            
        }]show];
        
        
        
       
        
    }
    
    if ([butn.titleLabel.text isEqualToString:@"发起约战"]) {
        

    }



}



#pragma mark tableView 委托协议
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [[rowDic allKeys] count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0)
    {
        return 5;
    }
    else
    {
          return 25;
    }
        
  
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *view=[[UIView alloc]init];
    
    if (section!=0) {
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 4, 55, 17)];
        label.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
        label.font=[UIFont systemFontOfSize:10];
        label.layer.cornerRadius=3;
        label.textColor=[UIColor whiteColor];
        label.textAlignment=1;
        label.layer.masksToBounds=YES;
        if (section==1) {

            label.text=@"技术统计";

        }
        
        if (section==2) {
            
            label.text=@"球队球员";
            
        }
        
        if (section>2) {
            
            
            
            
            NSString *string =[DataFormatHelper compareDate :self.dataArr[section-3]];
            string= [string stringByReplacingOccurrencesOfString:@"Monday" withString:@"星期一"];
            string= [string stringByReplacingOccurrencesOfString:@"Tuesday" withString:@"星期二"];
            string= [string stringByReplacingOccurrencesOfString:@"Wednesday" withString:@"星期三"];
            string= [string stringByReplacingOccurrencesOfString:@"Thursday" withString:@"星期四"];
            string= [string stringByReplacingOccurrencesOfString:@"Friday" withString:@"星期五"];
            string= [string stringByReplacingOccurrencesOfString:@"Saturday" withString:@"星期六"];
            string= [string stringByReplacingOccurrencesOfString:@"Sunday" withString:@"星期天"];
            
            label.text=string;
            
            if([label.text rangeOfString:@"今天"].location != NSNotFound)//_roaldSearchText
            {
                label.backgroundColor=TODAYCOLOR;
            }
            else
            {
                label.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
            }

            
            [label sizeToFit];
            label.frame=CGRectMake(0, 4, label.frame.size.width+2, 17);
        }

        [view addSubview:label];
    }

    return view;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    
    
    
    BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    [cell setCellBlock:^(id data) {
        
        MatchModel *match=model.value;

        
        if([data isKindOfClass:[PlayerModel class]]){
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
            PlayerModel *play=data;
            //如果是我自己
            if ([play.id intValue]==[[AccountHelper shareAccount].player.id intValue]) {
                controller.navigationItem.title=@"我的";
            }else{
                controller.navigationItem.title=play.name;
                [controller setValue:play.id forKey:@"userId"];
            }
            [self.navigationController pushViewController:controller animated:YES];
        
        }
        //下面的两个是点击头像的回调，但是没有用，不让它跳转，故而注释了跳转了代码
        else if ([data isEqualToString:@"left"])
        {
            TeamModel *team=[[TeamModel alloc]initWithDic:match.home];
            [self teamHeadImageClick:team.id];
        }
        else if ([data isEqualToString:@"right"])
        {
            TeamModel *team=[[TeamModel alloc]initWithDic:match.visiting];
              [self teamHeadImageClick:team.id];
        }
        else{
        
        
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
        }

        
    }];
    
    [cell setValue:model forKey:@"data"];
    
    //点击头像跳转，这里不跳转
 
    if (indexPath.section !=1) {
        if (indexPath.row%2) {
            
            cell.contentView.backgroundColor=BGCOLOR1;
            
            
        }else{
            cell.contentView.backgroundColor=BGCOLOR2;
        }
    }else{
        
        cell.contentView.backgroundColor=[UIColor clearColor];
        cell.backgroundColor=[UIColor clearColor];
    }
    
    
    return cell;
    
}

-(void)teamHeadImageClick:(NSString *)string{
    
//    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
//    [controller setValue:string forKey:@"teamId"];
//    [self.navigationController pushViewController:controller animated:YES];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
      MatchModel *match=model.value;
    if([model.reuseIdentifier isEqualToString:@"WatchCell"]){
 
    }
    
    if(indexPath.section > 2)
    {
        
        [self gotosomewhere:match];
        
    }
}
- (void)gotosomewhere:(MatchModel *)match
{
    if ([match.playerType  intValue] == 10000 ) {
        
        //赛前
        if ([match.status intValue]==10) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            controller.navigationItem.title=@"赛前";
            [controller setValue:match.id forKey:@"matchId"];
            
            [controller setControllerBlock:^(id data) {
                
                
                
            }];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        //比赛中
        if ([match.status intValue]==20) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            controller.navigationItem.title=@"赛中";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        //比赛结束
        if ([match.status intValue]==100) {
            
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
            controller.navigationItem.title=@"赛后";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }
        
    }else{
        //約赛中
        if ([match.status intValue]==0) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinWarViewController"];
            
            controller.navigationItem.title=@"约赛中";
            [controller setValue:match.id forKey:@"matchId"];
            
            [controller setControllerBlock:^(id data) {
                
                
            }];
            
            [self.navigationController pushViewController:controller animated:YES];
            
            
            
        }
        //以匹配（赛前）
        if ([match.status intValue]==10) {
            
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            
            controller.navigationItem.title=@"赛前";
            [controller setValue:match.id forKey:@"matchId"];
            
            [self.navigationController pushViewController:controller animated:YES];
            
            
        }
        //比赛中
        if ([match.status intValue]==20) {
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            
            controller.navigationItem.title=@"赛中";
            [controller setValue:match.id forKey:@"matchId"];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        //比赛结束
        if ([match.status intValue]==100) {
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
            controller.navigationItem.title=@"赛后";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}
-(NSMutableDictionary *)witchCellForRow{
    
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    int sencion=0;
    
    
        [BaseCellModel insetKeyForDic:dic key:[NSString stringWithFormat:@"%d",sencion] value:@[
                                                           
         [BaseCellModel setModel:@"" key:_teamId value:teamModel image:@"" reuseIdentifier:@"TeamInfo1Cell"],
         
                                            ]];
    sencion++;
        [BaseCellModel insetKeyForDic:dic key:[NSString stringWithFormat:@"%d",sencion] value:@[
                                                       
                                                       [BaseCellModel setModel:@"" key:@"" value:teamModel image:@"" reuseIdentifier:@"Team"],
                                                       
                                                       ]];
   sencion++;
    
        [BaseCellModel insetKeyForDic:dic key:[NSString stringWithFormat:@"%d",sencion] value:@[
                                                       
                                                       [BaseCellModel setModel:@"" key:teamModel.leader value:teamModel.players image:@"" reuseIdentifier:@"TeamMenberCell"],
                                                       
                                                       ]];
    
    
    if ([[self.dataDic allKeys] count]>0) {
        
        [self sortDate];
        
        for(NSString * dateStr in self.dataArr){
        
        
            sencion++;
        
            [BaseCellModel insetKeyForDic:dic key:[NSString stringWithFormat:@"%d",sencion] value:self.dataDic[dateStr]];
        
        }
        
        
        
        
        
    }
 
    return dic;
    
}

-(void)sortDate{
    
    
    dispatch_once(&onceToken, ^{
    self.dataArr=[self.dataDic allKeys];
    
    self.dataArr = (NSMutableArray *)[self.dataArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM月dd日 EEEE"];
        
        NSDate *date1 = [formatter dateFromString:obj1];
        NSDate *date2 = [formatter dateFromString:obj2];
        NSComparisonResult result = [date1 compare:date2];
        return result == NSOrderedAscending;
    }];
});
}

- (void)gotologins
{
    [[[FBAlertView alloc ]initWithTitle:@"您还未登陆，是否去登陆？" butnTitlt:@"去登陆!" block:^(id data) {
        [AccountHelper shareAccount].isReg=NO;
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
    }]show];
}



@end
