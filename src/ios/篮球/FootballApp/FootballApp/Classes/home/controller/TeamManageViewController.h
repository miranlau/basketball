//
//  TeamManageViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"
#import "TeamModel.h"
@interface TeamManageViewController : BaseViewController

@property(retain,nonatomic)TeamModel *team;

@end
