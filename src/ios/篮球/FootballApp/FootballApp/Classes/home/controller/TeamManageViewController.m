//
//  TeamManageViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamManageViewController.h"
#import "TeamHelper.h"
#import "OnePhotoSelecter.h"
#import "ManageCell.h"
@interface TeamManageViewController ()
{

    __weak IBOutlet UITableView *manageTableView;

    BaseViewController *lcontroller;
}

@end

@implementation TeamManageViewController

- (void)viewWillAppear:(BOOL)animated
{

}
- (void)viewDidLoad {
    [super viewDidLoad];
       
    [self loadTableView];
    
    
    [self witchCellForRow];
    
    [manageTableView reloadData];
}
-(void)loadTableView{
    
    
    
    manageTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    manageTableView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    manageTableView.rowHeight=UITableViewAutomaticDimension;
    manageTableView.estimatedRowHeight=100;
    
}
-(NSMutableDictionary *)witchCellForRow{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    
    
    [BaseCellModel insetKeyForDic:self.dataDic key:@"0" value:@[
                                                       
                                                       [BaseCellModel setModel:@"" key:@"" value:_team image:@"" reuseIdentifier:@"ManageCell1"]
                                                    
                                                       ]];
    
    [BaseCellModel insetKeyForDic:self.dataDic key:@"1" value:@[
                                                                
                                                               
                                                                [BaseCellModel setModel:@"管理球员" key:@"height" value:[AccountHelper shareAccount].player.height image:@"" reuseIdentifier:@"ManageCell2"],
                                                                [BaseCellModel setModel:@"修改球队暗号" key:@"weight" value:[AccountHelper shareAccount].player.weight image:@"" reuseIdentifier:@"ManageCell2"],
                                                                [BaseCellModel setModel:@"解散球队" key:@"age" value:[AccountHelper shareAccount].player.age image:@"" reuseIdentifier:@"ManageCell2"]
                                                                
                                                                ]];
    

    return dic;
    
}
#pragma mark tableView 委托协议

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return [[self.dataDic allKeys]count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[self.dataDic  objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view=[[UIView alloc]init];
    view.backgroundColor=[UIColor clearColor];
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[self.dataDic  objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
     ManageCell*cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    //回调事件
 
    if (indexPath.row%2) {
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    
    [cell setValue:model forKey:@"data"];
    
    
    [cell setCellBlock:^(id data) {
        
        
        [self changRowInfo:indexPath type:data];
        
    }];
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    ManageCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    
    
    NSArray *arr=[self.dataDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    if ([model.reuseIdentifier isEqualToString:@"ManageCell1"]) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamNameViewController"];
        controller.navigationItem.title=@"修改球队名字";
        [controller setValue:_team.id forKey:@"teamId"];
        [controller setValue:@"1" forKey:@"type"];
        
        [controller setControllerBlock:^(id model) {
            _team.name=model;
            cell.teamName.text=model;
            
        }];
        
        
        if (self.cblock) {
            self.cblock(@"修改");
        }
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    if ([model.name isEqualToString:@"管理球员"]){
        
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangeMenberViewController"];
        controller.navigationItem.title=model.name;
        [controller setValue:_team.id forKey:@"teamId"];
        controller.dataArr=[[NSMutableArray alloc ]initWithArray:_team.players];
        lcontroller=controller;
        [controller setControllerBlock:^(id data) {
            
            
            
            
            
           
            if (self.cblock) {
                self.cblock(@"修改");
            }
            
            
            
            [self loadTeamInfo:^(id data) {
               
                 lcontroller.dataArr=[[NSMutableArray alloc ]initWithArray:_team.players];
                [lcontroller performSelector:NSSelectorFromString(@"reloadTableView")];
            }];
        }];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    if ([model.name isEqualToString:@"修改球队暗号"]){
        
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamNameViewController"];
        controller.navigationItem.title=model.name;
        [controller setValue:_team.id forKey:@"teamId"];
        [controller setValue:@"2" forKey:@"type"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
    if ([model.name isEqualToString:@"解散球队"]){
        
        
        
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamNameViewController"];
        controller.navigationItem.title=model.name;
        [controller setValue:_team.id forKey:@"teamId"];
        [controller setValue:@"4" forKey:@"type"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
        
        
    }

}
-(void)changRowInfo:(NSIndexPath *)index type:(NSString *)type{
    
    if ([type isEqualToString:@"head"]) {
        
        [[OnePhotoSelecter shareInstance] selectImage:self imageBlock:^(id data) {
            
            ManageCell  *cell=[manageTableView cellForRowAtIndexPath:index];
            cell.teamLogo.image=data;
            
            [SVProgressHUD showWithStatus:@"上传图片中"];
            
            
            [HttpRequestHelper postWithURL:Url(@"resource/upload") params:@{@"type":@"team"} formDataArray:@[data] success:^(BOOL sucess, id responseObject1) {
                
                
                if (sucess) {
                    
                    [TeamHelper changeTeam:@{@"teamId":_team.id,@"logo":responseObject1[0]} success:^(id data) {
                        
                        
                        [SVProgressHUD showSuccessWithStatus:@"修改球队logo成功"];
                        if (self.cblock) {
                            self.cblock(@"修改");
                        }
                        
                    } failure:^(id data) {
                        
                        
                         [SVProgressHUD showErrorWithStatus:@"修改球队logo失败"];
                       
                    }];
          
                    
                }else{
                    
                    [SVProgressHUD showErrorWithStatus:responseObject1];
                    
                }
                
                
            } failure:^(NSError *error) {
                
                
            }];
         
        }];
    }
  
}

-(void)loadTeamInfo:(MutableBlock)block{
    
    

    [TeamHelper getTeam:@{@"teamId":_team.id} success:^(id data) {
        
        _team=[[TeamModel alloc]initWithDic:data];
        RadarDto *radarDto=[[RadarDto alloc]initWithDic:_team.radarDto];
        _team.radarDto=radarDto;
        
        block(@"");
        
    } failure:^(id data) {
        
    }];
   
    
    

    
   
}

@end
