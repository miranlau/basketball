//
//  TeamNameViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface TeamNameViewController : BaseViewController
@property(retain,nonatomic)NSString *teamId;
@property(assign,nonatomic)int type;
@end
