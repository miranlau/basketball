//
//  TeamNameViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamNameViewController.h"
#import "TeamHelper.h"
@interface TeamNameViewController ()<UITextFieldDelegate>
{
    __weak IBOutlet UITextField *teamName;

    __weak IBOutlet UILabel *message;

    __weak IBOutlet UIButton *button;

}
@end

@implementation TeamNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    [teamName becomeFirstResponder];
    teamName.delegate = self;
    switch (_type) {
        case 1:
            
            teamName.placeholder=@"请输入您的球队的新名字";
            message.text=@"请输入您的球队的新名字";
            break;
        case 2:
            teamName.placeholder=@"请输入您的球队的新暗号";
            message.text=@"请输入您的球队的新暗号";
            break;
        case 3:
            teamName.placeholder=@"请输入0-99内的球衣号";
            message.text=@"请输入该球员球衣号码";
          
            break;
            
        case 4:
            teamName.placeholder=@"我确定解散球队";
            message.text=@"请输入'我确定解散球队'来解散球队";
            break;
            
        default:
            break;
    }

}

- (IBAction)changeBtnClick:(id)sender {
    
    if (_type==1) {
        if([teamName.text isEqualToString:@""]){
            
            
            [SVProgressHUD showErrorWithStatus:@"请输入您的球队的新名字"];
            
            return;
            
        }
        
        [TeamHelper changeTeam:@{@"teamId":_teamId,@"name":teamName.text} success:^(id data) {
            
            self.cblock(teamName.text);
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } failure:^(id data) {
            
        }];
    }
    
    if (_type==2) {
        if([teamName.text isEqualToString:@""]){
            
            
            [SVProgressHUD showErrorWithStatus:@"请输入您的球队的新暗号"];
            
            return;
            
        }
        
        [TeamHelper changeTeam:@{@"teamId":_teamId,@"password":teamName.text} success:^(id data) {
            
         
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } failure:^(id data) {
            
        }];
    }
    
    if (_type==3) {
        
        
        if([teamName.text isEqualToString:@""]){
            
            
            [SVProgressHUD showErrorWithStatus:@"请输入该球员球衣号码"];
            
            return;
            
        }
        
        
        [self.dataDic setObject:teamName.text forKey:@"number"];
        
        
        [TeamHelper menberNum:self.dataDic success:^(id data) {
            self.cblock(teamName.text);
          
            [self.navigationController popViewControllerAnimated:YES];
            
        } failure:^(id data) {
           [SVProgressHUD showErrorWithStatus:data];
        }];
    }
    
    if (_type==4) {
        
        
        if(![teamName.text isEqualToString:@"我确定解散球队"]){
            
            
            [SVProgressHUD showErrorWithStatus:@"请输入我确定解散球队"];
            
            return;
            
        }
        
        
            [TeamHelper dissolveTeam:@{@"teamId":_teamId} success:^(id data) {
        
        
                [SVProgressHUD showSuccessWithStatus:@"解散球队成功"];
                
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"joinTeam" object:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                
            } failure:^(id data) {
                
                
                
            }];
    }
    
    
}

//这里只能输数字
- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

//让我的球衣号在0-99之间，多了再输就没用
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self.navigationItem.title isEqualToString:@"修改球员号码"]){
        if (textField == teamName) {
            if (string.length == 0) return YES;
            
            NSInteger existedLength = textField.text.length;
            NSInteger selectedLength = range.length;
            NSInteger replaceLength = string.length;
            if (existedLength - selectedLength + replaceLength > 2) {
                return NO;
            }
        }
    }
    
    //这里判断了是否是修改球衣
    if (_type!=3) {
        return YES;
    }else{
    return [self validateNumber:string];
    }
}
- (void)textFieldDidChange:(UITextField *)textField
{
     if ([self.navigationItem.title isEqualToString:@"修改球员号码"]){
         if (textField == teamName) {
             if (textField.text.length > 2) {
                 textField.text = [textField.text   substringToIndex:2];
             }
         }
     }
}
@end
