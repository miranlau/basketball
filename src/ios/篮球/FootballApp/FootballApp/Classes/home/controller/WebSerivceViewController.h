//
//  WebSerivceViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

#import "EGORefreshTableHeaderView.h" //UIWebView添加刷新头，像UITableView一样

@interface WebSerivceViewController : BaseViewController<UIWebViewDelegate, UIScrollViewDelegate, EGORefreshTableHeaderDelegate>
{
    //下拉视图
    EGORefreshTableHeaderView * _refreshHeaderView;
    //刷新标识，是否正在刷新过程中
    BOOL _reloading;
}
@property(strong,nonatomic)NSString *matchId;
- (void)getshareInfo;
@end
