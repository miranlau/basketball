//
//  WebSerivceViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import "WebSerivceViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "OnePhotoSelecter.h"
#import "AvatarHelper.h"
#import "AdvertWebController.h"

#import "ShareView.h"

#import "shareModle.h"

#import "SendNewViewController.h"

@protocol TeamProtocol <JSExport>

- (void)SendComment:(NSArray *)comment; //评论动态方法
- (void)SendPhotoAction:(NSString *)matchId;  //点赞
- (void)PhotoZoom:(NSString *)photoUrl;
- (void)ActionGoToTeams:(NSArray *)homeIdArray;  //点击球队
- (void)ActionGoToMatchs:(NSArray *)send;

- (void)commonJSBridge:(NSArray *)url;
@end






@interface WebSerivceViewController ()<TeamProtocol>
{

    __weak IBOutlet UIWebView *_webView;

      JSContext *context;
    shareModle *sharmodle;
    
    NSDictionary *shareDic;
    
    int status;
    
    NSString *shareText;
}

@end

@implementation WebSerivceViewController

- (void)gotoSendController
{

    
    if ([AccountHelper shareAccount].isLogin ==YES) {
        
        
        SendNewViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SendNewViewController"];
        controller.shareMarkText = @"分享到圈子";
        controller.shareContext = shareText;
        [self.navigationController pushViewController:controller animated:YES];
        
    }else{
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
    
   
 
}

- (void)shareClickedAfterRaces
{
    ShareView *view = [[ShareView alloc] initWithFrame:CGRectMake(0, kScreenHeight - 160, kScreenWidth, 160)];
    
    if (status == 10)
    {
//        view.title = shareDic[@"title"];//一般是你的App名字
//        view.message = sharmodle.description;//需要展示的信息简介
//        view.shareUrl = sharmodle.url;//分享的内容地址(点击跳转的地址)
//        view.pictureName = sharmodle.icon;//分享的图片名称（必须在左侧项目栏里，且为.png格式，因为shareView进行了配置）
        
        view.title = shareDic[@"title"];//一般是你的App名字
        view.message = shareDic[@"description"];//需要展示的信息简介
        view.shareUrl = shareDic[@"url"];//分享的内容地址(点击跳转的地址)
        view.pictureName  = shareDic[@"icon"];//分享的图片名称（必须在左侧项目栏里，且为.png格式，因为shareView进行了配置）
//        shareText = [NSString stringWithFormat:@"%@ , %@" ,shareDic[@"description"],shareDic[@"url"]];
        NSString *str1 = shareDic[@"description"];
        NSString *str2 = shareDic[@"url"];
        shareText = [str1 stringByAppendingString:[NSString stringWithFormat:@"\n%@",str2]];
        
        [view show];
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@"获取分享信息时失败"];
    }
    

    
    
    /**举个例子
     
     假如是京东某件商品分享
     *title 可以写京东
     *message 商品的介绍（一行字，如促销信息）
     *shareUrl 则为该商品的地址
     *pictureName 该商品的图片
     
     
     以上数据在开发中后端都会有相应的数据
     
     */
    
    
    
}

- (void)getshareInfo
{

    [HttpRequestHelper postWithURL:Url(@"match/share") params:@{@"matchId":_matchId} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
            shareDic=responseObject;
            NSDictionary *dic = responseObject;
             sharmodle =[[shareModle alloc]initWithDic:dic];
            NSString *stricon = responseObject[@"icon"];
            [Defaults setObject:stricon forKey:@"strICON"];
            [Defaults synchronize];
        }
        else
        {
            status = 11;
        }
        
    } failure:^(NSError *error) {
        status = 12;
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    status = 10;
    
    _webView.scrollView.delegate = self;
    _webView.delegate = self;
     [self loadPage];
    //初始化refreshView，添加到webview 的 scrollView子视图中
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-_webView.scrollView.bounds.size.height, _webView.scrollView.frame.size.width, _webView.scrollView.bounds.size.height)];
        _refreshHeaderView.delegate = self;
        [_webView.scrollView addSubview:_refreshHeaderView];
    }
    [_refreshHeaderView refreshLastUpdatedDate];
  
    
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"分享.png"] style:UIBarButtonItemStylePlain target:self action:@selector(shareClickedAfterRaces)];
    self.navigationItem.rightBarButtonItem = item;
    
    //进入页面就网络得到要分享的东西
    [self getshareInfo];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoSendController) name:@"gotoSendController" object:nil];
}

//加载网页
- (void)loadPage {
    
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"%@?matchId=%@&height=%.f",Url(@"game/detail"),_matchId,kScreenHeight-64]];
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:url];
    
    
    [[LoadingView shareView]start];
    
    if ([AccountHelper shareAccount].isLogin==YES) {
        
        [request setValue:[AccountHelper shareAccount].token forHTTPHeaderField:@"token"];
        
    }else{
        [request setValue:@"" forHTTPHeaderField:@"token"];
        
    }
    [_webView loadRequest:request];
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    _reloading = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"load page error:%@", [error description]);
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_webView.scrollView];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self loadPage];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
    
    return [NSDate date]; // should return date data source was last changed
}

//77 直播信息
- (void)SendComment:(NSArray *)comment
{
    NSLog(@"commment = %@",comment);
    //因为传到服务器的数据的编码是UTF-8，在服务器看到是乱码，那说明服务器解析数据采用的可能不是UTF-8，使用中文操作系统，默认的编码是GBK,
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSString *str = comment[1];
    NSData *data = [str dataUsingEncoding:enc];
    NSString *retStr = [[NSString alloc] initWithData:data encoding:enc];
    
    [HttpRequestHelper postWithURL:Url(@"game/sendMessage") params:@{@"msg":retStr,@"matchId":comment[0],@"status":@"77"} success:^(BOOL sucess, id responseObject) {
        if (sucess)
        {
            
           [SVProgressHUD showInfoWithStatus:@"评论发送成功"];
            
            [self viewDidLoad];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
    }];

}

//88 直播图片
- (void)SendPhotoAction:(NSString *)matchId
{
    NSLog(@"matchId = %@",matchId);

    
    [[OnePhotoSelecter shareInstance] selectImage:self imageBlock:^(id data) {
   
        [SVProgressHUD showWithStatus:@"上传图片中"];
        
        [HttpRequestHelper postWithURL:Url(@"resource/upload") params:@{@"type":@"other"} formDataArray:@[data] success:^(BOOL sucess, id responseObject1) {
            
            NSLog(@"photo = %@",responseObject1);
            NSString *strPhoto = responseObject1[0];
              NSLog(@"photonew = %@",strPhoto);
            if (sucess) {

               
                    [HttpRequestHelper postWithURL:Url(@"game/sendMessage") params:@{@"msg":responseObject1[0],@"matchId":matchId,@"status":@"88"} success:^(BOOL sucess, id responseObject) {
                        if (sucess)
                        {
                
                            [SVProgressHUD showSuccessWithStatus:@"图片上传成功"];
                
                            [self viewDidLoad];
                        }
                        else
                        {
                            [SVProgressHUD showErrorWithStatus:responseObject];
                        }
                        
                    } failure:^(NSError *error) {
                        
                    }];
//
            }
            
            
        } failure:^(NSError *error) {
            
            
        }];
    
    }];
    
    
}
//点击图片放大
- (void)PhotoZoom:(NSString *)photoUrl
{
    UIImageView *imageview = [[UIImageView alloc]init];
     [imageview setImageWithURL:[NSURL URLWithString:photoUrl] placeholderImage:HeadImage];
    [AvatarHelper showImage:imageview];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
    // 通过UIWebView获得网页中的JavaScript执行环境
    context = [_webView valueForKeyPath:
               @"documentView.webView.mainFrame.javaScriptContext"];
    // 设置处理异常的block回调
    [context setExceptionHandler:^(JSContext *ctx, JSValue *value) {
        NSLog(@"error: %@", value);
    }];
    context[@"JSBridge"] = self;
    
    
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_webView.scrollView];
    
    [[LoadingView shareView]stop];
    
    
    
    
}


- (void)ActionGoToTeams:(NSArray *)homeId
{
//     NSLog(@"11:%@,%@",homeId[0],homeId[1]);
    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    
//    controller.navigationItem.title=tm.name;
    [controller setValue:homeId[1] forKey:@"teamId"];
    
    [self.navigationController pushViewController:controller animated:YES];
}

//这个里面已经是赛后，可以不用跳转了，所以下面代码注释了
- (void)ActionGoToMatchs:(NSArray *)send
{
//    NSLog(@"22:%@,%@",send[0],send[1]);
//    NSString *status  = send[0];
//    NSString *matchid = send[1];
//    //赛前
//    if ([status intValue]==10) {
//        
//        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
//        controller.navigationItem.title=@"赛前";
//        [controller setValue:matchid forKey:@"matchId"];
//        
//      
//        [self.navigationController pushViewController:controller animated:YES];
//        
//    }
//    //比赛中
//    if ([status intValue]==20) {
//        
//        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
//        controller.navigationItem.title=@"赛中";
//        [controller setValue:matchid forKey:@"matchId"];
//        [self.navigationController pushViewController:controller animated:YES];
//        
//    }
//    //比赛结束
//    if ([status intValue]==100) {
//
//        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
//        controller.navigationItem.title=@"赛后";
//        [controller setValue:matchid forKey:@"matchId"];
//        [self.navigationController pushViewController:controller animated:YES];
//
//    }
    
}

//球员进入后，球员表现
- (void)commonJSBridge:(NSArray *)url
{

    NSString *strurl = url[0];
    NSString *title  = url[1];
 
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AdvertWebController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvertWebController"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vc.urlStr1 = Url(strurl);
        vc.navigationItem.title = title;
        [self.navigationController pushViewController:vc animated:YES];
    
}

@end
