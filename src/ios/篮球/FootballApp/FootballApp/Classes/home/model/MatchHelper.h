//
//  MatchHelper.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface MatchHelper :BaseModel

@property(assign,nonatomic) int duration;//比赛进行时长(秒)
@property(retain,nonatomic) NSString* homeScore;//主队分
@property(retain,nonatomic) NSString* visitingScore;//客队分
@property(retain,nonatomic) id home;//主队
@property(retain,nonatomic) NSString  * type;
@property(retain,nonatomic) id visiting;//客队，参考Team定义
@property(retain,nonatomic) id statring;//上场整容
@property(retain,nonatomic) id players;//球员名单
@property(assign,nonatomic) NSString * isSuspend;//是否暂停
@property(assign,nonatomic) int status;//比赛进行状态状态
@property(retain,nonatomic) NSString *id;
@property(retain,nonatomic) NSString * season;//节数
@property(retain,nonatomic) id homeStarting;//主队场上人员阵容
@property(retain,nonatomic) id visitingStarting;

@property(retain,nonatomic) id homePlayers;//主队没上场人员
@property(retain,nonatomic) id visitingPlayers;
@property(retain,nonatomic) NSArray * actions;


@property(strong,nonatomic) MatchHelper *match;




+(instancetype)shareMatch;


+(NSMutableDictionary *)playerA:(NSString *)playerA playerB:(NSString *)playerB status:(NSString *)status matchId:(id)matchId teamId:(NSString *)teamId;

+(void)matchActionRequest:(NSDictionary *)dic  success:(MutableBlock)sucessd failure:(MutableBlock)failured;


+(void)MatchInfo:(NSDictionary *)dic  success:(MutableBlock)sucessd failure:(MutableBlock)failured;

@end
