//
//  MatchHelper.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MatchHelper.h"
#import "XHToast.h"
@implementation MatchHelper
+(instancetype)shareMatch{

    static MatchHelper *account=nil;
    
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        account = [[self alloc] init] ;
    }) ;
    
    return account;
}
+(NSMutableDictionary *)playerA:(NSString *)playerA playerB:(NSString *)playerB status:(NSString *)status matchId:(id)matchId teamId:(NSString *)teamId;{
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    if(playerA){
        [dic setObject:playerA forKey:@"playerA"];
    }
   
    if(playerB){
        [dic setObject:playerB forKey:@"playerB"];
    }
    [dic setObject:matchId forKey:@"matchId"];
    [dic setObject:status forKey:@"status"];
    [dic setObject:teamId forKey:@"teamId"];

    return dic;

}

+(void)matchActionRequest:(NSDictionary *)dic  success:(MutableBlock)sucessd failure:(MutableBlock)failured{
    
    
    [AccountHelper shareAccount].isShow=YES;


    [HttpRequestHelper postWithURL:Url(@"game/submit") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
        [XHToast showBottomWithText:@"服务器异常"];
        
    }];


}

+(void)MatchInfo:(NSDictionary *)dic  success:(MutableBlock)sucessd failure:(MutableBlock)failured{
    
   
    

    [HttpRequestHelper postWithURL:Url(@"game/info") params:dic success:^(BOOL sucess, id responseObject) {
        
       
        
        if (sucess) {
            
            [MatchHelper shareMatch].match=nil;

            MatchHelper *match = [[MatchHelper alloc] initWithDic:responseObject];
            
            [MatchHelper shareMatch].match=match;

            sucessd(responseObject);
  
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
         [XHToast showBottomWithText:@"服务器异常"];
        
    }];


}
@end
