//
//  MatchModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface MatchModel : BaseModel



@property(strong,nonatomic)NSString *id;//比赛id
@property(strong,nonatomic)NSString *lid;//赛事id
@property(strong,nonatomic)id dating;//应约球队列表
@property(strong,nonatomic)NSString *datings;//应约球队数量
@property(strong,nonatomic)NSString *leagueName;//赛事名称
@property(strong,nonatomic)id home;//主队
@property(strong,nonatomic)id visiting;//客队
@property(strong,nonatomic)NSString *status;//状态
@property(strong,nonatomic)NSString *venue;//比赛场地名称
@property(strong,nonatomic)NSString *addr;
@property(strong,nonatomic)NSString *power;
@property(strong,nonatomic)NSString *age;
@property(strong,nonatomic)NSString *date;//比赛时间
@property(strong,nonatomic)NSString *type;//赛制，参考Match Type
@property(strong,nonatomic)NSString *desc;//比赛描述
@property(strong,nonatomic)NSString *isrecord;//是否有专业记分员 0:无 1:有
@property(strong,nonatomic)NSString *homerecord;//主队记分员id
@property(strong,nonatomic)NSString *visitingrecord;//客队记分员id
@property(strong,nonatomic)NSString *homescore;//主队分
@property(strong,nonatomic)NSString *visitingscore;//客队分
@property(strong,nonatomic)NSString *playerType;//比赛类型 单队，匹配
@property(strong,nonatomic)NSString *latitude;
@property(strong,nonatomic)NSString *longitude;
@property(strong,nonatomic)NSString *gauntlet;//所属球对已应约
@property(strong,nonatomic)NSString *enroll;//球员已经报名
@property(assign,nonatomic)int  homePenalty;
@property(assign,nonatomic)int  visitingPenalty;
@property(strong,nonatomic)NSString *isPenalty;
@property(assign,nonatomic)int vistingTotalScore;
@property(assign,nonatomic)int homeTotalScore;
@property(strong,nonatomic)id homeusers;//主队报名人员
@property(strong,nonatomic)id visitingusers;//客队报名人员
@end
