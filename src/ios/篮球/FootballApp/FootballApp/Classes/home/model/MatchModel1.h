//
//  MatchModel1.h
//  FootballApp
//
//  Created by zj on 16/5/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface MatchModel1 : BaseModel
//其实这里我只需要解析id
@property(strong,nonatomic)NSString *age;
@property(strong,nonatomic)NSString *avatar;
@property(strong,nonatomic)NSString *id;
@property(strong,nonatomic)NSString *isLeader;

@property(strong,nonatomic)NSString *name;
@property(strong,nonatomic)NSString *realName;
@property(strong,nonatomic)NSString *number;


//解析主队和客队来获得id，来和本地ID比对后，判断传过去的是哪一个id
+ (NSArray *)parseWithDictionary:(NSDictionary *)dict;
+ (NSArray *)parseWithDictionaryByhome:(NSDictionary *)dict;
@end
