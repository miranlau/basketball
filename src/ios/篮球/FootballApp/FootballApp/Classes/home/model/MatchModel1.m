//
//  MatchModel1.m
//  FootballApp
//
//  Created by zj on 16/5/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MatchModel1.h"

@implementation MatchModel1
+ (NSArray *)parseWithDictionary:(NSDictionary *)dict
{
    
    NSArray *results = [dict objectForKey:@"visitingusers"];
    NSMutableArray *businesses = [NSMutableArray array];
    [results enumerateObjectsUsingBlock:^(NSDictionary *dicts, NSUInteger idx, BOOL *stop) {
        MatchModel1 *modle         = [[MatchModel1 alloc]init];
        modle.id = [dicts objectForKey:@"id"];
        [businesses addObject:modle];
    }];
    return businesses;
}

+ (NSArray *)parseWithDictionaryByhome:(NSDictionary *)dict
{
    
    NSArray *results = [dict objectForKey:@"homeusers"];
    NSMutableArray *businesses = [NSMutableArray array];
    [results enumerateObjectsUsingBlock:^(NSDictionary *dicts, NSUInteger idx, BOOL *stop) {
        MatchModel1 *modle         = [[MatchModel1 alloc]init];
        modle.id = [dicts objectForKey:@"id"];
        [businesses addObject:modle];
    }];
    return businesses;
}
@end
