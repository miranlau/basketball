//
//  shareModle.h
//  FootballApp
//
//  Created by zj on 16/7/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface shareModle : BaseModel

@property(strong,nonatomic)NSString *icon;
@property(strong,nonatomic)NSString *title;
@property(nonatomic,strong)NSString *description;
@property(strong,nonatomic)NSString *url;

@end
