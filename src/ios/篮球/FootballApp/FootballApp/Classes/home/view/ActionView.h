//
//  ActionView.h
//  FootballApp
//
//  Created by North on 16/8/4.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionView : UIView
{
    
    MutableBlock clickBlock;
    __weak IBOutlet UIView *centerView;
    
    __weak IBOutlet UIButton *yesBtn;
    
    __weak IBOutlet UIButton *noBtn;
}
-(id)initWithBlock:(MutableBlock)block;
-(void)show;
-(void)hide;
@end
