//
//  ActionView.m
//  FootballApp
//
//  Created by North on 16/8/4.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ActionView.h"

@implementation ActionView

-(id)initWithBlock:(MutableBlock)block{

    self =[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        clickBlock=block;
       
        ViewRadius(centerView, 6.0);
        
        ViewRadius(yesBtn, 3.0);
        
        ViewRadius(noBtn, 3.0);
        [self  layoutIfNeeded];
    }
    return self;

}
- (IBAction)closeBtnClick:(id)sender {
    
    [self hide];
    
}
- (IBAction)btnClick:(UIButton *)sender {
    
    
    if (sender==yesBtn) {
        clickBlock(@"1000");
    }else{
     clickBlock(@"2000");
    }
    
    
    
    [self hide];
}

-(void)show{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setPopAnimation];
}
-(void)hide{
    
    
    [self removeFromSuperview];
    
}
- (void)setPopAnimation {
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:
                                      kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [centerView.layer addAnimation:popAnimation forKey:nil];
    
    
}
@end
