//
//  AddWatchView.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddWatchView : UIView
{

    MutableBlock mutableBlock;
    
    __weak IBOutlet NSLayoutConstraint *buttom;
     __weak IBOutlet UIView *contentView;
    
    __weak IBOutlet UIView *cancelView;

}
-(id)initWithBlock:(MutableBlock)block;

-(void)show;
-(void)hide;
@end
