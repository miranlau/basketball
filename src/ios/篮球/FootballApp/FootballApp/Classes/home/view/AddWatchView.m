//
//  AddWatchView.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AddWatchView.h"

@implementation AddWatchView


#define SHOW 0
#define HIDE 158



-(id)initWithBlock:(MutableBlock)block{
    
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        mutableBlock=block;
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        contentView.backgroundColor=[COLOR colorWithHexString:@"3a3a3a"];
        
        UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
        [cancelView addGestureRecognizer:tap];
        buttom.constant=-HIDE;
        [self layoutIfNeeded];
    }
    
    
    return self;
    
    
}
-(IBAction)btnClick:(UIButton *)sender{
    
    if (sender.tag==1001) {
        mutableBlock(@"1");

    }else{
        mutableBlock(@"2");
    
    }

    [self hide];

}



-(void)closeBtnClick:(id)sender{
    
    [self hide];
}
-(void)show{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
        buttom.constant=SHOW;
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        
        
    }];
}
-(void)hide{
    [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
        buttom.constant=-HIDE;
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
@end
