//
//  ChangeView.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/4.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeView : UIView
{
    MutableBlock clickBlock;
    __weak IBOutlet UIView *centerView;
    __weak IBOutlet UIButton *closeBtn;
}

-(id)initWithBlock:(MutableBlock)block;
-(void)show;
-(void)hide;
@end
