//
//  ChartCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/20.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "PiechartDetchView.h"
#import "PiechartModel.h"
#import "BSNumbersView.h"
#import "MenberNumModel.h"

@interface ChartCell : BaseCell
@property (weak, nonatomic) IBOutlet UILabel *value1;//左侧百分比
@property (weak, nonatomic) IBOutlet UILabel *value2;//右侧百分比
@property (weak, nonatomic) IBOutlet UILabel *name;//中间名字
@property(strong,nonatomic) PiechartDetchView *chartView;
@property (strong, nonatomic) BSNumbersView *numbersView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (weak, nonatomic) IBOutlet UIView *bgView;


@property (weak, nonatomic) IBOutlet UIImageView *centerImageView;
@property (weak, nonatomic) IBOutlet UILabel *startMatch;
@property (weak, nonatomic) IBOutlet UILabel *endMatch;
@property (weak, nonatomic) IBOutlet UILabel *leftName;
@property (weak, nonatomic) IBOutlet UILabel *rightName;

@property (weak, nonatomic) IBOutlet UIImageView *leftcard3;
@property (weak, nonatomic) IBOutlet UIImageView *leftcard2;
@property (weak, nonatomic) IBOutlet UIImageView *leftcard1;

@property (weak, nonatomic) IBOutlet UIImageView *rightcard1;
@property (weak, nonatomic) IBOutlet UIImageView *rightcard2;
@property (weak, nonatomic) IBOutlet UIImageView *rightcard3;
@property (weak, nonatomic) IBOutlet UIImageView *leftTop;
@property (weak, nonatomic) IBOutlet UIImageView *leftBot;
@property (weak, nonatomic) IBOutlet UILabel *leftLable1;
@property (weak, nonatomic) IBOutlet UILabel *leftLable2;
@property (weak, nonatomic) IBOutlet UIImageView *rigthTop;
@property (weak, nonatomic) IBOutlet UIImageView *rigthBot;
@property (weak, nonatomic) IBOutlet UILabel *rigthLable1;
@property (weak, nonatomic) IBOutlet UILabel *rigthLable2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bouttom;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet UIView *line2;
@property (weak, nonatomic) IBOutlet UIImageView *leftHeadImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;

@end
