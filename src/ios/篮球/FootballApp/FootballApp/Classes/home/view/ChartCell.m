//
//  ChartCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/20.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChartCell.h"

@implementation ChartCell



-(void)getdata:(id)data{
    BaseCellModel *model=(BaseCellModel*)data;
    
    if ([model.reuseIdentifier isEqualToString:@"ChartCell1"]) {
        [self setChartView:model.value];
    }
    if ([model.reuseIdentifier isEqualToString:@"ChartCell3"]) {
        
        [self setExcelView:model.value];
       
    }

    if ([model.reuseIdentifier isEqualToString:@"ChartCell5"]) {
        
        [self timeLine:model.value];
        
    }

}

-(void)setChartView:(id)value{
    
    //防止重复
    for (UIView *view in self.contentView.subviews) {
        if ([view isKindOfClass:[PiechartDetchView class]]) {
            [view removeFromSuperview];
        }
    }

    PiechartModel *model1 = [[PiechartModel alloc]init];
    model1.color = [UIColor greenColor];
    model1.perStr = @"0.3";
    
    PiechartModel *model2 = [[PiechartModel alloc]init];
    model2.color = [UIColor brownColor];
    model2.perStr = @"0.7";
    
    
    
    NSArray *testArray = @[model1,model2];
    
    _chartView = [[PiechartDetchView alloc]initWithFrame:CGRectMake(kScreenWidth/2-40, 15,80, 80) withStrokeWidth:15 andColor:[UIColor redColor] andPerArray:testArray  andAnimation:NO];
    [self.contentView addSubview:_chartView];

}
-(void)setExcelView:(id)value{
    
    
    
    //防止重复
    for (UIView *view in self.bgView.subviews) {
        if ([view isKindOfClass:[BSNumbersView class]]) {
            [view removeFromSuperview];
        }
    }

    
    
    NSMutableArray *array=[NSMutableArray array];
    
    
    
    for (int i=0; i<4; i++) {
        
        MenberNumModel *model=[[MenberNumModel alloc]init];
        
        model.name=[NSString stringWithFormat:@"球员%d",i];
        model.type1=@"1";
        model.type2=@"2";
        model.type3=@"3";
        model.type4=@"4";
        model.type5=@"3";
        model.type6=@"2";
        [array addObject:model];
        
    }
    
    self.height.constant=20*(array.count+1);
    [self layoutIfNeeded];
    
    self.numbersView=[[BSNumbersView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 20*(array.count+1))];
    
    
    [self.bgView addSubview:self.numbersView];
    
    
    self.numbersView.bodyData = array;
    self.numbersView.headerTextColor=[UIColor whiteColor];
    self.numbersView.headerBackgroundColor=[UIColor clearColor];
    self.numbersView.bodyTextColor=[UIColor whiteColor];
    self.numbersView.bodyBackgroundColor=[UIColor clearColor];
    
    self.numbersView.freezeBodyTextColor=[UIColor clearColor];
    self.numbersView.freezeBodyTextColor=[UIColor whiteColor];
    
    
    
    self.numbersView.headerData = @[@"球员", @"上场时间", @"进球", @"射正/射门", @"抢断/拦截/解围", @"扑救", @"犯规"];
    self.numbersView.freezeColumn = 1;
    self.numbersView.itemHeight=20;
//    self.numbersView.maxItemWidth=150;
    
    
    
    self.numbersView.bodyFont = [UIFont systemFontOfSize:12];
    self.numbersView.headerFont=[UIFont systemFontOfSize:12];
    self.numbersView.freezeBodyFont=[UIFont systemFontOfSize:12];
    
    
    
    [self.numbersView reloadData];
    
   



}
-(void)timeLine:(id)value{
    
    
    for (UIView *view in self.contentView.subviews) {
        view.hidden=YES;
    }
    
    _centerImageView.hidden=NO;
    
    _top.constant=5;
    _bouttom.constant=5;
    _line1.hidden=NO;
    _line2.hidden=NO;
    self.contentView.backgroundColor=BGCOLOR1;
    //开始比赛
    
    if ([value isEqualToString:@"begin"]) {
        _centerImageView.image=[UIImage imageNamed:@"比赛开始.png"];
        _endMatch.text=@"";
        _startMatch.hidden=NO;
        _startMatch.text=@"开始比赛";
        _top.constant=29;
        _line1.hidden=YES;
       
    }
    //进球
    
    if ([value isEqualToString:@"goal"]) {
        _centerImageView.image=[UIImage imageNamed:@"进球.png"];
        _endMatch.text=@"";
        _startMatch.text=@"";
        
        //主对进球
        _leftName.hidden=NO;
        _rightName.hidden=NO;
        
          _leftName.text=@"宋伟力";
          _rightName.text=@"3'3''";
        _leftHeadImageView.hidden=NO;

        //客队进球
  
    }
    //红黄牌
    if ([value isEqualToString:@"card"]) {
        _centerImageView.image=[UIImage imageNamed:@"红黄牌.png"];
        _endMatch.text=@"";
        _startMatch.text=@"";
        
        //主对换人
        _leftName.hidden=NO;
        _rightName.hidden=NO;
        
        _leftName.text=@"宋伟力";
        _rightName.text=@"3'3''";
        _leftHeadImageView.hidden=NO;
        
        _leftcard1.hidden=NO;
        _leftcard2.hidden=NO;
        _leftcard3.hidden=NO;
        
        //客队换人
    }
    
    //换人
    if ([value isEqualToString:@"change"]) {
        _centerImageView.image=[UIImage imageNamed:@"换人.png"];
        _endMatch.text=@"";
        _startMatch.text=@"";
        
        //主对换人
        _leftTop.hidden=NO;
        _leftBot.hidden=NO;
        _leftLable1.hidden=NO;
        _leftLable2.hidden=NO;
        _leftLable1.text=@"宋伟力";
        _leftLable2.text=@"狗不理";

        //客队换人
    }
    
    
    
    
    //结束比赛
    if ([value isEqualToString:@"end"]) {
        _centerImageView.image=[UIImage imageNamed:@"比赛开始.png"];
        _endMatch.text=@"结束比赛";
        _startMatch.text=@"";
        _endMatch.hidden=NO;
        _bouttom.constant=29;
        _line2.hidden=YES;
    }


   

}


@end
