//
//  EndView.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/27.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EndView : UIView
{

    __weak IBOutlet UIView *centerView;
    
    MutableBlock clickBlock;
    NSArray *actionArr;
    __weak IBOutlet UIButton *butn1;
    __weak IBOutlet UIButton *butn2;


}
-(id)initWithAddTime:(BOOL)isAdd block:(MutableBlock)block;
-(void)setHideTag:(int)tag;


-(void)show;
-(void)hide;
@end
