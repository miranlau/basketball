//
//  EndView.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/27.
//  Copyright © 2016年 North. All rights reserved.
//

#import "EndView.h"

@implementation EndView
-(id)initWithAddTime:(BOOL)isAdd block:(MutableBlock)block;{

    self =[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        clickBlock=block;
        //有加时赛
        if (isAdd) {
            actionArr =@[@"本节结束",@"加时赛"];
        }else{
        
            actionArr =@[@"本节结束",@"全场结束"];
        }
        
        
        
        for (UIView *view in  centerView.subviews) {
            view.layer.cornerRadius=5;
            view.layer.masksToBounds=YES;
        }
        
        
        centerView.layer.cornerRadius=5;
        centerView.layer.masksToBounds=YES;
        [self  layoutIfNeeded];
        
        
        int height=40;
        int width=150;
        int speace=(200-[actionArr count]*height)/([actionArr count]+1);
        
        for (int i=0; i<[actionArr count]; i++) {
            
            UIButton *button =[[UIButton alloc]initWithFrame:CGRectMake((240-width)/2, i*(height + speace)+speace, width, height)];
            
            [button setTitle:actionArr[i] forState:0];
            button.tag=i+1000;
            button.titleLabel.font=[UIFont systemFontOfSize:14];
            [button setTitleColor:[UIColor whiteColor] forState:0];
            button.backgroundColor=ACCOLOR1;
            ViewRadius(button, 5.0);
            [button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [centerView addSubview:button];
            
            
        }
        
        
        
    }
    
    return self;




}
- (IBAction)closeBtnClick:(id)sender {
    
    [self hide];
    
}
- (IBAction)btnClick:(UIButton *)sender {
    [self hide];
    clickBlock(sender.currentTitle);
    
    
    
}

-(void)setHideTag:(int)tag{

    if (tag==1001) {
        butn1.hidden=YES;
    }
    
    if (tag==1002) {
        butn2.hidden=YES;
    }


}

-(void)show{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    
    [self sendSubviewToBack:centerView];
    
    [self setPopAnimation];
    
}
-(void)hide{
    
    
    [self removeFromSuperview];
    
}
- (void)setPopAnimation {
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:
                                      kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [centerView.layer addAnimation:popAnimation forKey:nil];
    
    
}
@end
