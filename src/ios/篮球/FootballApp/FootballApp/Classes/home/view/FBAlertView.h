//
//  FBAlertView.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBAlertView : UIView
{

    MutableBlock clickBlock;
    __weak IBOutlet UIView *centerView;

    __weak IBOutlet UILabel *titleLable;
    __weak IBOutlet UIButton *yesBtn;
}
-(id)initWithTitle:(NSString *)title butnTitlt:(NSString *)butnt block:(MutableBlock)block;
-(void)show;
-(void)hide;
@end
