//
//  FBAlertView.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "FBAlertView.h"

@implementation FBAlertView

-(id)initWithTitle:(NSString *)title butnTitlt:(NSString *)butnt block:(MutableBlock)block{
    
    self =[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        clickBlock=block;
        titleLable.text=title;
        [yesBtn setTitle:butnt forState:0];
        yesBtn.layer.cornerRadius=3;
        yesBtn.layer.masksToBounds=YES;
        [self  layoutIfNeeded];
    }
    return self;
}
- (IBAction)closeBtnClick:(id)sender {
    
    [self hide];
    
}
- (IBAction)btnClick:(UIButton *)sender {
    
    clickBlock(@"");
    [self hide];
}

-(void)show{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setPopAnimation];
}
-(void)hide{
    
    
    [self removeFromSuperview];
    
}
- (void)setPopAnimation {
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:
                                      kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [centerView.layer addAnimation:popAnimation forKey:nil];
    
    
}

@end
