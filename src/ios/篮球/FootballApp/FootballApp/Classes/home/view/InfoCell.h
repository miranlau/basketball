//
//  InfoCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

@interface InfoCell : BaseCell

@property(weak,nonatomic)IBOutlet UILabel *power;
@property(weak,nonatomic)IBOutlet UILabel *age;
@property(weak,nonatomic)IBOutlet UILabel *deac;
@end
