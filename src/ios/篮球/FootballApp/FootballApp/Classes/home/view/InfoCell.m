//
//  InfoCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "InfoCell.h"

@implementation InfoCell

-(void)getdata:(id)data{

    BaseCellModel *model=data;
    
    if ([model.reuseIdentifier isEqualToString:@"InfoCell1"]) {
        if (!model.key) {
            model.key=@"";
        }
        if (!model.value) {
            model.value=@"";
        }
        _power.text=[NSString stringWithFormat:@"对手战斗力 %@",model.key];
         _age.text=[NSString stringWithFormat:@"对手年龄 %@",model.value];
    }
    
    if ([model.reuseIdentifier isEqualToString:@"InfoCell2"]) {
        if (!model.value) {
            model.value=@"";
        }
        _deac.text=[NSString stringWithFormat:@"%@",model.value];
    }
    

}

@end
