//
//  LeiDaCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "LeiDaCell.h"
#import "TeamModel.h"


@implementation LeiDaCell

-(void)awakeFromNib{
    
    self.contentView.backgroundColor=BGCOLOR1;

}
-(void)getdata:(id)data{
    
     BaseCellModel *model=data;
    
    
    if ([self.reuseIdentifier isEqualToString:@"Player"]) {
       PlayerModel *player=model.value;
        
        scoreCount.text=[NSString stringWithFormat:@"%@",player.scoreCount];
        avgBackboard.text=[NSString stringWithFormat:@"%@",player.avgBackboard];
        avgHelp.text=[NSString stringWithFormat:@"%@",player.avgHelp];
        avgAnError.text=[NSString stringWithFormat:@"%@",player.avgAnError];
        avgSteals.text=[NSString stringWithFormat:@"%@",player.avgSteals];
        avgCover.text=[NSString stringWithFormat:@"%@",player.avgCover];
    }
    
    if ([self.reuseIdentifier isEqualToString:@"Team"]) {
        TeamModel *team=model.value;
        
        scoreCount.text=[NSString stringWithFormat:@"%@",team.countScore];
        avgBackboard.text=[NSString stringWithFormat:@"%@",team.avgBackboard];
        avgHelp.text=[NSString stringWithFormat:@"%@",team.avgHelp];
        avgAnError.text=[NSString stringWithFormat:@"%@",team.avgAnError];
        avgSteals.text=[NSString stringWithFormat:@"%@",team.avgSteals];
        avgCover.text=[NSString stringWithFormat:@"%@",team.avgCover];
        
    }
    
    
    
  

}
@end
