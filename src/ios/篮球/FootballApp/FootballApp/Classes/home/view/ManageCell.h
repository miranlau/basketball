//
//  ManageCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "TeamModel.h"
@interface ManageCell : BaseCell

@property (weak, nonatomic) IBOutlet UIImageView *teamLogo;
@property (weak, nonatomic) IBOutlet UILabel *teamName;
@property (weak, nonatomic) IBOutlet UILabel *cellName;

@end
