//
//  ManageCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ManageCell.h"

@implementation ManageCell

-(void)getdata:(id)data{
    
    
    BaseCellModel *model=data;
    
    if ([model.reuseIdentifier isEqualToString:@"ManageCell1"]) {
        
        TeamModel *team=model.value;
        
        
        [_teamLogo setImageWithURL:[NSURL URLWithString:team.logo] placeholderImage:TeamLogo];
        
        [_teamLogo addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
        
        _teamLogo.layer.cornerRadius=35;
        _teamLogo.layer.masksToBounds=YES;
        
        _teamName.text=team.name;
       
        
        
    }
    
    
    if ([model.reuseIdentifier isEqualToString:@"ManageCell2"]) {
        
        self.cellName.text=model.name;
        
    }
    




}
-(void)headImageClick:(id)sender{
    
    self.cellBlock(@"head");//修改头像
    
}
@end
