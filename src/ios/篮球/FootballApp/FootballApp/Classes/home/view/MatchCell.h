//
//  MatchCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

@interface MatchCell : BaseCell
@property(weak,nonatomic)IBOutlet UIImageView *leftImageView;
@property(weak,nonatomic)IBOutlet UIImageView *rightImageView;
@property (weak, nonatomic) IBOutlet UILabel *leftTeamName;
@property (weak, nonatomic) IBOutlet UILabel *rightteamName;
@property (weak, nonatomic) IBOutlet UILabel *timeAndScore;//时间或者比分
@property (weak, nonatomic) IBOutlet UILabel *typeAndcup;//赛制或者赛事
@property (weak, nonatomic) IBOutlet UILabel *address;//地点

@property (weak, nonatomic) IBOutlet UIImageView *win1;
@property (weak, nonatomic) IBOutlet UIImageView *win2;
@property (weak, nonatomic) IBOutlet UILabel *teamCount;

@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;

@property (weak, nonatomic) IBOutlet UIButton *joined;
@property (weak, nonatomic) IBOutlet UILabel *matchStuat;

@property (weak, nonatomic) IBOutlet UILabel *leftPower;
@property (weak, nonatomic) IBOutlet UILabel *leftAge;

@property (weak, nonatomic) IBOutlet UILabel *rightPower;
@property (weak, nonatomic) IBOutlet UILabel *rightAge;
@property (weak, nonatomic) IBOutlet UILabel *dian;


@end
