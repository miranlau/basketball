//
//  MatchCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MatchCell.h"
#import "MatchModel.h"
#import "TeamModel.h"
#import "DataFormatHelper.h"
@implementation MatchCell

-(void)getdata:(id)data{
    
    [_leftImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftHeadClick:)]];
    
    _leftImageView.layer.cornerRadius=60/2.0;
    _leftImageView.layer.masksToBounds=YES;
    
    BaseCellModel *model=data;
    
    
    //赛前
    
    if ([model.reuseIdentifier isEqualToString:@"MatchCell1"]) {
        MatchModel *match=model.value;
         TeamModel *team=[[TeamModel alloc]initWithDic:match.home];
        
        [ _leftImageView setImageWithURL:[NSURL URLWithString:team.logo] placeholderImage:TeamLogo];
        _leftTeamName.text=[NSString stringWithFormat:@"%@",team.name];
        _timeAndScore.text=[DataFormatHelper timeChangeFormart:@"HH : mm" times:match.date];
        _timeAndScore.textColor=[UIColor orangeColor];
        //单队赛
        if ([match.playerType intValue]==0) {
            
            _teamCount.hidden=YES;
            _joined.hidden=YES;
            _type.hidden=NO;
             _address.text=@"";
            
            
            
            
        _typeAndcup.text=[NSString stringWithFormat:@"%@人制",match.type];
            if ([match.playerType intValue]==2) {
                _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
            }
            
            if ([match.lid intValue]!=10000&&[match.lid intValue]!=10100) {
                _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
            }
            
        }else{
            
             _teamCount.text=[NSString stringWithFormat:@"%@队应约",match.datings];
            _teamCount.hidden=NO;
             _joined.hidden=YES;
            if([match.gauntlet intValue]==1){
             _joined.hidden=NO;
            }
           
            _type.hidden=YES;
            _address.text=[NSString stringWithFormat:@"%@",match.addr];
            _typeAndcup.text=[NSString stringWithFormat:@"%@人制",match.type];
            
            if ([match.playerType intValue]==2) {
                 _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
            }
            
            if ([match.lid intValue]!=10000&&[match.lid intValue]!=10100) {
                _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
            }
            
        
        }
        
        
        _width.constant = kScreenWidth-200;
        
        
        
    }
    if ([model.reuseIdentifier isEqualToString:@"MatchCell2"]) {
        
        
        MatchModel *match=model.value;
        TeamModel *team=[[TeamModel alloc]initWithDic:match.home];
        
        [ _leftImageView setImageWithURL:[NSURL URLWithString:team.logo] placeholderImage:TeamLogo];
        _leftTeamName.text=[NSString stringWithFormat:@"%@",team.name];
       _timeAndScore.text=[NSString stringWithFormat:@"%d : %d",match.vistingTotalScore,match.vistingTotalScore];
        
        if([match.isPenalty intValue]==1){
            _timeAndScore.text=[NSString stringWithFormat:@"%d : %d",match.vistingTotalScore,match.vistingTotalScore];
            _dian.hidden=NO;
            
        }else{
            
            _timeAndScore.text=[NSString stringWithFormat:@"%d : %d",match.vistingTotalScore,match.vistingTotalScore];
            _dian.hidden=YES;
            
        }
        if([match.status intValue]==100){
            if (match.homeTotalScore > match.vistingTotalScore) {
                _win1.hidden=NO;
            }
        }else{
            
            _win1.hidden=YES;
        }
        _timeAndScore.font=[UIFont systemFontOfSize:36];
        if ([match.status intValue]==21) {
            _timeAndScore.textColor=[UIColor redColor];
             _timeAndScore.text=@"即将开赛";
             _timeAndScore.font=[UIFont systemFontOfSize:20];
        }else{
            
            if ([match.status intValue]==100) {
                 _timeAndScore.textColor=[UIColor whiteColor];
            }else{
                _timeAndScore.textColor=[UIColor greenColor];
            
            }
            
            
            if ([match.status intValue]==20) {
                _timeAndScore.text=@"比赛中";
                 _timeAndScore.font=[UIFont systemFontOfSize:20];
                _dian.hidden=YES;
            }
            
        }
        
         _typeAndcup.text=@"单队赛";
        
        _win1.hidden=YES;
        
        
       
        
        
    }
    if ([model.reuseIdentifier isEqualToString:@"MatchCell3"]) {
        
        MatchModel *match=model.value;
        TeamModel *team1=[[TeamModel alloc]initWithDic:match.home];
        
        [ _leftImageView setImageWithURL:[NSURL URLWithString:team1.logo] placeholderImage:TeamLogo];
        _leftTeamName.text=[NSString stringWithFormat:@"%@",team1.name];
        
        TeamModel *team2=[[TeamModel alloc]initWithDic:match.visiting];
        [_rightImageView setImageWithURL:[NSURL URLWithString:team2.logo] placeholderImage:TeamLogo];
        _rightteamName.text=[NSString stringWithFormat:@"%@",team2.name];
        _rightImageView.layer.cornerRadius=60/2.0;
        _rightImageView.layer.masksToBounds=YES;
        _typeAndcup.text=[NSString stringWithFormat:@"%@人制",match.type];
        if ([match.playerType intValue]==2) {
            _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
            
        }
        if ([match.lid intValue]!=10000&&[match.lid intValue]!=10100) {
            _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
            if (!match.leagueName) {
               _typeAndcup.text=@"";
            }
            
        }
        
        [_rightImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightHeadClick:)]];
        if ([match.playerType intValue]==2||([match.playerType intValue]!=10000&&[match.lid intValue]!=10100)) {
            _address.text=@"";
            _typeAndcup.numberOfLines=2;
        }else{
            _address.text=[NSString stringWithFormat:@"%@",match.addr];
            _typeAndcup.numberOfLines=1;
        }
        
        
        
        if([match.isPenalty intValue]==1){
         _timeAndScore.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
            _dian.hidden=NO;
            
        
        }else{
        
         _timeAndScore.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
        _dian.hidden=YES;
        }
         _timeAndScore.font=[UIFont systemFontOfSize:36];
        if ([match.status intValue]==21) {
            
            _timeAndScore.text=@"即将开赛";
            
            _timeAndScore.textColor=[UIColor redColor];
             _timeAndScore.font=[UIFont systemFontOfSize:20];
        }else{
            if ([match.status intValue]==100) {
                _timeAndScore.textColor=[UIColor whiteColor];
            }else{
                _timeAndScore.textColor=[UIColor greenColor];
                
            }
            
            if ([match.status intValue]==20) {
                _timeAndScore.text=@"比赛中";
                 _timeAndScore.font=[UIFont systemFontOfSize:20];
                _dian.hidden=YES;
                
            }
        }
       
        _win1.hidden=YES;
        _win2.hidden=YES;
        
        if([match.status intValue]==100){
            
            if (match.homeTotalScore > match.vistingTotalScore) {
                _win1.hidden=NO;
                _win2.hidden=YES;
            }
            
            if (match.homeTotalScore < match.vistingTotalScore) {
                _win2.hidden=NO;
                _win1.hidden=YES;
            }
        }else{
         _win1.hidden=YES;
         _win2.hidden=YES;
        }
        
        _width.constant = kScreenWidth-200;
        
    }
    if ([model.reuseIdentifier isEqualToString:@"MatchCell4"]) {
        
        
        
        MatchModel *match=model.value;
        TeamModel *team1=[[TeamModel alloc]initWithDic:match.home];
        
        [ _leftImageView setImageWithURL:[NSURL URLWithString:team1.logo] placeholderImage:TeamLogo];
        _leftTeamName.text=[NSString stringWithFormat:@"%@",team1.name];
        
         TeamModel *team2=[[TeamModel alloc]initWithDic:match.visiting];
        [_rightImageView setImageWithURL:[NSURL URLWithString:team2.logo] placeholderImage:TeamLogo];
        _rightteamName.text=[NSString stringWithFormat:@"%@",team2.name];
        _rightImageView.layer.cornerRadius=60/2.0;
        _rightImageView.layer.masksToBounds=YES;
          _typeAndcup.text=[NSString stringWithFormat:@"%@人制",match.type];
        if ([match.playerType intValue]==2) {
            _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
        }
        
        if ([match.lid intValue]!=10000&&[match.lid intValue]!=10100) {
            _typeAndcup.text=[NSString stringWithFormat:@"%@",match.leagueName];
        }
        [_rightImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightHeadClick:)]];
        if ([match.playerType intValue]==2||([match.playerType intValue]!=10000&&[match.lid intValue]!=10100)) {
            _address.text=@"";
            _typeAndcup.numberOfLines=2;
        }else{
            _address.text=[NSString stringWithFormat:@"%@",match.addr];
            _typeAndcup.numberOfLines=1;
        }
         _timeAndScore.text=[DataFormatHelper timeChangeFormart:@"HH : mm" times:match.date];
         _timeAndScore.textColor=[UIColor orangeColor];
       _width.constant = kScreenWidth-200;
        
    }
    

    
    if ([model.reuseIdentifier isEqualToString:@"MatchCell5"]) {
        
        
        MatchModel *match=model.value;
        TeamModel *team1=[[TeamModel alloc]initWithDic:match.home];
        
        [ _leftImageView setImageWithURL:[NSURL URLWithString:team1.logo] placeholderImage:TeamLogo];
        _leftImageView.layer.cornerRadius = 74.0/2;
        _leftTeamName.text=[NSString stringWithFormat:@"%@",team1.name];
        [_rightImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightHeadClick:)]];
        //比赛时间
         _timeAndScore.text=[DataFormatHelper timeChangeFormart:@"HH : mm" times:match.date];
        //地点
        _address.text=@"";
        _type.text=[NSString stringWithFormat:@" %@人制 ",match.type];
//        _leftPower.text=[NSString stringWithFormat:@"战斗力:%@",team1.]
        _leftAge.text=[NSString stringWithFormat:@"年龄 : %@",team1.age];
        
        if ([match.status intValue]==10) {
            _matchStuat.text=[NSString stringWithFormat:@" 赛前阶段 "];
            _typeAndcup.text=[NSString stringWithFormat:@"%@",match.date];
        }
        
        if ([match.status intValue]==20||[match.status intValue]==21) {
            _matchStuat.text=[NSString stringWithFormat:@" 赛中阶段 "];
           
            
             _typeAndcup.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
            
            if([match.isPenalty intValue]==1){
                _typeAndcup.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
                _dian.hidden=NO;
            }else{
                
                _typeAndcup.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
                _dian.hidden=YES;
            }
            _typeAndcup.font=[UIFont systemFontOfSize:20];
            
           
            
            
            
        }
        
        _matchStuat.layer.cornerRadius=2;
        _matchStuat.layer.masksToBounds=YES;
        
       
        if ([match.status intValue]!=10&&[match.status intValue]!=20&&[match.status intValue]!=21) {
            _typeAndcup.text=@"服务器错误";
        }
        
        
    }
    
    
    if ([model.reuseIdentifier isEqualToString:@"MatchCell6"]) {
        
        
        MatchModel *match=model.value;
        TeamModel *team1=[[TeamModel alloc]initWithDic:match.home];
        
        [ _leftImageView setImageWithURL:[NSURL URLWithString:team1.logo] placeholderImage:TeamLogo];
        _leftTeamName.text=[NSString stringWithFormat:@"%@",team1.name];
        [_rightImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightHeadClick:)]];
        
//        _leftPower.text=[NSString stringWithFormat:@"战斗力:%@",team1.]
        _leftAge.text=[NSString stringWithFormat:@"年龄:%@",team1.age];
        
        
        TeamModel *team2=[[TeamModel alloc]initWithDic:match.visiting];
        [_rightImageView setImageWithURL:[NSURL URLWithString:team2.logo] placeholderImage:TeamLogo];
        _rightteamName.text=[NSString stringWithFormat:@"%@",team2.name];
        _rightImageView.layer.cornerRadius=74/2.0;
        _leftImageView.layer.cornerRadius=74/2.0;
        _rightImageView.layer.masksToBounds=YES;
        
        
        //        _leftPower.text=[NSString stringWithFormat:@"战斗力:%@",team1.]
        _rightAge.text=[NSString stringWithFormat:@"年龄: %@",team2.age];
        
        //比赛时间
       
        _address.text=[NSString stringWithFormat:@"%@",match.addr];
        _type.text=[NSString stringWithFormat:@" %@人制 ",match.type];

        if ([match.status intValue]==10) {
            _matchStuat.text=[NSString stringWithFormat:@" 赛前阶段 "];
             _typeAndcup.text=[NSString stringWithFormat:@"%@",match.date];
        }else
        
        if ([match.status intValue]==20||[match.status intValue]==21) {
             _matchStuat.text=[NSString stringWithFormat:@" 赛中阶段 "];
             _typeAndcup.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
            
            if([match.isPenalty intValue]==1){
                _typeAndcup.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
                _dian.hidden=NO;
            }else{
                
                _typeAndcup.text=[NSString stringWithFormat:@"%d : %d",match.homeTotalScore,match.vistingTotalScore];
                _dian.hidden=YES;
            }
            _typeAndcup.font=[UIFont systemFontOfSize:20];
        }
        
        _matchStuat.layer.cornerRadius=2;
        _matchStuat.layer.masksToBounds=YES;
        
        if ([match.status intValue]!=10&&[match.status intValue]!=20&&[match.status intValue]!=21) {
            _typeAndcup.text=@"服务器错误";
        }
        
    }
    
  
     _win1.hidden=YES;
     _win2.hidden=YES;
}

-(void)leftHeadClick:(id)sender{
    
    if(self.cellBlock){

    self.cellBlock(@"left");
    }
    

}
-(void)rightHeadClick:(id)sender{
    
    
    if(self.cellBlock){
        
        self.cellBlock(@"right");
    }
    
}
@end
