//
//  MatchInfoCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MatchInfoCell.h"

@implementation MatchInfoCell


-(void)getdata:(id)data{
    
    BaseCellModel *model=(BaseCellModel *)data;
    
    
    if ([model.reuseIdentifier isEqualToString:@"MatchInfoCell"]) {
        self.leftImageView.image=[UIImage imageNamed:model.image];
        self.cellName.text=model.name;
        self.cellInfo.text=model.value;
    }else{
        
        self.leftImageView.image=[UIImage imageNamed:model.image];
        self.cellName.text=model.name;
        self.cellInfo.text=model.value;
        if ([self.cellInfo.text isEqualToString:@"(null)"]) {
            self.cellInfo.text=@"";
        }
        
    
    }
    
   
}

@end
