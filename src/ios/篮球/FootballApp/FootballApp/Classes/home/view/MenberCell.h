//
//  MenberCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "MatchModel1.h"
@interface MenberCell : BaseCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *num;
@property (weak, nonatomic) IBOutlet UIButton *button;



@property (weak, nonatomic) IBOutlet UIImageView *leaderImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heigH;

@end
