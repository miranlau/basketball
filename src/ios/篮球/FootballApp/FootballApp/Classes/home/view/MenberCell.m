//
//  MenberCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/19.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MenberCell.h"

@implementation MenberCell
-(void)getdata:(id)data{

     PlayerModel *model =[[PlayerModel alloc]initWithDic:data];

    [_headImageView setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:HeadImage];

    _headImageView.layer.cornerRadius=35;
    _headImageView.layer.masksToBounds=YES;
    
    _name.text=model.name;
    _num.text=[NSString stringWithFormat:@"球衣号码:%@",model.number];
    
    if ([model.number intValue]==100) {
        _num.textColor=[UIColor redColor];
    }else{
    _num.textColor=[UIColor whiteColor];
    }

//     if([_leader intValue]==[model.id intValue])
//     {
////         
//         UIImage *image=[UIImage imageNamed:@"leader.png"];
//         UIImageView *leader=[[UIImageView alloc]initWithImage:image];
//         leader.frame=CGRectMake(0, 0, image.size.width, image.size.height);
//    
//         [_headImageView addSubview:leader];
//     }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
    self.widthC.constant = 20;
    self.heigH.constant  = 35;
    // Configure the view for the selected state
}

@end
