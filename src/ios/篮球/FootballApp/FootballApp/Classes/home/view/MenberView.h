//
//  MenberView.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/29.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerView.h"
@interface MenberView : UIView<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
{
    
    UITableView *_tableView;
    MutableBlock rowBlock;
    PlayerView *playerView;
    UILabel *typeLabel;
    UIButton *done;
    UIPanGestureRecognizer *pan;
    MutableBlock doneBlock;
    MutableBlock  panBlock;
    MutableBlock  cancelBlock;
    NSIndexPath * _indexPath;
    
    CGPoint *vcenter;
    UIButton *cancelBtn;
    NSIndexPath *_indePath;
    
    BOOL isPaned;//是否拖拽

    UILongPressGestureRecognizer *longPress;
}
@property(copy,nonatomic)NSMutableArray *list;

@property(retain,nonatomic)NSString *leader;
-(id)initWithFrame:(CGRect)frame  isgest:(BOOL)isgest rowBlock:(MutableBlock)block panBlock:(MutableBlock)pblock;

-(void)setTitle:(NSString *)string doneBlock:(MutableBlock)block;

-(void)cancelBlock:(MutableBlock)block;

@end

#import "BaseCell.h"
@interface MenberViewCell : BaseCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *selectImage;
@property (weak, nonatomic) IBOutlet UILabel *num;
@property (weak, nonatomic) IBOutlet UIImageView *leaderImage;
@property(retain,nonatomic)NSString *leader;
@end

@interface BaseCode : NSObject

@property(copy,nonatomic)NSString* title;
@property(assign) NSInteger tagType;

+(NSArray *)getBtnList1;
+(NSArray *)getBtnList2;
+(NSArray *)getBtnList3;
+(NSArray *)getBtnList4;
@end