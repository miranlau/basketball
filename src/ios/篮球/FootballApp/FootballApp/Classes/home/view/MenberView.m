//
//  MenberView.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/29.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MenberView.h"

#import "PlayerModel.h"
@implementation MenberView


-(id)initWithFrame:(CGRect)frame  isgest:(BOOL)isgest rowBlock:(MutableBlock)block panBlock:(MutableBlock)pblock;
{
    
    self =[super initWithFrame:frame];
    
    if (self) {
        isPaned=isgest;
        rowBlock=block;
        panBlock=pblock;
        
        typeLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 150, 30)];
        
        typeLabel.text=@"换人";
        typeLabel.textColor=[UIColor whiteColor];
        typeLabel.font=[UIFont systemFontOfSize:14];
        [self addSubview:typeLabel];
        
        done =[[UIButton alloc]initWithFrame:CGRectMake(frame.size.width-50, 0, 50, 30)];
        [done setTitle:@"完成" forState:0];
        done.titleLabel.font=[UIFont systemFontOfSize:14];
        [done setTitleColor:[UIColor whiteColor] forState:0];
        [done addTarget:self action:@selector(doneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:done];
        
        _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 30, frame.size.width, frame.size.height-90)];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.rowHeight=UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight=100;
        _tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
        [_tableView registerNib:[UINib nibWithNibName:@"MenberViewCell" bundle:nil]forCellReuseIdentifier:@"MenberViewCell"];
        
        _tableView.backgroundColor=[UIColor clearColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        
        //是否需要手势
        if (isgest) {
            
            
            pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(longAndPan:)];
            pan.delegate=self;
            
           
            
            [_tableView addGestureRecognizer:pan];
        }
        
        [_tableView reloadData];
        [self addSubview:_tableView];
        
        cancelBtn =[[UIButton alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(_tableView.frame)+10, frame.size.width-40, 40)];
        cancelBtn.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
        [cancelBtn setTitle:@"确定" forState:0];
        cancelBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        [cancelBtn setTitleColor:[UIColor whiteColor] forState:0];
        [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        cancelBtn.layer.cornerRadius=5;
        cancelBtn.layer.masksToBounds=YES;
        
        
        [self addSubview:cancelBtn];
        
    }
    return self;
}

-(void)setTitle:(NSString *)string doneBlock:(MutableBlock)block{
    
    typeLabel.text=string;
    
    cancelBlock=block;
    [done setTitle:@"取消" forState:0];
    
    if ([string isEqualToString:@"换人"]) {
        [cancelBtn setTitle:@"完成" forState:0];
    }else{
        
        [cancelBtn setTitle:@"确定" forState:0];
    }
    
}
-(void)cancelBtnClick:(id)sender{
    
    if (cancelBlock) {
        
        cancelBlock(@"");
        
    }
    
    
}
-(void)cancelBlock:(MutableBlock)block{
    
    doneBlock=block;
}
-(void)doneBtnClick:(id)sender{
    
    if (doneBlock) {
        
        doneBlock(@"");
        
    }
    
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return [_list count];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MenberViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MenberViewCell" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=indexPath.row%2==0?BGCOLOR1:BGCOLOR2;
    
    [cell setValue:_leader forKey:@"leader"];
       PlayerModel *model=self.list[indexPath.row];
    [cell setValue:model forKey:@"data"];
    
    if (_indePath==indexPath) {
        
        cell.selectImage.hidden=NO;
    }else{
    
        cell.selectImage.hidden=YES;
    
    }
    
    
    
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    rowBlock(self.list[indexPath.row]);
    
    if (isPaned==NO) {
        _indePath=indexPath;
        
        [tableView reloadData];
    }
    
}
-(void)longAndPan:(UIGestureRecognizer *)gest{
    
    
    
    
    
     if([gest isKindOfClass:[UIPanGestureRecognizer class]]) {
        
        CGPoint point = [gest locationInView:_tableView.superview];//在父视图中的位置
        
         CGPoint point2 = [gest locationInView:_tableView];//在tableView中的位置
        
        if(gest.state==UIGestureRecognizerStateBegan){
            
            _indexPath = [_tableView indexPathForRowAtPoint:point2];
            
            if (!_indexPath|| _indexPath.row+1 >[_list count]) {
                
                return;
            }
            
            playerView=[[PlayerView  alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
            
            PlayerModel *model=self.list[_indexPath.row];
            
            playerView.play=model;
            
            playerView.center=point;
            
            [self.superview addSubview:playerView];
            
        }else  if(gest.state==UIGestureRecognizerStateChanged){
            
            playerView.center=point;
            
        }else if(gest.state==UIGestureRecognizerStateEnded){
            
            if (playerView) {
                
                //如果中心点移除了那个tableView
                if (playerView.center.x<0) {
                    //删除选中的行
                    
                    [self.list removeObject:self.list[_indexPath.row]];
                    
                    [_tableView beginUpdates];
                    [_tableView deleteRowsAtIndexPaths:@[_indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    [_tableView endUpdates];

                    
                    panBlock(playerView);
                    playerView=nil;
                    _indexPath=nil;
                   
                    
                    
                }else{

                    [playerView removeFromSuperview];
                    playerView=nil;
                    _indexPath=nil;
                    
                }
  
            }
  
            
        }
 
    }

    
    
    
}
@synthesize list=_list;
-(void)setList:(NSMutableArray *)list{
    _list=list;
    
    [_tableView reloadData];
    
    
}
@end

@implementation MenberViewCell

-(void)getdata:(id)data{
    PlayerModel *play=data;
    
    
    [self.headImageView setImageWithURL:[NSURL URLWithString:play.avatar] placeholderImage:HeadImage];
    self.headImageView.layer.cornerRadius=19;
    self.headImageView.layer.masksToBounds=YES;
    self.num.text=[NSString stringWithFormat:@"%@",play.number];
    if ([play.number intValue]==100) {
        self.num.textColor=[UIColor redColor];
    }
    
    
    NSString *name=[NSString stringWithFormat:@"%@",play.realName];
    
    
    if (!name || [name isEqualToString:@"(null)"]) {
          name=[NSString stringWithFormat:@"%@",play.name];
    }
    
    self.name.text=name;
    
    if ([self.leader intValue]==[play.id intValue]) {
        
        _leaderImage.hidden=NO;
    }else{
        _leaderImage.hidden=YES;
    
    }
    
}



@end

@implementation BaseCode
+(NSArray *)getBtnList1{
    
    
    
    return @[[self name:@"确定"tag:1000],[self name:@"取消"tag:1001]];
    
    
}
+(NSArray *)getBtnList2{
    
    return @[[self name:@"进球"tag:1005],[self name:@"射正未进"tag:1002],[self name:@"射偏"tag:1003],[self name:@"乌龙球"tag:1004],[self name:@"点球"tag:1007],[self name:@"取消"tag:1001]];
}
+(NSArray *)getBtnList3{
    
    return @[[self name:@"助攻"tag:1000],[self name:@"无助攻"tag:1006]];
}


+(NSArray *)getBtnList4{
    
    return @[[self name:@"罚进"tag:1008],[self name:@"罚丢"tag:1009]];
}

+(BaseCode *)name:(NSString *)name tag:(NSInteger)tag{
    BaseCode *code=[[BaseCode alloc]init];
    
    code.title=name;
    code.tagType=tag;
    
    return code;
    
    
}
@end
