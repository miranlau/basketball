//
//  PlayerView.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerModel.h"
@interface PlayerView : UIView
{

    UIImageView *headImageView;
    UIImageView *selectImage;
    UIImageView *numImage;
    UILabel *numLabel;
    UIButton *deleteBtn;
    
    MutableBlock  deleteBlock;
    MutableBlock selectBlock;

}
@property(assign,nonatomic)BOOL isSelect;//是否允许选择
@property(assign,nonatomic)BOOL isSelected;//是否已经选择
@property(copy,nonatomic)NSString *imageUrl;//头像url
@property(copy,nonatomic)NSString *num;//头像url
@property(strong,nonatomic)PlayerModel *play;//头像url

-(void)deleteBtnBlock:(MutableBlock )bolock;

-(void)selectBtnClick:(MutableBlock )bolock;



@end
