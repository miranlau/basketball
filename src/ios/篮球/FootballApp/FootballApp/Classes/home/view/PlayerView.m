//
//  PlayerView.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "PlayerView.h"

@implementation PlayerView

-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
        headImageView=[[UIImageView alloc]initWithFrame:self.bounds];
        headImageView.layer.cornerRadius=self.bounds.size.width/2.0;
        headImageView.image=[UIImage imageNamed:@"球衣.png"];
        headImageView.layer.masksToBounds=YES;
        [self addSubview:headImageView];
        
//        UIImage *image=[UIImage imageNamed:@"球衣.png"];
//        numImage=[[UIImageView alloc]initWithImage:image];
//        numImage.frame=CGRectMake(self.bounds.size.width-image.size.width,self.bounds.size.width-image.size.height, image.size.width, image.size.height);
//        
//        [self addSubview:numImage];
        
        numLabel=[[UILabel alloc]initWithFrame:self.bounds];
        
        numLabel.textAlignment=1;
        numLabel.textColor=[UIColor whiteColor];
//        numLabel.font=[UIFont systemFontOfSize:16];
       [numLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        
        [self addSubview:numLabel];
        
        UIImage *image2=[UIImage imageNamed:@"选中绿勾.png"];
        selectImage=[[UIImageView alloc]initWithImage:image2];
         selectImage.frame=CGRectMake(self.bounds.size.width-image2.size.width,0, image2.size.width, image2.size.height);
        
        selectImage.hidden=YES;
        [self addSubview:selectImage];
        
        
        
        
        deleteBtn=[[UIButton alloc]initWithFrame:selectImage.frame];
        [deleteBtn setImage:[UIImage imageNamed:@"xx按钮.png"] forState:0];
        deleteBtn.hidden=YES;
        [deleteBtn addTarget:self action:@selector(deleteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:deleteBtn];
        
        
        
        
        self.backgroundColor=[UIColor clearColor];
    }

    return self;

}

-(void)deleteBtnBlock:(MutableBlock )bolock{

    deleteBlock=bolock;

}

-(void)selectBtnClick:(MutableBlock )bolock{
    
    selectBlock=bolock;

}
-(void)setIsSelect:(BOOL)isSelect{


    _isSelect=isSelect;
    
    
    if (_isSelect==YES) {
        
        deleteBtn.hidden=YES;
        self.userInteractionEnabled=YES;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewBtnClick:)]];
    }else{
    
        deleteBtn.hidden=NO;
    }

}
-(void)viewBtnClick:(id)sender
{
    if (!self.isSelect) {
        return;
    }

    self.isSelected=!self.isSelected;
    
    
         
}
-(void)setIsSelected:(BOOL)isSelected{
    
    
    if (!_isSelect) {
        return;
    }

    _isSelected=isSelected;
    
    
    if (_isSelected) {
        
        headImageView.layer.borderColor=[UIColor greenColor].CGColor;
        headImageView.layer.borderWidth=1.0;
        selectImage.hidden=NO;
 
    }else{
    
        headImageView.layer.borderColor=[UIColor clearColor].CGColor;
        headImageView.layer.borderWidth=0;
        selectImage.hidden=YES;
    }
    if (selectBlock) {
        
        selectBlock(@"");
        
    }
    
    


}

-(void)setImageUrl:(NSString *)imageUrl{
    _imageUrl=imageUrl;
    
//    [headImageView setImageWithURL:[NSURL URLWithString:_imageUrl] placeholderImage:HeadImage];


}
-(void)setNum:(NSString *)num{
    _num=num;
    
    numLabel.text=_num;
    
    
    if ([_num intValue]==100) {
        numLabel.textColor=[UIColor redColor];
    }

}
-(void)setPlay:(PlayerModel *)play{

    _play=play;
    self.imageUrl=_play.avatar;
   self.num=[NSString stringWithFormat:@"%@",_play.number];

}
-(void)deleteBtnClick:(id)sender{

    deleteBlock(self.play);

}


@end
