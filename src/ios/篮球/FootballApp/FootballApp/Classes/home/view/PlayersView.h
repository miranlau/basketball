//
//  PlayersView.h
//  FootballApp
//
//  Created by North on 16/6/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerModel.h"
@interface PlayersView : UIView
{

    UIImageView *numImgView;//球衣背景图片
    UILabel *numLabel;//球衣号码

}

@property(strong,nonatomic)PlayerModel *play;
@property(assign,nonatomic) BOOL isSelected;//是否选中


@end
