//
//  PlayersView.m
//  FootballApp
//
//  Created by North on 16/6/30.
//  Copyright © 2016年 North. All rights reserved.
//

#import "PlayersView.h"

@implementation PlayersView
-(id)initWithFrame:(CGRect)frame{

    self=[super initWithFrame:frame];
    
    if (self) {
        self.userInteractionEnabled=YES;//允许交互
        //球衣背景
        numImgView=[[UIImageView alloc]initWithFrame:self.bounds];
        numImgView.image=[UIImage imageNamed:@"球衣.png"];
        [self addSubview:numImgView];
        
        //球衣号码
        numLabel=[[UILabel alloc]initWithFrame:self.bounds];
        numLabel.textAlignment=1;
        numLabel.textColor=[UIColor whiteColor];
        [numLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        [self addSubview:numLabel];
    }
    
    return self;

}
-(void)setPlay:(PlayerModel *)play{
    _play=play;
    numLabel.text=[NSString stringWithFormat:@"%@",_play.number];
    
}
-(void)setIsSelected:(BOOL)isSelected{
    ViewRadius(self, 22.5);
    if (isSelected) {
        
        
        self.layer.borderWidth=1;
        self.layer.borderColor=[UIColor greenColor].CGColor;
        
    }else{
        self.layer.borderWidth=0;
        self.layer.borderColor=[UIColor greenColor].CGColor;
    }


}
@end
