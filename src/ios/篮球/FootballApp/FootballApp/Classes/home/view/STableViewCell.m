//
//  STableViewCell.m
//  FootballApp
//
//  Created by North on 16/9/12.
//  Copyright © 2016年 North. All rights reserved.
//

#import "STableViewCell.h"

@implementation STableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    STableViewCell *cell=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (cell) {
        
        _showLabel=[[UILabel alloc]initWithFrame:CGRectMake(3, 0, 140-6, 15)];
        _showLabel.textColor=[UIColor whiteColor];
        _showLabel.font=[UIFont systemFontOfSize:10];
       
        [cell.contentView addSubview:_showLabel];
    }
    return cell;

}
- (void)awakeFromNib {
    [super awakeFromNib];
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

   
}

@end
