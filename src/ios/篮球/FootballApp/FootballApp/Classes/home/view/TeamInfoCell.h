//
//  TeamInfoCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"
#import "MBStarScore.h"
@interface TeamInfoCell : BaseCell

@property (weak, nonatomic) IBOutlet UIImageView *teamLogo;
@property (weak, nonatomic) IBOutlet UILabel *teamName;
@property (weak, nonatomic) IBOutlet UILabel *teamRecord;
@property (weak, nonatomic) IBOutlet UILabel *teamLevel;
@property (weak, nonatomic) IBOutlet UILabel *teamAge;
@property (weak, nonatomic) IBOutlet UIButton *followBtn;
@property(nonatomic,assign)BOOL isfollow;
@property(nonatomic,copy)NSString *teamID;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;
@property (weak, nonatomic) IBOutlet UILabel *changeLabel;


@property (weak, nonatomic) IBOutlet MBStarScore *store;
@end
