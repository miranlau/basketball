//
//  TeamInfoCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamInfoCell.h"
#import "TeamModel.h"
#import "AvatarHelper.h"
#import "MatchModel.h"
#import "SendNewViewController.h"
@implementation TeamInfoCell

-(void)getdata:(id)data{
    
     BaseCellModel *model=data;
    
    if([model.reuseIdentifier isEqualToString:@"TeamInfo1Cell"]){
    
      
        TeamModel *tm=model.value;
        
        [self.teamLogo setImageWithURL:[NSURL URLWithString:tm.logo] placeholderImage:TeamLogo];
       
        [self.teamLogo addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
        
        self.teamLogo.layer.cornerRadius=70.0/2;
        self.teamLogo.layer.masksToBounds=YES;
        
        self.teamAge.text=[NSString stringWithFormat:@"平均年龄:%@",tm.age];
        self.teamName.text=tm.name;

        self.teamRecord.text=[NSString stringWithFormat:@"战绩:%@胜%@负",tm.win,tm.lose];
        
        
        self.teamLevel.text=[NSString stringWithFormat:@" %@级球队 ",tm.level];
        self.teamLevel.layer.cornerRadius=2;
        self.teamLevel.layer.masksToBounds=YES;
        
        self.store.show_star=[tm.stars intValue]/20;
    
        
        self.isfollow = tm.follow;
        _teamID = model.key;
        if ([[NSString stringWithFormat:@"%d", tm.follow] isEqual:@"0"])
        {
            [_followBtn setTitle:@"+关注" forState:UIControlStateNormal];
        }
        else
        {
            [_followBtn setTitle:@"取消关注" forState:UIControlStateNormal];
        }
        
        
        
    }
    
    if([model.reuseIdentifier isEqualToString:@"TeamInfoCell"]){
        
        
        
        TeamModel *tm=model.value;
        
        [self.teamLogo setImageWithURL:[NSURL URLWithString:tm.logo] placeholderImage:TeamLogo];
        
        [self.teamLogo addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
        
        self.teamLogo.layer.cornerRadius=35 ;
        self.teamLogo.layer.masksToBounds=YES;
        
        self.teamAge.text=[NSString stringWithFormat:@"平均年龄:%@",tm.age];
        self.teamName.text=tm.name;
        
        self.teamRecord.text=[NSString stringWithFormat:@"战绩:%@胜%@负",tm.win,tm.lose];
        
        
        self.teamLevel.text=[NSString stringWithFormat:@" %@级球队 ",tm.level];
        self.teamLevel.layer.cornerRadius=2;
        self.teamLevel.layer.masksToBounds=YES;
        
        
        if ([model.key intValue]==0) {
            _changeBtn.hidden=YES;
            _changeLabel.hidden=YES;
        }else{
        
            _changeBtn.hidden=NO;
            _changeLabel.hidden=NO;
        }
        
        
    }
    
    if([model.reuseIdentifier isEqualToString:@"TeamInfo3Cell"]){
        
        BaseCellModel *base=data;
        
        MatchModel *model=base.value;
        
        
        TeamModel *tm=[[TeamModel alloc]initWithDic:model.home];
        
        [self.teamLogo setImageWithURL:[NSURL URLWithString:tm.logo] placeholderImage:TeamLogo];
        
        [self.teamLogo addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
        
        self.teamLogo.layer.cornerRadius=70/2;
        self.teamLogo.layer.masksToBounds=YES;
        
        self.teamAge.text=[NSString stringWithFormat:@"%@人制",model.type];
        self.teamName.text=tm.name;
        
        self.teamRecord.text=[NSString stringWithFormat:@"%@",model.addr];
        self.teamLevel.text=[NSString stringWithFormat:@"%@",model.date];
        self.teamLevel.layer.cornerRadius=2;
        self.teamLevel.layer.masksToBounds=YES;
        
        
    }
    
    
    
    


}
-(void)headImageClick:(UITapGestureRecognizer *)tap{
    
    
    if ([self.reuseIdentifier isEqualToString:@"TeamInfo3Cell"]) {
        
        if (self.cellBlock) {
            self.cellBlock(@"");
        }
        
    }else{
    
    [AvatarHelper showImage:(UIImageView *)tap.view];
        
        
    }
    
    
}

- (IBAction)changeTeamBtnClick:(id)sender {
    
    self.cellBlock(@"changeteam");
}
//关注
- (IBAction)PlayerAttention:(id)sender {
 
    if ([AccountHelper shareAccount].isLogin == NO) {
        //判断是否登录
        [[NSNotificationCenter defaultCenter]postNotificationName:@"gotologins" object:nil];
    }
    else
    {
    //已经关注了
    if (self.isfollow)
    {
        
        [HttpRequestHelper postWithURL:Url(@"team/unfollow") params:@{@"teamId":_teamID} success:^(BOOL sucess, id responseObject) {
                    if (sucess)
                    {
            
                        [SVProgressHUD showInfoWithStatus:@"取消关注成功"];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"unpayAttentionSuccess" object:nil userInfo:nil];
            
                        
                    }
                    else
                    {
                        [SVProgressHUD showErrorWithStatus:responseObject];
                    }
                    
                } failure:^(NSError *error) {
                    
                }];
    }
    else
    {
        //没有关注
        [HttpRequestHelper postWithURL:Url(@"team/follow") params:@{@"teamId":_teamID} success:^(BOOL sucess, id responseObject) {
            if (sucess)
            {
              
                   [SVProgressHUD showInfoWithStatus:@"关注成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"payAttentionSuccess" object:nil userInfo:nil];
                
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
        } failure:^(NSError *error) {
            
        }];
        
    }
}
    
}

@end
