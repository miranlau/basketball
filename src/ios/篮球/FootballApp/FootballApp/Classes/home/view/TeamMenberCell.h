//
//  TeamMenberCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

@interface TeamMenberCell : BaseCell
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *menberView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (copy, nonatomic) MutableBlock clickBlock;
@end
