//
//  TeamMenberCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamMenberCell.h"
#import "PlayerModel.h"
#import "BaseButton.h"
@implementation TeamMenberCell

#define speace 15

static int  width= 50;
static int  height= 110;



-(void)awakeFromNib{
    
    self.contentView.backgroundColor=BGCOLOR1;

}

-(void)getdata:(id)data{
    
        
    
     BaseCellModel *model=data;
    
    
    if (![self.reuseIdentifier isEqualToString:@"TeamMenber2Cell"]){
    
    if (self.scrollView) {
        
        
        
        for (UIView *view in self.scrollView.subviews) {
            [view removeFromSuperview];
        }
        
        
        for (int i=0; i<[model.value count]; i++) {
            
            PlayerModel *player=[[PlayerModel alloc]initWithDic:model.value[i]];
            
            int IMGH=width+20;
            UIView *bgView=[[UIView alloc]initWithFrame:CGRectMake(i*(width+speace)+speace, (height-IMGH)/2.0, width, IMGH)];
            
            
            
            
            
            
            //头像
            
            UIImageView *imageview=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, width, width)];
            
            
            
            [imageview setImageWithURL:[NSURL URLWithString:player.avatar] placeholderImage:HeadImage];
            
            imageview.tag=i;
            imageview.layer.cornerRadius=width/2.0;
            imageview.layer.masksToBounds=YES;
            imageview.userInteractionEnabled=YES;
            [imageview addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
            [bgView addSubview:imageview];
            
            UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(0, width, width, 20)];
            name.text=player.name;
            name.textAlignment=1;
            name.lineBreakMode=NSLineBreakByClipping;
            name.textColor=[UIColor whiteColor];
            name.font=[UIFont systemFontOfSize:12];
            
            
             [bgView addSubview:name];

            //背景
            UIImageView *numImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"球衣.png"]];
             [bgView addSubview:numImage];
            //号码
            
            UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(width-21, width-19, 25, 19)];
            
            label.text=[NSString stringWithFormat:@"%@",player.number];
            
            if ([player.number intValue]==100) {
                label.textColor=[UIColor redColor];
            }else{
                
                label.textColor=[UIColor whiteColor];
            }
            label.font=[UIFont systemFontOfSize:10];
            label.textAlignment=1;
            
            
            
            
            numImage.frame=CGRectMake(width-21, width-19, 25, 19);
            
             [bgView addSubview:label];
            //队长
            
            if([model.key intValue]==[player.id intValue]){
                
                UIImage *image=[UIImage imageNamed:@"leader.png"];
                UIImageView *leader=[[UIImageView alloc]initWithImage:image];
                leader.frame=CGRectMake(CGRectGetMinX(imageview.frame), CGRectGetMinY(imageview.frame), image.size.width, image.size.height);
                
                [self.menberView addSubview:leader];
                
            }
            
            [self.scrollView addSubview:bgView];
            
            
            
        }
        
        [self.scrollView setContentSize:CGSizeMake([model.value count]*(width+speace)+speace, height)];
    }
    
    if (self.menberView) {
        
        for (UIView *view in self.menberView.subviews) {
            [view removeFromSuperview];
        }
        //计算每行显示几个
        
       NSInteger xWidth=width+speace;
       NSInteger num =kScreenWidth/xWidth;
        
        int cha=(kScreenWidth -num*width)/(num+1);
        
        
        NSInteger count=[model.value count];
        
        int IMGH=width+20;
        
        for (int i=0; i<count; i++) {
            
            PlayerModel *player=[[PlayerModel alloc]initWithDic:model.value[i]];
            
            int x=i%num*(width+cha)+cha;
            int y=i/num*(width+speace+ speace)+speace;
            
            
            UIImageView *imageview=[[UIImageView alloc]initWithFrame:CGRectMake(x, y , width, width)];
         
           [imageview setImageWithURL:[NSURL URLWithString:player.avatar] placeholderImage:HeadImage];
//            imageview.image=HeadImage;
            
            imageview.tag=i;
            
            imageview.layer.cornerRadius=width/2.0;
            imageview.layer.masksToBounds=YES;
      
            
            imageview.userInteractionEnabled=YES;
            [imageview addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
            
            
            
            //背景
            UIImageView *numImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"球衣.png"]];
            numImage.frame=CGRectMake(CGRectGetMaxX(imageview.frame)-19, CGRectGetMaxY(imageview.frame)-19, 19, 19);
           
            //号码
            
            UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageview.frame)-19, CGRectGetMaxY(imageview.frame)-19, 19, 19)];
            
            label.text=[NSString stringWithFormat:@"%@",player.number];
            
            if ([player.number intValue]==100) {
                label.textColor=[UIColor redColor];
            }else{
            
            label.textColor=[UIColor whiteColor];
            }
            
            
            
            label.font=[UIFont systemFontOfSize:10];
            label.textAlignment=1;
            
            
            
            
            UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(x, y+width, width, 20)];
            name.text=player.name;
            name.textAlignment=1;
            name.lineBreakMode=NSLineBreakByClipping;
            name.textColor=[UIColor whiteColor];
            name.font=[UIFont systemFontOfSize:12];
            
            

            [self.menberView addSubview:imageview];
            [self.menberView addSubview:numImage];
             [self.menberView addSubview:label];
            
            [self.menberView addSubview:name];
            
            //队长
            
            if([model.key intValue]==[player.id intValue]){
                
                UIImage *image=[UIImage imageNamed:@"leader.png"];
                UIImageView *leader=[[UIImageView alloc]initWithImage:image];
                leader.frame=CGRectMake(CGRectGetMinX(imageview.frame), CGRectGetMinY(imageview.frame), image.size.width, image.size.height);
                
                [self.menberView addSubview:leader];
                
            }
            
            
            
            
            
            
            if (i==count-1) {
                
                self.height.constant=imageview.frame.origin.y+IMGH+speace;
                [self layoutIfNeeded];
            }
            
            
        }
        
        if (count ==0) {
            self.height.constant=0;
            [self layoutIfNeeded];
        }
        
        
    }
    }
    
    if ([self.reuseIdentifier isEqualToString:@"TeamMenber2Cell"]) {
        
        for (UIView *view in self.menberView.subviews) {
            [view removeFromSuperview];
        }
        //计算每行显示几个
        
        NSInteger xWidth=width+speace;
        NSInteger num =kScreenWidth/xWidth;
        
        int cha=(kScreenWidth -num*width)/(num+1);
        
        
        NSInteger count=[model.value count];
        
        
        
        for (int i=0; i<count; i++) {
            
            PlayerModel *player=[[PlayerModel alloc]initWithDic:model.value[i]];
            
            int x=i%num*(width+cha)+cha;
            int y=i/num*(width+speace)+speace;
            
            
            BaseButton *imageview=[[BaseButton alloc]initWithFrame:CGRectMake(x, y, width, width)];
            
            [imageview setBackgroundImageForState:0 withURL:[NSURL URLWithString:player.avatar] placeholderImage:HeadImage];
            
            
                UIImageView *select=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"选中绿勾"]];
                select.frame=CGRectMake(x+width-20, y, 20, 20);
                                        
            
                
                imageview.seletedView=select;
            
            
            
            
            if ([player.ispay intValue]==1) {
                 imageview.seleted=YES;
            }else{
                 imageview.seleted=NO;
            
            }
            
            imageview.seletedView.hidden=!imageview.seleted;
            
            
            
            imageview.tag=i;
            
            imageview.layer.cornerRadius=width/2.0;
            imageview.layer.masksToBounds=YES;
            
            imageview.userInteractionEnabled=YES;
            
            
            
            [imageview addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            //背景
            UIImageView *numImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"球衣.png"]];
            numImage.frame=CGRectMake(CGRectGetMaxX(imageview.frame)-19, CGRectGetMaxY(imageview.frame)-19, 19, 19);
            
            //号码
            
            UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageview.frame)-19, CGRectGetMaxY(imageview.frame)-19, 19, 19)];
            
            label.text=[NSString stringWithFormat:@"%@",player.number];
            
            if ([player.number intValue]==100) {
                label.textColor=[UIColor redColor];
            }else{
                
                label.textColor=[UIColor whiteColor];
            }
            
            
            
            label.font=[UIFont systemFontOfSize:10];
            label.textAlignment=1;
            
            
            
            
            
            
            
           
            [self.menberView addSubview:imageview];
            [self.menberView addSubview:numImage];
            [self.menberView addSubview:label];
             [self.menberView addSubview:select];
            
            //队长
            
            if([model.key intValue]==[player.id intValue]){
                
                UIImage *image=[UIImage imageNamed:@"leader.png"];
                UIImageView *leader=[[UIImageView alloc]initWithImage:image];
                leader.frame=CGRectMake(CGRectGetMinX(imageview.frame), CGRectGetMinY(imageview.frame), image.size.width, image.size.height);
                
                [self.menberView addSubview:leader];
                
            }
            
            if (i==count-1) {
                
                self.height.constant=imageview.frame.origin.y+width+speace;
                [self layoutIfNeeded];
            }
            
            
        }
        
        if (count ==0) {
            self.height.constant=0;
            [self layoutIfNeeded];
        }
    }
 
}
-(void)selectBtnClick:(BaseButton *)butn{
    
    butn.seleted=!butn.seleted;

    BaseCellModel *model=self.data;
    
    if ([model.name intValue]!=1) {
        return;
    }
    PlayerModel *play=[[PlayerModel alloc]initWithDic:model.value[butn.tag]];
    
    butn.seletedView.hidden=!butn.seleted;
  

    if ( self.cellBlock) {
        
        self.cellBlock(play.id);
    }

}

 -(void)headImageClick:(UITapGestureRecognizer *)tap{
     
     
     BaseCellModel *model=self.data;
     PlayerModel *play=[[PlayerModel alloc]initWithDic:model.value[tap.view.tag]];

     
     if ( self.cellBlock) {
         self.cellBlock(play);
     }
  
}
@end
