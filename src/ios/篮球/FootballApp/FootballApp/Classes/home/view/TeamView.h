//
//  TeamView.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamView : UIView
{
    __weak IBOutlet UIView *cancelView;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIView *centerView;
    NSArray * list;
    MutableBlock clickBlock;
    __weak IBOutlet NSLayoutConstraint *bottom;
}

-(id)initWithArr:(NSArray *)data clickBlock:(MutableBlock)block;

-(void)show;
-(void)hide;

@end
