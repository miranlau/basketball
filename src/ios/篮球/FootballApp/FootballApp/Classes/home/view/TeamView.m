//
//  TeamView.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamView.h"
#import "TeamModel.h"
#import "TeamHelper.h"
#define SHOW 0
#define HIDE 158
@implementation TeamView


-(id)initWithArr:(NSArray *)data clickBlock:(MutableBlock)block{

    
    self =[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        list=data;
        clickBlock=block;
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        centerView.backgroundColor=[COLOR colorWithHexString:@"3a3a3a"];
        
        UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
        [cancelView addGestureRecognizer:tap];
        bottom.constant=-HIDE;

        [self layoutIfNeeded];
        
        
        [self addTeam:list];
    }
    
    
    return self;

}


-(void)addTeam:(NSArray *)teams{
    
    int speace =10;
    int btnW= 98;
    
    
    //如果当前球队数量能够在当前屏幕显示完
    if (kScreenWidth-([teams count]*(speace+btnW))>0){
        speace=(kScreenWidth -[teams count]*btnW)/([teams count]+1);
        
        
    }
    for (NSInteger  i =0; i<[teams count]; i++) {
        TeamModel *model=teams[i];
        UIImageView *butn=[[UIImageView alloc]initWithFrame:CGRectMake(i%[teams count]*(btnW+speace)+speace, 35, btnW, btnW)];
        butn.userInteractionEnabled=YES;
        [butn setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:TeamLogo];
       
        
        butn.tag=i;
        butn.layer.cornerRadius=btnW/2.0;
        butn.layer.masksToBounds=YES;
        
        
        UILabel *teamName=[[UILabel alloc]initWithFrame:CGRectMake(i%[teams count]*(btnW+speace)+speace, 35+btnW, btnW, [TeamHelper widthOfString:model.name font:[UIFont systemFontOfSize:12] width:btnW] + 5)];
        
        teamName.font=[UIFont systemFontOfSize:12];
        teamName.numberOfLines = 0;
        teamName.lineBreakMode = NSLineBreakByTruncatingTail;
        teamName.textAlignment = 1;
        teamName.textColor=[UIColor whiteColor];
        teamName.text=model.name;
        
        [butn addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btnClick:)]];
        
        [scrollView addSubview:teamName];
        [scrollView addSubview:butn];
        
        
        
        if(i==[teams count]-1){
            
            scrollView.contentSize=CGSizeMake(i%[teams count]*(btnW+speace)+2*speace+btnW, HIDE);
        
        
        }
        
 
    }

}

-(void)btnClick:(UIGestureRecognizer *)sender{
    
    clickBlock(@(sender.view.tag));
    
    
    
    [self hide];
    
}


-(void)show{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
        bottom.constant=SHOW;
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        
        
    }];
}
-(void)hide{
    [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
        bottom.constant=-HIDE;
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
@end
