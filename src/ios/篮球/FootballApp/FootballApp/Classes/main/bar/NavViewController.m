//
//  NavViewController.m
//  FootballApp
//
//  Created by MagicBeans2 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "NavViewController.h"

@interface NavViewController ()<UIGestureRecognizerDelegate>

@end

@implementation NavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UINavigationBar appearance] setBarTintColor: BarColor];
    //    //设置按钮
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    //设置所有导航条样式
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackOpaque];
    [[UINavigationBar appearance] setTranslucent:NO];
    
   [[UINavigationBar appearance] setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont  systemFontOfSize:16]}];
    
            // 获取系统自带滑动手势的target对象
            id target = self.interactivePopGestureRecognizer.delegate;
            // 创建全屏滑动手势，调用系统自带滑动手势的target的action方法
            UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action: NSSelectorFromString(@"handleNavigationTransition:")];
    
          //  UISwipeGestureRecognizer *pan= [[UISwipeGestureRecognizer alloc] initWithTarget:target action: NSSelectorFromString(@"handleNavigationTransition:")];
            // 设置手势代理，拦截手势触发
    
            pan.delegate = self;
    
            // 给导航控制器的view添加全屏滑动手势
            [self.view addGestureRecognizer:pan];

            // 禁止使用系统自带的滑动手势
            self.interactivePopGestureRecognizer.enabled = NO;
    
}
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer
{
    // 注意：只有非根控制器才有滑动返回功能，根控制器没有。
    // 判断导航控制器是否只有一个子控制器，如果只有一个子控制器，肯定是根控制器
    if (self.childViewControllers.count == 1) {

        // 表示用户在根控制器界面，就不需要触发滑动手势，
        return NO;
    }

    CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view];
    if (translation.x <= 0) {
        return NO;
    }
    
    
    
    
    return YES;
}
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([self.viewControllers count] >0) {
        
        viewController.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back" ] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClick:)];
        viewController.hidesBottomBarWhenPushed=YES;//影藏状态栏
   
    }
    [super pushViewController:viewController animated:YES];
    
}

-(void)backBtnClick:(id)sender{
    
    [self popViewControllerAnimated:YES];
    
}
- (nullable UIViewController *)popViewControllerAnimated:(BOOL)animated{
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"goBack" object:nil];
    

    return [super popViewControllerAnimated:YES];
}
-(BOOL)shouldAutorotate
{
    return YES;
}
-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.viewControllers.lastObject
            supportedInterfaceOrientations];
}
-
(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [self.viewControllers.lastObject
            preferredInterfaceOrientationForPresentation];
}

@end
