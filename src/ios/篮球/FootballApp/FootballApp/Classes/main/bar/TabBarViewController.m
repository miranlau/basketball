//
//  TabBarViewController.m
//  FootballApp
//
//  Created by MagicBeans2 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]} forState:UIControlStateNormal];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateSelected];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
       [self.tabBar setBackgroundImage:[UIImage imageNamed:@"底部-ipad"]];
    }
    else
    {
       [self.tabBar setBackgroundImage:[UIImage imageNamed:@"底部导航栏底"]];
    }
    
    
    
    for (int i=0;i<[self.tabBar.items count];i++)
    {

        UITabBarItem *tabItem=[self.tabBar.items objectAtIndex:i];
        
        tabItem.title = @"";
        UIImage *img1=nil;
        UIImage *img2=nil;
        
     
        
        
//        [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        self.tabBar.shadowImage = [[UIImage alloc] init];
        
        
        if ([[self.tabBar.items objectAtIndex:i] isKindOfClass:[UITabBarItem class]])
        {
            switch (i){
                case 0:
                    
                    img1=[UIImage imageNamed:@"首页-选中.png"];
                    img2=[UIImage imageNamed:@"首页-未选中.png"];
                    tabItem.selectedImage=[img1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.image=[img2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.title=@"首页";
                    break;
                case 1:
                    img1=[UIImage imageNamed:@"发现-选中.png"];
                    img2=[UIImage imageNamed:@"发现-未选中.png"];
                    tabItem.selectedImage=[img1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.image=[img2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.title=@"发现";
                    break;
                case 2:
                    
                    img1=[UIImage imageNamed:@"圈子-选中.png"];
                    img2=[UIImage imageNamed:@"圈子-未选中.png"];
                    tabItem.selectedImage=[img1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.image=[img2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.title=@"圈子";
                    break;
                case 3:
                    img1=[UIImage imageNamed:@"消息-选中.png"];
                    img2=[UIImage imageNamed:@"消息-未选中.png"];
                    tabItem.selectedImage=[img1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.image=[img2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    tabItem.title=@"消息";
                    break;
                    
                default:
                    break;
            }
            
            
            
        }
        
        
        
    }
    
    
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}
@end
