//
//  BaseCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/14.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCell : UITableViewCell

@property(copy,nonatomic) MutableBlock cellBlock;

@property(copy,nonatomic) id data;

-(void)setCellBlock:(MutableBlock)cellBlock;
-(void)cellSelected;//被选中
-(void)cellunSelected;//被选中
-(void)getdata:(id)data;


@end
