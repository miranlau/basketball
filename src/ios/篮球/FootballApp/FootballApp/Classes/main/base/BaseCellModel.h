//
//  BaseCellModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseCellModel : NSObject

@property(retain,nonatomic) NSString *image;
@property(retain,nonatomic) NSString *name;
@property(retain,nonatomic) NSString *key;
@property(retain,nonatomic) id value;
@property(retain,nonatomic) NSString *reuseIdentifier;

+(BaseCellModel *)setModel :(NSString *)name key:(NSString *)key value:(id)value image:(NSString *)image reuseIdentifier :(NSString *)reuseIdentifier;
+(void)insetKeyForDic:(NSMutableDictionary *)dic  key:(NSString *)key value:(id)value;
@end
