//
//  BaseCellModel.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/15.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCellModel.h"

@implementation BaseCellModel


+(BaseCellModel *)setModel :(NSString *)name key:(NSString *)key value:(id)value image:(NSString *)image reuseIdentifier :(NSString *)reuseIdentifier{

    BaseCellModel *model=[[BaseCellModel alloc]init];
    model.name=name;
     model.image=image;
     model.reuseIdentifier=reuseIdentifier;
     model.key=key;
     model.value=value;
    return model;

}
+(void)insetKeyForDic:(NSMutableDictionary *)dic  key:(NSString *)key value:(id)value{
    
    [dic setValue:value forKey:key];
    
}
@end
