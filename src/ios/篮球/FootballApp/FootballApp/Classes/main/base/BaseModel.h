//
//  BaseModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/6.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject
@property(retain,nonatomic)NSMutableDictionary *unDefinedKeyDic;//不包含的的key数组
-(id)initWithDic:(NSDictionary *)dic;
-(void)setValue:(id)value forUndefinedKey:(NSString *)key;

@end
