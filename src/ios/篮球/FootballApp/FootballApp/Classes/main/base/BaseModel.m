//
//  BaseModel.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/6.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel
-(id)initWithDic:(NSDictionary *)dic{
    
    self=[super init];
    if (self) {
        
        [self setValuesForKeysWithDictionary:dic];
        
    }
    return self;

}
-(void)setValue:(id)value forKey:(NSString *)key{
    
//    NSString *valueString = [NSString stringWithFormat:@"%@",value];
//    if ([valueString isEqualToString:@"(null)"]||[valueString isEqualToString:@"null"]) {
//        value=nil;
//    }
    
    [super setValue:value forKey:key];
    
  
    
    
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    if(!_unDefinedKeyDic){
        _unDefinedKeyDic=[NSMutableDictionary dictionary];
    }
    [_unDefinedKeyDic  setValue:value forKey:key];

}
@end
