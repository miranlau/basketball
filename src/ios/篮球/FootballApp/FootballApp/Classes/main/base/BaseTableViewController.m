//
//  BaseTableViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goBack) name:@"goBack" object:nil];
    
    
}
-(void)goBack{
    
    
    if ([self respondsToSelector:NSSelectorFromString(@"backBtnClick")]) {
        
        [self backBtnClick:nil];
    }
    
    
}

-(void)backBtnClick:(id)sender{
    
    
    
}
@end


