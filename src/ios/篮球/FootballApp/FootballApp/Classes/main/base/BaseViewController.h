//
//  BaseViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>




@property(copy,nonatomic)MutableBlock cblock;
@property(retain,nonatomic)NSMutableArray *dataArr;
@property(retain,nonatomic)NSMutableDictionary *dataDic;


-(void)setControllerBlock:(MutableBlock)block;
//获取用户信息
-(void)loadUserData:(MutableBlock)sucessed;
//获取球员信息
-(void)loadPlayerData:(MutableBlock)sucessed;
-(void)backBtnClick:(id)sender;
-(void)headImageViewClick;
-(void)userInfoChanged;
-(void)headImageChange;
//登录成功
-(void)accountLogined;
//退出成功
-(void)accountLoginOut;
//得到我是队长的球队数组
-(NSMutableArray *)getMyLeaderTeams;
@end
