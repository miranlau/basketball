//
//  BaseViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"
#import "TeamModel.h"


@interface BaseViewController ()<UIGestureRecognizerDelegate>

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goBack) name:@"goBack" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(headImageChange) name:@"headImageViewChange" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userInfoChanged) name:@"userInfoChange" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(accountLogined) name:@"logined" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(accountLogined) name:@"loginOut" object:nil];
    
    
    
    
    
   
}
@synthesize dataArr=_dataArr,dataDic=_dataDic;

-(NSMutableArray *)dataArr{

    if (!_dataArr) {
        _dataArr=[NSMutableArray array];
    }

    return _dataArr;
}
-(NSMutableDictionary *)dataDic{
    
    if (!_dataDic) {
        _dataDic = [NSMutableDictionary dictionary];
    }
    
    return _dataDic;
}
-(void)setControllerBlock:(MutableBlock)block{

    self.cblock = block;

}



-(void)loadUserData:(MutableBlock)sucessed{
    
    
    [HttpRequestHelper postWithURL:Url(@"user/getUserInfo") params:@{@"userId":[AccountHelper shareAccount].player.id} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
           
            
            UserModel *model=[[UserModel alloc]initWithDic:responseObject];
            [AccountHelper shareAccount].user=model;
            sucessed(@(sucess));
        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
    
}


-(void)loadPlayerData:(MutableBlock)sucessed{
    
    
    
    [[AccountHelper shareAccount]getPlayer:^(id data) {
       
         sucessed(@(1));
        
    }];
    

}


-(void)goBack{
    
    
    
        
        [self backBtnClick:nil];
    


}
-(void)headImageChange{


}
//得到我是队长的球队数组
-(NSMutableArray *)getMyLeaderTeams{
    
    NSMutableArray *arr=[NSMutableArray array];
    
    for (NSDictionary *dic  in [AccountHelper shareAccount].player.teams) {
        
        TeamModel *model=[[TeamModel alloc]initWithDic:dic];
        
        if ([model.leader  intValue]==[[AccountHelper shareAccount].player.id intValue]){
            
            [arr addObject:model];
            
        }
    }
    
    return arr;
    
  
}
-(void)backBtnClick:(id)sender{
  

}
-(void)headImageViewClick{


    
    if ([AccountHelper shareAccount].isLogin==YES) {
            UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
            controller.navigationItem.title=@"我的";
            [self.navigationController pushViewController:controller animated:YES];
    }else{
    

    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
    controller.navigationItem.title=@"登录";
    [self.navigationController pushViewController:controller animated:YES];
        
    }

}

-(void)accountLogined{


}
-(void)accountLoginOut{



}
-(void)userInfoChanged{

}
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
