//
//  AccountHelper.h
//  FootballApp
//
//  Created by MagicBeans2 on 16/5/5.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerModel.h"
#import "UserModel.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import <HyphenateSDK/EMSDK.h>
#import <CoreLocation/CoreLocation.h>
#define MINEId [[AccountHelper shareAccount].player.id intValue]

@interface AccountHelper : NSObject<CLLocationManagerDelegate>

@property(retain,nonatomic) NSString *token;//用户唯一标识
@property(retain,nonatomic) NSString *city;//当前位置编码
@property(retain,nonatomic) NSString *addr;//当前街道
@property(retain,nonatomic) NSString *account;//账号
@property(retain,nonatomic) NSString *password;//密码
@property(assign,nonatomic) BOOL isLogin;//是否登录
@property(assign,nonatomic) BOOL isPublic;//是否发布 ，用于支付多少钱，自动填写验证码

@property(assign,nonatomic) BOOL isReg;//是否刚注册，用于显示去完善弹框
@property(assign,nonatomic) BOOL isLive;//是否正在直播

@property(assign,nonatomic) BOOL isBackHome;//是否正在直播

@property(assign,nonatomic) BOOL isShow;//是否显示足球

@property(copy,nonatomic) MutableBlock locationBlock;


@property(retain,nonatomic) NSString *versionNumber;//服务器最新版本号
@property(retain,nonatomic) NSString *downloadURL;  //服务器最新版本号下载路径

@property(strong,nonatomic)NSString *latitude;
@property(strong,nonatomic)NSString *longitude;

@property(strong,nonatomic)AMapLocationManager *locationManager ;
@property(strong,nonatomic)CLLocationManager  *locationManager2 ;

@property(retain,nonatomic)PlayerModel *player;//球员信息
@property(retain,nonatomic)UserModel *user;//球员信息
@property(nonatomic,copy)NSMutableArray *tuisongArray;

+(instancetype)shareAccount;
-(void)autoLogin;//自动登录
-(void)getPlayer:(MutableBlock)sucess;//获取我的球员信息
-(void)cancelLogin;//注销登录
-(void)autoLocation;//自动定位
-(void)saveAccount:(NSString *)account password:(NSString *)password;

+ (void)PushRegistration;//通知注册
+ (NSString*)GetUUID; //得到UUID

-(void)autoLoginHX:(NSString *)UserName;//自动登录环信

-(void)getLocation:(MutableBlock)block;



@end
