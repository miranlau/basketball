//
//  AccountHelper.m
//  FootballApp
//
//  Created by MagicBeans2 on 16/5/5.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AccountHelper.h"
#import "KeychainItemWrapper.h"
#import "TeamModel.h"
#import "JPUSHService.h"
#import <CoreLocation/CoreLocation.h>

#define identf @"BbasketballApp"

@implementation AccountHelper

+(instancetype)shareAccount{
    
    static AccountHelper *account=nil;
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        account = [[self alloc] init] ;
        account.isPublic=NO;
    });
    
    return account;
    
}

-(void)autoLogin
{
    
    KeychainItemWrapper *keyChain = [[KeychainItemWrapper alloc] initWithIdentifier:identf accessGroup:nil];
    
    [AccountHelper shareAccount].account=[keyChain objectForKey:(id)kSecAttrAccount];
    [AccountHelper shareAccount].password=[keyChain objectForKey:(id)kSecValueData];
    
    NSString *pwd=[NSString stringWithFormat:@"%@",[AccountHelper shareAccount].password];
    
    
    if (pwd &&![pwd isEqualToString:@""]&&![pwd isEqualToString:@"(null)"]&&![pwd isKindOfClass:[NSNull class]]){
        
        [AccountHelper shareAccount].isShow=YES;
        [HttpRequestHelper postWithURL:Url(@"login/submit") params:@{@"username":self.account,@"password":pwd} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                PlayerModel *model=[[PlayerModel alloc]initWithDic:responseObject];
                self.player=model;
                self.token=self.player.token;
                self.isLogin = YES;
                [self saveAccount:self.account password:self.password];

              [[NSNotificationCenter defaultCenter]postNotificationName:@"HomeLocalPhoneArray" object:nil];
                [self autoLoginHX:model.username];
                [AccountHelper PushRegistration];
                //获取我的球员信息
                 [AccountHelper shareAccount].isShow=YES;
                [self getPlayer:^(id data) {
                    
                    
                    //发送通知,防止home加载比获取数据块，获得头像
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"headImageViewChange" object:nil];
                    
//                    [[NSNotificationCenter defaultCenter]postNotificationName:@"logined" object:nil];
                    
             
                    
                   
                }];
   
            }
            else
            {
                [SVProgressHUD showInfoWithStatus:@"自动登录失败"];
            }
   
        } failure:^(NSError *error){
           
            
        }];
  
    }
  
 
}
-(void)getPlayer:(MutableBlock)block{
    
    
    
    
    [HttpRequestHelper postWithURL:Url(@"player/getPlayerInfo") params:@{@"userId":self.player.id} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            PlayerModel *model=[[PlayerModel alloc]initWithDic:responseObject];
            [AccountHelper shareAccount].player=model;
            
            block(@"");
        }
        
        
    } failure:^(NSError *error) {
        
        
        
        
    }];
    
    
    





}
-(void)cancelLogin{
    
   
    
    
    [HttpRequestHelper postWithURL:Url(@"user/logoff") params:@{} success:^(BOOL sucess, id responseObject) {
        
        
        
    } failure:^(NSError *error) {
        
    }];
    
    
    //移除版本升级的标示
    [Defaults removeObjectForKey:@"gotoupdateNewVersion"];
    
    self.isLogin=NO;
    self.token=nil;
    self.player=nil;
    self.user=nil;
    KeychainItemWrapper *keyChain = [[KeychainItemWrapper alloc] initWithIdentifier:identf accessGroup:nil];
    [keyChain setObject:@"" forKey:(id)CFBridgingRelease(kSecValueData)];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"loginOut" object:nil];

}
@synthesize isPublic=_isPublic;

-(void)autoLocation
{
   
    
    
    
//    
//    _locationManager = [[AMapLocationManager alloc] init];
//    [_locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
//    //   定位超时时间，可修改，最小2s
//    _locationManager.locationTimeout = 3;
//    //   逆地理请求超时时间，可修改，最小2s
//    _locationManager.reGeocodeTimeout = 3;
//    
//    
//    
//    [_locationManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
//        
//        if (error) {
//            if (_locationBlock) {
//                _locationBlock(@"1000");
//            }
//            return ;
//            
//        }
//
//        
//        if (location) {
//            self.latitude=[NSString stringWithFormat:@"%f",location.coordinate.latitude];
//            self.longitude=[NSString stringWithFormat:@"%f",location.coordinate.longitude];
//            
//            if (_locationBlock) {
//                _locationBlock(@"");
//            }
//            
//        }
//        
//        if (regeocode) {
//            
//            self.city=regeocode.citycode;
//            self.addr = regeocode.street;
//        }
//        
//    }];
    
    
    
    if([CLLocationManager locationServicesEnabled]) {
        //定位初始化
        _locationManager2=[[CLLocationManager alloc] init];
        _locationManager2.delegate=self;
        _locationManager2.desiredAccuracy=kCLLocationAccuracyBest;
        _locationManager2.distanceFilter=10;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >=8) {
            [_locationManager2 requestWhenInUseAuthorization];//使用程序其间允许访问位置数据（iOS8定位需要）
        }
        
        [_locationManager2 startUpdatingLocation];//开启定位
        
        
        
    }else {
        //提示用户无法进行定位操作
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"定位不成功 ,请确认开启定位" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
    // 开始定位
    [_locationManager2 startUpdatingLocation];
    
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    CLLocation *location=[locations lastObject];
    
    self.latitude=[NSString stringWithFormat:@"%f",location.coordinate.latitude];
    self.longitude=[NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    if (_locationBlock) {
        _locationBlock(@"");
        
    }
    
    CLGeocoder *clGeoCoder = [[CLGeocoder alloc] init];
    
    CLLocation *cl = [[CLLocation alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    
    
    [clGeoCoder reverseGeocodeLocation:cl completionHandler: ^(NSArray *placemarks,NSError *error) {
        
        
        CLPlacemark *placeMark=[placemarks lastObject];
        
        self.city=placeMark.addressDictionary[@"City"];
        self.addr=self.city;
        
        
    }];
    
    
    
    
    
}
-(void)getLocation:(MutableBlock)block{
    
    _locationBlock=block;
    
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        
        _locationBlock(@"1000");
    }else{
        
        if ([self.longitude doubleValue]>0) {
            _locationBlock(@"");
        }
        
    }


}

-(void)saveAccount:(NSString *)account password:(NSString *)password{
    self.account=account;
    self.isLogin=YES;
    
     KeychainItemWrapper *keyChain = [[KeychainItemWrapper alloc] initWithIdentifier:identf accessGroup:nil];
    [keyChain setObject:account forKey:(id)kSecAttrAccount];
    [keyChain setObject:password forKey:(id)kSecValueData];
}

+ (void)PushRegistration
{
    
    NSString *string =[JPUSHService registrationID];
    if(string && [AccountHelper shareAccount].isLogin==YES && ![string isEqualToString:@""])
    {
        NSDictionary *dic = @{@"userId":[NSString stringWithFormat:@"%d",MINEId] ,@"deviceId":[AccountHelper GetUUID],@"deviceToken": string ,@"deviceType":[NSNumber numberWithInt:1]};
         [AccountHelper shareAccount].isShow=YES;
        [HttpRequestHelper postWithURL:Url(@"deviceToken/update") params:dic success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
         
                
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
        } failure:^(NSError *error) {
            
        }];
    }
  
}
-(void)autoLoginHX:(NSString *)UserName{

    [[EMClient sharedClient] asyncLoginWithUsername:UserName password:UserName success:^{
        
     
         [[NSNotificationCenter defaultCenter] postNotificationName:@"joinTeam" object:nil];
        
    } failure:^(EMError *aError) {
        
        
        
    }];

}
+(NSString*)GetUUID
{
    CFUUIDRef puuid = CFUUIDCreate( nil );
    CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
    NSString * result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
    CFRelease(puuid);
    CFRelease(uuidString);
    return result;
}

@end
