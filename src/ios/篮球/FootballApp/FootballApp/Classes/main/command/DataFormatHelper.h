//
//  DataFormatHelper.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/24.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataFormatHelper : NSObject

//通过获取系统时间和本地比较来判断是今天明天昨天
+(NSString *)timeChangeFormart:(NSString *)string times:(NSString *)time;
+(NSString *)compareDate:(NSString *)date;

@end
