//
//  DataFormatHelper.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/24.
//  Copyright © 2016年 North. All rights reserved.
//

#import "DataFormatHelper.h"

@implementation DataFormatHelper
+(NSString *)timeChangeFormart:(NSString *)string times:(NSString *)time{
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    
    //用[NSDate date]可以获取系统当前时间
    NSDate *date = [dateFormatter dateFromString:time];
    
    if ([string isEqualToString:@"MM月dd日 EEEE"]) {
        [dateFormatter setDateFormat:string];
          NSString *datas=[dateFormatter stringFromDate:date];

        return  datas;
        
    }else{

        [dateFormatter setDateFormat:string];
    
    
    return [dateFormatter stringFromDate:date];
    }
    
}
//判断今天明天昨天
+(NSString *)compareDate:(NSString *)date{
    
    //获取今天时间
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"MM月dd日 EEEE"];
  
    NSString *todayString = [dateFormatter  stringFromDate:[NSDate date]];
    
    NSDate *today=[dateFormatter dateFromString:todayString];
    
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    
    

    NSDate *tomorrow, *yesterday;
    
    tomorrow = [today dateByAddingTimeInterval: secondsPerDay];
    yesterday = [today dateByAddingTimeInterval: -secondsPerDay];
    
    // 10 first characters of description is the calendar date:
  
    NSString * yesterdayString = [dateFormatter  stringFromDate:yesterday];
    NSString * tomorrowString = [dateFormatter  stringFromDate:tomorrow];
    
   
    
    if ([date isEqualToString:todayString])
    {
     
        
        
        return [NSString stringWithFormat:@"%@ 今天",date]    ;
    } else if ([date isEqualToString:yesterdayString])
    {
        return[NSString stringWithFormat:@"%@ 昨天",date];
    }else if ([date isEqualToString:tomorrowString])
    {
        return [NSString stringWithFormat:@"%@ 明天",date];
    }
    else
    {
        return date;
    }
}
@end
