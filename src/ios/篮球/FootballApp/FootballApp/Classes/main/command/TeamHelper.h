//
//  TeamHelper.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/10.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamHelper : NSObject

+(instancetype)shareTeam;


@property(copy,nonatomic)NSString *name;//球队名字
@property(copy,nonatomic)NSString *password;//暗号
@property(copy,nonatomic)NSString *logo;//球队logo



//创建球队
+(void)creatTeam:(MutableBlock)sucessd failure:(MutableBlock)failured;
//修改球队
+(void)changeTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;
//加入球队
+(void)joinTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;
//踢人或者退出球队
+(void)quitTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;
//退出球队
+(void)kickoutTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;
//解散球队
+(void)dissolveTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;
//查询球队信息
+(void)getTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;
+(void)menberNum:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;

//改变球员的球衣号
+(void)changeNum:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured;

//动态获取label高度
+ (CGFloat)widthOfString:(NSString *)string font:(UIFont *)font width:(CGFloat)width;

//得到获取通讯录的权限，这里设置权限以后在手机的隐私里面才会出现足球权限的app
+(void)CheckAddressBookAuthorization:(void (^)(bool isAuthorized))block;
@end
