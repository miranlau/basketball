//
//  TeamHelper.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/10.
//  Copyright © 2016年 North. All rights reserved.
//

#import "TeamHelper.h"
#import <AddressBook/AddressBook.h>
@implementation TeamHelper
+(instancetype)shareTeam{

    static TeamHelper *team=nil;
    
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        team = [[self alloc] init] ;
       
    }) ;
    
    return team;
}


+(void)creatTeam:(MutableBlock)sucessd failure:(MutableBlock)failured{
    
    
    NSDictionary *dic=@{@"name":[TeamHelper shareTeam].name,@"password":[TeamHelper shareTeam].password,@"logo":[TeamHelper shareTeam].logo};

    [HttpRequestHelper postWithURL:Url(@"team/create") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
        
        }
        
        
    } failure:^(NSError *error) {
        
    }];


}
+(void)changeTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{


    [HttpRequestHelper postWithURL:Url(@"team/modify") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];

}
//修改球员的球衣号
+(void)changeNum:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{
    
    
    [HttpRequestHelper postWithURL:Url(@"game/modifyNumber") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
}

//加入球队
+(void)joinTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{


    [HttpRequestHelper postWithURL:Url(@"team/join") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];



}
//踢人
+(void)quitTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{

    [HttpRequestHelper postWithURL:Url(@"team/kickout") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];

}
//退出球队
+(void)kickoutTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{
    
    [HttpRequestHelper postWithURL:Url(@"team/quit") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
}

//解散球队
+(void)dissolveTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{

    [HttpRequestHelper postWithURL:Url(@"team/dissolve") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
    }];


}
//查询球队信息
+(void)getTeam:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{


    [HttpRequestHelper postWithURL:Url(@"team/getTeam") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
        
        
        
    }];

}

//修改球队信息
+(void)menberNum:(id)dic success:(MutableBlock)sucessd failure:(MutableBlock)failured{
    
    
    [HttpRequestHelper postWithURL:Url(@"team/modifyNumber") params:dic success:^(BOOL sucess, id responseObject) {
        
        
        if (sucess) {
            sucessd(responseObject);
        }else{
            failured(responseObject);
            
        }
        
        
    } failure:^(NSError *error) {
        
        
        
        
    }];
    
}

//动态获取label高度
+ (CGFloat)widthOfString:(NSString *)string font:(UIFont *)font width:(CGFloat)width
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.height;
}


+(void)CheckAddressBookAuthorization:(void (^)(bool isAuthorized))block
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAuthorizationStatus authStatus = ABAddressBookGetAuthorizationStatus();
    
    if (authStatus != kABAuthorizationStatusAuthorized)
    {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         if (error)
                                                         {
                                                             NSLog(@"Error: %@", (__bridge NSError *)error);
                                                         }
                                                         else if (!granted)
                                                         {
                                                             
                                                             block(NO);
                                                         }
                                                         else
                                                         {
                                                             block(YES);
                                                         }
                                                     });
                                                 });
    }
    else
    {
        block(YES);
    }
    
}

@end
