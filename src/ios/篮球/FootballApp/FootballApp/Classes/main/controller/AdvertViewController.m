//
//  AdvertViewController.m
//  FootballApp
//
//  Created by zj on 16/6/20.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AdvertViewController.h"
#import "HomeViewController.h"
#import "AdvertWebController.h"
#import "TabBarViewController.h"
@interface AdvertViewController ()
{
    __weak IBOutlet UIImageView *myimageview;
    
    __weak IBOutlet UIButton *JumpButton;
    NSTimer *timer;
    int i ;
}
@end

@implementation AdvertViewController


- (void)gotoFirstView
{
    i = i-1;
    [JumpButton setTitle:[NSString stringWithFormat:@"跳过 %d",i] forState:UIControlStateNormal];
    if (i == 0)
    {
        [timer invalidate];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TabBarViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
       [self presentViewController:vc animated:YES completion:nil];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    i = 3;
    JumpButton.layer.cornerRadius = 4.0;
    JumpButton.layer.masksToBounds = YES;
    [JumpButton setTitle:[NSString stringWithFormat:@"跳过 %d",i] forState:UIControlStateNormal];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(gotoFirstView) userInfo:nil repeats:YES];
    
    UITapGestureRecognizer *tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotohaha)];
    
    [myimageview addGestureRecognizer:tap];
    
    
}
- (void)gotohaha
{
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    AdvertWebController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvertWebController"];
//    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    vc.urlStr1 = @"https://get.uber.com.cn/invite/%E5%85%A8%E7%BD%91%E8%B6%B3%E7%90%83";
//    [self presentViewController:vc animated:YES completion:nil];
   // 一般用于跳到外部链接
    NSString *strUrl = @"https://get.uber.com.cn/invite/%E5%85%A8%E7%BD%91%E8%B6%B3%E7%90%83";
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString:strUrl]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}



@end
