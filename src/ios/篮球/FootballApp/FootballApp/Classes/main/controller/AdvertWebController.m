//
//  AdvertWebController.m
//  FootballApp
//
//  Created by zj on 16/6/20.
//  Copyright © 2016年 North. All rights reserved.
//

#import "AdvertWebController.h"

@interface AdvertWebController ()
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@end

@implementation AdvertWebController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
//    NSString *urlStr1 = @"https://get.uber.com.cn/invite/%E5%85%A8%E7%BD%91%E8%B6%B3%E7%90%83";
    NSLog(@"%@",self.urlStr1);
    NSURL *url = [NSURL URLWithString:self.urlStr1];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
