//
//  AdvertPageView.h
//  advertpopView
//
//  Created by zj on 16/6/22.
//  Copyright © 2016年 zj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvertPageView : UIView
{
    UIView *myView;
    UIButton *hideBtn;
    NSTimer *timer;
    int i ;
    NSString *strurls;
}
- (id)initViewImage:(NSString *)str And:(NSString *)strurl;
- (void)show;
@end
