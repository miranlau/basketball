//
//  AdvertPageView.m
//  advertpopView
//
//  Created by zj on 16/6/22.
//  Copyright © 2016年 zj. All rights reserved.
//

#import "AdvertPageView.h"
#import "AppDelegate.h"
@implementation AdvertPageView

- (id)initViewImage:(NSString *)str And:(NSString *)strurl
{
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        
        UIImageView *ImageView=[[UIImageView alloc]initWithFrame:self.frame];
        
        
         __weak typeof (UIImageView)*weakImageView = ImageView;
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
        [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];

//        [ImageView setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
        
        [ImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            weakImageView.image=image;
            weakImageView.contentMode=UIViewContentModeScaleAspectFill;
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
        
        
        
        
        ImageView.userInteractionEnabled = YES;
          UITapGestureRecognizer *tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoAdvertpage)];
        [ImageView addGestureRecognizer:tap];
        
        [self addSubview:ImageView];
        i = 5;
        strurls = strurl;
        
        UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(kScreenWidth-70, 15, 60, 25)];
        backView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:0.7];
        backView.layer.cornerRadius = 4.0;
        backView.layer.masksToBounds = YES;
        [self addSubview:backView];
        
        hideBtn=[[UIButton alloc]initWithFrame:CGRectMake(5, 0, 50, 25)];
        [hideBtn setTitle:[NSString stringWithFormat:@"跳过 %d",i] forState:UIControlStateNormal];
        [hideBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        hideBtn.titleLabel.font = [UIFont systemFontOfSize:11];
       
        hideBtn.backgroundColor = [UIColor clearColor];
        [hideBtn addTarget:self action:@selector(gotohomeview) forControlEvents:UIControlEventTouchUpInside];
        
        
         timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(gotoFirstViews) userInfo:nil repeats:YES];
        
        [backView addSubview:hideBtn];
        
    }
    [self layoutIfNeeded];
    
    [self addSubview:myView];
    return self;
}
-(void)show{
    
    if([UIApplication sharedApplication].keyWindow){
        
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        
    }else{
        
        //程序运行没有效果可能是keyWindow还没有创建
        AppDelegate *delegate=[UIApplication sharedApplication].delegate;
        [delegate.window addSubview:self];
        
    }
}

-(void)gotohomeview
{
     [timer invalidate];
    [self removeFromSuperview];
    
    
}
- (void)gotoAdvertpage
{
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString:strurls]];
  
}

- (void)gotoFirstViews
{
    i = i-1;
    [hideBtn setTitle:[NSString stringWithFormat:@"跳过 %d",i] forState:UIControlStateNormal];
    if (i == 0)
    {
        [timer invalidate];
        [self removeFromSuperview];
    }
}
@end
