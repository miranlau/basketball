//
//  GuidePageView.h
//  CAD-plantform
//
//  Created by MagicBeans2 on 16/3/10.
//  Copyright © 2016年 Hogan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuidePageView : UIView
{

      UIScrollView *scrolleView;

}
-(id)initView;
-(void)show;
@end
