//
//  GuidePageView.m
//  CAD-plantform
//
//  Created by MagicBeans2 on 16/3/10.
//  Copyright © 2016年 Hogan. All rights reserved.
//

#import "GuidePageView.h"
#import "MyPageView.h"
#import "AppDelegate.h"
@implementation GuidePageView

-(id)initView{

     self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
    if (self) {
        self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);

        MyPageView *pageView=[[MyPageView alloc]initWithFrame:self.frame];
        pageView.imageList=@[@"引导页1.png",@"引导页2.png",@"引导页3.png"];
        [self addSubview:pageView];

        UIButton *hideBtn=[[UIButton alloc]initWithFrame:CGRectMake((kScreenWidth-120)/2, kScreenHeight-60, 120, 30)];
        
        [hideBtn setImage:[UIImage imageNamed:@"体验.png"] forState:0];
//        [hideBtn setBackgroundColor:[UIColor redColor]];
        [hideBtn addTarget:self action:@selector(Hide:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:hideBtn];
        
    }
     [self layoutIfNeeded];
    
    [self addSubview:scrolleView];
    return self;
}
-(void)show{
    
    if([UIApplication sharedApplication].keyWindow){
    
        [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    }else{
        
        AppDelegate *delegate=[UIApplication sharedApplication].delegate;
        [delegate.window addSubview:self];
    
    }
}

-(void)Hide:(id)sender{

    [self removeFromSuperview];

    
}
@end
