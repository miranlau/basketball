//
//  MyPageView.h
//  AutoTest
//
//  Created by yangtao on 15/4/11.
//  Copyright (c) 2015年 yangtao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MyPageView : UIView<UIScrollViewDelegate>
@property(retain,nonatomic)UIScrollView *pageSc;
@property (retain,nonatomic)UIPageControl *pageControl;
@property (assign)int nowPage;
@property(readwrite,nonatomic)BOOL isAllrowClick;
@property(readwrite,nonatomic)BOOL isIndex;
@property(retain,nonatomic,setter=setimageList:)NSArray *imageList;
@end


