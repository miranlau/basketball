//
//  MyPageView.m
//  AutoTest
//
//  Created by yangtao on 15/4/11.
//  Copyright (c) 2015年 yangtao. All rights reserved.
//

#import "MyPageView.h"
@implementation MyPageView
@synthesize imageList;
-(id)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
     
        self.pageSc =[[UIScrollView alloc]initWithFrame:frame];
      
        self.pageSc.pagingEnabled=YES;
      
        self.pageSc.delegate=self;
        [self addSubview:self.pageSc];
        
        
        UIPageControl *pagec=[[UIPageControl alloc]initWithFrame:CGRectMake(frame.size.width/4, frame.size.height-100, frame.size.width/2, 30)];
        self.pageControl=pagec;
//        self.pageControl.backgroundColor = [UIColor redColor];
        self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        self.pageControl.alpha = 0.6;
        self.pageControl.currentPageIndicatorTintColor = [UIColor darkGrayColor];
        [self.pageControl addTarget:self action:@selector(pageControlValueChange:) forControlEvents:UIControlEventValueChanged];
        
        [self addSubview:self.pageControl];
        
     
        
        NSTimer *timer=[NSTimer timerWithTimeInterval:2.0f target:self selector:@selector(pageNumberChanged) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop]addTimer:timer forMode:NSDefaultRunLoopMode];
        //要想在scrollView滚动的同时也接受其他runloop的消息，则需要改变两者之间的RunLoopMode
    }
    return self;
}
-(void)setimageList:(NSArray *)list{
    if ([list count]>0) {
        imageList=list;
        
    }
    [self addPageView:list];
}
-(void)addPageView:(NSArray *)lsit{

    for (UIView *view in [self.pageSc subviews]) {
        [view removeFromSuperview];
        
    }
    
    self.pageSc.contentSize=CGSizeMake([lsit count]*self.pageSc.frame.size.width, self.pageSc.frame.size.height);
    self.pageControl.numberOfPages=[lsit count];
    self.pageControl.currentPage = 0;
    self.nowPage=0;
    if ([lsit count]>0) {
        self.pageControl.hidden=NO;
        
        for (int i=0;i<[lsit count];i++) {
            UIView * pv=[[UIView alloc] initWithFrame:CGRectMake(i*self.pageSc.frame.size.width, 0, self.pageSc.frame.size.width, self.pageSc.frame.size.height)];
        
        
            UIImageView *pageImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.pageSc.frame.size.width, self.pageSc.frame.size.height)];
           
            pageImage.image=[UIImage imageNamed:lsit[i]];
            
            [pv addSubview:pageImage];

            [self.pageSc addSubview:pv];
           
            
        }
        

    }else{
    
            self.pageControl.hidden=YES;
    
    
    }
    
    

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.pageControl.currentPage = page;
    self.nowPage=page;
}
-(void)pageControlValueChange:(id)sender{
    
     UIPageControl* control = (UIPageControl*)sender;
     int page = (int)control.currentPage;
    self.nowPage=page;


}
//下面不让自动循环翻页，故而注释了
-(void)pageNumberChanged{
//    self.nowPage++;
//    
//    if (self.nowPage==[imageList count]) {
//        self.nowPage=0;
//    }
//    
//    self.pageControl.currentPage=self.nowPage;
//     [self.pageSc setContentOffset:CGPointMake(self.nowPage *self.pageSc.frame.size.width, 0) animated:YES];
//


}

@end
