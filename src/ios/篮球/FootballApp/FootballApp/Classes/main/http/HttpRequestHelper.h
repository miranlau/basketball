//
//  HttpRequestHelper.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/4.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoadingView.h"
@interface HttpRequestHelper : NSObject
/**
 *  发送一个post请求
 *
 *  @param url     请求路径
 *  @param params  请求参数
 *  @param success 请求成功后的回调
 *  @param failure 请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(id)params success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure;


/**
 *  发送一个post请求,带上传文件
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param formDataArray 保存（多个）文件数据的数组
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params formDataArray:(NSArray *)formDataArray success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure;
/**
 *  发送一个post请求,带进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure;
/**
 *  发送一个post请求,带进文件和进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param formDataArray 保存（多个）文件数据的数组
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params formDataArray:(NSArray *)formDataArray progress:(void(^)(NSProgress *progress))progress success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure;

/**
 *  发送一个get请求
 *
 *  @param url     请求路径
 *  @param params  请求参数
 *  @param success 请求成功后的回调
 *  @param failure 请求失败后的回调
 */
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure;

/**
 *  发送一个get请求,带进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure;

@end
