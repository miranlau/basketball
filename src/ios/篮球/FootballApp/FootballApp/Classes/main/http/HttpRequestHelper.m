//
//  HttpRequestHelper.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/4.
//  Copyright © 2016年 North. All rights reserved.
//

#import "HttpRequestHelper.h"
#import <AFNetworking.h>
#import "XHToast.h"
@implementation HttpRequestHelper
/**
 *  发送一个post请求
 *
 *  @param url     请求路径
 *  @param params  请求参数
 *  @param success 请求成功后的回调
 *  @param failure 请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(id)params success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure{

    
   if ([AccountHelper shareAccount].isShow==NO) {
    
         [[LoadingView shareView]start];

       
   }else{
   
       [AccountHelper shareAccount].isShow=NO;
   }

  
    
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
   
    if ([AccountHelper shareAccount].isLogin==YES) {
        
        [manager.requestSerializer setValue:[AccountHelper shareAccount].token forHTTPHeaderField:@"token"];
    }
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       

        if([responseObject[@"code"] intValue]==3005){
        
            [[NSNotificationCenter defaultCenter] postNotificationName:@"matchChange" object:nil];
        
        
        }
        
        
        
        if ([responseObject[@"flag"] intValue]==0&&[responseObject[@"code"] intValue]==0) {
         
            success(YES,responseObject[@"data"]);
       
             [[LoadingView shareView]stop];
  
        }else{
            [[LoadingView shareView]stop];
            success(NO,responseObject[@"msg"]);
            
        }

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        failure(error);
        
       
        
        [[LoadingView shareView]stop];

//        [XHToast showBottomWithText:@"网络连接错误,请检查你的网络"];
        
    }];
    

}


/**
 *  发送一个post请求,带上传文件
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param formDataArray 保存（多个）文件数据的数组
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params formDataArray:(NSArray *)formDataArray success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure{

    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
 
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (UIImage *image in formDataArray) {
            
            [formData appendPartWithFileData:UIImagePNGRepresentation(image) name:@"files" fileName:@"imageFile.png" mimeType:@"image/png"];
        
        }

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        

        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        if ([responseObject[@"flag"] intValue]==0){
            
            success(YES,responseObject[@"data"]);
            
        }else{
            
         success(NO,responseObject[@"msg"]);
        
        }
   
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
          [SVProgressHUD showErrorWithStatus:@"网络连接错误,请检查你的网络"];
        
        failure(error);
        
    }];



}
/**
 *  发送一个post请求,带进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure{



    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    
    [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
        progress(uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        if ([responseObject[@"flag"] intValue]==0){
            
            success(YES,responseObject[@"data"]);
            
        }else{
            
            success(NO,responseObject[@"msg"]);
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [SVProgressHUD showErrorWithStatus:@"网络连接错误,请检查你的网络"];
        failure(error);
        
    }];






}
/**
 *  发送一个post请求,带进文件和进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param formDataArray 保存（多个）文件数据的数组
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params formDataArray:(NSArray *)formDataArray progress:(void(^)(NSProgress *progress))progress success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure{


    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        progress(uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(error);
        
    }];
    
}

/**
 *  发送一个get请求
 *
 *  @param url     请求路径
 *  @param params  请求参数
 *  @param success 请求成功后的回调
 *  @param failure 请求失败后的回调
 */
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure{

    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    
    if ([AccountHelper shareAccount].isLogin==YES) {
        [manager.requestSerializer setValue:[AccountHelper shareAccount].token forHTTPHeaderField:@"token"];
    }
    
    [manager GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success");
        
        
        if ([responseObject[@"flag"] intValue]==0&&[responseObject[@"code"] intValue]==0) {
            
            success(YES,responseObject[@"data"]);
            
        }else{
            
            success(NO,responseObject[@"msg"]);
            
        }

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
/**
 *  发送一个get请求,带上传文件
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param formDataArray 保存（多个）文件数据的数组
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */

/**
 *  发送一个get请求,带进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(BOOL sucess,id responseObject))success failure:(void(^)(NSError *error))failure{


    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    [manager GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        progress(downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];



}
/**
 *  发送一个get请求,带进文件和进度条
 *
 *  @param url           请求路径
 *  @param params        请求参数
 *  @param formDataArray 保存（多个）文件数据的数组
 *  @param progress      百分比
 *  @param success       请求成功后的回调
 *  @param failure       请求失败后的回调
 */

@end
