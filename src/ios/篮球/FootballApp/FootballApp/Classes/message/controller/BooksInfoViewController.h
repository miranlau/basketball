//
//  BooksInfoViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface BooksInfoViewController : BaseViewController
@property(strong,nonatomic)NSMutableDictionary *teamDic;
@property(strong ,nonatomic)NSString *teamId;

@end
