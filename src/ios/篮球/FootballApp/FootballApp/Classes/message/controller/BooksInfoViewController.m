//
//  BooksInfoViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BooksInfoViewController.h"
#import "XHToast.h"
@interface BooksInfoViewController ()
{

    __weak IBOutlet NSLayoutConstraint *button;
    __weak IBOutlet UITableView *booksTableView;
    NSMutableDictionary *rowDic;
    
    NSString *total;
    NSString *averge;
    
    NSMutableArray *payArr;
    
    
    
}
@end

@implementation BooksInfoViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableView];
    
    self.navigationItem.title=@"账单";
    
    
    
   
    
    [self loadNitWorkDate];
    
    
}
-(void)loadNitWorkDate{
    NSString *matchId=@"";
    
    if ([self.teamDic objectForKey:@"id"]){
        matchId=self.teamDic[@"id"];
}else{
        matchId=self.teamDic[@"mid"];

}
    
  

    [HttpRequestHelper postWithURL:Url(@"team/getBillList") params:@{@"matchId":self.teamDic [@"id"],@"teamId":_teamId}success:^(BOOL sucess, id responseObject) {
        
        self.teamDic=responseObject;
        rowDic=[self witchCellForRow:0];
        
        [booksTableView reloadData];
        
        
        if ([_teamDic[@"leader"] intValue]==1) {
            
            button.constant=0;
            
        }else{
            
            button.constant=-45;
        }
        
        payArr=[NSMutableArray array];
        
        for (NSDictionary  *dic  in self.teamDic[@"billPayDtos"]) {
            if ([dic[@"ispay"] intValue]==1) {
                [payArr addObject:dic[@"id"]];
            }
            
        }
    } failure:^(NSError *error) {
        
        
        
    }];


}
-(void)loadTableView{
    
    booksTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    booksTableView.rowHeight=UITableViewAutomaticDimension;
    booksTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]init];
    bgImageView.image=BGIMG;
    booksTableView.backgroundView=bgImageView;
    
    
    
    
}
-(IBAction)saveBtnClick:(id)sender{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    [dic setObject:self.teamDic[@"mid"] forKey:@"mid"];
    [dic setObject:_teamId forKey:@"tid"];
    [dic setValue:total forKey:@"total"];
    [dic setValue:averge forKey:@"averge"];
    
    NSString *paids=nil;

    for(NSString *dtring in payArr){
        
        if (!paids) {
            paids=dtring;
        }else{
            paids=[NSString stringWithFormat:@"%@&playerIds=%@",paids,dtring];
        }

    }
    
    if ([payArr count]==0) {
        paids=@"";
    }
    
    [dic setValue:paids forKey:@"playerIds"];
    
    
    NSString *url=[NSString stringWithFormat:@"%@?tid=%@&mid=%@&playerIds=%@",Url(@"team/saveBill"),_teamId,self.teamDic[@"mid"],paids];
    
    if (total) {
        url=[NSString stringWithFormat:@"%@&total=%@&averge=%@",url,total,averge];
    }
    
    
    
    
    [HttpRequestHelper postWithURL:url params:nil success:^(BOOL sucess, id responseObject) {
        
        if(sucess){
            
            if (self.cblock) {
                self.cblock(@"");
            }
            
            
            [self.navigationController popViewControllerAnimated:YES];
        
        }else{
        
           [XHToast showBottomWithText:responseObject];
        
        }
        
        
    } failure:^(NSError *error) {
        
        
        
    }];
    
    
    

}
-(NSMutableDictionary *)witchCellForRow:(NSInteger)tag{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    
    
    [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                       
                                                       [BaseCellModel setModel:@"" key:@"" value:_teamDic image:@"" reuseIdentifier:@"Books1Cell"],
                                                       
                                                       ]];
    
    
    
    [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                       
                                                       
                                                       [BaseCellModel setModel:self.teamDic[@"leader"] key:self.teamDic[@"leaderId"]value:_teamDic[@"billPayDtos"] image:@"" reuseIdentifier:@"TeamMenber2Cell"]
                                                       
                                                       
                                                       ]];
    
    
    
    
    
    
    
    
    
    
    
    return dic;
    
}
#pragma mark tableView 委托协议

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return [[rowDic allKeys]count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view=[[UIView alloc]init];
    view.backgroundColor=[UIColor clearColor];
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    //回调事件
    
    
    
    if (indexPath.row%2) {
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    
    [cell setValue:model forKey:@"data"];
    
    [cell setCellBlock:^(id data) {
        
        NSString *string=(NSString *)data;
        
        if ([payArr containsObject:string]) {
            [payArr removeObject:string];
        }else{
            [payArr addObject:string];
        }
        
        
        
        
    }];
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    
    if ([model.reuseIdentifier isEqualToString:@"Books1Cell"]) {
        
        
        if ([_teamDic[@"leader"] intValue]==1) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"EditBooksViewController"];
            
            [controller setControllerBlock:^(id data) {
                
            
                NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithDictionary:self.teamDic];
                
                [dic setObject:data[@"total"] forKey:@"total"];
                [dic setObject:data[@"averge"] forKey:@"averge"];
                
                
                total=data[@"total"];
                averge=data[@"averge"];
                self.teamDic=dic;
                
                
                rowDic=[self witchCellForRow:0];
                
                [booksTableView reloadData];
               
                
            }];
            
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        
        
       
    }
    
    
}

@end
