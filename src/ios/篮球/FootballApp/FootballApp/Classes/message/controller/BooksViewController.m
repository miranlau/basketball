//
//  BooksViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BooksViewController.h"

@interface BooksViewController ()
{

    __weak IBOutlet UITableView *booksTableView;
    
    
}
@end

@implementation BooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"账单";
    
     [self loadTableView];
    
    [self  loadNetWorkData];
}



-(void)loadNetWorkData{
    
    [self.dataArr removeAllObjects];
    
    [HttpRequestHelper postWithURL:Url(@"team/getDoneMatch") params:@{@"teamId":_teamId} success:^(BOOL sucess, id responseObject) {
        
        [self.dataArr addObjectsFromArray:responseObject];
        [booksTableView reloadData];
        
    } failure:^(NSError *error) {
        
        
        
    }];
    


}


-(void)loadTableView{
    
    booksTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    booksTableView.rowHeight=UITableViewAutomaticDimension;
    booksTableView.estimatedRowHeight=100;
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    booksTableView.backgroundView=bgImageView;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"BooksCell" forIndexPath:indexPath];
    cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
    
    
    [cell setValue:self.dataArr[indexPath.row] forKey:@"data"];
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"BooksInfoViewController"];
    
    [controller setValue:self.dataArr[indexPath.row] forKey:@"teamDic"];
    
    [controller setValue:_teamId forKey:@"teamId"];
    
    [controller setControllerBlock:^(id data) {
        [self  loadNetWorkData];
    }];
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
@end
