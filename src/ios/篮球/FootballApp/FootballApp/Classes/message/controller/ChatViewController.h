//
//  ChatViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface ChatViewController : BaseViewController
@property(retain,nonatomic)NSString *aConversationId;//会话id
@property(retain,nonatomic)NSString *teamId;
@end
