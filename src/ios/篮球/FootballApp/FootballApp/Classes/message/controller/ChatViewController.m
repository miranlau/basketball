//
//  ChatViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChatViewController.h"
#import "UUMessageCell.h"
#import "UUMessageFrame.h"
#import "UUInputFunctionView.h"
#import "UUMessage.h"
#import <IQKeyboardManager.h>
#import <HyphenateSDK/EMSDK.h>
#import "EMVoiceConverter.h"
#import "ChatHelper.h"
@interface ChatViewController ()<UUMessageCellDelegate,UUInputFunctionViewDelegate>
{
    __weak IBOutlet UITableView *chatTableView;
    
     UUInputFunctionView *inputView;
    __weak IBOutlet NSLayoutConstraint *bottom;
    EMConversation *conversation;//当前会话
    EMMessage *lastMessage;//最早一条消息

}
@end

@implementation ChatViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;

    
    
    
    
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tableViewScrollToBottom) name:UIKeyboardDidShowNotification object:nil];
}
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:NO];
    
    [chatTableView reloadData];
    [self tableViewScrollToBottom];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *right=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"账本.png"] style:UIBarButtonItemStylePlain target:self action:@selector(booksBtnClick:)];
    
    
    self.navigationItem.rightBarButtonItem=right;
    
    [self loadTableView];
    
    [self loadMessageList];
    
    
    
    
}
//加载会话消息
-(void)loadMessageList{
    

    //获取本会话
   conversation =[[EMClient sharedClient].chatManager getConversation:_aConversationId type:EMConversationTypeGroupChat createIfNotExist:YES];
    
    
    //有未读消息才设置
    if([conversation unreadMessagesCount]>0){
        
        [conversation markAllMessagesAsRead];//设置所有消息已读
        //防止数据库未保存
        [self performSelector:@selector(postMessageUnreadChange) withObject:nil afterDelay:1];
        
        
    }
    
    //本会话收到新消息
    [[ChatHelper chatService] reciveNewMessageForConversation:_aConversationId newMessageBlock:^(id data) {
        
        [self reciveNewMessage:data];
        
    }];

    
    
        [self loadLastMessage];
   
    
    
        [self tableViewScrollToBottom];
    
    
    

}
-(void)reciveNewMessage:(EMMessage *)message{

    [self.dataArr addObject:[self EsModelExchangeUUMessage:message]];
    
    [chatTableView reloadData];
    
    
    [self tableViewScrollToBottom];

}
-(void)postMessageUnreadChange{
    
    if (self.cblock){
        
        self.cblock(@"");
        
    }
    
    

}
-(void)loadLastMessage{
    
    if(!lastMessage){
        
     lastMessage= [conversation latestMessage];
        
        if (lastMessage) {
            
            [self.dataArr addObject:[self EsModelExchangeUUMessage:lastMessage]];
        }
        
    }
    //获取
    NSArray *messageList= [conversation loadMoreMessagesContain:nil before:lastMessage.timestamp limit:10 from:nil direction:EMMessageSearchDirectionUp];
    
    
    
    if ([messageList count]>0) {
         lastMessage=messageList[0];//加载王城更改最早的消息
    }
    
   
    
    NSMutableArray *array=[NSMutableArray array];
    
    for(EMMessage *message in messageList){
        
        [array addObject:[self EsModelExchangeUUMessage:message]];
  
    }

    //插入到最前面
    if ([array count]>0) {
        
        
        NSMutableArray *arr2=[NSMutableArray arrayWithArray:array];
        
        [arr2 addObjectsFromArray:self.dataArr];
        
        
        self.dataArr=arr2;
    }
    
    [chatTableView reloadData];
    
    

}




-(void)booksBtnClick:(id)sender{
    
    
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"BooksViewController"];
    [controller setValue:_teamId forKey:@"_teamId"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
-(void)loadTableView{
    
    chatTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    chatTableView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    chatTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
       
        [self loadLastMessage];
        [chatTableView.mj_header endRefreshing];
    }];
    
    inputView = [[UUInputFunctionView alloc]initWithSuperVC:self];
    inputView.delegate = self;
    [self.view addSubview:inputView];
    
    
}
- (void)tableViewScrollToBottom
{
    if ([self.dataArr count]==0){
        return;
    }
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.dataArr.count-1 inSection:0];
    [chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

   
    
}
-(void)keyboardChange:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    //adjust ChatTableView's height
    if (notification.name == UIKeyboardWillShowNotification) {
        bottom.constant = keyboardEndFrame.size.height+40;
    }else{
        bottom.constant = 40;
    }
    
    [self.view layoutIfNeeded];
    
    //adjust UUInputFunctionView's originPoint
    CGRect newFrame = inputView.frame;
    newFrame.origin.y = keyboardEndFrame.origin.y - newFrame.size.height-64;
    inputView.frame = newFrame;
    
    [UIView commitAnimations];
    
}
#pragma mark - InputFunctionViewDelegate
- (void)UUInputFunctionView:(UUInputFunctionView *)funcView sendMessage:(NSString *)message
{
    
    UUMessage *uMessage=[[UUMessage alloc]init];
    uMessage.type=UUMessageTypeText;
    uMessage.from=UUMessageFromMe;
    uMessage.strName=@"";
    uMessage.strContent=message;
    uMessage.strIcon=[AccountHelper shareAccount].player.avatar;
    uMessage.strTime=[NSDate date].description;
    
    UUMessageFrame *messageFrame=[[UUMessageFrame alloc]init];
     messageFrame.message=uMessage;
    [uMessage minuteOffSetStart:previousTime end:[NSDate date].description];
    messageFrame.showTime = uMessage.showDateLabel;
    [self.dataArr addObject:messageFrame];
    
    [chatTableView reloadData];
    
    [self tableViewScrollToBottom];

    funcView.TextViewInput.text = @"";
    [funcView changeSendBtnWithPhoto:YES];
    
    
    
    
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:message];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    //生成Message
    EMMessage *emessage = [[EMMessage alloc] initWithConversationID:_aConversationId from:from to:_aConversationId body:body ext:nil];
    emessage.chatType = EMChatTypeGroupChat;// 设置为单聊消息
    
    
    NSString *name=[AccountHelper shareAccount].player.name;
    
//    if ([AccountHelper shareAccount].player.realName) {
//        name=[AccountHelper shareAccount].player.realName;
//    }
    
    emessage.ext=@{@"avatar":[AccountHelper shareAccount].player.avatar,@"userName":name,@"id":[AccountHelper shareAccount].player.id};
    
    emessage.isRead=YES;
    [[EMClient sharedClient].chatManager asyncSendMessage:emessage progress:^(int progress) {
      
     
        
    } completion:^(EMMessage *sssmessage, EMError *error) {
        
           [self postMessageUnreadChange];
   
    }];
    
    
    
   
}

- (void)UUInputFunctionView:(UUInputFunctionView *)funcView sendPicture:(UIImage *)image
{
   
    
    
    UUMessage *uMessage=[[UUMessage alloc]init];
    uMessage.type=UUMessageTypePicture;
    uMessage.from=UUMessageFromMe;
    uMessage.strName=@"";
    uMessage.picture=image;
    uMessage.strIcon=[AccountHelper shareAccount].player.avatar;
    uMessage.strTime=[NSDate date].description;
    
    UUMessageFrame *messageFrame=[[UUMessageFrame alloc]init];
    messageFrame.message=uMessage;
    [uMessage minuteOffSetStart:previousTime end:[NSDate date].description];
    messageFrame.showTime = uMessage.showDateLabel;
    [self.dataArr addObject:messageFrame];
    
    [chatTableView reloadData];
    
    [self tableViewScrollToBottom];
    
    
    EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:UIImagePNGRepresentation(image) displayName:@"image.png"];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    //生成Message
    EMMessage *eMessage = [[EMMessage alloc] initWithConversationID:_aConversationId from:from to:_aConversationId body:body ext:nil];
  
    eMessage.chatType = EMChatTypeGroupChat;// 设置为群聊消息
    NSString *name=[AccountHelper shareAccount].player.name;
    
//    if ([AccountHelper shareAccount].player.realName) {
//        name=[AccountHelper shareAccount].player.realName;
//    }
    eMessage.isRead=YES;
    eMessage.ext=@{@"avatar":[AccountHelper shareAccount].player.avatar,@"userName":name,@"id":[AccountHelper shareAccount].player.id};
    [[EMClient sharedClient].chatManager asyncSendMessage:eMessage progress:^(int progress) {
    
        
    } completion:^(EMMessage *message, EMError *error) {
          [self postMessageUnreadChange];
    }];
}

- (void)UUInputFunctionView:(UUInputFunctionView *)funcView sendVoice:(NSData *)voice time:(NSInteger)second
{
   
    
    
    UUMessage *uMessage=[[UUMessage alloc]init];
    uMessage.type=UUMessageTypeVoice;
    uMessage.from=UUMessageFromMe;
   
    uMessage.strName=@"";
    uMessage.voice=voice;
    uMessage.strIcon=[AccountHelper shareAccount].player.avatar;
    uMessage.strTime=[NSDate date].description;
    uMessage.strVoiceTime=[NSString stringWithFormat:@"%d",(int)second];
    
    UUMessageFrame *messageFrame=[[UUMessageFrame alloc]init];
    messageFrame.message=uMessage;
    [uMessage minuteOffSetStart:previousTime end:[NSDate date].description];
    messageFrame.showTime = uMessage.showDateLabel;
    [self.dataArr addObject:messageFrame];
    
    [chatTableView reloadData];
    
    [self tableViewScrollToBottom];

    
    NSString *uuidString = [[NSUUID UUID] UUIDString];
    NSString *amrFilePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/tmp/%@.amr",uuidString]];
    NSString *wavFilePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/tmp/%@.wav",uuidString]];
    
    /**
     *  将amr转换为wav格式
     */
    [voice writeToFile:wavFilePath atomically:NO];
    [EMVoiceConverter wavToAmr:wavFilePath amrSavePath:amrFilePath];
    NSData *voiceData = [NSData dataWithContentsOfFile:amrFilePath];
    
    EMVoiceMessageBody *body = [[EMVoiceMessageBody alloc] initWithData:voiceData displayName:@"voice"];
    body.duration =(int)second;
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    // 生成message
    EMMessage *eMessage = [[EMMessage alloc] initWithConversationID:_aConversationId from:from to:_aConversationId body:body ext:nil];
   eMessage.isRead=YES;
    eMessage.chatType = EMChatTypeGroupChat;// 设置为群聊消息
    NSString *name=[AccountHelper shareAccount].player.name;
    
//    if ([AccountHelper shareAccount].player.realName) {
//        name=[AccountHelper shareAccount].player.realName;
//    }
    
    eMessage.ext=@{@"avatar":[AccountHelper shareAccount].player.avatar,@"userName":name,@"id":[AccountHelper shareAccount].player.id};
    [[EMClient sharedClient].chatManager asyncSendMessage:eMessage progress:^(int progress) {
        
        
    } completion:^(EMMessage *message, EMError *error) {
        
           [self postMessageUnreadChange];
        
        
        
    }];
    
}
#pragma mark 环信消息转换UUchat消息模型

-(UUMessageFrame *)EsModelExchangeUUMessage:(EMMessage *)eMessage{
    eMessage.isRead=YES;
    UUMessageFrame *messageFrame=[[UUMessageFrame alloc]init];
    
    UUMessage *uMessage=[[UUMessage alloc]init];
    EMMessageBody *msgBody=eMessage.body;
    
    
    if(msgBody.type == EMMessageBodyTypeText){
        uMessage.type=UUMessageTypeText;
        EMTextMessageBody * textBody=(EMTextMessageBody *)msgBody;
        uMessage.strContent=textBody.text;
    }
    
    if(msgBody.type == EMMessageBodyTypeImage){
        
        uMessage.type=UUMessageTypePicture;
        EMImageMessageBody *picBody=(EMImageMessageBody *)msgBody;
        
        
        if (picBody.remotePath) {
            
            uMessage.picture=(id)[NSURL URLWithString:picBody.remotePath];
            
        }else{
        
            uMessage.picture=(id)[NSURL fileURLWithPath:picBody.localPath];
        }
        
    }
    
    if(msgBody.type == EMMessageBodyTypeVoice){
        uMessage.type=UUMessageTypeVoice;
        
        EMVoiceMessageBody *voiceBody=(EMVoiceMessageBody *)msgBody;
        
        
        
        
        NSData *voiceData=nil;
        if (voiceBody.localPath) {
            
            
            voiceData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:voiceBody.localPath]];
            //如果本地为空
            if (!voiceData) {
                
                voiceData= [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:voiceBody.remotePath]] returningResponse:NULL error:NULL];
            }
            
        }
        else
        {
            
            
          voiceData= [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:voiceBody.remotePath]] returningResponse:NULL error:NULL];
        }

        
        NSString *uuidString = [[NSUUID UUID] UUIDString];
        
        NSString *amrFilePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/tmp/%@.amr",uuidString]];
        NSString *wavFilePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/tmp/%@.wav",uuidString]];
        
        /**
         *  将amr转换为wav格式
         */
        [voiceData writeToFile:amrFilePath atomically:NO];
        if(![EMVoiceConverter amrToWav:amrFilePath wavSavePath:wavFilePath])
        {
            uMessage.voice=[NSData dataWithContentsOfFile:wavFilePath];
            uMessage.strVoiceTime=[NSString stringWithFormat:@"%ld",(long)voiceBody.duration];
        }
        
        
        
        

        
    }
    
    double timeInterval = 0;
    if(eMessage.timestamp > 140000000000) {
        timeInterval = eMessage.timestamp / 1000;
    }
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    uMessage.strTime= [uMessage changeTheDateString:date];
   
    NSLog(@"%@",eMessage.ext);
    //如果消息发送方是我
    if (eMessage.direction==EMMessageDirectionSend) {
        uMessage.from=UUMessageFromMe;
        uMessage.strName=@"";
        uMessage.strIcon=[AccountHelper shareAccount].player.avatar;
        uMessage.strId=[AccountHelper shareAccount].player.id;
        
    }else{
        uMessage.from=UUMessageFromOther;
        uMessage.strName=eMessage.ext[@"userName"];
        uMessage.strIcon=eMessage.ext[@"avatar"];
        uMessage.strId=eMessage.ext[@"id"];
    }
    [uMessage minuteOffSetStart:previousTime end:date.description];
    messageFrame.showTime = uMessage.showDateLabel;

    messageFrame.message=uMessage;



    return messageFrame;
}
static NSString *previousTime = nil;

#pragma mark - tableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UUMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (cell == nil) {
        cell = [[UUMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
        cell.delegate = self;
    }
    [cell setMessageFrame:self.dataArr[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self.dataArr[indexPath.row] cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

#pragma mark - cellDelegate
- (void)headImageDidClick:(UUMessageCell *)cell userId:(NSString *)userId name:(NSString *)name{
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
    
    //如果是我自己
    if ([userId intValue]==MINEId) {
        controller.navigationItem.title=@"我的";
    }else{
        controller.navigationItem.title=name;
        [controller setValue:userId forKey:@"userId"];
    }
    [self.navigationController pushViewController:controller animated:YES];
    
}
@end
