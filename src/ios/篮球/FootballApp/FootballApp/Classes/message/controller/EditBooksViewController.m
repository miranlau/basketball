//
//  EditBooksViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "EditBooksViewController.h"

@interface EditBooksViewController ()
{
    __weak IBOutlet UITextField *total;
    __weak IBOutlet UITextField *averge;

}
@end

@implementation EditBooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
//    UIBarButtonItem *right=[[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnClick)];
//    
//    
//    self.navigationItem.rightBarButtonItem=right;
    
    
    self.navigationItem.title=@"账单";
    
}
-(IBAction)saveBtnClick:(id)sender{

    if ([total.text isEqualToString:@""]) {
        
        
         [SVProgressHUD showErrorWithStatus:@"请输入总金额"];
        
        return;
    }
    
    if ([averge.text isEqualToString:@""]) {

        [SVProgressHUD showErrorWithStatus:@"请输入每人应付"];
        
        return;
    }
    
    
    if(self.cblock){
    
        self.cblock(@{@"total":total.text,@"averge":averge.text});
        
        [self.navigationController popViewControllerAnimated:YES];
    
    }


}



@end
