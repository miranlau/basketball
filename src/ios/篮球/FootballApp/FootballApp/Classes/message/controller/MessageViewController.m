//
//  MessageViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageCell.h"
#import "messageTuishong.h"
#import "AppDelegate.h"

#import "messageTuishong.h"
#import "MessageViewController2.h"
#import "AvatarHelper.h"
#import <HyphenateSDK/EMSDK.h>
#import "UUMessage.h"
@interface MessageViewController ()
{

    __weak IBOutlet UITableView *msgTableView;
    NSMutableArray *messageArray;
    UIButton *leftImageView;

}
@property(nonatomic,strong)NSMutableArray *array0;
@property(nonatomic,strong)NSMutableArray *array1;
@property(nonatomic,strong)NSMutableArray *array2;
@property(nonatomic,strong)NSMutableArray *array3;
@end

@implementation MessageViewController

-(void)headImageChange{
    
    if([AccountHelper shareAccount].isLogin ==YES){

        [leftImageView setBackgroundImageForState:0 withURL:[NSURL URLWithString:[AccountHelper shareAccount].player.avatar] placeholderImage:HeadImage];
        [leftImageView setTitle:@"" forState:0];
    }else{
        [leftImageView setBackgroundImage:HeadImage forState:0];
        [leftImageView setTitle:@"未登录" forState:0];
        [leftImageView setTitleColor:[UIColor grayColor] forState:0];
        leftImageView.titleLabel.font=[UIFont systemFontOfSize:12];
        
    }
    
   
}
#pragma mark  获取会话列表
-(void)getChatList{
    
    
    //如果已经登录了
    if ([EMClient sharedClient].isLoggedIn != YES) {
         [self.dataArr removeAllObjects];
         [msgTableView reloadData];
         return;
        
    }
    
   
    [AccountHelper shareAccount].isShow=YES;
    
    
   
    
    
    
    
    [HttpRequestHelper postWithURL:Url(@"team/getChatData") params:@{} success:^(BOOL sucess, id responseObject) {
        
        [self.dataArr removeAllObjects];

        for (NSDictionary *dic in responseObject) {
            
            
            [self.dataArr addObject:dic];
        }
       
        
        [msgTableView reloadData];
        [msgTableView.mj_header endRefreshing];
        
    } failure:^(NSError *error) {
         [msgTableView.mj_header endRefreshing];
    }];
    

}
- (void)getHeadInfo
{
    leftImageView=[[UIButton alloc]initWithFrame:CGRectMake(-10, 0, 39, 39)];
    leftImageView.layer.cornerRadius=19.5;
    leftImageView.layer.masksToBounds=YES;
    leftImageView.userInteractionEnabled=YES;
    [leftImageView addTarget:self action:@selector(headImageViewClick) forControlEvents:UIControlEventTouchUpInside];
    [self headImageChange];
    
    
    //去除边线
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftImageView];
}
-(void)headImageViewClick{
    
    
    
    if ([AccountHelper shareAccount].isLogin==YES) {
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        controller.navigationItem.title=@"我的";
        [self.navigationController pushViewController:controller animated:YES];
    }else{
        
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getInfoMessage];
        [msgTableView reloadData];
  
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"pushnewmessage" object:nil];
    
   
    for (int i = 0; i<[AccountHelper shareAccount].tuisongArray.count ; i++)
    {
        messageTuishong *arInfo = [[messageTuishong alloc]init];
        arInfo = [AccountHelper shareAccount].tuisongArray[i];
      
    }
     [self loadTableView];
    if ([AccountHelper shareAccount].isLogin == NO) {
      
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
    }

    else
    {
    
        [self getInfoMessage];
   
    }
}
- (void)getInfoMessage
{
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    msgTableView.backgroundView=bgImageView;
    [self  getChatList];
    [self getHeadInfo];
    
    
    self.navigationItem.title = @"消息";
    messageArray = [NSMutableArray arrayWithCapacity:0];
    self.array0 = [NSMutableArray arrayWithCapacity:0];
    self.array1 = [NSMutableArray arrayWithCapacity:0];
    self.array2 = [NSMutableArray arrayWithCapacity:0];
    self.array3 = [NSMutableArray arrayWithCapacity:0];
    
    // 马上进入刷新状态
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getChatList) name:@"joinTeam" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getChatList) name:RECIVENEWMESSAGE object:nil];

    
    msgTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        
        [self loadNotiData];
        
    }];
    
    
    
    [msgTableView.mj_header beginRefreshing];
}
-(void)loadNotiData{

    // 进入刷新状态后会自动调用这个block
    self.array0 = [NSMutableArray arrayWithCapacity:0];
    self.array0 = [AccountHelper shareAccount].tuisongArray;
    
    if (self.array0.count > 0)
    {
        [self.array2 removeAllObjects];
        [self.array3 removeAllObjects];

        for(int i = 0;i < self.array0.count;i++)
        {
            messageTuishong *mod = self.array0[i];
            if ([mod.pushType intValue] == 0)
            {
                [self.array2 addObject:mod];
            }
            else
            {
                [self.array3 addObject:mod];
            }
            
        }
        
        [msgTableView reloadData];
    }
    else
    {
        [msgTableView reloadData];
    }
    
    [msgTableView.mj_header endRefreshing];
}
-(void)loadTableView{
    
    msgTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    msgTableView.rowHeight=UITableViewAutomaticDimension;
    msgTableView.estimatedRowHeight=100;
    [msgTableView.mj_header endRefreshing];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2 + [self.dataArr count];
    
    

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)
    {
        
        if (self.array2.count > 0 ) {
            messageTuishong *modle = self.array2[0];
            MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
            //这里只显示最新的一个
            [cell.myhead setImageWithURL:[NSURL URLWithString:modle.senderHeader] placeholderImage:HeadImage];
            cell.myhead.layer.cornerRadius = 37;
            cell.myhead.clipsToBounds = YES;
            cell.myname.text = modle.senderName;
            cell.mytime.text = modle.sendTime;
            cell.mycontent.text = modle.content;
            cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
            return cell;
            }
        else
        {
            MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
            cell.myhead.image = [UIImage imageNamed:@"消息-全网篮球默认图.png"];
            cell.myhead.contentMode=2;
            cell.myhead.layer.cornerRadius = 37;
            cell.myhead.clipsToBounds = YES;
            cell.myname.text = @"全网篮球";
            cell.mytime.text = @"";
            cell.mycontent.text = @"现在还没有人评论您的动态。";
            cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
            return cell;
        }
    }
    else {
    
        if (indexPath.row==1) {
            if (self.array3.count > 0 ) {
                
                messageTuishong *modle = self.array3[0];
                MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
                cell.myhead.image = [UIImage imageNamed:@"消息-赛场管家默认图.png"];
                cell.myhead.contentMode=2;
                cell.myhead.layer.cornerRadius = 37;
                cell.myhead.clipsToBounds = YES;
                cell.myname.text = @"赛事管家";
                if (modle.contentinfo)
                {
                    cell.mycontent.text = modle.contentinfo;
                }
                else
                {
                    cell.mycontent.text = @"您现在没有任何约赛信息";
                }
                if (modle.sendTime)
                {
                    cell.mytime.text = modle.sendTime;
                }
                else
                {
                    cell.mytime.text = @"";
                }
                
                
                cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
                return cell;
            }
            else
            {
                MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
                cell.myhead.image = [UIImage imageNamed:@"消息-赛场管家默认图.png"];
                cell.myhead.contentMode=2;
                cell.myhead.layer.cornerRadius = 37;
                cell.myhead.clipsToBounds = YES;
                cell.myname.text = @"赛事管家";
                cell.mycontent.text = @"您现在还没有任何约赛信息。";
                cell.mytime.text = @"";
                cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
                return cell;
            }
        }else{
            
            NSDictionary *dic =self.dataArr[indexPath.row-2];
            
          EMConversation * conversation =[[EMClient sharedClient].chatManager getConversation:dic[@"groupId"] type:EMConversationTypeGroupChat createIfNotExist:YES];
            
            
            EMMessage *lastMessage= [conversation latestMessage];
            
            
            
           
            MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell3" forIndexPath:indexPath];
            [cell.myhead setImageWithURL:[NSURL URLWithString:dic[@"logo"]] placeholderImage:TeamLogo];
            cell.myhead.layer.cornerRadius = 37;
            cell.myhead.clipsToBounds = YES;
            cell.myname.text = dic[@"teamName"];
            if (lastMessage) {
                if(lastMessage.body.type == EMMessageBodyTypeText){
                    EMTextMessageBody * textBody=(EMTextMessageBody *)lastMessage.body;
                    cell.mycontent.text =textBody.text;
                }
                
                if(lastMessage.body.type == EMMessageBodyTypeImage){
                    
                    cell.mycontent.text =@"[图片]";
                    
                }
                
                if(lastMessage.body.type == EMMessageBodyTypeVoice){
                    
                    cell.mycontent.text =@"[语音]";
                }
                
                
                double timeInterval = 0;
                if(lastMessage.timestamp > 140000000000) {
                    timeInterval = lastMessage.timestamp / 1000;
                }
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                
                UUMessage *message=[[UUMessage alloc]init];
                cell.mytime.text =  [message changeTheDateString:date];
                
            }else{
            
                cell.mycontent.text =@"暂时没有新消息";
                cell.mytime.text =@"";
            
            }
 
            if(conversation.unreadMessagesCount >0){
            cell.uredLabel.hidden=NO;
                cell.uredLabel.text=[NSString stringWithFormat:@"%d",conversation.unreadMessagesCount];
            
            }else{
                cell.uredLabel.hidden=YES;
            }
            
          
            cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
            
            
            return cell;
        }
    
       
    
    }
   


}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([AccountHelper shareAccount].isLogin == NO) {
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else
    {
        
        if (indexPath.row ==0)
        {

            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController2"];
        
            controller.navigationItem.title=@"动态,回复";
            [controller setValue:self.array2 forKey:@"messageArray"];
        
            [self.navigationController pushViewController:controller animated:YES];
        
        }
        else if (indexPath.row ==1)
        {

            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MessageViewController2"];
        
            controller.navigationItem.title=@"赛事管家";
            [controller setValue:self.array3 forKey:@"messageArray"];
        
            [self.navigationController pushViewController:controller animated:YES];
        
        }
        else
        {
        
            NSDictionary *dic =self.dataArr[indexPath.row-2];
        
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        
            [controller setValue:dic[@"groupId"] forKey:@"aConversationId"];
            [controller setValue:dic[@"teamId"] forKey:@"teamId"];
            [controller setControllerBlock:^(id data) {
            
                [tableView reloadData];
            
            }];
            controller.navigationItem.title=dic[@"teamName"];
            [self.navigationController pushViewController:controller animated:YES];
  
        }
    }

}

@end
