//
//  MessageViewController2.m
//  FootballApp
//
//  Created by zj on 16/6/8.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MessageViewController2.h"
#import "MessageCell.h"
#import "messageTuishong.h"
#import "AppDelegate.h"
#import "messageTuishong.h"
#import "DynamicDeatilController.h"

@interface MessageViewController2 ()
{
    
    __weak IBOutlet UITableView *msgTableView;
}
//
@end

@implementation MessageViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
   
    [self loadTableView];
    
   
    
}

-(void)loadTableView{
    
    msgTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    msgTableView.rowHeight=UITableViewAutomaticDimension;
    msgTableView.estimatedRowHeight=100;
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    msgTableView.backgroundView=bgImageView;
     [msgTableView reloadData];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _messageArray.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
    messageTuishong *modle = _messageArray[indexPath.row];

    if ([modle.pushType intValue] == 0)
    {
        MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
        
        [cell.myhead setImageWithURL:[NSURL URLWithString:modle.senderHeader] placeholderImage:HeadImage];
       
        cell.myhead.layer.cornerRadius = 37;
        cell.myhead.clipsToBounds = YES;
        cell.myname.text = modle.senderName;
        cell.mytime.text = modle.sendTime;
        cell.mycontent.text = modle.content;
        cell.mycontent.numberOfLines = 0;
        cell.mycontent.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
        return cell;
    }
    else
    {
        MessageCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
        
        cell.myhead.image = [UIImage imageNamed:@"申请加入.png"];
        cell.myhead.layer.cornerRadius = 37;
        cell.myhead.clipsToBounds = YES;
        cell.myname.text = @"赛场管家";//modle.senderName;
        cell.mytime.text = modle.sendTime;
        cell.mycontent.text = modle.content;
        cell.mycontent.numberOfLines = 0;
        cell.mycontent.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.contentView.backgroundColor=indexPath.row%2?BGCOLOR1:BGCOLOR2;
        return cell;
    }
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     messageTuishong *modle = _messageArray[indexPath.row];
    if ([modle.pushType intValue] == 0)
    {
  
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DynamicDeatilController* vc = [storyboard instantiateViewControllerWithIdentifier:@"DynamicDeatilController"];
    vc.dynamicId = [NSString stringWithFormat:@"%@",modle.dynamicId];
    
    [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [HttpRequestHelper postWithURL:Url(@"match/queryMatchStatus") params:@{@"matchId":modle.matchId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess)
            {
//                NSLog(@"resp = %@",responseObject);
                NSString *status = [NSString stringWithFormat:@"%@",responseObject];
                [self gotosomething:status And:modle.matchId];
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
        } failure:^(NSError *error) {
            
            
        }];
    }
}




- (void)gotosomething:(NSString *)status And:(NSString *)matchId
{
    //約赛中
    if ([status intValue]==0) {
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinWarViewController"];
        
        controller.navigationItem.title=@"约赛中";
        [controller setValue:matchId forKey:@"matchId"];
        [self.navigationController pushViewController:controller animated:YES];
        
        
        
    }
    //以匹配（赛前）
    if ([status intValue]==10) {
        
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        
        controller.navigationItem.title=@"赛前";
        [controller setValue:matchId forKey:@"matchId"];
        
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    //比赛中
    if ([status intValue]==20) {
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        
        controller.navigationItem.title=@"赛中";
        [controller setValue:matchId forKey:@"matchId"];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    //比赛结束
    if ([status intValue]==100) {
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
        controller.navigationItem.title=@"赛后";
        [controller setValue:matchId forKey:@"matchId"];
        [self.navigationController pushViewController:controller animated:YES];
    }


}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
