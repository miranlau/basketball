//
//  BaseButton.h
//  FootballApp
//
//  Created by North on 16/6/24.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseButton : UIButton

@property(assign,nonatomic) BOOL seleted;
@property(retain,nonatomic) UIImageView * seletedView;

@end
