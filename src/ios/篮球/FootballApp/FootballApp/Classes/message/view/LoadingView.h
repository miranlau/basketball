//
//  LoadingView.h
//  FootballApp
//
//  Created by 杨涛 on 16/6/7.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
{

    __weak IBOutlet NSLayoutConstraint *top;
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UIView *centerView;
    NSTimer *timer;

}
+(instancetype)shareView;
-(void)start;
-(void)stop;
@end
