//
//  LoadingView.m
//  FootballApp
//
//  Created by 杨涛 on 16/6/7.
//  Copyright © 2016年 North. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView
+(instancetype)shareView{
    
    static LoadingView *load=nil;
    
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        
        load = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] lastObject];
    
        load.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        [load layoutIfNeeded];
        
        
        
    });
    
    return load;

}
-(void)start{
    
    ViewRadius(centerView, 6.0);
    self.frame=CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [self layoutIfNeeded];

    [self  stop];
    
    top.constant=5;
    
    if (!timer) {
        timer =  [NSTimer scheduledTimerWithTimeInterval:.6 target:self selector:@selector(animationBegin) userInfo:nil repeats:YES];
    }
    [timer setFireDate:[NSDate date]];
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    
   

}

-(void)animationBegin{
    
    [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        top.constant=30;
        [centerView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            top.constant=5;
            [centerView layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            
        }];
    }];


}

-(void)stop{
    
    [timer setFireDate:[NSDate distantFuture]];
    
    [self removeFromSuperview];

    

}
@end
