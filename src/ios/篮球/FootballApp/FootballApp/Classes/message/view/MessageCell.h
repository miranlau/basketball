//
//  MessageCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/26.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

@interface MessageCell : BaseCell
@property (weak, nonatomic) IBOutlet UIImageView *myhead;
@property (weak, nonatomic) IBOutlet UILabel *myname;
@property (weak, nonatomic) IBOutlet UILabel *mytime;
@property (weak, nonatomic) IBOutlet UILabel *mycontent;
@property (weak, nonatomic) IBOutlet UILabel *uredLabel;

@end
