//
//  ChangePWdViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChangePWdViewController.h"

@interface ChangePWdViewController ()
{

    __weak IBOutlet UITextField *account;
    __weak IBOutlet UITextField *oldPwd;
    __weak IBOutlet UITextField *newPwd2;
    __weak IBOutlet UITextField *newPwd1;
}
@end

@implementation ChangePWdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     account.text=[AccountHelper shareAccount].account;
}


- (IBAction)submitBtnClick:(id)sender {
    
   
    
    if([oldPwd.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"请输入原密码"];
        
        return;
    }
    
    if([newPwd1.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"请输入新密码"];
        
        return;
    }
    
    if([newPwd2.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"请再次输入新密码"];
        
        return;
    }
    
    
    if(![newPwd1.text isEqualToString:newPwd2.text]){
        [SVProgressHUD showErrorWithStatus:@"两次新密码不一致，请重新输入"];
        return;
    }
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
//    [dic addEntriesFromDictionary:self.dic];
    [dic setValue:account.text forKey:@"mobile"];
    [dic setValue:oldPwd.text forKey:@"oldpsd"];
    [dic setValue:newPwd1.text forKey:@"newpsd"];

    [SVProgressHUD showWithStatus:@"修改中"];

    [HttpRequestHelper postWithURL:Url(@"user/modifyPassword") params:dic success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            [SVProgressHUD showSuccessWithStatus:@"修改成功"];
            
            //如果不是已经发布，自动填充密码
            
            [[AccountHelper shareAccount] saveAccount:account.text password:newPwd1.text];
            
            [self performSelector:@selector(regSucess) withObject:nil afterDelay:1.5];
            
            
            
        }else{
            
            
            
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
        
        
    } failure:^(NSError *error){
        
        
        
    }];
    
    
    
}

- (IBAction)forgetBtnClick:(id)sender {
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CheckNoViewController"];
    controller.navigationItem.title=@"忘记密码";
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

-(void)regSucess{
 
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
