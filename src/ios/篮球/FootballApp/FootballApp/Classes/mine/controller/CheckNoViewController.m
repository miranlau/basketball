//
//  CheckNoViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "CheckNoViewController.h"

@interface CheckNoViewController ()
{

    __weak IBOutlet UITextField *mobileTextFiled;

    __weak IBOutlet UITextField *codeNoTextFiled;
    __weak IBOutlet UIButton *checkBtn;
    
    NSTimer *timer;//倒计时
    
    NSInteger scends;//秒数
}

@property(nonatomic,copy)NSString *codeString;

@end

@implementation CheckNoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   [mobileTextFiled becomeFirstResponder];
    
    
}
//获取验证码

- (IBAction)getCodeNo:(id)sender {
    
    [self.view endEditing:YES];
    
    if([mobileTextFiled.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"手机号码不能为空"];
        
        return;
    }
    
    if(![RegularTools validateMobile:mobileTextFiled.text]){
    
     [SVProgressHUD showErrorWithStatus:@"手机号码错误"];
        return;
    
    }
    
    scends=60;
    timer=  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
   
    NSString *url=@"";
    if ([self.navigationItem.title isEqualToString:@"忘记密码"]) {
        url=@"user/getVerifyCode";
    }else{
    
        url=@"register/getVerifyCode";
    }
    
    [HttpRequestHelper postWithURL:Url(url) params:@{@"mobile":mobileTextFiled.text} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            [SVProgressHUD showSuccessWithStatus:@"验证码发送成功"];
            self.codeString = responseObject;
            //如果不是已经发布，自动填充密码
            if ([AccountHelper shareAccount].isPublic!=YES) {
                codeNoTextFiled.text=responseObject;//自动填充验证码
            }
            
            
        }else{
            
            [timer invalidate];
            checkBtn.userInteractionEnabled=YES;
            [checkBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        
            [SVProgressHUD showSuccessWithStatus:responseObject];
        }
        
        
        
    } failure:^(NSError *error){
        
        [timer invalidate];
        checkBtn.userInteractionEnabled=YES;
        [checkBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        
    }];
    
    
    
}






- (IBAction)nextBtnClick:(id)sender {
    
    if([mobileTextFiled.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"手机号码不能为空"];
        
        return;
    }
    
    if(![RegularTools validateMobile:mobileTextFiled.text]){
        
        [SVProgressHUD showErrorWithStatus:@"手机号码错误"];
        return;
        
    }
    
    if([codeNoTextFiled.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"请输入验证码"];
        
        return;
    }
    
    if(![codeNoTextFiled.text isEqualToString: self.codeString]){
        
        [SVProgressHUD showErrorWithStatus:@"验证码输入错误"];
        
        return;
    }
    
    if([self.navigationItem.title isEqualToString:@"注册"]||[self.navigationItem.title isEqualToString:@"忘记密码"]){
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"PasswordViewController"];
        [controller setValue:@{@"code":codeNoTextFiled.text,@"mobile":mobileTextFiled.text} forKey:@"dic"];
         controller.navigationItem.title=self.navigationItem.title;
        [self.navigationController pushViewController:controller animated:YES];
    
    }else{
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangePWdViewController"];
        controller.navigationItem.title=@"修改密码";
        [controller setValue:@{@"code":codeNoTextFiled.text,@"mobile":mobileTextFiled.text} forKey:@"dic"];
        [self.navigationController pushViewController:controller animated:YES];
    
    
    
    
    }
    
    
   
    
}

-(void)timeChange{
    scends--;
    [checkBtn setTitle:[NSString stringWithFormat:@"%ld秒",scends] forState:UIControlStateNormal];
    
    if (scends<=0) {
        
        [timer invalidate];
        checkBtn.userInteractionEnabled=YES;
        [checkBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    }
    
    
}


@end
