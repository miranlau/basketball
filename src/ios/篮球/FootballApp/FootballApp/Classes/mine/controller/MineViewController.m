//
//  MineViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MineViewController.h"
#import "PlayerModel.h"
@interface MineViewController ()
{

    __weak IBOutlet UITextField *userNameText;
    
    __weak IBOutlet UITextField *passwordText;

}
@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userNameText.text= [AccountHelper shareAccount].account;
    
}
- (IBAction)regBtnClick:(id)sender {
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CheckNoViewController"];
    controller.navigationItem.title=@"注册";
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)forgetPasswordBtnClick:(id)sender {
    
    UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CheckNoViewController"];
    controller.navigationItem.title=@"忘记密码";
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (IBAction)loginBtnClick:(id)sender {
    
    
     [SVProgressHUD showWithStatus:@"登录中..."];
    
    [HttpRequestHelper postWithURL:Url(@"login/submit") params:@{@"username":userNameText.text,@"password":passwordText.text} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            
            [AccountHelper shareAccount].player=[[PlayerModel alloc]initWithDic:responseObject];
            [AccountHelper shareAccount].token=[AccountHelper shareAccount].player.token;
            [[AccountHelper shareAccount] saveAccount:userNameText.text password:passwordText.text];
            
            [SVProgressHUD showSuccessWithStatus:@"登录成功"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"headImageViewChange" object:nil];
            
//             [[NSNotificationCenter defaultCenter]postNotificationName:@"logined" object:nil];
            
            //注册推送
            [AccountHelper PushRegistration];
            
            [[AccountHelper shareAccount] autoLoginHX:[AccountHelper shareAccount].player.username];
            
            
           
            
            [[AccountHelper shareAccount] getPlayer:^(id data) {
                 [self.navigationController popViewControllerAnimated:YES];
            }];

            
        }else{
        
         [SVProgressHUD showErrorWithStatus:responseObject];
        
        }
        
    } failure:^(NSError *error){
        
        
    }];
    
    
    
   
    
}
- (IBAction)showPwdBtnClick:(UIButton *)sender {
    sender.selected=!sender.selected;
    
    if (sender.selected) {
        
        passwordText.secureTextEntry=NO;
        
    }else{
        
    passwordText.secureTextEntry=YES;
        
    }
    
    
}



@end
