//
//  MyFansViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MyFansViewController.h"

@interface MyFansViewController ()
{

    __weak IBOutlet UITableView *fansTableView;


}
@end

@implementation MyFansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   self.navigationItem.title = @"我的粉丝";
    
    [self loadTableView];
    
     [fansTableView reloadData];
}
-(void)loadTableView{
    
    fansTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    fansTableView.rowHeight=UITableViewAutomaticDimension;
    fansTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]init];
    bgImageView.image=BGIMG;
    fansTableView.backgroundView=bgImageView;
    
    fansTableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        
        [self  loadNetWorkData];
    }];
     [fansTableView.mj_header beginRefreshing];
    
}
- (void)loadNetWorkData
{
    [self.dataArr removeAllObjects];
    [HttpRequestHelper postWithURL:Url(@"user/myFans") params:nil success:^(BOOL sucess, id responseObject) {
        
        if (sucess)
        {
            
            NSArray *array = responseObject;
            for(NSDictionary *dic in array){
                
                PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
                
                [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"UserCell1"]];
            }
            
            
            [fansTableView.mj_header endRefreshing];
            [fansTableView reloadData];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
        
        [fansTableView.mj_header endRefreshing];
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    BaseCellModel *model=self.dataArr[indexPath.row];
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    if (indexPath.row%2) {
        cell.contentView.backgroundColor=BGCOLOR1;
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    [cell setValue:model forKey:@"data"];
 
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
   BaseCellModel *model=self.dataArr[indexPath.row];
        
    if ([model.reuseIdentifier isEqualToString:@"UserCell1"]) {
        
        PlayerModel *modelname = model.value;
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        controller.navigationItem.title= modelname.name;
        [controller setValue:modelname.id forKey:@"userId"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
}


@end
