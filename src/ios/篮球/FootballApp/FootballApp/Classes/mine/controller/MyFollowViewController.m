//
//  MyFollowViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MyFollowViewController.h"
#import "RaceEventModel.h"
#import "TeamModel.h"
#import "PlayerModel.h"
#import "TeamModel.h"
@interface MyFollowViewController ()
{

    __weak IBOutlet UITableView *followTableView;
    __weak IBOutlet UIView *headView;
    
    UIButton *lastBtn;
    
    __weak IBOutlet NSLayoutConstraint *left;
    __weak IBOutlet UIButton *firstBtn;
    __weak IBOutlet UIImageView *line;

}
@end

@implementation MyFollowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我关注的";
    NSLog(@"token = %@",[AccountHelper shareAccount].token);
    
    [self loadHeadView];
    
    [self loadTableView];
    
    [self headBtnClick:firstBtn];
}

-(void)loadHeadView{
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    headView.backgroundColor=BarColor;
    
}

-(void)loadTableView{
    
    followTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    followTableView.rowHeight=UITableViewAutomaticDimension;
    followTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    bgImageView.image=BGIMG;
    followTableView.backgroundView=bgImageView;
  
    
    followTableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        
        [self  loadNetWorkData:lastBtn.tag];
    }];
    
    
  
    
}
-(IBAction)headBtnClick:(UIButton *)butn{
    
    //防止点击多次
    if (butn==lastBtn) {
        return;
    }
    
    lastBtn.selected=NO;
    butn.selected=!butn.selected;
    
    lastBtn=butn;
    
    [UIView animateWithDuration:.3 animations:^{
        
        left.constant=kScreenWidth/3*(butn.tag-1001);
        [line layoutIfNeeded];
        
        
    }];
    
//    [followTableView reloadData];
     [followTableView.mj_header beginRefreshing];
    
}
-(void)loadNetWorkData:(NSInteger )sender{

    
    //关注的球员
    if (sender==1001) {
        
//        NSLog(@"url = %@",Url(@"player/myFocusPlayer"));
        [self.dataArr removeAllObjects];
        [HttpRequestHelper postWithURL:Url(@"player/myFocusPlayer") params:nil success:^(BOOL sucess, id responseObject) {
            
            if (sucess)
            {
                
                NSArray *array = responseObject;
                for(NSDictionary *dic in array){
                    
                    PlayerModel *model=[[PlayerModel alloc]initWithDic:dic];
                    
                    [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"UserCell1"]];
                }
             
                
                [followTableView.mj_header endRefreshing];
                [followTableView.mj_footer endRefreshing];
                
                [followTableView reloadData];
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
        } failure:^(NSError *error) {
            
            [followTableView.mj_header endRefreshing];
            [followTableView.mj_footer endRefreshing];
        }];
        
    }
    
    
    //关注球队
    if (sender==1002) {
        
         [self.dataArr removeAllObjects];
        [HttpRequestHelper postWithURL:Url(@"team/myFocusTeam") params:nil success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                NSArray *array = responseObject;
                for(NSDictionary *dic in array){
                    
                    TeamModel *model=[[TeamModel alloc]initWithDic:dic];
                    
                    [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"TeamCell"]];

                }
                
                [followTableView.mj_header endRefreshing];
                [followTableView.mj_footer endRefreshing];
                
                [followTableView reloadData];
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
                
            }
            
            
            
            
        } failure:^(NSError *error) {
            
            [followTableView.mj_header endRefreshing];
            [followTableView.mj_footer endRefreshing];
            
            
        }];
        
        
    }
    //关注联赛
    if (sender==1003) {
        [self.dataArr removeAllObjects];
        [HttpRequestHelper postWithURL:Url(@"league/myFocusLeague") params:nil success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                NSArray *array = responseObject;
                for(NSDictionary *dic in array){
                    
                    RaceEventModel *model=[[RaceEventModel alloc]initWithDic:dic];
                    
                    [self.dataArr addObject:[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell"]];
                    
                }
                
                [followTableView.mj_header endRefreshing];
                [followTableView.mj_footer endRefreshing];
                
                [followTableView reloadData];
            }else{
                
                [SVProgressHUD showErrorWithStatus:responseObject];
                
            }
            
            
            
            
        } failure:^(NSError *error) {
            
            [followTableView.mj_header endRefreshing];
            [followTableView.mj_footer endRefreshing];
            
            
        }];    }
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:lastBtn.tag==1001?@"MenberCell":@"MatchCell" forIndexPath:indexPath];
    
    BaseCellModel *model=self.dataArr[indexPath.row];
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
        if (indexPath.row%2) {
            cell.contentView.backgroundColor=BGCOLOR1;
            }else{
            cell.contentView.backgroundColor=BGCOLOR2;
            }
    
    if ([model.reuseIdentifier isEqualToString:@"UserCell1"]) {
        [cell setValue:model forKey:@"data"];
    }
    if ([model.reuseIdentifier isEqualToString:@"TeamCell"]) {
        [cell setValue:model forKey:@"data"];
    }
    if ([model.reuseIdentifier isEqualToString:@"MatchCell"]) {
        [cell setValue:model forKey:@"data"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    BaseCellModel *model=self.dataArr[indexPath.row];
    RaceEventModel *raceModle = model.value;
    
    
    //队员详情
    if(lastBtn.tag==1001){
        PlayerModel *modelname = model.value;
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"UserViewController"];
        controller.navigationItem.title= modelname.name;
         [controller setValue:modelname.id forKey:@"userId"];
        [self.navigationController pushViewController:controller animated:YES];
    
    }
    
    //联赛详情
    if ([model.reuseIdentifier isEqualToString:@"MatchCell"]) {
        
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
        
        [controller setValue:raceModle.id     forKey:@"MatchId"];
        [controller setValue:raceModle.status forKey:@"status"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    //球队详情
    if ([model.reuseIdentifier isEqualToString:@"TeamCell"]) {
        
        TeamModel *tm=model.value;
        UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
        controller.navigationItem.title=tm.name;
        [controller setValue:tm.id forKey:@"teamId"];
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
}

@end
