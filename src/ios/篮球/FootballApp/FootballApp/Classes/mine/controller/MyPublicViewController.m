//
//  MyPublicViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "MyPublicViewController.h"
#import "FriendModle.h"
#import "DynamicDeatilController.h"
#import <UITableView+FDTemplateLayoutCell.h>

@interface MyPublicViewController ()
{
 __weak IBOutlet UITableView *circleTableView;
 int pageNumber;
    
    
}
@property(nonatomic,strong)NSMutableArray *array0;
@property(nonatomic,strong)NSMutableArray *array1;

@end

@implementation MyPublicViewController
- (void)reloadtheDeleveViewdata
{
    [self viewDidLoad];
    [circleTableView.mj_header beginRefreshing];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = @"我发表的";
    
    [self loadTableView];
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadtheDeleveViewdata) name:@"reloadtheDeleveViewdata" object:nil];
    
    
    [circleTableView reloadData];
}
-(void)loadTableView{
    
    circleTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    circleTableView.rowHeight=UITableViewAutomaticDimension;
    circleTableView.estimatedRowHeight=125;
    
    UIImageView *bgImageView=[[UIImageView alloc]init];
    bgImageView.image=BGIMG;
    circleTableView.backgroundView=bgImageView;
    
    circleTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 进入刷新状态后会自动调用这个block
        pageNumber=1;
        [self  loadNetWorkDatas];
        
        
    }];
    
    //    [circleTableView.mj_header beginRefreshing];
    
    circleTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        pageNumber++;
        
        [self  loadNetWorkDatas];
        
    }];
    [circleTableView.mj_header beginRefreshing]; //注意这个玩意放的位置应该在block后面之类的
}

-(void)loadNetWorkDatas
{
    
    if (pageNumber==1) {
        
        [self.dataArr removeAllObjects];
    }
   
        [HttpRequestHelper postWithURL:Url(@"dynamic/myDynamicList") params:@{@"pageNumber":@(pageNumber)} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {
                
                for (NSDictionary *dic  in responseObject[@"content"]) {
                    
                    FriendModle *model=[[FriendModle alloc]initWithDic:dic];
                    
                    BaseCellModel *base=nil;
                    
                    base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"CircleCell"];
                    [self.dataArr addObject:base];
                    
                }
                [circleTableView reloadData];
                
                [circleTableView.mj_header endRefreshing];
                [circleTableView.mj_footer endRefreshing];
                
                
            }else{
                [circleTableView.mj_header endRefreshing];
                [circleTableView.mj_footer endRefreshing];
                [SVProgressHUD showErrorWithStatus:responseObject];
            }
            
            
        } failure:^(NSError *error) {
            
            [circleTableView.mj_header endRefreshing];
            [circleTableView.mj_footer endRefreshing];
            
        }];
   
}

//每个分区的行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseCellModel *model = self.dataArr[indexPath.row];
    
    return [tableView fd_heightForCellWithIdentifier:model.reuseIdentifier cacheByIndexPath:indexPath configuration:^(UITableViewCell *cell) {
        
        
        cell.fd_enforceFrameLayout = NO;
        
        [cell setValue:model forKey:@"data"];
        
        
    }];
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BaseCellModel *model = self.dataArr[indexPath.row];
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier];
    
    cell.fd_enforceFrameLayout = NO;
    [cell setValue:model forKey:@"data"];
    
    if (indexPath.row%2) {
        
        
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    
    
    
    
    return cell;
    
    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BaseCellModel *model = self.dataArr[indexPath.row];
    FriendModle *fridenmo = model.value;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DynamicDeatilController* vc = [storyboard instantiateViewControllerWithIdentifier:@"DynamicDeatilController"];
    vc.dynamicId = [NSString stringWithFormat:@"%@",fridenmo.id];
    vc.MinePublishSymbol = @"这里进去可以删除";
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
