//
//  PasswordViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()
{

    __weak IBOutlet UITextField *mobileTextFiled;

    __weak IBOutlet UITextField *password2;
    __weak IBOutlet UITextField *password1;
}
@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mobileTextFiled.text=self.dic[@"mobile"];
    
    [password1 becomeFirstResponder];
   
}

- (IBAction)sumbitBtnClick:(id)sender {
    
    
    
    if([password1.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"请输入密码"];
        
        return;
    }
    
    if([password2.text isEqualToString:@""]){
        
        [SVProgressHUD showErrorWithStatus:@"请再次输入密码"];
        
        return;
    }
    
    
    if(![password2.text isEqualToString:password1.text]){
    [SVProgressHUD showErrorWithStatus:@"两次密码不一致，请重新输入"];
        return;
    }
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    [dic addEntriesFromDictionary:self.dic];
    [dic setValue:password1.text forKey:@"password"];
    
    NSString *message=@"";
    NSString *message2=@"";
    NSString *url=@"";
    
    if ([self.navigationItem.title isEqualToString:@"注册"]) {
        
        
        url=@"register/submit";
        message=@"注册中";
        message2=@"注册成功";
    }else{
        message=@"修改中";
        url=@"user/findPassword";
        message2=@"修改成功";
    }
    
    
    
    
    [SVProgressHUD showWithStatus:message];

    [HttpRequestHelper postWithURL:Url(url) params:dic success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            [SVProgressHUD showSuccessWithStatus:message2];
            
            if ([message2 isEqualToString:@"注册成功"]) {
                
                [AccountHelper shareAccount].token=responseObject[@"token"];
                [AccountHelper shareAccount].player=[[PlayerModel alloc]init];
                [AccountHelper shareAccount].player.id=responseObject[@"id"];
                [AccountHelper shareAccount].isReg=YES;
                
                
//                [[NSNotificationCenter defaultCenter]postNotificationName:@"logined" object:nil];
                
                [[AccountHelper shareAccount] autoLoginHX:[AccountHelper shareAccount].player.username];
                
            }
            
            [[AccountHelper shareAccount] saveAccount:mobileTextFiled.text password:password1.text];
            
             [[AccountHelper shareAccount]autoLogin];
            
            [self performSelector:@selector(regSucess) withObject:nil afterDelay:1.5];
          
        }else{
            
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
        
        
    } failure:^(NSError *error){
      
    }];

}
-(void)regSucess{
        [self.navigationController popToRootViewControllerAnimated:YES];
    
}

@end
