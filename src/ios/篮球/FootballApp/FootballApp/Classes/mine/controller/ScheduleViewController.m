//
//  ScheduleViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ScheduleViewController.h"
#import "CalendarHomeViewController.h"
#import "MatchCell.h"
#import "MatchModel.h"
#import "TeamModel.h"

@interface ScheduleViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *newtableview;
    CalendarHomeViewController *calendarView;
    NSMutableArray *dateArray;
     NSMutableArray *dateArray2;
    NSMutableArray *dateArray3; //里面装我的点击某一天的所有比赛
    long numberDate;            //两个日期之间的天数
    __weak IBOutlet UIButton *xxButton;
    
    
    __weak IBOutlet NSLayoutConstraint *tableHeadConst;
    __weak IBOutlet NSLayoutConstraint *heightTableVIewConst;
    
}
@property(nonatomic,assign)BOOL flag;
@end

@implementation ScheduleViewController
- (IBAction)xxbuttonClicked:(UIButton *)sender {
    newtableview.hidden = YES;
    xxButton.hidden = YES;
    _flag = NO;
    //newtablevie清除数据
}

- (void)appearTableView
{
     _flag = YES;
    newtableview.hidden = NO;
    xxButton.hidden = NO;
    [newtableview reloadData];
   
    if(dateArray3.count == 0)
    {
        newtableview.hidden = YES;
        xxButton.hidden = YES;
    }
    else if (dateArray3.count == 1)
    {
        tableHeadConst.constant = kScreenHeight/2 - 150;
        heightTableVIewConst.constant =  127;
    }
    else if (dateArray3.count == 2)
    {
        tableHeadConst.constant = kScreenHeight/2 - 180;
        heightTableVIewConst.constant = 250;
    }
    else if (dateArray3.count == 3)
    {
        tableHeadConst.constant = kScreenHeight/2 - 140 - kScreenHeight * 0.19;
        heightTableVIewConst.constant =375;
    }
    else
    {
        tableHeadConst.constant = 40;
        heightTableVIewConst.constant = kScreenHeight * 0.72;
    }
    
    [newtableview reloadData];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    newtableview.hidden = YES;
    xxButton.hidden = YES;
    _flag = NO;
    dateArray = [NSMutableArray arrayWithCapacity:0];
     dateArray2 = [NSMutableArray arrayWithCapacity:0];
     dateArray3 = [NSMutableArray arrayWithCapacity:0];
//    NSLog(@"%@",[AccountHelper shareAccount].token);
    calendarView = [[CalendarHomeViewController alloc]init];
    calendarView.view.frame=CGRectMake(0,0,kScreenWidth, kScreenHeight);
    calendarView.view.backgroundColor=[UIColor clearColor];

    self.navigationItem.title = @"赛程";
    [self.view addSubview:calendarView.view];
    
    newtableview.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    newtableview.rowHeight=UITableViewAutomaticDimension;
    newtableview.estimatedRowHeight=125;
    newtableview.backgroundColor = [UIColor colorWithRed:80.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0];
    newtableview.layer.cornerRadius = 5;
    newtableview.layer.borderWidth = 1;
    newtableview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.view addSubview:newtableview];
    [self.view addSubview:xxButton];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appearTableView) name:@"appearTableView" object:nil];
    
 
 
    
    [self getWebDataOnline];
    
 
    
}

- (void)getWebDataOnline
{
    [HttpRequestHelper postWithURL:Url(@"match/queryMyMatchShedule") params:nil success:^(BOOL sucess, id responseObject) {
        
        if (sucess)
        {
           
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *array = responseObject;
                for(NSString  *str in array){
                    NSString *str1 = [NSString stringWithFormat:@"%f", [str doubleValue] / 1000];
                    [dateArray addObject:str1];
                }
                
                
                for (int i = 0; i < dateArray.count; i++)
                {
                    NSString *newdata = [self changTimeRight:dateArray[i]];
                    NSLog(@"newdata = %@",newdata);
                    [dateArray2 addObject:newdata];
                }
                
                
                
                NSString *strMax = [self changTimeRight:[dateArray valueForKeyPath:@"@max.doubleValue"]]; //取出数组中最大值
                NSString *strMin = [self changTimeRight:[dateArray valueForKeyPath:@"@min.doubleValue"]]; //取出数组中最大值
                
                NSString *str0000 = [dateArray valueForKeyPath:@"@max.doubleValue"];
                NSString *str1111 = [dateArray valueForKeyPath:@"@min.doubleValue"];
                
                
                NSArray *array0 = [strMax  componentsSeparatedByString:@"-"];
                NSArray *array1 = [strMin  componentsSeparatedByString:@"-"];
                
                
                //年月日的格式，计算两个日期之间的天数
                if (array0.count == 3 && array1.count == 3) {
                    
                    
                    
                    numberDate =  [self calcDaysFromBegin:[NSDate dateWithTimeIntervalSince1970:[str1111 doubleValue]] end:[NSDate dateWithTimeIntervalSince1970:[str0000 doubleValue]]];
                    NSLog(@"numberDate = %@",str1111);
                    
                    
                    
                    
                }
                else
                {
                    [SVProgressHUD showInfoWithStatus:@"服务器返回时间格式错误"];
                    numberDate = 90;
                }
                
                
                
                if (numberDate != 0) {
                    
                    
                    [calendarView setTrainToDay:(int)numberDate ToDateforString:str1111];//前面参数是日历显示的天数，后面参数是日历最开始的日期
                }
                else
                {
                    [calendarView setTrainToDay:90 ToDateforString:nil];//飞机初始化方法
                    
//                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                                    message:@"您没有赛事记录"
//                                                                   delegate:nil
//                                                          cancelButtonTitle:@"确定"
//                                                          otherButtonTitles:nil];
//                    
//                    [alert show];
                }
                
                __typeof(self) weakSelf = self;
                
                
                
                calendarView.dicArray = dateArray2;
                
                calendarView.calendarArray = ^(NSArray *array)
                {
                    NSLog(@"%lu",(unsigned long)array.count);
                    dateArray3 = [array mutableCopy];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"appearTableView" object:nil];
                };
                
                
                calendarView.calendarblock = ^(CalendarDayModel *model){
                    
                    if (weakSelf.flag)
                    {
                        
                    }
                    else
                    {
                        [SVProgressHUD showInfoWithStatus:@"当前时间没有比赛"];
                    }
                };
            }) ;
            
            
            
            
            
            

            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshmydata" object:nil];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:responseObject];
        }
        
    } failure:^(NSError *error) {
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dateArray3.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    BaseCellModel *model=dateArray3[indexPath.row];
  
    BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
       cell.contentView.backgroundColor=[UIColor colorWithRed:80.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0];
    
    
    [cell setCellBlock:^(id data) {
        
        //左边和右边头像的跳转
        MatchModel *match=model.value;
        
        if ([data isEqualToString:@"left"]) {
            TeamModel *team=[[TeamModel alloc]initWithDic:match.home];
            [self teamHeadImageClick:team.id];
        }
        if ([data isEqualToString:@"right"]) {
            TeamModel *team=[[TeamModel alloc]initWithDic:match.visiting];
            [self teamHeadImageClick:team.id];
            
        }
      
    }];
    
//    if (indexPath.row%2) {
//        
//        cell.contentView.backgroundColor=BGCOLOR1;
//
//        
//    }else{
//        cell.contentView.backgroundColor=BGCOLOR2;
//    }
    [cell setValue:model forKey:@"data"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
 
    BaseCellModel *model=dateArray3[indexPath.row];
    MatchModel *match=model.value;
    
    
    
    if ([match.lid intValue]==10000) {
        
        //赛前
        if ([match.status intValue]==10) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            controller.navigationItem.title=@"赛前";
            [controller setValue:match.id forKey:@"matchId"];
            
            [controller setControllerBlock:^(id data) {
                
                [newtableview.mj_header beginRefreshing];
                
            }];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        //比赛中
        if ([match.status intValue]==20) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            controller.navigationItem.title=@"赛中";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        //比赛结束
        if ([match.status intValue]==100) {
   
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
            controller.navigationItem.title=@"赛后";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
   
        }
        
    }else{
        //約赛中
        if ([match.status intValue]==0) {
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinWarViewController"];
            
            controller.navigationItem.title=@"约赛中";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
  
        }
        //以匹配（赛前）
        if ([match.status intValue]==10) {
            
            
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            
            controller.navigationItem.title=@"赛前";
            [controller setValue:match.id forKey:@"matchId"];
            
            [self.navigationController pushViewController:controller animated:YES];
         
        }
        //比赛中
        if ([match.status intValue]==20) {
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"JoinViewController"];
            
            controller.navigationItem.title=@"赛中";
            [controller setValue:match.id forKey:@"matchId"];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        //比赛结束
        if ([match.status intValue]==100) {
            BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebSerivceViewController"];
            controller.navigationItem.title=@"赛后";
            [controller setValue:match.id forKey:@"matchId"];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
 
}
-(void)teamHeadImageClick:(NSString *)string{
    
    UIViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
    [controller setValue:string forKey:@"teamId"];
    [self.navigationController pushViewController:controller animated:YES];
    
    
    
}


//时间转换成年月日
- (NSString *)changTimeRight:(NSString *)time
{
    NSDate *nd = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:nd];
//    NSLog(@"datatime = %@",dateString);
    return dateString;
}


//计算两个日期之间的天数
-(NSInteger)calcDaysFromBegin:(NSDate *)inBegin end:(NSDate *)inEnd
{
    NSInteger unitFlags = NSDayCalendarUnit| NSMonthCalendarUnit | NSYearCalendarUnit;
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [cal components:unitFlags fromDate:inBegin];
    NSDate *newBegin  = [cal dateFromComponents:comps];
   
    
    NSCalendar *cal2 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps2 = [cal2 components:unitFlags fromDate:inEnd];
    NSDate *newEnd  = [cal2 dateFromComponents:comps2];
  
    
    NSTimeInterval interval = [newEnd timeIntervalSinceDate:newBegin];
    NSInteger beginDays=((NSInteger)interval)/(3600*24);
    
    return beginDays;
}
@end
