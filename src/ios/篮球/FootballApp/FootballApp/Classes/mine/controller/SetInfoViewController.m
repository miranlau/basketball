//
//  SetInfoViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/9.
//  Copyright © 2016年 North. All rights reserved.
//

#import "SetInfoViewController.h"

@interface SetInfoViewController ()
{
    
    __weak IBOutlet UITextField *textFiled;
    
    NSInteger maxLength;

}
@end

@implementation SetInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [textFiled becomeFirstResponder];
    textFiled.text=_info;
    textFiled.text =[textFiled.text stringByReplacingOccurrencesOfString:@" cm" withString:@""];
    textFiled.text =[textFiled.text stringByReplacingOccurrencesOfString:@" kg" withString:@""];
    textFiled.text =[textFiled.text stringByReplacingOccurrencesOfString:@" 岁" withString:@""];
    
   [textFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    if ([self.navigationItem.title isEqualToString:@"修改姓名"]||[self.navigationItem.title isEqualToString:@"身份证号"]||[self.navigationItem.title isEqualToString:@"真实名字"]) {
        textFiled.keyboardType=UIKeyboardTypeDefault;
        
        maxLength=20;
    }else{
        maxLength=3;
    }
    textFiled.leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 40)];
    textFiled.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
}
-(void)textFieldDidChange:(UITextField *)textfield{


   
        if (textFiled.text.length > maxLength) {
            textFiled.text = [textFiled.text substringToIndex:maxLength];
        }
    


}
-(void)save:(id)sender{
    
    if ([textFiled.text isEqualToString:@""]) {
        
         [SVProgressHUD showErrorWithStatus:@"请先输入"];
         return;
        
    }
 
    NSString *type=nil;
    if ([self.navigationItem.title isEqualToString:@"修改姓名"]) {
        type=@"name";
    }
    if ([self.navigationItem.title isEqualToString:@"身高"]) {
        type=@"height";
    }
    if ([self.navigationItem.title isEqualToString:@"体重"]) {
        type=@"weight";
    }
    if ([self.navigationItem.title isEqualToString:@"年龄"]) {
        type=@"age";
    }
    if ([self.navigationItem.title isEqualToString:@"真实名字"]) {
        type=@"realName";
    }
    
    if ([self.navigationItem.title isEqualToString:@"身份证号"]) {
        type=@"idcard";
        
        
        
        if (![RegularTools validateIdentityCard:textFiled.text]) {
            
             [SVProgressHUD showErrorWithStatus:@"身份证错误"];
            return;
        }
        
        
    }
    
    
    [ HttpRequestHelper postWithURL:Url(@"user/modifyData") params:@{type:textFiled.text} success:^(BOOL sucess, id responseObject2) {
        
        if (sucess) {
            
            if ([self.navigationItem.title isEqualToString:@"修改姓名"]) {
                
                [AccountHelper shareAccount].player.name=textFiled.text;
                 [AccountHelper shareAccount].user.name=textFiled.text;
            }
        
            [[NSNotificationCenter defaultCenter]postNotificationName:@"userInfoChange" object:nil];
            
            [self.navigationController popViewControllerAnimated:YES];
            
            
        }else{
            [SVProgressHUD showErrorWithStatus:responseObject2];
            
        }
        
    } failure:^(NSError *error) {
        
    }];


}


@end
