//
//  SettingViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "SettingViewController.h"
#import "OnePhotoSelecter.h"
#import "SettingCell.h"
#import "CityListViewController.h"
#import "TeamHelper.h"

@interface SettingViewController ()
{


    __weak IBOutlet UITableView *setTableView;

    NSMutableDictionary *rowDic;

}
@property(nonatomic,strong) SettingCell *cell0;
@end

@implementation SettingViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"个人设置";
    
//    NSLog(@"token = %@",[AccountHelper shareAccount].token);
 
    [self userInfoChanged];
    
    [self loadTableView];
    
  
    //设置访问手机通讯录权限
    [self getMineAccesssAuthPhone];
}
//设置访问手机通讯录权限
- (void)getMineAccesssAuthPhone
{
    [TeamHelper CheckAddressBookAuthorization:^(bool isAuthorized) {
        if (isAuthorized)
        {
            NSLog(@"同意了APP访问该手机通讯录");
        }
        else
        {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"提示"  message: @"请到 设置>隐私>通讯录 打开通讯录权限"    preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
    
}

-(void)loadTableView{
    
    setTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    setTableView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    setTableView.rowHeight=UITableViewAutomaticDimension;
    setTableView.estimatedRowHeight=100;
    
}
-(NSMutableDictionary *)witchCellForRow{
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    
    
    [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                       
                                                       [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"SettingCell1"],
                                                       [BaseCellModel setModel:@"身高" key:@" cm" value:[AccountHelper shareAccount].user.height image:@"" reuseIdentifier:@"SettingCell2"],
                                                       [BaseCellModel setModel:@"体重" key:@" kg" value:[AccountHelper shareAccount].user.weight image:@"" reuseIdentifier:@"SettingCell2"],
                                                       [BaseCellModel setModel:@"年龄" key:@" 岁" value:[AccountHelper shareAccount].user.age image:@"" reuseIdentifier:@"SettingCell2"],
                                                       
                                                       [BaseCellModel setModel:@"真实名字" key:@"" value:[AccountHelper shareAccount].user.realName image:@"" reuseIdentifier:@"SettingCell2"],
                                                       
                                                       [BaseCellModel setModel:@"身份证号" key:@"" value:[AccountHelper shareAccount].user.idcard image:@"" reuseIdentifier:@"SettingCell2"]
                                                       
                                                       ]];

    
    
    [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                       
                                                       [BaseCellModel setModel:@"允许他人查看我的资料" key:@"" value:@"0" image:@"" reuseIdentifier:@"SettingCell3"],
                                                       [BaseCellModel setModel:@"允许聊天信息通知" key:@"" value:@"1" image:@"" reuseIdentifier:@"SettingCell3"],
                                                       [BaseCellModel setModel:@"自动下载更新并提示安装" key:@"" value:@"2" image:@""  reuseIdentifier:@"SettingCell3"],
//                                                       [BaseCellModel setModel:@"切换地理位置" key:@"" value:@"" image:@"" reuseIdentifier:@"SettingCell2"],
                                                       [BaseCellModel setModel:@"允许打开通讯录" key:@"" value:@"3" image:@"" reuseIdentifier:@"SettingCell3"],

                                                       
                                                       [BaseCellModel setModel:@"修改密码" key:@"" value:@"" image:@"" reuseIdentifier:@"SettingCell2"],
                                                       [BaseCellModel setModel:@"退出登录" key:@"" value:@"" image:@"" reuseIdentifier:@"SettingCell2"]
                                                       ]];
    
    
    

    return dic;
    
}
#pragma mark tableView 委托协议

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView{
    return [[rowDic allKeys]count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view=[[UIView alloc]init];
    view.backgroundColor=[UIColor clearColor];
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    SettingCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
    //回调事件
 
    
    if (indexPath.row%2) {
        
        cell.contentView.backgroundColor=BGCOLOR1;
        
    }else{
        cell.contentView.backgroundColor=BGCOLOR2;
    }
    
    
    [cell setValue:model forKey:@"data"];
    
    
    [cell setCellBlock:^(id data) {
        
        [self changRowInfo:indexPath type:data];
        
    }];
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
    
    if ([model.name isEqualToString:@"修改密码"]) {
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangePWdViewController"];

        controller.navigationItem.title=model.name;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    if ([model.name isEqualToString:@"退出登录"]) {
        
        
        
        [[[FBAlertView alloc]initWithTitle:@"退出以后将会清空用户信息，是否退出" butnTitlt:@"退出" block:^(id data) {
            
             [[AccountHelper shareAccount]cancelLogin];
             [[NSNotificationCenter defaultCenter]postNotificationName:@"headImageViewChange" object:nil];
            
           
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            
        }]show];
      
  
    }
    
    if ([model.name isEqualToString:@"切换地理位置"]) {
        
        CityListViewController *cityListView = [[CityListViewController alloc]initWithBlock:^(id data) {
        
        }];
        cityListView.navigationItem.title=model.name;
        [self.navigationController pushViewController:cityListView animated:YES];
        
    }
    
    if ([model.reuseIdentifier isEqualToString:@"SettingCell1"]) {
        SettingCell *cell=[tableView cellForRowAtIndexPath:indexPath];

        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SetInfoViewController"];
        controller.navigationItem.title=@"修改姓名";
          [controller setValue:cell.userName.text forKey:@"info"];
  
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    if ([model.name isEqualToString:@"身高"]||[model.name isEqualToString:@"体重"]||[model.name isEqualToString:@"年龄"]||[model.name isEqualToString:@"身份证号"]||[model.name isEqualToString:@"真实名字"]) {
        
        
        SettingCell *cell=[tableView cellForRowAtIndexPath:indexPath];
 
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SetInfoViewController"];
        controller.navigationItem.title=model.name;
        [controller setValue:cell.infoLabel.text forKey:@"info"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
 
}
-(void)changRowInfo:(NSIndexPath *)index type:(NSString *)type{

    if ([type isEqualToString:@"head"]) {
        
        [[OnePhotoSelecter shareInstance] selectImage:self imageBlock:^(id data) {
            
            SettingCell *cell=[setTableView cellForRowAtIndexPath:index];
            cell.headImageView.image=data;
            
             [SVProgressHUD showWithStatus:@"上传图片中"];

            [HttpRequestHelper postWithURL:Url(@"resource/upload") params:@{@"type":@"avatar"} formDataArray:@[data] success:^(BOOL sucess, id responseObject1) {
                
                
                if (sucess) {
                    
                    
                    [ HttpRequestHelper postWithURL:Url(@"user/modifyData") params:@{@"avatar":responseObject1[0]} success:^(BOOL sucess, id responseObject2) {
                        
                        if (sucess) {
    
                            [AccountHelper shareAccount].player.avatar=responseObject1[0];
                            [AccountHelper shareAccount].user.avatar=responseObject1[0];
                            [SVProgressHUD showSuccessWithStatus:@"修改头像成功"];
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"headImageViewChange" object:nil];
                            
                        }else{
                        [SVProgressHUD showErrorWithStatus:responseObject2];
                        
                        }
                       
                   } failure:^(NSError *error) {
                       
                   }];

                    
                }else{
                
                     [SVProgressHUD showErrorWithStatus:responseObject1];
                
                }
                
                
            } failure:^(NSError *error) {
                
                
            }];
            
            
            
            
        }];
    }
    



}
-(void)userInfoChanged{

    
    [self loadUserData:^(id data) {

        dispatch_async(dispatch_get_main_queue(), ^{
            rowDic=[self witchCellForRow];
            [setTableView reloadData];
        });
 
       
    }];
    

}
@end
