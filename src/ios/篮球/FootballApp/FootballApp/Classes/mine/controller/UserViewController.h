//
//  UserViewController.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseViewController.h"

@interface UserViewController : BaseViewController

@property(retain,nonatomic)NSString *userId;

@end
