//
//  UserViewController.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "UserViewController.h"
#import "BaseCell.h"
#import "PlayerModel.h"
#import "TeamModel.h"
#import "UserCell.h"
#import "ShareView.h"
#import "SendNewViewController.h"

@interface UserViewController ()
{

    __weak IBOutlet UITableView *userTableView;
    NSMutableDictionary *rowDic;
    __weak IBOutlet NSLayoutConstraint *buttom;
    __weak IBOutlet UIButton *setBtn;
    PlayerModel *otherPlayer;
    NSString *canView;
    
     NSString *shareText;

}
@end

@implementation UserViewController

- (void)shareClicked
{
    
   
    NSString *iconurl = [Defaults objectForKey:@"strICON"];
  
    ShareView *view = [[ShareView alloc] initWithFrame:CGRectMake(0, kScreenHeight - 160, kScreenWidth, 160)];
    NSString *shareURL = [NSString stringWithFormat:@"%@?userId=%d",Url(@"player/share"),MINEId];
    NSString *strmessage = @"我在使用全网篮球，这是我的个人数据";
    view.title = @"全网篮球";//一般是你的App名字
    view.message = strmessage;//需要展示的信息简介
    view.shareUrl = shareURL;//分享的内容地址(点击跳转的地址)
    if(iconurl && ![iconurl isEqualToString:@"(null)"])
    {
         view.pictureName =iconurl;
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@"分析图标未获取到"];
    }
//    view.pictureName =iconurl;// @"http://192.168.0.139:8080/football-api/resource/img/icon/icon.png";//分享的图片名称（必须在左侧项目栏里，且为.png格式，因为shareView进行了配置）
     shareText = [strmessage stringByAppendingString:[NSString stringWithFormat:@"\n%@",shareURL]];
     view.weretogo = @"个人分享";
    /**举个例子
     
     假如是京东某件商品分享
     *title 可以写京东
     *message 商品的介绍（一行字，如促销信息）
     *shareUrl 则为该商品的地址
     *pictureName 该商品的图片
     
     
     以上数据在开发中后端都会有相应的数据
     
     */
    
    [view show];
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    if([self.navigationItem.title isEqualToString:@"我的"])
    {
        UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"分享.png"] style:UIBarButtonItemStylePlain target:self action:@selector(shareClicked)];
        self.navigationItem.rightBarButtonItem = item;
    }
    
    [self showButtomView:1];
 
    [self loadTableView];
    
    [self userInfoChanged];
    
    //这里是分享到圈子的注册
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoSendController) name:@"gotoSendController" object:nil];
}

- (void)gotoSendController
{
    
    
    if ([AccountHelper shareAccount].isLogin ==YES) {
        
        
        SendNewViewController  *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SendNewViewController"];
        controller.shareMarkText = @"分享到圈子";
        controller.shareContext = shareText;
        [self.navigationController pushViewController:controller animated:YES];
        
    }else{
        
        UIViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
        controller.navigationItem.title=@"登录";
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    
    
    
    
}
-(void)loadTableView{
    
    userTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    userTableView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    userTableView.rowHeight=UITableViewAutomaticDimension;
    userTableView.estimatedRowHeight=100;
    
}
#pragma mark tableView 委托协议
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [[rowDic allKeys] count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    return arr.count;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return section==0?5:25;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view=[[UIView alloc]init];
    
    if (section!=0) {
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 4, 55, 17)];
        label.backgroundColor=[COLOR colorWithHexString:@"3e87b4"];
        label.font=[UIFont systemFontOfSize:10];
        label.layer.cornerRadius=3;
        label.textColor=[UIColor whiteColor];
        label.textAlignment=1;
        label.layer.masksToBounds=YES;
        
        if (section==1) {
            label.text=@"技术统计";
        }else{
        label.text=[self.navigationItem.title isEqualToString:@"我的"]?@"我的球队":@"所属球队" ;
//        [label sizeToFit];
        }
        [view addSubview:label];
    }
    
    
    
    return view;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];

        BaseCell *cell=[tableView dequeueReusableCellWithIdentifier:model.reuseIdentifier forIndexPath:indexPath];
    
        [cell setCellBlock:^(id data) {
        
        [self clickType:[data intValue]];
      
        }];
    
        [cell setValue:model forKey:@"data"];
    
    
    if (indexPath.section !=1) {
        if (indexPath.row%2) {
            
            cell.contentView.backgroundColor=BGCOLOR1;
            
            
        }else{
            cell.contentView.backgroundColor=BGCOLOR2;
        }
    }else{
    
        cell.contentView.backgroundColor=[UIColor clearColor];
        cell.backgroundColor=[UIColor clearColor];
    }
    
    
   
    
    
        return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSArray *arr=[rowDic objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    BaseCellModel *model=arr[indexPath.row];
  
    if([model.reuseIdentifier isEqualToString:@"TeamCell"]){
        
        TeamModel *team=model.value;
        
        BaseViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"TeamInfoViewController"];
        
        [controller setValue:team.id forKey:@"teamId"];
        //回调
        
        [controller setControllerBlock:^(id data) {
            
            [self userInfoChanged];
            
        }];
        
        
        [self.navigationController pushViewController:controller animated:YES];

    
    
    }
    
}
-(IBAction)zsBtnClick:(id)sender{



}
-(IBAction)setBtnClick:(id)sender{

    
           [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"] animated:YES];


}

-(void)clickType:(int)type{

    
    
    
    switch (type) {
        case 1://关注别人的
            if (otherPlayer.follow)
            {
                [HttpRequestHelper postWithURL:Url(@"user/unfollow") params:@{@"focusId":otherPlayer.id} success:^(BOOL sucess, id responseObject) {
                    if (sucess)
                    {
                        [SVProgressHUD showInfoWithStatus:@"取消关注成功"];
                         [self userInfoChanged];
                    }
                    else
                    {
                        [SVProgressHUD showErrorWithStatus:responseObject];
                    }
                    
                } failure:^(NSError *error) {
                    
                }];
            }
            else
            {
                //没有关注
                [HttpRequestHelper postWithURL:Url(@"user/follow") params:@{@"focusId":otherPlayer.id} success:^(BOOL sucess, id responseObject) {
                    if (sucess)
                    {
                        
                        [SVProgressHUD showInfoWithStatus:@"关注成功"];
                        [self userInfoChanged];

                        
                    }
                    else
                    {
                        [SVProgressHUD showErrorWithStatus:responseObject];
                    }
                    
                } failure:^(NSError *error) {
                    
                }];
 
            }
            
            
            
            break;
        case 2://点击了我的赛程
            
            
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleViewController"] animated:YES];
            
            break;
        case 3://我关注额

            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyFollowViewController"] animated:YES];

            
            break;
        case 4://我的粉丝
             [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyFansViewController"] animated:YES];
            break;
        case 5://我的发表
            
             [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyPublicViewController"] animated:YES];
            
            break;
            
        default:
            break;
    }
    



}

-(NSMutableDictionary *)witchCellForRow{
    
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    
    
    
    
    if([self.navigationItem.title isEqualToString:@"我的"]){
    
        [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:[AccountHelper shareAccount].player image:@"" reuseIdentifier:@"UserCell1"],
                                                          
                                                           [BaseCellModel setModel:@"" key:@"" value:@"" image:@"" reuseIdentifier:@"UserCell3"]
                                                           ]];
        
        
        
        [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:[AccountHelper shareAccount].player image:@"" reuseIdentifier:@"Player"]]];

        NSMutableArray *arr=[NSMutableArray array];
        
        
        
        for ( NSDictionary *ddic in [AccountHelper shareAccount].player.teams) {
            
            TeamModel *model=[[TeamModel alloc]initWithDic:ddic];
            [arr addObject: [BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"TeamCell"]];
            
        }
        
        
        
        if ([arr count]>0) {
            [BaseCellModel insetKeyForDic:dic key:@"2" value:arr];
        }
        
    }else{
 
        [BaseCellModel insetKeyForDic:dic key:@"0" value:@[
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:otherPlayer image:@"" reuseIdentifier:@"UserCell2"],
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:otherPlayer image:@"" reuseIdentifier:@"UserCell4"]
                                                           
                                                           ]];
        
        [BaseCellModel insetKeyForDic:dic key:@"1" value:@[
                                                           
                                                           [BaseCellModel setModel:@"" key:@"" value:[AccountHelper shareAccount].player image:@"" reuseIdentifier:@"Player"]]];
        
        
        NSMutableArray *arr=[NSMutableArray array];

        for ( NSDictionary *ddic in otherPlayer.teams) {
            
            TeamModel *model=[[TeamModel alloc]initWithDic:ddic];
            [arr addObject: [BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"TeamCell"]];
            
        }
        
        if ([arr count]>0) {
            [BaseCellModel insetKeyForDic:dic key:@"2" value:arr];
        }
    
    
    }
    
    
    return dic;
    
}
-(void)showButtomView:(NSInteger)type{
    
    
    switch (type) {
        case 1://自己的信息，不是记分员
            
            buttom.constant=0;
            setBtn.hidden=NO;
            
            break;
        case 2://自己的信息，是记分员
            buttom.constant=0;
            setBtn.hidden=YES;
            break;
        case 3://别人的信息
            buttom.constant=-48;
            break;
            
        default:
            break;
    }




}
-(void)userInfoChanged{
    
    if([self.navigationItem.title isEqualToString:@"我的"] ){
        
    
    [self loadPlayerData:^(id data) {
        
        rowDic= [self witchCellForRow];
        
        
        [userTableView reloadData];
    }];

    }else{
        
        [self showButtomView:3];
    
        
        [HttpRequestHelper postWithURL:Url(@"player/getPlayerInfo") params:@{@"userId":_userId} success:^(BOOL sucess, id responseObject) {
            
            if (sucess) {

                otherPlayer =[[PlayerModel alloc]initWithDic:responseObject];
                
                RadarDto *radio=[[RadarDto alloc]initWithDic:otherPlayer.radarDto];
                otherPlayer.radarDto=radio;

                canView =  [NSString  stringWithFormat:@"%@",responseObject[@"canview"]];

                
                if ([canView isEqualToString:@"1"]) //允许查看
                {
                    otherPlayer =[[PlayerModel alloc]initWithDic:responseObject];
                    rowDic= [self witchCellForRow];
                    [userTableView reloadData];
                   
                }
                else                                //不允许查看
                {
                    if ( MINEId == [_userId intValue]) //若是点的自己，还是能看的
                    {
                        otherPlayer =[[PlayerModel alloc]initWithDic:responseObject];
                        rowDic= [self witchCellForRow];
                        [userTableView reloadData];
                    }
                    else
                    {
                        [userTableView reloadData];

                        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""  message: @"抱歉，该用户不允许陌生人查看资料"    preferredStyle:UIAlertControllerStyleAlert];
                        
                        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                            [self.navigationController popViewControllerAnimated:YES];
                            
                        }]];
                        [self presentViewController:alertController animated:YES completion:nil];
                        

                    }
                 
                }
          
            }
            
            
        } failure:^(NSError *error) {
            
                      
        }];
        
        
    }

}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:NO];
       [self userInfoChanged];    
   
    

}

@end
