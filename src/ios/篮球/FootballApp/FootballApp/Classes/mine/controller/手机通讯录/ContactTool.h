//
//  ContactTool.h
//  FastCall
//
//  Created by qiu on 15/10/31.
//  Copyright (c) 2015年 guotion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactTool : NSObject
//添加模型数组
@property (nonatomic,strong)NSMutableArray *contModelArray;
//手机号集合
@property (nonatomic,strong)NSMutableArray *phoneArray;

- (NSString *)searchIphoneNumber:(NSString *)iphoneNumber;
@end
