//
//  ContactTool.m
//  FastCall
//
//  Created by qiu on 15/10/31.
//  Copyright (c) 2015年 guotion. All rights reserved.
//

#import "ContactTool.h"
#import "ContactsModel.h"
#import <AddressBookUI/AddressBookUI.h>

@implementation ContactTool
- (NSMutableArray *)contModelArray{
    if (_contModelArray == nil) {
        _contModelArray = [NSMutableArray array];
    }
    return _contModelArray;
}
- (NSMutableArray *)phoneArray{
    if (_phoneArray == nil) {
        _phoneArray = [NSMutableArray array];
    }
    return _phoneArray;
}
- (instancetype)init{
    if (self = [super init]) {
        [self contactTool];
    }
    return self;
}

//查询地电话号码
- (NSString *)searchIphoneNumber:(NSString *)iphoneNumber{
    
    for (ContactsModel *model in self.contModelArray) {
        if ([model.iphoneNumber isEqualToString:iphoneNumber] ) {
            return model.name;
//            NSLog(@"%@",model.name);
        }
    }
    return nil;
}
//获取联系人
- (void)contactTool{
    
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    
    if (status != kABAuthorizationStatusAuthorized ) {
        return;
    }
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    //获取到所有人联系人纪录
    CFArrayRef peopleArray = ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    //遍历所有的联系人纪录
    CFIndex peoplecount = CFArrayGetCount(peopleArray);
    for (CFIndex i = 0 ; i< peoplecount ; i++ ) {
        
        //具体到联系人
        ABRecordRef *person = CFArrayGetValueAtIndex(peopleArray, i);
        
        //获取联系人姓名
        NSString *firstname = (__bridge_transfer NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastname = (__bridge_transfer NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
        //NSLog(@"%@ --%@",lastname,firstname);
        NSString *namestring = nil;
        if(lastname == nil) {
            
            namestring = [NSString stringWithFormat:@"%@",firstname];
        }
        else if(firstname == nil ) {
            namestring = [NSString stringWithFormat:@"%@",lastname];
        }
        else{
            namestring = [NSString stringWithFormat:@"%@%@",lastname,firstname];
        }
        
        
        
        
        
        
        //获取联系人电话
        ABMultiValueRef phone =  ABRecordCopyValue(person,kABPersonPhoneProperty);
        CFIndex phoneCount = ABMultiValueGetCount(phone);
        
        for (CFIndex j = 0 ; j<phoneCount ; j++ ) {
            NSString *phonelvalue = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phone, j);
//            NSLog(@"phonelvalue====%@",phonelvalue);
            
            phonelvalue = [phonelvalue stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
            phonelvalue = [phonelvalue stringByReplacingOccurrencesOfString:@"(" withString:@""];
            
            phonelvalue = [phonelvalue stringByReplacingOccurrencesOfString:@")" withString:@""];
//
//            phonelvalue = [phonelvalue stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            ContactsModel *model = [[ContactsModel alloc] init];
            
            model.name = namestring;
            
            model.iphoneNumber = phonelvalue;
            
            [self.contModelArray addObject:model];
            [self.phoneArray addObject:phonelvalue];
            
        }
        
    }
    
    
    
    CFRelease(addressBook);
    CFRelease(peopleArray);
    
    
}
@end
