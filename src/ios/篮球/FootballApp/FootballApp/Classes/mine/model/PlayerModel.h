//
//  PlayerModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/9.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface PlayerModel : BaseModel
@property(copy,nonatomic)NSString* id;//id
@property(copy,nonatomic)NSString* token;//token
@property(copy,nonatomic)NSString *name;//名字
@property(copy,nonatomic)NSString *realName;//真实名字
@property(copy,nonatomic)NSString *avatar;//头像
@property(copy,nonatomic)NSString *height;//身高
@property(copy,nonatomic)NSString *weight;//体重
@property(copy,nonatomic)NSString *age;//年龄
@property(copy,nonatomic)NSString *position;//位置
@property(copy,nonatomic)NSString *stars;//评分
@property(copy,nonatomic)NSString *credit;//信誉
@property(copy,nonatomic)NSString *matches;//比赛数
@property(copy,nonatomic)NSString *scores;//进球数
@property(copy,nonatomic)NSString *score;//总得分
@property(copy,nonatomic)NSString *backboard;//总篮板
@property(copy,nonatomic)NSString *level;//等级
@property(copy,nonatomic)NSString *number;//球衣号码
@property(copy,nonatomic)NSString *idcard;//身份证号码
@property(copy,nonatomic)NSString *x;//位置
@property(copy,nonatomic)NSString *y;//位置
@property(copy,nonatomic)NSString *ispay;
@property(copy,nonatomic)NSString *myFans;      //我的粉丝数
@property(copy,nonatomic)NSString *myPublish;   //我的发表数
@property(copy,nonatomic)NSString *myFocus;     //我的关注数
@property(copy,nonatomic)NSString *username;
@property(copy,nonatomic)NSMutableArray *teams;//所属球队
@property(retain,nonatomic)id radarDto;//雷达

@property(copy,nonatomic)NSString *scoreCount;//总得分
//@property(copy,nonatomic)NSString *two_rate;
//@property(copy,nonatomic)NSString *three_rate;
//@property(copy,nonatomic)NSString *Freethrow_rate;
@property(copy,nonatomic)NSString *avgCover;
@property(copy,nonatomic)NSString *avgSteals;
@property(copy,nonatomic)NSString *avgBackboard;
@property(copy,nonatomic)NSString *avgHelp;
@property(copy,nonatomic)NSString *avgAnError;
//@property(copy,nonatomic)NSString *two_three;
@property(copy,nonatomic)NSString *joinCount;

@property(nonatomic,assign)BOOL follow;//是否关注




@end
