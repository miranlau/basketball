//
//  TeamModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/9.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"

@interface TeamModel : BaseModel

@property(copy,nonatomic)NSString* id;//id
@property(copy,nonatomic)NSString *name;//球队名字
@property(copy,nonatomic)NSString *logo;//球队logo
@property(copy,nonatomic)NSString *password;//暗号
@property(copy,nonatomic)NSString *leader;//队长id
@property(copy,nonatomic)NSString *leaderName;//队长名字
@property(copy,nonatomic)NSString *mobile;//队长电话
@property(copy,nonatomic)NSString *age;//平均年龄
@property(copy,nonatomic)NSString *stars;//评分
@property(copy,nonatomic)NSString *win;//胜利场次
@property(copy,nonatomic)NSString *deuce;//平局场次
@property(copy,nonatomic)NSString *lose;//失利场次
@property(copy,nonatomic)NSString *level;//等级
@property(copy,nonatomic)NSString *city;//城市
@property(copy,nonatomic)NSString *attr;//球队属性
@property(copy,nonatomic)NSString *right;
@property(retain,nonatomic) id radarDto;//球队属性
@property(copy,nonatomic)NSMutableArray *players;//球员列表

@property(copy,nonatomic)NSString *countScore;
@property(copy,nonatomic)NSString *avgSteals;
@property(copy,nonatomic)NSString *avgHelp;
@property(copy,nonatomic)NSString *avgBackboard;
@property(copy,nonatomic)NSString *avgAnError;
@property(copy,nonatomic)NSString *avgCover;

@property(copy,nonatomic)NSString *joinCount;

@property(nonatomic,assign)BOOL follow;

@end


@interface RadarDto : BaseModel

@property(copy,nonatomic)NSString* common;
@property(copy,nonatomic)NSString* credit;
@property(copy,nonatomic)NSString* fangshou;
@property(copy,nonatomic)NSString* jingong;
@property(copy,nonatomic)NSString* stars;
@property(copy,nonatomic)NSString* sudu;
@property(copy,nonatomic)NSString* tineng;
@end












