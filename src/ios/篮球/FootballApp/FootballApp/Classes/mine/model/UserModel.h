//
//  UserModel.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/16.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseModel.h"


@interface UserModel : BaseModel

@property(copy,nonatomic)NSString*id;
@property(copy,nonatomic)NSString* name;//名字
@property(copy,nonatomic)NSString* avatar;//头像
@property(copy,nonatomic)NSString* age;//年龄
@property(copy,nonatomic)NSString* realName;//年龄
@property(copy,nonatomic)NSString* weight;//体重
@property(copy,nonatomic)NSString* height;//身高
@property(copy,nonatomic)NSString* idcard;//身份证号码

@property(copy,nonatomic)NSString* position;//城市



@property(assign,nonatomic)BOOL chatpush;//体重
@property(assign,nonatomic)BOOL canview;//身高
@property(assign,nonatomic)BOOL autoupdate;//自动更新

@end
