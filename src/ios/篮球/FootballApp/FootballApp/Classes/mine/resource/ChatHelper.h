//
//  ChatHelper.h
//  FootballApp
//
//  Created by 杨涛 on 16/6/2.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyphenateSDK/EMSDK.h>
@interface ChatHelper : NSObject<EMClientDelegate,EMChatManagerDelegate>
{
    MutableBlock reciveNewMessage;
    NSString * _conversationId;
}
+(instancetype)chatService;
+(void)initEMCha;//初始化环信聊天
-(void)resginChatDelegate;//注册消息委托

-(void)reciveNewMessageForConversation:(NSString *)conversationId newMessageBlock:(MutableBlock)block;

@end
