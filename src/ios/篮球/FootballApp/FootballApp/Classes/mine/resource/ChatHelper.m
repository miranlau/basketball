//
//  ChatHelper.m
//  FootballApp
//
//  Created by 杨涛 on 16/6/2.
//  Copyright © 2016年 North. All rights reserved.
//

#import "ChatHelper.h"

@implementation ChatHelper


+(instancetype)chatService{
    
    static ChatHelper *account=nil;
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        account = [[self alloc] init] ;
        
    }) ;
    
    return account;
    
}
+(void)initEMCha{
    //AppKey:注册的AppKey，详细见下面注释。
    //apnsCertName:推送证书名（不需要加后缀），详细见下面注释。
    EMOptions *options = [EMOptions optionsWithAppkey:@"ski-demo#basketball"];
    
    NSString *apns=[AccountHelper shareAccount ].isPublic==YES?@"wocaonierye":@"wocaonidaye";
    
    options.apnsCertName =apns;
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    
    

}
-(void)resginChatDelegate{

    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
}

//收到消息
- (void)didReceiveMessages:(NSArray *)aMessages{
    
    for (EMMessage *message in aMessages) {
        
        if ([message.conversationId  intValue]==[_conversationId intValue] &&reciveNewMessage ) {
            reciveNewMessage(message);
        }
        //如果程序在后台
        if([UIApplication sharedApplication].applicationState ==UIApplicationStateBackground){
            
            
            
            if ([AccountHelper shareAccount].user.chatpush) {
                
                [self showNotificationWithMessage];
            }
            
            
        
        }
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RECIVENEWMESSAGE object:nil];
    
}


-(void)reciveNewMessageForConversation:(NSString *)conversationId newMessageBlock:(MutableBlock)block{
    reciveNewMessage=block;

    _conversationId=conversationId;

}

- (void)showNotificationWithMessage
{
   
    //发送本地推送
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date]; //触发通知的时间
    
    notification.alertBody=@"你有一条新的消息";
      notification.soundName = UILocalNotificationDefaultSoundName;
    //发送通知
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        UIApplication *application = [UIApplication sharedApplication];
        application.applicationIconBadgeNumber += 1;
}
@end
