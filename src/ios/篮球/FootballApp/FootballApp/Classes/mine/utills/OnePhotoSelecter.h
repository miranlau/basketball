//
//  OnePhotoSelecter.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/9.
//  Copyright © 2016年 North. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnePhotoSelecter : NSObject<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    MutableBlock  myBlock;
    UIViewController *superViewController;

}

/*
 superVC 需要调用相机的ViewController
 block 得到照片之后的回调
 UIImagePickerControllerSourceType 相册还是相机
 */
+(instancetype)shareInstance;

-(void)selectImage:(UIViewController *)superVC imageBlock:(MutableBlock)block;
@end
