//
//  OnePhotoSelecter.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/9.
//  Copyright © 2016年 North. All rights reserved.
//

#import "OnePhotoSelecter.h"

@implementation OnePhotoSelecter
+(instancetype)shareInstance{
    
    static OnePhotoSelecter *imageSelect=nil;
    
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        imageSelect = [[self alloc] init] ;
    }) ;
    
    return imageSelect ;
    
}

-(void)selectImage:(UIViewController *)superVC imageBlock:(MutableBlock)block{
    
    
    superViewController=superVC;
    myBlock=block;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选择图片" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"从相册选一张" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self selectPhotoAlbumPhotos];
        
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"现在照一张" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self selectCamera];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消"
                                                        style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction *action) {
                                                          // 此处处理点击退出按钮逻辑
                                                          
                                                      }]];
    [superVC presentViewController:alertController animated:YES completion:nil];
}


/**
 *  相册选择照片
 */
- (void)selectPhotoAlbumPhotos {
    //获取支持的媒体格式
    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    //判断是否支持需要设置的sourceType
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        //1.初始化图片拾取器
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        //2.设置图片拾取器上得sourceType
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //3.设置支持的媒体格式
        imagePickerController.delegate=self;
        imagePickerController.mediaTypes = @[mediaTypes[0]];
        //4.其他设置
        //        imagePickerController.allowsEditing = YES;
        //5.推送图片拾取器控制器
        
        [superViewController presentViewController:imagePickerController animated:YES completion:nil];
    }
}
/**
 *  使用照相机
 */
- (void)selectCamera {
    
    //获取支持的媒体格式
    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    //判断是否支持需要设置的sourceType
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        //1.初始化图片拾取器
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        //2.设置图片拾取器上得sourceType
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        //3.设置支持的媒体格式
        imagePickerController.delegate=self;
        imagePickerController.mediaTypes = @[mediaTypes[0]];
        //默认打开后置摄像头
        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //        前置摄像头
        //        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        //4.其他设置
        //        imagePickerController.allowsEditing = YES;
        //5.推送图片拾取器控制器
        [superViewController presentViewController:imagePickerController animated:YES completion:nil];
    }
}

//当用户选择了某一张图片或编辑使用了某张图片后触发此方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *editedImage = info[@"UIImagePickerControllerOriginalImage"];
    UIImage *image=[self sizedImage:editedImage withMaxValue:480];
    myBlock(image);
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
//图片压缩处理
- (UIImage *)sizedImage:(UIImage *)originalImage withMaxValue:(float)maxValue
{
    UIImage *image = originalImage;
    CGFloat var1;
    CGFloat var2;
    
    if (image.size.width > image.size.height){
        // Landscape
        CGFloat aspect = image.size.width / image.size.height;
        CGFloat height = ceilf(maxValue/aspect);
        var1 = maxValue;
        var2 = height;
    }
    else if (image.size.height > image.size.width){
        // Portrait
        CGFloat aspect = image.size.height / image.size.width;
        CGFloat width = ceilf(maxValue/aspect);
        var1 = width;
        var2 = maxValue;
    }
    else{
        // Square
        var1 = maxValue;
        var2 = maxValue;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(var1, var2), NO, 1);
    [image drawInRect:CGRectMake(0, 0, var1, var2)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
