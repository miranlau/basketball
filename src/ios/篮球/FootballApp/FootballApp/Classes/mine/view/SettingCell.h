//
//  SettingCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BaseCell.h"

@interface SettingCell : BaseCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *mobile;
@property (weak, nonatomic) IBOutlet UILabel *cellName;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UISwitch *swit;
@property (nonatomic,assign)    BOOL canview;
@property (nonatomic,assign)    BOOL chatpush;
@property (nonatomic,assign)    BOOL autoupdate;
@end
