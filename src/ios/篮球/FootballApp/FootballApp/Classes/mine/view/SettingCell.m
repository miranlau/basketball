//
//  SettingCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/5/3.
//  Copyright © 2016年 North. All rights reserved.
//

#import "SettingCell.h"
#import <HyphenateSDK/EMSDK.h>
@implementation SettingCell

-(void)getdata:(id)data{
    
    BaseCellModel *model=(BaseCellModel *)data;

    if ([model.reuseIdentifier isEqualToString:@"SettingCell1"]) {
        
        
        
        [_headImageView setImageWithURL:[NSURL URLWithString:[AccountHelper shareAccount].player.avatar] placeholderImage:nil];
        
        [_headImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
        
        _headImageView.layer.cornerRadius=35;
        _headImageView.layer.masksToBounds=YES;
        _headImageView.layer.borderColor=[UIColor whiteColor].CGColor;
        _headImageView.layer.borderWidth=1;
        
        _mobile.text=[AccountHelper shareAccount].account;
         _userName.text=[AccountHelper shareAccount].player.name;
//        if ([AccountHelper shareAccount].player.realName)
//        {
//            _userName.text=[AccountHelper shareAccount].player.realName;
//        }
//        else
//        {
//            _userName.text=[AccountHelper shareAccount].player.name;
//        }
       
    }
    
    if ([model.reuseIdentifier isEqualToString:@"SettingCell2"]) {
         self.cellName.text=model.name;
        self.infoLabel.text=[NSString stringWithFormat:@"%@%@",model.value,model.key];
        
        if ([self.infoLabel.text isEqualToString:@"(null)"]) {
            
            self.infoLabel.text=@"";
        }
        
    }
    
    if ([model.reuseIdentifier isEqualToString:@"SettingCell3"]) {
         self.cellName.text=model.name;
        self.swit.tag = [model.value intValue];

        if([model.value isEqualToString:@"0"])
        {
            
            if([AccountHelper shareAccount].user.canview) //允许他人查看我的资料
            {
                [self.swit setOn:YES animated:YES];
            }
            else
            {
                [self.swit setOn:NO animated:YES];
            }
        }
        
        if([model.value isEqualToString:@"1"])
        {
            
            if([AccountHelper shareAccount].user.chatpush)  //允许聊天信息通知
            {
                [self.swit setOn:YES animated:YES];
                
                
                
                
                
            }
            else
            {
                [self.swit setOn:NO animated:YES];
            }
        }
        
        if([model.value isEqualToString:@"2"])
        {
           
            if([AccountHelper shareAccount].user.autoupdate)  //自动下载更新并提示安装
            {
                
                [self.swit setOn:YES animated:YES];
            }
            else
            {
               
                [self.swit setOn:NO animated:YES];
            }
        }
        if([model.value isEqualToString:@"3"])
        {
             //允许打开通讯录
            NSString *str = [Defaults objectForKey:@"canuploadmyphones"];
            if ([str isEqualToString:@"canuploadmyphones"])
            {
                
                [self.swit setOn:NO animated:YES];
                [Defaults setObject:@"canuploadmyphones" forKey:@"canuploadmyphones"];
                [Defaults synchronize];
            }
            else
            {
                 [self.swit setOn:YES animated:YES];
                
                 [Defaults removeObjectForKey:@"canuploadmyphones"];

            }
           
        }
    }
  
  

}
- (IBAction)SwitchClicked:(UISwitch *)sender {
    if ( self.swit.tag == 0)
    {
        if (self.swit.isOn) {
            
            [self.swit setOn:YES animated:YES];
            _canview = YES;
             [self SwitchOnOrOff:@"canview" And:_canview];
        }
        else
        {
            [self.swit setOn:NO animated:YES];
            _canview = NO;
             [self SwitchOnOrOff:@"canview" And:_canview];
        }
    }
    
    if ( self.swit.tag == 1)
    {
        if (self.swit.isOn) {
            
            [self.swit setOn:YES animated:YES];
            _chatpush = YES;
             [self SwitchOnOrOff:@"chatpush" And:_chatpush];
        }
        else
        {
            [self.swit setOn:NO animated:YES];
            _chatpush = NO;
             [self SwitchOnOrOff:@"chatpush" And:_chatpush];
        }
        [AccountHelper shareAccount].user.chatpush=_chatpush;
        [self updateNoti:_chatpush];
 
    }
    if ( self.swit.tag == 2)
    {
        if (self.swit.isOn) {
            
             [self.swit setOn:YES animated:YES];
             _autoupdate = YES;
             [self SwitchOnOrOff:@"autoupdate" And:_autoupdate];
        }
        else
        {
            [self.swit setOn:NO animated:YES];
             _autoupdate = NO;
            [self SwitchOnOrOff:@"autoupdate" And:_autoupdate];
        }
    }
    if ( self.swit.tag == 3)
    {
        if (self.swit.isOn) {
            
            [self.swit setOn:YES animated:YES];
            [Defaults removeObjectForKey:@"canuploadmyphones"];
            
        }
        else
        {
            [self.swit setOn:NO animated:YES];
           
            [Defaults setObject:@"canuploadmyphones" forKey:@"canuploadmyphones"];
            [Defaults synchronize];
            
        }
    }
    
}
-(void)updateNoti:(BOOL)flag{

    EMPushOptions *options = [[EMClient sharedClient] pushOptions];
    options.noDisturbStatus =flag?EMPushNoDisturbStatusDay:EMPushNoDisturbStatusClose;
    [[EMClient sharedClient] updatePushOptionsToServer];
    
    


}

//设置的三个UISwitch的网络url
- (void)SwitchOnOrOff:(NSString *)str And:(BOOL)changes
{

    [HttpRequestHelper postWithURL:Url(@"user/modifyData") params:@{str:[NSNumber numberWithBool:changes]} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            //服务器布尔字段会变化
          
        }
    
        
    } failure:^(NSError *error) {
        
    }];
}
-(void)headImageClick:(id)sender{

    self.cellBlock(@"head");//修改头像

}

@end
