//
//  UserCell.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "BooksCell.h"
#import "MBStarScore.h"

@interface UserCell : BooksCell
@property (weak, nonatomic) IBOutlet UILabel *gzNum;
@property (weak, nonatomic) IBOutlet UILabel *messageNum;
@property (weak, nonatomic) IBOutlet UILabel *fansNum;
@property (weak, nonatomic) IBOutlet UILabel *publicNum;
@property (weak, nonatomic) IBOutlet UILabel *height;
@property (weak, nonatomic) IBOutlet UILabel *weith;
@property (weak, nonatomic) IBOutlet UILabel *age;
@property (weak, nonatomic) IBOutlet UILabel *bmi;
@property (weak, nonatomic) IBOutlet MBStarScore *stares;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *matches;
@property (weak, nonatomic) IBOutlet UILabel *level;
@property (weak, nonatomic) IBOutlet UILabel *scores;

@property (weak, nonatomic) IBOutlet UIButton *isFollow;

@end
