//
//  UserCell.m
//  FootballApp
//
//  Created by 杨涛 on 16/4/28.
//  Copyright © 2016年 North. All rights reserved.
//

#import "UserCell.h"
#import "AvatarHelper.h"
#import <HyphenateSDK/EMSDK.h>
@implementation UserCell

-(void)getdata:(id)data{
    
    BaseCellModel *model=(BaseCellModel *)data;
    if ([model.reuseIdentifier isEqualToString:@"UserCell1"]||[model.reuseIdentifier isEqualToString:@"UserCell2"]) {
        
        _headImageView.layer.cornerRadius=37;
        _headImageView.layer.masksToBounds=YES;
        
        _headImageView.layer.borderColor=[UIColor whiteColor].CGColor;
        _headImageView.layer.borderWidth=1;
        
        PlayerModel*play= (PlayerModel*) model.value;
        
        [_headImageView setImageWithURL:[NSURL URLWithString:play.avatar] placeholderImage:HeadImage];
        
        [_headImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headImageClick:)]];
        
        
        _name.text= [NSString stringWithFormat:@"%@",play.name];
        _matches.text=[NSString stringWithFormat:@"%@",play.matches];
        _scores.text=[NSString stringWithFormat:@"%@",play.score];
        _level.text=[NSString stringWithFormat:@"%@",play.backboard];
        _stares.show_star=[play.stars intValue]/20;
        
        if(play.follow)
        {
            [_isFollow setTitle:@"取消关注" forState:UIControlStateNormal];
        }
        else
        {
            [_isFollow setTitle:@"+关注" forState:UIControlStateNormal];
        }
        
    }
    
    if ([model.reuseIdentifier isEqualToString:@"UserCell4"]){
        
        
        PlayerModel *player=model.value;
        _weith.text=[NSString stringWithFormat:@"%@",player.weight];
        _height.text=[NSString stringWithFormat:@"%@",player.height];
        _age.text=[NSString stringWithFormat:@"%@",player.age];
        
        if ([player.weight doubleValue] >0 && [player.height intValue]>0) {
            
            CGFloat ti=([player.height doubleValue]/100) *([player.height doubleValue]/100);
            
            CGFloat bmi= [player.weight doubleValue]/ti;
            
            _bmi.text=[NSString stringWithFormat:@"%.1f",bmi];
        }else{
            _bmi.text=@"0";
        }
        
        
        
    }
    
    //这里显示我所关注的数量，我的粉丝数，我的发表动态条数
    if ([model.reuseIdentifier isEqualToString:@"UserCell3"]){
        
        PlayerModel *player =  [AccountHelper shareAccount].player;
        _gzNum.text         =[NSString stringWithFormat:@"%@",player.myFocus];
        _fansNum.text       =[NSString stringWithFormat:@"%@",player.myFans];
        _publicNum.text     =[NSString stringWithFormat:@"%@",player.myPublish];
        
        
        
        NSArray *arr=[[EMClient sharedClient].chatManager getAllConversations];
        
        int num=0;
        
        for (EMConversation *con  in arr) {
            num+=con.unreadMessagesCount;
        }
        
        
        _messageNum.text     =[NSString stringWithFormat:@"%d",num];
    }
    
    
    
    
    
    
}

-(void)headImageClick:(UITapGestureRecognizer *)tap{
    
    [AvatarHelper showImage:(UIImageView *)tap.view];
    
    
}
//关注

- (IBAction)guanzhuBtnClick:(id)sender {
    
    if (self.cellBlock) {
        self.cellBlock(@(1));
    }
    
}
- (IBAction)riliBtnClick:(id)sender {
    
    
    if (self.cellBlock) {
        self.cellBlock(@(2));
    }
    
    
}
//关注列表
- (IBAction)gzBtnClick:(id)sender {
    if (self.cellBlock) {
        self.cellBlock(@(3));
    }
    
    
}
- (IBAction)fansBtnClick:(id)sender {
    
    if (self.cellBlock) {
        self.cellBlock(@(4));
    }
    
}
- (IBAction)publickBtnClick:(id)sender {
    
    
    if (self.cellBlock) {
        self.cellBlock(@(5));
    }
}
- (IBAction)messageBtnClick:(id)sender {
    
    
    if (self.cellBlock) {
        self.cellBlock(@(6));
    }
}
@end
