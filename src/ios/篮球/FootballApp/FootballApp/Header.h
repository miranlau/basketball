//
//  Header.h
//  FootballApp
//
//  Created by 杨涛 on 16/4/13.
//  Copyright © 2016年 North. All rights reserved.
//

#ifndef Header_h
#define Header_h

//物理宽度
#define kScreenWidth [[UIScreen mainScreen]bounds].size.width
//物理高度
#define kScreenHeight [[UIScreen mainScreen]bounds].size.height
// View 坐标(x,y)和宽高(width,height)
#define X(v)                    (v).frame.origin.x
#define Y(v)                    (v).frame.origin.y
#define WIDTH(v)                (v).frame.size.width
#define HEIGHT(v)               (v).frame.size.height

#define MinX(v)                 CGRectGetMinX((v).frame)
#define MinY(v)                 CGRectGetMinY((v).frame)

#define MidX(v)                 CGRectGetMidX((v).frame)
#define MidY(v)                 CGRectGetMidY((v).frame)

#define MaxX(v)                 CGRectGetMaxX((v).frame)
#define MaxY(v)                 CGRectGetMaxY((v).frame)


#define RECT_CHANGE_x(v,x)          CGRectMake(x, Y(v), WIDTH(v), HEIGHT(v))
#define RECT_CHANGE_y(v,y)          CGRectMake(X(v), y, WIDTH(v), HEIGHT(v))
#define RECT_CHANGE_point(v,x,y)    CGRectMake(x, y, WIDTH(v), HEIGHT(v))
#define RECT_CHANGE_width(v,w)      CGRectMake(X(v), Y(v), w, HEIGHT(v))
#define RECT_CHANGE_height(v,h)     CGRectMake(X(v), Y(v), WIDTH(v), h)
#define RECT_CHANGE_size(v,w,h)     CGRectMake(X(v), Y(v), w, h)
// 颜色(RGB)
#define RGBCOLOR(r, g, b)       [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r, g, b, a)   [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

//number转String
#define IntTranslateStr(int_str) [NSString stringWithFormat:@"%d",int_str];
#define FloatTranslateStr(float_str) [NSString stringWithFormat:@"%.2d",float_str];

// View 圆角和加边框
#define ViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

// View 圆角
#define ViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

#define COLOR [[UIColor alloc]init]

#define Defaults [NSUserDefaults standardUserDefaults]

#define BGCOLOR1 [UIColor colorWithRed:35/255.0 green:34/255.0 blue:36/255.0 alpha:.8]
#define BGCOLOR2 [UIColor colorWithRed:46/255.0 green:45/255.0 blue:47/255.0 alpha:.8]
#define TODAYCOLOR [UIColor colorWithRed:236/255.0  green:147/255.0 blue:2/255.0 alpha:1]
#define WERED [UIColor colorWithRed:236/255.0  green:94/255.0 blue:106/255.0 alpha:1]



#define COLOR1 [UIColor colorWithRed:235/255.0  green:79/255.0 blue:56/255.0 alpha:1]
#define COLOR2 [UIColor colorWithRed:19/255.0  green:204/255.0 blue:100/255.0 alpha:1]
#define COLOR3 [UIColor colorWithRed:78/255.0  green:152/255.0 blue:193/255.0 alpha:1]
#define COLOR4 [UIColor colorWithRed:158/255.0  green:84/255.0 blue:185/255.0 alpha:1]
#define COLOR5 [UIColor colorWithRed:51/255.0  green:71/255.0 blue:95/255.0 alpha:1]
#define COLOR6 [UIColor colorWithRed:241/255.0  green:155/255.0 blue:34/255.0 alpha:1]


#define BarColor [COLOR colorWithHexString:@"2e2e30"] //导航条 tabbar颜色
#define BGIMG [UIImage imageNamed:@"大背景图.png"]

#define HeadImage [UIImage imageNamed:@"默认头像.png"]//默认头像
#define NonePhoto [UIImage imageNamed:@"nonePhoto.png"]//默认加载失败图片
#define PlaceHold [UIImage imageNamed:@"placeholder.png"]//默认加载失败图片
#define UnLogin [UIImage imageNamed:@"avatar.png.png"]//未登录图片
#define TeamLogo [UIImage imageNamed:@"默认球队.png"]//未登录图片
#define DELEGATE [[UIApplication sharedApplication] delegate]


#define ACCOLOR1 [UIColor colorWithRed:238/255.0  green:156/255.0 blue:34/255.0 alpha:1]
#define ACCOLOR2 [UIColor colorWithRed:220/255.0  green:76/255.0 blue:54/255.0 alpha:1]
#define ACCOLOR3 [UIColor colorWithRed:75/255.0  green:152/255.0 blue:192/255.0 alpha:1]
#define ACCOLOR4 [UIColor colorWithRed:50/255.0  green:72/255.0 blue:95/255.0 alpha:1]

//环信
//Client Id:	YXA6fMuJcFhfEean-TNETw_Dbg
//Client Secret:	YXA63aOP7tydzms5KgpDEqSaaO8MbvM
//
//极光
//AppKey:404583ac6a1caf2a8510a806
//Master Secret:e2b8889cae810eb4e51498f0
//
//高德足球
//Android key:4c34a0faec27581a97a1a2424f1ed7cd
//iOS key:99645140362618d12cc62b90d68e013f

//地图
#define MAP_KEY @"4c34a0faec27581a97a1a2424f1ed7cd"
//极光
#define JP_KEY @"404583ac6a1caf2a8510a806"
#define JP_Channel @"Publish channel"


#define RECIVENEWMESSAGE @"newMessage"

typedef void(^MutableBlock)(id data);//公用块

#endif /* Header_h */
