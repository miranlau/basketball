//
//  MBBrowserImageView.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBWaitingView.h"
#import "MBSetConfig.h"
#import "UIImageView+WebCache.h"

@interface MBBrowserImageView : UIImageView <UIGestureRecognizerDelegate>
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, assign, readonly) BOOL isScaled;
@property (nonatomic, assign) BOOL hasLoadedImage;

- (void)eliminateScale; // 清除缩放

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder;

- (void)doubleTapToZommWithScale:(CGFloat)scale;

- (void)clear;
@end
