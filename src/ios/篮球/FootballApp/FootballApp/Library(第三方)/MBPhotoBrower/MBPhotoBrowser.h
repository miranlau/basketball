//
//  MBPhotoBrowser.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBSetConfig.h"
#import "MBBrowserImageView.h"
@interface MBPhotoBrowser : UIView<UIScrollViewDelegate>

@property(retain,nonatomic)NSMutableArray *viewArr;//存放图片的imgView的数组
@property(retain,nonatomic)NSMutableArray *urlsArr;//高清图片的数组、不传代表本地图片

-(void)Show:(UIImageView *)imageView;//当前点击的view
@end

