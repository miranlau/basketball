//
//  MBSetConfig.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#ifndef MBSetConfig_h
#define MBSetConfig_h

typedef enum {
    MBWaitingViewModeLoopDiagram, // 环形
    MBWaitingViewModePieDiagram // 饼型
} MBWaitingViewMode;

// 图片保存成功提示文字
#define MBPhotoBrowserSaveImageSuccessText @" ^_^ 保存成功 ";

// 图片保存失败提示文字
#define MBPhotoBrowserSaveImageFailText @" >_< 保存失败 ";

// browser背景颜色
#define MBPhotoBrowserBackgrounColor [UIColor colorWithRed:0 green:0 blue:0 alpha:0.95]

// browser中图片间的margin
#define MBPhotoBrowserImageViewMargin 10

// browser中显示图片动画时长
#define MBPhotoBrowserShowImageAnimationDuration 0.3f

// browser中显示图片动画时长
#define MBPhotoBrowserHideImageAnimationDuration 0.3f

// 图片下载进度指示进度显示样式（SDWaitingViewModeLoopDiagram 环形，SDWaitingViewModePieDiagram 饼型）
#define MBWaitingViewProgressMode MBWaitingViewModeLoopDiagram

// 图片下载进度指示器背景色
#define MBWaitingViewBackgroundColor [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]

// 图片下载进度指示器内部控件间的间距
#define MBWaitingViewItemMargin 10

#endif /* MBSetConfig_h */
