//
//  MBWaitingView.h
//  FootballApp
//
//  Created by 杨涛 on 16/5/17.
//  Copyright © 2016年 North. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBSetConfig.h"
@interface MBWaitingView : UIView
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, assign) int mode;
@end
