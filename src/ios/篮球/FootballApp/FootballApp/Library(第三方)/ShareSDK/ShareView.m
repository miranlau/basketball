//
//  ShareView.m
//  天巢网
//
//  Created by 赵贺 on 15/11/20.
//  Copyright © 2015年 tangjp. All rights reserved.
//

#import "ShareView.h"
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>


/*   <!-- 应用规范字体大小 -->*/
#define  text_size_big_1    21
#define  text_size_big_2    20
#define  text_size_big_3    19
#define  text_size_middle_1 18
#define  text_size_middle_2 17
#define  text_size_middle_3 15
#define  text_size_little_1 14
#define  text_size_little_2 13
#define  text_size_little_3 12
#define  text_size_little_4 10
#define  text_size_other    16

#define  AppFont(c_font) [UIFont systemFontOfSize:c_font]
#define ApplicationframeValue [[UIScreen mainScreen]bounds].size


//**提示框宏定义
CG_INLINE void AlertLog (NSString *titleStr,NSString *message,NSString *button1,NSString *button2)
{
    if(!titleStr)
        titleStr = @"";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: titleStr
                                                        message: message
                                                       delegate: nil
                                              cancelButtonTitle: button1
                                              otherButtonTitles: button2,
                              nil];
    [alertView show];
    
}
@interface ShareView ()
{
    UIImage *IconImage;
}
@property (nonatomic,strong)NSArray *imageArr;
@property (nonatomic,strong)NSArray *titleArr;

@end



@implementation ShareView


-(NSArray *)imageArr
{
    //QQ空间，微信好友，朋友圈，新浪微博
    if (!_imageArr) { //@"wechatmoments.png",@"classic_wechat.png",
        _imageArr = [NSArray arrayWithObjects:@"qzone.png",@"sinaweibo.png",@"theshareCircle.png",@"wechatmoments",@"classic_wechat",nil];
        //         _imageArr = [NSArray arrayWithObjects:@"分享_朋友圈",@"分享_微信",@"分享_QQ",@"qzone.png",nil];
    }
    return _imageArr;
}

-(NSArray *)titleArr
{
    
    if (!_titleArr) {
        //        _titleArr = [NSArray arrayWithObjects:@"朋友圈",@"微信好友",@"QQ",@"QQ空间",nil];
        _titleArr = [NSArray arrayWithObjects:@"QQ空间",@"新浪微博",@"圈子",@"朋友圈",@"微信好友",nil];
    }
    return _titleArr;
}

-(id)initWithFrame:(CGRect)frame
{
    float Margin = ceilf(ApplicationframeValue.width/self.imageArr.count/2);
    
    self = [super initWithFrame:frame];
    if (self) {
        self.overlayView = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.overlayView.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
        [self.overlayView addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *viewS = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ApplicationframeValue.width, 160)];
        viewS.backgroundColor =[UIColor whiteColor];
        [self addSubview:viewS];
        
        UILabel *TitleLabel = [[UILabel alloc] initWithFrame:CGRectMake((ApplicationframeValue.width-100)/2, 15, 100, 20)];
        TitleLabel.textAlignment = NSTextAlignmentCenter;
        TitleLabel.text = @"分享到";
        TitleLabel.backgroundColor = [UIColor whiteColor];
        TitleLabel.font = AppFont(text_size_other);
        [viewS addSubview:TitleLabel];
        
        UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 45, ApplicationframeValue.width, 1)];
        lineImage.alpha = 0.5f;
        lineImage.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4f];
        [viewS addSubview:lineImage];
        
        for (int i = 0; i < [self.imageArr count]; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.tag = i +1;
            btn.frame = CGRectMake(Margin/2+2*Margin*i,60, Margin, Margin);
            [btn setBackgroundImage:[UIImage imageNamed:self.imageArr[i]] forState:UIControlStateNormal];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(Margin/2+2*Margin*i - 9, CGRectGetMaxY(btn.frame)+10, Margin+16, 15)];
            
            label.textAlignment = NSTextAlignmentCenter;
            label.text = self.titleArr[i];
            //            label.backgroundColor = [UIColor redColor];
            label.font = AppFont(text_size_little_3);
            //            label.adjustsFontSizeToFitWidth = YES;
            [btn addTarget:self action:@selector(onBtn:) forControlEvents:UIControlEventTouchUpInside];
            [viewS addSubview:label];
            [viewS addSubview:btn];
        }
        
        //        UIButton *buttonCancel = [[UIButton alloc]initWithFrame:CGRectMake(20, 170, 335, 35)];
        //        buttonCancel.backgroundColor = [UIColor lightGrayColor];
        //        buttonCancel.titleLabel.font = AppFont(text_size_little_2);
        //        buttonCancel.layer.cornerRadius = 4;
        //    [buttonCancel addTarget:self action:@selector(cancellss) forControlEvents:UIControlEventTouchUpInside];
        //        [buttonCancel setTitle:@"取消" forState:UIControlStateNormal];
        //
        //         [viewS addSubview:buttonCancel];
        
        
        
    }
    return self;
}

//- (void)cancellss
//{
//    [self fadeOut];
//}

-(void)onBtn:(UIButton *)sender{
    
    switch (sender.tag) {
            //朋友圈
        case 4:{
            if ([WXApi isWXAppInstalled]) {
                id<ISSContent> publishContent = [ShareSDK content:nil defaultContent:nil image:nil title:nil url:nil description:nil             mediaType:SSPublishContentMediaTypeNews];
                [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeNews]content:self.message?self.message:INHERIT_VALUE
                                                        title:self.title?self.title:INHERIT_VALUE  url:self.shareUrl?self.shareUrl:INHERIT_VALUE
                 
                                                   thumbImage:[ShareSDK imageWithUrl:self.pictureName]
                                                        image:INHERIT_VALUE
                                                 musicFileUrl:nil
                                                      extInfo:nil
                                                     fileData:nil
                                                 emoticonData:nil];
                
                [ShareSDK showShareViewWithType:ShareTypeWeixiTimeline container:nil content:publishContent statusBarTips:NO authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                    if (state == SSResponseStateSuccess)
                    {
                        [self fadeOut];
                        AlertLog(@"", @"分享成功", @"确定", nil);
                    }
                    else if (state == SSResponseStateFail)
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alert show];
                    }
                    
                }];
            }
            
            else{
                AlertLog(@"", @"没有安装微信客户端，无法进行微信分享", @"确定", nil);
            }
        }
            break;
            
            
            //微信好友
        case 5:{
            if ([WXApi isWXAppInstalled]) {
                id<ISSContent> publishContent = [ShareSDK content:nil defaultContent:nil image:nil title:nil url:nil description:nil mediaType:SSPublishContentMediaTypeText];
                [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeNews]
                                                     content:self.message?self.message:INHERIT_VALUE
                                                       title:self.title?self.title:INHERIT_VALUE
                                                         url:self.shareUrl?self.shareUrl:INHERIT_VALUE
                                                  thumbImage:[ShareSDK imageWithUrl:self.pictureName]
                                                       image:INHERIT_VALUE
                                                musicFileUrl:nil
                                                     extInfo:nil
                                                    fileData:nil
                                                emoticonData:nil];
                
                
                [ShareSDK showShareViewWithType:ShareTypeWeixiSession container:nil content:publishContent statusBarTips:NO authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                    if (state == SSResponseStateSuccess)
                    {
                        [self fadeOut];
                        AlertLog(@"", @"分享成功", @"确定", nil);
                    }
                    else if (state == SSResponseStateFail)
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alert show];
                    }
                    
                }];
            }
            else{
                AlertLog(@"", @"没有安装微信客户端，无法进行微信分享", @"确定", nil);
            }
        }
            break;
            //QQ好友
            //        case 3:{
            //            if ([QQApiInterface isQQInstalled]) {
            //                id<ISSContent> publishContent = [ShareSDK content:nil defaultContent:nil image:nil title:nil url:nil description:nil mediaType:SSPublishContentMediaTypeNews];
            //                [publishContent addQQUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeNews]
            //                                          content:self.message?self.message:INHERIT_VALUE
            //                                            title:self.title?self.title:INHERIT_VALUE
            //                                              url:self.shareUrl?self.shareUrl:INHERIT_VALUE
            //                                              image:[ShareSDK imageWithPath:[[NSBundle mainBundle] pathForResource:self.pictureName ofType:@"png"]]];
            //
            //
            //                 [ShareSDK showShareViewWithType:ShareTypeQQ container:nil content:publishContent statusBarTips:NO authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
            //                     if (state == SSResponseStateSuccess)
            //                     {
            //                         AlertLog(@"", @"分享成功", @"确定", nil);
            //                     }
            //                     else if (state == SSResponseStateFail)
            //                     {
            //                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //                         [alert show];
            //                     }
            //
            //                 }];
            //            }
            //            else{
            //                AlertLog(@"", @"没有安装QQ客户端，无法进行QQ分享", @"确定", nil);
            //
            //                 }
            //            }
            //                 break;
            //新浪微博
        case 2:{
            
            NSString *description = [NSString stringWithFormat:@"%@ , %@" ,self.message?self.message:INHERIT_VALUE,self.shareUrl?self.shareUrl:INHERIT_VALUE];
            id<ISSContent> publishContent = [ShareSDK content:description defaultContent:nil image:nil title:self.title?self.title:INHERIT_VALUE url:self.shareUrl?self.shareUrl:INHERIT_VALUE description:@"descripe" mediaType:SSPublishContentMediaTypeNews];
            //
            //            if (![ShareSDK hasAuthorizedWithType:ShareTypeSinaWeibo]) {
            //                [ShareSDK authWithType:ShareTypeSinaWeibo options:nil result:^(SSAuthState state, id<ICMErrorInfo> error) {
            //                    if (state == SSAuthStateSuccess) {
            //
            //                        [publishContent addSinaWeiboUnitWithContent:self.message?self.message:INHERIT_VALUE image:INHERIT_VALUE];
            //
            //
            //                        [ShareSDK showShareViewWithType:ShareTypeSinaWeibo container:nil content:publishContent statusBarTips:NO authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
            //                            if (state == SSResponseStateSuccess)
            //                            {
            //                                AlertLog(@"", @"分享成功", @"确定", nil);
            //                            }
            //                            else if (state == SSResponseStateFail)
            //                            {
            //                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //                                [alert show];
            //                            }
            //                        }];
            //                    }
            //                    else
            //                    {
            //                        NSLog(@"失败了，something wrong");
            //
            //
            //                    }
            //
            //                }];
            //
            //            } else {
            //
            //                 NSLog(@"授权失败了，shit");
            //                [publishContent addSinaWeiboUnitWithContent:self.message?self.message:INHERIT_VALUE image:INHERIT_VALUE];
            //
            //                [ShareSDK showShareViewWithType:ShareTypeSinaWeibo container:nil content:publishContent statusBarTips:NO authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
            //                    if (state == SSResponseStateSuccess)
            //                    {
            //                        AlertLog(@"", @"分享成功", @"确定", nil);
            //                    }
            //                    else if (state == SSResponseStateFail)
            //                    {
            //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //                        [alert show];
            //                    }
            //                }];
            //
            //            }
            
            
            
            
            id<ISSContainer>
            container = [ShareSDK container];
            
            [container
             setIPadContainerWithView:sender
             
             arrowDirect:UIPopoverArrowDirectionUp];
            
            
            [ShareSDK showShareViewWithType:ShareTypeSinaWeibo container:container content:publishContent statusBarTips:YES authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                
                NSLog(@"haha:%@,and %@",statusInfo,error);
                
                
                if(state
                   == SSPublishContentStateSuccess)
                    
                {
                    
                    [self fadeOut];
                    AlertLog(@"", @"分享成功", @"确定", nil);
                }
                
                else if(state
                        == SSPublishContentStateFail)
                    
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    NSLog(@"发布失败!errorcode == %ld, error code == %@",
                          (long)[error errorCode], [error errorDescription]);
                    
                }
            }];
            //显示分享菜单
            
            
            //            id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
            //                                                                 allowCallback:YES
            //                                                                 authViewStyle:SSAuthViewStyleFullScreenPopup
            //                                                                  viewDelegate:nil
            //                                                       authManagerViewDelegate:self];
            //            [ShareSDK showShareViewWithType:ShareTypeSinaWeibo
            //                                  container:container
            //                                    content:publishContent
            //                              statusBarTips:YES
            //                                authOptions:authOptions
            //                               shareOptions:[ShareSDK defaultShareOptionsWithTitle:nil
            //                                                                   oneKeyShareList:[NSArray defaultOneKeyShareList]
            //                                                                    qqButtonHidden:NO
            //                                                             wxSessionButtonHidden:NO
            //                                                            wxTimelineButtonHidden:NO
            //                                                              showKeyboardOnAppear:NO
            //                                                                 shareViewDelegate:self
            //                                                               friendsViewDelegate:self
            //                                                             picViewerViewDelegate:nil]
            //                                     result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
            //
            //                                         if (state == SSPublishContentStateSuccess)
            //                                         {
            //                                             NSLog(@"发表成功");
            //                                         }
            //                                         else if (state == SSPublishContentStateFail)
            //                                         {
            //                                             NSLog(@"发布失败!error code == %ld, error code == %@", (long)[error errorCode], [error errorDescription]);
            //                                         }
            //                                     }];
            
            
            
        }
            break;
            //QQ空间
        case 1:{
            if ([QQApiInterface isQQInstalled]) {
                
                
                
                id<ISSContent> publishContent = [ShareSDK content:nil defaultContent:nil image:nil title:nil url:nil description:nil mediaType:SSPublishContentMediaTypeNews];
                
                [publishContent addQQSpaceUnitWithtitle:self.title?self.title:INHERIT_VALUE url:self.shareUrl?self.shareUrl:INHERIT_VALUE description:self.message?self.message:INHERIT_VALUE image:[ShareSDK imageWithUrl:self.pictureName] type:[NSNumber numberWithInteger:SSPublishContentMediaTypeNews]];
                
                
                
                [ShareSDK showShareViewWithType:ShareTypeQQSpace container:nil content:publishContent statusBarTips:NO authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                    if (state == SSResponseStateSuccess)
                    {
                        [self fadeOut];
                        AlertLog(@"", @"分享成功", @"确定", nil);
                    }
                    else if (state == SSResponseStateFail)
                    {
                        NSString *str = [NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]];
                        NSLog(@"%@",str);
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"分享失败,错误码:%ld,错误描述:%@",(long)[error errorCode], [error errorDescription]] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alert show];
                    }
                    
                }];
            }
            else{
                AlertLog(@"", @"没有安装QQ客户端，无法进行QQ分享", @"确定", nil);
                
            }
            
            
            
        }
            break;
        case 3:{
            
            [self fadeOut];
            if( [self.weretogo isEqualToString: @"球队分享"])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoSendControllerTeam" object:nil];
            }
            if( [self.weretogo isEqualToString: @"个人分享"])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoSendController" object:nil];
            }
            
            
            
        }
            break;
            
    }
}

-(void)onCancleBtn{
    
    [self dismiss];
    
}


- (void)show
{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.overlayView];
    [keywindow addSubview:self];
    
    [self fadeIn];
}

- (void)dismiss
{
    [self fadeOut];
}

//弹入层
- (void)fadeIn
{
    
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        
        self.alpha = 1;
        
    }];
    
}

//弹出层
- (void)fadeOut
{
    [UIView animateWithDuration:.35 animations:^{
        
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        if (finished) {
            [self.overlayView removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}


@end
