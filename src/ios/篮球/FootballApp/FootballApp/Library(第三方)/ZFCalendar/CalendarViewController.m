//
//  CalendarViewController.m
//  Calendar
//
//  Created by 张凡 on 14-8-21.
//  Copyright (c) 2014年 张凡. All rights reserved.
//

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com

#import "CalendarViewController.h"
//UI
#import "CalendarMonthCollectionViewLayout.h"
#import "CalendarMonthHeaderView.h"
#import "CalendarDayCell.h"
//MODEL
#import "CalendarDayModel.h"
#import "MatchModel.h"

@interface CalendarViewController ()
<UICollectionViewDataSource,UICollectionViewDelegate>
{

     NSTimer* timer;//定时器
    NSMutableArray *timeArray;  //里面装的是
    NSMutableArray *timeArray2; //里面装的某一天时间 2015-03-24 样式
     NSMutableArray *dataArray; //里面装这一天里面有多少场比赛
}
@end

@implementation CalendarViewController

static NSString *MonthHeader = @"MonthHeaderView";

static NSString *DayCell = @"DayCell";


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self initData];
        [self initView];
    }
    return self;
}
- (void)reloaded
{
    [self viewDidLoad];
  [self.collectionView reloadData];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    for (int i = 0; i< self.dicArray.count; i++)
    {
        NSString *str = self.dicArray[i];
        NSString *key = [NSString stringWithFormat:@"h%@",str];
        [Defaults removeObjectForKey:key];
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    timeArray = [NSMutableArray arrayWithCapacity:0];
    timeArray2= [NSMutableArray arrayWithCapacity:0];
    dataArray = [NSMutableArray arrayWithCapacity:0];
    self.selectArray=[[NSMutableArray alloc] init];
    
    self.flag=0;
    
  
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloaded) name:@"refreshmydata" object:nil];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    [self.collectionView reloadData];

    
   
}
- (void)initView{
    
    
    [self setTitle:@"选择日期"];
    
    CalendarMonthCollectionViewLayout *layout = [CalendarMonthCollectionViewLayout new];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50) collectionViewLayout:layout]; //初始化网格视图大小
    [self.collectionView registerClass:[CalendarDayCell class] forCellWithReuseIdentifier:DayCell];//cell重用设置ID
    
    [self.collectionView registerClass:[CalendarMonthHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MonthHeader];
    
//    self.collectionView.bounces = NO;//将网格视图的下拉效果关闭
    
    self.collectionView.delegate = self;//实现网格视图的delegate
    
    self.collectionView.dataSource = self;//实现网格视图的dataSource
    
    self.collectionView.backgroundView=[[UIImageView alloc]initWithImage:BGIMG];
    
    [self.view addSubview:self.collectionView];
    
}



-(void)initData{
    
    self.calendarMonth = [[NSMutableArray alloc]init];//每个月份的数组
    
}

#pragma mark - CollectionView代理方法

//定义展示的Section的个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.calendarMonth.count;
}


//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSMutableArray *monthArray = [self.calendarMonth objectAtIndex:section];
   
    return monthArray.count;
}


//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarDayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DayCell forIndexPath:indexPath];
    
    NSMutableArray *monthArray = [self.calendarMonth objectAtIndex:indexPath.section];
    
    CalendarDayModel *model = [monthArray objectAtIndex:indexPath.row];

    
    //这里显示账户当前所有赛事的日期的颜色
    if (self.dicArray.count > 0)
    {
      
        for (int i = 0; i < self.dicArray.count; i++)
        {
            NSArray *array3 = [NSMutableArray arrayWithCapacity:0];
            array3 = [self.dicArray[i] componentsSeparatedByString:@"-"];
            
            
            if (model.year == [array3[0] integerValue] && model.month == [array3[1] integerValue] && model.day == [array3[2] integerValue]) {
              
                
                        NSString *str = self.dicArray[i];
                        NSString *key = [NSString stringWithFormat:@"h%@",str];
                       
                
                
                        model.style=CellDayTypeClick;
                      
                      
                        //这里用于在每个月的最后一行和下个月第一行不要显示重复。
                        [Defaults setObject:str forKey:key];
                        [Defaults synchronize];
 
                        
                        NSString *str1 = [NSString stringWithFormat:@"%ld%ld", (long)indexPath.section,(long)indexPath.row - 1];
                        //这里用于点击传值当前的日期作为参数参加网络访问
                        [Defaults setObject:str forKey:str1];
                        [Defaults synchronize];
                        

                        [timeArray  addObject:str1];
                        [timeArray2 addObject:self.dicArray[i]];
                    }    
        }
    }
    else
    {
        model.style=CellDayTypeFutur;
    }
    
  
    
    
    
//    if (model.month == 5) {
//   
//        if (model.day == 16 ||  model.day == 17 ||model.day == 18)
//        {
//            model.style=CellDayTypeClick;
//        }
//    }
//    if (_selectModel==model) {
//        
//        model.style=CellDayTypeClick;
//    }else{
//        model.style=CellDayTypeFutur;
//    
//    }
     cell.model = model;
    cell.backgroundColor=[UIColor clearColor];
    
    cell.contentView.backgroundColor=[UIColor clearColor];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;

    if (kind == UICollectionElementKindSectionHeader){
        
        NSMutableArray *month_Array = [self.calendarMonth objectAtIndex:indexPath.section];
        CalendarDayModel *model = [month_Array objectAtIndex:15];

        CalendarMonthHeaderView *monthHeader = [collectionView
                                                dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MonthHeader forIndexPath:indexPath];
        monthHeader.masterLabel.text = [NSString stringWithFormat:@"%lu年 %lu月",(unsigned long)model.year,(unsigned long)model.month];//@"日期";
        monthHeader.masterLabel.textAlignment = NSTextAlignmentCenter;
        reusableview = monthHeader;
        monthHeader.backgroundColor=[UIColor clearColor];
    }
    return reusableview;
    
}


//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *month_Array = [self.calendarMonth objectAtIndex:indexPath.section];

    
    CalendarDayModel *model = [month_Array objectAtIndex:indexPath.row];

    NSString *str1 = [NSString stringWithFormat:@"%ld%ld", (long)indexPath.section,(long)indexPath.row -1 ];
    if ([timeArray containsObject:str1])
    {

         NSString *newstr1 = [Defaults objectForKey:str1];
        
        [self getAllMatchesOnline:newstr1]; //网络请求得到当前日期的所有比赛
        
        if (dataArray.count > 0)
        {
            self.calendarArray(dataArray);
        }
        else
        {
//            self.calendarblock(model); //这里提示有问题

        }
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"appearTableView" object:nil];
    }
    else
    {
        self.calendarblock(model);
    }

}

- (void)getAllMatchesOnline:(NSString *)date
{
    [dataArray removeAllObjects];
    
    [HttpRequestHelper postWithURL:Url(@"match/myMatchSheduleDetail") params:@{@"date":date} success:^(BOOL sucess, id responseObject) {
        
        if (sucess) {
            
            NSArray *array = responseObject;
            if ([array count]==0) {
                
                [self.collectionView reloadData];
                
                return ;
            }
        
            for (NSDictionary *dic  in array) {
                
                MatchModel *model=[[MatchModel alloc]initWithDic:dic];
                
                
                BaseCellModel *base=nil;
                
                if ([model.lid intValue]==10000) {
                    
                    
                    
                    if ([model.status intValue]==0) {
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                        
                    }
                    
                    //赛前
                    if ([model.status intValue]==10) {
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                        
                    }
                    //比赛中
                    if ([model.status intValue]==20||[model.status intValue]==21) {
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                        
                    }
                    //比赛结束
                    if ([model.status intValue]==100) {
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell2"];
                        
                    }
                    
                }else{
                    //約赛中
                    if ([model.status intValue]==0) {
                        
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell1"];
                    }
                    //以匹配（赛前）
                    if ([model.status intValue]==10) {
                        
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell4"];
                    }
                    //比赛中
                    if ([model.status intValue]==20||[model.status intValue]==21) {
                        
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                    }
                    //比赛结束
                    if ([model.status intValue]==100) {
                        base=[BaseCellModel setModel:@"" key:@"" value:model image:@"" reuseIdentifier:@"MatchCell3"];
                    }
                    
                    
                }
                
                [dataArray addObject:base];
                
                 [self.collectionView reloadData];
                
                
            }
            
            
            [self.collectionView reloadData];
            if (dataArray.count > 0)
            {
                self.calendarArray(dataArray);
            }
            
            
        }else{
            
            [SVProgressHUD showErrorWithStatus:responseObject];
          [self.collectionView reloadData];
            
        }
        
        
    } failure:^(NSError *error) {
        
        [self.collectionView reloadData];
        
    }];

}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return YES;
}




//定时器方法
- (void)onTimer{
    
    [timer invalidate];//定时器无效
    
    timer = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
