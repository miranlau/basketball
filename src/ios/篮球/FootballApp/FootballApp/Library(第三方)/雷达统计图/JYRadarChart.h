//
//  JYRadarChart.h
//  JYRadarChart
//
//  Created by jy on 13-10-31.
//  Copyright (c) 2013年 wcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYRadarChart : UIView

@property (nonatomic, assign) CGFloat r;//半径
@property (nonatomic, assign) CGFloat maxValue;//最小值
@property (nonatomic, assign) CGFloat minValue;//最大值
@property (nonatomic, assign) BOOL drawPoints;//是否画点
@property (nonatomic, assign) BOOL fillArea;//是否填充点之间的面积
@property (nonatomic, assign) BOOL showLegend;
@property (nonatomic, assign) BOOL showStepText;//是否显示每一圈的值
@property (nonatomic, assign) CGFloat colorOpacity;
@property (nonatomic, strong) UIColor *backgroundLineColorRadial;//背景线的颜色
@property (nonatomic, strong) NSArray *dataSeries;//数值
@property (nonatomic, strong) NSArray *attributes;//每一个项
@property (nonatomic, assign) NSUInteger steps;//几个圈
@property (nonatomic, assign) CGPoint centerPoint;//中心点
@property (nonatomic, strong) UIColor *backgroundFillColor;//背景色

@property (nonatomic, assign) BOOL  clockwise; //direction of data

- (void)setTitles:(NSArray *)titles;
- (void)setColors:(NSArray *)colors;

@end
