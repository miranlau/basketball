package com.renyi.basketball.api.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.utils.DateUtil;

@Controller
@RequestMapping("/action")
public class ActionController extends BaseController {
	@Resource
	private MatchStatsService matchStatsService;

	/**
	 * 计分
	 */
	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	@ResponseBody
	public ViewData submit(Long matchId, Long teamId, Long recorderId, Long playerId, Integer actionStatus, Integer quarterRemainingTime,
			String extension) {
		logger.debug("enter submit(), matchId=" + matchId + ", teamId=" + teamId + ", recorderId=" + recorderId + ", player=" + playerId
				+ ", actionStatus=" + actionStatus + ", quarterRemainingTime=" + quarterRemainingTime);

		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(), "请登录");
		}

		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		if (teamId == null || teamId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID));
		}

		if (recorderId == null || recorderId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.RECORDER_ID));
		}

		if (actionStatus == null || actionStatus <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.ACTION_STATUS));
		}

		if (quarterRemainingTime == null || quarterRemainingTime < 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.QUARTER_REMAINING_TIME));
		}

		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), String.format(ResponseMsgConstants.MATCH_NOT_FOUND, matchId));
		}

		if (matchStats.getMatchStatus() == ActionStatus.GAME_OVER) {
			return buildFailureJson(ErrorCode.GAME_IS_OVER.code(), String.format(ResponseMsgConstants.MATCH_IS_OVER, matchId));
		}

		try {
			return buildSuccessJson(0, "数据录入成功",
					matchStatsService.addAction(matchId, teamId, user, playerId, actionStatus, quarterRemainingTime, extension));
		} catch (BusinessException e) {
			if (e.getErrorCode() != ErrorCode.GAME_IS_STATUS.code()) {
				logger.warn(e.getMessage(), e);
			}
			return buildFailureJson(e.getErrorCode(), e.getMessage());
		}

	}

	/**
	 * 1对1或者多对多换人.
	 */
	@RequestMapping("/substitute")
	@ResponseBody
	public ViewData substitute(Long matchId, Long teamId, Long recorderId, Long[] upPlayerIds, Long[] downPlayerIds,
			Integer quarterRemainingTime) {
		logger.debug("enter substitute, recorderId=" + recorderId + ", matchId=" + matchId + ", teamId=" + teamId + ", upPlayerId="
				+ upPlayerIds + ", downPlayerId=" + downPlayerIds);

		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(), "请登录");
		}

		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		if (teamId == null || teamId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID));
		}

		if (recorderId == null || recorderId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.RECORDER_ID));
		}

		if (upPlayerIds == null || upPlayerIds.length <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.UP_PLAYER_IDS));
		}

		if (downPlayerIds == null || downPlayerIds.length <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.DOWN_PLAYER_IDS));
		}

		if (quarterRemainingTime == null || quarterRemainingTime < 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.QUARTER_REMAINING_TIME));
		}

		try {
			return buildSuccessJson(0, "换人成功",
					matchStatsService.substituteAction(user, matchId, teamId, upPlayerIds, downPlayerIds, quarterRemainingTime));
		} catch (BusinessException e) {
			logger.error("Exception when substitute", e);
			return buildFailureJson(ErrorCode.FAIL.code(), "Substitute failed");
		}
	}

	/**
	 * 撤销计分
	 */
	@RequestMapping("/revert")
	@ResponseBody
	public ViewData revert(Long matchId, Long teamId, Long recorderId, Integer quarterRemainingTime) {
		logger.debug("enter revert(), matchId=" + matchId + ", teamId=" + teamId + ", recorderId=" + recorderId);

		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(), "请登录");
		}

		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		if (teamId == null || teamId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID));
		}

		if (recorderId == null || recorderId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.RECORDER_ID));
		}

		if (quarterRemainingTime == null || quarterRemainingTime < 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.QUARTER_REMAINING_TIME));
		}

		MatchStats matchStatus = matchStatsService.build(matchId);
		if (matchStatus == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), String.format(ResponseMsgConstants.MATCH_NOT_FOUND, matchId));
		}

		if (matchStatus.getMatchStatus() == ActionStatus.GAME_OVER) {
			return buildFailureJson(ErrorCode.GAME_IS_OVER.code(), String.format(ResponseMsgConstants.MATCH_IS_OVER, matchId));
		}

		try {
			return buildSuccessJson(0, "撤销成功", matchStatsService.revertAction(matchId, teamId, user, quarterRemainingTime));
		} catch (BusinessException e) {
			if (e.getErrorCode() != ErrorCode.GAME_IS_STATUS.code()) {
				logger.error(e.getMessage(), e);
			}
			return buildFailureJson(e.getErrorCode(), e.getMessage());
		}

	}

	/**
	 * 文字直播的列表
	 */
	@RequestMapping("/speaks")
	@ResponseBody
	public ViewData getActionSpeaks(Long matchId, Long latestId) {
		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), String.format(ResponseMsgConstants.MATCH_STATS_NOT_FOUND, matchId));
		}

		List<Action> actionList = matchStats.getActionList();
		if (CollectionUtils.isEmpty(actionList)) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), String.format(ResponseMsgConstants.ACTION_LIST_NOT_FOUND, matchId));
		}

		if (latestId == null || latestId <= 0) {
			List<Map<String, Object>> liveList = new ArrayList<Map<String, Object>>();
			for (Action action : actionList) {
				if (StringUtils.isBlank(action.getSpeaks())) {
					continue;
				}
				liveList.add(actionToMap(action));
			}
			return buildSuccessJson(0, "Get live info successfully", liveList);
		} else {
			List<Map<String, Object>> liveList = new ArrayList<Map<String, Object>>();
			for (int i = actionList.size() - 1; i >= 0; i--) {
				Action action = actionList.get(i);
				Long id = action.getId();
				if (id.equals(latestId)) {
					break;
				}
				if (StringUtils.isNotBlank(action.getSpeaks())) {
					liveList.add(actionToMap(action));
				}
			}
			if (liveList.size() > 1) {
				Collections.sort(liveList, new Comparator<Map<String, Object>>() {
					@Override
					public int compare(Map<String, Object> live1, Map<String, Object> live2) {
						Long id1 = (Long) live1.get(ResponseParams.ID);
						Long id2 = (Long) live2.get(ResponseParams.ID);
						return id1.compareTo(id2);
					}
				});
			}
			return buildSuccessJson(0, "Get latest live info successfully", liveList);
		}
	}

	private Map<String, Object> actionToMap(Action action) {
		Map<String, Object> live = new HashMap<String, Object>();
		live.put(ResponseParams.ID, action.getId());
		live.put(ResponseParams.STATUS, action.getStatus());
		live.put(ResponseParams.SPEAK, action.getSpeaks());
		live.put(ResponseParams.TIME, DateUtil.format("HH:mm", action.getCreateTime()));
		return live;
	}

	@Override
	protected Class<?> getClazz() {
		return ActionController.class;
	}

}
