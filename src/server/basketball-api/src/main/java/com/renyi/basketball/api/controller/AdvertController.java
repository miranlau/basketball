package com.renyi.basketball.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.dto.AdvertDto;
import com.renyi.basketball.bussness.dto.Setting;
import com.renyi.basketball.bussness.utils.SettingUtils;

@Controller
@RequestMapping("/advert")
public class AdvertController extends BaseController {
	@RequestMapping("/get")
	@ResponseBody
	public ViewData get() {
		AdvertDto advertDto = new AdvertDto();
		try {
			Setting setting = (Setting) SettingUtils.get();
			if (setting == null) {
				return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "获取广告异常");
			}
			advertDto = setting.getAdvertDto();
		} catch (Exception e) {

		}

		if (advertDto.getIsOpen()) {
			return buildSuccessJson(ApiConstants.SUCCESS, "获取广告成功", advertDto);
		} else {
			advertDto.setSrcPath("");
			advertDto.setUrlPath("");
			return buildSuccessJson(ApiConstants.SUCCESS, "广告未开启", advertDto);
		}
	}

	@Override
	protected Class<?> getClazz() {
		return AdvertController.class;
	}
}
