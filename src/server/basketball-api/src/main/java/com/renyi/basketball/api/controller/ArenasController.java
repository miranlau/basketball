package com.renyi.basketball.api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.dto.ArenasDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Place;
import com.renyi.basketball.bussness.service.ArenasService;
import com.renyi.basketball.bussness.service.PlacesService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/arenas")
public class ArenasController extends BaseController {
	private Logger logger = Logger.getLogger(getClass());
	@Resource
	private ArenasService arenasService;
	@Resource
	private PlacesService placesService;

	/**
	 * 球场列表
	 * 
	 * @param pageable
	 * @param keyword
	 * @return
	 */
	@RequestMapping("/list")
	@ResponseBody
	public ViewData arenasList(Pageable pageable, String keyword) {
		try {
			List<Integer> chooseStatus = new ArrayList<Integer>();
			chooseStatus.add(1);// 审核通过
			Page<ArenasDto> page = arenasService.query(keyword, null, chooseStatus, pageable, null);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取球场列表成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取球场列表异常");
		}
	}

	/**
	 * 球场详情
	 * 
	 * @param arenasId
	 * @param map
	 * @return
	 */
	@RequestMapping("/detail")
	public String arenasDetail(Long arenasId, ModelMap map) {
		if (arenasId == null) {
			map.put("msg", "参数不足，请重新访问");
		} else {
			try {
				ArenasDto arenasDto = arenasService.queryById(arenasId);
				map.put("arenas", arenasDto);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				map.put("msg", "查询异常");
			}
		}
		return "/arenas/detail";
	}

	/**
	 * 获取场馆的场地安排列表
	 * 
	 * @param arenasId
	 * @return
	 */
	@RequestMapping("/getPlaceSchedule")
	@ResponseBody
	public ViewData getPlaceSchedule(Long arenasId) {
		try {
			List<Place> place = placesService.queryScheduleListByArenasId(arenasId, BasketballConstants.MAX_PLACE_SCHEDULE);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取场地安排列表成功", place);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取异常");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return ArenasController.class;
	}
}
