package com.renyi.basketball.api.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.dao.ActionDao;
import com.renyi.basketball.bussness.dto.PlayerDto;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.service.ActionService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.MatchService;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/calc")
public class CalcController extends BaseController {

	@Resource
	private MatchService matchService;
	@Resource
	private ActionService actionService;
	@Resource
	private ActionDao actionDao;
	@Resource
	private MatchStatsService matchStatsService;

	private Logger logger = Logger.getLogger(CalcController.class);

	@RequestMapping("")
	@ResponseBody
	public ViewData calcDutytime(long[] matchIds) {
		// Integer[] matchIds = new Integer[] {203568, 203569, 203570, 203571,
		// 203572, 203573, 203574, 203575};
		// Integer[] matchIds = new Integer[] {203571};
		for (long matchId : matchIds) {
			// Match match = matchService.find(matchId);

			// 更新动态时间
			updateDynamicTime(matchId);

			// 更新上场时间
			updateDutyTime(matchId);
			matchStatsService.build(matchId, true);
		}

		return buildSuccessCodeJson(0, "更新数据成功!");
	}

	public void updateDynamicTime(long matchId) {
		Map<String, Object> params = new HashMap<>();
		params.put("mid", matchId);
		List<Action> list = actionDao.queryListByMatchId(matchId);
		long startTime = 0;
		for (Action orginal : list) {
			if (orginal.getStatus() == 11) {
				startTime = orginal.getCreateTime().getTime();
			}

			long passTime = orginal.getCreateTime().getTime() - startTime;
			Action action = new Action();
			action.setId(orginal.getId());
			// FIXME xfenwei
			//action.setQuarterRemainingTime(passTime / 1000 + "");

			actionService.update(action);
		}
	}

	private void updateDutyTime(long matchId) {
		Match match = matchService.find(matchId);

		List<Action> list = actionDao.queryListByMatchId(matchId);

		List<Action> actions = new ArrayList<>();
		for (int i = list.size() - 1; i >= 0; i--) {
			Action action = list.get(i);
			if (action.getStatus() == 11 || action.getStatus() == 22 || action.getStatus() == 100) {
				actions.add(action);
			}
		}

		Set<PlayerDto> playerDtos = calcDutyTime(match, actions);

		List<Map<String, Object>> players = new ArrayList<>();
		for (PlayerDto orginal : playerDtos) {
			Map<String, Object> player = new HashMap<>();
			player.put("id", orginal.getId());
			player.put("name", orginal.getName());
			// player.put("avatar", orginal.geta)
			player.put("number", orginal.getUniformNumber());
			player.put("playTime", orginal.getPlayTime());

			players.add(player);

			logger.info(player);
		}

		Match m = new Match();
		m.setId(matchId);
		// FIXME
		// m.setPlayers(JSONArray.fromObject(players).toString());
		matchService.updateMatch(m);
	}

	private Set<PlayerDto> calcDutyTime(Match match, List<Action> actions) {
		Set<PlayerDto> list = new HashSet<>();
		List<PlayerDto> displays = JSON.parseArray(match.getHomeStartingIds(), PlayerDto.class);
		if (null != match.getVisitingId()) {
			displays.addAll(JSON.parseArray(match.getVisitingStartingIds(), PlayerDto.class));
		}

		for (Action action : actions) {
			if (action.getStatus() == 100) {
				for (PlayerDto display : displays) {
					PlayerDto player = new PlayerDto();
					player.setId(display.getId());
					player.setPlayTime(0L);
					player.setName(display.getName());
					player.setAvatar(display.getAvatar());
					player.setOnTime(action.getCreateTime());

					list.add(player);
				}
			} else if (action.getStatus() == 22) {
				JSONObject actionObj = JSONObject.fromObject(action.getExtension());
				try {
					// 换上
					JSONObject json = actionObj.getJSONObject("playerB");
					if (null != json) {
						PlayerDto huanshang = (PlayerDto) JSONObject.toBean(json, PlayerDto.class);
						PlayerDto origin = getPlayer(list, huanshang.getId());
						origin.setPlayTime(origin.getPlayTime()
								+ (origin.getOnTime().getTime() - action.getCreateTime().getTime()));
						origin.setOnTime(null);
					}
				} catch (Exception e) {
					logger.info(action);
					logger.error(e.getMessage(), e);
				}

				try {
					// 换下
					JSONObject json = actionObj.getJSONObject("playerA");
					if (null != json) {
						PlayerDto huanxia = (PlayerDto) JSONObject.toBean(json, PlayerDto.class);
						PlayerDto origin = getPlayer(list, huanxia.getId());
						if (null == origin) {
							origin = huanxia;
							origin.setPlayTime(0L);
						}

						origin.setOnTime(action.getCreateTime());
						list.add(origin);
					}

				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			} else if (action.getStatus() == 11) {

				for (PlayerDto player : list) {
					if (null != player.getOnTime()) {
						player.setPlayTime(player.getPlayTime()
								+ (player.getOnTime().getTime() - action.getCreateTime().getTime()));
					}
				}
			}
		}

		return list;
	}

	PlayerDto getPlayer(Set<PlayerDto> list, long playerId) {
		for (PlayerDto player : list) {
			if (player.getId() == playerId) {
				return player;
			}
		}

		return null;
	}

	@Override
	protected Class<?> getClazz() {
		return CalcController.class;
	}

}
