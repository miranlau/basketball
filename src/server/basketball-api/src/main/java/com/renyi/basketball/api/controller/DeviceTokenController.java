package com.renyi.basketball.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.UserService;

@Controller
@RequestMapping("/deviceToken")
public class DeviceTokenController extends BaseController {

	@Resource
	private UserService userService;

	@RequestMapping("/update")
	@ResponseBody
	public ViewData update(Long userId, String deviceId, String deviceToken, Integer deviceType) {
		User account = userService.find(userId);
		if (null == account) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, -1, "用户不存在!");
		} else if (StringUtils.isEmpty(deviceId)) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, -1, "device id 不允许为空!");
		} else if (StringUtils.isEmpty(deviceToken)) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, -1, "device token 不允许为空!");
		} else if (null == deviceType || (deviceType < 0 || deviceType > 1)) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, -1, "无法获取设备类型!");
		}
		Map<String, Object> params = new HashMap<>();
		params.put("deviceId", deviceId);
		params.put("deviceType", deviceType);
		List<User> users = userService.find(params);
		if (users != null) {
			for (User user : users) {
				user.setDeviceId("");
				user.setDeviceToken("");
				user.setDeviceType(deviceType);
				userService.updateUser(user);
			}
		}
		account.setDeviceId(deviceId);
		account.setDeviceToken(deviceToken);
		account.setDeviceType(deviceType);
		userService.updateUser(account);
		LoginHelper.addUser(account);
		return buildSuccessViewData(0, "device token更新成功!", null);
	}

	@Override
	protected Class<?> getClazz() {
		return DeviceTokenController.class;
	}
}
