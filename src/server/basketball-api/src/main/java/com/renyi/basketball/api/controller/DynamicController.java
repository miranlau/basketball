package com.renyi.basketball.api.controller;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dto.DynamicDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.UserDynamic;
import com.renyi.basketball.bussness.po.DynamicComment;
import com.renyi.basketball.bussness.po.ReadLaud;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.DynamicCommentService;
import com.renyi.basketball.bussness.service.DynamicService;
import com.renyi.basketball.bussness.service.ReadLaudRecordService;
import com.renyi.basketball.bussness.service.UserService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/dynamic")
public class DynamicController extends BaseController {
	@Resource
	private DynamicService dynamicService;
	@Resource
	private ReadLaudRecordService readLaudRecordService;
	@Resource
	private DynamicCommentService commentService;
	@Resource
	private UserService userService;

	Logger logger = Logger.getLogger(getClass());

	/**
	 * 用户发布动态
	 * 
	 * @param userDynamic
	 * @param imgs
	 * @return
	 */
	@RequestMapping("/publish")
	@ResponseBody
	public ViewData publish(UserDynamic userDynamic, String[] imgs) {
		User pusher = LoginHelper.getCurrent();
		if (pusher == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "您还未登陆，请登陆后发布");
		}
		if (imgs != null) {
			userDynamic.setImageStr(Arrays.asList(imgs));
		}
		try {
			userDynamic.setUserId(pusher.getId());
			dynamicService.saveDynamic(userDynamic);
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "发布动态成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "添加动态异常!");
		}
	}

	/**
	 * 获取全部动态列表
	 * 
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/getAllDynamicPager")
	@ResponseBody
	public ViewData allDynamicPager(Pageable pageable) {
		try {
			Page<DynamicDto> page = dynamicService.getAllDynamicPager(pageable);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取动态列表成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取动态列表异常!");
		}
	}

	/**
	 * 获取好友动态列表
	 * 
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/getFriendDynamicPager")
	@ResponseBody
	public ViewData friendDynamicPager(Pageable pageable) {
		User current = LoginHelper.getCurrent();
		if (current == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "您还未登陆，请登陆后查看");
		}
		try {
			Page<DynamicDto> page = dynamicService.friendDynamicPager(pageable, current);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取好友动态列表成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取好友动态列表异常!");
		}
	}

	/**
	 * 动态详情
	 * 
	 * @param dynamicId
	 * @return
	 */
	@RequestMapping("/dynamicDetail")
	@ResponseBody
	public ViewData dynamicDetail(Long dynamicId) {
		if (dynamicId == null) {
			return buildFailureMessage("参数不足");
		}
		User current = LoginHelper.getCurrent();
		UserDynamic userDynamic = dynamicService.find(dynamicId);
		if (current != null) {
			try {
				Boolean isRead = readLaudRecordService.isClick(current.getId(), dynamicId, ReadLaud.NUM_TYPE_READ);
				if (!isRead) {
					// 阅读数+1
					readLaudRecordService.addNum(current.getId(), dynamicId, ReadLaud.NUM_TYPE_READ);
					userDynamic.setReadNum(userDynamic.getReadNum() == null ? 0 : userDynamic.getReadNum() + 1);
					dynamicService.updateDynamic(userDynamic);
				}
			} catch (BusinessException e) {
				logger.error(e.getMessage(), e);
				return buildFailureMessage("获取动态详情异常");
			}
		}
		// 查询动态详情
		try {
			DynamicDto dynamicDto = dynamicService.queryDynamicDetail(dynamicId);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取动态详情成功",
					dynamicDto);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取动态详情异常");
		}
	}

	/**
	 * 用户点赞动态
	 * 
	 * @param dynamicId
	 * @return
	 */
	@RequestMapping("/clickLaud")
	@ResponseBody
	public ViewData clickLaud(Long dynamicId) {
		User current = LoginHelper.getCurrent();
		if (current == null) {
			return buildFailureMessage("未登录，不能点赞!");
		}

		Boolean isLaud = readLaudRecordService.isClick(current.getId(), dynamicId, ReadLaud.NUM_TYPE_LAUD);
		if (!isLaud) {
			try {
				UserDynamic userDynamic = dynamicService.find(dynamicId);
				// 点赞+1
				readLaudRecordService.addNum(current.getId(), dynamicId, ReadLaud.NUM_TYPE_LAUD);
				userDynamic.setLaudNum(userDynamic.getLaudNum() == null ? 0 : userDynamic.getLaudNum() + 1);
				dynamicService.updateDynamic(userDynamic);
				return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "点赞成功");
			} catch (BusinessException e) {
				logger.error(e.getMessage(), e);
				return buildFailureMessage("点赞异常");
			}
		}
		return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.ALREADY_MODIFY, "您已赞过");
	}

	/**
	 * 用户评论动态
	 * 
	 * @param /content
	 *            评论内容
	 * @param /recieverid
	 *            接收者ID
	 * @param /dynamicid
	 *            动态ID
	 * @return
	 */
	@RequestMapping("/commentDynamic")
	@ResponseBody
	public ViewData commentDynamic(DynamicComment dynamicComment) {
		User current = LoginHelper.getCurrent();
		if (current == null) {
			return buildFailureMessage("未登录，不能评论!");
		}
		try {
			UserDynamic userDynamic = dynamicService.find(dynamicComment.getDynamicId());

			// 评论数+1
			userDynamic.setCommentNum(userDynamic.getCommentNum() + 1);
			dynamicService.updateDynamic(userDynamic);
			dynamicComment.setSenderId(current.getId());// 发送者
			commentService.saveDynamic(dynamicComment);
			// 推送给接收者
			commentService.sendPush(current, dynamicComment);
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "评论成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("评论异常");
		}
	}

	/**
	 * 我发表的动态列表
	 * 
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/myDynamicList")
	@ResponseBody
	public ViewData myDynamicList(Pageable pageable) {
		User current = LoginHelper.getCurrent();
		if (current == null) {
			return buildFailureMessage("未登录，无法获取");
		}
		try {
			Page<DynamicDto> page = dynamicService.findMyDynamicList(current.getId(), pageable);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取个人动态列表成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取个人动态列表异常");
		}
	}

	/**
	 * 删除动态
	 * 
	 * @param dynamicId
	 * @return
	 */
	@RequestMapping("/deleteDynamic")
	@ResponseBody
	public ViewData deleteDynamic(Long dynamicId) {
		User current = LoginHelper.getCurrent();
		if (current == null) {
			return buildFailureMessage("未登录，无法删除");
		}
		try {
			dynamicService.deleteDynamic(dynamicId);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "删除动态成功", null);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("删除动态异常");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return DynamicController.class;
	}
}
