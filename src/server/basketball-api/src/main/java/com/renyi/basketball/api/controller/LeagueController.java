package com.renyi.basketball.api.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.LeagueStatus;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dto.LeagueCountCache;
import com.renyi.basketball.bussness.dto.LeagueDto;
import com.renyi.basketball.bussness.dto.LeagueInfoCommetDto;
import com.renyi.basketball.bussness.dto.LeagueInfoDto;
import com.renyi.basketball.bussness.dto.TeamDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.LeagueInfoCommentService;
import com.renyi.basketball.bussness.service.LeagueInfoService;
import com.renyi.basketball.bussness.service.LeagueService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

/**
 * Created by Administrator on 2016/5/16 0016.
 */
@Controller
@RequestMapping("/league")
public class LeagueController extends BaseController {
	@Resource
	private LeagueService leagueService;
	@Resource
	private TeamService teamService;
	@Resource
	private UserService userService;
	@Resource
	private MatchService matchService;
	@Resource
	private LeagueInfoService leagueInfoService;
	@Resource
	private LeagueInfoCommentService leagueInfoCommentService;

	/**
	 * 查询所有的赛事列表
	 *
	 * @param keyword
	 * @param pageable
	 * @return
	 */
	@RequestMapping(value = "/query", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData query(String keyword, Pageable pageable) {
		try {
			List<Integer> status = new ArrayList<>();
			status.add(LeagueStatus.end.code());// 比赛结束
			status.add(LeagueStatus.gaming.code());// 比赛中
			status.add(LeagueStatus.signend.code());// 报名截止
			status.add(LeagueStatus.signing.code());// 报名中
			Page<League> page = leagueService.query(keyword, status, pageable, null);
			return buildSuccessJson(0, "获取比赛列表成功!", page);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取比赛列表失败!");
		}
	}

	/**
	 * 通过赛事id查询详细
	 *
	 * @param leagueId
	 * @return
	 */
	@RequestMapping(value = "/queryLeagueView")
	public String queryLeagueView(Long leagueId, ModelMap map) {
		User user = LoginHelper.getCurrent();
		List<TeamDto> teamDtos = null;
		if (user != null) {
			// FIXME xfenwei teamDtos = teamService.queryByUser(user.getId());
		}
		if (teamDtos != null) {
			Iterator iterator = teamDtos.iterator();
			while (iterator.hasNext()) {
				TeamDto teamDto = (TeamDto) iterator.next();
				if (!teamDto.getLeader().equals(user.getId())) {
					iterator.remove();
				}
			}
		}
		League leagueDto = leagueService.queryById(leagueId);
		if (user != null) {
			Integer list = leagueService.queryfollow(user.getId(), leagueId);

		}
		map.put("league", leagueDto);
		Integer status = leagueDto.getStatus();
		if (status == 0 || status == 1) { // 报名中或者报名截止
			return "/league/detail";
		}
		// 比赛中或比赛结束
		return "/league/match_list";

	}

	/**
	 * 获得赛事动态列表
	 *
	 * @param leagueId
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/getLeagueInfos")
	@ResponseBody
	public ViewData getLeagueInfos(Long leagueId, Pageable pageable) {
		if (leagueId == null) {
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			Page<LeagueInfoDto> infos = leagueInfoService.queryInfoByLeagueId(leagueId, pageable);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取列表成功", infos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL,
					"获取动态列表异常");
		}
	}

	/**
	 * 通过赛事id查询详细
	 *
	 * @param leagueId
	 * @return
	 */
	@RequestMapping(value = "/queryById", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData queryById(Long leagueId) {
		User user = LoginHelper.getCurrent();
		List<Team> teamDtos = null;
		if (user != null) {
			teamDtos = teamService.queryByUser(user.getId());
		}
		try {
//			LeagueDto leagueDto = leagueService.queryById(leagueId, teamDtos);
//			if (user != null) {
//				Integer list = leagueService.queryfollow(user.getId(), leagueId);
//				if (list != null && list != 0) {
//					leagueDto.setIsfollow(true);
//				} else {
//					leagueDto.setIsfollow(false);
//				}
//			}
//			return buildSuccessJson(0, "获取比赛详情成功!", leagueDto);
			return null; //FIXME xfenwei
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取比赛详情失败!");
		}

	}

	/**
	 * 关注赛事
	 *
	 * @param leagueId
	 * @return
	 */
	@RequestMapping(value = "/follow", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData follow(Long leagueId) {
		if (leagueId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "  联赛id不能为空!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "  当前用户不存在!");
		}
		try {
			int record = leagueService.queryfollow(user.getId(), leagueId);
			if (record >= 1) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你已经关注了比赛!");
			}
			leagueService.follow(user.getId(), leagueId);
			return buildSuccessJson(0, "关注赛事成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "关注赛事失败!");
		}
	}

	/**
	 * 取消关注
	 *
	 * @param leagueId
	 * @return
	 */
	@RequestMapping(value = "/unfollow", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData unfollow(Long leagueId) {
		if (leagueId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "  联赛id不能为空!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "  当前用户不存在!");
		}
		try {
			int record = leagueService.queryfollow(user.getId(), leagueId);
			if (record < 1) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你还没有关注比赛!");
			}
			leagueService.unfollow(user.getId(), leagueId);
			return buildSuccessJson(0, "取消关注成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "取消关注失败!");
		}
	}

	/**
	 * 参加联赛
	 *
	 * @param leagueId
	 *            比赛Id
	 * @param teamId
	 *            球队id
	 * @return
	 */
	@RequestMapping("/enroll")
	@ResponseBody
	public ViewData enroll(Long leagueId, Long teamId) {
		if (leagueId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "赛事ID不能为空!");
		}
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "应战队伍ID不能为空!");
		}
		League league = leagueService.find(leagueId);
		if (league == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "联赛不存在!");
		}
		Team team = teamService.find(teamId);
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "队伍不存在!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		try {
			if (league.getTeams().indexOf(team.getId().toString()) >= 0) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你的队伍已经参加该赛事了!");
			}
			leagueService.enroll(leagueId, teamId);
			return buildSuccessJson(0, "参加赛事成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "参加赛事失败!");
		}
	}

	/**
	 * 取消参加联赛
	 *
	 * @param leagueId
	 * @return
	 */
	@RequestMapping("/cancelEnroll")
	@ResponseBody
	public ViewData cancelEnroll(Long leagueId) {
		User user = LoginHelper.getCurrent();
		// 验证传入参数
		if (leagueId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "赛事ID不能为空!");
		}
		League league = leagueService.find(leagueId);
		if (league == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "赛事不存在!");
		}
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		List<Team> teams = teamService.queryByUsers(user.getId());
		boolean flag = false;
		try {
			for (Team team : teams) {
//				if (team.getLeader().equals(user.getId())) {
//					leagueService.cancelEnroll(leagueId, team.getId());
//					flag = true;
//				}
			}
			if (flag) {
				return buildSuccessJson(0, "取消应约成功!", null);
			} else {
				return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "你还没有参加此赛事!");
			}
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "退出赛事失败!");
		}
	}

	/**
	 * 查询赛事参赛队伍
	 *
	 * @param leagueId
	 * @param map
	 * @return
	 */
	@RequestMapping("/queryTeamsByLeagueId")
	public String queryTeamsByLeagueId(Long leagueId, ModelMap map) {
		try {
			List<Team> teamDtos = teamService.queryTeamsByLeagueId(leagueId);
			map.put("teams", teamDtos);
			return "/league/team_list";
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查看参赛队伍异常!");
			return "/league/detail";
		}
	}

	/**
	 * 赛事赛程
	 *
	 * @param leagueId
	 * @param map
	 * @return
	 */
	@RequestMapping("/queryMatchByLeagueId")
	public String queryMatchByLeagueId(Long leagueId, ModelMap map) {
		try {
			List<Match> matchDtos = matchService.queryLeagueMatch(leagueId);
			map.put("matches", matchDtos);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return "/league/schedule_list";
	}

	/**
	 * 查询赛事动态详情
	 *
	 * @param infoId
	 * @return
	 */
	@RequestMapping("/leagueInfoDetail")
	public String leagueInfoDetail(Long infoId, ModelMap map) {
		if (infoId == null) {
			map.put("msg", "请选中动态");
			return "/league/match_list";
		}
		map.put("infoId", infoId);
		// 查询基本信息
		try {
			LeagueInfoDto infoDto = leagueInfoService.queryInfoDetail(infoId);
			// 阅读加1
			leagueInfoService.addReadNum(infoId);
			map.put("infoDetail", infoDto);
			return "/league/match_detail";
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "获取异常");
			return "/league/match_list";
		}
	}

	/**
	 * 获取动态评论列表
	 *
	 * @param infoId
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/queryCommentsByInfoId")
	@ResponseBody
	public ViewData queryCommentsByInfoId(Long infoId, Pageable pageable) {
		if (infoId == null) {
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			Page<LeagueInfoCommetDto> infos = leagueInfoCommentService.queryCommentsByInfoId(infoId, pageable);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取列表成功", infos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL,
					"获取动态列表异常");
		}
	}

	/**
	 * 评论动态
	 *
	 * @param content
	 * @param infoId
	 * @return
	 */
	@RequestMapping("/commentLeagueInfo")
	@ResponseBody
	public ViewData commentLeagueInfo(String content, Long infoId) {
		if (content == null) {
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		try {
			leagueInfoCommentService.addComment(user, content, infoId);
			leagueInfoService.addCommentNum(infoId);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "评论成功", content);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL,
					"发表赛事评论异常");
		}
	}

	@RequestMapping("/addLaud")
	@ResponseBody
	public ViewData addLaud(Long infoId) {
		if (infoId == null) {
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			leagueInfoService.addLaudNum(infoId);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "点赞成功", null);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL,
					"点赞异常");
		}
	}

	/**
	 * 赛事榜单页面
	 *
	 * @param leagueId
	 * @return
	 */
	@RequestMapping("/leagueBang")
	public String leagueBang(Long leagueId, ModelMap map) {
		try {
			LeagueCountCache leagueCount = leagueService.queryLeagueBang(leagueId);
			if (leagueCount != null) {
				map.put("leagueBang", leagueCount);
			}
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
		}
		return "/league/billboard_list";
	}

	/**
	 * 我关注的赛事
	 * 
	 * @return
	 */
	@RequestMapping("/myFocusLeague")
	@ResponseBody
	public ViewData myFocusLeague() {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "请登陆");
		}
		try {
			List<LeagueDto> leagueDtos = leagueService.queryMyFocusLeague(user.getId());
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取我关注的赛事成功",
					leagueDtos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL,
					"获取我关注的赛事异常");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return LeagueController.class;
	}
}
