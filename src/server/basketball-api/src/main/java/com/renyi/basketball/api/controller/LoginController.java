package com.renyi.basketball.api.controller;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;

@Controller
@RequestMapping("/login")
public class LoginController extends BaseController {
	@Resource
	private UserService userService;
	@Resource
	private TeamService teamService;
	@Resource
	private MatchService matchService;

	@RequestMapping("/submit")
	@ResponseBody
	public ViewData submit(String userName, String password) throws Exception {
		if (StringUtils.isEmpty(userName)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "用户名不能为空");
		} else if (StringUtils.isEmpty(password)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "密码不能为空");
		}

		User user = userService.queryByMobile(userName);
		if (user == null) {
			user = userService.queryByUsername(userName);
		}

		if (null == user || !DigestUtils.md5Hex(password + BasketballConstants.PASSWORD_MD5_PRIFX).equals(user.getPassword())) {
			return buildFailureJson(ErrorCode.FAIL.code(), "用户名或密码不正确");
		}

		user.setAccessToken(LoginHelper.addUser(user));
		
		return buildSuccessJson(0, "登录成功!", user);
	}

	@Override
	protected Class<?> getClazz() {
		return LoginController.class;
	}
}
