package com.renyi.basketball.api.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.PlayerService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.service.VenueService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/match")
public class MatchController extends BaseController {
	@Resource
	private MatchService matchService;
	@Resource
	private TeamService teamService;
	@Resource
	private PlayerService playerService;
	@Resource
	private VenueService venueService;
	@Resource
	private UserService userService;
	@Resource
	private MatchStatsService matchStatsService;

	/**
	 * 查询参赛球队的所有的球员
	 * 
	 * @param teamId
	 *            Long
	 * @param matchId
	 *            Long
	 * @return ViewData
	 */
	@RequestMapping("/teamPlayers")
	@ResponseBody
	public ViewData queryTeamPlayers(Long teamId, Long matchId) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID));
		}
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),
					String.format(ResponseMsgConstants.MATCH_NOT_FOUND, matchId));
		}

		List<Player> playerList = playerService.queryPlayerListByTeamId(teamId);
		if (playerList == null || playerList.size() == 0) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),
					String.format(ResponseMsgConstants.PLAYER_LIST_NOT_FOUND, teamId));
		}

		List<Long> registerIdList = teamId.equals(match.getHomeId()) ? match.getHomePlayerIdList()
				: match.getVisitingPlayerIdList();
		List<Long> startingIdList = teamId.equals(match.getHomeId()) ? match.getHomeStartingIdList()
				: match.getVisitingStartingIdList();
		List<Long> lineupIdList = teamId.equals(match.getHomeId()) ? match.getHomeLineupIdList()
				: match.getVisitingLineupIdList();

		for (Player player : playerList) {
			if (registerIdList != null && registerIdList.size() > 0 && registerIdList.contains(player.getId())) {
				player.setIsEnroll(true);
			}
			if (startingIdList != null && startingIdList.size() > 0 && startingIdList.contains(player.getId())) {
				player.setIsStarting(true);
			}
			if (lineupIdList != null && lineupIdList.size() > 0 && lineupIdList.contains(player.getId())) {
				player.setIsLineup(true);
			}
		}

		return buildSuccessJson(0, "Get All players successfully", playerList);
	}

	/**
	 * 保存报名球员
	 * 
	 * @param teamId
	 *            Long
	 * @param matchId
	 *            Long
	 * @param playerIds
	 *            Long[]
	 * @return ViewData
	 */
	@RequestMapping("/enroll")
	@ResponseBody
	public ViewData enroll(Long teamId, Long matchId, Long[] playerIds) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队ID不能为空!");
		}
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛Id不能为空!");
		}
		if (playerIds == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "用户ID不能为空!");
		}

		try {
			Match match = matchService.find(matchId);
			if (match == null) {
				return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "该比赛不存在！");
			}
			if (match.getStatus() == ActionStatus.GAME_OVER) {
				return buildFailureJson(ErrorCode.GAME_IS_OVER.code(), "比赛已经结束，无法添加报名球员！");
			}

			matchService.enroll(match, teamId, playerIds);
			return buildSuccessJson(0, "添加球员成功!", null);
		} catch (BusinessException e) {
			logger.error("add match players exception", e);
			return buildFailureJson(ViewData.FlagEnum.NORMAL, e.getErrorCode(), e.getMessage());
		}
	}

	/**
	 * 保存首发球员
	 * 
	 * @param matchId
	 *            matchId Long
	 * @param matchId
	 *            recorderId Long
	 * @param matchId
	 *            homeStarting LongString
	 * @param matchId
	 *            visitingStarting String
	 * 
	 * @return ViewData
	 */
	@RequestMapping("/starting")
	@ResponseBody
	public ViewData saveStartingPlayers(Long matchId, Boolean isHome, String startingIds) {
		logger.debug("Start to invoke saveStartingPlayers(), matchId=" + matchId + ", isHome=" + isHome
				+ ", startingIds=" + startingIds);
		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.STARTING_IDS));
		}
		if (isHome == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.IS_HOME));
		}
		if (StringUtils.isBlank(startingIds)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.STARTING_IDS));
		}

		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),
					String.format(ResponseMsgConstants.MATCH_NOT_FOUND, matchId));
		}

		try {
			matchService.saveStarting(matchId, isHome, startingIds);
			return buildSuccessJson(0, "确认首发成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(e.getErrorCode(), e.getMessage());
		}
	}

	/**
	 * 获取比赛详情
	 * 
	 * @param matchId
	 * @return 比赛详情
	 */
	@RequestMapping("/info")
	@ResponseBody
	public ViewData getMatchInfo(Long matchId, Boolean isRefresh) {
		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}
		MatchStats matchStats = matchStatsService.build(matchId, isRefresh);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),
					String.format(ResponseMsgConstants.MATCH_NOT_FOUND, matchId));
		}
		return buildSuccessJson(0, "Get match info successfully", matchStats);
	}

	/**
	 * 比赛直播的页面
	 * 
	 * @param matchId
	 *            Long
	 * @return String
	 */
	@RequestMapping("/live")
	public String showLive(Long matchId, ModelMap model) {
		if (matchId == null || matchId < 0) {
			return "404";
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return "404";
		}
		model.addAttribute("matchId", matchId);
		return "/match/live";
	}

	/**
	 * 获取正在比赛中，球队信息
	 * 
	 * @param matchId
	 *            赛事ID
	 * @return
	 */
	@RequestMapping(value = "/getTeamInfo")
	@ResponseBody
	public ViewData getTeamInfo(Long matchId, Long teamId) {
		if (null == matchId) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛id不能为空");
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (null == matchStats) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在");
		}
		return buildSuccessJson(0, "获取阵容成功", matchStats.getLineupPlayers(teamId));
	}

	/**
	 * 获取场下球员
	 * 
	 * @param matchId
	 * @param teamId
	 * @return
	 */
	@RequestMapping("/linedown")
	@ResponseBody
	public ViewData getLinedown(Long matchId, Long teamId) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛id不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队id不能为空!");
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "未找到阵容!");
		}
		// Map map = game.getStartingMap();
		List<Long> userIds = teamId.equals(match.getHomeId()) ? match.getHomePlayerIdList()
				: match.getVisitingPlayerIdList();
		List<Player> displayDtos = matchStats.getLineupPlayers(teamId);
		List<Long> otherIds = new ArrayList<>();
		List<Player> otherPlayers = new ArrayList<>();
		for (Player displayDto : displayDtos) {
			if (userIds.contains(displayDto.getId())) {
				continue;
			}
			otherIds.add(displayDto.getId());
		}
		if (!otherIds.isEmpty()) {
			otherPlayers = playerService.queryPlayerList(otherIds, teamId);
		}
		return buildSuccessJson(0, "获取其它球员成功!", otherPlayers);
	}

	/**
	 * 分页查询比赛
	 * 
	 * @param keyword
	 *            关键字
	 * @param pageable
	 *            分页信息
	 * @return
	 */
	@RequestMapping(value = "/query", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData query(String keyword, Pageable pageable) {
		User user = LoginHelper.getCurrent();
		Long userId = null;
		if (user != null) {
			userId = user.getId();
		}
		try {
			Page<Match> page = matchService.query(pageable, keyword, userId, null, null);
			return buildSuccessJson(0, "获取比赛列表成功!", page);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取比赛列表失败!");
		}
	}

	/**
	 * 根据用户可积分的比赛
	 * 
	 * @param recorderId
	 *            Long
	 * @return ViewData
	 */
	@RequestMapping(value = "/queryByRecorder", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData queryByRecorder() {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "Recorder is not login");
		}
		try {
			List<Match> matches = matchService.queryMatchsNeedRecorder(user.getId());
			return buildSuccessJson(0, "获取待计分比赛成功!", matches);
		} catch (BusinessException e) {
			logger.error("获取待计分比赛异常", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取待计分比赛失败!");
		}
	}

	/**
	 * 查询比赛详细
	 * 
	 * @param matchId
	 *            比赛Id
	 * @return
	 */
	// FIXME xfenwei 暂时不删除，该方法以后需要删除
	@RequestMapping(value = "/queryById", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData queryById(Long matchId) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		try {
			Match matchDto = matchService.queryMatchById(matchId);
			return buildSuccessJson(0, "获取比赛详情成功!", matchDto);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取比赛详情失败!");
		}
	}

	/**
	 * 创建匹配赛
	 * 
	 * @param teamId
	 *            队伍id
	 * @param date
	 *            比赛日期
	 * @param type
	 *            比赛类别
	 * @param areacode
	 *            比赛地点
	 * @param power
	 *            战斗力范围
	 * @param age
	 *            年龄范围
	 * @param desc
	 *            比赛描述
	 * @return
	 */
	@RequestMapping(value = "/createMatching", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData createMatching(Long teamId, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date,
			Byte matchFormat, String areacode, String power, String age, String desc, Double longitude, Double latitude,
			String addr) {
		if (null == matchFormat || matchFormat < 2 || matchFormat > 5 || 0 == matchFormat) {
			return buildFailureJson(ErrorCode.FIELD_TYPE_ERROR.code(), "比赛赛制须2-5人制!");
		}

		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队ID不能为空!");
		}
		if (addr == null || addr.equals("")) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛地址不能为空!");
		}
		/*
		 * if(power == null) { return
		 * buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "战斗力不能为空!"); }
		 * if(age == null) { return
		 * buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "年龄不能为空!"); }
		 */
		User user = LoginHelper.getCurrent();
		Team team = teamService.find(teamId);
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}

		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "球队不存在");
		}

		// if (!team.getLeader().equals(user.getId())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(),
		// "你不是队长，无法创建比赛");
		// }
		try {
			matchService.createMatching(teamId, user.getId(), date, matchFormat, areacode, power, age, desc, longitude,
					latitude, addr);
			// matchService.enroll(teamId,user.getId(),0);
			return buildSuccessCodeJson(0, "创建匹配赛成功");
		} catch (BusinessException e) {
			logger.error("创建匹配赛异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "创建匹配赛失败, 服务器繁忙!");
		}
	}

	/**
	 * 创建单队赛
	 * 
	 * @param teamId
	 *            队伍id
	 * @param date
	 *            日期
	 * @param type
	 *            类型
	 * @return
	 */
	@RequestMapping(value = "/createSingle", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData createSingle(Long teamId, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date,
			Byte matchFormat) {
		if (null == matchFormat || matchFormat < 3 || matchFormat > 11) {
			return buildFailureJson(ErrorCode.FIELD_TYPE_ERROR.code(), "比赛赛制须3-11人制!");
		}
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队ID不能为空!");
		}
		User user = LoginHelper.getCurrent();
		Team team = teamService.find(teamId);
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "球队不存在");
		}

		// if (!team.getLeader().equals(user.getId())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(),
		// "你不是队长，无法创建比赛");
		// }
		try {
			matchService.createSingle(teamId, date, matchFormat, user.getId());
			// matchService.enroll(teamId,user.getId(),0);
			return buildSuccessCodeJson(0, "创建单队赛成功");
		} catch (BusinessException e) {
			logger.error("创建单队赛异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "创建单队赛失败, 服务器繁忙!");
		}
	}

	/**
	 * 分页查询附近比赛
	 * 
	 * @param pageable
	 *            分页信息
	 * @return
	 */
	@RequestMapping(value = "/queryNear", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData queryNear(Double longitude, Double latitude, Pageable pageable) {
		User user = LoginHelper.getCurrent();
		Long userId = null;
		if (user != null) {
			userId = user.getId();
		}
		if (longitude == null && latitude == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "经度或纬度不能为空");
		}
		try {
			Page<Match> page = matchService.query(pageable, null, userId, longitude, latitude);
			return buildSuccessJson(0, "获取比赛列表成功!", page);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取比赛列表失败!");
		}
	}

	/**
	 * 根据用户Id分页查询比赛
	 * 
	 * @param pageable
	 *            分页信息
	 * @return
	 */
	@RequestMapping(value = "/queryByUser", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData queryByUser(Pageable pageable) {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		Long userId = user.getId();
		List<Team> teams = teamService.queryByUsers(userId);
		try {
			Page<Match> page = matchService.queryByUser(user.getId(), pageable, teams);
			return buildSuccessJson(0, "获取比赛列表成功!", page);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取比赛列表失败!");
		}
	}

	/**
	 * 删除比赛
	 * 
	 * @param matchId
	 *            比赛Id
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public ViewData delete(Long matchId) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		Long homeTeam = match.getHomeId();
		Team team = teamService.find(homeTeam);
		User user = LoginHelper.getCurrent();
		// if (!user.getId().equals(team.getLeader())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(),
		// "你不是主队队长，无法删除比赛");
		// }
		if (match.getStatus() >= 20) {
			return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "比赛已经开始，不能删除！");
		}
		try {
			matchService.delete(matchId);
			return buildSuccessJson(0, "删除比赛成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "删除比赛失败!");
		}
	}

	/**
	 * 修改比赛
	 * 
	 * @param matchId
	 * @param teamId
	 * @param date
	 * @param type
	 * @param areacode
	 * @param power
	 * @param age
	 * @param desc
	 * @return
	 */
	@RequestMapping("/modify")
	@ResponseBody
	public ViewData modify(Long matchId, Long teamId, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date,
			Byte matchFormat, String areacode, String power, String age, String desc, Double longitude, Double latitude,
			String addr) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		Team team = teamService.find(teamId);
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "队伍不存在!");
		}
		if (null == matchFormat || matchFormat < 2 || matchFormat > 5 || 0 == matchFormat) {
			return buildFailureJson(ErrorCode.FIELD_TYPE_ERROR.code(), "比赛赛制须2-5人制!");
		}
		try {
			matchService.update(matchId, teamId, date, matchFormat, areacode, power, age, desc, longitude, latitude,
					addr);
			return buildSuccessJson(0, "更新成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "更新比赛失败!");
		}
	}

	/**
	 * 应约比赛
	 * 
	 * @param matchId
	 *            比赛Id
	 * @param teamId
	 *            球队id
	 * @return
	 */
	@RequestMapping("/gauntlet")
	@ResponseBody
	public ViewData gauntlet(Long matchId, Long teamId) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "应战队伍ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		Team team = teamService.find(teamId);
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "队伍不存在!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		// if (!team.getLeader().equals(user.getId())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(),
		// "你不是队长，无法应约此比赛");
		// }
		try {
			// if (match.getDatingTeamIds() != null &&
			// match.getDatingTeamIds().indexOf(team.getId().toString()) >= 0) {
			// return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(),
			// "你已经应约该比赛了!");
			// }
			if (new Date().after(match.getMatchTime())) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "当前时间大于比赛开始时间，无法应约该比赛。");
			}
			matchService.gauntlet(matchId, teamId);
			return buildSuccessJson(0, "应战成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "应战失败!");
		}
	}

	/**
	 * 取消应约
	 * 
	 * @param matchId
	 *            比赛Id
	 * @return
	 */
	@RequestMapping("/cancelGauntlet")
	@ResponseBody
	public ViewData cancelGauntlet(Long matchId) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}

		// 判断Match状态
		// FIXME xfenwei 已经删除约赛的status
		// if (match.getStatus() != MatchStatus.booking.getId()) {
		// return buildFailureJson(ErrorCode.MATCH_STAUTS_ERROR.code(),
		// "比赛状态非应约中!");
		// }

		List<Team> teams = teamService.queryByUsers(user.getId());
		boolean flag = false;
		try {
			for (Team team : teams) {
				// if (team.getLeader().equals(user.getId())) {
				// matchService.cancelGauntlet(matchId, team.getId());
				// flag = true;
				// }
			}
			if (flag) {
				return buildSuccessJson(0, "取消应约成功!", null);
			} else {
				return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "你还没有应约此比赛!");
			}

		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "取消应约失败!");
		}
	}

	/**
	 * 选择应约球队
	 * 
	 * @param matchId
	 * @param teamId
	 * @return
	 */
	@RequestMapping("/chooseTeam")
	@ResponseBody
	public ViewData chooseTeam(Long matchId, Long teamId) {
		// 验证传入参数
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "应战队伍ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		Team team = teamService.find(teamId);
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "队伍不存在!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}

		// 判断Match状态
		// FIXME xfenwei 已经删除约赛的status
		// if (match.getStatus() != MatchStatus.booking.getId()) {
		// return buildFailureJson(ErrorCode.MATCH_STAUTS_ERROR.code(),
		// "比赛状态非应约中!");
		// }

		// 逻辑权限判断
		Team homeTeam = teamService.find(match.getHomeId());
		// if (!user.getId().equals(homeTeam.getLeader())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(),
		// "你不是主队队长，没有权限选择应约球队");
		// }
		// 业务操作
		try {
			matchService.chooseTeam(matchId, teamId);
			return buildSuccessJson(0, "选择应约球队成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "选择应约球队失败!");
		}
	}

	/**
	 * 取消报名
	 * 
	 * @param matchId
	 * @return
	 */
	@RequestMapping("/cancelEnroll")
	@ResponseBody
	public ViewData cancelEnroll(Long matchId) {
		User user = LoginHelper.getCurrent();
		// 验证传入参数
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		if (match.getStatus() >= 20) {
			return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "比赛已经开始，不能取消报名！");
		}
		// 业务操作
		try {
			boolean isHome = teamService.queryRecord(user.getId(), match.getHomeId()) > 0;
			boolean isVisiting = false;
			if (match.getVisitingId() != null && !("").equals(match.getVisitingId())) {
				isVisiting = teamService.queryRecord(user.getId(), match.getVisitingId()) > 0;
			}
			if (isHome || isVisiting) {
				// 判断主队中是否报名
				if (CollectionUtils.isNotEmpty(match.getHomePlayerIdList())
						&& match.getHomePlayerIdList().contains(user.getId())) {
					matchService.cancel(matchId, user.getId(), 0);
					return buildSuccessJson(0, "取消报名成功!", null);
				}

				// 判断客队中是否报名
				if (CollectionUtils.isNotEmpty(match.getVisitingPlayerIdList())
						&& match.getVisitingPlayerIdList().contains(user.getId())) {
					matchService.cancel(matchId, user.getId(), 1);
					return buildSuccessJson(0, "取消报名成功!", null);
				}

				return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "你还未报名!");
			} else {
				return buildFailureJson(ErrorCode.PLAY_NOT_EXIST.code(), "你不是参赛球队的队员!");
			}
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "取消报名失败!");
		}
	}

	/**
	 * 查询球队的最近N场比赛
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping("/queryMatchByTeamId")
	@ResponseBody
	public ViewData queryMatchByTeamId(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FIELD_NOT_NULL, "球队Id不能为空");
		}
		try {
			List<Match> matches = matchService.queryOnlyMatchByTeam(teamId, BasketballConstants.CLOSE_MATCH);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取球队比赛成功", matches);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FAIL, "查询球队比赛异常");
		}
	}

	/**
	 * 查询比赛状态
	 * 
	 * @param matchId
	 * @return
	 */
	@RequestMapping("/queryMatchStatus")
	@ResponseBody
	public ViewData queryMatchStatus(Long matchId) {
		if (matchId == null) {
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FIELD_NOT_NULL, "比赛Id不能为空");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.OBJECT_NOT_EXIST, "比赛不存在");
		}
		return buildSuccessJson(0, "取消报名成功!", match.getStatus());
	}

	/**
	 * 我的赛程安排
	 * 
	 * @return
	 */
	@RequestMapping("/queryMyMatchShedule")
	@ResponseBody
	public ViewData queryMyMatchShedule() {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "请登陆!");
		}
		try {
			List<Long> result = matchService.queryMyMatchSheduleTime(user);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取我的赛程安排成功", result);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("查看我的赛程安排异常");
		}
	}

	/**
	 * 具体某一天的赛程
	 * 
	 * @param date(yyyy-MM-dd)
	 * @return
	 */
	@RequestMapping("/myMatchSheduleDetail")
	@ResponseBody
	public ViewData myMatchSheduleDetail(String date) {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "请登陆!");
		}
		if (StringUtils.isEmpty(date)) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "请选择日期");
		}
		try {
			List<Match> result = matchService.queryMyMatchShedule(user, date);
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取我的赛程安排详情成功",
					result);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("查看我的赛程安排详情异常");
		}
	}

	/**
	 * 获取比赛分享数据.
	 * 
	 * @param matchId
	 *            Long
	 */
	@RequestMapping("/share")
	@ResponseBody
	public ViewData share(long matchId) {
		Match match = matchService.find(matchId);
		if (null == match) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		String base = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath();

		String home = teamService.find(match.getHomeId()).getName();
		String visit = "客队";
		if (null != match.getVisitingId()) {
			visit = teamService.find(match.getVisitingId()).getName();
		}

		String description = "";
		if (match.getStatus() < 20) {
			Date statDate = match.getMatchTime();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			description = format.format(statDate) + " " + home + " VS " + visit + "比赛等你来.";
		} else if (match.getStatus() == 20) {
			description = home + " VS " + visit + "比赛正在激烈进行中，赶紧来看比赛吧.";
		} else if (match.getStatus() == 100) {
			if (match.getHomeScore() != match.getVisitingScore()) {
				String winner = match.getHomeScore() > match.getVisitingScore() ? home : visit;
				description = home + " " + match.getHomeScore() + ":" + match.getVisitingScore() + " " + visit
						+ "，双方打的甚为精彩，恭喜获胜的" + winner + "队.";
			} else {
				description = home + " " + match.getHomeScore() + ":" + match.getVisitingScore() + " " + visit
						+ "，双方握手言和，非常感谢双方球队为我们奉献了一场精彩的比赛.";
			}
		}
		String url = base + "/ctl/game/detail?matchId=" + matchId;
		String icon = base + "/resource/img/icon/icon.png";
		Map<String, Object> result = new HashMap<>();
		result.put("title", "全网篮球带你看比赛，球队球员数据全都有");
		result.put("description", description);
		result.put("url", url);
		result.put("icon", icon);

		return buildSuccessJson(0, "", result);
	}

	/**
	 * 获取当前阵容
	 * 
	 * @param matchId
	 * @param teamId
	 * @return
	 */
	@RequestMapping("/lineup")
	@ResponseBody
	public ViewData getLineup(Long matchId, Long teamId) {
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛id不能为空!");
		}
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队id不能为空!");
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "未找到阵容!");
		}
		return buildSuccessJson(0, "获取阵容成功", matchStats.getLineupPlayers(teamId));
	}

	/**
	 * 获取赛事球员的信息
	 * 
	 * @param matchId
	 *            赛事ID
	 * @return
	 */
	@RequestMapping("/getPlayerInfo")
	@ResponseBody
	public ViewData getPlyerInfo(Long matchId) {
		if (null == matchId) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛ID不能为空!");
		}

		MatchStats matchStats = matchStatsService.build(matchId);
		if (null == matchStats) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}

		if (matchStats.getMatchStatus() != (ActionStatus.GAME_OVER)
				&& matchStats.getMatchStatus() != (ActionStatus.NOT_START)) {
			// FIXME xfenwei
			// JSONArray playersJSON = matchStats.calcPlayers();
			// matchStats.setPlayingTime(playersJSON.toString());
		}

		Map<String, Map<String, Object>> playerDetailInfo = new HashMap<>();
		List<Player> homeDisplays = matchStats.getHomePlayerList();
		List<Player> visitingDisplays = new ArrayList<>();
		if (null != matchStats.getVisiting()) {
			visitingDisplays = matchStats.getVisitingPlayerList();
		}

		Map<String, Object> homeInfo = new HashMap<>();
		Map<String, Object> vistingInfo = new HashMap<>();
		List<Object> players = new ArrayList<>();
		for (Player player : homeDisplays) {
			// PlayerMatchStats playerData =
			// matchStats.buildPlayerMatchStats(player,
			// matchStats.getVisiting().getId());
			// players.add(playerData);
		}
		Collections.sort(players, new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				Map<String, Object> o1data = (Map<String, Object>) o1;
				Map<String, Object> o2data = (Map<String, Object>) o2;
				String o1player = (String) o1data.get("playeringTime");
				String o2player = (String) o2data.get("playeringTime");
				return o2player.compareTo(o1player);
			}
		});
		homeInfo.put("homePlayer", players);
		List<Object> visitings = new ArrayList<>();
		for (Player player : visitingDisplays) {
			// PlayerMatchStats playere =
			// matchStats.buildPlayerMatchStats(player,
			// matchStats.getVisiting().getId());
			// if (playere != null) {
			// visitings.add(playere);
			// }
		}
		Collections.sort(visitings, new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				Map<String, Object> o1data = (Map<String, Object>) o1;
				Map<String, Object> o2data = (Map<String, Object>) o2;
				String o1player = (String) o1data.get("playeringTime");
				String o2player = (String) o2data.get("playeringTime");
				return o2player.compareTo(o1player);
			}
		});

		vistingInfo.put("visitingPlayer", visitings);
		playerDetailInfo.put("home", homeInfo);
		playerDetailInfo.put("visiting", vistingInfo);
		return buildSuccessJson(0, "数据读取成功", playerDetailInfo);
	}

	/**
	 * 获取比赛的阵型
	 * 
	 * @param matchId
	 *            赛事ID
	 * @return
	 */
	@RequestMapping("/getMatchLineUp")
	@ResponseBody
	public ViewData getMatchLineUp(Long matchId) {
		if (null == matchId) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛id不能为空!");
		}

		MatchStats matchStats = matchStatsService.build(matchId);
		if (null == matchStats) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}

		List<Player> home = matchStats.getLineupPlayers(matchStats.getHome().getId());
		List<Player> visitings = new ArrayList<Player>();
		if (null != matchStats.getVisiting()) {
			visitings = matchStats.getLineupPlayers(matchStats.getVisiting().getId());
		}

		Map<String, List<Player>> lineUp = new HashMap<>();
		lineUp.put("home", home);
		lineUp.put("visiting", visitings);
		return buildSuccessJson(0, "获取数据成功", lineUp);
	}

	/**
	 * 获取比赛时，更新球赛状况
	 * 
	 * @param matchId
	 *            球赛ID
	 * @return
	 */
	@RequestMapping("/timeDetails")
	@ResponseBody
	public ViewData getTimeDetails(Long matchId) {
		if (null == matchId) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛id不能为空!");
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (null == matchStats) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在!");
		}
		// JSONArray jsonArray = game.getTimeline();
		/*
		 * if(null == jsonArray){ return
		 * buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),"没有赛事信息"); }
		 */
		List<JSONObject> list = new ArrayList<>();
		/*
		 * for (Object jsonObject : jsonArray){
		 * list.add((JSONObject)jsonObject); }
		 */
		return buildSuccessJson(0, "获取成功", list);
	}

	/**
	 * 获取正在比赛球队详情JSP
	 * 
	 * @return
	 */
	@RequestMapping("/detail")
	public String detail(Long matchId, Long height, ModelMap model) {
		model.addAttribute("matchId", matchId);
		model.addAttribute("height", height);
		return "/game/info";
	}

	/**
	 *
	 * @return
	 */
	@RequestMapping("/sendMessage")
	@ResponseBody
	public ViewData sendMessage(Long matchId, String msg, Long playerId, Integer status) {
		logger.debug("发送的信息：" + msg);
		User user = LoginHelper.getCurrent();
		if (null == user) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(), "请先登录");
		}
		if (null == matchId) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "赛事ID不能为空");
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (null == matchStats) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "比赛不存在");
		}
		if (null == msg || msg.trim().length() == 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "信息不能为空");
		}
		try {
			List<Map<String, Object>> speaks = matchStatsService.publishMessage(matchId, user, msg, playerId, status);
			return buildSuccessJson(0, "发送成功", speaks);
		} catch (BusinessException e) {
			logger.warn(e.getMessage(), e);
			return buildFailureJson(e.getErrorCode(), e.getMessage());
		}
	}

	/**
	 * 是否可以计分
	 * 
	 * @param matchId
	 * @return
	 */
	@RequestMapping("/isScoring")
	@ResponseBody
	public ViewData isScoring(Long matchId) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (matchId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "赛事ID不能为空");
		}
		User user = LoginHelper.getCurrent();
		Match match = matchService.find(matchId);
		if (match == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "比赛不存在");
		}
		if (user == null) {
			map.put("isScoring", 0);
			return buildSuccessJson(0, "游客，进入直播互动界面", map);
		}
		return null;
		// FIXME xfenwei 删除了isRecording
		// if (match.getIsRecording()) {
		// if (match.getHomeRecorderId() != null &&
		// user.getId().equals(match.getHomeRecorderId())) {
		// map.put("isScoring", 1);
		// return buildSuccessJson(0, "主队记分员，进入赛中界面", map);
		// } else if (match.getVisitingRecorderId() != null &&
		// user.getId().equals(match.getVisitingRecorderId())) {
		// map.put("isScoring", 1);
		// return buildSuccessJson(0, "客队记分员，进入赛中界面", map);
		// } else {
		// map.put("isScoring", 0);
		// return buildSuccessJson(0, "非记分员，进入直播互动界面", map);
		// }
		// } else {
		// List<Long> idList = new ArrayList<>();
		// if (match.getHomePlayerIdList() != null) {
		// String[] ids = match.getHomePlayerIdList().toArray(new String[0]);
		// int len = ids.length;
		// for (int i = 0; i < len; i++) {
		// idList.add(Long.parseLong(ids[i]));
		// }
		// }
		// if (match.getVisitingPlayerIdList() != null) {
		// String[] ids = match.getVisitingPlayerIdList().toArray(new
		// String[0]);
		// int len = ids.length;
		// for (int i = 0; i < len; i++) {
		// idList.add(Long.parseLong(ids[i]));
		// }
		// }
		// for (Long id : idList) {
		// if (id.equals(user.getId())) {
		// map.put("isScoring", 1);
		// return buildSuccessJson(0, "主客队队员，进入赛中界面", map);
		// }
		// }
		// map.put("flag", 0);
		// return buildSuccessJson(0, "非主客队队员，进入直播互动界面", map);
		// }

	}

	/**
	 * 清除比赛缓存
	 * 
	 * @param matchIds
	 * @return
	 */
	@RequestMapping("/clear")
	@ResponseBody
	public ViewData clear(Long[] matchIds) {
		if (matchIds != null && matchIds.length != 0) {
			matchStatsService.clear(matchIds);
			return buildSuccessJson(0, "清楚缓存成功", matchIds);
		} else {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "赛事ID不能为空");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return MatchController.class;
	}
}
