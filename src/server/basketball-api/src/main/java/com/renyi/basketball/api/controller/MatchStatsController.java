package com.renyi.basketball.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.service.MatchStatsService;

/**
 * 比赛数据统计
 */
@Controller
@RequestMapping("/match_stats")
public class MatchStatsController extends BaseController {
	@Resource
	private MatchStatsService matchStatsService;

	/**
	 * 比赛数据的页面
	 * 
	 * @param matchId
	 *            Long
	 * @return String
	 */
	@RequestMapping("/detail")
	public String showTeamStats(Long matchId, ModelMap model) {
		if (matchId == null || matchId < 0) {
			return "404";
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return "404";
		}
		model.addAttribute(ResponseParams.MATCH_ID, matchId);
		model.addAttribute(ResponseParams.HOME_ID, matchStats.getHome().getId());
		model.addAttribute(ResponseParams.VISITING_ID, matchStats.getVisiting().getId());
		model.addAttribute(ResponseParams.LEAGUE_ID, matchStats.getLeagueId());
		model.addAttribute(ResponseParams.LEAGUE_STAGE_ID, matchStats.getLeagueStageId());
		return "/match_stats/detail";
	}
	
	/**
	 * 技术统计术语的页面
	 * 
	 * @param matchId
	 *            Long
	 * @return String
	 */
	@RequestMapping("/stats_glossary")
	public String showStatsGlossary() {
		return "/match_stats/stats_glossary";
	}

	/**
	 * 获取比赛的基本信息
	 * 
	 * @param matchId
	 *            Long
	 * @return ViewData
	 */
	@RequestMapping("/basic")
	@ResponseBody
	public ViewData getMatchStatsBasic(Long matchId) {
		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), String.format(ResponseMsgConstants.MATCH_STATS_NOT_FOUND, matchId));
		}

		return buildSuccessJson(0, "Get MatchStats basic successfully", matchStats.getMatchBasicInfo());
	}

	/**
	 * 获取赛事的数据统计
	 * 
	 * @param matchId
	 *            Long
	 * @return ViewData
	 */
	@RequestMapping(value = "/advanced")
	@ResponseBody
	public ViewData getMatchStatsAdvanced(Long matchId) {
		if (matchId == null || matchId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.MATCH_ID));
		}

		MatchStats matchStats = matchStatsService.build(matchId, true);
		if (matchStats == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), String.format(ResponseMsgConstants.MATCH_STATS_NOT_FOUND, matchId));
		}

		Map<String, Object> info = new HashMap<String, Object>();
		info.put(ResponseParams.QUARTER_SCORE_LIST, matchStats.buildQuarterScoreList());
		info.put(ResponseParams.PLAYER_STATS_LIST, matchStats.buildPlayerMatchStatsForTeam());
		info.put(ResponseParams.TEAM_STATS, matchStats.buildTeamMatchStatsForTeam());

		return buildSuccessJson(0, "Get MatchStats advanced successfully", info);
	}

	@Override
	protected Class<?> getClazz() {
		return MatchStatsController.class;
	}
}
