package com.renyi.basketball.api.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.PlayerService;
import com.renyi.basketball.bussness.service.StatsService;
import com.renyi.basketball.bussness.service.TeamService;

@Controller
@RequestMapping("/player_stats")
public class PlayerStatsController extends BaseController {
	@Resource
	private PlayerService playerService;
	@Resource
	private MatchService matchService;
	@Resource
	private StatsService playerStatsService;

	@RequestMapping("/intro")
	public String getIntro(Long playerId, Long teamId, Long leagueId, Long leagueStageId, ModelMap model) {
		if (playerId != null) {
			Player player = playerService.queryPlayerById(playerId);
			model.addAttribute("player", player);
		}
		model.addAttribute(ResponseParams.TEAM_ID, teamId);
		model.addAttribute(ResponseParams.LEAGUE_ID, leagueId);
		model.addAttribute(ResponseParams.LEAGUE_STAGE_ID, leagueStageId);
		return "player/intro";
	}

	@Override
	protected Class<?> getClazz() {
		return PlayerStatsController.class;
	}
}
