package com.renyi.basketball.api.controller;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.magicbeans.huanxin.HuanXinUtil;
import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dto.UserDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.sms.SMSSendFailException;
import com.renyi.basketball.bussness.sms.YTXSMSTransmitter;
import com.renyi.basketball.bussness.utils.CachedManager;
import com.renyi.basketball.bussness.utils.CaptchaUtil;

@Controller
@RequestMapping("/register")
public class RegisterController extends BaseController {

	/** 云通讯 ，需要填写帐号和密码 */
	YTXSMSTransmitter ytxsmsTransmitter = new YTXSMSTransmitter(BasketballConstants.SMSConstant.ACCOUNT_SID,
			BasketballConstants.SMSConstant.AUTH_TOKEN, BasketballConstants.SMSConstant.APP_ID);

	@Resource
	private UserService userService;

	private final static String REG_CODE_PREFIX = "_REG_CODE_";
	// 默认属性值
	private final static String RADAR = "{\"jingong\":50,\"fangshou\":50,\"tineng\":50,\"sudu\":50,\"credit\":50,\"stars\":50,\"common\":50}";

	/**
	 * 注册获取验证码.
	 * 
	 * @param mobile
	 *            - 手机号
	 */
	@RequestMapping("/getVerifyCode")
	@ResponseBody
	public ViewData getVerifyCode(String mobile) {
		if (StringUtils.isEmpty(mobile)) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.FIELD_NOT_NULL.code(), "手机号码不能为空!");
		}

		// 判断是否已经注册
		User user = userService.queryByMobile(mobile);
		if (user != null) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.MOBILE_ALREADY_EXIST.code(), "该手机号已经注册!");
		}
		String code = CaptchaUtil.code(6);
		logger.debug("注册验证码:" + code);
		try {
			ytxsmsTransmitter.sendSMS(mobile,
					new String[]{code, String.valueOf(BasketballConstants.SMSConstant.TERM_OF_VALIDLITY)},
					BasketballConstants.SMSConstant.REGISTER_TEMPLATE_ID);
		} catch (SMSSendFailException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.FAIL.code(), e.getMessage());
		}
		CachedManager.put(REG_CODE_PREFIX + mobile, code);

		return buildSuccessJson(0, "获取成功", code);
	}

	/**
	 * 用户注册.
	 * 
	 * @param mobile
	 *            - 手机号
	 * @param password
	 *            - 密码
	 * @param code
	 *            - 验证码
	 */
	@RequestMapping("/submit")
	@ResponseBody
	public ViewData submit(String mobile, String password, String code) {
		if (StringUtils.isEmpty(mobile)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "手机号码不能为空!");
		} else if (StringUtils.isEmpty(password)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "密码码不能为空!");
		} else if (StringUtils.isEmpty(code)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "验证码不能为空!");
		}
		String cachedCode = CachedManager.get(REG_CODE_PREFIX + mobile, String.class);
		logger.debug("缓存验证码:" + cachedCode);

		// 判断验证码是否一致
		if (!code.equals(cachedCode)) {
			return buildFailureMessage("验证码不正确!");
		}

		// 查询用户
		User user = userService.queryByMobile(mobile);
		if (user == null) {
			user = userService.queryByUsername(mobile);
		}
		// 用户已存在
		if (user != null) {
			return buildFailureJson(ErrorCode.USER_NAME_EXIST.code(), "该手机号已存在!");
		}
		user = new User();
		user.setUserName(mobile);
		user.setMobile(mobile);
		user.setPassword(DigestUtils.md5Hex(password + BasketballConstants.PASSWORD_MD5_PRIFX));
//		user.setPassword(password);

		try {
			userService.save(user);
			// 注册环信账号
			Boolean exist = HuanXinUtil.existUser(user.getUserName());
			if (!exist && user.getUserName() != null && user.getPassword() != null) {
				HuanXinUtil.addUser(user.getUserName(), user.getUserName(), user.getNickName());
			}

			// 移除缓存
			CachedManager.remove(REG_CODE_PREFIX + mobile);

			String token = LoginHelper.addUser(user);
			UserDto userDto = new UserDto();
			userDto.setId(user.getId());
			userDto.setUserName(user.getUserName());
			userDto.setToken(token);
			return buildSuccessJson(0, "注册成功", userDto);
		} catch (BusinessException e) {
			logger.error("用户注册异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "注册失败!");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return RegisterController.class;
	}
}
