package com.renyi.basketball.api.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.utils.FileUpload;
import com.renyi.basketball.bussness.utils.UuidUtil;

@Controller
@RequestMapping("/resource")
public class ResourceController extends BaseController {

	@RequestMapping(value = "/upload", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ViewData upload(@RequestParam MultipartFile[] files,
			@RequestParam(value = "type", defaultValue = "avatar") String type, HttpServletRequest request) {

		Calendar ca = Calendar.getInstance();
		if (null != files && files.length > 0) {
			List<String> list = new ArrayList<String>();
			for (MultipartFile file : files) {
				if (file.isEmpty()) {
					continue;
				}
				String filename = file.getOriginalFilename();
				String filePath = "upload/" + ca.get(Calendar.YEAR) + "/" + ca.get(Calendar.MONTH) + "/"
						+ ca.get(Calendar.DAY_OF_MONTH);
				String resName = FileUpload.fileUp(file, filePath, UuidUtil.get32UUID());

				StringBuffer picURL = new StringBuffer();
				picURL.append(request.getScheme() + "://");
				picURL.append(request.getServerName() + ":");
				picURL.append(request.getServerPort() + "");
				picURL.append(request.getContextPath() + "/");
				// picURL.append("uploadImg/" + file.getName());
				picURL.append(filePath + resName);
				// list.add(filePath + resName);
				list.add(picURL.toString());
			}

			return buildSuccessJson(ApiConstants.SUCCESS, "上传成功", list);
		} else {
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.DB_EXCEPTION, "上传失败");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return ResourceController.class;
	}
}
