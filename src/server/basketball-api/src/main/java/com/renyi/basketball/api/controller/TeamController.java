package com.renyi.basketball.api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.IdAnalyzer;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.dto.BillDto;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.dto.TeamGroupChat;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Bill;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.TeamSeasonStats;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.BillService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.PlayerService;
import com.renyi.basketball.bussness.service.RankingService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.StatsService;
import com.renyi.basketball.bussness.service.UserService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/team")
public class TeamController extends BaseController {
	@Resource
	private TeamService teamService;
	@Resource
	private UserService userService;
	@Resource
	private BillService billService;
	@Resource
	private MatchService matchService;
	@Resource
	private PlayerService playerService;
	@Resource
	private MatchStatsService matchStatsService;
	@Resource
	private RankingService rankingService;

	/**
	 * 球队相关比赛的页面
	 * 
	 * @param matchId
	 *            Long
	 * @return String
	 */
	@RequestMapping("/related_match")
	public String showRelatedMatch(Long matchId, ModelMap model) {
		if (matchId == null || matchId < 0) {
			return "404";
		}
		MatchStats matchStats = matchStatsService.build(matchId);
		if (matchStats == null) {
			return "404";
		}
		Long homeId = matchStats.getHome().getId();
		Long visitingId = matchStats.getVisiting().getId();
		int round = 3;

		if (homeId != null && homeId > 0) {
			model.addAttribute("homePassMatchList", teamService.queryPassMatches(homeId, 0, round));
			model.addAttribute("homeFutureMatchList", teamService.queryFutureMatches(homeId, 0, round));
		}

		if (visitingId != null && visitingId > 0) {
			model.addAttribute("visitingPassMatchList", teamService.queryPassMatches(visitingId, 0, round));
			model.addAttribute("visitingFutureMatchList", teamService.queryFutureMatches(visitingId, 0, round));
		}

		if ((homeId != null && homeId > 0) && (visitingId != null && visitingId > 0)) {
			model.addAttribute("matchHistoryList", teamService.queryMatchHistory(homeId, visitingId, 0, round));
		}

		model.addAttribute("matchId", matchId);
		model.addAttribute("homeName", matchStats.getHome().getName());
		model.addAttribute("visitingName", matchStats.getVisiting().getName());

		return "/team/related_match";
	}

	/**
	 * 显示阵容页面
	 * 
	 * @param teamId
	 *            Long
	 * @return String
	 */
	@RequestMapping("/lineup")
	public String showLineup(Long teamId, Long leagueId, Long leagueStageId, ModelMap model) {
		if (teamId == null || teamId < 0) {
			return "404";
		}
		model.addAttribute(ResponseParams.TEAM, teamService.queryTeam(teamId, false));
		model.addAttribute(ResponseParams.PLAYER_LIST, playerService.queryPlayerListByTeamId(teamId));
		model.addAttribute(ResponseParams.TEAM_RANK, rankingService.getRank(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.LEAGUE_ID, leagueId);
		model.addAttribute(ResponseParams.LEAGUE_STAGE_ID, leagueStageId);
		return "/team/lineup";
	}

	/**
	 * 显示赛程页面
	 * 
	 * @param teamId
	 *            Long
	 * @param leaugeId
	 *            Long
	 * @return String
	 */
	@RequestMapping("/schedule")
	public String showSchedule(Long teamId, Long leagueId, Long leagueStageId, ModelMap model) {
		if (teamId == null || teamId < 0) {
			return "404";
		}
		model.addAttribute(ResponseParams.TEAM, teamService.queryTeam(teamId, false));
		model.addAttribute(ResponseParams.MATCH_LIST, matchService.queryMatchByTeamIdAndLeagueId(teamId, leagueId));
		model.addAttribute(ResponseParams.TEAM_RANK, rankingService.getRank(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.LEAGUE_ID, leagueId);
		model.addAttribute(ResponseParams.LEAGUE_STAGE_ID, leagueStageId);
		return "/team/schedule";
	}

	/**
	 * 查询所有球队列表
	 * 
	 * @param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getAllTeam")
	@ResponseBody
	public ViewData getAllTeam(String keyword, Pageable pageable) {
		try {
			List<Team> teamDtos = teamService.queryAll();
			return buildSuccessJson(0, "载入信息成功", teamDtos);
		} catch (BusinessException e) {
			logger.error("载入信息异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取球队列表失败!");
		}
	}

	/**
	 * 查询所有球队列表
	 * 
	 * @param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/queryByIds")
	@ResponseBody
	public ViewData queryByIds(String teamIds) {
		if (StringUtils.isBlank(teamIds)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_IDS));
		}
		try {
			List<Team> teamDtos = teamService.queryTeamByIds(IdAnalyzer.toList(teamIds));
			return buildSuccessJson(0, "查询赛事的球队成功", teamDtos);
		} catch (BusinessException e) {
			logger.error("载入信息异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "查询赛事的球队失败!");
		}
	}

	/**
	 * 分页查询球队.
	 * 
	 * @param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/query", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData query(String keyword, Pageable pageable) {
		try {
			Page<Team> page = teamService.query(keyword, false, pageable);
			return buildSuccessJson(0, "获取球队列表成功!", page);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获取球队列表失败!");
		}
	}

	/**
	 * 创建球队
	 * 
	 * @param name
	 *            球队姓名
	 * @param logo
	 *            球队logo
	 * @param password
	 *            球队暗号
	 * @return
	 */
	@RequestMapping("/create")
	@ResponseBody
	public ViewData create(String name, String logo, String password) {
		User user = LoginHelper.getCurrent();
		if (StringUtils.isEmpty(name)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队名称不能为空!");
		}
		if (StringUtils.isEmpty(password)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "暗号不能为空!");
		}
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		Team team = new Team();
		team.setName(name);
		team.setLogo(logo);
		// team.setPassword(password);
		team.setCreateTime(new Date());
		try {
			// 创建球队群聊
			// String groupId = HuanXinUtil.createGroup(team.getName(),
			// team.getName(), false, null, false,
			// user.getUserName(), null);
			// team.setGroupId(groupId);
			teamService.create(team);
			// 队长加入群聊
			// if (team.getGroupId() != null && !"".equals(team.getGroupId())) {
			// if (HuanXinUtil.existUser(user.getUserName())) {
			// HuanXinUtil.addBatchuserToGroup(team.getGroupId(), new
			// String[]{user.getUserName()});
			// }
			// }
			return buildSuccessCodeJson(0, "恭喜您创建球队，但如果在3天以内您的球员数没有达到3位及以上，球队将解散");
		} catch (BusinessException e) {
			logger.error("创建球队异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "创建失败, 服务器繁忙!");
		}
	}

	/**
	 * 修改队名和暗号
	 * 
	 * @param name
	 *            球队姓名
	 * @param password
	 *            球队暗号
	 * @param teamId
	 *            球队id
	 * @param logo
	 *            球队logo
	 * @return
	 */
	@RequestMapping("/modify")
	@ResponseBody
	public ViewData modify(String name, String password, Long teamId, String logo) {
		// if (teamId == null)
		// return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(),
		// "球队Id不能为空!");
		// Team team = teamService.find(teamId);
		// if (team == null)
		// return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),
		// "当前队伍不存在!");
		// try {
		// if (!StringUtils.isEmpty(name)) {
		// Team newteam = new Team();
		// newteam.setId(team.getId());
		// newteam.setName(name);
		// teamService.update(newteam);
		//// if (newteam.getGroupId() != null &&
		// !"".equals(newteam.getGroupId())) {
		//// // 更新群名
		//// HuanXinUtil.updateGroup(newteam.getGroupId(), newteam.getName(),
		// newteam.getName());
		//// }
		// return buildSuccessCodeJson(0, "修改球队名称成功");
		// }
		// if (!StringUtils.isEmpty(password)) {
		// Team newteam = new Team();
		// newteam.setId(team.getId());
		// newteam.setPassword(password);
		// teamService.update(newteam);
		// return buildSuccessCodeJson(0, "修噶球队暗号成功");
		// }
		// if (!StringUtils.isEmpty(logo)) {
		// Team newteam = new Team();
		// newteam.setId(team.getId());
		// newteam.setLogo(logo);
		// teamService.update(newteam);
		// return buildSuccessCodeJson(0, "更换logo成功");
		// } else {
		// return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
		// "修改数据不能为空!");
		// }
		// } catch (BusinessException e) {
		// logger.error("修改信息异常!", e);
		// return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(),
		// "修改信息失败, 服务器繁忙!");
		// }
		return null;
	}

	/**
	 * 解散球队
	 * 
	 * @param teamId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/dissolve")
	@ResponseBody
	public ViewData dissolve(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(), "球队Id不能为空!");
		}
		Team team = teamService.find(teamId);
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户不存在!");
		}
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前队伍不存在!");
		}
		// if (!user.getId().equals(team.getLeader())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(),
		// "对不起，你不是队长,没有解散权限");
		// }
		Team newTeam = new Team();
		newTeam.setId(teamId);
		newTeam.setIsDismissed(true);
		try {
			teamService.dissolve(teamId);
			// HuanXinUtil.deleteGroup(team.getGroupId());
			return buildSuccessCodeJson(0, "球队已解散");
		} catch (BusinessException e) {
			logger.error("球队解散异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "球队解散异常, 服务器繁忙!");
		}
	}

	/**
	 * 查询球队详情
	 * 
	 * @param teamId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getTeam")
	@ResponseBody
	public ViewData getTeam(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_NULL.code(), "球队Id不能为空!");
		}
		// 获取登录用户
		User user = LoginHelper.getCurrent();
		try {
			Team team = teamService.find(teamId);
			if (team == null) {
				return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "队伍不存在!");
			}
			return buildSuccessJson(0, "载入信息成功", team);
		} catch (BusinessException e) {
			logger.error("载入信息异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "载入信息异常, 服务器繁忙!");
		}

	}

	/**
	 * 加入球队
	 * 
	 * @param teamId
	 * @param
	 * @return
	 */
	@RequestMapping("/join")
	@ResponseBody
	public ViewData join(Long teamId, String password) {
		// if (teamId == null) {
		// return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
		// "球队Id不能为空!");
		// }
		// if (StringUtils.isEmpty(password)) {
		// return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "暗号不能为空!");
		// }
		// User user = LoginHelper.getCurrent();
		// Team team = teamService.find(teamId);
		// if (team == null) {
		// return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "队伍不存在!");
		// }
		// if (team.getIsDismissed()) {
		// return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(),
		// "队伍已解散，不能加入球队!");
		// }
		// if (user == null) {
		// return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "用户不存在!");
		// }
		// if (team.getPassword().equals(password)) {
		// try {
		// int record = teamService.join(user.getId(), teamId);
		// if (record == 0) {
		// teamService.updateRadar(teamId);
		// // 加入群聊
		//// if (team.getGroupId() != null && !"".equals(team.getGroupId())) {
		//// if (HuanXinUtil.existUser(user.getUserName())) {
		//// HuanXinUtil.addBatchuserToGroup(team.getGroupId(), new
		// String[]{user.getUserName()});
		//// }
		//// }
		// return buildSuccessCodeJson(0, "加入球队成功！");
		// } else {
		// return buildFailureJson(ErrorCode.PLAY_EXIST_TEAM.code(),
		// "已经是该球队的队员了!");
		// }
		// } catch (BusinessException e) {
		// logger.error("加入球队异常!", e);
		// return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(),
		// "加入球队异常, 服务器繁忙!");
		// }
		// } else
		// return buildFailureJson(ErrorCode.FAIL.code(), "暗号不匹配，加入失败!");
		return null;
	}

	/**
	 * 退出球队
	 * 
	 * @param teamId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/quit")
	@ResponseBody
	public ViewData quitTeam(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队Id不能为空!");
		}
		Team team = teamService.find(teamId);
		if (team == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "该球队不存在!");
		}
		User user = LoginHelper.getCurrent();
		try {
			teamService.quit(user.getId(), teamId);
			teamService.updateRadar(teamId);
			// if (team.getGroupId() != null && !"".equals(team.getGroupId())) {
			// if (HuanXinUtil.existUser(user.getUserName())) {
			// HuanXinUtil.deleteUserFormGroup(team.getGroupId(),
			// user.getUserName());
			// }
			// }
			return buildSuccessCodeJson(0, "退出球队成功！");
		} catch (BusinessException e) {
			logger.error("退出球队异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "退出球队异常!");
		}
	}

	/**
	 * 将球员踢出球队.
	 * 
	 * @param teamId
	 *            球队ID
	 * @param userId
	 *            待踢出球员ID
	 * @return
	 */
	@RequestMapping("/kickout")
	@ResponseBody
	public ViewData kickout(Long teamId, Long userId) {
		User current = LoginHelper.getCurrent();
		Team team = teamService.find(teamId);
		if (null == team) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "该球队不存在!");
		}
		// else if (!team.getLeader().equals(current.getId())) {
		// return buildFailureJson(ErrorCode.PLAY_NOT_LEADER.code(), "不是队长,
		// 不能踢出队员!");
		// } else if (team.getLeader().equals(userId)) {
		// return buildFailureJson(ErrorCode.CAN_NOT_KICK_OUT_LEADER.code(),
		// "不能踢出队长!");
		// }
		try {
			teamService.kickout(userId, teamId);
			teamService.updateRadar(teamId);
			User user = userService.find(userId);
			// if (team.getGroupId() != null && !"".equals(team.getGroupId())) {
			// if (HuanXinUtil.existUser(user.getUserName())) {
			// HuanXinUtil.deleteUserFormGroup(team.getGroupId(),
			// user.getUserName());
			// }
			// }
			return buildSuccessCodeJson(0, "踢出球员成功!");
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, e.getErrorCode(), e.getMessage());
		}
	}

	/**
	 * 更改球员球衣号码
	 * 
	 * @param teamId
	 * @param userId
	 * @param number
	 * @return
	 */
	@RequestMapping("/modifyNumber")
	@ResponseBody
	public ViewData modifyNumber(Long teamId, Long userId, Integer number) {
		if (number == null)
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球衣号不能为空!");
		if (teamId == null)
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "球队Id不能为空!");
		Team team = teamService.find(teamId);
		if (team == null)
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前队伍不存在!");
		if (userId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "用户Id不能为空!");
		}
		User user = userService.find(userId);
		if (user == null)
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "用户不存在!");
		try {
			int flag = teamService.modifyNumber(userId, teamId, number);
			if (flag > 0) {
				return buildSuccessCodeJson(0, "修改球员球衣号码成功！");
			} else {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "球衣号码已经存在，请重新输入!");
			}
		} catch (BusinessException e) {
			logger.error("修改球员球衣号码成功异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "修改球员球衣号码成功异常, 服务器繁忙!");
		}
	}

	/**
	 * 关注球队
	 *
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/follow", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData follow(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "  球队id不能为空!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "  当前用户不存在!");
		}
		try {
			Integer record = teamService.queryfollow(user.getId(), teamId);
			if (record >= 1) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你已经关注了比赛!");
			}
			teamService.follow(user.getId(), teamId);
			return buildSuccessJson(0, "关注球队成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "关注球队失败!");
		}
	}

	/**
	 * 取消关注
	 *
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/unfollow", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData unfollow(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "  球队id不能为空!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "  当前用户不存在!");
		}
		try {
			int record = teamService.queryfollow(user.getId(), teamId);
			if (record < 1) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你还没有关注球队!");
			}
			teamService.unfollow(user.getId(), teamId);
			return buildSuccessJson(0, "取消关注成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "取消关注失败!");
		}
	}

	/**
	 * 我关注的球队
	 * 
	 * @return
	 */
	@RequestMapping("/myFocusTeam")
	@ResponseBody
	public ViewData myFocusTeam() {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "请登陆");
		}
		try {
			List<Team> leagueDtos = teamService.queryMyFocusTeam(user.getId());
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取我关注的球队成功", leagueDtos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL, "获取我关注的球队异常");
		}
	}

	/**
	 * 获取当前用户球队会话列表
	 * 
	 * @return
	 */
	@RequestMapping("/getChatData")
	@ResponseBody
	public ViewData getChatData() {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "请登陆!");
		}
		try {
			List<Team> teamDtos = teamService.queryByUser(user.getId());
			List<TeamGroupChat> result = null;
			if (teamDtos != null) {
				result = new ArrayList<>();
				for (Team teamDto : teamDtos) {
					TeamGroupChat chat = new TeamGroupChat();
					// chat.setGroupId(teamDto.getGroupId());
					chat.setTeamName(teamDto.getName());
					chat.setLogo(teamDto.getLogo());
					chat.setTeamId(teamDto.getId());
					result.add(chat);
				}
			}
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取球队会话成功", result);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FAIL, "获取球队会话异常");
		}
	}

	/**
	 * 获得已结束的比赛
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping("/getDoneMatch")
	@ResponseBody
	public ViewData getDoneMatch(Long teamId) {
		try {
			List<Match> matches = matchService.queryDoneMatches(teamId);

			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取已结束比赛列表成功", matches);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FAIL, "获取已结束比赛列表异常");
		}
	}

	/**
	 * 获取球队账本列表
	 * 
	 * @return
	 */
	@RequestMapping("/getBillList")
	@ResponseBody
	public ViewData getBillList(Long teamId, Long matchId) {
		User user = LoginHelper.getCurrent();
		try {
			BillDto billDto = billService.queryBillList(user.getId(), matchId, teamId);

			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取账本列表成功", billDto);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FAIL, "获取账本列表异常");
		}
	}

	/**
	 * 队长保存账单
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping("/saveBill")
	@ResponseBody
	public ViewData saveBill(Long mid, Long tid, Double total, Double averge, Long[] playerIds) {
		// User user = LoginHelper.getCurrent();
		try {
			Bill bill = new Bill();
			bill.setAverge(averge);
			bill.setTotal(total);
			bill.setMatchId(mid);
			bill.setTeamId(tid);
			billService.updateBill(bill, playerIds);
			return buildSuccessCodeJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "保存账本成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(com.renyi.basketball.bussness.constants.ApiConstants.FAIL, "保存账本异常");
		}
	}

	/**
	 * 用户分享
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping("/share")
	public String share(Long teamId, ModelMap map) {
		if (teamId != null) {
			Team team = teamService.find(teamId);
			if (team != null) {
				Team teamDto = teamService.queryTeam(teamId, true);
				map.put("team", teamDto);
				List<Match> matches = matchService.queryOnlyMatchByTeam(teamId, BasketballConstants.CLOSE_MATCH);
				map.put("matches", matches);
			}
		}
		return "team/team-detail";
	}

	/**
	 * 获取球队过去的几场比赛
	 * 
	 * @return ViewData
	 */
	@RequestMapping("/pass")
	@ResponseBody
	public ViewData getPassMatches(Long teamId, @RequestParam(required = false, defaultValue = "3") Integer round) {
		if (teamId == null || teamId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID));
		}
		if (round == null || round <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.ROUND));
		}

		return buildSuccessJson(0, "Get pass match successfully", teamService.queryPassMatches(teamId, 0, round));
	}

	/**
	 * 获取球队未来几场比赛
	 * 
	 * @return ViewData
	 */
	@RequestMapping("/future")
	@ResponseBody
	public ViewData getFutureMatches(Long teamId, @RequestParam(required = false, defaultValue = "3") Integer round) {
		if (teamId == null || teamId <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID));
		}
		if (round == null || round <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.ROUND));
		}

		return buildSuccessJson(0, "Get future match successfully", teamService.queryFutureMatches(teamId, 0, round));
	}

	/**
	 * 获取两只球队最近的交战记录
	 * 
	 * @return ViewData
	 */
	@RequestMapping("/history")
	@ResponseBody
	public ViewData getMatchHistory(Long teamId1, Long teamId2, @RequestParam(required = false, defaultValue = "3") Integer round) {
		if (teamId1 == null || teamId1 <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID1));
		}
		if (teamId2 == null || teamId2 <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TEAM_ID2));
		}
		if (round == null || round <= 0) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(),
					String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.ROUND));
		}

		return buildSuccessJson(0, "Get match history successfully", teamService.queryMatchHistory(teamId1, teamId2, 0, round));
	}

	@Override
	protected Class<?> getClazz() {
		return TeamController.class;
	}
}
