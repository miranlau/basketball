package com.renyi.basketball.api.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.service.RankingService;
import com.renyi.basketball.bussness.service.StatsService;

/**
 * 球队数据统计
 */
@Controller
@RequestMapping("/team_stats")
public class TeamStatsController extends BaseController {
	@Resource
	private StatsService teamStatsService;
	@Resource
	private RankingService rankingService;

	/**
	 * 显示球队赛季的基础数据统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @return ViewData
	 */
	@RequestMapping("/season/basic")
	public String showBasic(Long teamId, Long leagueId, Long leagueStageId, ModelMap model) {
		if (teamId == null || leagueId == null) {
			return "404";
		}
		model.addAttribute(ResponseParams.TEAM_SEASON_STATS, teamStatsService.getTeamSeasonStats(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.TEAM_MATCH_STATS, teamStatsService.getTeamMatchStats(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.PLAYER_STATS_LIST, teamStatsService.getTeamPlayerSeasonStats(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.TEAM_RANK, rankingService.getRank(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.TEAM_ID, teamId);
		model.addAttribute(ResponseParams.LEAGUE_ID, leagueId);
		model.addAttribute(ResponseParams.LEAGUE_STAGE_ID, leagueStageId);
		return "/team_stats/tech-basic";
	}

	/**
	 * 显示球队赛季的进阶数据统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @return ViewData
	 */
	@RequestMapping("/season/advanced")
	public String showAdvanced(Long teamId, Long leagueId, Long leagueStageId, ModelMap model) {
		if (teamId == null || leagueId == null) {
			return "404";
		}
		model.addAttribute(ResponseParams.TEAM_SEASON_STATS, teamStatsService.getTeamSeasonStats(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.TEAM_RANK, rankingService.getRank(teamId, leagueId, leagueStageId));
		model.addAttribute(ResponseParams.TEAM_ID, teamId);
		model.addAttribute(ResponseParams.LEAGUE_ID, leagueId);
		model.addAttribute(ResponseParams.LEAGUE_STAGE_ID, leagueStageId);
		return "/team_stats/tech-advanced";
	}
	
	@Override
	protected Class<?> getClazz() {
		return TeamStatsController.class;
	}

}
