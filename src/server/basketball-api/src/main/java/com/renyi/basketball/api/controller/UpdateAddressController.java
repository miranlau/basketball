package com.renyi.basketball.api.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.FollowService;

/**
 * 更新登陆用户的通讯录联系人为关注
 */
@Controller
@RequestMapping("/address")
public class UpdateAddressController extends BaseController {
	@Resource
	private FollowService followService;

	@RequestMapping("/update")
	@ResponseBody
	public ViewData update(String[] contactList) {
		User original = LoginHelper.getCurrent();
		if (contactList == null) {
			return buildSuccessCodeJson(ApiConstants.SUCCESS, "通讯录空");
		}
		followService.updateAddressFollow(original, contactList);

		return buildSuccessCodeJson(ApiConstants.SUCCESS, "更新完成");
	}

	@Override
	protected Class<?> getClazz() {
		return UpdateAddressController.class;
	}
}
