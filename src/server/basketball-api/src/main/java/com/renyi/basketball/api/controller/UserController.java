package com.renyi.basketball.api.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dto.PlayerDto;
import com.renyi.basketball.bussness.dto.UserDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.sms.SMSSendFailException;
import com.renyi.basketball.bussness.sms.YTXSMSTransmitter;
import com.renyi.basketball.bussness.utils.CachedManager;
import com.renyi.basketball.bussness.utils.CaptchaUtil;
import com.renyi.basketball.bussness.utils.PropertyUtils;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

	/** 云通讯 ，需要填写帐号和密码 */
	YTXSMSTransmitter ytxsmsTransmitter = new YTXSMSTransmitter(BasketballConstants.SMSConstant.ACCOUNT_SID,
			BasketballConstants.SMSConstant.AUTH_TOKEN, BasketballConstants.SMSConstant.APP_ID);

	@Resource
	private UserService userService;
	@Resource
	private TeamService teamService;
	private final static String RET_CODE_PREFIX = "_RET_CODE_";

	@RequestMapping("/getUserInfo")
	@ResponseBody
	public ViewData getUserInfo(Long userId) throws Exception {
		User currenUser = LoginHelper.getCurrent();
		if (currenUser == null) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.FIELD_NOT_NULL.code(), "您未登录!");
		}
		if (userId == null) {
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.FIELD_NOT_NULL.code(), "用户Id不能为空!");
		}
		User user = userService.find(userId);
		if (user == null) {
			logger.error("没有找到该用户信息，userId=" + userId);
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.OBJECT_NOT_EXIST.code(), "用户不存在!");
		}
		UserDto userDto = new UserDto();
		PropertyUtils.copyPropertiesExclude(user, userDto, new String[]{"stars"});
		logger.error("获取用户信息成功，userId=" + userId);
		return buildSuccessJson(0, "获取用户信息成功!", userDto);
	}

	/**
	 * 获取验证码.
	 * 
	 * @param mobile
	 *            - 手机号
	 */
	@RequestMapping("/getVerifyCode")
	@ResponseBody
	public ViewData getVerifyCode(String mobile) {
		if (StringUtils.isEmpty(mobile)) {
			logger.warn("手机号码为空，无法获取短信验证码");
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.FIELD_NOT_NULL.code(), "手机号码不能为空!");
		}

		// 判断是否已经注册
		User user = userService.queryByMobile(mobile);
		if (user == null) {
			logger.warn("该手机号码未注册，无法获取短信验证码， mobile=" + mobile);
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.MOBILE_NOT_EXIST.code(), "手机号码不存在!");
		}

		String code = CaptchaUtil.code(6);
		logger.debug("生成的验证码为: " + code);
		try {
			ytxsmsTransmitter.sendSMS(mobile,
					new String[]{code, String.valueOf(BasketballConstants.SMSConstant.TERM_OF_VALIDLITY)},
					BasketballConstants.SMSConstant.FORGET_PASSWORD_TEMPLATE_ID);
		} catch (SMSSendFailException e) {
			logger.error("发送验证码异常", e);
			return buildFailureJson(ViewData.FlagEnum.NORMAL, ErrorCode.FAIL.code(), "发送验证码异常");
		}
		CachedManager.put(RET_CODE_PREFIX + mobile, code);
		return buildSuccessJson(0, "获取短信验证码成功", code);
	}

	/**
	 * 用户重置密码.
	 * 
	 * @param mobile
	 *            - 手机号
	 * @param password
	 *            - 密码
	 * @param code
	 *            - 验证码
	 */
	@RequestMapping("/findPassword")
	@ResponseBody
	public ViewData submit(String mobile, String password, String code) {
		logger.debug("重置密码，  mobile=" + mobile);
		if (StringUtils.isEmpty(mobile)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "手机号码不能为空!");
		} else if (StringUtils.isEmpty(password)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "密码不能为空!");
		} else if (StringUtils.isEmpty(code)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "验证码不能为空!");
		}
		String cachedCode = CachedManager.get(RET_CODE_PREFIX + mobile, String.class);
		// 判断验证码是否一致
		if (!code.equals(cachedCode)) {
			logger.error("验证码错误，cachedCode=" + cachedCode + ", code=" + code);
			return buildFailureMessage("验证码不正确!");
		}
		// 查询用户
		User user = userService.queryByMobile(mobile);
		// 用户不存在
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "该手机号还未注册!");
		}
		logger.debug("用户id:" + user.getId());

		User newuser = new User();
		newuser.setId(user.getId());
		newuser.setPassword(DigestUtils.md5Hex(password + BasketballConstants.PASSWORD_MD5_PRIFX));
//		newuser.setPassword(password);
		try {
			userService.updateUser(newuser);
			// 存放在登录用户中
			String token = LoginHelper.addUser(user);
			UserDto userDto = new UserDto();
			try {
				PropertyUtils.copyProperties(newuser, userDto);
			} catch (Exception e) {
				logger.error("类型转换异常!", e);
				return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "该手机号还未注册!");
			}
			userDto.setToken(token);
			return buildSuccessCodeJson(0, "找回密码成功");
		} catch (BusinessException e) {
			logger.error("找回密码异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "找回密码失败, 服务器繁忙!");
		}
	}

	/**
	 * 用户修改密码
	 * 
	 * @param mobile
	 *            -手机号
	 * @param oldpsd
	 *            -旧密码
	 * @param newpsd
	 *            -新密码
	 * @return
	 */
	@RequestMapping("/modifyPassword")
	@ResponseBody
	public ViewData ModifyPassowrd(String mobile, String oldpsd, String newpsd) throws Exception {
		if (StringUtils.isEmpty(mobile)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "手机号码不能为空!");
		} else if (StringUtils.isEmpty(oldpsd)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "旧密码不能为空!");
		} else if (StringUtils.isEmpty(newpsd)) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "新密码不能为空!");
		}
		// 查询用户
		User user = userService.queryByMobile(mobile);
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "用户名不存在!");
		}
		logger.debug("查询到用户" + user);
		if (!user.getPassword().equals(DigestUtils.md5Hex(oldpsd + BasketballConstants.PASSWORD_MD5_PRIFX))) {
			return buildFailureJson(ErrorCode.FAIL.code(), "密码验证失败！");
		}
		User newuser = new User();
		newuser.setId(user.getId());
		newuser.setPassword(DigestUtils.md5Hex(newpsd + BasketballConstants.PASSWORD_MD5_PRIFX));
		try {
			userService.updateUser(newuser);
			// 存放在登录用户中
			String token = LoginHelper.addUser(user);

			UserDto userDto = new UserDto();
			PropertyUtils.copyProperties(newuser, userDto);
			userDto.setToken(token);
			return buildSuccessCodeJson(0, "修改密码成功");
		} catch (BusinessException e) {
			logger.error("修改密码异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "修改密码失败, 服务器繁忙!");
		}
	}

	/**
	 * 修改基本信息
	 * 
	 * @param height
	 * @param weight
	 * @param age
	 * @param name
	 * @param idcard
	 * @param chatpush
	 * @param canview
	 * @param location
	 * @return
	 */
	@RequestMapping(value = "/modifyData", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ViewData modifyData(String height, String weight, String age, String name, String idcard, Boolean chatpush,
			Boolean canview, Boolean autoupdate, String location, String avatar, String realName) {
		User user = LoginHelper.getCurrent();
		User newuser = null;
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "当前用户未登录！");
		}
		try {
			if (!StringUtils.isEmpty(height)) {
				newuser = new User();
				newuser.setId(user.getId());
//				newuser.setHeight(Integer.parseInt(height));
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, " 修改身高成功");
			} else if (!StringUtils.isEmpty(weight)) {
				newuser = new User();
				newuser.setId(user.getId());
//				newuser.setWeight(Integer.parseInt(weight));
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改体重成功");
			} else if (!StringUtils.isEmpty(age)) {
				newuser = new User();
				newuser.setId(user.getId());
//				newuser.setAge(Integer.parseInt(age));
				userService.updateUser(newuser);
				List<Team> teams = teamService.queryByUsers(user.getId());
				if (teams != null && teams.size() != 0) {
					for (Team team : teams) {
						teamService.updateRadar(team.getId());
					}
				}
				return buildSuccessCodeJson(0, "修改年龄成功");
			} else if (!StringUtils.isEmpty(avatar)) {
				newuser = new User();
				newuser.setId(user.getId());
				newuser.setAvatar(avatar);
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "更换头像成功");
			} else if (!StringUtils.isEmpty(idcard)) {
				newuser = new User();
				newuser.setId(user.getId());
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改成功");
			}

			else if (canview != null) {
				newuser = new User();
				newuser.setId(user.getId());
				newuser.setCanView(canview);
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改成功");
			}

			else if (autoupdate != null) {
				newuser = new User();
				newuser.setId(user.getId());
				newuser.setAutoUpdate(autoupdate);
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改成功");
			} else if (chatpush != null) {
				newuser = new User();
				newuser.setId(user.getId());
				newuser.setChatPush(chatpush);
				/*
				 * //查询用户的所有队伍， List<TeamDto> list =
				 * teamService.queryByUser(user.getId()); //屏蔽所有租的环信推送 if (list
				 * != null && list.size() != 0) { if (!chatpush) { for (TeamDto
				 * teamDto : list) {
				 * HuanXinUtil.addBlockUserToChatGroup(teamDto.getGroupId(),
				 * user.getUsername()); } }else { for (TeamDto teamDto : list) {
				 * HuanXinUtil.removeBlockUserFromChatGroup(teamDto.getGroupId()
				 * , user.getUsername()); } } }
				 */
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改成功");
			} else if (!StringUtils.isEmpty(location)) {
				newuser = new User();
				newuser.setId(user.getId());
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "切换地理位置成功");
			} else if (!StringUtils.isEmpty(name)) {
				newuser = new User();
				newuser.setId(user.getId());
				newuser.setNickName(name);
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改昵称成功");
			} else if (!StringUtils.isEmpty(realName)) {
				newuser = new User();
				newuser.setId(user.getId());
				newuser.setRealName(realName);
				userService.updateUser(newuser);
				return buildSuccessCodeJson(0, "修改真实姓名成功");
			} else {
				return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "修改数据不能为空!");
			}
		} catch (BusinessException e) {
			logger.error("修改信息异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "修改信息失败, 服务器繁忙!");
		}
	}

	/**
	 * 关注球员
	 *
	 * @param focusId
	 * @return
	 */
	@RequestMapping(value = "/follow", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData follow(Long focusId) {
		if (focusId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "  球员id不能为空!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "  当前用户不存在!");
		}
		try {
			Integer record = userService.queryfollow(user.getId(), focusId);
			if (record >= 1) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你已经关注了该球员!");
			}
			userService.follow(user.getId(), focusId);
			return buildSuccessJson(0, "关注球员成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "关注球员失败!");
		}
	}

	/**
	 * 取消关注
	 *
	 * @param focusId
	 * @return
	 */
	@RequestMapping(value = "/unfollow", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ViewData unfollow(Long focusId) {
		if (focusId == null) {
			return buildFailureJson(ErrorCode.FIELD_NOT_NULL.code(), "  球员id不能为空!");
		}
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "  当前用户不存在!");
		}
		try {
			int record = userService.queryfollow(user.getId(), focusId);
			if (record < 1) {
				return buildFailureJson(ErrorCode.OBJECT_EXIST_ERROR.code(), "你还没有关注该球员!");
			}
			userService.unfollow(user.getId(), focusId);
			return buildSuccessJson(0, "取消关注成功!", null);
		} catch (BusinessException e) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "取消关注失败!");
		}
	}

	/**
	 * 查询我的粉丝
	 * 
	 * @return
	 */
	@RequestMapping("/myFans")
	@ResponseBody
	public ViewData myFans() {
		User user = LoginHelper.getCurrent();
		if (user == null) {
			return buildFailureJson(ErrorCode.OBJECT_NOT_EXIST.code(), "请登陆");
		}
		try {
			List<PlayerDto> leagueDtos = userService.queryMyFans(user.getId());
			return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "获取我的粉丝成功",
					leagueDtos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, com.renyi.basketball.bussness.constants.ApiConstants.FAIL,
					"获取我的粉丝异常");
		}
	}

	/**
	 * 注销登录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/logoff")
	@ResponseBody
	public ViewData logoff(HttpServletRequest request) {
		User orginal = LoginHelper.getCurrent();
		if (orginal != null) {
			User user = new User();
			user.setId(orginal.getId());
			user.setDeviceId("");
			user.setDeviceToken("");
			userService.updateUser(user);
			LoginHelper.invalidate();
		}
		return buildSuccessJson(com.renyi.basketball.bussness.constants.ApiConstants.SUCCESS, "注销登录成功", null);
	}

	@Override
	protected Class<?> getClazz() {
		return UserController.class;
	}

}
