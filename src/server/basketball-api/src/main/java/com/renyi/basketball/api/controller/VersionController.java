package com.renyi.basketball.api.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.dto.Setting;
import com.renyi.basketball.bussness.dto.Version;
import com.renyi.basketball.bussness.rmi.SettingService;
import com.renyi.basketball.bussness.utils.SettingUtils;

/**
 * 版本检测更新
 * 
 * @author EiT
 *
 */
@Controller
@RequestMapping("/version")
public class VersionController extends BaseController {
	@Resource
	private SettingService settingService;

	/**
	 * 获得最新版本
	 * 
	 * @param type
	 *            0安卓，1苹果
	 * @return
	 */
	@RequestMapping("/getVersion")
	@ResponseBody
	public ViewData getVersion(Integer type) {
		Version version = new Version();
		try {
			if (type == null) {
				return buildFailureJson(ViewData.FlagEnum.NORMAL, ApiConstants.FIELD_NOT_NULL, "参数不足");
			}
			Setting setting = (Setting) SettingUtils.get();
			if (setting == null) {
				return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "获取版本异常");
			}
			version = setting.getVersion(type);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取版本成功", version);
		} catch (Exception e) {
			logger.error(e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "获取版本异常");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return VersionController.class;
	}
}
