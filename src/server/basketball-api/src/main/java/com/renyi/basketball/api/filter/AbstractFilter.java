package com.renyi.basketball.api.filter;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.renyi.basketball.bussness.common.ViewData;

public abstract class AbstractFilter implements Filter {

	protected List<String> excludePath;

	protected Logger logger = Logger.getLogger(getClass());

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String excludePathv = filterConfig.getInitParameter("excludePath");
		excludePath = new ArrayList<String>();
		if (!StringUtils.isBlank(excludePathv)) {
			String[] paths = excludePathv.split(";");
			for (String path : paths) {
				if (!StringUtils.isEmpty(path)) {
					excludePath.add(path.trim());
				}
			}
		}
	}

	protected ViewData buildViewData(ViewData.FlagEnum flag, int code, String message, Object data) {
		ViewData viewData = new ViewData();
		viewData.setFlag(flag.ordinal());
		viewData.setCode(Integer.valueOf(code));
		viewData.setMsg(message);
		viewData.setData(data);
		return viewData;
	}

	protected void writeJsonToResponse(HttpServletResponse response, String responseJson) {
		try {
			response.setContentType("text/html;charset=UTF-8");
			OutputStream os = response.getOutputStream();
			os.write(responseJson.getBytes("UTF-8"));
			os.flush();
			os.close();
		} catch (Exception exp) {
			logger.error("error writing response, message=" + exp.getMessage(), exp);
		}
	}
}
