package com.renyi.basketball.api.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.renyi.basketball.api.util.LoginHelper;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.utils.PathPatternMatcher;

/**
 * 检查用户登录.
 */
public class LoginCheckFilter extends AbstractFilter {

	public static Integer RESPONSE_CODE_NEED_LOGIN_IN = HttpServletResponse.SC_UNAUTHORIZED;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		String path = request.getServletPath() + (null != request.getPathInfo() ? request.getPathInfo() : "");
		logger.info("Request path :" + path);

		if (PathPatternMatcher.urlPathMatch(excludePath, path)) {
			filterChain.doFilter(request, response);
			return;
		}

		User user = LoginHelper.getCurrent();
		if (null == user) {
			logger.info("Login required, path = " + path);
			String res = buildViewData(ViewData.FlagEnum.ERROR, RESPONSE_CODE_NEED_LOGIN_IN, "Login required", null).toJson();
			writeJsonToResponse(response, res);
			return;
		}
		filterChain.doFilter(request, response);
	}

	public boolean isValidUser(Long uid) {
		return true;
	}

	@Override
	public void destroy() {

	}
}
