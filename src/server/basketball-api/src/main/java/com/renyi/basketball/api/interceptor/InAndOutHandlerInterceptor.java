/*
 * 
 * 
 * 
 */
package com.renyi.basketball.api.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.renyi.basketball.bussness.utils.JsonUtil;

public class InAndOutHandlerInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(InAndOutHandlerInterceptor.class);

	/** "开始时间"参数名称 */
	private static final String START_TIME_ATTRIBUTE_NAME = InAndOutHandlerInterceptor.class.getName() + ".START_TIME";
	/** "执行时间"参数名称 */
	public static final String EXECUTE_TIME_ATTRIBUTE_NAME = InAndOutHandlerInterceptor.class.getName()
			+ ".EXECUTE_TIME";
	/** 重定向视图名称前缀 */
	private static final String REDIRECT_VIEW_NAME_PREFIX = "redirect:";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		Long startTime = (Long) request.getAttribute(START_TIME_ATTRIBUTE_NAME);
		if (startTime == null) {
			startTime = System.currentTimeMillis();
			request.setAttribute(START_TIME_ATTRIBUTE_NAME, startTime);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("+---------------------------------------- IN ----------------------------------------+");
			logger.debug("Request Path: {}", request.getRequestURI());
			String contentType = request.getContentType();
			logger.debug("Request Method: {}, ContentType: {}, CharacterEncoding: {}", request.getMethod(), contentType,
					request.getCharacterEncoding());
			logger.debug("Request Cookies: ", JsonUtil.toJson(request.getCookies()));
			if (StringUtils.isNotBlank(contentType) && contentType.equals(MediaType.APPLICATION_JSON)) {
				// TODO
				logger.debug("Request Parameters: {}", "");
			} else {
				logger.debug("Request Parameters: {}", JsonUtil.toJson(request.getParameterMap()));
			}
			logger.debug("Remote IP Addr: {}", getRemortIp(request));
			logger.debug("+---------------------------------------- IN ----------------------------------------+");
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		Long executeTime = (Long) request.getAttribute(EXECUTE_TIME_ATTRIBUTE_NAME);
		if (executeTime == null) {
			Long startTime = (Long) request.getAttribute(START_TIME_ATTRIBUTE_NAME);
			Long endTime = System.currentTimeMillis();
			executeTime = endTime - startTime;
			request.setAttribute(START_TIME_ATTRIBUTE_NAME, startTime);
		}

		if (modelAndView != null) {
			String viewName = modelAndView.getViewName();
			if (!StringUtils.startsWith(viewName, REDIRECT_VIEW_NAME_PREFIX)) {
				modelAndView.addObject(EXECUTE_TIME_ATTRIBUTE_NAME, executeTime);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("+---------------------------------------- OUT ----------------------------------------+");
			logger.debug("Request Path: {}", request.getRequestURI());
			logger.debug("Response ContentType: {}, CharacterEncoding: {}", response.getContentType(),
					response.getCharacterEncoding());
			logger.debug("Total Time: {} ms", executeTime);
			logger.debug("+---------------------------------------- OUT ----------------------------------------+");
		}

	}

	public String getRemortIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

}