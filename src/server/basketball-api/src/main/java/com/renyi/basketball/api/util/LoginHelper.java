package com.renyi.basketball.api.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.utils.CachedManager;

public class LoginHelper {
	protected static Logger logger = Logger.getLogger(LoginHelper.class);

	/** 请求Header Token. */
	public final static String ACCESS_TOKEN = "access_token";
	private final static String ACCESS_TOKEN_PIRX = "_access_token_";
	private final static String SESSION_USER = "session_user";

	public final static String addUser(User user) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		request.getSession().setAttribute(SESSION_USER, user);
		String accessToken = DigestUtils.md5Hex(user.getId().toString());
		CachedManager.put(ACCESS_TOKEN_PIRX + accessToken, user);
		return accessToken;
	}

	public final static User getCurrent() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		String accessToken = request.getHeader(ACCESS_TOKEN);
		if (StringUtils.isEmpty(accessToken)) {
			logger.info("User is not login due to access_token = " + accessToken);
			return null;
		}
		User userInSession = (User) request.getSession().getAttribute(SESSION_USER);
		User userInCache = CachedManager.get(ACCESS_TOKEN_PIRX + accessToken, User.class);
		if (userInSession == null && userInCache == null) {
			logger.info("User is not found in session, access_token = " + accessToken);
			return null;
		}
		if (userInSession != null && accessToken.equals(userInSession.getAccessToken())) {
			logger.info("User was login, access_token = " + accessToken);
			return userInSession;
		}
		if (userInCache != null && accessToken.equals(userInCache.getAccessToken())) {
			request.getSession().setAttribute(SESSION_USER, userInCache);
			logger.info("User was login, access_token = " + accessToken);
			return userInCache;
		}
		return null;
	}

	public final static void invalidate() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		Object temp = request.getSession().getAttribute(SESSION_USER);
		if (null != temp) {
			String token = request.getHeader(ACCESS_TOKEN);
			request.getSession().invalidate();
			CachedManager.remove(ACCESS_TOKEN_PIRX + token);
		}
	}
}
