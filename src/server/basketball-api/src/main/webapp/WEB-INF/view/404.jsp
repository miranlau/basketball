<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
</head>

<body>
    <section class="flex-container center notfound">
        <div class="flex">
            <h3 class="orange">404</h3>
            <h3 class="blue">ERROR</h3>
            <p class="blue">您<span class="orange">访问的页面</span>不存在！</p>
        </div>
    </section>
</body>

</html>
