<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/court-detail.css" />

    <link rel="stylesheet" href="<%=base%>/resource/alert/sweet-alert.css" />
    <script type="text/javascript" src="<%=base%>/resource/alert/sweet-alert.js" ></script>
</head>
<body>
<div class="mui-content">
    <div class="detailBox">
        <div class="imgBox">
            <img src="<%=base%>/resource/img/court-pic.png" />
        </div>
        <ul class="content">
            <li>
                <div class="title">${arenas.name}</div>
                <%--<div class="star">--%>
                    <%--<img src="<%=base%>/resource/img/icon/star-yellow.png" />--%>
                    <%--<img src="<%=base%>/resource/img/icon/star-yellow.png" />--%>
                    <%--<img src="<%=base%>/resource/img/icon/star-yellow.png" />--%>
                    <%--<img src="<%=base%>/resource/img/icon/star-yellow.png" />--%>
                    <%--<img src="<%=base%>/resource/img/icon/star-white.png" />--%>
                <%--</div>--%>
                <div class="description">${arenas.descs}</div>
            </li>
            <li class="tell mui-navigate-right" id="tel">
                <img src="<%=base%>/resource/img/icon/phone.png" />球场联系电话:<span class="mui-pull-right">${arenas.telephone_number_1}</span>
            </li>
            <li class="address mui-navigate-right" id="map">
                <img src="<%=base%>/resource/img/icon/map.png" />球场地址:<span class="mui-pull-right mui-ellipsis">${arenas.address}</span>
                <input type="hidden" name="lng" value="${arenas.longitude}">
                <input type="hidden" name="lat" value="${arenas.latitude}">
                <input type="hidden" name="name" value="${arenas.name}">
                <input type="hidden" name="addr" value="${arenas.address}">
            </li>
        </ul>
        <div class="courtState">
            <div class="time">场地状态</div>
            <div class="tab">

            </div>
            <div class="stateBox">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/court.js" ></script>
<script>
    $("#tel").on('tap',function(){
        var tel = $(this).find("span").eq(0).html();
        window.JSBridge.takePhone(tel);
    })
    $("#map").on('tap',function(){
        var lng = $(this).find('input[name="lng"]').eq(0).val();
        var lat = $(this).find('input[name="lat"]').eq(0).val();
        var addr = $(this).find('input[name="addr"]').eq(0).val();
        var name = $(this).find('input[name="name"]').eq(0).val();
        var data = [];
        data.push(lng);
        data.push(lat);
        data.push(name);
        data.push(addr);
        window.JSBridge.takeMap(data);
    })

    $(function(){
        getDatas(${arenas.id});
    })
    function getDatas(arenasId) {
        $.get('getPlaceSchedule',{
            arenasId:arenasId
        }, function (json) {
            if (json.flag == 0 && json.code == 0) {
                $(".stateBox").empty();
                var str = '';
                if (json.data != null && json.data != undefined) {
                    for (var i = 0;i < json.data.length; ++i) {
                        str += '<div class="page">'+
                                '<div class="left">'+
                                '<span>时间/日期</span>'+
                                '<span>08:00~10:00</span>'+
                                '<span>10:00~12:00</span>'+
                                '<span>14:00~16:00</span>'+
                                '<span>16:00~18:00</span>'+
                                '<span>19:00~21:00</span>'+
                                '<span>21:00~23:00</span>'+
                                '</div>'+
                                '<div class="right">'+
                                '<div class="rightContent">';
                        $(".courtState .tab").append('<a href="javascript:void(0)">' + json.data[i].name + '</a>')
                        var list = json.data[i].placeScheduleList;
                        for (var j = 0; j <list.length; ++j) {
                            str +=  '<div class="date">' +
                                    '<span class="long">' + list[j].month + '-' + list[j].day + '</span>'
                            str +=  '<span>空</span>'+
                                    '<span>空</span>'+
                                    '<span>空</span>'+
                                    '<span>空</span>'+
                                    '<span>空</span>'+
                                    '<span>空</span>';
                            str += '</div>';
                        }
                        str += '</div></div></div>';
                    }
                    $(".stateBox").append(str);
                    for (var i = 0;i < json.data.length; ++i) {
                        var list = json.data[i].placeScheduleList;
                        for (var j = 0; j < list.length; ++j) {
                            for (var k = 0; k < list[j].schedule_today.length; ++k) {
                                var hasSchedule = list[j].schedule_today[k].start_time;
                                var $span = $(".page").eq(i).find(".date").eq(j).find("span");
                                //console.log($span)
                                if (hasSchedule == '08:00~10:00') {
                                    $span.eq(1).empty();
                                    $span.eq(1).addClass('full');
                                    $span.eq(1).html('满');
                                } else if (hasSchedule == '10:00~12:00') {
                                    $span.eq(2).empty();
                                    $span.eq(2).addClass('full');
                                    $span.eq(2).html('满');
                                } else if (hasSchedule == '14:00~16:00') {
                                    $span.eq(3).empty();
                                    $span.eq(3).addClass('full');
                                    $span.eq(3).html('满');
                                } else if (hasSchedule == '16:00~18:00') {
                                    $span.eq(4).empty();
                                    $span.eq(4).addClass('full');
                                    $span.eq(4).html('满');
                                } else if (hasSchedule == '19:00~21:00') {
                                    $span.eq(5).empty();
                                    $span.eq(5).addClass('full');
                                    $span.eq(5).html('满');
                                } else if (hasSchedule == '21:00~23:00') {
                                    $span.eq(6).empty();
                                    $span.eq(6).addClass('full');
                                    $span.eq(6).html('满');
                                }
                            }
                        }
                    }
                    $(".courtState .tab").find("a").eq(0).addClass("active");
                } else {
                    $(".tab").append('<div class="">暂无场馆</div>');
                }
            } else {
                mui.toast("服务器异常");
            }
        })
    }
    $('body').delegate('.courtState .tab a','tap',function(){
        $('.courtState .tab a').removeClass('active');
        $(this).addClass('active');
        var index=$(this).index();
        $('.stateBox .page').hide();
        $('.stateBox .page').eq(index).show();
    })


</script>
</body>
</html>
