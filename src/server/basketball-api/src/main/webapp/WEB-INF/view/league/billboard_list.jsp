<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/list.css" />
    <style>
        /*.mui-control-content {*/
            /*background-color: white;*/
            /*min-height: 200px;*/
        /*}*/
        /*.mui-control-content .mui-loading {*/
            /*margin-top: 50px;*/
        /*}*/
    </style>
</head>
<body>
<div class="mui-content">
    <div id="slider" class="slider">
        <div class="sliderSegmentedControl">
            <a class="item mui-active" href="javascript:void(0)">积分榜</a>
            <a class="item" href="javascript:void(0)">得分榜</a>
            <a class="item" href="javascript:void(0)">助攻榜</a>
            <a class="item" href="javascript:void(0)">篮板榜</a>
            <a class="item" href="javascript:void(0)">抢断牌</a>
            <a class="item" href="javascript:void(0)">盖帽榜</a>
        </div>
        <div class="slideBox">
            <div id="item1mobile" class="mui-slider-item">
                <div class="table-head">
                    <span>排名</span>
                    <span>球队</span>
                    <span>胜/负</span>
                    <span>积分</span>
                </div>
                <div class="table-content">
                    <c:if test="${leagueBang.scoresList != null}">
                    <c:forEach items="${leagueBang.scoresList}" var="point" varStatus="status">
                        <div class="list">
                            <span>${status.index + 1}</span>
                            <span>${point.teamName}</span>
                            <span>${point.win}/${point.lose}</span>
                            <span>${point.points}</span>
                        </div>
                    </c:forEach>
                        </c:if>
                </div>
            </div>
            <div id="item2mobile" class="mui-slider-item">
                <div class="table-head">
                    <span>排名</span>
                    <span>球员</span>
                    <span>球队</span>
                    <span>场均得分</span>
                </div>
                <div class="table-content">
                    <c:if test="${leagueBang.pensoerScoreList != null}">
                    <c:forEach items="${leagueBang.pensoerScoreList}" var="shooter" varStatus="status">
                        <div class="list">
                            <span>${status.index + 1}</span>
                            <span>${shooter.name}</span>
                            <span>${shooter.teamName}</span>
                            <span>${shooter.avgScore}</span>
                        </div>
                    </c:forEach>
                    </c:if>
                </div>
            </div>

            <div id="item3mobile" class="mui-slider-item">
                <div class="table-head">
                    <span>排名</span>
                    <span>球员</span>
                    <span>球队</span>
                    <span>场均助攻</span>
                </div>
                <div class="table-content">
                    <c:if test="${leagueBang.helpScoreList != null}">
                    <c:forEach items="${leagueBang.helpScoreList}" var="assist" varStatus="status">
                        <div class="list">

                                <span>${status.index + 1}</span>
                                <span>${assist.name}</span>
                                <span>${assist.teamName}</span>
                                <span>${assist.avgHelp}</span>

                        </div>
                    </c:forEach>
                        </c:if>
                </div>
            </div>
            <div id="item4mobile" class="mui-slider-item">
                <div class="table-head">
                    <span>排名</span>
                    <span>球员</span>
                    <span>球队</span>
                    <span>场均篮板</span>
                </div>
                <div class="table-content">
                    <c:if test="${leagueBang.backboardList != null}">
                    <c:forEach items="${leagueBang.backboardList}" var="save" varStatus="status">
                        <div class="list">
                            <span>${status.index + 1}</span>
                            <span>${save.name}</span>
                            <span>${save.teamName}</span>
                            <span>${save.avgBackboard}</span>
                        </div>
                    </c:forEach>
                        </c:if>
                </div>
            </div>
            <div id="item5mobile" class="mui-slider-item">
                <div class="table-head">
                    <span>排名</span>
                    <span>球员</span>
                    <span>球队</span>
                    <span>场均抢断</span>
                </div>
                <div class="table-content">
                    <c:if test="${leagueBang.stealsList != null}">
                    <c:forEach items="${leagueBang.stealsList}" var="redYellow" varStatus="status">
                        <div class="list">
                            <span>${status.index + 1}</span>
                            <span>${redYellow.name}</span>
                            <span>${redYellow.teamName}</span>
                            <span>${redYellow.avgSteals}</span>
                        </div>
                    </c:forEach>
                        </c:if>
                </div>

            </div>
            <div id="item6mobile" class="mui-slider-item">
                <div class="table-head">
                    <span>排名</span>
                    <span>球员</span>
                    <span>球队</span>
                    <span>场均盖帽</span>
                </div>
                <div class="table-content">
                    <c:if test="${leagueBang.coverList != null}">
                    <c:forEach items="${leagueBang.coverList}" var="redYellow" varStatus="status">
                        <div class="list">
                            <span>${status.index + 1}</span>
                            <span>${redYellow.name}</span>
                            <span>${redYellow.teamName}</span>
                            <span>${redYellow.avgCover}</span>
                        </div>
                    </c:forEach>
                        </c:if>
                </div>

            </div>
        </div>
    </div>

</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript">
    mui.init({
        swipeBack: false
    });
    (function($) {
        $('.mui-scroll-wrapper').scroll({
            indicators: true //是否显示滚动条
        });
    })(mui);

    $('.sliderSegmentedControl a').on('tap', function () {
        var index=$(this).index();
        $('.sliderSegmentedControl a').removeClass('mui-active');
        $(this).addClass('mui-active');
        $('.slideBox .mui-slider-item').hide();
        $('.slideBox .mui-slider-item').eq(index).show();
    })
</script>
</body>
</html>

