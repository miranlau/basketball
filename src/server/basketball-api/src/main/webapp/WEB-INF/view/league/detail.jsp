<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/detail.css" />

    <link rel="stylesheet" href="<%=base%>/resource/alert/sweet-alert.css" />
    <script type="text/javascript" src="<%=base%>/resource/alert/sweet-alert.js" ></script>
</head>
<body>
<div class="mui-content">
    <!--isOver 比赛报名已结束 -->
    <div class="detail-content">
        <div class="detail-img"><img src="<%=base%>/resource/img/banner.png" /></div>
        <div class="detail-info">
            <div class="title">
                <h4 class="mui-pull-left mui-ellipsis-2" id="title"> ${league.name}</h4>
                <!--no-follow  已关注-->
                <a href="javascript:window.JSBridge.PayAttentionAction();" <c:if test="${league.isfollow}">class="mui-btn mui-pull-right no-follow"</c:if>
                   <c:if test="${!league.isfollow}">class="mui-btn mui-pull-right"</c:if>>
                    <span class="mui-icon"></span><em>关注</em><i>已关注</i>
                </a>
            </div>
            <div class="location-time">
                <p><img src="<%=base%>/resource/img/icon/time.png" />
                    <fmt:formatDate value="${league.begindate}" pattern="yyyy-MM-dd" />
                 至 <fmt:formatDate value="${league.enddate}" pattern="yyyy-MM-dd" /></p>
                <p><img src="<%=base%>/resource/img/icon/map.png" />${league.areaname} </p>
                <p><img src="<%=base%>/resource/img/icon/pattern.png" />${league.matchtype}人制</p>
            </div>
            <div class="notice">
                <div class="main">${league.details}</div>
            </div>
        </div>
        <c:choose>
            <c:when test="${league.status == 0}">
                <div class="marque marque-start">
                    <img src="<%=base%>/resource/img/icon/warn.png" />该赛事正在报名中！
                </div>
            </c:when>
            <c:when test="${league.status == 1}">
                <div class="marque marque-start">
                    <img src="<%=base%>/resource/img/icon/warn.png" />该赛事报名已结束，请等待比赛开始！
                </div>
            </c:when>
        </c:choose>
        <!--isPartake 是否参与  有这个类，参与了，没这个类，代表需要报名-->
        <%--<div class="editBtn">--%>
            <%--<a href="enroll?leagueId=${league.id}&teamId" class="mui-btn mui-btn-block partake">参与报名</a>--%>
            <%--<a href="javascript:void(0)" class="mui-btn mui-btn-block giveup">取消报名</a>--%>
        <%--</div>--%>
    </div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/detail.js" ></script>
<script>


</script>
</body>
</html>

