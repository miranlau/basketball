<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/dynamic-detail.css" />
<style>
    .listBox {
        overflow: hidden;
    }
</style>
    <!--<link rel="stylesheet" href="alert/sweet-alert.css" />
    <script type="text/javascript" src="alert/sweet-alert.js" ></script>-->
</head>
<body>
<div class="mui-content">
    <div class="dynamic-content">
        <div class="head">
            <img src="<%=base%>/resource/img/head.png" />
            <div class="name-time">
                <h4>赛事组委会</h4>
                <p>${infoDetail.pubTime}</p>
            </div>
        </div>

        <div class="description">
            <a href="javascript:void(0)">
                <p>
                    ${infoDetail.content}
                </p>
                <div class="matchImgBox">
                    <c:forEach items="${infoDetail.imgs}" var="img">
                        <span><img src="${img}" /></span>
                    </c:forEach>
                </div>
            </a>
        </div>
        <div class="pageView">
            <a href="javascript:void(0)">
                <img src="<%=base%>/resource/img/icon/read.png" />${infoDetail.read_num}
            </a>
            <a id="submitlaud">
                <img src="<%=base%>/resource/img/icon/praise.png" />${infoDetail.laud_num}
            </a>
            <a href="javascript:void(0)">
                <img src="<%=base%>/resource/img/icon/evaluate.png" />${infoDetail.comment_num}
            </a>
        </div>
    </div>
    <div class="listBox">
        <%--<c:forEach items="${infoDetail.comments}" var="comment">--%>
            <%--<div class="item">--%>
                <%--<img src="${comment.sender.avatar}" />--%>
                <%--<div class="con">--%>
                    <%--<h4>${comment.sender.name}</h4>--%>
                    <%--<p>${comment.pubTime}</p>--%>
                    <%--<div class="description">--%>
                            <%--${comment.content}--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</c:forEach>--%>
    </div>
    <div class="footer">正在加载中</div>
    <div class="from-box">
        <input type="hidden" id="infoId" value="${infoDetail.id}">
        <input type="text" id="content" class="form-control mui-pull-left" />
        <button id="submitcomment" class="mui-pull-right"><img src="<%=base%>/resource/img/icon/send.png" /></button>
    </div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/match-detail.js" ></script>
<script>
    $("#submitcomment").on("tap",function(){
        var content = $("#content").val();
        var infoId = $("#infoId").val();
        var send = [];
        if (content == '') {
            mui.toast("请输入评论内容");
        } else {
            /**将评论内容和ID拼接为数组供客户端接收*/
            send.push(content);
            send.push(infoId);
            window.JSBridge.ActionComment(send);
        }
    })
    $("#submitlaud").on("tap",function(){
        var newLaud = $("#submitlaud").val();
        var infoId = $("#infoId").val();
        window.JSBridge.ActionLaud(infoId);
        $("#submitlaud").val(newLaud + 1);
    })
    var pageNumber = 1;//初始化页码
    var id;
    $(function(){
        //页面加载初始化动态信息
        $('.footer').hide();
        id = ${infoId};
        if (id != undefined) {
            getData(id,pageNumber);
        }

    })
    function getData(id,pageNumber){
        $.getJSON("queryCommentsByInfoId",{
            infoId:id,
            pageNumber:pageNumber
        },function(json){
            if (json.flag == 0 && json.code == 0) {
                var data = json.data.content;
                var str='';
                $.each(data,function(index,obj){
                    str+='<div class="item">'+
                        '<img src="' + convertAvatar(obj.sender.avatar) + '" />'+
                        '<div class="con">'+
                        '<h4>' + obj.sender.name + '</h4>'+
                        '<p>' + obj.pubTime + '</p>'+
                        '<div class="description">'+ obj.content +
                        '</div>'+
                        '</div>'+
                        '</div>';
                })
                $('.listBox').append(str);
            }
        })
    }
    var url=window.document.location.href;
    var pName=window.document.location.pathname;
    var localhostPaht=url.substring(0,url.indexOf(pName))+'/football-api/';
    var urlPattern = /^((http)?:\/\/)[^\s]+/;
    function convertAvatar(avatar) {
        if(urlPattern.test(avatar)){
            return avatar;
        }else{
            return localhostPaht+avatar;
        }
    }
    $(window).on('drag',function(){

    })
    $(window).on('dragend',function(){
        var bodyH=$('body').height();
        var winH=$(window).height();
        var scrollH=$(window).scrollTop();
        if((bodyH-winH)<scrollH){
            $('.footer').show();
            setTimeout(function(){
                pageNumber = pageNumber + 1;
                getData(id,pageNumber);
                $('.footer').hide();
            },200);

        }
    })
</script>
</body>
</html>

