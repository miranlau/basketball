<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/match.css" />

    <!--<link rel="stylesheet" href="alert/sweet-alert.css" />
    <script type="text/javascript" src="alert/sweet-alert.js" ></script>-->
</head>
<body>
<div class="mui-content">
    <div class="match-content">
        <div class="match-img"><img src="<%=base%>/resource/img/banner-radius.png" /></div>
        <div class="title">
            ${league.name}
        </div>
        <div class="trophy">
            <img src="<%=base%>/resource/img/icon/trophy-shine.png" />
            <h4>
                <c:if test="${league.status == 2}">
                    联赛已经开始
                </c:if>
                <c:if test="${league.status == 3}">
                    联赛已经结束
                </c:if>
            </h4>
        </div>
        <div class="location-time">
            <span><img src="<%=base%>/resource/img/icon/time.png" />
                <fmt:formatDate value="${league.begindate}" pattern="yyyy-MM-dd" />
                 至 <fmt:formatDate value="${league.enddate}" pattern="yyyy-MM-dd" />
            </span>
            <span><img src="<%=base%>/resource/img/icon/map.png" />${league.areaname}</span>
            <span><img src="<%=base%>/resource/img/icon/pattern.png" />${league.matchtype}人制</span>
        </div>
        <div class="tab">
            <a href="javascript:window.JSBridge.ActionTeamsList();">
                <img src="<%=base%>/resource/img/icon/billboard.png" />榜单
            </a>
            <a href="javascript:window.JSBridge.ActionTeams();">
                <img src="<%=base%>/resource/img/icon/team.png" />球队
            </a>
            <%--<a href="<%=base%>/ctl/league/queryMatchByLeagueId?leagueId=${league.id}">--%>
            <a href="javascript:window.JSBridge.ActionTeamsProcess();">
                <img src="<%=base%>/resource/img/icon/schedule.png" />赛程
            </a>
        </div>
    </div>
    <div class="match-content">
        <div class="listBox">
        </div>
    </div>
    <div class="footer">正在加载中</div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/match.js" ></script>
<script>
    var pageNumber = 1;//初始化页码
    var id ;
    $(function(){
        //页面加载初始化动态信息
        id = ${league.id};
        if (id != undefined) {
            getData(id,pageNumber);
        }

    })
    function getData(id,pageNumber){
        $.getJSON("getLeagueInfos",{
            leagueId:id,
            pageNumber:pageNumber
        },function(json){
            console.log(json)
            if (json.flag == 0 && json.code == 0) {
                var data = json.data.content;
                var str='';
                $.each(data,function(index,obj){
                    var imges='';
                    $.each(obj.imgs,function(index,img){
                        imges += '<span><img src="' + img + '" /></span>'
                    })
                    str+='<div class="item">'+
                            '<div class="head">'+
                            '<img src="<%=base%>/resource/img/head.png" />'+
                            '<div class="name-time">'+
                            '<h4>赛事组委会</h4>'+
                            '<p>' + obj.pubTime + '</p>'+
                            '</div>'+
                            '</div>'+
                            '<div class="description">'+
                            '<a data-id="'+ obj.id +'"+>'+
                            '<p>'+ obj.content +
                            '</p>'+
                            '<div class="matchImgBox">' + imges +
                            '</div>'+
                            '</a>'+
                            '</div>'+
                            '<div class="pageView">'+
                            '<a href="javascript:void(0)">'+
                            '<img src="<%=base%>/resource/img/icon/read.png" />'+ obj.read_num +
                            '</a>'+
                            '<a href="javascript:void(0)">'+
                            '<img src="<%=base%>/resource/img/icon/praise.png" />'+ obj.laud_num +
                            '</a>'+
                            '<a href="javascript:void(0)">'+
                            '<img src="<%=base%>/resource/img/icon/evaluate.png" />'+ obj.comment_num +
                            '</a>'+
                            '</div>'+
                            '</div>'
                })
                $('.listBox').append(str);
                $('.description a').on("tap",function () {
                    window.JSBridge.ActionCheckInfo(parseInt($(this).data('id')));
                })
            }
        })
    }

    $(window).on('drag',function(){

    })
    $(window).on('dragend',function(){
        var bodyH=$('body').height();
        var winH=$(window).height();
        var scrollH=$(window).scrollTop();
        if((bodyH-winH)<scrollH){
            $('.footer').show();
            setTimeout(function(){
                pageNumber = pageNumber + 1;
                getData(id,pageNumber);
                $('.footer').hide();
            },200);

        }
    })


</script>
</body>
</html>
