<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.List" %>
<%@ page import="com.renyi.basketball.bussness.dto.MatchDto" %>
<%@ page import="com.renyi.basketball.bussness.utils.DateUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/schedule-list.css" />


    <style>

    </style>
</head>
<body>
<div class="mui-content">
    <div class="schedule">
        <ul>
            <%
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                List<MatchDto> matchDtos = (List<MatchDto>) request.getAttribute("matches");
                String dayTo = "";
                if(matchDtos != null) {
                for (MatchDto match:matchDtos) {
                    if (!dayTo.equals(DateUtil.showWeek(match.getDate()))){
                        dayTo = DateUtil.showWeek(match.getDate());
                        if ("今天".equals(DateUtil.showWeek(match.getDate()))){
                    %>
                        <div class="time-today"><%=DateUtil.showWeek(match.getDate())%></div>
                    <%
                        }else {
                    %>
                        <div class="time"> <%=DateUtil.showWeek(match.getDate())%></div>
                    <%
                        }
                    }
            %>
            <li>
                <div class="team left">
                    <a href="javascript:window.JSBridge.ActionGoToTeam(<%=match.getHome().getTeamId()%>);">
                        <div class="imgbox">
                            <img src="<%=match.getHome().getLogo() == null || "".equals(match.getHome().getLogo()) ?
                            base + "/resource/img/default_team.png" : match.getHome().getLogo()%>" />
                            <!--获胜队伍 -->
                            <%--<span><img src="<%=base%>/resource/img/icon/winner.png" /></span>--%>
                        </div>
                        <h4 class="mui-ellipsis-2"><%=match.getHome().getName()%></h4>
                    </a>
                </div>
                <div class="content">
                    <a class="submit">
                        <input type="hidden" name="status" value="<%=match.getStatus()%>">
                        <input type="hidden" name="id" value="<%=match.getMatchId()%>">
                        <div class="score">
                            <c:choose>
                                <c:when test="<%=match.getStatus() == 100%>">
                                    <!--比赛结束，显示比分-->
                                    <%=match.getHomescore()%>:<%=match.getVisitingscore()%>
                                </c:when>
                                <c:when test="<%=match.getStatus() == 110%>">
                                    <!--比赛取消 -->
                                    本场比赛已取消
                                </c:when>
                                <c:when test="<%=match.getStatus() == 20%>">
                                    <!--比赛中 -->
                                    比赛中
                                </c:when>
                                <c:otherwise>
                                    <!--显示比赛时间-->
                                    <%=sdf.format(match.getDate())%>开始
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="name mui-ellipsis-2"><%=match.getLeagueName()%></div>
                        <div class="address"><%=match.getAddr()%></div>
                    </a>
                </div>
                <div class="team right">
                    <a href="javascript:window.JSBridge.ActionGoToTeam(<%=match.getVisiting().getTeamId()%>);">
                        <div class="imgbox">
                            <img src="<%=match.getVisiting().getLogo() == null || "".equals(match.getVisiting().getLogo()) ?
                            base + "/resource/img/default_team.png" : match.getVisiting().getLogo()%>" />
                            <!--获胜队伍 -->
                            <%--<span><img src="<%=base%>/resource/img/icon/winner.png" /></span>--%>
                        </div>
                        <h4 class="mui-ellipsis-2"><%=match.getVisiting().getName()%></h4>
                    </a>
                </div>
            </li>
            <%
                    }
                }
            %>
        </ul>
    </div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script>
    $(".submit").on("tap",function(){
        var id = $(this).find("input[name='id']").val();
        var status = $(this).find("input[name='status']").val();
        var send  = [];
        send.push(status);
        send.push(id)
        window.JSBridge.ActionGoToMatch(send);
    })
</script>
</body>
</html>
