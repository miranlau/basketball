<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/team-list.css" />


    <style>
        .star img{
            width: 10px;
        }
        .half-star{
            width: 10px;
            display: inline-block;
            position: relative;
        }
        .half-star div{
            line-height: 0;
        }
        .half-star div.left{
            position: absolute;
            left: 0;
            top: 0;
            z-index: 5;
            overflow: hidden;
        }
    </style>
</head>
<body>
<div class="mui-content">
    <ul>
        <c:forEach items="${teams}" var="team">
            <li>
                <a class="go">
                    <input type="hidden" name="id" value="${team.id}">
                    <c:choose>
                        <c:when test="${team.logo != null || team.logo !=''}">
                            <img class="mui-media-object mui-pull-left" src="${team.logo}">
                        </c:when>
                        <c:otherwise>
                            <img class="mui-media-object mui-pull-left" src="<%=base%>/resource/img/default_team.png">
                        </c:otherwise>
                    </c:choose>
                    <div class="mui-media-body">
                        <h4>
                            <span class="mui-pull-left">${team.name}</span>
                            <span class="mui-pull-right">${team.level}级球队</span>
                        </h4>
                        <div class="star">
                            <c:forEach var="item" begin="0" end="4" varStatus="status">
                                <div class="half-star">
                                    <div class="left" style="width:
                                    <c:choose>
                                            <c:when test="${(team.stars - status.index * 20) >= 20 }">
                                                100%
                                            </c:when>
                                            <c:when test="${(team.stars - status.index * 20) <= 0 }">
                                                0%
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:formatNumber value="${(team.stars - status.index * 20) / 20}" type="percent" />
                                            </c:otherwise>
                                    </c:choose>
                                            !important;"><img src="<%=base%>/resource/img/icon/star-yellow.png" /></div>
                                    <div class="right"><img src="<%=base%>/resource/img/icon/star-white.png" /></div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="record">
                            <span class="mui-pull-left"><img src="<%=base%>/resource/img/icon/record.png"/>战绩：${team.win}胜${team.deuce}平${team.lose}负</span>
                            <span class="mui-pull-right"><img src="<%=base%>/resource/img/icon/age.png"/>平均年龄：${team.age}</span>
                        </div>
                    </div>
                </a>
            </li>
        </c:forEach>
    </ul>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script>
    $(".go").on("tap", function () {
        var id = $(this).find("input[name='id']").val();
       // alert(id);
        window.JSBridge.ActionAccessTeam(id);
    })
</script>
</body>
</html>

