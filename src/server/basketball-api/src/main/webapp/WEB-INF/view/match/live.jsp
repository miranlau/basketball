<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-数据-直播</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <script type="text/javascript" src="<%=contextPath%>/resource/js/jquery-2.1.0.js"></script>
</head>

<body>
    
    <!-- 比赛基本信息 -->
	<jsp:include page="../match_stats/basic.jsp" flush="true" />
	
    <section class="flex-container text-center border-bottom padding">
        <div class="flex">
            <div class="btn active">直播</div>
        </div>
        <div class="flex">
            <a class="btn" href="<%=contextPath%>/ctl/match_stats/detail?matchId=${matchId}">数据</a>
        </div>
        <div class="flex">
            <a class="btn" href="<%=contextPath%>/ctl/team/related_match?matchId=${matchId}">相关</a>
        </div>
    </section>
    <table id="liveList" class="timeline margin">
    	<!--
        <tr>
            <th>19:06</th>
            <td>
                各位网友晚上好
            </td>
        </tr>
        <tr>
            <th>19:06</th>
            <td>
                欢迎大家来到全网CBA直播室
            </td>
        </tr>
        <tr>
            <th>19:06</th>
            <td>
                即将为您带来CBA常规赛，四川VS广东，欢迎收看
            </td>
        </tr>
        <tr>
            <th>19:07</th>
            <td>
                [0:0]四川男篮上一场主场大胜广州男篮。目前以13胜12负排名CBA第9。
            </td>
        </tr>
        <tr>
            <th>19:35</th>
            <td>
                [0:0]跳球开始！！！
            </td>
        </tr>
        <tr>
            <th>19:37</th>
            <td class="cell">
                <img src="css/img/logo1.png" alt="四川品胜"> <span class="top">[2:0]孟达左侧运球投篮两分：命中</span>
            </td>
        </tr>
        <tr>
            <th>19:37</th>
            <td class="cell">
                <img src="css/img/logo2.png" alt="广东宏远"> <span class="top">[2:0]易建篮球下：不中</span>
            </td>
        </tr>
         -->
    </table>
    <script type="text/javascript">
	    var latestId = 0;
	    
	    var refreshLiveInfo = function() {
	    	$.ajax({
	   			type: 'get',
	   			url: '<%=contextPath%>/ctl/action/speaks',
	   			data: {
	   				matchId: '${matchId}',
	   				latestId: latestId
	   			},
	   			dataType: 'json',
	   			timeout: 20000,
	   			success: function(json) {
	   				var liveList = json.data;
	   				if (liveList == null || liveList.length == 0) {
	   					return;
	   				}
	   				$.each(liveList, function(i, val) {
	   					$('#liveList').append('<tr><th>' + val.time + '</th><td>' + val.speak + '</td></tr>');
	   					latestId = val.id;
	   				});
				}
			});
	    };
	    
	    $(function() {
	    	var refreshInterval = 30000;
	    	setInterval('refreshLiveInfo()', refreshInterval);
	    	
	    	refreshLiveInfo();
		});
    
    </script>
</body>

</html>