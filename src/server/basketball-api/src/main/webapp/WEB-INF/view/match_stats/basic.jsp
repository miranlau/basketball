<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>
<script type="text/javascript">
    
	var loadMatchStatsBasicInfo = function() {
		$.ajax({
   			type: 'get',
   			url: '<%=contextPath%>/ctl/match_stats/basic',
   			data: {
   				matchId: '${matchId}'
   			},
   			dataType: 'json',
   			timeout: 5000,
   			success: function(json) {
   				if (json.code == 1004) {
					location.href = '<%=contextPath%>/jsp/404.jsp';
					return;
   				}
   				var data = json.data;
   				var homeName = data.homeName;
   				var visitingName = data.visitingName;
   				$('#homeName').text(homeName);
   				$('#visitingName').text(visitingName);
   				$('#homeLogo').attr('src', data.homeLogo);
   				$('#visitingLogo').attr('src', data.visitingLogo);
				$('#matchScores').text(data.homeScore + ' - ' + data.visitingScore);
				$('#quarterName').text(data.quarterName);
				var matchStatus = data.matchStatus;
				if (matchStatus > 0 && matchStatus < 100) {
					$('#quarterRemainingTime').text(data.quarterRemainingTime);
				}
			}
		});
	};

	$(function() {
		loadMatchStatsBasicInfo();
	});
	
</script>

<section class="hero">
	<div class="flex-container padding">
		<div class="flex">
			<a href="<%=contextPath%>/ctl/team/lineup?teamId=${homeId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}"><img id="homeLogo" class="logo" src="" alt=""></a>
			<p id="homeName"></p>
		</div>
		<div class="flex flex-container column">
			<h3 id="matchScores"></h3>
			<p id=quarterName></p>
			<p id="quarterRemainingTime"></p>
		</div>
		<div class="flex">
			<a href="<%=contextPath%>/ctl/team/lineup?teamId=${visitingId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}"><img id="visitingLogo" class="logo" src="" alt=""></a>
			<p id="visitingName"></p>
		</div>
	</div>
	<!-- 
	<div class="progressbar margin-horizontal">
		<div class="blue" style="width: 100%"></div>
		<div class="orange" style="width: 60%"></div>
	</div>
	<div class="flex-container margin-horizontal padding-top">
		<div class="flex">
			<div class="cell">
				<img class="like" src="<%=contextPath%>/css/img/hand1.png" alt="点赞">
				<span class="middle">8888</span>
			</div>
		</div>
		<div class="flex">
			<div class="cell float-right margin-right">
				<span class="middle">6666</span> <img class="like"
					src="<%=contextPath%>/resource/css/img/hand2.png" alt="点赞">
			</div>
		</div>
	</div>
	 -->
</section>
