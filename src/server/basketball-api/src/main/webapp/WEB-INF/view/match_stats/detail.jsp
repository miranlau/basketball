<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-数据-数据展示</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <script type="text/javascript" src="<%=contextPath%>/resource/js/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="<%=contextPath%>/resource/js/echarts-3.5.4.js"></script>
</head>

<body>
	<!-- 比赛基本信息 -->
	<jsp:include page="basic.jsp" flush="true" />
    
    <section class="flex-container text-center padding">
        <div class="flex">
        	<a class="btn" href="<%=contextPath%>/ctl/match/live?matchId=${matchId}">直播</a>
        </div>
        <div class="flex">
        	<a class="btn active">数据</a>
        </div>
        <div class="flex">
	         <a class="btn" href="<%=contextPath%>/ctl/team/related_match?matchId=${matchId}">相关</a>
        </div>
    </section>
    <table id="quarterScoreList" class="border-top border-bottom" style="width:100%;">
        <thead>
            <tr id="quarterTitle"></tr>
        </thead>
        <tbody>
            <tr id="quarterHomeScore"></tr>
            <tr id="quarterVisitingScore"></tr>
        </tbody>
    </table>
    <table style="width: 100%">
        <thead>
            <tr>
                <th></th>
                <th class="nowrap">
                    <div class="cell">
                        <span id="spanHomeName" class="middle"></span>
                        <img id="imgHomeLogo" src="" alt="">
                    </div>
                </th>
                <th></th>
                <th class="nowrap">
                    <div class="cell">
                        <img id="imgVisitingLogo" src="" alt="">
                        <span id="spanHomeName" class="middle"></span>
                    </div>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id="homeReboundComparsion"></td>
                <td style="width: 35%">
                    <div class="progressbar">
                        <div id="homeReboundComparsionBack" class="orange" style="width:100%"></div>
                        <div id="homeReboundComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td>篮板</td>
                <td style="width: 35%">
                    <div class="progressbar">
                        <div id="visitingReboundComparsionBack" class="gray" style="width:100%"></div>
                        <div id="visitingReboundComparsionFront" class="blue" style="width:0%"></div>
                    </div>
                </td>
                <td id="visitingReboundComparsion"></td>
            </tr>
            <tr>
                <td id="homeAssistComparsion"></td>
                <td>
                    <div class="progressbar">
                        <div id="homeAssistComparsionBack" class="blue" style="width: 100%"></div>
                        <div id="homeAssistComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td>助攻</td>
                <td>
                    <div class="progressbar">
                        <div id="visitingAssistComparsionBack" class="gray" style="width:100%"></div>
                        <div id="visitingAssistComparsionFront" class="orange" style="width: 0%"></div>
                    </div>
                </td>
                <td id="visitingAssistComparsion"></td>
            </tr>
            <tr>
                <td id="homeTurnoverComparsion"></td>
                <td>
                    <div class="progressbar">
                        <div id="homeTurnoverComparsionBack" class="blue" style="width: 100%"></div>
                        <div id="homeTurnoverComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td>失误</td>
                <td>
                    <div class="progressbar">
                        <div id="visitingTurnoverComparsionBack" class="gray" style="width:100%"></div>
                        <div id="visitingTurnoverComparsionFront" class="orange" style="width: 0%"></div>
                    </div>
                </td>
                <td id="visitingTurnoverComparsion"></td>
            </tr>
            <tr>
                <td id="homeStealComparsion"></td>
                <td>
                    <div class="progressbar">
                        <div id="homeStealComparsionBack" class="orange" style="width:100%"></div>
                        <div id="homeStealComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td>抢断</td>
                <td>
                    <div class="progressbar">
                        <div id="visitingStealComparsionBack" class="gray" style="width:100%"></div>
                        <div id="visitingStealComparsionFront" class="blue" style="width:0%"></div>
                    </div>
                </td>
                <td id="visitingStealComparsion"></td>
            </tr>
            <tr>
                <td id="homeBlockShotComparsion"></td>
                <td>
                    <div class="progressbar">
                        <div id="homeBlockShotComparsionBack" class="orange" style="width: 100%"></div>
                        <div id="homeBlockShotComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td>盖帽</td>
                <td>
                    <div class="progressbar">
                        <div id="visitingBlockShotComparsionBack" class="gray" style="width:100%"></div>
                        <div id="visitingBlockShotComparsionFront" class="blue" style="width:0%"></div>
                    </div>
                </td>
                <td id="visitingBlockShotComparsion"></td>
            </tr>
            <tr>
                <td id="homeFoulComparsion"></td>
                <td>
                    <div class="progressbar">
                        <div id="homeFoulComparsionBack" class="blue" style="width: 100%"></div>
                        <div id="homeFoulComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td>犯规</td>
                <td>
                    <div class="progressbar">
                        <div id="visitingFoulComparsionBack" class="gray" style="width:100%"></div>
                        <div id="visitingFoulComparsionFront" class="orange" style="width: 0%"></div>
                    </div>
                </td>
                <td id="visitingFoulComparsion"></td>
            </tr>
            <tr>
                <td id="homeOffensivePctComparsion"></td>
                <td>
                    <div class="progressbar">
                        <div id="homeOffensivePctComparsionBack" class="orange" style="width: 100%"></div>
                        <div id="homeOffensivePctComparsionFront" class="gray" style="width:100%"></div>
                    </div>
                </td>
                <td class="nowrap">进攻成功率%</td>
                <td>
                    <div class="progressbar">
                        <div id="visitingOffensivePctComparsionBack" class="gray" style="width: 100%"></div>
                        <div id="visitingOffensivePctComparsionFront" class="blue" style="width:0%"></div>
                    </div>
                </td>
                <td id="visitingOffensivePctComparsion"></td>
            </tr>
    </table>
    <section class="text-center padding">
        <div class="btn-group">
            <div id="btnHomeStats" class="btn active"></div>
            <div id="btnVisitingStats" class="btn"></div>
        </div>
    </section>
    <section id="homeTeamStats">
    	<section class="scroll-container">
	        <div class="scrollable">
	            <table class="strip nowrap">
	                <thead class="border-top border-bottom">
	                    <tr>
	                        <th>球员</th>
	                        <th>号码</th>
	                        <th>得分</th>
	                        <th>两分球</th>
	                        <th>两分球命中率</th>
	                        <th>三分球</th>
	                        <th>三分球命中率</th>
	                        <th>罚球</th>
	                        <th>罚球命中率</th>
	                        <th>后场篮板</th>
	                        <th>前场篮板</th>
	                        <th>助攻</th>
	                        <th>抢断</th>
	                        <th>盖帽</th>
	                        <th>失误</th>
	                        <th>犯规</th>
	                    </tr>
	                </thead>
	                <tbody id="playerMatchStats"></tbody>
	            </table>
	        </div>
	    </section>
	    <section class="padding">
	        <h3 id="teamShottingArea" class="orange"></h3>
	        <div class="topview margin-vertical">
	            <img style="width: 100%" src="<%=contextPath%>/resource/css/img/area.png" alt="">
	            <div id="threeLeftWingArea" class="lt">0-0<br>0.00%</div>
	            <div id="threeLeftside45Area" class="lb">0-0<br>0.00%</div>
	            <div id="twoLeftsideArea" class="lc">0-0<br>0.00%</div>
	            <div id="twoPaintedArea" class="ct">0-0 0.00%</div>
	            <div id="twoFrontArea" class="cm">0-0 0.00%</div>
	            <div id="threeRootArcArea" class="cb">0-0 0.00%</div>
	            <div id="threeRightWingArea" class="rt">0-0<br>0.00%</div>
	            <div id="threeRightside45Area" class="rb">0-0<br>0.00%</div>
	            <div id="twoRightsideArea" class="rc">0-0<br>0.00%</div>
	        </div>
	        <table class="highlight">
	            <thead>
	                <tr>
	                    <th>运球投篮</th>
	                    <th>接球投篮</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td id="dribblingStats">0-0</td>
	                    <td id="standingStats">0-0</td>
	                </tr>
	            </tbody>
	        </table>
	    </section>
	    <section class="padding">
	        <h3 id="teamAdvanceStats" class="orange"></h3>
	        <p id="offensiveAttempt" class="padding-top">节奏：0.0</p>
	        <p id="offensiveEff" class="gray">进攻效率：0.0</p>
	        <p id="defensiveEff" class="gray padding-bottom">防守效率：0.0</p>
	        <table class="highlight" style="margin: 0 auto">
	            <thead>
	                <tr>
	                    <th colspan="4">进攻侧重</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr>
	                    <td>左侧</td>
	                    <td>正面</td>
	                    <td>篮下</td>
	                    <td>右侧</td>
	                </tr>
	                <tr>
	                    <td id="leftOffensivePct">0.00%</td>
	                    <td id="frontOffensivePct">0.00%</td>
	                    <td id="paintedOffensivePct">0.00%</td>
	                    <td id="rightOffensivePct">0.00%</td>
	                </tr>
	            </tbody>
	        </table>
	    </section>
    </section>
    <section class="padding">
    	<div id="radar" style="margin: 0 auto;width:auto;height:400px"></div>
    </section>
    
    <script type="text/javascript">
    	var isFirstLoad = true;
    	var refreshMatchStatsDetailInfo = function() {
    		$.ajax({
       			type: 'get',
       			url: '<%=contextPath%>/ctl/match_stats/advanced',
       			data: {
       				matchId: '${matchId}'
       			},
       			dataType: 'json',
       			timeout: 20000,
       			success: function(json) {
       				var data = json.data;
       				// 球队数据统计
       				var homeTeamStats = data.teamStats.home;
       				var visitingTeamStats = data.teamStats.visiting;
       				// 球员数据统计
       				var homePlayerStatsList = data.playerStatsList.home;
       				var visitingPlayerStatsList = data.playerStatsList.visiting;
       				// 球队名
       				var homeName = homeTeamStats.teamName;
       				var visitingName = visitingTeamStats.teamName;
       				// 加载每节比分
       				loadQuarterList(homeName, visitingName, data.quarterScoreList);
       				// 加载两只球队数据对比
       				loadBasicStatsComparsion(homeTeamStats, visitingTeamStats);
					// 加载主队的数据统计       				
       				loadHomeTeamStats(homeName, homeTeamStats, homePlayerStatsList);
					// 加载客队的数据统计
       				loadVisitingTeamStats(visitingName, visitingTeamStats, visitingPlayerStatsList);
    			}
    		});
    	};
    	
    	// 加载主队的数据统计
    	var loadHomeTeamStats = function(homeName, homeTeamStats, homePlayerStatsList) {
    		var btnHomeStats = $('#btnHomeStats');
			btnHomeStats.text(homeName);
			btnHomeStats.click(function() {
				$(this).attr('class', 'btn active');
				$('#btnVisitingStats').attr('class', 'btn');
				loadTeamStats(homeTeamStats, homePlayerStatsList);
				loadRadar(homeName, homeTeamStats);
			});
			if (isFirstLoad) {
				loadTeamStats(homeTeamStats, homePlayerStatsList);
				loadRadar(homeName, homeTeamStats);
				isFirstLoad = false;
			}
    	};
    	// 加载客队的数据统计
    	var loadVisitingTeamStats = function(visitingName, visitingTeamStats, visitingPlayerStatsList) {
    		var btnVisitingStats = $('#btnVisitingStats');
			btnVisitingStats.text(visitingName);
			btnVisitingStats.click(function() {
				$(this).attr('class', 'btn active');
				$('#btnHomeStats').attr('class', 'btn');
				loadTeamStats(visitingTeamStats, visitingPlayerStatsList);
				loadRadar(visitingName, visitingTeamStats);
			});	
    	}
    	// 球队的数据统计
    	var loadTeamStats = function(teamStats, playerStatsList) {
    		loadplayerMatchStats(playerStatsList);
			loadShootingZone(teamStats);
			loadAdvancedStats(teamStats);
    	};
    	// 每节比分
    	var loadQuarterList = function(hostName, visitingName, quarterScoreList) {
    		if (quarterScoreList != undefined && quarterScoreList.length > 0) {
    			var quarterTitleTr = $('#quarterTitle');
    			var quarterHomeScoreTr = $('#quarterHomeScore');
    			var quarterVisitingScoreTr = $('#quarterVisitingScore');
    			
    			quarterTitleTr.empty();
    			quarterHomeScoreTr.empty();
    			quarterVisitingScoreTr.empty();
    			
    			quarterTitleTr.append('<td>球队</td>');
    			quarterHomeScoreTr.append('<td>' + hostName + '</td>');
    			quarterVisitingScoreTr.append('<td>' + visitingName + '</td>');
    			
    			$.each(quarterScoreList, function(i, val) {
    				if (i < quarterScoreList.length - 1) {
    					quarterTitleTr.append(val.isOT ? ('<th>加时'+val.quarterIndex+'</th>' ) : ('<th>第'+val.quarterIndex+'节</th>'));
        				quarterHomeScoreTr.append('<td>' + val.home + '</td>');
        				quarterVisitingScoreTr.append('<td>' + val.visiting + '</td>');
    				} else {
    					quarterTitleTr.append('<th>总分</th>');
        				quarterHomeScoreTr.append('<td>' + val.home + '</td>');
        				quarterVisitingScoreTr.append('<td>' + val.visiting + '</td>');
    				}
    			});
    		} else {
    			//alert('quarterScoreList is null');
    		}
    	};
    	// 两队的数据对比
    	var loadBasicStatsComparsion = function(homeTeamStats, visitingTeamStats) {
    		loadStatsProgressoBar('homeReboundComparsion', homeTeamStats.rebound, visitingTeamStats.rebound, false, true);
    		loadStatsProgressoBar('homeAssistComparsion', homeTeamStats.assist, visitingTeamStats.assist, false, true);
    		loadStatsProgressoBar('homeTurnoverComparsion', homeTeamStats.turnover, visitingTeamStats.turnover, false, true);
    		loadStatsProgressoBar('homeStealComparsion', homeTeamStats.steal, visitingTeamStats.steal, false, true);
    		loadStatsProgressoBar('homeBlockShotComparsion', homeTeamStats.blockShot, visitingTeamStats.blockShot,false, true);
    		loadStatsProgressoBar('homeFoulComparsion', homeTeamStats.foul, visitingTeamStats.foul, false, true);
    		loadStatsProgressoBar('homeOffensivePctComparsion', homeTeamStats.offensivePct, visitingTeamStats.offensivePct, true, true);
    		loadStatsProgressoBar('visitingReboundComparsion', homeTeamStats.rebound, visitingTeamStats.rebound, false, false);
    		loadStatsProgressoBar('visitingAssistComparsion', homeTeamStats.assist, visitingTeamStats.assist, false, false);
    		loadStatsProgressoBar('visitingTurnoverComparsion', homeTeamStats.turnover, visitingTeamStats.turnover, false, false);
    		loadStatsProgressoBar('visitingStealComparsion', homeTeamStats.steal, visitingTeamStats.steal, false, false);
    		loadStatsProgressoBar('visitingBlockShotComparsion', homeTeamStats.blockShot, visitingTeamStats.blockShot, false, false);
    		loadStatsProgressoBar('visitingFoulComparsion', homeTeamStats.foul, visitingTeamStats.foul, false, false);
    		loadStatsProgressoBar('visitingOffensivePctComparsion', homeTeamStats.offensivePct, visitingTeamStats.offensivePct, true, false);
    	};
    	// 数据统计的进度条
    	var loadStatsProgressoBar = function(id, homeVal, visitingVal, isPecentVal, isHome) {
    		if (isHome) {
    			$('#' + id).text(homeVal);
       			$('#' + id + 'Back').attr('class', ((homeVal >= visitingVal) ? 'orange' : 'blue'));
        		if(!isPecentVal) {
        			$('#' + id + 'Front').attr('style', 'width:' + ((visitingVal / (homeVal + visitingVal)) * 100) + '%');
        		} else {
        			$('#' + id + 'Front').attr('style', 'width:' + (100 - homeVal) + '%');
        		}
    		} else {
    			$('#' + id).text(visitingVal);
       			$('#' + id + 'Front').attr('class', ((homeVal >= visitingVal) ? 'blue' : 'orange'));
    			$('#' + id + 'Front').attr('style', 'width:' + ((visitingVal / (homeVal + visitingVal)) * 100) + '%');
    			if(!isPecentVal) {
    				$('#' + id + 'Front').attr('style', 'width:' + ((visitingVal / (homeVal + visitingVal)) * 100) + '%');
        		} else {
        			$('#' + id + 'Front').attr('style', 'width:' + visitingVal + '%');
        		}
    		}
    	};
    	// 球员数据
    	var loadplayerMatchStats = function(playerStatsList) {
    		if (playerStatsList != undefined && playerStatsList.length > 0) {
    			$('#playerMatchStats').empty();
    			$.each(playerStatsList, function(i, playerStats) {
    				var tr = '<tr>' +
	 	               '<td>' + playerStats.playerName + '</td>' +
	 	               '<td>' + playerStats.uniformNumber + '</td>' +
	 	               '<td>' + playerStats.score + '</td>' +
	 	               '<td>' + playerStats.twoPointsShot + '-' + playerStats.twoPointsAttempt + '</td>' +
	 	               '<td>' + playerStats.twoPointsPct + '%</td>' +
	 	               '<td>' + playerStats.threePointsShot + '-' + playerStats.threePointsAttempt + '</td>' +
	 	               '<td>' + playerStats.threePointsPct + '%</td>' +
	 	               '<td>' + playerStats.freeThrowShot + '-' + playerStats.freeThrowAttempt + '</td>' +
	 	               '<td>' + playerStats.freeThrowPct + '%</td>' +
	 	               '<td>' + playerStats.defensiveRebound + '</td>' +
	 	               '<td>' + playerStats.offensiveRebound + '</td>' +
	 	               '<td>' + playerStats.assist + '</td>' +
	 	               '<td>' + playerStats.steal + '</td>' +
	 	               '<td>' + playerStats.blockShot + '</td>' +
	 	               '<td>' + playerStats.turnover + '</td>' +
	 	               '<td>' + playerStats.foul + '</td>' +
	 			   '</tr>';
     				$('#playerMatchStats').append(tr);
    			});
    		} else {
    			//alert('playerStatsList is null');
    		}
    	};
    	// 投篮热区
    	var loadShootingZone = function(teamStats) {
    		$('#teamShottingArea').html(teamStats.teamName + '&nbsp;投篮热区图<a href="./team_stats_glossary" class="float-right white margin-right">？</a>');
    		$('#twoLeftsideArea').html(teamStats.twoLeftsideShot + '-' + teamStats.twoLeftsideAttempt + '<br/>' + teamStats.twoLeftsidePct + '%');
    		$('#twoRightsideArea').html(teamStats.twoRightsideShot + '-' + teamStats.twoRightsideAttempt + '<br/>' + teamStats.twoRightsidePct + '%');
    		$('#twoPaintedArea').html(teamStats.twoPaintedShot + '-' + teamStats.twoPaintedAttempt + '&nbsp;'+ teamStats.twoPaintedPct + '%');
    		$('#twoFrontArea').html(teamStats.twoFrontShot + '-' + teamStats.twoFrontAttempt + '&nbsp;'+ teamStats.twoFrontPct + '%');
    		$('#threeLeftWingArea').html(teamStats.threeLeftWingShot + '-' + teamStats.threeLeftWingAttempt + '<br/>'+ teamStats.threeLeftWingPct + '%');
    		$('#threeRightWingArea').html(teamStats.threeRightWingShot + '-' + teamStats.threeRightWingAttempt + '<br/>' + teamStats.threeRightWingPct + '%');
    		$('#threeLeftside45Area').html(teamStats.threeLeftside45Shot + '-' + teamStats.threeLeftside45Attempt + '<br/>' + teamStats.threeLeftside45Pct + '%');
    		$('#threeRightside45Area').html(teamStats.threeRightside45Shot + '-' + teamStats.threeRightside45Attempt + '<br/>' + teamStats.threeRightside45Pct + '%');
    		$('#threeRootArcArea').html(teamStats.threeLeftside45Shot + '-' + teamStats.threeLeftside45Attempt + '&nbsp;' + teamStats.threeLeftside45Pct + '%');
    	};
    	// 进阶数据
    	var loadAdvancedStats = function(teamStats) {
    		$('#dribblingStats').text(teamStats.dribblingShot + '-' + teamStats.dribblingAttempt);
    		$('#standingStats').text(teamStats.standingShot + '-' + teamStats.standingAttempt);
    		$('#offensiveAttempt').text('节奏：' + teamStats.offensiveAttempt);
    		$('#offensiveEff').text('进攻效率：' + teamStats.offensiveEff);
    		$('#defensiveEff').text('防守效率：' + teamStats.defensiveEff);
    		$('#teamAdvanceStats').html(teamStats.teamName + '&nbsp;进阶数据');
    		$('#leftOffensivePct').text(teamStats.leftOffensivePct + '%');
    		$('#frontOffensivePct').text(teamStats.frontOffensivePct + '%');
    		$('#paintedOffensivePct').text(teamStats.paintedOffensivePct + '%');
    		$('#rightOffensivePct').text(teamStats.rightOffensivePct + '%');
    	};
    	
    	var loadRadar = function(teamName, teamStats) {
			var radar = echarts.init(document.getElementById('radar'));
			var option = {
			    tooltip: {},
			    radar: {
			        indicator: [
			           { name: '助攻率', max: 100},
			           { name: '篮板率', max: 100},
			           { name: '失误率', max: 100},
			           { name: '进攻成功率', max: 100},
			           { name: '有效命中率', max: 100},
			           { name: '真实命中率', max: 100}
			        ],
			      	radius: 160
			        //center: [50%, 50%],
	            	
			    },
			    series: [{
			        type: 'radar',
			        data : [{
		                value : [teamStats.assistPct, teamStats.reboundPct, teamStats.turnoverPct, teamStats.offensivePct, teamStats.efgPct, teamStats.tsPct],
		                name : teamName,
		                areaStyle: {
		                    normal: {
		                        opacity: 0.7,
		                        color: new echarts.graphic.RadialGradient(0.5, 0.5, 1, [
		                            {
		                                color: '#B8D3E4',
		                                offset: 0
		                            },
		                            {
		                                color: '#72ACD1',
		                                offset: 1
		                            }
		                        ])
		                    }
		                }
			        }]
			    }]
			};
			radar.setOption(option);	
    	};
    	
    	$(function() {
    		var refreshInterval = 30000;
    		setInterval('refreshMatchStatsDetailInfo()', refreshInterval);
    		
    		refreshMatchStatsDetailInfo();
    	});
    
    </script>
</body>
</html>
