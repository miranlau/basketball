<%@ page import="com.renyi.basketball.bussness.po.User" %>
<%@ page import="com.renyi.basketball.api.util.LoginHelper" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String base = request.getContextPath();
	//当前管理员类型
	/*User user = LoginHelper.getCurrent();*/
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title></title>
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
	<link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
	<link rel="stylesheet" href="<%=base%>/resource/css/multipleTeam.css" />
</head>
<body>
	<div class="multlpleTeamBox">
		<div class="fixBox">
			<div class="base-info">
				<div class="team left"></div>
				<div class="center">
					<div class="main">
						<p class="score"></p>
						<p class="state"></p>
					</div>
				</div>
				<div class="team right"></div>
			</div>
			<div class="tab">
			<a href="javascript:void(0)" class="active" id="team">球队</a> <a
				href="javascript:void(0)" id="player">球员</a> <a
				href="javascript:void(0)" id="image-text">图文</a> <a
				href="javascript:void(0)" id="lineup">阵型</a> <a
				href="javascript:void(0)" id="time">时间</a>
		</div>
		</div>
		<div class="mainContent">
			<div class="page team">
				<div class="singleTeam">
					<div class="item">
						<span class="z"><i></i><em>%</em></span> <span class="k"><i></i><em>%</em></span>
						<div id="single-pie"></div>
					</div>
					<div class="item">
						<div class="title">进攻</div>
						<div class="main">
							<div class="left">
								<p class="attack goal">
									<em></em>进球
								</p>
								<p class="shoot">
									射门：<i></i>
								</p>
								<p class="cornerKick">
									角球：<i></i>
								</p>
							</div>
							<div class="right">
								<p class="attack  assault">
									<em></em>助攻
								</p>
								<p class="shotOn">
									射正：<i></i>
								</p>
								<p class="freeKick">
									任意球：<i></i>
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="title">防守</div>
						<div class="main">
							<div class="left">
								<p class="steals">
									抢断：<i></i>
								</p>
								<p class="saved">
									扑救：<i></i>
								</p>
							</div>
							<div class="right">
								<p class="intercept">
									拦截：<i></i>
								</p>
								<p class="defend">
									解围：<i></i>
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="title">犯规</div>
						<div class="main">
							<div class="left">
								<p class="foul">
									犯规：<i></i>
								</p>
								<p class="yellowCard">
									黄牌：<i></i>
								</p>
							</div>
							<div class="right">
								<p class="offside">
									越位：<i></i>
								</p>
								<p class="readCard">
									红牌：<i></i>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="multiTeam">
					<div class="pie">
						<span class="z"><i></i><em>%</em></span> <span class="k"><i></i><em>%</em></span>
						<div id="muli-pie"></div>
					</div>
					<div class="content">
					<%--	<div class="item shotOn">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">射正率</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>--%>
						<div class="item attatk">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">助攻</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item shoot">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">射门</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item cornerKick">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">角球</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item freeKick">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">任意球</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item steals">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">抢断</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item intercept">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">拦截</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item defend">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">解围</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item saved">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">扑救</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item foul">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">犯规</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item offside">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">越位</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item yellowCard">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">黄牌</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
						<div class="item readCard">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
							<div class="name">红牌</div>
							<div class="k_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="page player">
				<div class="player-tab">
					<div class="title mui-ellipsis active "></div>
					<div class="title mui-ellipsis"></div>
				</div>
				<div class="player-content">
					<div class="item">
						<%--<div class="title"></div>--%>
						<div class="content">
							<div class="top-head">
								<span>球员</span>
								<span>进球/助攻</span>
								<span>射正/射门</span>
								<span>抢断/拦截/解围</span>
								<span>扑救</span>
								<span>犯规</span>
								<span>上场时间</span>
							</div>
							<div class="bottom-content"></div>
						</div>
					</div>
					<div class="item">
						<%--<div class="title"></div>--%>
						<div class="content">
							<div class="top-head">
								<span>球员</span>

								<span>进球/助攻</span>
								<span>射正/射门</span>
								<span>抢断/拦截/解围</span>
								<span>扑救</span>
								<span>犯规</span>
								<span>上场时间</span>
							</div>
							<div class="bottom-content"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="page imageText">
				<%--<% if (user == null) {%>
				<div class="listBox nopadding"></div>
				<%}%>

				<% if (user != null) {%>--%>
				<div class="listBox"></div>
				<div class="from-box">
					<div class="fileUpload mui-pull-left">
						<!-- <form id="postForm" enctype ="multipart/form-data" method="post"> -->
						<!-- <input type="file" id="fileName"  accept="image/png,image/gif"  name="files" class="form-control mui-pull-left" />  -->
						<img src="<%=base%>/resource/img/icon/camera.png" id="fileName" />
						<!-- </form> -->
					</div>
					<input type="text" id="sendContent"
						   class="form-control mui-pull-left" />
					<button class="mui-pull-right" id="sendBtn">
						<img src="<%=base%>/resource/img/icon/send.png" />
					</button>
				</div>
	<%--			<%}%>--%>
			</div>
			<div class="page formation">
				<div class="bg">
					<img src="<%=base%>/resource/img/icon/formation.png" />
				</div>
				<div class="content"></div>
			</div>
			<div class="page timeBox"></div>
		</div>
	</div>
<script type="text/javascript"
		src="<%=base%>/resource/js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/ajaxfileupload.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/echarts.min.js"></script>
<script type="text/javascript">
	var matchId = '${matchId}';
	var winH = '${height}';
	//var winH=window.screen.height;

	if(winH == undefined || winH == null || winH == ""){
		winH = $(window).height()+20;
	}

	if(winH!=undefined && winH != "" && winH != null){
		$('.mainContent').css('height',winH-162);
		$('.mainContent .page').css('height',winH-162);
		$('.mainContent .imageText .listBox').css('height',winH-162);
		$('.mainContent .player-content .item .bottom-content').css('height',winH-245);
	}
	var timer = '';
</script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/common.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/percent.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/muitl-pie.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/single-pie.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/player.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/image-text.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/formation.js"></script>
<script type="text/javascript"
		src="<%=base%>/resource/js/multiple-game/time-axis.js"></script>
</body>
</html>