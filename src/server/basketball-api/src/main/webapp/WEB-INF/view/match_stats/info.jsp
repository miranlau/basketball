<%@ page import="com.renyi.basketball.bussness.po.User" %>
<%@ page import="com.renyi.basketball.api.util.LoginHelper" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String base = request.getContextPath();
	//当前管理员类型
	/*User user = LoginHelper.getCurrent();*/
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<title></title>
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
		<%--<link rel="stylesheet" href="css/live.css" />--%>
		<link rel="stylesheet" href="<%=base%>/resource/css/live.css" />
		<style>
			.headerImg{
				width: 37px!important;
				height: 37px;
				border-radius: 50%;
			}
		</style>
	</head>
	<body>
		<div class="mui-content">
			<!--顶部定位-->
		    <div class="headContent">
				<div class="baseInfo">
					<div class="team left">
						<img src="<%=base%>/resource/img/head-l.png" />
						<h4 class="mui-ellipsis"><%--成都巴萨FC--%></h4>
						<span></span>
					</div>
					<div class="center">
						<div class="main">
							<p class="score"><%--1:0--%></p>
							<p class="state"><%--比赛中--%></p>
						</div>
					</div>
					<div class="team right">
						<img src="<%=base%>/resource/img/head-r.png" />
						<h4 class="mui-ellipsis"><%--成都巴萨FC--%></h4>
					</div>
				</div>
				<div class="mainTtab">
					<a href="javascript:void(0)" class="active">球队</a>
					<a href="javascript:void(0)">球员</a>
					<a href="javascript:void(0)">图文</a>
					<a href="javascript:void(0)">阵型</a>
				</div>
			</div>
			<div class="mainContent">
				<div class="page team">
					<div class="scrollBox">
					<div class="item team-top">
						<%--<span>球队</span>
						<span>第一节</span>
						<span>第二节</span>
						<span>第三节</span>
						<span>第四节</span>--%>
					</div>
					<div class="item team-info home">
					<%--	<span class="mui-ellipsis">新奥尔良大黄蜂</span>
						<span>50</span>
						<span>35</span>
						<span>7</span>
						<span>9</span>--%>
					</div>
					<div class="item team-info visiting">
						<%--<span class="mui-ellipsis">波士顿凯尔特人队</span>
						<span>50</span>
						<span>35</span>
						<span>7</span>
						<span>9</span>--%>
					</div>
						</div>
					<div class="content">
						<div class="item shotOn">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">投篮</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item attatk">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0%</div>
							</div>
							<div class="name">投篮命中率</div>
							<div class="k_info">
								<div class="num">0%</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item shoot">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">助攻</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item penalty">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">篮板</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item steals">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">罚球</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item muff">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">抢断</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item snowed">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">失误</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
						<div class="item foul">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">盖帽</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>

						<div class="item illegality">
							<div class="z_info">
								<div class="progrossBox">
									<div class="con"></div>
								</div>
								<div class="num">0</div>
							</div>
							<div class="name">犯规</div>
							<div class="k_info">
								<div class="num">0</div>
								<div class="progrossBox">
									<div class="con"></div>
								</div>
							</div>
						</div>
					</div>					
				</div>
				<div class="page player">
					<div class="player-tab">
						<div class="title mui-ellipsis active ">
							<img src="<%=base%>/resource/img/player.png"><%--成都巴萨FC--%>
						</div>
						<div class="title mui-ellipsis">
							<img src="<%=base%>/resource/img/player.png"><%--成都巴萨FC--%>
						</div>
					</div>
					<div class="player-content">
						<div class="item">
							<div class="content">
								<div class="top-head">
									<span>球员</span>
									<%--<span>上场时间</span>--%>
									<span>投篮</span>
									<span>投篮命中率</span>
									<span>助攻</span>
									<span>总篮板(前场)</span>
									<span>罚球</span>
									<span>抢断</span>
									<span>失误</span>
									<span>盖帽</span>
									<span>犯规</span>
								</div>
								<div class="bottom-content">
									<%--<div class="list">
										<span>10 梅西</span>
										<span>45’56’’</span>
										<span>35</span>
										<span>7</span>
										<span>9</span>
										<span>11-20</span>
										<span>5-7</span>
										<span>8-11</span>
										<span>7</span>
										<span>4</span>
										<span>4</span>
										<span>0</span>
										<span>4</span>
										<span>+31</span>
									</div>--%>

								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="top-head">
									<span>球员</span>
									<%--<span>上场时间</span>--%>
									<span>投篮</span>
									<span>投篮命中率</span>
									<span>助攻</span>
									<span>总篮板(前场)</span>
									<span>罚球</span>
									<span>抢断</span>
									<span>失误</span>
									<span>盖帽</span>
									<span>犯规</span>
								</div>
								<div class="bottom-content">
								<%--	<div class="list">
										<span>10 梅西</span>
										<span>45’56’’</span>
										<span>35</span>
										<span>7</span>
										<span>9</span>
										<span>11-20</span>
										<span>5-7</span>
										<span>8-11</span>
										<span>7</span>
										<span>4</span>
										<span>4</span>
										<span>0</span>
										<span>4</span>
										<span>+31</span>
									</div>--%>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page imageText">
					<div class="listBox">
						<%--<div class="item">
							<img src="<%=base%>/resource/img/head.png" />
							<div class="con">
								<p>
									<span>江玉馨</span>
									<span>00'00"</span>
								</p>
								<div class="description">
									啊啊啊啊啊啊，图一这个球员假摔功夫到位！，不愧是影帝级人物！
								</div>
							</div>
						</div>--%>
					</div>

					<div class="from-box">
						<div class="fileUpload mui-pull-left">
							<input type="file" id="name1" type="file" accept="image/png,image/gif" name="file" class="form-control" />
							<img src="<%=base%>/resource/img/camera.png" id="fileName"/>
						</div>
						<input type="text" id="sendContent" class="form-control" />
						<button id="sendBtn"><img src="<%=base%>/resource/img/send.png" /></button>
					</div>
				</div>
				<div class="page formation">
					<div class="bg"><img src="<%=base%>/resource/img/formation-bg.png" /></div>
					<div class="content">
						<div class="item item1">
							<img src="<%=base%>/resource/img/formation-head.png" />
							<span class="span_z">11</span>
						</div>
						<div class="item item2">
							<img src="<%=base%>/resource/img/formation-head.png" />
							<span class="span_k">11</span>
						</div>
					</div>					
				</div>
			</div> 
		</div>
		<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
		<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
		<script type="text/javascript" src="<%=base%>/resource/js/game/common.js" ></script>
		<%--<script type="text/javascript" src="<%=base%>/resource/js/single-game/image-text.js"></script>--%>
		<script type="text/javascript" src="<%=base%>/resource/js/multiple-game/image-text.js"></script>
		<script type="text/javascript" src="<%=base%>/resource/js/multiple-game/formation.js"></script>
		<script type="text/javascript" src="<%=base%>/resource/js/multiple-game/player.js"></script>
	<%--	<script type="text/javascript" src="<%=base%>/resource/js/game/player.js" ></script>
		<script type="text/javascript" src="<%=base%>/resource/js/game/formation.js" ></script>--%>
		<script type="text/javascript" src="<%=base%>/resource/js/multiple-game/percent.js"></script>

		<%--<script type="text/javascript" src="<%=base%>/resource/js/multiple-game/muitl-pie.js"></script>--%>
	    <script type="text/javascript">
			var matchId = '${matchId}';
			/*sessionStorage.setItem("index",0);*/
			var timer = '';
/*//			var matchId = '${matchId}';
//			var winH = '${height}';
//			if(winH == "" || winH == undefined || winH == null){
		//		winH=window.screen.height;
			var winH=$(window).height();
//			}

			if(winH!=undefined && winH != "" && winH != null){
				$('.mainContent .player-content .item .bottom-content').css('height',winH-204);
			}
			var timer = '';*/
			/*var winH=$(window).height();

			if(winH!=undefined && winH != "" && winH != null){
				$('.mainContent .player-content .item .bottom-content').css('height',winH-204);
			}*/
			var timer = '';
			var winH=$(window).height();
			if(winH!=undefined && winH != "" && winH != null){
				$('.mainContent .player-content .item .bottom-content').css('height',winH-204);
			}

			$(function(){
				var width = $(window).width();
				$('.scrollBox .item').css("min-width",width);
			})

		</script>
	</body>
</html>
