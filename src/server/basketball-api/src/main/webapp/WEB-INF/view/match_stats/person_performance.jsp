<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title></title>
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent" />
<link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
</head>
<body>
	<div class="mui-content">

	</div>
	<script type="text/javascript"
		src="<%=base%>/resource/js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js"></script>
	<script type="text/javascript"
		src="<%=base%>/resource/js/echarts.min.js"></script>
	<script type="text/javascript">
		var matchId = '${matchId}';
		var timer = '';
	</script>
</body>
</html>