<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String base = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title></title>
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<link rel="stylesheet" type="text/css" href="<%=base%>/resourcecss/mui.min.css"  />
	<link rel="stylesheet" type="text/css" href="<%=base%>/resource/css/common.css" />
	<link rel="stylesheet" type="text/css" href="<%=base%>/resource/css/per_performance.css" />
	<style>
		.star img{
			width: 10px;
		}
		.half-star{
			width: 16px;
			display: inline-block;
			position: relative;
		}
		.half-star div{
			line-height: 0;
		}
		.half-star div.left{
			position: absolute;
			left: 0;
			top: 0;
			z-index: 5;
			overflow: hidden;
		}
	</style>
</head>
<body>

<div class="mui-content">
	<div class="base_info">
		<div class="head">
			<img src="${performance.user.avatar}" />
		</div>
		<div class="match-info">
			<h4>
				<span class="name">${performance.user.name}</span>
		    		<%--	<span class="star">
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-white.png" />
		    			</span>--%>
				<c:forEach var="item" begin="0" end="4" varStatus="status">
					<div class="half-star">

						<div class="left" style="width:
						<c:choose>
						<c:when test="${(performance.user.stars - status.index * 20) >= 20 }">
								100%
						</c:when>
						<c:when test="${(performance.user.stars - status.index * 20) <= 0 }">
								0%
						</c:when>
						<c:otherwise>
							<fmt:formatNumber value="${(performance.user.stars - status.index * 20) / 20}" type="percent" />
						</c:otherwise>
						</c:choose>
								!important;"><img src="<%=base%>/resource/img/icon/star-yellow.png" /></div>
						<div class="right"><img src="<%=base%>/resource/img/icon/star-white.png" /></div>
					</div>
				</c:forEach>
			</h4>
			<p>
				<span class="game">参赛：<em>${performance.user.matches}</em></span>
				<span class="goal">进球：<em>${performance.user.scores}</em></span>
				<span class="grade">等级：<em>${performance.user.level}</em></span>
			</p>
		</div>
	</div>
	<div class="mainContent">
		<div class="item">
			<div class="shoot_ratio pie">
				<div class="l txt">
					<span>${performance.uShootNum}</span>
					<p>射门</p>
				</div>
				<div class="r txt">

					<span><fmt:formatNumber value="${performance.shoot}" pattern="0.00"/>%</span>
					<p>球队射门</p>
				</div>
				<div id="shoot_ratio">

				</div>
			</div>
		</div>
		<div class="item">
			<div class="left percent pie">
				<div class="l txt">
					<span>${performance.uAssistsNum}</span>
					<p>助攻</p>
				</div>
				<div class="r txt">
					<span><fmt:formatNumber value="${performance.assists}" pattern="0.00"/>%</span>
					<p>球队助攻</p>
				</div>
				<div id="assists">

				</div>
			</div>
			<div class="right percent pie">
				<div class="l txt">
					<span>${performance.uSavesNum}</span>
					<p>扑救</p>
				</div>
				<div class="r txt">
					<span><fmt:formatNumber value="${performance.saves}" pattern="0.00"/>%</span>
					<p>球队扑救</p>
				</div>
				<div id="fight">

				</div>
			</div>
		</div>
		<div class="item">
			<div class="left percent pie">
				<div class="l txt">
					<span>${performance.uStealsNum}</span>
					<p>抢断</p>
				</div>
				<div class="r txt">
					<span><fmt:formatNumber value="${performance.steals}" pattern="0.00"/>%</span>
					<p>球队抢断</p>
				</div>
				<div id="steals">

				</div>
			</div>
			<div class="right percent pie">
				<div class="l txt">
					<span>${performance.uFoulNum}</span>
					<p>犯规</p>
				</div>
				<div class="r txt">
					<span><fmt:formatNumber value="${performance.foul}" pattern="0.00"/>%</span>
					<p>球队犯规</p>
				</div>
				<div id="foul">

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/echarts.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/personal/shoot_ratio.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/personal/assist.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/personal/fight.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/personal/steal.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/personal/foul.js" ></script>
</body>
<script>
	setSteals(${performance.steals});
	setFight(${performance.saves});
	setAssist(${performance.assists});
	setFoul(${performance.foul});
	setShoot(${performance.shoot});
</script>
</html>