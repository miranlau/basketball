<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-球员-资料</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/app.css">
</head>

<body>
    <section class="hero fix-top">
        <div class="flex-container padding-top">
            <div class="flex">
                <img class="person" src="${player.avatar}" alt="${player.name}">
            </div>
            <div class="flex flex-container column">
                <h3>${player.name}  ${player.uniformNumber}号</h3>
            </div>
            <div class="flex">
                <img class="logo" src="${player.teamLogo}" alt="${player.teamName}">
                <p>${player.teamName}</p>
            </div>
        </div>
        <div class="flex-container text-center padding-vertical">
            <div class="flex">
                <div class="btn">直播</div>
            </div>
            <div class="flex">
                <div class="btn active">资料</div>
            </div>
            <div class="flex">
                <div class="btn">技术统计</div>
            </div>
            <div class="flex">
                <div class="btn">进阶数据</div>
            </div>
        </div>
    </section>
    <section class="padding" style="margin-top: 12rem">
        <h3 class="orange padding-bottom">${player.name}</h3>
        <p>${player.profile}</p>
        <h3 class="orange padding-vertical">运动生涯</h3>
        <p class="padding-bottom">${player.career}</p>
    </section>
</body>

</html>
