<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/player-detail.css" />
    <style>
        .star img{
            width: 10px;
        }
        .half-star{
            width: 16px;
            display: inline-block;
            position: relative;
        }
        .half-star div{
            line-height: 0;
        }
        .half-star div.left{
            position: absolute;
            left: 0;
            top: 0;
            z-index: 5;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div class="mui-content">
    <div class="base_info">
        <div class="head">
            <img src="<%=base%>/resource/img/head.png" />
        </div>
        <div class="match-info">
            <h4>
                <span class="name">${player.name}</span>
		    			<%--<span class="star">
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-yellow.png" />
		    				<img src="<%=base%>/resource/img/icon/star-white.png" />
		    			</span>--%>
                <c:forEach var="item" begin="0" end="4" varStatus="status">
                    <div class="half-star">

                        <div class="left" style="width:
                        <c:choose>
                        <c:when test="${(player.stars - status.index * 20) >= 20 }">
                                100%
                        </c:when>
                        <c:when test="${(player.stars - status.index * 20) <= 0 }">
                                0%
                        </c:when>
                        <c:otherwise>
                            <fmt:formatNumber value="${(player.stars - status.index * 20) / 20}" type="percent" />
                        </c:otherwise>
                        </c:choose>
                                !important;"><img src="<%=base%>/resource/img/icon/star-yellow.png" /></div>
                        <div class="right"><img src="<%=base%>/resource/img/icon/star-white.png" /></div>
                    </div>
                </c:forEach>
            </h4>
            <p>
                <span class="game">参赛：<em>${player.matches}</em></span>
                <span class="goal">进球：<em>${player.two_three}</em></span>
                <span class="grade">等级：<em>${player.level}</em></span>
            </p>
        </div>

    </div>
    <div class="commonDiv">
        <div id="radar"></div>
    </div>
    <div class="commonDiv my-message">
        <ul>
            <li><strong>${player.myFocus}</strong>我关注的</li>
            <li><strong>${player.myFans}</strong>我的粉丝</li>
            <li><strong>${player.myPublish}</strong>我的发表</li>
        </ul>
    </div>
    <div id="mainContent">
        <div class="commonDiv">
            <div class="time">我的球队</div>
            <ul>
                <c:forEach items="${player.teams}" var="item" varStatus="status">
                    <li>
                        <a href="../team/share?teamId=${item.id}" class="mui-navigate-right">
                            <img class="mui-media-object mui-pull-left" src="${item.logo}">
                            <div class="mui-media-body">
                                <h4>${item.name}<span>${item.level}级球队</span></h4>
                                <div class="star">
                                    <c:forEach var="item1" begin="0" end="4" varStatus="status1">
                                        <div class="half-star">
                                            <div class="left" style="width:
                                            <c:choose>
                                            <c:when test="${(item.stars - status1.index * 20) >= 20 }">
                                                    100%
                                            </c:when>
                                            <c:when test="${(item.stars - status1.index * 20) <= 0 }">
                                                    0%
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:formatNumber value="${(item.stars - status1.index * 20) / 20}" type="percent" />
                                            </c:otherwise>
                                            </c:choose>
                                                    !important;"><img src="<%=base%>/resource/img/icon/star-yellow.png" /></div>
                                            <div class="right"><img src="<%=base%>/resource/img/icon/star-white.png" /></div>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="record">
                                    <img src="<%=base%>/resource/img/icon/record.png"/>战绩：${item.win}胜${item.deuce}平${item.lose}负
                                    <span><img src="<%=base%>/resource/img/icon/age.png"/>平均年龄：${item.age}</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    </c:forEach>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/echarts.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/radar.js" ></script>
<script>
   // console.log(${player.teams})
    var radar = [${player.radarDto.credit},${player.radarDto.jingong},${player.radarDto.sudu},${player.radarDto.common},${player.radarDto.fangshou},${player.radarDto.tineng}];
    console.log(radar)
    initMyCahrt(radar.toString());
</script>
</body>
</html>
