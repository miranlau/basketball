<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>职业版-数据-球员-直播开始</title>
    <link rel="stylesheet" href="css/app.css">
</head>

<body>
    <section class="hero fix-top">
        <div class="flex-container padding-top">
            <div class="flex">
                <img class="person" src="css/img/person.png" alt="孟达">
            </div>
            <div class="flex flex-container column">
                <h3>孟达 12号</h3>
            </div>
            <div class="flex">
                <img class="logo" src="css/img/logo1.png" alt="四川品胜">
                <p>四川品胜</p>
            </div>
        </div>
        <div class="flex-container text-center padding-vertical">
            <div class="flex">
                <div class="btn active">直播</div>
            </div>
            <div class="flex">
                <div class="btn">资料</div>
            </div>
            <div class="flex">
                <div class="btn">技术统计</div>
            </div>
            <div class="flex">
                <div class="btn">进阶数据</div>
            </div>
        </div>
    </section>
    <section class="padding" style="margin-top: 12rem">
        <p class="orange">孟达 本场比赛 场均投篮热区图 <span class="float-right white margin-right">？</span></p>
        <div class="topview margin-vertical">
            <img style="width: 100%" src="css/img/area.png" alt="">
            <div class="lt">2-8<br>25%</div>
            <div class="lb">5-8<br>62.5%</div>
            <div class="lc">7-8<br>87.5%</div>
            <div class="ct">2-10 20%</div>
            <div class="cm">2-10 20%</div>
            <div class="cb">2-10 20%</div>
            <div class="rt">2-8<br>25%</div>
            <div class="rb">5-8<br>62.5%</div>
            <div class="rc">7-8<br>87.5%</div>
        </div>
        <div class="flex-container">
            <div class="flex">
                <table class="highlight">
                    <thead>
                        <tr>
                            <th>运球投篮</th>
                            <th>接球投篮</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>12-15</td>
                            <td>7-11</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="flex">
                <table class="highlight" style="margin: 0 0 0 auto">
                    <thead>
                        <tr>
                            <th>效率值</th>
                            <th>正负值</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>20.87</td>
                            <td>+8</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <table class="highlight margin-top" style="width: 100%">
            <thead>
                <tr>
                    <th colspan="4">进攻侧重</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>左侧</td>
                    <td>正面</td>
                    <td>篮下</td>
                    <td>右侧</td>
                </tr>
                <tr>
                    <td>25.00%</td>
                    <td>10.00%</td>
                    <td>35.00%</td>
                    <td>30.00%</td>
                </tr>
            </tbody>
        </table>
        <p class="orange padding-vertical">进攻占比</p>
        <!--TODO: chart here-->
        <p class="orange padding-vertical">其它数据 <span class="float-right white margin-right">球员对比（同位置）</span></p>
        <!--TODO: chart here-->
    </section>
</body>

</html>
