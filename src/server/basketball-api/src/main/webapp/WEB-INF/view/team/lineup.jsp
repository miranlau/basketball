<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-数据展示-球队-阵容</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/app.css">
    <script src="<%=contextPath%>/resource/laydate/laydate.js"></script>
</head>
<body>
	<section class="hero padding-top fix-top">
        <img class="logo" src="${team.logo}" alt="${team.name}">
        <h3 class="padding-bottom">${team.name}</h3>
        <p>战绩：排名第${teamRank.rank}（${teamRank.win}胜-${teamRank.loss}负）</p>
        <p>主场：${team.venueName}</p>
        <div class="flex-container text-center padding-vertical">
            <div class="flex">
                <a class="btn active">阵容</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team/schedule?teamId=${team.id}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">赛程</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team_stats/season/basic?teamId=${team.id}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">技术统计</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team_stats/season/advanced?teamId=${team.id}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">进阶数据</a>
            </div>
        </div>
    </section>
   	<section class="padding" style="margin-top: 14rem">
    <table class="team margin-bottom" style="width: 100%">
        <thead>
            <tr>
                <th></th>
                <th>姓名</th>
                <th>球衣号</th>
                <th>位置</th>
                <th>身高</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="player" varStatus="status" items="${playerList}">
				<tr>
	                <td><img class="avatar" src="${player.avatar}" alt="${player.name}"></td>
	                <td>${player.name}</td>
	                <td>${player.uniformNumber}</td>
	                <td>${player.positionName}</td>
	                <td>${player.height}</td>
            	</tr>        	
            	<p>
        	</c:forEach>
        </tbody>
    </table>
    </section>
</body>
</html>