<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-数据-相关</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <script type="text/javascript" src="<%=contextPath%>/resource/js/jquery-2.1.0.js"></script>
</head>

<body>

    <!-- 比赛基本信息 -->
	<jsp:include page="../match_stats/basic.jsp" flush="true" />
	
    <section class="flex-container text-center border-bottom padding">
        <div class="flex">
            <a class="btn" href="<%=contextPath%>/ctl/match/live?matchId=${matchId}">直播</a>
        </div>
        <div class="flex">
            <a class="btn" href="<%=contextPath%>/ctl/match_stats/detail?matchId=${matchId}">数据</a>
        </div>
        <div class="flex">
            <a class="btn active">相关</a>
        </div>
    </section>
    
    <section class="padding">
        <p class="orange">最近历史交战记录</p>
        <c:forEach var="match" varStatus="status" items="${matchHistoryList}">
        	<p><span><fmt:formatDate pattern="yyyy年MM月dd日" value="${match.matchTime}"/></span> ${match.homeName} (${match.homeScore}) - ${match.visitingName} (${match.visitingScore})</p>
        </c:forEach>

        <p class="orange padding-top">${homeName} 近三场赛况</p>
        <c:forEach var="match" varStatus="status" items="${homePassMatchList}">
        	<p><span><fmt:formatDate pattern="yyyy年MM月dd日" value="${match.matchTime}"/></span> ${match.homeName} (${match.homeScore}) - ${match.visitingName} (${match.visitingScore})</p>
        </c:forEach>

        <p class="orange padding-top">${homeName} 未来赛程</p>
        <c:forEach var="match" varStatus="status" items="${homeFutureMatchList}">
        	<p><span><fmt:formatDate pattern="yyyy年MM月dd日" value="${match.matchTime}"/></span> ${match.homeName} - ${match.visitingName}</p>
        </c:forEach>

		<p class="orange padding-top">${visitingName} 近三场赛况</p>
        <c:forEach var="match" varStatus="status" items="${visitingPassMatchList}">
        	<p><span><fmt:formatDate pattern="yyyy年MM月dd日" value="${match.matchTime}"/></span> ${match.homeName} (${match.homeScore}) - ${match.visitingName} (${match.visitingScore})</p>
        </c:forEach>
        
        <p class="orange padding-top">${visitingName} 未来赛程</p>
        <c:forEach var="match" varStatus="status" items="${visitingFutureMatchList}">
        	<p><span><fmt:formatDate pattern="yyyy年MM月dd日" value="${match.matchTime}"/></span> ${match.homeName} - ${match.visitingName}</p>
        </c:forEach>
    </section>
</body>

</html>
