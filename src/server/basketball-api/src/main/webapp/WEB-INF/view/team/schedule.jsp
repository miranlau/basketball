<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-数据展示-球队-赛程</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/app.css">
    <script src="<%=contextPath%>/resource/laydate/laydate.js"></script>
</head>

<body>
    <section class="hero padding-top fix-top">
        <img class="logo" src="${team.logo}" alt="${team.name}">
        <h3 class="padding-bottom">${team.name}</h3>
        <p>战绩：排名第${teamRank.rank}（${teamRank.win}胜-${teamRank.loss}负）</p>
        <p>主场：${team.venueName}</p>
        <div class="flex-container text-center padding-vertical">
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team/lineup?teamId=${team.id}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">阵容</a>
            </div>
            <div class="flex">
                <div class="btn active">赛程</div>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team_stats/season/basic?teamId=${team.id}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">技术统计</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team_stats/season/advanced?teamId=${team.id}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">进阶数据</a>
            </div>
        </div>
    </section>
    <div style="margin-top: 14rem">
        <c:forEach var="match" varStatus="status" items="${matchList}">
        	<div class="flex-container schedule padding">
            	<div class="flex">
                	<img class="logo" src="${match.homeLogo}" alt="${match.homeName}">
                	<p>${match.homeName}</p>
            	</div>
            	<div class="flex flex-container column">
                	<p class="time"><fmt:formatDate value="${match.matchTime}" pattern="YYYY年MM月dd日 " /></p>
                	<h3>${match.homeScore} - ${match.visitingScore}</h3>
                	<p class="green">（${match.status}）</p>
           		</div>
            	<div class="flex"> 
                	<img class="logo" src="${match.visitingLogo}" alt="${match.visitingName}">
                	<p>${match.visitingName}</p>
            	</div>
        	</div>
        </c:forEach>
    </div>
</body>

</html>
