
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="stylesheet" href="<%=base%>/resource/css/mui.min.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/common.css" />
    <link rel="stylesheet" href="<%=base%>/resource/css/team-detail.css" />
    <style>
        .star img{
            width: 10px;
        }
        .half-star{
            width: 16px;
            display: inline-block;
            position: relative;
        }
        .half-star div{
            line-height: 0;
        }
        .half-star div.left{
            position: absolute;
            left: 0;
            top: 0;
            z-index: 5;
            overflow: hidden;
        }
    </style>
</head>
<body>
<div class="mui-content">
    <div class="base-info">
        <div class="content">
            <div class="imgBox mui-pull-left">
                <img src="${team.logo}" />
            </div>
            <div class="description">
                <span><img src="<%=base%>/resource/img/icon/record.png" />战绩：${team.win}胜${team.deuce}平${team.lose}负 </span>
                <span><img src="<%=base%>/resource/img/icon/star-white.png" />球队等级：${team.level} </span>
                <span><img src="<%=base%>/resource/img/icon/age.png" />平均年龄：${team.age} </span>
            </div>
            <%--<a href="javascript:void(0)" class="mui-btn mui-pull-right">--%>
                <%--<span class="mui-icon"></span><em>关注</em><i>已关注</i>--%>
            <%--</a>--%>
        </div>
        <div class="name">
            ${team.name}
                <c:forEach var="item" begin="0" end="4" varStatus="status">
                    <div class="half-star">

                        <div class="left" style="width:
                        <c:choose>
                        <c:when test="${(team.stars - status.index * 20) >= 20 }">
                                100%
                        </c:when>
                        <c:when test="${(team.stars - status.index * 20) <= 0 }">
                                0%
                        </c:when>
                        <c:otherwise>
                            <fmt:formatNumber value="${(team.stars - status.index * 20) / 20}" type="percent" />
                        </c:otherwise>
                        </c:choose>
                                !important;"><img src="<%=base%>/resource/img/icon/star-yellow.png" /></div>
                        <div class="right"><img src="<%=base%>/resource/img/icon/star-white.png" /></div>
                    </div>
                </c:forEach>
        </div>
    </div>
    <div class="commonDiv">
        <div class="time">战斗力指数</div>
        <div id="radar"></div>
    </div>
    <div class="commonDiv">
        <div class="time">球队球员</div>
        <div class="team-member">
            <c:forEach items="${team.players}" var="item" varStatus="status">
            <div class="item">
                <c:choose>
                    <c:when test="${item.id == team.leader}">
                <span class="captain"><img src="<%=base%>/resource/img/icon/leader.png" /></span>
                <div class="headImg"><img src="${item.avatar}" /></div>
                <span class="number">${item.number}</span>
                        <p class="mui-ellipsis">${item.name}</p>
                    </c:when>
                    <c:when test="${item.id != team.leader}">

                        <div class="headImg"><img src="${item.avatar}" /></div>
                        <span class="number">${item.number}</span>
                        <p class="mui-ellipsis">${item.name}</p>
                    </c:when>
                </c:choose>
            </div>
           </c:forEach>
        </div>
    </div>
    <div id="mainContent">
        <div class="commonDiv">
            <c:forEach items="${matches}" var="item" varStatus="status">
                <c:choose>
                <c:when test="${status.first || matches[status.index-1].date != item.date}">
                <ul>
                    <div class="time"><fmt:formatDate value="${item.date}" type="date" dateStyle="full"/></div>
                </c:when>
                </c:choose>
                <li>
                    <div class="team left">
                        <div class="imgbox">
                            <img src="${item.home.logo}" />
                            <c:choose>
                            <c:when test="${item.homescore > item.visitingscore}">
                                <span><img src="<%=base%>/resource/img/icon/winner.png" /></span>
                            </c:when>
                            </c:choose>

                        </div>
                        <h4 class="mui-ellipsis-2">${item.home.name}</h4>
                    </div>
                    <div class="content">
                        <c:choose>
                            <c:when test="${item.status == 100}">
                                <div class="score">${item.homescore}:${item.visitingscore}</div>
                            </c:when>
                            <c:when test="${item.status == 21}">
                                <div class="score" style="color: red;font-size: 20px">即将开赛</div>
                            </c:when>
                            <c:when test="${item.status == 20}">
                                <div class="score" style="color: green;font-size: 20px">比赛中</div>
                            </c:when>
                            <c:when test="${item.status == 10}">
                                <div class="score" style="color: orangered"><fmt:formatDate value="${item.date}" pattern="HH:mm"/></div>
                            </c:when>
                        </c:choose>

                        <div class="name mui-ellipsis-2">${item.leagueName}</div>
                        <div class="address mui-ellipsis-2">${item.addr}</div>
                    </div>
                    <div class="team right">
                        <div class="imgbox">
                            <img src="${item.visiting.logo}" />
                            <c:choose>
                                <c:when test="${item.homescore < item.visitingscore}">
                                    <span><img src="<%=base%>/resource/img/icon/winner.png" /></span>
                                </c:when>
                            </c:choose>
                        </div>
                        <h4 class="mui-ellipsis-2">${item.visiting.name}</h4>
                    </div>
                </li>
                    <c:choose>
                    <c:when test="${status.end || matches[status.index+1].date != item.date}">
                        </ul>
                    </c:when>
                    </c:choose>
            </c:forEach>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=base%>/resource/js/jquery-2.1.0.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/mui.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/team-detail.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/echarts.min.js" ></script>
<script type="text/javascript" src="<%=base%>/resource/js/radar.js" ></script>
<script>
    var radar = [${team.radarDto.credit},${team.radarDto.jingong},${team.radarDto.sudu},${team.radarDto.common},${team.radarDto.fangshou},${team.radarDto.tineng}];
    console.log(radar)
    initMyCahrt(radar.toString());
</script>
</body>
</html>
