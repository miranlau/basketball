<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-球队-进阶数据</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/app.css">
    <script src="<%=contextPath%>/resource/laydate/laydate.js"></script>
    <script type="text/javascript" src="<%=contextPath%>/resource/js/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="<%=contextPath%>/resource/js/echarts-3.5.4.js"></script>
</head>

<body>
    <section class="hero padding-top fix-top">
        <img class="logo" src="${teamSeasonStats.teamLogo}" alt="${teamSeasonStats.teamName}">
        <h3 class="padding-bottom">${teamSeasonStats.teamName}</h3>
        <p>战绩：排名第${teamRank.rank} （${teamSeasonStats.win}胜-${teamSeasonStats.loss}负）</p>
        <p>主场：${teamSeasonStats.teamCourt}</p>
        <div class="flex-container text-center padding-vertical">
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team/lineup?teamId=${teamId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">阵容</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team/schedule?teamId=${teamId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">赛程</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team_stats/season/basic?teamId=${teamId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">技术统计</a>
            </div>
            <div class="flex">
                <div class="btn active">进阶数据</div>
            </div>
        </div>
    </section>
    <section class="padding" style="margin-top: 14rem">
        <h3 class="orange">${teamSeasonStats.teamName}投篮热区图 <span class="float-right white margin-right">？</span></h3>
        <div class="topview margin-vertical">
            <img style="width: 100%" src="<%=contextPath%>/resource/css/img/area.png" alt="">
            <div class="lt">${teamSeasonStats.threeLeftWingShot}-${teamSeasonStats.threeLeftWingAttempt}<br>${teamSeasonStats.threeLeftWingPct}%</div>
            <div class="lb">${teamSeasonStats.threeLeftside45Shot}-${teamSeasonStats.threeLeftside45Attempt}<br>${teamSeasonStats.threeLeftside45Pct}%</div>
            <div class="lc">${teamSeasonStats.twoLeftsideShot}-${teamSeasonStats.twoLeftsideAttempt}<br>${teamSeasonStats.twoLeftsidePct}%</div>
            <div class="ct">${teamSeasonStats.twoPaintedShot}-${teamSeasonStats.twoPaintedAttempt} ${teamSeasonStats.twoPaintedPct}%</div>
            <div class="cm">${teamSeasonStats.twoFrontShot}-${teamSeasonStats.twoFrontAttempt} ${teamSeasonStats.twoFrontPct}%</div>
            <div class="cb">${teamSeasonStats.threeRootArcShot}-${teamSeasonStats.threeRootArcAttempt} ${teamSeasonStats.threeRootArcPct}%</div>
            <div class="rt">${teamSeasonStats.threeRightWingShot}-${teamSeasonStats.threeRightWingAttempt}<br>${teamSeasonStats.threeRightWingPct}%</div>
            <div class="rb">${teamSeasonStats.threeRightside45Shot}-${teamSeasonStats.threeRightside45Attempt}<br>${teamSeasonStats.threeRightside45Pct}%</div>
            <div class="rc">${teamSeasonStats.twoRightsideShot}-${teamSeasonStats.twoRightsideAttempt}<br>${teamSeasonStats.twoRightsidePct}%</div>
        </div>
        <table class="highlight">
            <thead>
                <tr>
                    <th>运球投篮</th>
                    <th>接球投篮</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${teamSeasonStats.dribblingShot}-${teamSeasonStats.dribblingAttempt}</td>
                    <td>${teamSeasonStats.standingShot}-${teamSeasonStats.standingAttempt}</td>
                </tr>
            </tbody>
        </table>
    </section>
    <section class="padding">
        <h3 class="orange">${teamSeasonStats.teamName}进阶数据</h3>
        <p class="padding-top">节奏：${teamSeasonStats.offensiveAttempt} 联盟排名：7</p>
        <p class="gray">进攻效率：${teamSeasonStats.offensiveEff} 联盟排名：5</p>
        <p class="gray padding-bottom">防守效率：${teamSeasonStats.defensiveEff} 联盟排名：9</p>
        <table class="highlight" style="margin: 0 auto">
            <thead>
                <tr>
                    <th colspan="4">进攻侧重</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>左侧</td>
                    <td>正面</td>
                    <td>篮下</td>
                    <td>右侧</td>
                </tr>
                <tr>
                    <td>${teamSeasonStats.leftOffensivePct}%</td>
                    <td>${teamSeasonStats.frontOffensivePct}%</td>
                    <td>${teamSeasonStats.paintedOffensivePct}%</td>
                    <td>${teamSeasonStats.rightOffensivePct}%</td>
                </tr>
            </tbody>
        </table>
    </section>
    <section class="padding">
    	<div id="radar" style="margin: 0 auto;width:auto;height:400px"></div>
    </section>

<script type="text/javascript">

$(function() {
	loadRadar();
});

var loadRadar = function() {
	var radar = echarts.init(document.getElementById('radar'));
	var option = {
	    tooltip: {},
	    radar: {
	        indicator: [
	           { name: '助攻率', max: 100},
	           { name: '篮板率', max: 100},
	           { name: '失误率', max: 100},
	           { name: '进攻成功率', max: 100},
	           { name: '有效命中率', max: 100},
	           { name: '真实命中率', max: 100}
	        ],
	      	radius: 160
	        //center: [50%, 50%],
        	
	    },
	    series: [{
	        type: 'radar',
	        data : [{
                value : [${teamSeasonStats.assistPct}, ${teamSeasonStats.reboundPct}, ${teamSeasonStats.turnoverPct}, ${teamSeasonStats.offensivePct}, ${teamSeasonStats.efgPct}, ${teamSeasonStats.tsPct}],
                name : '${teamSeasonStats.teamName}',
                areaStyle: {
                    normal: {
                        opacity: 0.7,
                        color: new echarts.graphic.RadialGradient(0.5, 0.5, 1, [
                            {
                                color: '#B8D3E4',
                                offset: 0
                            },
                            {
                                color: '#72ACD1',
                                offset: 1
                            }
                        ])
                    }
                }
	        }]
	    }]
	};
	radar.setOption(option);	
};


</script>
</body>
</html>
