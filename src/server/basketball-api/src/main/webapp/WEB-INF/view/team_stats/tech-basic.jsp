<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>全网篮球-球队-技术统计</title>
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/core.css">
    <link rel="stylesheet" href="<%=contextPath%>/resource/css/app.css">
    <script src="<%=contextPath%>/resource/laydate/laydate.js"></script>
</head>

<body>
    <section class="hero padding-top fix-top">
        <img class="logo" src="css/img/logo1.png" alt="${teamSeasonStats.teamName}">
        <h3 class="padding-bottom">${teamSeasonStats.teamName}</h3>
        <p>战绩：排名第${teamRank.rank} （${teamSeasonStats.win}胜-${teamSeasonStats.loss}负）</p>
        <p>主场：${teamSeasonStats.teamCourt}</p> 
        <div class="flex-container text-center padding-vertical">
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team/lineup?teamId=${teamId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">阵容</a>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team/schedule?teamId=${teamId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">赛程</a>
            </div>
            <div class="flex">
                <div class="btn active">技术统计</div>
            </div>
            <div class="flex">
                <a class="btn" href="<%=contextPath%>/ctl/team_stats/season/advanced?teamId=${teamId}&leagueId=${leagueId}&leagueStageId=${leagueStageId}">进阶数据</a>
            </div>
        </div>
    </section>
    <h3 class="text-center padding-vertical" style="margin-top: 15rem">球队数据</h3>
    <section class="scroll-container">
        <div class="scrollable">
            <table class="highlight2 nowrap">
                <thead>
                    <tr>
                        <th>时间</th>
                        <th>对手</th>
                        <th>比分</th>
                        <th>胜负</th>
                        <th>两分球</th>
                        <th>两分球命中率</th>
                        <th>三分球</th>
                        <th>三分球命中率</th>
                        <th>罚球</th>
                        <th>后场篮板</th>
                        <th>前场篮板</th>
                        <th>助攻</th>
                        <th>抢断</th>
                        <th>失误</th>
                        <th>盖帽</th>
                        <th>犯规</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>赛季平均</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>${teamSeasonStats.twoPointsShot}-${teamSeasonStats.twoPointsAttempt}</td>
                        <td>${teamSeasonStats.twoPointsAttempt}%</td>
                        <td>${teamSeasonStats.threePointsShot}-${teamSeasonStats.threePointsAttempt}</td>
                        <td>${teamSeasonStats.threePointsAttempt}%</td>
                        <td>1${teamSeasonStats.freeThrowShot}</td>
                        <td>2${teamSeasonStats.defensiveRebound}</td>
                        <td>3${teamSeasonStats.offensiveRebound}</td>
                        <td>3${teamSeasonStats.assist}</td>
                        <td>5${teamSeasonStats.steal}</td>
                        <td>4${teamSeasonStats.turnover}</td>
                        <td>1${teamSeasonStats.blockShot}</td>
                        <td>2${teamSeasonStats.foul}</td>
                    </tr>
                    <c:forEach var="matchStats" varStatus="status" items="${teamMatchStats}">
                    	<tr>
                        	<td><fmt:formatDate value="${matchStats.matchTime}" pattern="YYYY年MM月dd日 " /></td>
                        	<td>${matchStats.opposingTeamName}</td>
                        	<td>
                        		<c:choose>
                        			<c:when test="${matchStats.isHomeCourt}">
                        				${matchStats.homeScore}-${matchStats.visitingScore}
                        			</c:when>
                        			<c:otherwise>
                        				${matchStats.visitingScore}-${matchStats.homeScore}
                        			</c:otherwise>
                        		</c:choose>
                        	</td>
                        	<td>
                        		<c:choose>
                        			<c:when test="${matchStats.isWin}">胜</c:when>
                        			<c:otherwise>负</c:otherwise>
                        		</c:choose>
                        		
                        	</td>
                        	<td>${matchStats.twoPointsShot}-${matchStats.twoPointsAttempt}</td>
                        	<td>${matchStats.twoPointsPct}%</td>
                        	<td>${matchStats.threePointsShot}-${matchStats.threePointsAttempt}</td>
                        	<td>${matchStats.threePointsPct}%</td>
                        	<td>${matchStats.freeThrowShot}</td>
                        	<td>${matchStats.defensiveRebound}</td>
                        	<td>${matchStats.offensiveRebound}</td>
                        	<td>${matchStats.assist}</td>
                        	<td>${matchStats.steal}</td>
                        	<td>${matchStats.turnover}</td>
                        	<td>${matchStats.blockShot}</td>
                        	<td>${matchStats.foul}</td>
                    	</tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </section>
    <h3 class="text-center padding-vertical">球员场均数据</h3>
    <section class="scroll-container margin-bottom">
        <div class="scrollable">
            <table class="highlight2 nowrap">
                <thead>
                    <tr>
                        <th>球员</th>
                        <th>号码</th>
                        <th>得分</th>
                        <th>两分球</th>
                        <th>两分球命中率</th>
                        <th>三分球</th>
                        <th>三分球命中率</th>
                        <th>罚球</th>
                        <th>后场篮板</th>
                        <th>前场篮板</th>
                        <th>助攻</th>
                        <th>抢断</th>
                        <th>失误</th>
                        <th>盖帽</th>
                        <th>犯规</th>
                    </tr>
                </thead>
                <tbody>
                	<c:forEach var="playerStats" varStatus="status" items="${playerStatsList}">
                    	<tr>
                        	<td>${playerStats.playerName}</td>
                        	<td>${playerStats.uniformNumber}</td>
                        	<td>${playerStats.score}</td>
                        	<td>${playerStats.twoPointsShot}-${playerStats.twoPointsAttempt}</td>
                        	<td>${playerStats.twoPointsPct}%</td>
                        	<td>${playerStats.threePointsShot}-${playerStats.threePointsAttempt}</td>
                        	<td>${playerStats.threePointsPct}%</td>
                        	<td>1${playerStats.freeThrowShot}</td>
                        	<td>2${playerStats.defensiveRebound}</td>
                        	<td>3${playerStats.offensiveRebound}</td>
                       	 	<td>3${playerStats.assist}</td>
                        	<td>5${playerStats.steal}</td>
                        	<td>4${playerStats.turnover}</td>
                        	<td>1${playerStats.blockShot}</td>
                        	<td>2${playerStats.foul}</td>
                    	</tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </section>
</body>

</html>
