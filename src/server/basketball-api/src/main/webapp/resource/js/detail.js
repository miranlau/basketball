//参与报名
$('.partake').on('tap',function(){
	swal({
		title: "",
		text: "您已成功报名，请等待比赛开始！",
		confirmButtonColor: '#4c98c0',
		confirmButtonText: '好！',
	},function(){
		$('.editBtn').addClass('isPartake');
	});
})

//取消报名
$('.giveup').on('tap',function(){
	swal({
		title: "",
		text: "确定要取消报名吗？机会难得哟！",
		confirmButtonColor: '#4c98c0',
		confirmButtonText: '我要取消！',
		showCancelButton: true,
		closeOnConfirm: false 
	},function(){
		swal({
			title: "",
			text: "您已成功取消报名",
			confirmButtonColor: '#4c98c0',
			confirmButtonText: '好！',
		},function(){
			$('.editBtn').removeClass('isPartake');
		})
	});
})

//关注
$('.title a.mui-btn').on('tap',function(){
	if($(this).hasClass('no-follow')){
		$(this).removeClass('no-follow');
	}else{
		$('.title a.mui-btn').addClass('no-follow');
	}
})
