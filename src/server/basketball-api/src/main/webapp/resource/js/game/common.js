

var homeLogo='',homeName='',homeId='';
var visitingLogo='',visitingName='',visitingId='';
var url=window.document.location.href;
var pName=window.document.location.pathname;
var localhostPaht=url.substring(0,url.indexOf(pName))+'/basketball-api/';
var urlPattern = /^((http)?:\/\/)[^\s]+/;
$(function() {
	getCommonInfo();
	var index = sessionStorage.getItem('index');
	if (index != "" && index != undefined && index != null) {
		$('.mainTtab a').removeClass('active');
		$('.mainTtab a').eq(index).addClass('active');
		$('.mainContent .page').hide();
		$('.mainContent .page').eq(index).show();
		if (index == 0) {
			getHomeVisiting();
		} else if (index == 1) {
			getPlayerList();
		} else if (index == 2) {
			getDate();
		} else if (index == 3) {
			getPosition();
		}
	}else{
		$('.mainTtab a').removeClass('active');
		$('.mainTtab a').eq(0).addClass('active');
		$('.mainContent .page').hide();
		$('.mainContent .page').eq(0).show();
	}
});
$('.mainTtab a').on('tap', function() {
	if(!$(this).hasClass("active")){
		$('.mainTtab a').removeClass('active');
		$(this).addClass('active');
		var index = $(this).index();
		$('.mainContent .page').hide();
		$('.mainContent .page').eq(index).show();
		sessionStorage.setItem('index', index);
		clearInterval(timer);
		if (index == 0) {
			getHomeVisiting();
		} else if (index == 1) {
			getPlayerList();
		} else if (index == 2) {
			getDate();
		} else if (index == 3) {
			getPosition();
		}
	}
})

function getCommonInfo(){
	$.ajax({
		type:"get",
		url: "/basketball-api/ctl/game/getMatchBasicInfo",
		data:{matchId:matchId},
		async:true,
		dataType:"json",
		timeout:5000,
		success:function(json){
			console.log(json);
			if(json.flag==0 && json.code==0){
				var obj=json.data;
				sessionStorage.setItem("statusCount",obj.status);
				if(obj.home.homeLogo == undefined || obj.home.homeLogo==null){
					homeLogo = localhostPaht+"resource/img/default_team.png";
				}else{
					if(urlPattern.test(obj.home.homeLogo)){
						homeLogo=obj.home.homeLogo;
					}else{
						homeLogo=localhostPaht+obj.home.homeLogo;
					}

				}
				homeName=obj.home.homeName;
				sessionStorage.setItem("homeName",homeName);
				homeId=obj.home.homeId;
				sessionStorage.setItem("homeId",homeId);
				var singleStatus=obj.matchStatus;
				var matchStatus=obj.status;
				var singgleStr='';
				if(singleStatus=="比赛中" || singleStatus == "比赛结束"){
					if(obj.isPenalty==1) {
						singgleStr = '<p class="score">' + obj.homeTotalScore + "(" + obj.homePenalty + ") : " + obj.visitingScore + '(' + obj.visitingPenalty + ')</p>';
					}else{
						'<p class="score">' + obj.homeTotalScore + ": " + obj.visitingScore + '</p>';
					}
				}else{
					singgleStr='<p class="time">'+obj.time+'</p>'+
							'<p class="state">'+obj.matchStatus+'</p>';
				}
				$('.baseInfo .left').html('<img src="'+homeLogo+'" />'+
						'<h4 class="mui-ellipsis">'+homeName+'</h4>');
				if(obj.visiting.visitingName== undefined || obj.visiting.visitingName==null ){
					$('.baseInfo .right').html('<div class="match-type">单队赛</div>');

					if (obj.matchStatus == "比赛中" || obj.matchStatus == "比赛结束") {
						if(obj.isPenalty==1) {
							$('.baseInfo .main').html('<p class="score">' + obj.homeScore + "(" + obj.homePenalty + ") : " + obj.visitingScore + '(' + obj.visitingPenalty + ')</p>' +
									'<p class="state">' + obj.matchStatus + '</p>');
						}else{
							$('.baseInfo .main').html('<p class="score">' + obj.homeScore + " : " + obj.visitingScore + '</p>' +
									'<p class="state">' + obj.matchStatus + '</p>');
						}
					} else {
						$('.baseInfo .main').html('<p class="time">'+obj.time+'</p>'+
								'<p class="state">'+obj.matchStatus+'</p>');
					}
				}else{
					if(obj.visiting.visitingLogo == undefined || obj.visiting.visitingLogo==null){
						visitingLogo = localhostPaht+"resource/img/default_team.png";
					}else{
						if(urlPattern.test(obj.visiting.visitingLogo)){
							visitingLogo=obj.visiting.visitingLogo;
						}else{
							visitingLogo=localhostPaht+obj.visiting.visitingLogo;
						}
					}
					visitingName=obj.visiting.visitingName;
					sessionStorage.setItem("visitingName",visitingName);
					visitingId=obj.visiting.visitingId;
					sessionStorage.setItem("visitingId",visitingId);
					$('.baseInfo .right').html('<img src="'+visitingLogo+'" />'+
							'<h4 class="mui-ellipsis">'+visitingName+'</h4>');
					if(obj.isPenalty==1) {
						$('.baseInfo .main').html('<p class="score">' + obj.homeScore + "(" + obj.homePenalty + ") : " + obj.visitingScore + '(' + obj.visitingPenalty + ')</p>' +
								'<p class="state">' + obj.matchStatus + '</p>');
					}else{
						$('.baseInfo .main').html('<p class="score">' + obj.homeScore + " : " + obj.visitingScore + '</p>' +
								'<p class="state">' + obj.matchStatus + '</p>');
					}

					$('.baseInfo .right').on('tap',function(){
						var send=[];
						send.push(matchId);
						send.push(visitingId);
						window.JSBridge.ActionGoToTeams(send);
					})

				}

				var topHtml = '<span>球队</span><span>第1节</span>';
				var homeHtml ='<span>'+homeName+'</span><span>0</span>';
				if(sessionStorage.getItem("visitingName")!=null){
					var visitingHtml = '<span>'+sessionStorage.getItem("visitingName")+'</span><span>0</span>';
					$('.visiting').html(visitingHtml);
				}


				console.log(topHtml);
				console.log(homeHtml);


				$('.team-top').html(topHtml);
				$('.home').html(homeHtml);
				var spanLen=0;
				$('.scrollBox .team-top span').each(function(){
					spanLen+=$(this).width();
				})
				$('.scrollBox .item').css('width',spanLen);



				$('.baseInfo .left').on('tap',function(){
					var send=[];
					send.push(matchId);
					send.push(homeId);
					window.JSBridge.ActionGoToTeams(send);
				})
				$('.baseInfo .main').on('tap',function(){
					var send=[];
					send.push(matchStatus);
					send.push(matchId);
					window.JSBridge.ActionGoToMatchs(send);
				})
				getHomeVisiting();

			}
		},
		error:function(err){
			console.log(err);
		}
	});
}
//下拉刷新
$('.headContent').on('swipedown',function(){
	location.reload();
})
