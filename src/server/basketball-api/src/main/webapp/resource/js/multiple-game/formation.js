function getPosition(){
	var $avgW='',$avgH='';
	var $formation=$('.formation');
	var $bg=$formation.find('.bg');
	var $imgW=$bg.find('img').width();
	var $imgH=$bg.find('img').height();
	$formation.find('.content').css({"width":$imgW,"height":$imgH,"position":"relative"});
/*
	console.log($imgW);
	console.log($imgH);*/
	$avgW=$imgW/39;
	$avgH=$imgH/37/2;
	/*console.log($avgW)
	console.log($avgH)*/
		$.ajax({
			type:"get",
			url: "/basketball-api/ctl/game/getMatchLineUp",
			data:{matchId:matchId},
			async:true,
			dataType:"json",
			timeout:5000, 
			success:function(json){
				if(json.flag==0 && json.code==0) {
				/*	console.log(json);*/
					var formation_zd = '';
					var home_formation = json.data.home;
					for(var i=0;i<home_formation.length;i++){
						var homeHead = '';
						if(home_formation[i].up){
						if (home_formation[i].avatar != undefined || home_formation[i].avatar != "" || home_formation[i].avatar == null){
							if (urlPattern.test(home_formation[i].avatar)) {
								homeHead = home_formation[i].avatar;
							} else {
								homeHead = localhostPaht + home_formation[i].avatar;
							}
						}else {
							homeHead = localhostPaht+"resource/img/default_team.png";
						}
						var left=$avgW*(39-home_formation[i].y)+'px';
						var top=($avgH*home_formation[i].x)+'px';
							/*console.log(left+" "+top)*/
						formation_zd+='<div class="item" style="left:'+left+';top:'+top+'"><img src="'+homeHead+'" class="headerImg" /><span>'+home_formation[i].number+'</span></div>';
						//console.log(formation_zd);
					}
					}
					if(json.data.visiting!=undefined || json.data.visiting!=null) {
						var visiting_formation = json.data.visiting;

							for (var i = 0; i < visiting_formation.length; i++) {
								var visitingHead = '';
								if (visiting_formation[i].up) {
								if (visiting_formation[i].avatar != undefined || visiting_formation[i].avatar != "" || visiting_formation[i].avatar == null) {
									if (urlPattern.test(visiting_formation[i].avatar)) {
										visitingHead = visiting_formation[i].avatar;
									} else {
										visitingHead = localhostPaht + visiting_formation[i].avatar;
									}
								} else {
									visitingHead = localhostPaht + "resource/img/default_team.png";
								}
								var left_k = $avgW * (37 - visiting_formation[i].y) + 'px';
								var top_k = ($avgH * visiting_formation[i].x)+($imgH/2) + 'px';
								formation_zd += '<div class="item" style="left:' + left_k + ';top:' + top_k + '"><img src="' + visitingHead + '" class="headerImg" /><span>' + visiting_formation[i].number + '</span></div>';
							}
						}
					}
					$('.formation .content').html(formation_zd);
				}
			},
			error:function(err){
				console.log(err);
			}
		})
}
