var aid='';
function getDate(){
	initInfo();
	timer=setInterval(initInfo,20000);
	$('.imageText .listBox').on('dragend',function(){
		var listBoxH=$('.imageText .listBox').height();
		var scrollH=$('.imageText .listBox').offset().top-126;
		console.log("scrollh:"+scrollH)
		console.log("listBoxH:"+listBoxH)
		console.log("winH:"+winH)
		if(( listBoxH+scrollH ) <= (winH-100)  && scrollH !=0){
			clearInterval(timer);
			aid=$('.imageText .listBox .item:last-child').attr("data-id");
			getImagetextList(aid,"up");
		}
		if(scrollH==0){
			clearInterval(timer);
			aid=$('.imageText .listBox .item:first-child').attr("data-id");
			getImagetextList(aid,"down");	
		}
	})
}

function initInfo(){
	getImagetextList();
}
var pattern = /^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+/;
function getImagetextList(aid,state){
	$.ajax({
		type:"get",
		url: "/basketball-api/ctl/game/liveInfo",
		data:{matchId:matchId,id:aid,status:state},
		async:true,
		dataType:"json",
		timeout:5000, 
		success:function(json){
			console.log(json)
			if(json.flag==0&& json.code==0){
				var str='';
				for(var i = json.data.length - 1;i >= 0;i --){
					var headUrl='',nameStr='',content='',actionString='',userContent='',orange= '';;
					if(json.data[i].actionString != undefined && json.data[i].actionString != "" && json.data[i].actionString != null){
						actionString=JSON.parse(json.data[i].actionString);
						if(actionString.photo !=undefined){
							if(urlPattern.test(actionString.photo)){
								headUrl=actionString.photo;
							}else{
								headUrl=localhostPaht+actionString.photo;
							}
						}else{
							headUrl=localhostPaht+'resource/img/default_header.png';
						}
						if(actionString.name != "" && actionString.name != undefined){
							nameStr='<span>'+actionString.name+'</span>';
						}else{
							nameStr='';
						}					
						if(actionString.message != "" && actionString.message != undefined){
							if(json.data[i].status == 77  ){
								userContent='<div class="description">'+actionString.message+'</div>';
							}else if(json.data[i].status == 88){
								userContent='<div class="imgBox" ><span><img src="'+actionString.message+'" /></span></div>';
							}							
						}else{
							userContent='';
						}					
						
						
					}
					var imageTextTime=getTime2(json.data[i].dynamicTime, json.data[i].createTime);
					
					if(json.data[i].content !=undefined && pattern.test(json.data[i].content)){
						content='<div class="imgBox" ><span><img src="'+json.data[i].content+'" /></span></div>';
					}else{
						content='<div class="description">'+json.data[i].content+'</div>';
					}

					if(json.data[i].status == 33 ||  json.data[i].status == 23){
						orange='orange';
					}
					if(json.data[i].status == 77 ||  json.data[i].status == 88 ){
						str+='<div class="item" data-id="'+json.data[i].id+'">'+
							'<img src="'+headUrl+'" />'+
							'<div class="con">'+
								'<p>'+nameStr+
									'<span>'+imageTextTime+'</span>'+
								'</p>'+userContent+
							'</div>'+
						'</div>';
					}else{
						str+='<div class="item" data-id="'+json.data[i].id+'">'+
							'<img src="'+localhostPaht+'resource/img/live.png" />'+
							'<div class="con">'+
								'<p>'+
									'<span>'+imageTextTime+'</span>'+
								'</p>'+
								'<div class="description '+orange+'">'+json.data[i].content+'</div>'+
							'</div>'+
						'</div>';
					}
				}
				$('.imageText .listBox').html(str);
				
				$('.imageText .listBox .imgBox img').on('tap',function(){
					var imgUrl=$(this).attr("src");
					window.JSBridge.PhotoZoom(imgUrl);
				})
			}
		},
		error:function(err){
			console.log(err);
		}
	});
}

Date.prototype.Format = function (fmt) { //author: meizz
	var o = {
		"M+": this.getMonth() + 1, //月份
		"d+": this.getDate(), //日
		"h+": this.getHours(), //小时
		"m+": this.getMinutes(), //分
		"s+": this.getSeconds(), //秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
};

//时间转换
function getTime2(dynamicTime, createTime){

	// 超过8000秒显示日期
	if (dynamicTime > 8000) {
		var date = new Date(createTime);
		// return date.Format("yyyy-MM-dd hh:mm:ss");
		return new Date(createTime * 1).toLocaleString().replace(/年|月/g, "-").replace(/日/g, " ");
	}

	var minu=parseInt(dynamicTime/60);
	var second=dynamicTime-(minu*60);
	if(minu<10){
		minu="0"+minu;
	}else{
		minu=minu;
	}
	if(second<10){
		second="0"+second;
	}else{
		second=second;
	}
	
	var time=(minu+"'"+second+'"');
	return time;
}



$('#sendBtn').on('tap',function(){
	var sendArr = [];
	var content = $("#sendContent").val();
	if (content == "") {
		mui.toast("请输入评论内容");
	}else{
		console.log(matchId + ":"+ content)
		sendArr.push(matchId);
		sendArr.push(content);
		window.JSBridge.SendComment(sendArr);
	}
})



$( '#fileName').on('tap',function(){
	window.JSBridge.SendPhotoAction(matchId);
//	var sendArr = [];
//	$.ajaxFileUpload({  
//		url : "/football-api/ctl/resource/upload",  
//        secureuri:false,  
//        fileElementId:'fileName',//file标签的id  
//        dataType: 'text',//返回数据的类型  
//        success: function (data, status) {  
//        	obj=data.substring( data.indexOf('{') , data.indexOf('}')+1 );
//        	var json= JSON.parse(obj);
//        	if(json.flag==0 && json.code==0){
//        		sendArr.push(matchId);
//        		sendArr.push(json.data[0]);
//        		window.JSBridge.SendPhotoAction(sendArr);
//        		
//        	}
//        },  
//        error: function (data, status, e) {  
//        	obj=data.substring( data.indexOf('{') , data.indexOf('}')+1 );
//        	var json= JSON.parse(obj);
//        	console.log(json);
//        }  
//    });  
})