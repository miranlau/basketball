function getPlayerList(){
	$.ajax({
		type:"get",
		url: "/basketball-api/ctl/game/getPlayerInfo",
		data:{matchId:matchId},
		async:true,
		dataType:"json",
		timeout:5000,
		success:function(json){
			console.log(json);
			if(json.flag==0 && json.code==0){
				var objHome=json.data.home.homePlayer;
				$('.player .player-tab .title:first-child').html('<img src="'+homeLogo+'" />'+homeName);
				var player_zd='',player_kd='';
				if(objHome.length==0){
					player_zd+='<div class="list"><span class="nodata">暂时没有球员</span></div> ';
				}else{
					for(var i=0;i<objHome.length;i++){
						var yellowCard='';
						var redCard='';
						/*onclick="clicked('+objHome[i].playerId+','+homeId+','+matchId+');"*/
						player_zd+='<div class="list">'+
							'<span><strong>'+objHome[i].number+'</strong><a class="gotoPerformance"  href="#">'+objHome[i].playerName+'</a> '+yellowCard+' '+redCard+'</span>'+

						/*	'<span>'+objHome[i].playeringTime+'</span>'+*/
							'<span>'+objHome[i].shoot_all+'</span>'+
							'<span>'+(objHome[i].average)*100+'%</span>'+
							'<span>'+objHome[i].assists+'</span>'+
							'<span>'+objHome[i].backboard+'</span>'+
							'<span>'+objHome[i].penalty_all+'</span>'+
								'<span>'+objHome[i].steals+'</span>'+
								'<span>'+objHome[i].muff+'</span>'+
								'<span>'+objHome[i].blockShot+'</span>'+
								'<span>'+objHome[i].illegality+'</span>'+
							'</div>';
					}
				}

				$('.player .player-content .item').eq(0).find('.bottom-content').html(player_zd);
				if(json.data.visiting.visitingPlayer!=undefined && json.data.visiting.visitingPlayer!=null  && json.data.visiting.visitingPlayer!=""){
					$('.player .player-tab .title:last-child').css('display','inline-block');
					var objVisiting=json.data.visiting.visitingPlayer;
					$('.player .player-tab .title:last-child').html('<img src="'+visitingLogo+'" />'+visitingName);
					if(objVisiting.length==0){
						player_kd+='<div class="list"><span class="nodata">暂时没有球员</span></div> ';
					}else{
						for(var i=0;i<objVisiting.length;i++){
							var yellowCard='';
							var redCard='';

							player_kd+='<div class="list">'+
								'<span>'+objVisiting[i].number+' <a class="gotoPerformance"  href="#">'+objVisiting[i].playerName+'</a> '+yellowCard+' '+redCard+'</span>'+
								/*	'<span>'+objVisiting[i].playeringTime+'</span>'+*/
									'<span>'+objVisiting[i].shoot_all+'</span>'+
									'<span>'+(objVisiting[i].average)*100+'%</span>'+
									'<span>'+objVisiting[i].assists+'</span>'+
									'<span>'+objVisiting[i].backboard+'</span>'+
									'<span>'+objVisiting[i].penalty_all+'</span>'+
									'<span>'+objVisiting[i].steals+'</span>'+
									'<span>'+objVisiting[i].muff+'</span>'+
									'<span>'+objVisiting[i].blockShot+'</span>'+
									'<span>'+objVisiting[i].illegality+'</span>'+
								'</div>';
						}
					}
					$('.player .player-content .item').eq(1).find('.bottom-content').html(player_kd);
				}
			}
		},
		error:function(err){
			console.log(err);
		}
	});
}

$(".gotoPerformance").on("tap",function() {



})
function clicked(playerId,teamId,matchId) {
	var url = "player/playerPerformance?userId="+playerId+'&teamId='+teamId+'&matchId='+matchId;
	var performce = [url,"本场表现"];
	window.JSBridge.commonJSBridge(performce);//同用桥方法，客户端接收url，直接访问
}

$(function () {
	$('.player-content .item').hide();
	$('.player-content .item').eq(0).show();
})

$('.player-tab .title').on('tap',function () {
	var index=$(this).index();
	$('.player-tab .title').removeClass('active');
	$(this).addClass('active');
	$('.player-content .item').hide();
	$('.player-content .item').eq(index).show();
})