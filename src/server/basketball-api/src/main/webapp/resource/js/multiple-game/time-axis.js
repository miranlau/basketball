function getTimeAxis(){
	$.ajax({
		type:"get",
		url: "/basketball-api/ctl/game/timeDetails",
		data:{matchId:matchId},
		async:true,
		dataType:"json",
		timeout:5000,
		success:function(json){
			if(json.flag==0 && json.code==0){
				var isStart = false;
				var str='';
				var obj=json.data;
				for(var i=0;i<obj.length;i++){
					var iconHtml='',redCardHtml='',yellowCard='',headHtml='',timmer='',itemClass='';
					iconHtml=getStateIcon(obj[i].status);

					if(obj[i].tid==homeId){
						itemClass='item_z';
					}
					if(obj[i].tid==visitingId){
						itemClass='item_k';
					}
					if(obj[i].status==22){
						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							if (obj[i].action.playerB != undefined && obj[i].action.playerB != null && obj[i].action.playerB != "") {
								headHtml = '<span class="up">' + obj[i].action.playerB.name + '</span> <span class="down">' + obj[i].action.playerA.name + '</span>';
							} else {
								headHtml = '<span class="down" style="margin-top: 8px">' + obj[i].action.playerA.name + '</span>';
							}
						}else{
							headHtml = '<span class="up" style="margin-top: 8px">' + obj[i].action.playerB.name + '</span>';
						}

					}else if(obj[i].status==23){
						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							headHtml = obj[i].action.playerA.name;
						}
					}else if(obj[i].status==33) {
						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							if (obj[i].action.playerB != undefined && obj[i].action.playerB != null && obj[i].action.playerB != "" && obj[i].action.playerB.id != -1) {

								headHtml = '<span class="goalin">' + obj[i].action.playerA.name + '</span><span class="attack">' + obj[i].action.playerB.name + '</span>';
							} else {
								var timerHead = '';
								if (urlPattern.test(obj[i].action.playerA.avatar)) {
									timerHead = obj[i].action.playerA.avatar;
								} else {
									timerHead = localhostPaht + obj[i].action.playerA.avatar;
								}
								headHtml = '<div class="goal"><img src="' + localhostPaht + '/resource/img/icon/ball.png" /></div>' + obj[i].action.playerA.name;
							}
						}
					}else if(obj[i].status==38) {
						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							if (obj[i].action.playerB != undefined && obj[i].action.playerB != null && obj[i].action.playerB != "") {
								headHtml = '<span class="goalin">' + obj[i].action.playerA.name + '</span><span class="attack">' + obj[i].action.playerB.name + '</span>';
							} else {
								var timerHead = '';
								if (urlPattern.test(obj[i].action.playerA.avatar)) {
									timerHead = obj[i].action.playerA.avatar;
								} else {
									timerHead = localhostPaht + obj[i].action.playerA.avatar;
								}
								headHtml = '<div class="goal"><img src="' + localhostPaht + '/resource/img/icon/ball.png" /></div>' + obj[i].action.playerA.name;
							}
						}
					}else if(obj[i].status==39) {
						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							if (obj[i].action.playerB != undefined && obj[i].action.playerB != null && obj[i].action.playerB != "") {
								headHtml = '<span class="goalin">' + obj[i].action.playerA.name + '</span><span class="attack">' + obj[i].action.playerB.name + '</span>';
							} else {
								var timerHead = '';
								if (urlPattern.test(obj[i].action.playerA.avatar)) {
									timerHead = obj[i].action.playerA.avatar;
								} else {
									timerHead = localhostPaht + obj[i].action.playerA.avatar;
								}
								headHtml = '<div class="goal"><img src="' + localhostPaht + '/resource/img/icon/ball_not_in.png" /></div>' + obj[i].action.playerA.name;
							}
						}
					}else if(obj[i].status==42){
						redCardHtml='<div class="red-card">';

						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							redCardHtml += obj[i].action.playerA.name;
						}

						if(obj[i].redSum==undefined && obj[i].redSum == null && obj[i].redSum == ""  ){
							redCardHtml+='<img src="'+localhostPaht+'resource/img/icon/red-card.png" /></div>';
						}else{
							redCardHtml+='<img src="'+localhostPaht+'resource/img/icon/red-card.png" /></div>';
						}
					}else if(obj[i].status==43){

						yellowCard='<div class="yellow-card">';
						if (obj[i].action.playerA != undefined && obj[i].action.playerA != null && obj[i].action.playerA != "") {
							yellowCard += obj[i].action.playerA.name;
						}
						if(obj[i].yellowSum==undefined && obj[i].yellowSum == null && obj[i].yellowSum == ""  ){
							yellowCard+='<img src="'+localhostPaht+'resource/img/icon/yellow-card.png" /></div>';
						}else{
							yellowCard+='<img src="'+localhostPaht+'resource/img/icon/yellow-card.png" /></div>';
						}
					}


					// 已经开始就只显示一个哨子
					if (obj[i].status == 11 && isStart) {
						continue;
					}

					if(obj[i].status==11 || obj[i].status==100){
						isStart = true;
						str+='<div class="item">'+
							'<div class="icon">'+
								'<img src="'+localhostPaht+'resource/img/icon/start.png" />'+
							'</div>'+
						'</div>';
						if(obj[i].status==100){
							str+='<div class="title">比赛结束</div>';
						}
					}else{
						var time = getTime(parseInt(obj[i].dynamicTime));
						str+='<div class="item '+itemClass+'">'+
									'<div class="icon">'+iconHtml+'</div>'+
							'<div class="info">'+redCardHtml+' '+yellowCard+' '+headHtml+
							'</div>'+
							'<div class="time">'+time+'</div>'+
						'</div>';

					}
				}
				$('.timeBox').html('<div class="title">比赛开始</div><div class="showtime">'+str);

			}
		},
		error:function(err){
			console.log(err);
		}
	})
}
//红区状态
function getStateIcon(status){
	switch (status){
		case 11:
			return '<img src="'+localhostPaht+'resource/img/icon/start.png" />';
			break;
		case 22:
			return '<img src="'+localhostPaht+'resource/img/icon/substitution.png" />';
			break;
		case 23:
			return '<img src="'+localhostPaht+'resource/img/icon/own_goal.png" />';
			break;
		case 38:
		case 39:
		case 70:
			return '<img src="'+localhostPaht+'resource/img/icon/leader.png" />';
			break;
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:
		case 41:
		case 44:
		case 55:
		case 56:
		case 57:
		case 58:
		case 60:
		case 61:
		case 62:
		case 77:
		case 88:
			return '<img src="'+localhostPaht+'resource/img/icon/goal.png" />';
			break;
		case 42: case 43:
			return '<img src="'+localhostPaht+'resource/img/icon/red-yellow.png" />';
			break;
		case 100: case 110:
			return '<img src="'+localhostPaht+'resource/img/icon/start.png" />';
			break;
		default:
			break;
	}
}

////时间转换
function getTime(dynamicTime){
	var minu=parseInt(dynamicTime/60);
	var second=dynamicTime-(minu*60);
	if(minu<10){
		minu="0"+minu;
	}else{
		minu=minu;
	}
	if(second<10){
		second="0"+second;
	}else{
		second=second;
	}

	var time=(minu+"'"+second+'"');
	return time;
}