// 基于准备好的dom，初始化echarts实例
function setShoot(param) {


    var shoot_ratio = echarts.init(document.getElementById('shoot_ratio'));
// 指定图表的配置项和数据
    option = {
        color: ["#F09B22", "#4D9AC1"],
        textStyle: {
            color: '#fff',
            fontSize: 12
        },
        series: [
            {
                name: '射门比',
                type: 'pie',
                radius: ['50%', '65%'],
                avoidLabelOverlap: false,
                hoverAnimation: false,
                clockwise: false,
                label: {
                    normal: {
                        show: true,
                        position: 'center',
                        formatter: '{a}'
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: [
                    {value: param, name: param + '%'},
                    {value: 100 - param, name: 100 - param + '%'}
                ]
            }
        ]
    };


// 使用刚指定的配置项和数据显示图表。
    shoot_ratio.setOption(option);
}