// 基于准备好的dom，初始化echarts实例
function initMyCahrt(radarStr) {
    if (radarStr != null && radarStr != "") {
        var radar = radarStr.split(",");
    }
    var myChart = echarts.init(document.getElementById('radar'));
// 指定图表的配置项和数据
option = {
	textStyle:{
		color:'#ffffff',
		fontWeight:'normal',
		fontSize :13,
		fontFamily:'-apple-system,helvetica Neue,arial,PingFang SC,Hiragino Sans GB,HelveticaNeue-Light, Helvetica Neue Light,Droid Sans,STHeiti,microsoft yahei,Microsoft JhengHei,Source Han Sans SC,SimSun,sans-serif'
	},
	color:[
		'#79cbf6'
	],
    radar: {
    	zlevel:-1,
        indicator: [
            { text: '信誉:'+radar[0], max: 100 },
            { text: '进攻:'+radar[1], max: 100 },
            { text: '速度:'+radar[2], max: 100 },
            { text: '评价:'+radar[3], max: 100 },
            { text: '防守:'+radar[4], max: 100 },
            { text: '体能:'+radar[5], max: 100 }
        ],
        center: ['50%', '50%'],
        splitLine:false,
        splitArea:{
        	areaStyle:{
        		color :['#363538','#323133']
        	}
        },
         axisLine: {
            lineStyle: {
                color: 'rgba(255, 255, 255, 0.1'
            }
        }
    },
    series: [{
        type: 'radar',
        itemStyle:{
        	normal:{
        		borderColor :"#79cbf6"
        	}
       	},
        data: [
            {
                value: [radar[0],radar[1],radar[2],radar[3],radar[4],radar[5]],
                areaStyle:{
		        	normal:{
		        		color:"#79cbf6",
		        		opacity :'0.5'
		        	}
		        }
            },
        ],
        
    }]
}

// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);

}