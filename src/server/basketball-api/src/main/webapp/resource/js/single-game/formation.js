function getPosition(){
	var $avgW='',$avgH='';
	var $formation=$('.formation');
	var $bg=$formation.find('.bg');
	var $imgW=$bg.find('img').width();
	var $imgH=$bg.find('img').height();
	$formation.find('.content').css({"width":$imgW,"height":$imgH});
	
	$avgW=$imgW/25;
	$avgH=$imgH/50;
	
	var x1=15,y1=10;  //组队定位的点
	var x2=5,y2=5;	  //客队的定位点
	
	//主队定位方法
	$('.item1').css({
		left:$avgW*x1,
		top:$avgH*(25-y1),
	})
	
	//客队定位方法
	$('.item2').css({
		left:$avgW*x2,
		top:$avgH*(25+y2),
	})
}
