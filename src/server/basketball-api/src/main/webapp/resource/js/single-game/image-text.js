var aid='';
function getDate(){
	getImagetextList();
	timer=setInterval(getImagetextList(),20000);
	$('.imageText .listBox').on('dragend',function(){
		var bodyH=$('body').height();
		var winH=$(window).height();
		var scrollH=$('.imageText .listBox').scrollTop();
		if((bodyH-winH)<scrollH){
			clearInterval(timer);
			aid=$('.imageText .listBox .item:last-child').attr("data-id");
			getImagetextList(aid,"up");
		}
		if(scrollH==0){
			clearInterval(timer);
			aid=$('.imageText .listBox .item:first-child').attr("data-id");
			getImagetextList(aid,"down");
		}
	})
}
var pattern = /^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+/;
function getImagetextList(aid,state){
	$.ajax({
		type:"get",
		url: "/basketball-api/ctl/game/liveInfo",
		data:{matchId:matchId,id:aid,status:state},
		async:true,
		dataType:"json",
		timeout:5000,
		success:function(json){
			console.log(json);
			if(json.flag==0&& json.code==0){
				var str='';
				for(var i = json.data.length - 1;i >= 0;i --){
					var headUrl='',nameStr='',content='',actionString='',userContent='',orange= '';;
					if(json.data[i].actionString != undefined && json.data[i].actionString != "" && json.data[i].actionString != null){
						actionString=JSON.parse(json.data[i].actionString);
						if(actionString.photo !=undefined){
							if(urlPattern.test(actionString.photo)){
								headUrl=actionString.photo;
							}else{
								headUrl=localhostPaht+actionString.photo;
							}
						}else{
							headUrl=localhostPaht+'resource/img/default_header.png';
						}
						if(actionString.name != "" && actionString.name != undefined){
							nameStr='<span>'+actionString.name+'</span>';
						}else{
							nameStr='';
						}
						if(actionString.message != "" && actionString.message != undefined){
							if(json.data[i].status == 77  ){
								userContent='<div class="description">'+actionString.message+'</div>';
							}else if(json.data[i].status == 88){
								userContent='<div class="imgBox" ><span><img src="'+actionString.message+'" /></span></div>';
							}
						}else{
							userContent='';
						}


					}
					var imageTextTime=getTime2(json.data[i].dynamicTime, json.data[i].createTime);

					if(json.data[i].content !=undefined && pattern.test(json.data[i].content)){
						content='<div class="imgBox" ><span><img src="'+json.data[i].content+'" /></span></div>';
					}else{
						content='<div class="description">'+json.data[i].content+'</div>';
					}

					if(json.data[i].status == 33 ||  json.data[i].status == 23){
						orange='orange';
					}
					if(json.data[i].status == 77 ||  json.data[i].status == 88 ){
						str+='<div class="item" data-id="'+json.data[i].id+'">'+
								'<img src="'+headUrl+'" />'+
								'<div class="con">'+
								'<p>'+nameStr+
								'<span>'+imageTextTime+'</span>'+
								'</p>'+userContent+
								'</div>'+
								'</div>';
					}else{
						str+='<div class="item" data-id="'+json.data[i].id+'">'+
								'<img src="'+localhostPaht+'resource/img/live.png" />'+
								'<div class="con">'+
								'<p>'+
								'<span>'+imageTextTime+'</span>'+
								'</p>'+
								'<div class="description '+orange+'">'+json.data[i].content+'</div>'+
								'</div>'+
								'</div>';
					}
				}
				$('.imageText .listBox').html(str);

				$('.imageText .listBox .imgBox img').on('tap',function(){
					var imgUrl=$(this).attr("src");
					window.JSBridge.PhotoZoom(imgUrl);
				})
			}
		},
		error:function(err){
			console.log(err);
		}
	});
}


/*


function getDate(){
	$(window).on('dragend',function(){
		var bodyH=$('body').height();
		var winH=$(window).height();
		var scrollH=$(window).scrollTop();
		if((bodyH-winH)<scrollH){
			var str='';
			setTimeout(function(){
				str='<div class="item">'+
					'<img src="img/head.png" />'+
					'<div class="con">'+
						'<p>'+
							'<span>江玉馨</span>'+
							'<span>00&apos;00"</span>'+
						'</p>'+
						'<div class="description">'+
							'啊啊啊啊啊啊，图一这个球员假摔功夫到位！，不愧是影帝级人物！'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="item">'+
					'<img src="img/head.png" />'+
					'<div class="con">'+
						'<p>'+
							'<span>00&apos;00"</span>'+
						'</p>'+
						'<div class="description">'+
							'啊啊啊啊啊啊，图一这个球员假摔功夫到位！，不愧是影帝级人物！'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="item">'+
					'<img src="img/head.png" />'+
					'<div class="con">'+
						'<p>'+
							'<span>江玉馨</span>'+
						'</p>'+
						'<div class="description">'+
							'啊啊啊啊啊啊，图一这个球员假摔功夫到位！，不愧是影帝级人物！'+
						'</div>'+
						'<div class="imgBox">'+
							'<span><img src="img/match-pic1.png" /></span>'+
							'<span><img src="img/match-pic2.png" /></span>'+
							'<span><img src="img/match-pic3.png" /></span>'+
						'</div>'+
					'</div>'+
				'</div>';
				$('.imageText .listBox').append(str);
			},100);
			
		}
	})

}
*/
