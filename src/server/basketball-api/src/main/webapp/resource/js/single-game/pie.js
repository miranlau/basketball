// 基于准备好的dom，初始化echarts实例
var pie = echarts.init(document.getElementById('pie'));
// 指定图表的配置项和数据
option = {
    color:["#F09B22","#4D9AC1"],
    textStyle:{
    	color:'#fff',
    	fontSize:12
    },
    series: [
        {
            name:'射正率',
            type:'pie',
            radius: ['50%', '65%'],
            avoidLabelOverlap: false,
            hoverAnimation:false,
            startAngle: 45,
            clockwise :false,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter:'{a}'
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
                {value:56, name:'56%'},
                {value:44, name:'44%'}
            ]
        }
    ]
};


// 使用刚指定的配置项和数据显示图表。
pie.setOption(option);