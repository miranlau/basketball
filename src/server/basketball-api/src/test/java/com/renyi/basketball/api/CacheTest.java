package com.renyi.basketball.api;

import org.junit.Ignore;

import com.renyi.basketball.bussness.dto.LeagueCountCache;
import com.renyi.basketball.bussness.utils.CachedManager;

/**
 * Created by Roy.xiao on 2016/6/7.
 */
@Ignore
public class CacheTest {
	public static void main(String[] args) {
		LeagueCountCache l = new LeagueCountCache();
		l.setId(1l);
		CachedManager.put("LeagueCountCache", "ceshi", l);

		LeagueCountCache l2 = (LeagueCountCache) CachedManager.get("LeagueCountCache", "ceshi");
		System.out.println(l2.getId());
	}
}
