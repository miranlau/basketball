package com.renyi.basketball.api;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.renyi.basketball.bussness.po.Product;
import com.renyi.basketball.bussness.service.ProductService;

/**
 * Created by Roy.xiao on 2016/7/11.
 */
@Ignore
public class TransactionTest {

	private Logger logger = Logger.getLogger(getClass());

	public static SqlSessionFactory sqlSessionFactory = null;

	private static boolean initialed = false;

	ApplicationContext ctx = null;

	@Before
	public void setUp() throws Exception {
		if (!initialed) {
			ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
			initialed = true;
		}
	}

	@Test
	public void test() {
		ProductService productService = ctx.getBean(ProductService.class);
		// System.err.println(AopUtils.isAopProxy(productService));
		Product product = new Product();
		try {
			productService.saveTest(product);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
