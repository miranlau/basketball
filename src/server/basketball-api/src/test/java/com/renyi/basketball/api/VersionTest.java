package com.renyi.basketball.api;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.renyi.basketball.bussness.dto.Setting;
import com.renyi.basketball.bussness.dto.Version;
import com.renyi.basketball.bussness.utils.SettingUtils;

@Ignore
public class VersionTest {

	private static Logger logger = Logger.getLogger(Version.class);

	private static boolean initialed = false;

	@Before
	public void setUp() throws Exception {
		// if (!initialed) {
		// ApplicationContext ctx = new
		// ClassPathXmlApplicationContext("classpath:applicationContext.xml",
		// "classpath:applicationContext.xml");
		// initialed = true;
		// }
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testVersion() {
		Version version = new Version();
		version.setCode(2);
		version.setDownloadURL("http://www.baidu.com");
		version.setMinCode(1);
		version.setName("ios");
		version.setVersionNumber("2.0.1");
		Setting setting = (Setting) SettingUtils.get();
		setting.setVersion(1, version);
		// Version versionAndroid = setting.getVersion(0);
		// System.err.println(versionAndroid.toString());
		SettingUtils.set(setting);
		// System.err.println("写入setting");
	}
}
