package com.renyi.basketball.bussness.common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.renyi.basketball.bussness.constants.BasketballConstants;

public final class IdAnalyzer {
	
	public static String toString(List<Long> idList) {
		if (idList == null || idList.size() == 0) {
			return null;
		}
		
		StringBuilder ids = new StringBuilder();
		for (int i = 0; i < idList.size(); i++) {
			if (i < idList.size() - 1) {
				ids.append(idList.get(i) + BasketballConstants.SEPARATOR);
			} else {
				ids.append(idList.get(i));
			}
		}
		
		return ids.toString();
	}
	
	public static String toString(Long[] idList) {
		if (idList == null || idList.length == 0) {
			return null;
		}
		
		StringBuilder ids = new StringBuilder();
		for (int i = 0; i < idList.length; i++) {
			if (i < idList.length - 1) {
				ids.append(idList[i] + BasketballConstants.SEPARATOR);
			} else {
				ids.append(idList[i]);
			}
		}
		
		return ids.toString();
	}
	
	public static String toString(Set<Long> idSet) {
		if (idSet == null || idSet.size() == 0){
			return null;
		}
		List<Long> idList = new ArrayList<Long>(idSet);

		StringBuilder ids = new StringBuilder();
		for (int i = 0; i < idList.size(); i++) {
			if (i < idList.size() - 1) {
				ids.append(idList.get(i) + BasketballConstants.SEPARATOR);
			} else {
				ids.append(idList.get(i));
			}
		}
		
		return ids.toString();
	}
	
	public static List<Long> toList(String ids) {
		if (StringUtils.isBlank(ids)) {
			return null;
		}
		
		String[] idArray = ids.split(BasketballConstants.SEPARATOR);
		if (idArray == null || idArray.length == 0) {
			return null;
		}
		
		List<Long> idList = new ArrayList<Long>();
		for (String id : idArray) {
			idList.add(Long.parseLong(id));
		}
		
		return idList;
	}
	
	public static Set<Long> toSet(String playerIds) {
		if (StringUtils.isBlank(playerIds)) {
			return null;
		}
		
		String[] playerIdArray = playerIds.split(BasketballConstants.SEPARATOR);
		if (playerIdArray == null || playerIdArray.length == 0) {
			return null;
		}
		
		Set<Long> playerIdSet = new HashSet<Long>();
		for (String playerId : playerIdArray) {
			playerIdSet.add(Long.parseLong(playerId));
		}
		
		return playerIdSet;
	}
	
	public static void main(String[] args) {
		List<Long> playerIdList = new ArrayList<Long>();
		playerIdList.add(1L);
		playerIdList.add(2L);
		playerIdList.add(3L);
		System.out.println(toString(playerIdList));
		
		playerIdList = toList("4,5,6");
		for (Long playerId : playerIdList) {
			System.out.println(playerId);
		}
	}
	
}
