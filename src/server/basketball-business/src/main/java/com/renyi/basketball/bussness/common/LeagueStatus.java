package com.renyi.basketball.bussness.common;

public enum LeagueStatus {

	/** 报名中. */
	signing(0),
	/** 报名结束. */
	signend(1),
	/** 比赛中. */
	gaming(2),
	/** 比赛结束. */
	end(3),
	/** 赛事未通过 */
	not_through(4),
	/** 赛事待审核 */
	wait_through(6);

	private int code;

	LeagueStatus(int code) {
		this.code = code;
	}

	public int code() {
		return code;
	}
}
