package com.renyi.basketball.bussness.common;

public enum MatchFormat {
	one(1, "一人制"),
	/** 两人制 */
	two(2, "两人制"),
	/** 三人制 */
	three(3, "三人制"),
	/** 四人制 */
	four(4, "四人制"),
	/** 五人制 */
	five(5, "五人制");

	private int id;
	private String name;

	private MatchFormat(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
