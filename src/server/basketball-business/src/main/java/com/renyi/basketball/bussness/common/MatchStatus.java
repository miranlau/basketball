package com.renyi.basketball.bussness.common;

/**
 * 比赛状态
 */
public enum MatchStatus {

	not_start(0, "未开始"), 
	quarter_1st(51, "第一节"), 
	quarter_2nd(52, "第二节"), 
	quarter_3rd(53, "第三节"), 
	quarter_4th(54, "第四节"), 
	overtime_1st(55, "加时1"), 
	overtime_2nd(56, "加时2"), 
	overtime_3rd(57, "加时3"), 
	overtime_4th(58, "加时4"), 
	overtime_5th(59, "加时5"), 
	overtime_6th(60, "加时6"),
	end(100, "结束");

	private int id;
	private String name;

	private static final MatchStatus[] allowedMatchStatus = { 
			not_start, 
			quarter_1st,
			quarter_2nd, 
			quarter_3rd,
			quarter_4th,
			overtime_1st,
			overtime_2nd, 
			overtime_3rd,
			overtime_4th, 
			overtime_5th, 
			overtime_6th, 
			end
	};

	public static String getName(int id) {
		return getName(id, false);
	}
	
	public static String getName(int id, boolean isExplicit) {
		for (MatchStatus matchStatus : allowedMatchStatus) {
			if (matchStatus.getId() == id) {
				if (isExplicit) {
					return matchStatus.getName();
				}
				if (matchStatus.getId() >= quarter_1st.getId() && matchStatus.getId() <= overtime_6th.getId()) {
					return "赛中";
				}
			}
		}
		return null;
	}
	
	private MatchStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
}
