package com.renyi.basketball.bussness.common;

/**
 * 球员位置
 */
public enum PlayerPosition {
	/** 控球后卫 */
	point_guard(1, "控球后卫"),
	/** 得分后卫 */
	shooting_guard(2, "得分后卫"),
	/** 小前锋 */
	small_foward(3, "小前锋"),
	/** 大前锋 */
	power_foward(4, "大前锋"),
	/** 中锋 */
	center(5, "中锋");

	private int id;
	private String name;

	private PlayerPosition(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static PlayerPosition valueOf(int id) {
		switch (id) {
		case 1:
			return point_guard;
		case 2:
			return shooting_guard;
		case 3:
			return small_foward;
		case 4:
			return power_foward;
		case 5:
			return center;
		default:
			return null;
		}
	}

}
