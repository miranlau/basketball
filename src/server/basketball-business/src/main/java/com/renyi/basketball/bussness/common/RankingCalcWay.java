package com.renyi.basketball.bussness.common;

public enum RankingCalcWay {
	NBL_TEAM(0, "NBL球队排名"), CBA_TEAM(1, "CBA球队排名");

	private int id;
	private String name;

	private RankingCalcWay(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
