package com.renyi.basketball.bussness.common;

public enum RankingType {
	TEAM_RANKING(0, "球队排名"), PLAYER_SCORING(1, "球员得分"), PLAYER_REBOUND(1, "球员篮板"), PLAYER_ASSIST(1, "球员助攻");

	private int id;
	private String name;

	private RankingType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
