package com.renyi.basketball.bussness.common;

/**
 * 投篮位置
 */
public enum ScoringPosition {
	/** 油漆区(篮下) */
	painted_area(10, "painted_area"),
	/** 正面 */
	front(11, "front"),
	/** 左侧 */
	leftside(12, "leftside"),
	/** 右侧 */
	rightside(13, "rightside"),
	/** 左侧底角 */
	left_wing(20, "left_wing"),
	/** 右侧底角 */
	right_wing(21, "right_wing"),
	/** 左侧45度 */
	leftside_45(22, "leftside_45"),
	/** 右侧45度 */
	rightside_45(23, "rightside_45"),
	/** 顶弧 */
	root_arc(24, "root_arc");
	
	private int id;
	private String name;

	private ScoringPosition(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
