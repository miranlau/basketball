package com.renyi.basketball.bussness.common;

/**
 * 得分方式
 */
public enum ShootingWay {
	/** 接球投篮 */
	standing_shot(0, "standing_shot"),
	/** 运球投篮 */
	drive_shot(1, "drive_shot"),
	/** 罚球 */
	free_throw(2, "free_throw");

	private int id;
	private String name;

	private ShootingWay(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
