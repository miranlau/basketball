package com.renyi.basketball.bussness.common;

import java.text.DecimalFormat;

public final class StatsAlgorithm {
	private static final String PERCENT_PATTERN = "00.00";

	public static int sum(Integer... ints) {
		if (ints == null || ints.length == 0) {
			return 0;
		}
		int sum = 0;
		for (Integer i : ints) {
			sum = sum + i;
		}
		return sum;
	}

	public static int score(int twoPointsShot, int threePointsShot, int freeThrowShot) {
		return (twoPointsShot * 2) + (threePointsShot * 3) + freeThrowShot;
	}

	public static float divided(double dividend, int divisor) {
		if (dividend == 0 || divisor == 0) {
			return 0.00F;
		}
		return Float.parseFloat(new DecimalFormat(PERCENT_PATTERN).format((dividend / divisor)));
	}

	public static float percent(double dividend, int divisor) {
		if (dividend == 0 || divisor == 0) {
			return 0.00F;
		}
		return Float.parseFloat(new DecimalFormat(PERCENT_PATTERN).format((dividend / divisor) * 100));
	}

	public static float percent(double dividend, double divisor) {
		if (dividend == 0 || divisor == 0) {
			return 0.00F;
		}
		return Float.parseFloat(new DecimalFormat(PERCENT_PATTERN).format((dividend / divisor) * 100));
	}

	public static float percent(int dividend, int divisor) {
		if (dividend == 0 || divisor == 0) {
			return 0.00F;
		}
		return Float.parseFloat(new DecimalFormat(PERCENT_PATTERN).format(((dividend * 1.0) / divisor) * 100));
	}

	public static float avg(float a, float b) {
		if (a == 0 || b == 0) {
			return 0.00F;
		}
		return (a + b) / 2;
	}

	public static float avg(Float currentAvgVal, Integer currentVal, Integer round) {
		if ((currentVal == null || currentVal == 0) || (round == null || round == 0)) {
			if (currentAvgVal == null || currentAvgVal == 0) {
				return 0;
			}
			return currentAvgVal;
		}
		return (((round - 1) * currentAvgVal) + currentVal) / round;
	}

	public static float avg(Float currentAvgVal, Float currentVal, Integer round) {
		if ((currentVal == null || currentVal == 0) || (round == null || round == 0)) {
			if (currentAvgVal == null || currentAvgVal == 0) {
				return 0;
			}
			return currentAvgVal;
		}
		return (((round - 1) * currentAvgVal) + currentVal) / round;
	}

}
