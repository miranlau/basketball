package com.renyi.basketball.bussness.common;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.PlayerSeasonStats;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.po.TeamSeasonStats;
import com.renyi.basketball.bussness.utils.PropertyUtils;

public final class StatsBuilder {
	private static Logger logger = Logger.getLogger(StatsBuilder.class);

	public static TeamSeasonStats buildTeamSeasonStatsForSave(TeamMatchStats teamMatchStats) {
		TeamSeasonStats teamSeasonStats = new TeamSeasonStats();
		try {
			PropertyUtils.copyProperties(teamMatchStats, teamSeasonStats);
		} catch (Exception e) {
			logger.error("copy property from TeamMatchStats to TeamSeasonStats failed", e);
			return teamSeasonStats;
		}
		teamSeasonStats.setMatchIds(String.valueOf(teamMatchStats.getMatchId()));
		// 胜负统计
		if (teamMatchStats.getIsWin()) {
			teamSeasonStats.setWin(1);
		} else {
			teamSeasonStats.setLoss(1);
		}
		return teamSeasonStats;
	}

	public static TeamSeasonStats buildTeamSeasonStatsForUpdate(TeamSeasonStats teamSeasonStats, TeamMatchStats teamMatchStats) {
		teamSeasonStats.setRoundId(teamMatchStats.getRoundId());
		teamSeasonStats.setMatchIds(mergeMatchIds(teamSeasonStats.getMatchIds(), teamMatchStats.getMatchId()));

		// 胜负统计
		if (teamMatchStats.getIsWin()) {
			teamSeasonStats.setWin(teamSeasonStats.getWin() + 1);
		} else {
			teamSeasonStats.setLoss(teamSeasonStats.getLoss() + 1);
		}

		Integer round = getRoundIndex(teamSeasonStats.getMatchIds());

		// 基础数据
		teamSeasonStats.setTwoPointsShot(StatsAlgorithm.avg(teamSeasonStats.getTwoPaintedShot(), teamMatchStats.getTwoPointsShot(), round));
		teamSeasonStats.setTwoPointsAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getTwoPointsAttempt(), teamMatchStats.getTwoPaintedAttempt(), round));
		teamSeasonStats
				.setThreePointsShot(StatsAlgorithm.avg(teamSeasonStats.getThreePointsShot(), teamMatchStats.getThreePointsShot(), round));
		teamSeasonStats.setThreePointsAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getThreePointsShot(), teamMatchStats.getThreePointsAttempt(), round));
		teamSeasonStats.setFreeThrowShot(StatsAlgorithm.avg(teamSeasonStats.getFreeThrowShot(), teamMatchStats.getFreeThrowShot(), round));
		teamSeasonStats.setFreeThrowAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getFreeThrowAttempt(), teamMatchStats.getFreeThrowAttempt(), round));
		teamSeasonStats.setOffensiveRebound(
				StatsAlgorithm.avg(teamSeasonStats.getOffensiveRebound(), teamMatchStats.getOffensiveRebound(), round));
		teamSeasonStats.setDefensiveRebound(
				StatsAlgorithm.avg(teamSeasonStats.getDefensiveRebound(), teamMatchStats.getDefensiveRebound(), round));
		teamSeasonStats.setAssist(StatsAlgorithm.avg(teamSeasonStats.getAssist(), teamMatchStats.getAssist(), round));
		teamSeasonStats.setSteal(StatsAlgorithm.avg(teamSeasonStats.getSteal(), teamMatchStats.getSteal(), round));
		teamSeasonStats.setTurnover(StatsAlgorithm.avg(teamSeasonStats.getTurnover(), teamMatchStats.getTurnover(), round));
		teamSeasonStats.setBlockShot(StatsAlgorithm.avg(teamSeasonStats.getBlockShot(), teamMatchStats.getBlockShot(), round));
		teamSeasonStats.setFoul(StatsAlgorithm.avg(teamSeasonStats.getFoul(), teamMatchStats.getFoul(), round));
		// 进阶数据
		teamSeasonStats.setTwoPaintedShot(StatsAlgorithm.avg(teamSeasonStats.getTwoPaintedShot(), teamMatchStats.getTwoFrontShot(), round));
		teamSeasonStats.setTwoPaintedAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getTwoPaintedAttempt(), teamMatchStats.getTwoPaintedAttempt(), round));
		teamSeasonStats.setTwoFrontShot(StatsAlgorithm.avg(teamSeasonStats.getTwoFrontShot(), teamMatchStats.getTwoFrontShot(), round));
		teamSeasonStats
				.setTwoFrontAttempt(StatsAlgorithm.avg(teamSeasonStats.getTwoFrontAttempt(), teamMatchStats.getTwoFrontAttempt(), round));
		teamSeasonStats
				.setTwoLeftsideShot(StatsAlgorithm.avg(teamSeasonStats.getTwoLeftsideShot(), teamMatchStats.getTwoLeftsideShot(), round));
		teamSeasonStats.setTwoLeftsideAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getTwoLeftsideAttempt(), teamMatchStats.getTwoLeftsideAttempt(), round));
		teamSeasonStats.setTwoRightsideShot(
				StatsAlgorithm.avg(teamSeasonStats.getTwoRightsideShot(), teamMatchStats.getTwoRightsideShot(), round));
		teamSeasonStats.setTwoRightsideAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getTwoRightsideAttempt(), teamMatchStats.getTwoRightsideAttempt(), round));
		teamSeasonStats.setThreeLeftWingShot(
				StatsAlgorithm.avg(teamSeasonStats.getThreeLeftWingShot(), teamMatchStats.getThreeLeftWingShot(), round));
		teamSeasonStats.setThreeLeftWingAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getThreeLeftWingAttempt(), teamMatchStats.getThreeLeftWingAttempt(), round));
		teamSeasonStats.setThreeRightWingShot(
				StatsAlgorithm.avg(teamSeasonStats.getThreeRightWingShot(), teamMatchStats.getThreeRightWingShot(), round));
		teamSeasonStats.setThreeRightWingAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getThreeRightWingAttempt(), teamMatchStats.getThreeRightWingAttempt(), round));
		teamSeasonStats.setThreeLeftside45Shot(
				StatsAlgorithm.avg(teamSeasonStats.getThreeLeftside45Shot(), teamMatchStats.getThreeLeftside45Shot(), round));
		teamSeasonStats.setThreeLeftside45Attempt(
				StatsAlgorithm.avg(teamSeasonStats.getThreeLeftside45Attempt(), teamMatchStats.getThreeLeftside45Attempt(), round));
		teamSeasonStats.setThreeRightside45Shot(
				StatsAlgorithm.avg(teamSeasonStats.getThreeRightside45Shot(), teamMatchStats.getThreeRightside45Shot(), round));
		teamSeasonStats.setThreeRightside45Attempt(
				StatsAlgorithm.avg(teamSeasonStats.getThreeRightside45Attempt(), teamMatchStats.getThreeRightside45Attempt(), round));
		teamSeasonStats.setThreeRootArcShot(
				StatsAlgorithm.avg(teamSeasonStats.getThreeRootArcShot(), teamMatchStats.getThreeRootArcShot(), round));
		teamSeasonStats.setThreeRootArcAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getThreeRootArcAttempt(), teamMatchStats.getThreeRootArcAttempt(), round));
		// 进攻侧重比
		teamSeasonStats.setPaintedOffensivePct(
				StatsAlgorithm.avg(teamSeasonStats.getPaintedOffensivePct(), teamMatchStats.getPaintedOffensivePct(), round));
		teamSeasonStats.setLeftOffensivePct(
				StatsAlgorithm.avg(teamSeasonStats.getLeftOffensivePct(), teamMatchStats.getLeftOffensivePct(), round));
		teamSeasonStats.setRightOffensivePct(
				StatsAlgorithm.avg(teamSeasonStats.getRightOffensivePct(), teamMatchStats.getRightOffensivePct(), round));
		teamSeasonStats.setFrontOffensivePct(
				StatsAlgorithm.avg(teamSeasonStats.getFrontOffensivePct(), teamMatchStats.getFrontOffensivePct(), round));
		// 运球接球投篮
		teamSeasonStats.setStandingShot(StatsAlgorithm.avg(teamSeasonStats.getStandingShot(), teamMatchStats.getStandingShot(), round));
		teamSeasonStats
				.setStandingAttempt(StatsAlgorithm.avg(teamSeasonStats.getStandingAttempt(), teamMatchStats.getStandingAttempt(), round));
		teamSeasonStats.setDribblingShot(StatsAlgorithm.avg(teamSeasonStats.getDribblingShot(), teamMatchStats.getDribblingShot(), round));
		teamSeasonStats.setDribblingAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getDribblingAttempt(), teamMatchStats.getDribblingAttempt(), round));
		// 进攻节奏(进攻的总次数)
		teamSeasonStats.setOffensiveAttempt(
				StatsAlgorithm.avg(teamSeasonStats.getDribblingAttempt(), teamMatchStats.getDribblingAttempt(), round));
		// 效率
		teamSeasonStats.setOffensiveEff(StatsAlgorithm.avg(teamSeasonStats.getOffensiveEff(), teamMatchStats.getOffensiveEff(), round));
		teamSeasonStats.setDefensiveEff(StatsAlgorithm.avg(teamSeasonStats.getDefensiveEff(), teamMatchStats.getDefensiveEff(), round));
		// 雷达图
		teamSeasonStats.setEfgPct(StatsAlgorithm.avg(teamSeasonStats.getEfgPct(), teamMatchStats.getEfgPct(), round));
		teamSeasonStats.setTsPct(StatsAlgorithm.avg(teamSeasonStats.getTsPct(), teamMatchStats.getTsPct(), round));
		teamSeasonStats.setOffensivePct(StatsAlgorithm.avg(teamSeasonStats.getOffensivePct(), teamMatchStats.getOffensivePct(), round));
		teamSeasonStats.setAssistPct(StatsAlgorithm.avg(teamSeasonStats.getAssistPct(), teamMatchStats.getAssistPct(), round));
		teamSeasonStats.setReboundPct(StatsAlgorithm.avg(teamSeasonStats.getReboundPct(), teamMatchStats.getReboundPct(), round));
		teamSeasonStats.setTurnoverPct(StatsAlgorithm.avg(teamSeasonStats.getTurnoverPct(), teamMatchStats.getTurnoverPct(), round));
		return teamSeasonStats;
	}

	public static PlayerSeasonStats buildPlayerSeasonStatsForSave(PlayerMatchStats playerMatchStats) {
		PlayerSeasonStats playerSeasonStats = new PlayerSeasonStats();
		try {
			PropertyUtils.copyProperties(playerMatchStats, playerSeasonStats);
		} catch (Exception e) {
			logger.error("copy property from PlayerMatchStats to PlayerSeasonStats failed", e);
			return playerSeasonStats;
		}
		playerSeasonStats.setMatchIds(String.valueOf(playerMatchStats.getMatchId()));
		return playerSeasonStats;
	}

	public static PlayerSeasonStats buildPlayerSeasonStatsForUpdate(PlayerSeasonStats playerSeasonStats,
			PlayerMatchStats playerMatchStats) {
		playerSeasonStats.setRoundId(playerMatchStats.getRoundId());
		playerSeasonStats.setMatchIds(mergeMatchIds(playerSeasonStats.getMatchIds(), playerMatchStats.getMatchId()));
		Integer round = getRoundIndex(playerSeasonStats.getMatchIds());
		// 基础数据
		playerSeasonStats
				.setPlayingTime(StatsAlgorithm.avg(playerSeasonStats.getPlayingTime(), playerMatchStats.getTwoPointsShot(), round));
		playerSeasonStats.setScore(StatsAlgorithm.avg(playerSeasonStats.getScore(), playerMatchStats.getScore(), round));
		playerSeasonStats
				.setTwoPointsShot(StatsAlgorithm.avg(playerSeasonStats.getTwoPointsShot(), playerMatchStats.getTwoPointsShot(), round));
		playerSeasonStats.setTwoPointsAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getTwoPointsAttempt(), playerMatchStats.getTwoPointsAttempt(), round));
		playerSeasonStats
				.setTwoPointsShot(StatsAlgorithm.avg(playerSeasonStats.getThreePointsShot(), playerMatchStats.getThreePointsShot(), round));
		playerSeasonStats.setThreePointsShot(
				StatsAlgorithm.avg(playerSeasonStats.getThreePointsShot(), playerMatchStats.getThreePointsShot(), round));
		playerSeasonStats.setThreePointsAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getThreePointsAttempt(), playerMatchStats.getThreePointsAttempt(), round));
		playerSeasonStats
				.setFreeThrowShot(StatsAlgorithm.avg(playerSeasonStats.getFreeThrowShot(), playerMatchStats.getFreeThrowShot(), round));
		playerSeasonStats.setFreeThrowAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getFreeThrowAttempt(), playerMatchStats.getFreeThrowAttempt(), round));
		playerSeasonStats.setOffensiveRebound(
				StatsAlgorithm.avg(playerSeasonStats.getOffensiveRebound(), playerMatchStats.getOffensiveRebound(), round));
		playerSeasonStats.setDefensiveRebound(
				StatsAlgorithm.avg(playerSeasonStats.getDefensiveRebound(), playerMatchStats.getDefensiveRebound(), round));
		playerSeasonStats.setAssist(StatsAlgorithm.avg(playerSeasonStats.getAssist(), playerMatchStats.getAssist(), round));
		playerSeasonStats.setSteal(StatsAlgorithm.avg(playerSeasonStats.getSteal(), playerMatchStats.getSteal(), round));
		playerSeasonStats.setTurnover(StatsAlgorithm.avg(playerSeasonStats.getTurnover(), playerMatchStats.getTurnover(), round));
		playerSeasonStats.setBlockShot(StatsAlgorithm.avg(playerSeasonStats.getBlockShot(), playerMatchStats.getBlockShot(), round));
		playerSeasonStats.setFoul(StatsAlgorithm.avg(playerSeasonStats.getFoul(), playerMatchStats.getFoul(), round));
		// 进阶数据(投篮区域)
		playerSeasonStats
				.setTwoPaintedShot(StatsAlgorithm.avg(playerSeasonStats.getTwoPaintedShot(), playerMatchStats.getTwoPaintedShot(), round));
		playerSeasonStats.setTwoPaintedAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getTwoPaintedAttempt(), playerMatchStats.getTwoPaintedAttempt(), round));
		playerSeasonStats
				.setTwoFrontShot(StatsAlgorithm.avg(playerSeasonStats.getTwoFrontShot(), playerMatchStats.getTwoFrontShot(), round));
		playerSeasonStats.setTwoFrontAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getTwoFrontAttempt(), playerMatchStats.getTwoFrontAttempt(), round));
		playerSeasonStats.setTwoLeftsideShot(
				StatsAlgorithm.avg(playerSeasonStats.getTwoLeftsideShot(), playerMatchStats.getTwoLeftsideShot(), round));
		playerSeasonStats.setTwoLeftsideAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getTwoLeftsideAttempt(), playerMatchStats.getTwoLeftsideAttempt(), round));
		playerSeasonStats.setTwoRightsideShot(
				StatsAlgorithm.avg(playerSeasonStats.getTwoRightsideShot(), playerMatchStats.getTwoRightsideShot(), round));
		playerSeasonStats.setTwoRightsideAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getTwoRightsideAttempt(), playerMatchStats.getTwoRightsideAttempt(), round));
		playerSeasonStats.setThreeLeftWingShot(
				StatsAlgorithm.avg(playerSeasonStats.getThreeLeftWingShot(), playerMatchStats.getThreeLeftWingShot(), round));
		playerSeasonStats.setThreeLeftWingAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getThreeLeftWingAttempt(), playerMatchStats.getThreeLeftWingAttempt(), round));
		playerSeasonStats.setThreeRightWingShot(
				StatsAlgorithm.avg(playerSeasonStats.getThreeRightWingShot(), playerMatchStats.getThreeRightWingShot(), round));
		playerSeasonStats.setThreeRightWingAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getThreeRightWingAttempt(), playerMatchStats.getThreeRightWingAttempt(), round));
		playerSeasonStats.setThreeLeftside45Shot(
				StatsAlgorithm.avg(playerSeasonStats.getThreeLeftside45Shot(), playerMatchStats.getThreeLeftside45Shot(), round));
		playerSeasonStats.setThreeLeftside45Attempt(
				StatsAlgorithm.avg(playerSeasonStats.getThreeLeftside45Attempt(), playerMatchStats.getThreeLeftside45Attempt(), round));
		playerSeasonStats.setThreeRightside45Shot(
				StatsAlgorithm.avg(playerSeasonStats.getThreeRightside45Shot(), playerMatchStats.getThreeRightside45Shot(), round));
		playerSeasonStats.setThreeRightside45Attempt(
				StatsAlgorithm.avg(playerSeasonStats.getThreeRightside45Attempt(), playerMatchStats.getThreeRightside45Attempt(), round));
		playerSeasonStats.setThreeRootArcShot(
				StatsAlgorithm.avg(playerSeasonStats.getThreeRootArcShot(), playerMatchStats.getThreeRootArcShot(), round));
		playerSeasonStats.setThreeRootArcAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getThreeRootArcAttempt(), playerMatchStats.getThreeRootArcAttempt(), round));
		// 进阶数据(其它)
		// 进攻侧重比
		playerSeasonStats.setPaintedOffensivePct(
				StatsAlgorithm.avg(playerSeasonStats.getPaintedOffensivePct(), playerMatchStats.getPaintedOffensivePct(), round));
		playerSeasonStats.setLeftOffensivePct(
				StatsAlgorithm.avg(playerSeasonStats.getLeftOffensivePct(), playerMatchStats.getLeftOffensivePct(), round));
		playerSeasonStats.setRightOffensivePct(
				StatsAlgorithm.avg(playerSeasonStats.getRightOffensivePct(), playerMatchStats.getRightOffensivePct(), round));
		playerSeasonStats.setFrontOffensivePct(
				StatsAlgorithm.avg(playerSeasonStats.getFrontOffensivePct(), playerMatchStats.getFrontOffensivePct(), round));
		// 运球接球投篮
		playerSeasonStats
				.setStandingShot(StatsAlgorithm.avg(playerSeasonStats.getStandingShot(), playerMatchStats.getStandingShot(), round));
		playerSeasonStats.setStandingAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getStandingAttempt(), playerMatchStats.getStandingAttempt(), round));
		playerSeasonStats
				.setDribblingShot(StatsAlgorithm.avg(playerSeasonStats.getDribblingShot(), playerMatchStats.getDribblingShot(), round));
		playerSeasonStats.setDribblingAttempt(
				StatsAlgorithm.avg(playerSeasonStats.getDribblingAttempt(), playerMatchStats.getDribblingAttempt(), round));
		// 进攻占比
		playerSeasonStats
				.setOffensiveRatio(StatsAlgorithm.avg(playerSeasonStats.getOffensiveRatio(), playerMatchStats.getOffensiveRatio(), round));
		// 效率值(The Player Efficiency Rating)
		playerSeasonStats.setPer(StatsAlgorithm.avg(playerSeasonStats.getPer(), playerMatchStats.getPer(), round));
		// 正负值 (real plus-minus)
		playerSeasonStats.setRpm(StatsAlgorithm.avg(playerSeasonStats.getRpm(), playerMatchStats.getRpm(), round));
		// 雷达图
		playerSeasonStats.setEfgPct(StatsAlgorithm.avg(playerSeasonStats.getEfgPct(), playerMatchStats.getEfgPct(), round));
		playerSeasonStats.setTsPct(StatsAlgorithm.avg(playerSeasonStats.getTsPct(), playerMatchStats.getTsPct(), round));
		playerSeasonStats
				.setOffensivePct(StatsAlgorithm.avg(playerSeasonStats.getOffensivePct(), playerMatchStats.getOffensivePct(), round));
		playerSeasonStats.setAssistPct(StatsAlgorithm.avg(playerSeasonStats.getAssistPct(), playerMatchStats.getAssistPct(), round));
		playerSeasonStats.setReboundPct(StatsAlgorithm.avg(playerSeasonStats.getReboundPct(), playerMatchStats.getReboundPct(), round));
		playerSeasonStats.setTurnoverPct(StatsAlgorithm.avg(playerSeasonStats.getTurnoverPct(), playerMatchStats.getTurnoverPct(), round));
		return playerSeasonStats;
	}

	private static int getRoundIndex(String matchIds) {
		List<Long> matchIdList = IdAnalyzer.toList(matchIds);
		if (CollectionUtils.isEmpty(matchIdList)) {
			return 1;
		}
		return matchIdList.size() + 1;
	}

	private static String mergeMatchIds(String matchIds, Long matchId) {
		Set<Long> matchIdSet = IdAnalyzer.toSet(matchIds);
		if (CollectionUtils.isEmpty(matchIdSet)) {
			matchIdSet = new HashSet<Long>();
		}
		matchIdSet.add(matchId);
		return IdAnalyzer.toString(matchIdSet);
	}

}
