package com.renyi.basketball.bussness.common;

public enum StatsStatus {
	/** 未生成 */
	not_generated(0, "not_generated"),
	/** 生成球队比赛的数据统计 */
	generate_team_match_stats(1, "generate_team_match_stats"),
	/** 生成球队赛季的数据统计 */
	generate_team_season_stats(2, "generate_team_season_stats"),
	/** 生成球队的赛季排名 */
	generate_team_ranking(3, "generate_team_ranking"),
	/** 生成球队赛季的数据统计 */
	generate_player_match_stats(4, "generate_player_match_stats"),
	/** 生成球员赛季的数据统计 */
	tenerate_player_season_stats(5, "generate_player_season_stats");

	private Integer id;
	private String name;

	private StatsStatus(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public static boolean isNotValid(Integer id) {
		return !isValid(id);
	}
	
	public static boolean isValid(Integer id) {
		if (id == null) {
			return false;
		}
		StatsStatus[] values = StatsStatus.values();
		for (StatsStatus statsStatus : values) {
			if (statsStatus.getId().equals(id)) {
				return true;
			}
		}
		return false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
