package com.renyi.basketball.bussness.constants;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public final class ActionSpeaker {

	/** 话术. */
	private final static Map<Integer, String> SPEAKS = new HashMap<>();
	
	private static final String SPEAKS_TWO_POINTS_IN = "{0}{1}，两分出手！命中！当前比分{2}:{3}";
	private static final String SPEAKS_TWO_POINTS_OUT = "{0}{1}，两分出手！未命中！当前比分{2}:{3}";
	private static final String SPEAKS_THREE_POINTS_IN = "{0}{1}，三分出手！命中！当前比分{2}:{3}";
	private static final String SPEAKS_THREE_POINTS_OUT = "{0}{1}，三分出手！未命中！当前比分{2}:{3}";

	static {
		SPEAKS.put(ActionStatus.START, "跳球！比赛开始！");
		
		SPEAKS.put(ActionStatus.TWO_LEFTSIDE_STANDING_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_LEFTSIDE_DRIBBLING_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_RIGHTSIDE_STANDING_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_RIGHTSIDE_DRIBBLING_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_PAINTED_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_FRONT_STANDING_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_FRONT_DRIBBLING_IN, SPEAKS_TWO_POINTS_IN);
		SPEAKS.put(ActionStatus.TWO_LEFTSIDE_STANDING_OUT, SPEAKS_TWO_POINTS_OUT);
		SPEAKS.put(ActionStatus.TWO_LEFTSIDE_DRIBBLING_OUT, SPEAKS_TWO_POINTS_OUT);
		SPEAKS.put(ActionStatus.TWO_RIGHTSIDE_STANDING_OUT, SPEAKS_TWO_POINTS_OUT);
		SPEAKS.put(ActionStatus.TWO_RIGHTSIDE_DRIBBLING_OUT, SPEAKS_TWO_POINTS_OUT);
		SPEAKS.put(ActionStatus.TWO_PAINTED_OUT, SPEAKS_TWO_POINTS_OUT);
		SPEAKS.put(ActionStatus.TWO_FRONT_STANDING_OUT, SPEAKS_TWO_POINTS_OUT);
		SPEAKS.put(ActionStatus.TWO_FRONT_DRIBBLING_OUT, SPEAKS_TWO_POINTS_OUT);
		
		SPEAKS.put(ActionStatus.THREE_LEFT_WING_STANDING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_LEFT_WING_DRIBBLING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_LEFTSIDE45_STANDING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_LEFTSIDE45_DRIBBLING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_ROOT_ARC_STANDING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_ROOT_ARC_DRIBBLING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_RIGHTSIDE45_STANDING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_RIGHT_WING_STANDING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_RIGHT_WING_DRIBBLING_IN, SPEAKS_THREE_POINTS_IN);
		SPEAKS.put(ActionStatus.THREE_LEFT_WING_STANDING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_LEFT_WING_DRIBBLING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_LEFTSIDE45_STANDING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_LEFTSIDE45_DRIBBLING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_ROOT_ARC_STANDING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_ROOT_ARC_DRIBBLING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_RIGHTSIDE45_STANDING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_RIGHT_WING_STANDING_OUT, SPEAKS_THREE_POINTS_OUT);
		SPEAKS.put(ActionStatus.THREE_RIGHT_WING_DRIBBLING_OUT, SPEAKS_THREE_POINTS_OUT);
		
		SPEAKS.put(ActionStatus.BLOCKSHOT, "{0}{1}，送出一个盖帽！");
		SPEAKS.put(ActionStatus.OFFENSIVE_REBOUND, "{0}{1}，摘下一个前场篮板！");
		SPEAKS.put(ActionStatus.DEFENSIVE_REBOUND, "{0}{1}，摘下一个后场篮板！");
		SPEAKS.put(ActionStatus.STEAL, "{0}{1}，完成一次抢断！");
		SPEAKS.put(ActionStatus.OFFENSIVE_FOUL, "{0}{1}，进攻犯规！");
		SPEAKS.put(ActionStatus.DEFENSIVE_FOUL, "{0}{1}，防守犯规！");
		SPEAKS.put(ActionStatus.TECHNICAL_FOUL, "{0}{1}，技术犯规！");
		SPEAKS.put(ActionStatus.FLAGRANT_FOUL, "{0}{1}，恶意犯规！");
		SPEAKS.put(ActionStatus.UNSPORTSMANLIKE_FOUL, "{0}{1}，违体犯规！");
		SPEAKS.put(ActionStatus.ASSIST, "{0}{1}助攻！");
		SPEAKS.put(ActionStatus.TURNOVER, "{0}{1}失误！");

		SPEAKS.put(ActionStatus.SUBSTITUTE, "{0}队换人，换上{1}队员，换下{2}队员");

		SPEAKS.put(ActionStatus.FREE_THROW_IN, "{0}{1}罚进，比分{2}:{3}");
		SPEAKS.put(ActionStatus.FREE_THROW_OUT, "{0}{1}罚丢，就差一点儿就进了");
		SPEAKS.put(ActionStatus.QUARTER_START, "第{0}节比赛开始！");
		SPEAKS.put(ActionStatus.QUARTER_END, "第{0}节比赛结束！");
		SPEAKS.put(ActionStatus.OVERTIME_START, "第{0}节加时比赛开始！");
		SPEAKS.put(ActionStatus.OVERTIME_END, "第{0}节加时比赛结束！");

		SPEAKS.put(ActionStatus.TIMEOUT, "比赛暂停！当前比分为{0}:{1}！");
		SPEAKS.put(ActionStatus.TIMEOUT_RESUME, "比赛恢复！当前比分为{0}:{1}！");
		SPEAKS.put(ActionStatus.GAME_OVER, "比赛结束！比分为{0}:{1}！恭喜{2}获得胜利，同时希望{3}继续努力！");

	}

	public static String getSpeak(Integer action, Object... args) {
		String templete = SPEAKS.get(action);
		if (templete == null) {
			return null;
		}
		return MessageFormat.format(templete, args);
	}

}
