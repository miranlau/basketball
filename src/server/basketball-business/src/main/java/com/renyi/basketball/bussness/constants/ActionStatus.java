package com.renyi.basketball.bussness.constants;

public final class ActionStatus {
	public final static int MIN_STATUS = 0;
	public final static int MAX_STATUS = 100;
	public final static int MIN_ACTION = 101;
	public final static int MAX_ACTION = 399;
	
	public static boolean canRevoke(Integer status) {
		return status != null && status >= MIN_ACTION && status < MAX_ACTION;
	}

	public static boolean canNotRevoke(Integer status) {
		return status != null && status >= MIN_STATUS && status <= MAX_STATUS && status != SUBSTITUTE && status != REVOKE_ACTION;
	}
	
	/** 比赛状态: 未开始. */
	public final static int NOT_START = 0;
	/** 比赛状态: 开始. */
	public final static int START = 1;
	/** 比赛状态： 跳球. */
	public final static int JUMP_BALL = 2;
	/** 比赛状态: 暂停. */
	public final static int TIMEOUT = 3;
	/** 暂停后继续 */
	public final static int TIMEOUT_RESUME = 4;
	/** 比赛状态： 换人. */
	public final static int SUBSTITUTE = 5;
	/** 比赛状态: 一节开始. */
	public final static int QUARTER_START = 6;
	/** 比赛状态: 一节结束. */
	public final static int QUARTER_END = 7;
	/** 比赛状态: 加时赛开始 */
	public final static int OVERTIME_START = 8;
	/** 比赛状态: 加时赛结束 */
	public final static int OVERTIME_END = 9;
	/** 比赛状态: 场上比赛阶段，第一节：51，第二节：52，第三节53，第四节：54，加时赛第一节：55，以此类推. */
	public final static int NORMAL_TIME = 50;
	/** 比赛状态: 比赛停止状态，第一节：71，第二节：72，第三节73，第四节：74，加时赛第一节：75，以此类推 */
	public final static int STOP_TIME = 70;
	/** 直播信息 */
	public final static int LIVE_MESSAGE = 77;
	/** 直播图片 */
	public final static int LIVE_PHOTO = 88;
	/** 撤销上一次action */
	public final static int REVOKE_ACTION = 99;
	/** 比赛状态: 结束. */
	public final static int GAME_OVER = 100;
	
	// ---------------------------------------------------------------
	
	/** 前场篮板 */
	public final static int OFFENSIVE_REBOUND = 101;
	/** 后场篮板 */
	public final static int DEFENSIVE_REBOUND = 102;
	/** 盖帽 */
	public final static int BLOCKSHOT = 103;
	/** 助攻 */
	public final static int ASSIST = 104;
	/** 抢断 */
	public final static int STEAL = 105;
	/** 失误 */
	public final static int TURNOVER = 106;
	/** 进攻犯规 */
	public final static int OFFENSIVE_FOUL = 107;
	/** 防守犯规 */
	public final static int DEFENSIVE_FOUL = 108;
	/** 技术犯规 */
	public final static int TECHNICAL_FOUL = 109;
	/** 恶意犯规 */
	public final static int FLAGRANT_FOUL = 110;
	/** 违踢犯规 */
	public final static int UNSPORTSMANLIKE_FOUL = 111;
	
	/** 罚篮命中 */
	public final static int FREE_THROW_IN = 112;
	/** 罚篮不中 */
	public final static int FREE_THROW_OUT = 113;

	/** 2分 左侧 接球 命中 */
	public final static int TWO_LEFTSIDE_STANDING_IN = 201;
	/** 2分 左侧 接球 不中 */
	public final static int TWO_LEFTSIDE_STANDING_OUT = 202;
	/** 2分 左侧 运球 命中 */
	public final static int TWO_LEFTSIDE_DRIBBLING_IN = 203;
	/** 2分 左侧 运球 不中 */
	public final static int TWO_LEFTSIDE_DRIBBLING_OUT = 204;

	/** 2分 右侧 接球 命中 */
	public final static int TWO_RIGHTSIDE_STANDING_IN = 205;
	/** 2分 右侧 接球 不中 */
	public final static int TWO_RIGHTSIDE_STANDING_OUT = 206;
	/** 2分 右侧 运球 命中 */
	public final static int TWO_RIGHTSIDE_DRIBBLING_IN = 207;
	/** 2分 右侧 运球 不中 */
	public final static int TWO_RIGHTSIDE_DRIBBLING_OUT = 208;

	/** 2分 篮下（油漆区） 命中 */
	public final static int TWO_PAINTED_IN = 209;
	/** 2分 篮下（油漆区）不中 */
	public final static int TWO_PAINTED_OUT = 210;

	/** 2分 正面 接球 命中 */
	public final static int TWO_FRONT_STANDING_IN = 213;
	/** 2分 正面 接球 不中 */
	public final static int TWO_FRONT_STANDING_OUT = 214;
	/** 2分 正面 运球 命中 */
	public final static int TWO_FRONT_DRIBBLING_IN = 215;
	/** 2分 正面 运球 不中 */
	public final static int TWO_FRONT_DRIBBLING_OUT = 216;

	/** 3分 左侧0度 接球 命中 */
	public final static int THREE_LEFT_WING_STANDING_IN = 301;
	/** 3分 左侧0度 接球 不中 */
	public final static int THREE_LEFT_WING_STANDING_OUT = 302;
	/** 3分 左侧0度 运球 命中 */
	public final static int THREE_LEFT_WING_DRIBBLING_IN = 303;
	/** 3分 左侧0度 运球 不中 */
	public final static int THREE_LEFT_WING_DRIBBLING_OUT = 304;

	/** 3分 左侧45度 接球 命中 */
	public final static int THREE_LEFTSIDE45_STANDING_IN = 305;
	/** 3分 左侧45度 接球 不中 */
	public final static int THREE_LEFTSIDE45_STANDING_OUT = 306;
	/** 3分 左侧45度 运球 命中 */
	public final static int THREE_LEFTSIDE45_DRIBBLING_IN = 307;
	/** 3分 左侧45度 运球 不中 */
	public final static int THREE_LEFTSIDE45_DRIBBLING_OUT = 308;

	/** 3分 顶弧 接球 命中 */
	public final static int THREE_ROOT_ARC_STANDING_IN = 309;
	/** 3分 顶弧 接球 不中 */
	public final static int THREE_ROOT_ARC_STANDING_OUT = 310;
	/** 3分 顶弧 运球 命中 */
	public final static int THREE_ROOT_ARC_DRIBBLING_IN = 311;
	/** 3分 顶弧 运球 不中 */
	public final static int THREE_ROOT_ARC_DRIBBLING_OUT = 312;

	/** 3分 右侧45 接球 命中 */
	public final static int THREE_RIGHTSIDE45_STANDING_IN = 313;
	/** 3分 右侧45 接球 不中 */
	public final static int THREE_RIGHTSIDE45_STANDING_OUT = 314;
	/** 3分 右侧45 运球 命中 */
	public final static int THREE_RIGHTSIDE45_DRIBBLING_IN = 315;
	/** 3分 右侧45 运球 不中 */
	public final static int THREE_RIGHTSIDE45_DRIBBLING_OUT = 316;

	/** 3分 左侧0度 接球 命中 */
	public final static int THREE_RIGHT_WING_STANDING_IN = 317;
	/** 3分 左侧0度 接球 不中 */
	public final static int THREE_RIGHT_WING_STANDING_OUT = 318;
	/** 3分 左侧0度 运球 命中 */
	public final static int THREE_RIGHT_WING_DRIBBLING_IN = 319;
	/** 3分 左侧0度 运球 不中 */
	public final static int THREE_RIGHT_WING_DRIBBLING_OUT = 320;

	/** 时间轴统计类型. */
	public final static int[] TIME_LINE_ACTIONS = new int[] { 
		// 比赛重要节点
		ActionStatus.START, 
		ActionStatus.QUARTER_START,
		ActionStatus.QUARTER_END,
		ActionStatus.OVERTIME_START,
		ActionStatus.OVERTIME_END,
		ActionStatus.TIMEOUT,
		ActionStatus.TIMEOUT_RESUME,
		ActionStatus.GAME_OVER,
		// 换人
		ActionStatus.SUBSTITUTE,
		// 罚球中
		ActionStatus.FREE_THROW_IN,
		// 2分中
		ActionStatus.TWO_LEFTSIDE_STANDING_IN, 
		ActionStatus.TWO_LEFTSIDE_DRIBBLING_IN, 
		ActionStatus.TWO_RIGHTSIDE_STANDING_IN, 
		ActionStatus.TWO_RIGHTSIDE_DRIBBLING_IN, 
		ActionStatus.TWO_PAINTED_IN, 
		ActionStatus.TWO_FRONT_STANDING_IN, 
		ActionStatus.TWO_FRONT_DRIBBLING_IN, 
		// 3分中
		ActionStatus.THREE_LEFT_WING_STANDING_IN,
		ActionStatus.THREE_LEFT_WING_DRIBBLING_IN,
		ActionStatus.THREE_LEFTSIDE45_STANDING_IN,
		ActionStatus.THREE_LEFTSIDE45_DRIBBLING_IN,
		ActionStatus.THREE_ROOT_ARC_STANDING_IN,
		ActionStatus.THREE_ROOT_ARC_DRIBBLING_IN,
		ActionStatus.THREE_RIGHTSIDE45_STANDING_IN,
		ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_IN,
		ActionStatus.THREE_RIGHT_WING_STANDING_IN,
		ActionStatus.THREE_RIGHT_WING_DRIBBLING_IN,
		// 技术
		ActionStatus.ASSIST, 
		ActionStatus.OFFENSIVE_REBOUND,
		ActionStatus.DEFENSIVE_REBOUND,
		ActionStatus.BLOCKSHOT,
		ActionStatus.STEAL,
		ActionStatus.TURNOVER,
		ActionStatus.OFFENSIVE_FOUL,
		ActionStatus.DEFENSIVE_FOUL,
		ActionStatus.FLAGRANT_FOUL,
		ActionStatus.UNSPORTSMANLIKE_FOUL
	};
	
}
