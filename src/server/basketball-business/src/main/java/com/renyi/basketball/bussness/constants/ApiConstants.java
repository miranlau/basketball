package com.renyi.basketball.bussness.constants;

/**
 * 返回代码定义.
 * Created by flyong86 on 2016/4/5.
 */
public class ApiConstants {

    /** 成功. */
    public final static int SUCCESS = 0;
    /** 失败. */
    public final static int FAIL = - 1;
    /** 数据库访问错误. */
    public final static int DB_EXCEPTION = -2;
    /** 字段不能为null. */
    public final static int FIELD_NOT_NULL = -1001;
    /** 字段类型不正确.*/
    public final static int FIELD_TYPE_ERROR = -1002;
    /** 字段超长. */
    public final static int FIELD_OVER_LONG = -1003;
    /** 查询对象不存在. */
    public final static int OBJECT_NOT_EXIST = -1004;
    /** 已操作 */
    public final static int ALREADY_MODIFY = -1005;



}
