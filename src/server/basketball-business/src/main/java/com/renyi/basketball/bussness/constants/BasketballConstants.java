package com.renyi.basketball.bussness.constants;

/**
 * Created by flyong86 on 2016/5/5.
 */
public interface BasketballConstants {

    interface SMSConstant{
        String ACCOUNT_SID = "8a48b5514fac9535014fb0ee14780a2d";
        String AUTH_TOKEN = "ac258af688104015b21a695b5c51d0d5";
        /** 全网足球appId */
        String APP_ID = "aaf98f894fd44d15014fe3894b100e88";

        /** 注册的短信模版 */
        String REGISTER_TEMPLATE_ID = "89782";
        /** 忘记密码的短信模版 */
        String FORGET_PASSWORD_TEMPLATE_ID = "89784";
        /** 绑定手机号的短信模版 */
        String BIND_MOBILE_TEMPLATE_ID = "89785";

        /** 后台创建球员提醒*/
        String ADD_PLAYER_TEMPLATE_ID = "50976";
        /** 后台添加比赛 1,时间  2,地点 3,对手*/
        String ADD_MATCH_TEMPLATE_ID = "46105";
        /** 验证码有效期 */
        int TERM_OF_VALIDLITY = 5;
    }
    
    public final static String SEPARATOR = ",";
    
    
	public static final String FILEPATHIMG = "upload/";	//图片上传路径
	public static final String FILEPATHFILE = "uploadFiles/file/";		//文件上传路径

    /** 密码加密后缀. */
    public final static String PASSWORD_MD5_PRIFX = "michael";

    /** 附近约赛  方圆50公里 .*/
    public final static Long CLEAR_MILES = 50000l;

    /** 用户球队缓存前缀. */
    public final static String CACHE_USER_TEAM_PREFIX = "USER_TEAM_";
    /** 比赛缓存前缀. */
    public final static String CACHE_GAME_TEAM_PREFIX = "MATCH_STATS_";
    /** 赛事数据榜单统计前缀 */
    public final static String CACHE_LEAGUE_BANG_PREFIX = "LEAGUE_BANG_";
    /** 赛事数据榜单统计缓存名 */
    public final static String CACHE_LEAGUE_NAME = "LeagueCountCache";

    /** 赛事比赛胜利获得积分. */
    public final static Integer WIN_TEAM_SCORE = 2;
    /** 赛事比赛失利获得积分. */
    public final static Integer LOSE_TEAM_SCORE = 1;

    /** 赛事榜单显示前多少名. */
    public final static Integer LEAGUE_BANG_MAX_LIST = 100;

    /** 比赛进入赛中间隔(提前30分钟). */
    public final static long START_MATCH_EXPR = -1 * 60 * 30;
    /** 比赛结束间隔(比赛开始后二十四小时). */
    public final static long END_MATCH_EXPR = 1440 * 60;

    /** 非该队队员. */
    public final static Integer RIGHT_NORMAL = 0;
    /** 该队队员. */
    public final static Integer RIGHT_MEMBER = 1;
    /** 该队队长. */
    public final static Integer RIGHT_LEADER = 2;

    /** 比赛赛制:5人制. */
    public final static Integer MATCH_TYPE_5 = 5;
    /** 比赛赛制:7人制. */
    public final static Integer MATCH_TYPE_7 = 7;
    /** 比赛赛制:9人制. */
    public final static Integer MATCH_TYPE_9 = 9;
    /** 比赛赛制:11人制. */
    public final static Integer MATCH_TYPE_11 = 11;

    /** 球员ID分隔符. */
    public final static String PLAYER_ID_SEPARATOR = ",";
    /** 球队ID分隔符. */
    public final static String TEAM_ID_SEPARATOR = ",";

    /** 球队的比赛，取最近前五条*/
    public final static Integer CLOSE_MATCH = 5;
    /** 场地安排，7天的安排. */
    public final static Integer MAX_PLACE_SCHEDULE = 7;
    
    /** 阅读，评论*/
    public final static String READ_NUM = "read_num";
    public final static String LAUD_NUM = "laud_num";
    public final static String COMMENT_NUM = "comment_num";
    
    /** 主队默认名称. */
	public final static String DEFAULT_HOME_TEAM_NAME = "主";
	/** 客队默认名称. */
	public final static String DEFAULT_VISITING_TEAM_NAME = "客";
	
	public final static String KEY_HOME_TEAM = "home";
	public final static String KEY_VISITING_TEAM = "visiting";
	
	public static final String KEY_CHANGE_UP = "up";
	public static final String KEY_CHANGE_DOWN = "down";
	public static final String KEY_FREE_THROW_CONTINUE = "continue";
	/** 图文直播最大返回数量. */
	public final static int MAX_LIVE_RESULT = 1000;
	
	public static final String KEY_TEAM_ID = "teamId";
    
}
