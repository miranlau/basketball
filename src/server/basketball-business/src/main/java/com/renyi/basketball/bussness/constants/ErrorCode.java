package com.renyi.basketball.bussness.constants;

/**
 * 异常编号.
 */
public enum ErrorCode {

	/** 共通错误. */
	FAIL(-1),
	/** 数据库访问异常. */
	DB_EXCEPTION(-2),

	/** 字段不能为null. */
	FIELD_NOT_NULL(1001),
	/** 字段值无效 */
	FIELD_VALUE_INVALID(1007),
	/** 字段类型不正确. */
	FIELD_TYPE_ERROR(1002),
	/** 字段超长. */
	FIELD_OVER_LONG(1003),
	/** 查询对象不存在. */
	OBJECT_NOT_EXIST(1004),
	/** 对象已存在. */
	OBJECT_EXIST_ERROR(1005),
	/** 对象不能为空 */
	OBJECT_NOT_NULL(1006),
	/** 用户名已存在. */
	USER_NAME_EXIST(2001),
	/** PO转换DTO失败. */
	PO_CANNOT_TO_DTO(2002),

	/** 球队信息不正确. */
	TEAM_NOT_VALID(3001),
	/** 无效的Action . */
	INVALID_ACTION(3002),
	/** 比赛已结束. */
	GAME_IS_OVER(3003),

	/** 换人失败. */
	GAME_CHANGE_PLAYER_FAIL(3004),
	/** 状态匹配不对，要求更新 */
	GAME_IS_STATUS(3005),

	MOBILE_ALREADY_EXIST(2002), 
	
	MOBILE_NOT_EXIST(2003),
	/** 不是队长没有权限 */
	PLAY_NOT_LEADER(3001),
	/** 球员已经加入该队伍 */
	PLAY_EXIST_TEAM(3002),
	/** 球员不存在该队伍 */
	PLAY_NOT_EXIST(3003),
	/** 不能踢出队长. */
	CAN_NOT_KICK_OUT_LEADER(3004),
	/** 已经创建过球队 */
	TEAM_EXIST_ERROR(3005),
	/** 比赛状态不正确. */
	MATCH_STAUTS_ERROR(4001),
	/** 用户资料未公开. */
	USER_NOT_OPEN(5001);

	int code;

	private ErrorCode(int code) {
		this.code = code;
	}

	public int code() {
		return code;
	}

}
