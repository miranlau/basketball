package com.renyi.basketball.bussness.constants;

public class RequestParams {

	public static final String MATCH_ID = "matchId";
	public static final String LEAGUE_ID = "leagueId";
	public static final String TEAM_ID = "teamId";
	public static final String TEAM_IDS = "teamIds";
	public static final String TEAM_ID1 = "teamId1";
	public static final String TEAM_ID2 = "teamId2";
	public static final String ROUND = "round";
	public static final String TYPE = "type";
	public static final String SIZE = "size";
	public static final String IS_CONTINUOUS_FREE_THROW = "isContinuousFreeThrow";
	public static final String IS_FREE_THROW_IN = "isFreeThrowIn";
	public static final String EXTENSION = "extension";
	public static final String STARTING_IDS = "startingIds";
	public static final String RECORDER_ID = "recorderId";
	public static final String QUARTER_REMAINING_TIME = "quarterRemainingTime";
	public static final String IS_HOME = "isHome";
	public static final String UP_PLAYER_IDS = "upPlayerIds";
	public static final String DOWN_PLAYER_IDS = "downPlayerIds";
	public static final String ACTION_STATUS = "actionStatus";
}