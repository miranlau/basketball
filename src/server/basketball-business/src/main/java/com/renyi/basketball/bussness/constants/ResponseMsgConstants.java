package com.renyi.basketball.bussness.constants;

public final class ResponseMsgConstants {
	public static final String PARAMETER_REQUIRED = "Parameter[%s] is required";
	public static final String PARAMETER_INVALID = "Parameter[%s] is invalid";
	public static final String LOGIN_REQUIRED = "Login requried";
	
	public static final String MATCH_STATS_NOT_FOUND = "MatchStats is not found, matchId = %d";
	public static final String ACTION_LIST_NOT_FOUND = "ActionList is not found, matchId = %d";
	public static final String TEAM_SEASON_STATS_NOT_FOUND = "TeamSeasonStats is not found, teamId = %d, leagueId = %d";
	public static final String MATCH_NOT_FOUND = "Match is not found, matchId = %d";
	public static final String MATCH_IS_OVER = "Match is over, matchId = %d";
	public static final String PLAYER_LIST_NOT_FOUND = "Player list is not found, teamId = %d";
	
	public static final String INSERT_TEAM_MATCH_STATS_ERROR = "save TeamMatchStats failed, teamId = %d, matchId = %d";
	public static final String INSERT_TEAM_SEASON_STATS_ERROR = "save TeamSeasonStats failed, teamId = %d, leagueId = %d, leagueStage = %d";
	public static final String UPDATE_TEAM_SEASON_STATS_ERROR = "update TeamSeasonStats failed, teamId = %d, leagueId = %d, leagueStage = %d";
	
	public static final String INSERT_PLAYER_MATCH_STATS_ERROR = "save PlayerMatchStats failed, playerId = %d, matchId = %d, error: %s";
	public static final String INSERT_PLAYER_SEASON_STATS_ERROR = "save PlayerSeasonStats failed, playerId = %d, leagueId = %d, leagueStage = %d";
	public static final String UPDATE_PLAYER_SEASON_STATS_ERROR = "update PlayerSeasonStats failed, playerId = %d, leagueId = %d, leagueStage = %d";
	
	public static final String INSERT_RANKING_ERROR = "save Ranking failed, leagueId = %d, leagueStageId = %d";
	public static final String UPDATE_RANKING_ERROR = "update Ranking failed, leagueId = %d, leagueStageId = %d";
	
	public static final String TEAM_NAME_EXIST = "team name is not exist, name: %s, please correct it.";
	public static final String UPLOAD_PLAYER_ERROR = "<script> window.parent.Notify('%s', 'top-right', '5000', 'danger', 'fa-desktop', true);</script>";
	public static final String UPLOAD_PLAYER_SUCCESS = "<script> window.parent.Notify('%s', 'top-right', '5000', 'success', 'fa-desktop', true);</script>";

	public static final String EXCEL_PLAYER_PARAMETER_REQUIRED = "<script> window.parent.Notify('参数[%s]是必选的, 球员名=%s', 'top-right', '5000', 'danger', 'fa-desktop', true);</script>";
	
}
