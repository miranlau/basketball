package com.renyi.basketball.bussness.constants;

public class ResponseParams {

	public static final String HOME = "home";
	public static final String VISITING = "visiting";
	public static final String HOME_TEAM_STATS = "homeTeamStats";
	public static final String VISITING_TEAM_STATS = "visitingTeamStats";
	public static final String HOME_PLAYER_STATS = "homePlayerStats";
	public static final String VISITING_PLAYER_STATS = "visitingPlayerStats";
	public static final String MATCH_SPEAK_LIST = "matchSpeakList";
	public static final String HOME_LINEUP_ID_LIST = "homeLineupIdList";
	public static final String VISITING_LINEUP_ID_LIST = "visitingLineupIdList";
	public static final String EXTENSION = "extension";
	
	public static final String HOME_ID = "homeId";
	public static final String HOME_LOGO = "homeLogo";
	public static final String HOME_NAME = "homeName";
	public static final String HOME_SCORE = "homeScore";
	
	public static final String VISITING_ID = "visitingId";
	public static final String VISITING_LOGO = "visitingLogo";
	public static final String VISITING_NAME = "visitingName";
	public static final String VISITING_SCORE = "visitingScore";
	
	public static final String MATCH_ID = "matchId";
	public static final String LEAGUE_ID = "leagueId";
	public static final String LEAGUE_STAGE_ID = "leagueStageId";
	public static final String STATUS = "status";
	public static final String MATCH_STATUS = "matchStatus";
	public static final String STATUS_NAME = "statusName";
	public static final String MATCH_FORMAT = "matchFormat";
	public static final String MATCH_TIME = "matchTime";
	public static final String MATCH_LIST = "matchList";
	
	public static final String QUARTER_NAME = "quarterName";
	public static final String QUARTER_INDEX = "quarterIndex";
	public static final String QUARTER_REMAINING_TIME = "quarterRemainingTime";
	
	public static final String TEAM = "team";
	public static final String TEAM_ID = "teamId";
	public static final String PLAYER_LIST = "playerList";
	public static final String PLAYER_STATS = "playerStats";
	public static final String PLAYER_STATS_LIST = "playerStatsList";
	public static final String TEAM_STATS = "teamStats";
	public static final String TEAM_MATCH_STATS = "teamMatchStats";
	public static final String TEAM_SEASON_STATS = "teamSeasonStats";
	public static final String TEAM_RANK = "teamRank";
	
	public static final String QUARTER_SCORE_LIST = "quarterScoreList";
	public static final String QUARTER_DATA_LIST = "quarterDataList";
	public static final String HOME_PLAYING_TIME = "homePlayingTime";
	public static final String VISITING_PLAYING_TIME = "visitingPlayingTime";
	
	public static final String IS_OT = "isOT";
	public static final String ID = "id";
	public static final String SPEAK = "speak";
	public static final String CREATE_TIME = "createTime";
	public static final String TIME = "time";
}
