package com.renyi.basketball.bussness.constants;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/11/11 0011.
 */
public interface Setting extends Serializable {

    /** 缓存名称. */
    public final static String CACHE_NAME = "setting";
    /** 缓存Key */
    public static final Integer CACHE_KEY = 0;
}
