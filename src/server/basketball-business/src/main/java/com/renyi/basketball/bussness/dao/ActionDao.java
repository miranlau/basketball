package com.renyi.basketball.bussness.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Performance;

import cn.magicbeans.mybatis.dao.BaseDao;

/**
 * Created by flyong86 on 2016/4/5.
 */
public interface ActionDao extends BaseDao<Action, Long> {

	/**
	 * 通过比赛ID查询最新的Action
	 * 
	 * @param matchId
	 *            Long
	 * @param maxResult
	 *            Integer
	 * @return List<Action>
	 */
	List<Action> queryListByMatchId(Long matchId, Integer maxResult);
	
	/**
	 * 通过比赛ID查询所有的Action
	 * 
	 * @param matchId
	 *            Long
	 * @param maxResult
	 *            Integer
	 * @return List<Action>
	 */
	List<Action> queryListByMatchId(Long matchId);
	
	/**
	 * 查询联赛的射手进球数
	 * 
	 * @param matchIds
	 *            (该联赛的所有比赛ID)
	 * @return
	 */
	List<Action> queryShooterInLeague(@Param("matchIds") List<Long> matchIds);

	/**
	 * 找出有红黄牌的球员
	 * 
	 * @param matchId
	 * @return
	 */
	List<Action> findFoulPlayerId(Long matchId);

	/**
	 * 球员红黄牌数
	 * 
	 * @param matchIds
	 * @param playerId
	 * @param action
	 * @return
	 */
	Integer countNum(List<Long> matchIds, Long playerId, int action);

	/**
	 * 找出双黄牌数
	 * 
	 * @param matchIds
	 * @param playerId
	 * @return
	 */
	Integer countDoubleYellow(List<Long> matchIds, Long playerId);

	/**
	 * 查询球队和球员本次比赛表现
	 * 
	 * @param matchId
	 * @param teamId
	 * @param userId
	 * @return
	 */
	Performance queryPerformance(Long matchId, Long teamId, Long userId);

	/**
	 * 通过联赛ID查询比赛
	 * 
	 * @param leagueId
	 * @return
	 */
	List<Action> queryByLeague(Long leagueId);

	/**
	 *
	 * @param params
	 * @return
	 */
	List<Action> findActions(Map<String, Object> params);

	void updateAction(Action action);

	void insertAction(Action action);

	List<Action> querySavesInLeague(List<Long> matchIds);

	/**
	 * 通过联赛ID查询助攻数据
	 * 
	 * @param leagueId
	 * @return
	 */
	List<Action> queryHelp(Long leagueId, int status);
}
