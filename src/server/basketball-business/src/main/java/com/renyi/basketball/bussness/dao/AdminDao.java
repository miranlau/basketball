package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.Admin;

/**
 * Created by Roy.xiao on 2016/5/25.
 */
public interface AdminDao extends BaseDao<Admin, Long> {
}
