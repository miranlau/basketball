package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.Area;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/20.
 */
public interface AreaDao extends BaseDao<Area, Long> {
    /**
     * 查询所有省
     * @return
     */
    List<Area> queryAllProvince();

    /**
     * 根据Code查询该省的所有城市
     * @param code
     * @return
     */
    List<Area> queryCities(String code);

    /**
     * 根据code查询该城市的所有区
     * @param code
     * @return
     */
    List<Area> queryAreas(String code);
}
