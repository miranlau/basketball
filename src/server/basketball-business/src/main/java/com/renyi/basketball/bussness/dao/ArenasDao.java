package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.Arenas;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
public interface ArenasDao extends BaseDao<Arenas, Long> {
    /**
     * 搜索球场.
     * @param name 名称
     * @param areaCode 地区编号
     * @param chooseStatus 状态
     * @param companyId 公司
     * @return
     */
    List<Arenas> query(String name, String areaCode, List<Integer> chooseStatus, Long companyId);

    /**
     * 查询公司的场馆
     * @param companyId
     * @return
     */
    List<Arenas> queryArenasByCompany(Long companyId);
}
