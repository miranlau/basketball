package com.renyi.basketball.bussness.dao;

import com.renyi.basketball.bussness.po.Bill;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface BillDao extends BaseDao<Bill, Long> {
	Bill queryBill(Long teamId, Long id);
}
