package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.Company;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/25.
 */
public interface CompanyDao extends BaseDao<Company, Long> {
    /**
     * 查询公司(条件查询)
     * @param searchKeyword
     * @param province
     * @return
     */
    List<Company> queryCompanies(String searchKeyword, Integer province);
}
