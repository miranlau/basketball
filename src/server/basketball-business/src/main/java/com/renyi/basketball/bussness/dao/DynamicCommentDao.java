package com.renyi.basketball.bussness.dao;

import com.renyi.basketball.bussness.po.DynamicComment;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface DynamicCommentDao extends BaseDao<DynamicComment, Long> {

	void insertComment(DynamicComment entity);

	void deleteByDynamicId(Long dynamicId);
}
