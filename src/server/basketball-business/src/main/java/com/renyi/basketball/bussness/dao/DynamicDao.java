package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.UserDynamic;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/6/1.
 */
public interface DynamicDao extends BaseDao<UserDynamic, Long> {
    void insertDynamic(UserDynamic entity);

    /**
     * 查询好友动态
     * @param friends
     * @return
     */
    List<UserDynamic> queryFriendDynamic(List<Long> friends);

    void updateDynamic(UserDynamic userDynamic);

    List<UserDynamic> findAllDynamic();

    void deleteById(Long dynamicId);

    List<UserDynamic> findMyDynamicList(Long userId);
}
