package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.BaseEntity;
import com.renyi.basketball.bussness.po.Follow;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface FollowDao extends BaseDao<Follow, Long> {
	/**
	 * 插入关注记录
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 */
	void insertFollow(Long uid, Long lid, Integer type);
	/**
	 * 删除关注记录
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 */
	void deletefollow(Long uid, Long lid, Integer type);
	/**
	 * 查询关注记录
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 */
	List<Follow> queryfollow(Long uid, Long lid, Integer type);

	/**
	 * 根据类型查出相对应的PO
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 * @return
	 */
	List<? extends BaseEntity> queryEntity(Long uid, Long lid, Integer type);

}
