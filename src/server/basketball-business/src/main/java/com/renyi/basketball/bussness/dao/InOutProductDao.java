package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.InOutProduct;

/**
 * Created by Administrator on 2016/7/6 0006.
 */
public interface InOutProductDao extends BaseDao<InOutProduct, Long> {
}
