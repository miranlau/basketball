package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.League;

import cn.magicbeans.mybatis.dao.BaseDao;

/**
 * Created by flyong86 on 2016/4/5.
 */
public interface LeagueDao extends BaseDao<League, Long> {

	/**
	 * 关键字分页查询赛事
	 * 
	 * @param searchKeyword
	 *            status
	 * @return
	 */
	List<League> queryLeagues(String searchKeyword, List<Integer> status, Long companyId);

	/**
	 * 更新联赛信息
	 * 
	 * @param league
	 */
	void updateLeague(League league);

	/**
	 * 查询该状态的赛事列表
	 * 
	 * @param signing
	 * @return
	 */
	List<League> quertExpireLeague(String dateTime, Integer signing);

	/**
	 * 更新赛事状态
	 * 
	 * @param id
	 * @param code
	 */
	void updateStatus(Long id, int code) throws Exception;

	/**
	 * 所有联赛的ID
	 * 
	 * @return
	 */
	List<Long> queryAllIds();
}
