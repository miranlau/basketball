package com.renyi.basketball.bussness.dao;

import com.renyi.basketball.bussness.po.LeagueInfoComment;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface LeagueInfoCommentDao extends BaseDao<LeagueInfoComment, Long> {
	@Override
	LeagueInfoComment insert(LeagueInfoComment entity) throws Exception;
}
