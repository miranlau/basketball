package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.LeagueInfo;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface LeagueInfoDao extends BaseDao<LeagueInfo, Long> {
	/**
	 * 查询赛事动态列表
	 * 
	 * @param leagueId
	 * @return
	 */
	List<LeagueInfo> queryInfoByLeagueId(Long leagueId);

	/**
	 * 添加(阅读数、点赞数、评论数)
	 * 
	 * @param infoId
	 * @param num
	 */
	void addNum(Long newNum, Long infoId, String num);
}
