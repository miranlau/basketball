package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.LeagueStage;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface LeagueStageDao  extends BaseDao<LeagueStage, Integer> {
	
	List<LeagueStage> queryAll();
	
}
