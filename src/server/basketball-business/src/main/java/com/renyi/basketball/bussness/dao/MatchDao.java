package com.renyi.basketball.bussness.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.common.MatchStatus;
import com.renyi.basketball.bussness.po.Match;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface MatchDao extends BaseDao<Match, Long> {
	/**
	 * 查询全部比赛
	 * 
	 * @param searchKeyword
	 * @return
	 */
	List<Match> queryMatches(String searchKeyword);

	/**
	 * 通过用户Id查询比赛
	 * 
	 * @param userId
	 * @return
	 */
	List<Match> queryByUser(Long userId);
	/**
	 * 删除比赛
	 * 
	 * @param id
	 *            比赛Id
	 */
	void deleteById(Long id);

	/**
	 * 查询比赛详细
	 * 
	 * @param id
	 *            比赛id
	 * @return
	 */
	Match queryById(Long id);

	/**
	 * 更新比赛信息
	 * 
	 * @param match
	 *            比赛信息
	 */
	void updateMatch(Match match);

	/**
	 * 
	 * 查询赛事比赛列表
	 * 
	 * @param id
	 * @return
	 */
	List<Match> queryLeagueMatch(Long id);

	/**
	 * * 更新比赛状态.
	 * 
	 * @param matchId
	 *            - 比赛ID
	 * @param status
	 *            - 状态
	 * @throws Exception
	 */
	void updateStatus(Long matchId, MatchStatus status) throws Exception;

	/**
	 * 查询过期的比赛
	 * 
	 * @param status
	 *            比赛状态
	 * @param expr
	 *            比较间隔
	 * @return 比赛列表
	 */
	List<Match> queryExpireMatch(Integer status, Long expr);

	/**
	 * 查询球队比赛
	 * 
	 * @param teamId
	 * @param closeNum
	 * @return
	 */
	List<Match> queryMatchByTeamId(Long teamId, Integer closeNum);
	
	/**
	 * 查询球队的赛事比赛
	 * 
	 * @param teamId
	 * @param leagueId
	 * @return
	 */
	List<Match> queryMatchByTeamIdAndLeagueId(Long teamId, Long leagueId);

	/**
	 * 分页查询比赛
	 * 
	 * @param teanName
	 *            球队名字
	 * @return
	 */
	List<Match> queryMatchesPage(String teanName, String leagueName, String start, String end);

	/**
	 * 查询附近比赛
	 * 
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	List<Match> queryNearMatches(Long miles, Double longitude, Double latitude);

	/**
	 * 查询我的比赛时间安排
	 * 
	 * @param userid
	 * @return
	 */
	List<Match> findMyMatchDate(Long userid);

	/**
	 * 已完成的比赛
	 * 
	 * @param teamId
	 * @return
	 */
	List<Match> queryDoneMatches(Long teamId);
	
	/**
	 * 查询要生成数据统计的比赛
	 * 
	 * @return List<Match>
	 */
	List<Match> queryNeedStatsMatches();
	
	/**
	 * 查询两只球队最近的交战史
	 * 
	 * @param teamId1
	 *            Long
	 * @param taemId2
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryMatchHistoryByTeamIds(Long teamId1, Long taemId2, Integer start, Integer size);
	
	/**
	 * 查询球队未来的比赛
	 * 
	 * @param teamId
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryFutureMatches(Long teamId, Integer start, Integer size);
	
	/**
	 * 查询球队过去的比赛
	 * 
	 * @param teamId
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryPassMatches(Long teamId, Integer start, Integer size);
	
	/**
	 * 更新数据统计的状态
	 * 
	 * @param id Long
	 * @param isGenerateStats Boolean
	 */
	void updateStatsStatus(Long id, Integer statsStatus);
	
	/**
	 * 查询记分员的比赛
	 * 
	 * @param recorderId
	 *            Long
	 * @param status
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryMatchsNeedRecorder(Long recorderId);
	
}
