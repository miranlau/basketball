package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.MatchRound;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface MatchRoundDao  extends BaseDao<MatchRound, Integer> {
	
	List<MatchRound> queryAll();
	
}
