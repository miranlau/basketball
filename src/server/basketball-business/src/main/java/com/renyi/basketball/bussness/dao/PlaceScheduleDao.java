package com.renyi.basketball.bussness.dao;

import java.util.HashMap;
import java.util.List;

import com.renyi.basketball.bussness.po.PlaceSchedule;

import cn.magicbeans.mybatis.dao.BaseDao;

/**
 * Created by Roy.xiao on 2016/7/2.
 */
public interface PlaceScheduleDao extends BaseDao<PlaceSchedule, Long> {
    List<HashMap<String,String>> querySchedule(int year, int month, int day, Long placeId);
}
