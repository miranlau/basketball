package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.Place;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
public interface PlacesDao extends BaseDao<Place, Long> {
    /**
     * 查询球馆的场地列表
     * @param arenasId
     * @return
     */
    List<Place> queryPlaceByArenasId(Long arenasId);
}
