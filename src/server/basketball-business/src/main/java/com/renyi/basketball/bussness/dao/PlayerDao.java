package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.Player;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface PlayerDao extends BaseDao<Player, Long> {
	
	Player queryPlayerById(Long playerId);

	List<Player> queryPlayerList(List<Long> playerIds, Long teamId);
	
	List<Player> queryPlayerListByTeamId(Long teamId);

	List<Player> queryPlayerListByName(String name);
	
}
