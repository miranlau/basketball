package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.PlayerMatchStats;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface PlayerMatchStatsDao extends BaseDao<PlayerMatchStats, Long> {

	void batchSave(List<PlayerMatchStats> playerMatchStatsList);
	
	List<PlayerMatchStats> queryByMatchId(Long matchId);
	
	List<PlayerMatchStats> queryByPlayerIdAndLeague(Long playerId, Long leagueId);
	
	void deleteByMatchId(Long matchId);
	
}
