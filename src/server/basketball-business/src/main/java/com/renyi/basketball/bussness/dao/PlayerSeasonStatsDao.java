package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.PlayerSeasonStats;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface PlayerSeasonStatsDao extends BaseDao<PlayerSeasonStats, Long> {

	/**
	 * 按阶段查询球员赛季的技术统计
	 * 
	 * @param playerId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return TeamSeasonStats
	 */
	PlayerSeasonStats queryByPlayerIdAndLeague(Long playerId, Long leagueId, Long leagueStageId);
	
	/**
	 * 按阶段查询球队所有赛季的技术统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return PlayerSeasonStats
	 */
	List<PlayerSeasonStats> queryByTeamIdAndLeague(Long teamId, Long leagueId, Long leagueStageId);

}
