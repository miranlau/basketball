package com.renyi.basketball.bussness.dao;

import java.util.HashMap;
import java.util.List;

import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.po.Product;

import cn.magicbeans.mybatis.dao.BaseDao;

/**
 * Created by Administrator on 2016/7/6 0006.
 */
public interface ProductDao extends BaseDao<Product, Long> {
	/**
	 * 商品分页查询
	 * 
	 * @param searchKeyword
	 * @param areansId
	 * @param companyId
	 * 			@return
	 */
	List<Product> query(String searchKeyword, Long companyId, Long areansId);

	/**
	 * 插入商品和球场的关联
	 * 
	 * @param areansId
	 * @param areansId
	 */
	void insertProductArenas(Long productId, Long areansId);

	/**
	 * 查询商品的库存和销量
	 * 
	 * @param productId
	 * @param areansId
	 * @param companyId
	 * @return
	 */
	List<HashMap> queryGoods(Long productId, Long areansId, Long companyId);

	/**
	 * 删除商品球场的关联
	 * 
	 * @param productId
	 * @param areansId
	 */
	void deletesPA(Long productId, Long areansId);

	/**
	 * 进出货
	 */
	void inoutproduct(InOutProduct inoutproduct);

	/**
	 * 更新销量库存
	 * 
	 * @param inoutproduct
	 */
	void updatePA(InOutProduct inoutproduct);
}
