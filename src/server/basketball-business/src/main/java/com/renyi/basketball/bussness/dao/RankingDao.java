package com.renyi.basketball.bussness.dao;

import com.renyi.basketball.bussness.po.Ranking;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface RankingDao extends BaseDao<Ranking, Long> {

	Ranking queryTeamRanking(Long leagueId, Long leagueStageId);

}
