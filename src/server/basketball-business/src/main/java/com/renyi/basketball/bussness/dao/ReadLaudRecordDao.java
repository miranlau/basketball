package com.renyi.basketball.bussness.dao;

import com.renyi.basketball.bussness.po.ReadLaud;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface ReadLaudRecordDao extends BaseDao<ReadLaud, Long> {
	/**
	 * 增加点赞或阅读次数
	 * 
	 * @param id
	 * @param dynamicId
	 * @param numTypeRead
	 */
	void addNum(Long id, Long dynamicId, Integer numTypeRead);

	/**
	 * 查询用户记录条数
	 * 
	 * @param id
	 * @param dynamicId
	 * @param type
	 * @return
	 */
	Integer count(Long id, Long dynamicId, Integer type);

	void deleteByDynamicId(Long dynamicId);
}
