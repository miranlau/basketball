package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.Season;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface SeasonDao extends BaseDao<Season, Long> {
	List<Season> queryByLeague(Long leagueId);

	List<Season> queryByIds(List<Long> venueIds);
	
	List<Season> queryByName(Long leagueId, String name);
}
