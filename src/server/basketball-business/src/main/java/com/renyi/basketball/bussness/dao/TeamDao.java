package com.renyi.basketball.bussness.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.mybatis.dao.BaseDao;

/**
 * Created by flyong86 on 2016/5/8.
 */
public interface TeamDao extends BaseDao<Team, Long> {
	/**
	 * 创建球队
	 *
	 * @param team
	 * @param leaderId
	 */
	void create(Team team, Long leaderId);

	/**
	 * 删除球队
	 *
	 * @param userId
	 * @param teamId
	 */
	void deleteTeam(Long userId, Long teamId);

	/**
	 * 查询球员在该球队的记录
	 *
	 * @param userId
	 * @param teamId
	 * @return
	 */
	int queryRecord(Long userId, Long teamId);
	
	/**
	 * 根据多个id，查询球队
	 *
	 * @param userId
	 * @param teamId
	 * @return
	 */
	List<Team> queryByIds(List<Long> teamIds);

	/**
	 * 更新球员球衣号码
	 *
	 * @param userId
	 * @param teamId
	 * @param number
	 */
	void updateNumber(Long userId, Long teamId, int number);

	/**
	 * 解散队伍
	 *
	 * @param teamId
	 */
	void dissolve(Long teamId);

	/**
	 * 删除队伍信息user_team
	 *
	 * @param teamId
	 */
	@Override
	void delete(Long teamId);

	/**
	 * 删除球队关注关联信息
	 *
	 * @param lid
	 */
	void deleteFollow(Long lid);

	List<Team> queryAll();

	/**
	 * 查询球衣号码
	 *
	 * @param userId
	 * @param teamId
	 * @return
	 */
	int queryNumber(Long userId, Long teamId);

	/**
	 * 更具用户id查询用户的所有比赛
	 *
	 * @param userId
	 * @return
	 */
	List<Team> queryByUser(Long userId);

	/**
	 * 查询球队列表.
	 *
	 * @param keyword
	 *            - 搜索关键字
	 * @param isdel
	 *            - 是否删除
	 * @return 球队列表
	 */
	List<Team> queryTeams(String keyword, Boolean isdel);
	
	Team queryTeam(Long teamId);

	/**
	 * 查询用户创建的球队数量
	 *
	 * @param userId
	 * @return
	 */
	int queryLeader(Long userId);

	/**
	 * 查询队伍所有的球员号码
	 *
	 * @param teamId
	 * @return
	 */
	List<Integer> queryAllNumber(Long teamId);

	/**
	 * 更新球队 赢场、平场、输场 和 等级
	 *
	 * @param team
	 */
	void updateTeam(Team team);

	/**
	 * 根据球队ID与号码查询球员列表.
	 *
	 * @param teamId
	 *            - 球队ID
	 * @param num
	 *            - 号码
	 * @return 球员集合
	 */
	List<Integer> queryPlayersByNumber(Long teamId, Integer num);

	/**
	 * 更新球队球员号码.
	 *
	 * @param teamId
	 *            球队ID
	 * @param fromNum
	 *            原号码
	 * @param toNum
	 *            新号码
	 */
	void updatePlayerNum(Long teamId, int fromNum, int toNum);

	/**
	 * 找出该球队的球员
	 * 
	 * @param id
	 * @param includeLeader
	 *            是否包含队长
	 * @return
	 */
	List<User> findPlayer(Long id, Boolean includeLeader);

	List<Map<Integer, Integer>> queryNoLeader();

	void updateNoLeader(Map<Integer, Integer> result);

	List<Team> queryStillNoLead();

	User findOnePlayer(Long teamId);

	void updateLeader(Long teamId, Long userId);

	void updateLeaders(Long userId, Long teamId);

	/**
	 * 判断球队是否已经存在
	 * 
	 * @return
	 * @param team
	 */
	boolean hasExist(Team team);

	void saveUT(Long teamId, Boolean isLeader, Integer member);

	/**
	 *
	 * @param date
	 * @return
	 */
	List<Team> queryThreeDay(Date date);

	/**
	 * 查询球队某个状态的比赛
	 * 
	 * @param teamId
	 * @param stauts
	 */
	List<Match> queryMatch(Long teamId, Integer stauts);

	/**
	 * 球队里面添加球员.
	 * 
	 * @param uid
	 *            球员ID
	 * @param tid
	 *            球队ID
	 * @param role
	 *            角色
	 */
	void addUser(Long uid, Long tid, Integer role);

	/**
	 * 球队里面删除球员.
	 * 
	 * @param uid
	 *            球员ID
	 * @param tid
	 *            球队ID
	 */
	void removeUser(Long uid, Long tid);

	/**
	 * 更新球员角色.
	 * 
	 * @param uid
	 *            球员ID
	 * @param tid
	 *            球队ID
	 * @param role
	 *            角色
	 */
	void updateUserRole(Long uid, Long tid, Integer role);

	Team queryByName(String name);
}
