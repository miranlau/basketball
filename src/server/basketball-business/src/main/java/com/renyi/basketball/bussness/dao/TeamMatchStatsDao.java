package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.TeamMatchStats;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface TeamMatchStatsDao extends BaseDao<TeamMatchStats, Long> {
	
	/**
	 * 查询比赛的技术统计
	 * 
	 * @param matchId
	 *            Long
	 * 
	 * @return List<TeamMatchStats>
	 */
	List<TeamMatchStats> queryByMatchId(Long matchId);
	
	void deleteByMatchId(Long matchId);

	/**
	 * 查询球队赛事中每场比赛的技术统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Integer
	 * 
	 * @return List<TeamMatchStats>
	 */
	List<TeamMatchStats> queryByTeamIdAndLeague(Long teamId, Long leagueId, Long leagueStageId);
	
}
