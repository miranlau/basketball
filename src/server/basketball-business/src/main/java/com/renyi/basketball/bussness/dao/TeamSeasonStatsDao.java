package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.dto.TeamRankingDto;
import com.renyi.basketball.bussness.po.TeamSeasonStats;

import cn.magicbeans.mybatis.dao.BaseDao;

public interface TeamSeasonStatsDao extends BaseDao<TeamSeasonStats, Long> {

	/**
	 * 按阶段查询球队赛季的技术统计
	 * 
	 * @param leagueId
	 *            Long
	 * @param teamId
	 *            Long
	 * @return TeamSeasonStats
	 */
	TeamSeasonStats queryByTeamIdAndLeague(Long teamId, Long leagueId, Long leagueStageId);

	/**
	 * 按阶段查询球队的胜负排名
	 * 
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return TeamSeasonStats
	 */
	public List<TeamRankingDto> queryWinLossRanking(Long leagueId, Long leagueStageId);

}
