package com.renyi.basketball.bussness.dao;

import java.util.List;

import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.mybatis.dao.BaseDao;

/**
 * Created by flyong86 on 2016/4/5.
 */
public interface UserDao extends BaseDao<User, Long> {
	/**
	 * 查询球队的所有球员
	 * 
	 * @param teamId
	 * @return
	 */
	List<User> queryByTeam(Long teamId);

	/**
	 * 查询属性
	 * 
	 * @param userId
	 * @return
	 */
	String queryRadar(Long userId);

	/**
	 * 查询球员数据.
	 * 
	 * @param userId
	 *            - 用户ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员
	 */
	User queryPlayer(Long userId, Long teamId);

	/**
	 * 查询球员数据.
	 * 
	 * @param userIds
	 *            - 用户ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员
	 */
	List<User> queryPlayerList(List<Long> userIds, Long teamId);

	/**
	 * 通过名字模糊查询用户
	 * 
	 * @param name
	 * @return
	 */
	List<User> queryUserByName(String name);

	/**
	 * 通过球队名称模糊查询用户
	 * 
	 * @param searchKeyword
	 * @return
	 */
	List<User> queryUserByTeamName(String searchKeyword);
	/**
	 * 通过手机号码模糊查询用户
	 * 
	 * @param searchKeyword
	 * @return
	 */
	List<User> queryUserByMobile(String searchKeyword);
	/**
	 * 更新user的总赛数、总得分、平局、等级
	 * 
	 * @param user
	 */
	void updateUser(User user);

	/**
	 * 电话号码存在
	 * 
	 * @return
	 * @param user
	 */
	boolean mobileHasExist(User user);

	/**
	 * 用户是否已经加入该球队（导入时，判断队名和用户名都相同）
	 * 
	 * @return
	 */
	boolean hasSameTeam(User user, Team team);

	/**
	 * 获取最大的虚拟号码
	 * 
	 * @return
	 */
	String getMaxMobile();

	/**
	 * 查询同队同名
	 * 
	 * @param user
	 * @param team
	 * @return
	 */
	Long queryByTeam(User user, Team team);
	/**
	 * 更新球员比赛数据
	 * 
	 * @param id
	 * @param score
	 * @param win
	 * @param lose
	 * @param help
	 * @param backboard
	 * @param two_in
	 * @param two_out
	 * @param three_in
	 * @param three_out
	 * @param Freethrow_in
	 * @param Freethrow_out
	 * @param cover
	 * @param steals
	 */
	void updatePlayer(Long id, Integer score, Integer win, Integer lose, Integer help, Integer backboard,
			Integer two_in, Integer two_out, Integer three_in, Integer three_out, Integer Freethrow_in,
			Integer Freethrow_out, Integer cover, Integer steals, Integer anError);

	/**
	 * 更新赢场
	 * 
	 * @param id
	 * @param win
	 */
	void updateWin(Long id, int win);

	/**
	 * 更新输场
	 * 
	 * @param id
	 * @param lose
	 */
	void updateLose(Long id, int lose);

}
