package com.renyi.basketball.bussness.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import com.renyi.basketball.bussness.po.Venue;

import java.util.List;

public interface VenueDao extends BaseDao<Venue, Long> {
    List<Venue> findVenuesPager(String name);
    List<Venue> queryByIds(List<Long> venueIds);
}
