package com.renyi.basketball.bussness.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.ActionDao;
import com.renyi.basketball.bussness.dao.mapper.ActionMapper;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Performance;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class ActionDaoImpl extends BaseDaoImpl<Action, Long> implements ActionDao {
    @Resource
    private ActionMapper mapper;
    @Resource
    public void setBaseMapper(ActionMapper mapper) {
        super.setBaseMapper(mapper);
    }

    @Override
   	public List<Action> queryListByMatchId(Long matchId) {
   		return mapper.queryListByMatchId(matchId, null);
   	}
    
    @Override
	public List<Action> queryListByMatchId(Long matchId, Integer maxResult) {
		return mapper.queryListByMatchId(matchId, maxResult);
	}
    
    @Override
    public List<Action> queryShooterInLeague(@Param("matchIds") List<Long> matchIds) {
        if (matchIds != null && matchIds.size() > 0) {
            return mapper.queryShooterInLeague(matchIds);
        }
        return null;
    }

    @Override
    public List<Action> findFoulPlayerId(Long matchId) {
        return mapper.findFoulPlayerId(matchId);
    }

    @Override
    public Integer countNum(List<Long> matchIds, Long playerId, int action) {
        return mapper.countNum(matchIds,playerId,action);
    }

    @Override
    public Integer countDoubleYellow(List<Long> matchIds, Long playerId) {
        if (matchIds == null) {
            return null;
        }
        Integer count = 0;
        for (Long matchId : matchIds) {
            if (mapper.countDoubleYellow(matchId,playerId) >= 2) {
                count++;
            }
        }
        return count;
    }

    @Override
    public Performance queryPerformance(Long matchId, Long teamId, Long userId) {
    	HashMap map = new HashMap();
        map.put("matchId",matchId);
        map.put("teamId",teamId);
        map.put("userId",userId);
        return mapper.queryPerformance(map);
    }

    @Override
    public List<Action> queryByLeague(Long leagueId) {
        return mapper.queryByLeague(leagueId);
    }

    @Override
    public List<Action> findActions(Map<String, Object> params) {
        return mapper.findActions(params);
    }

    @Override
    public void updateAction(Action action) {
        mapper.updateAction(action);
    }

    @Override
    public void insertAction(Action action) {
        mapper.insertAction(action);
    }

    @Override
    public List<Action> querySavesInLeague(List<Long> matchIds) {
        return mapper.querySavesInLeague(matchIds);
    }

    @Override
    public List<Action> queryHelp(Long leagueId,int status) {
        return mapper.queryHelp(leagueId,status);
    }

}
