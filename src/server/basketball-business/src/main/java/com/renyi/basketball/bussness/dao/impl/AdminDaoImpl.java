package com.renyi.basketball.bussness.dao.impl;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import com.renyi.basketball.bussness.dao.AdminDao;
import com.renyi.basketball.bussness.dao.mapper.AdminMapper;
import com.renyi.basketball.bussness.po.Admin;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class AdminDaoImpl extends BaseDaoImpl<Admin,Long>implements AdminDao{
    @Resource
    private AdminMapper adminMapper;

    @Resource
    public void setBaseMapper(AdminMapper adminMapper) {
        super.setBaseMapper(adminMapper);
    }
}
