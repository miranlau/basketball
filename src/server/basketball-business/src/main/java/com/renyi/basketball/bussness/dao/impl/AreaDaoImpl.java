package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.AreaDao;
import com.renyi.basketball.bussness.dao.mapper.AreaMapper;
import com.renyi.basketball.bussness.po.Area;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class AreaDaoImpl extends BaseDaoImpl<Area, Long> implements AreaDao {
	@Autowired
	private AreaMapper areaMapper;

	@Autowired
	public void setBaseMapper(AreaMapper areaMapper) {
		super.setBaseMapper(areaMapper);
	}

	@Override
	public List<Area> queryAllProvince() {
		return areaMapper.queryAllProvince();
	}

	@Override
	public List<Area> queryCities(String code) {
		// 截取省份Code
		String queryCode = null;
		if (code != null) {
			queryCode = code.substring(0, 2) + "%";

		}
		return areaMapper.queryCities(queryCode, code);
	}

	@Override
	public List<Area> queryAreas(String code) {
		String queryCode = null;
		if (code != null) {
			queryCode = code.substring(0, 4) + "%";

		}
		return areaMapper.queryAreas(queryCode, code);
	}
}
