package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.ArenasDao;
import com.renyi.basketball.bussness.dao.mapper.ArenasMapper;
import com.renyi.basketball.bussness.po.Arenas;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
@Repository
public class ArenasDaoImpl extends BaseDaoImpl<Arenas, Long>implements ArenasDao {
    @Resource
    private ArenasMapper arenasMapper;

    @Resource
    public void setBaseMapper(ArenasMapper arenasMapper) {
        super.setBaseMapper(arenasMapper);
    }

    @Override
    public List<Arenas> query(String name, String areaCode, List<Integer> chooseStatus, Long companyId) {
        return arenasMapper.query(name, areaCode, chooseStatus, companyId);
    }

    @Override
    public List<Arenas> queryArenasByCompany(Long companyId) {
        return arenasMapper.queryArenasByCompany(companyId);
    }
}
