package com.renyi.basketball.bussness.dao.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.BillDao;
import com.renyi.basketball.bussness.dao.mapper.BillMapper;
import com.renyi.basketball.bussness.po.Bill;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class BillDaoImpl extends BaseDaoImpl<Bill, Long> implements BillDao {
	@Resource
	private BillMapper mapper;

	@Resource
	public void setBaseMapper(BillMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public Bill queryBill(Long teamId, Long matchId) {
		return mapper.queryBill(teamId, matchId);
	}
}
