package com.renyi.basketball.bussness.dao.impl;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import com.renyi.basketball.bussness.dao.CompanyDao;
import com.renyi.basketball.bussness.dao.mapper.CompanyMapper;
import com.renyi.basketball.bussness.po.Company;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class CompanyDaoImpl extends BaseDaoImpl<Company, Long> implements CompanyDao {
	@Resource
	private CompanyMapper companyMapper;

	@Resource
	public void setBaseMapper(CompanyMapper companyMapper) {
		super.setBaseMapper(companyMapper);
	}

	@Override
	public List<Company> queryCompanies(String searchKeyword, Integer province) {
		return companyMapper.queryCompanies(searchKeyword, province);
	}
}
