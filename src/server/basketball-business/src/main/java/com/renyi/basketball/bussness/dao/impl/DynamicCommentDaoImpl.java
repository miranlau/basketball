package com.renyi.basketball.bussness.dao.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.DynamicCommentDao;
import com.renyi.basketball.bussness.dao.mapper.DynamicCommentMapper;
import com.renyi.basketball.bussness.po.DynamicComment;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class DynamicCommentDaoImpl extends BaseDaoImpl<DynamicComment, Long> implements DynamicCommentDao {
	@Resource
	private DynamicCommentMapper mapper;

	@Resource
	public void setBaseMapper(DynamicCommentMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public void insertComment(DynamicComment entity) {
		mapper.insertComment(entity);
	}

	@Override
	public void deleteByDynamicId(Long dynamicId) {
		mapper.deleteByDynamicId(dynamicId);
	}
}
