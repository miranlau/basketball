package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.DynamicDao;
import com.renyi.basketball.bussness.dao.mapper.UserDynamicMapper;
import com.renyi.basketball.bussness.po.UserDynamic;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class DynamicDaoImpl extends BaseDaoImpl<UserDynamic, Long> implements DynamicDao {
	@Resource
	private UserDynamicMapper mapper;

	@Resource
	public void setBaseMapper(UserDynamicMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public void insertDynamic(UserDynamic entity) {
		mapper.insertDynamic(entity);
	}

	@Override
	public List<UserDynamic> queryFriendDynamic(List<Long> friends) {
		if (friends == null || friends.size() == 0) {
			return null;
		}
		return mapper.queryFriendDynamic(friends);
	}

	@Override
	public void updateDynamic(UserDynamic userDynamic) {
		mapper.updateDynamic(userDynamic);
	}

	@Override
	public List<UserDynamic> findAllDynamic() {
		return mapper.findAllDynamic();
	}

	@Override
	public void deleteById(Long dynamicId) {
		mapper.deleteById(dynamicId);
	}

	@Override
	public List<UserDynamic> findMyDynamicList(Long userId) {
		return mapper.findMyDynamicList(userId);
	}
}
