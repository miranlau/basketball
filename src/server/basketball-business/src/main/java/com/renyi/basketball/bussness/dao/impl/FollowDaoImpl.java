package com.renyi.basketball.bussness.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.FollowDao;
import com.renyi.basketball.bussness.dao.LeagueDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dao.mapper.FollowMapper;
import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Team;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class FollowDaoImpl extends BaseDaoImpl<Follow, Long> implements FollowDao {
	@Resource
	private FollowMapper mapper;
	@Resource
	private UserDao userDao;
	@Resource
	private TeamDao teamDao;
	@Resource
	private LeagueDao leagueDao;
	@Resource
	public void setBaseMapper(FollowMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public void insertFollow(Long uid, Long lid, Integer type) {
		mapper.insertFollow(uid, lid, type);
	}

	@Override
	public void deletefollow(Long uid, Long lid, Integer type) {
		mapper.deletefollow(uid, lid, type);
	}

	@Override
	public List<Follow> queryfollow(Long uid, Long lid, Integer type) {
		return mapper.queryfollow(uid, lid, type);
	}

	@Override
	public List queryEntity(Long uid, Long lid, Integer type) {
		List<Follow> follows = queryfollow(uid, lid, type);
		List result = new ArrayList<>();
		if (follows != null) {
			if (type == Follow.PLAYER_FOLLOW) {// 球员表
				for (Follow f : follows) {
					result.add(userDao.find(f.getLeagueId()));
				}
			} else if (type == Follow.TEAM_FOLLOW) {// 球队
				Team team;
				for (Follow f : follows) {
					team = teamDao.find(f.getLeagueId());
					if (team != null) {
						result.add(team);
					}
				}
			} else if (type == Follow.LEAGUE_FOLLOW) {// 赛事
				League league;
				for (Follow f : follows) {
					league = leagueDao.find(f.getLeagueId());
					if (league != null) {
						result.add(league);
					}
				}
			}
		}
		return result;
	}
}
