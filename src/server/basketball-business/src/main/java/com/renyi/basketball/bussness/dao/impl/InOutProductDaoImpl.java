package com.renyi.basketball.bussness.dao.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.InOutProductDao;
import com.renyi.basketball.bussness.dao.mapper.InOutProductMapper;
import com.renyi.basketball.bussness.po.InOutProduct;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class InOutProductDaoImpl extends BaseDaoImpl<InOutProduct, Long> implements InOutProductDao {

	@Resource
	public void setBaseMapper(InOutProductMapper inOutProductMapper) {
		super.setBaseMapper(inOutProductMapper);
	}

	@Resource
	private InOutProductMapper inOutProductMapper;

}
