package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.LeagueDao;
import com.renyi.basketball.bussness.dao.mapper.LeagueMapper;
import com.renyi.basketball.bussness.po.League;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class LeagueDaoImpl extends BaseDaoImpl<League, Long> implements LeagueDao {
	@Autowired
	private LeagueMapper mapper;
	@Autowired
	public void setBaseMapper(LeagueMapper mapper) {
		super.setBaseMapper(mapper);
	}
	@Override
	public List<League> queryLeagues(String searchKeyword, List<Integer> status, Long companyId) {
		return mapper.queryLeagues(searchKeyword, status, companyId);
	}

	@Override
	public void updateLeague(League league) {
		mapper.updateById(league);
	}

	@Override
	public List<League> quertExpireLeague(String dateType, Integer signing) {
		return mapper.quertExpireLeague(dateType, signing);
	}

	@Override
	public void updateStatus(Long id, int code) throws Exception {
		League league = new League();
		league.setId(id);
		league.setStatus(code);
		// league.setUpdateTime(new SimpleDateFormat("yyyy-MM-dd
		// HH:mm:ss").format(new Date()));
		super.update(league);
	}

	@Override
	public List<Long> queryAllIds() {
		return mapper.queryAllIds();
	}
}
