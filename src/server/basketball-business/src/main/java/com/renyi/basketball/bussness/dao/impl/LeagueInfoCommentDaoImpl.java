package com.renyi.basketball.bussness.dao.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.LeagueInfoCommentDao;
import com.renyi.basketball.bussness.dao.mapper.LeagueInfoCommentMapper;
import com.renyi.basketball.bussness.po.LeagueInfoComment;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class LeagueInfoCommentDaoImpl extends BaseDaoImpl<LeagueInfoComment, Long> implements LeagueInfoCommentDao {
	@Resource
	private LeagueInfoCommentMapper leagueInfoCommentMapper;

	@Resource
	public void setBaseMapper(LeagueInfoCommentMapper leagueInfoCommentMapper) {
		super.setBaseMapper(leagueInfoCommentMapper);
	}

	@Override
	public LeagueInfoComment insert(LeagueInfoComment entity) throws Exception {
		leagueInfoCommentMapper.newinsert(entity);
		return entity;
	}
}
