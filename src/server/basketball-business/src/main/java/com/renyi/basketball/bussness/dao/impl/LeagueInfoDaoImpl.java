package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.LeagueInfoDao;
import com.renyi.basketball.bussness.dao.mapper.LeagueInfoMapper;
import com.renyi.basketball.bussness.po.LeagueInfo;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class LeagueInfoDaoImpl extends BaseDaoImpl<LeagueInfo, Long> implements LeagueInfoDao {
	@Resource
	private LeagueInfoMapper leagueInfoMapper;

	@Resource
	public void setBaseMapper(LeagueInfoMapper leagueInfoMapper) {
		super.setBaseMapper(leagueInfoMapper);
	}

	@Override
	public List<LeagueInfo> queryInfoByLeagueId(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		return leagueInfoMapper.queryInfoByLeagueId(leagueId);
	}

	@Override
	public void addNum(Long newNum, Long infoId, String num) {

		leagueInfoMapper.addNum(newNum, infoId, num);
	}
}
