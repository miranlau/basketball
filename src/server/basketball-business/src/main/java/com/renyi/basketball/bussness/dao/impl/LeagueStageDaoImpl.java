package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.LeagueStageDao;
import com.renyi.basketball.bussness.dao.mapper.LeagueStageMapper;
import com.renyi.basketball.bussness.po.LeagueStage;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class LeagueStageDaoImpl extends BaseDaoImpl<LeagueStage, Integer> implements LeagueStageDao {
	@Resource
	private LeagueStageMapper mapper;

	@Resource
	public void setBaseMapper(LeagueStageMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public List<LeagueStage> queryAll() {
		return mapper.queryAll();
	}

}
