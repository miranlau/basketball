package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.common.MatchStatus;
import com.renyi.basketball.bussness.dao.MatchDao;
import com.renyi.basketball.bussness.dao.mapper.MatchMapper;
import com.renyi.basketball.bussness.po.Match;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class MatchDaoImpl extends BaseDaoImpl<Match, Long> implements MatchDao {
	@Autowired
	private MatchMapper mapper;

	@Autowired
	public void setBaseMapper(MatchMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public Match insert(Match match) {
		mapper.insert(match);
		return match;
	}

	@Override
	public List<Match> queryMatches(String searchKeyword) {
		return mapper.queryMatches(searchKeyword);
	}

	@Override
	public List<Match> queryByUser(Long userId) {
		return mapper.queryByUser(userId);
	}

	@Override
	public void deleteById(Long id) {
		mapper.deleteById(id);
	}

	@Override
	public Match queryById(Long id) {
		return mapper.queryById(id);
	}

	@Override
	public void updateMatch(Match match) {
		mapper.updateMatch(match);
	}

	@Override
	public List<Match> queryLeagueMatch(Long id) {
		return mapper.queryLeagueMatch(id);
	}

	@Override
	public void updateStatus(Long matchId, MatchStatus status) throws Exception {
		Match match = new Match();
		match.setId(matchId);
		match.setStatus(status.getId());

		super.update(match);
	}

	@Override
	public List<Match> queryExpireMatch(Integer status, Long expr) {
		return mapper.queryExpireMatch(status, expr);

	}

	@Override
	public List<Match> queryMatchByTeamId(Long teamId, Integer closeNum) {
		return mapper.queryMatchByTeamId(teamId, closeNum);
	}
	
	@Override
	public List<Match> queryMatchByTeamIdAndLeagueId(Long teamId, Long leagueId) {
		return mapper.queryMatchByTeamIdAndLeagueId(teamId, leagueId);
	}

	@Override
	public List<Match> queryMatchesPage(String teanName, String leagueName, String start,
			String end) {
		if (StringUtils.isNotEmpty(teanName)) {
			teanName = "%" + teanName + "%";
		} else {
			teanName = null;
		}
		if (StringUtils.isNotEmpty(leagueName)) {
			leagueName = "%" + leagueName + "%";
		} else {
			leagueName = null;
		}
		return mapper.queryMatchesPage(teanName, leagueName, start, end);
	}

	@Override
	public List<Match> queryNearMatches(Long miles, Double longitude, Double latitude) {
		return mapper.queryNearMatches(miles, longitude, latitude);
	}

	@Override
	public List<Match> findMyMatchDate(Long userid) {
		return mapper.findMyMatchDate(userid);
	}

	@Override
	public List<Match> queryDoneMatches(Long teamId) {
		return mapper.queryDoneMatches(teamId);
	}
	
	@Override
	public List<Match> queryNeedStatsMatches() {
		return mapper.queryNeedStatsMatches();
	}

	@Override
	public List<Match> queryMatchHistoryByTeamIds(Long teamId1, Long teamId2, Integer start, Integer size) {
		return mapper.queryMatchHistoryByTeamIds(teamId1, teamId2, start, size);
	}
	
	@Override
	public List<Match> queryFutureMatches(Long teamId, Integer start, Integer size) {
		return mapper.queryFutureMatches(teamId, start, size);
	}
	
	@Override
	public void updateStatsStatus(Long id, Integer statsStatus) {
		mapper.updateStatsStatus(id, statsStatus);
	}

	@Override
	public List<Match> queryPassMatches(Long teamId, Integer start, Integer size) {
		return mapper.queryPassMatches(teamId, start, size);
	}
	
	@Override
	public List<Match> queryMatchsNeedRecorder(Long recorderId) {
		return mapper.queryMatchsNeedRecorder(recorderId);
	}
}
