package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.MatchRoundDao;
import com.renyi.basketball.bussness.dao.mapper.MatchRoundMapper;
import com.renyi.basketball.bussness.po.MatchRound;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class MatchRoundDaoImpl extends BaseDaoImpl<MatchRound, Integer> implements MatchRoundDao {
	@Resource
	private MatchRoundMapper mapper;

	@Resource
	public void setBaseMapper(MatchRoundMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public List<MatchRound> queryAll() {
		return mapper.queryAll();
	}

}
