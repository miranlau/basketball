package com.renyi.basketball.bussness.dao.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.PlaceScheduleDao;
import com.renyi.basketball.bussness.dao.mapper.PlaceScheduleMapper;
import com.renyi.basketball.bussness.po.PlaceSchedule;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class PlaceScheduleDaoImpl extends BaseDaoImpl<PlaceSchedule, Long> implements PlaceScheduleDao {
	@Resource
	private PlaceScheduleMapper mapper;

	@Resource
	public void setBaseMapper(PlaceScheduleMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public List<HashMap<String, String>> querySchedule(int year, int month, int day, Long placeId) {
		return mapper.querySchedule(year, month, day, placeId);
	}
}
