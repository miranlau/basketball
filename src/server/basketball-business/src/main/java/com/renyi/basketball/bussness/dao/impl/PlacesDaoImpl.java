package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.PlacesDao;
import com.renyi.basketball.bussness.dao.mapper.PlaceMapper;
import com.renyi.basketball.bussness.po.Place;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class PlacesDaoImpl extends BaseDaoImpl<Place, Long> implements PlacesDao {
	@Resource
	private PlaceMapper placeMapper;

	@Resource
	public void setBaseMapper(PlaceMapper placeMapper) {
		super.setBaseMapper(placeMapper);
	}

	@Override
	public List<Place> queryPlaceByArenasId(Long arenasId) {
		return placeMapper.queryPlaceByArenasId(arenasId);
	}
}
