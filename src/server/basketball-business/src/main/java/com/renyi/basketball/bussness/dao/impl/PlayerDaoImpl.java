package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.PlayerDao;
import com.renyi.basketball.bussness.dao.mapper.PlayerMapper;
import com.renyi.basketball.bussness.po.Player;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import jersey.repackaged.com.google.common.collect.Lists;

@Repository
public class PlayerDaoImpl extends BaseDaoImpl<Player, Long> implements PlayerDao {
	@Autowired
	private PlayerMapper mapper;

	@Autowired
	public void setBaseMapper(PlayerMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public Player insert(Player player) throws Exception {
		Date now = new Date();
		player.setCreateTime(now);
		player.setUpdateTime(now);
		return super.insert(player);
	}
	
	@Override
	public Player queryPlayerById(Long playerId) {
		return mapper.queryPlayerById(playerId);
	}
	
	@Override
	public List<Player> queryPlayerList(List<Long> playerIds, Long teamId) {
		if (CollectionUtils.isEmpty(playerIds) || teamId == null) {
			return Lists.newArrayList();
		}
		return mapper.queryPlayerList(playerIds, teamId);
	}
	
	@Override
	public List<Player> queryPlayerListByTeamId(Long teamId) {
		return mapper.queryPlayerListByTeamId(teamId);
	}

	@Override
	public List<Player> queryPlayerListByName(String name) {
		return mapper.queryPlayerListByName(name);
	}

}
