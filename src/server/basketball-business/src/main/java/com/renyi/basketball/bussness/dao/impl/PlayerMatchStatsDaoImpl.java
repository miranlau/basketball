package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.dao.PlayerMatchStatsDao;
import com.renyi.basketball.bussness.dao.mapper.PlayerMatchStatsMapper;
import com.renyi.basketball.bussness.po.PlayerMatchStats;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class PlayerMatchStatsDaoImpl extends BaseDaoImpl<PlayerMatchStats, Long> implements PlayerMatchStatsDao {
	private static Logger logger = Logger.getLogger(PlayerMatchStatsDaoImpl.class);
	
	@Resource
	private PlayerMatchStatsMapper mapper;

	@Resource
	public void setBaseMapper(PlayerMatchStatsMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public PlayerMatchStats insert(PlayerMatchStats playerMatchStats) throws Exception {
		Date date = new Date();
		playerMatchStats.setCreateTime(date);
		playerMatchStats.setUpdateTime(date);
		return super.insert(playerMatchStats);
	}

	@Override
	public List<PlayerMatchStats> queryByMatchId(Long matchId) {
		return mapper.queryByMatchId(matchId);
	}
	
	@Override
	public List<PlayerMatchStats> queryByPlayerIdAndLeague(Long playerId, Long leagueId) {
		return mapper.queryByPlayerIdAndLeague(playerId, leagueId);
	}

	@Override
	public void deleteByMatchId(Long matchId) {
		mapper.deleteByMatchId(matchId);
	}

	@Override
	public void batchSave(List<PlayerMatchStats> playerMatchStatsList) {
		if (CollectionUtils.isEmpty(playerMatchStatsList)) {
			return;
		}
		Date date = new Date();
		for (PlayerMatchStats playerMatchStats : playerMatchStatsList) {
			playerMatchStats.setCreateTime(date);
			playerMatchStats.setUpdateTime(date);
			try {
				mapper.insert(playerMatchStats);
			} catch (Exception e) {
				logger.error(String.format(ResponseMsgConstants.INSERT_PLAYER_MATCH_STATS_ERROR,
						playerMatchStats.getTeamId(), playerMatchStats.getMatchId(), e.getMessage()));
				break;
			}
		}
	}
	
}
