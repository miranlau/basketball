package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.PlayerSeasonStatsDao;
import com.renyi.basketball.bussness.dao.mapper.PlayerSeasonStatsMapper;
import com.renyi.basketball.bussness.po.PlayerSeasonStats;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class PlayerSeasonStatsDaoImpl extends BaseDaoImpl<PlayerSeasonStats, Long> implements PlayerSeasonStatsDao {

	@Resource
	private PlayerSeasonStatsMapper mapper;

	@Resource
	public void setBaseMapper(PlayerSeasonStatsMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public PlayerSeasonStats insert(PlayerSeasonStats playerSeasonStats) throws Exception {
		Date now = new Date();
		playerSeasonStats.setCreateTime(now);
		playerSeasonStats.setUpdateTime(now);
		return super.insert(playerSeasonStats);
	}

	@Override
	public PlayerSeasonStats update(PlayerSeasonStats playerSeasonStats) throws Exception {
		playerSeasonStats.setUpdateTime(new Date());
		return super.update(playerSeasonStats);
	}

	@Override
	public PlayerSeasonStats queryByPlayerIdAndLeague(Long playerId, Long leagueId, Long leagueStageId) {
		return mapper.queryByPlayerIdAndLeague(playerId, leagueId, leagueStageId);
	}
	
	@Override
	public List<PlayerSeasonStats> queryByTeamIdAndLeague(Long teamId, Long leagueId, Long leagueStageId) {
		return mapper.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
	}

}
