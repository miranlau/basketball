package com.renyi.basketball.bussness.dao.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.ProductDao;
import com.renyi.basketball.bussness.dao.mapper.ProductMapper;
import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.po.Product;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class ProductDaoImpl extends BaseDaoImpl<Product, Long> implements ProductDao {

	@Resource
	public void setBaseMapper(ProductMapper productMapper) {
		super.setBaseMapper(productMapper);
	}

	@Resource
	private ProductMapper productMapper;

	@Override
	public List<Product> query(String searchKeyword, Long companyId, Long areansId) {

		return productMapper.query(searchKeyword, companyId, areansId);
	}

	@Override
	public void insertProductArenas(Long productId, Long areansId) {
		productMapper.insertProductArenas(productId, areansId);
	}

	public List<HashMap> queryGoods(Long productId, Long areansId, Long companyId) {
		return productMapper.queryGoods(productId, areansId, companyId);
	}

	@Override
	public void deletesPA(Long productId, Long areansId) {
		productMapper.deletesPA(productId, areansId);
	}

	@Override
	public void inoutproduct(InOutProduct inoutproduct) {
		productMapper.inoutproduct(inoutproduct);
	}

	@Override
	public void updatePA(InOutProduct inoutproduct) {
		productMapper.updatePA(inoutproduct);
	}
}
