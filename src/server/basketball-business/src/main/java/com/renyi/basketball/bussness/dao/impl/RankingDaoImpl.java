package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.RankingDao;
import com.renyi.basketball.bussness.dao.mapper.RankingMapper;
import com.renyi.basketball.bussness.po.Ranking;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class RankingDaoImpl extends BaseDaoImpl<Ranking, Long> implements RankingDao {
	@Resource
	private RankingMapper mapper;

	@Resource
	public void setBaseMapper(RankingMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public Ranking insert(Ranking ranking) throws Exception {
		Date now = new Date();
		ranking.setCreateTime(now);
		ranking.setUpdateTime(now);
		return super.insert(ranking);
	}

	@Override
	public Ranking update(Ranking ranking) throws Exception {
		ranking.setUpdateTime(new Date());
		return super.update(ranking);
	}

	@Override
	public Ranking queryTeamRanking(Long leagueId, Long leagueStageId) {
		return mapper.queryTeamRanking(leagueId, leagueStageId);
	}

}
