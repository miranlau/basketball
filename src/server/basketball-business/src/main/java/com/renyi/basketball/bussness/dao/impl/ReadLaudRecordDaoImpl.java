package com.renyi.basketball.bussness.dao.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.ReadLaudRecordDao;
import com.renyi.basketball.bussness.dao.mapper.ReadLaudMapper;
import com.renyi.basketball.bussness.po.ReadLaud;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class ReadLaudRecordDaoImpl extends BaseDaoImpl<ReadLaud, Long> implements ReadLaudRecordDao {
	@Resource
	private ReadLaudMapper mapper;

	@Resource
	public void setBaseMapper(ReadLaudMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public void addNum(Long id, Long dynamicId, Integer numTypeRead) {
		mapper.addNum(id, dynamicId, numTypeRead);
	}

	@Override
	public Integer count(Long id, Long dynamicId, Integer type) {
		return mapper.count(id, dynamicId, type);
	}

	@Override
	public void deleteByDynamicId(Long dynamicId) {
		mapper.deleteByDynamicId(dynamicId);
	}
}
