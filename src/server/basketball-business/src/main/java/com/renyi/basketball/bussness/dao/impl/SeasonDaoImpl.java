package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.SeasonDao;
import com.renyi.basketball.bussness.dao.mapper.SeasonMapper;
import com.renyi.basketball.bussness.po.Season;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import jersey.repackaged.com.google.common.collect.Lists;

@Repository
public class SeasonDaoImpl extends BaseDaoImpl<Season, Long> implements SeasonDao {
	@Resource
	private SeasonMapper mapper;

	@Resource
	public void setBaseMapper(SeasonMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public List<Season> queryByLeague(Long leagueId) {
		return mapper.queryByLeague(leagueId);
	}
	
	@Override
	public List<Season> queryByIds(List<Long> seasonIds) {
		if (CollectionUtils.isNotEmpty(seasonIds)) {
			return mapper.queryByIds(seasonIds);
		} else {
			return Lists.newArrayList();
		}
	}
	@Override
	public List<Season> queryByName(Long leagueId, String name) {
		return mapper.queryByLeagueAndName(leagueId, name);
	}
}
