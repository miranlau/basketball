package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.mapper.TeamMapper;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import jersey.repackaged.com.google.common.collect.Lists;

@Repository
public class TeamDaoImpl extends BaseDaoImpl<Team, Long> implements TeamDao {

	@Autowired
	private TeamMapper mapper;

	@Autowired
	public void setBaseMapper(TeamMapper teamMapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public void create(Team team, Long leaderId) {
		HashMap<String, Object> map = new HashMap();
		// SimpletimeFormatter sf = new SimpleFormatter("");
		map.put("uid", leaderId);
		map.put("tid", team.getId());
		map.put("role", "9");
		map.put("number", "0");
		map.put("createtime", new Date());
		map.put("updatetime", new Date());
		mapper.saveTeam(map);
	}

	@Override
	public void deleteTeam(Long userId, Long teamId) {
		HashMap<String, Object> map = new HashMap();
		map.put("uid", userId);
		map.put("tid", teamId);
		mapper.deleteTeam(map);
	}

	@Override
	public int queryRecord(Long userId, Long teamId) {
		HashMap<String, Object> map = new HashMap();
		map.put("uid", userId);
		map.put("tid", teamId);
		return mapper.queryRecord(map);
	}
	
	@Override
	public List<Team> queryByIds(List<Long> teamIds) {
		if (CollectionUtils.isNotEmpty(teamIds)) {
			return mapper.queryByIds(teamIds);
		} else {
			return Lists.newArrayList();
		}
	}

	@Override
	public void updateNumber(Long userId, Long teamId, int number) {
		HashMap<String, Object> map = new HashMap();
		map.put("uid", userId);
		map.put("tid", teamId);
		map.put("number", number);
		mapper.updateNumber(map);
	}
	@Override
	public void dissolve(Long id) {
		mapper.dissolve(id);
	}

	@Override
	public void delete(Long id) {
		mapper.deleteUT(id);
	}

	@Override
	public void deleteFollow(Long lid) {
		mapper.deleteFollow(lid);
	}

	@Override
	public List<Team> queryAll() {
		return mapper.queryAllTeam();
	}

	@Override
	public int queryNumber(Long userId, Long teamId) {
		HashMap<String, Object> map = new HashMap();
		map.put("uid", userId);
		map.put("tid", teamId);
		return mapper.queryNumber(map);
	}
	@Override
	public List<Team> queryByUser(Long userId) {
		return mapper.queryByUser(userId);
	}

	@Override
	public List<Team> queryTeams(String keyword, Boolean isdel) {
		return mapper.queryTeams(keyword, isdel);
	}
	
	@Override
	public Team queryTeam(@Param("teamId") Long teamId) {
		return mapper.queryTeam(teamId);
	}
	
	@Override
	public int queryLeader(Long userId) {
		return mapper.queryLeader(userId);
	}

	@Override
	public List<Integer> queryAllNumber(Long teamId) {
		return mapper.queryAllNumber(teamId);
	}

	@Override
	public void updateTeam(Team team) {
		mapper.updateTeam(team);
	}

	@Override
	public List<Integer> queryPlayersByNumber(Long teamId, Integer num) {
		return mapper.queryPlayersByNumber(teamId, num);
	}

	@Override
	public void updatePlayerNum(Long teamId, int fromNum, int toNum) {
		mapper.updatePlayerNum(teamId, fromNum, toNum);
	}

	@Override
	public List<User> findPlayer(Long id, Boolean includeLeader) {
		return mapper.findPlayer(id, includeLeader);
	}

	@Override
	public List<Map<Integer, Integer>> queryNoLeader() {
		return mapper.queryNoLeader();
	}

	@Override
	public void updateNoLeader(Map<Integer, Integer> result) {
		mapper.updateNoLeader(result);
	}

	@Override
	public List<Team> queryStillNoLead() {
		return mapper.queryStillNoLead();
	}

	@Override
	public User findOnePlayer(Long teamId) {
		return mapper.findOnePlayer(teamId);
	}

	@Override
	public void updateLeader(Long teamId, Long userId) {
		mapper.updateLeader(teamId, userId);
	}

	@Override
	public void updateLeaders(Long userId, Long teamId) {
		mapper.updateLeaders(userId, teamId);
	}

	@Override
	public boolean hasExist(Team team) {
		Integer count = mapper.queryName(team.getName());
		if (count > 0) {
			return true;
		}
		return false;
	}

	@Override
	public void saveUT(Long teamId, Boolean isLeader, Integer member) {
		mapper.saveUT(teamId, isLeader, member);
	}

	@Override
	public List<Team> queryThreeDay(Date date) {
		return mapper.queryThreeDay(date);
	}

	@Override
	public List<Match> queryMatch(Long teamId, Integer stauts) {
		return mapper.queryMatch(teamId, stauts);
	}

	@Override
	public void addUser(Long uid, Long tid, Integer role) {
		mapper.addUser(uid, tid, role);
	}

	@Override
	public void removeUser(Long uid, Long tid) {
		mapper.removeUser(uid, tid);
	}

	@Override
	public void updateUserRole(Long uid, Long tid, Integer role) {
		mapper.updateUserRole(uid, tid, role);
	}

	@Override
	public Team queryByName(String name) {
		return mapper.queryByName(name);
	}
	
}
