package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.TeamMatchStatsDao;
import com.renyi.basketball.bussness.dao.mapper.TeamMatchStatsMapper;
import com.renyi.basketball.bussness.po.TeamMatchStats;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class TeamMatchStatsDaoImpl extends BaseDaoImpl<TeamMatchStats, Long> implements TeamMatchStatsDao {

	@Resource
	private TeamMatchStatsMapper mapper;

	@Resource
	public void setBaseMapper(TeamMatchStatsMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public TeamMatchStats insert(TeamMatchStats teamMatchStats) throws Exception {
		Date date = new Date();
		teamMatchStats.setCreateTime(date);
		teamMatchStats.setUpdateTime(date);
		return super.insert(teamMatchStats);
	}

	@Override
	public List<TeamMatchStats> queryByMatchId(Long matchId) {
		return mapper.queryByMatchId(matchId);
	}

	@Override
	public void deleteByMatchId(Long matchId) {
		mapper.deleteByMatchId(matchId);
	}

	@Override
	public List<TeamMatchStats> queryByTeamIdAndLeague(Long teamId, Long leagueId, Long leagueStageId) {
		return mapper.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
	}
	
}
