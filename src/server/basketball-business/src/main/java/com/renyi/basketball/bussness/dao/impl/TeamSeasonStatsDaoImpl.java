package com.renyi.basketball.bussness.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.TeamSeasonStatsDao;
import com.renyi.basketball.bussness.dao.mapper.TeamSeasonStatsMapper;
import com.renyi.basketball.bussness.dto.TeamRankingDto;
import com.renyi.basketball.bussness.po.TeamSeasonStats;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class TeamSeasonStatsDaoImpl extends BaseDaoImpl<TeamSeasonStats, Long> implements TeamSeasonStatsDao {

	@Resource
	private TeamSeasonStatsMapper mapper;

	@Resource
	public void setBaseMapper(TeamSeasonStatsMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public TeamSeasonStats insert(TeamSeasonStats teamSeasonStats) throws Exception {
		Date now = new Date();
		teamSeasonStats.setCreateTime(now);
		teamSeasonStats.setUpdateTime(now);
		return super.insert(teamSeasonStats);
	}

	@Override
	public TeamSeasonStats update(TeamSeasonStats teamSeasonStats) throws Exception {
		teamSeasonStats.setUpdateTime(new Date());
		return super.update(teamSeasonStats);
	}

	@Override
	public TeamSeasonStats queryByTeamIdAndLeague(Long teamId, Long leagueId, Long leagueStageId) {
		return mapper.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
	}

	@Override
	public List<TeamRankingDto> queryWinLossRanking(Long leagueId, Long leagueStageId) {
		return mapper.queryWinLossRanking(leagueId, leagueStageId);
	}

}
