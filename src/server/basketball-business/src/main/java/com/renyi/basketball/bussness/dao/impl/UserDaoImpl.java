package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dao.mapper.UserMapper;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User, Long> implements UserDao {
	@Autowired
	private UserMapper mapper;

	@Autowired
	public void setBaseMapper(UserMapper mapper) {
		super.setBaseMapper(mapper);
	}

	@Override
	public List<User> queryByTeam(Long teamid) {
		return mapper.queryByTeam(teamid);
	}

	@Override
	public String queryRadar(Long userId) {
		return mapper.queryRadar(userId);
	}

	@Override
	public User queryPlayer(Long userId, Long teamId) {
		return mapper.queryPlayer(userId, teamId);
	}

	@Override
	public List<User> queryPlayerList(List<Long> userIds, Long teamId) {
		return mapper.queryPlayerList(userIds, teamId);
	}

	@Override
	public List<User> queryUserByName(String name) {
		if (name != null) {
			name = "%" + name + "%";
		}
		return mapper.queryUserByName(name);
	}

	@Override
	public List<User> queryUserByTeamName(String searchKeyword) {
		if (searchKeyword != null) {
			searchKeyword = "%" + searchKeyword + "%";
		}
		return mapper.queryUserByTeamName(searchKeyword);
	}

	@Override
	public List<User> queryUserByMobile(String searchKeyword) {
		if (searchKeyword != null) {
			searchKeyword = "%" + searchKeyword + "%";
		}
		return mapper.queryUserByMobile(searchKeyword);
	}

	@Override
	public void updateUser(User user) {
		mapper.updateUser(user);
	}

	@Override
	public boolean mobileHasExist(User user) {
		if (user.getMobile() != null) {
			Integer count = mapper.queryMobile(user.getMobile());
			if (count > 0) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean hasSameTeam(User user, Team team) {
		if (user.getNickName() != null) {
			Long userId = mapper.hasSameTeam(user.getNickName(), team.getName());
			if (null != userId && userId > 0) {
				return true;
			}
		}
		return false;
	}
	@Override
	public Long queryByTeam(User user, Team team) {
		return mapper.hasSameTeam(user.getNickName(), team.getName());
	}

	@Override
	public void updatePlayer(Long id, Integer score, Integer win, Integer lose, Integer help, Integer backboard,
			Integer two_in, Integer two_out, Integer three_in, Integer three_out, Integer Freethrow_in,
			Integer Freethrow_out, Integer cover, Integer steals, Integer anError) {
		mapper.updatePlayer(id, score, win, lose, help, backboard, two_in, two_out, three_in, three_out, Freethrow_in,
				Freethrow_out, cover, steals, anError);
	}

	@Override
	public void updateWin(Long id, int win) {
		mapper.updateWin(id, win);
	}

	@Override
	public void updateLose(Long id, int lose) {
		mapper.updateLose(id, lose);
	}

	@Override
	public String getMaxMobile() {
		return mapper.getMaxMobile();
	}
}
