package com.renyi.basketball.bussness.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.VenueDao;
import com.renyi.basketball.bussness.dao.mapper.VenueMapper;
import com.renyi.basketball.bussness.po.Venue;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import jersey.repackaged.com.google.common.collect.Lists;

@Repository
public class VenueDaoImpl extends BaseDaoImpl<Venue, Long> implements VenueDao {
	@Resource
	private VenueMapper mapper;

	@Resource
	public void setBaseMapper(VenueMapper baseMapper) {
		super.setBaseMapper(baseMapper);
	}

	@Override
	public List<Venue> findVenuesPager(String name) {
		return mapper.findVenuesPager(name);
	}
	
	@Override
	public List<Venue> queryByIds(List<Long> venueIds) {
		if (CollectionUtils.isNotEmpty(venueIds)) {
			return mapper.queryByIds(venueIds);
		} else {
			return Lists.newArrayList();
		}
	}
}
