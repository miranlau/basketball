package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Performance;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by flyong86 on 2016/5/8.
 */
public interface ActionMapper extends BaseMapper<Action, Long> {

	/**
	 * 通过比赛ID查询最新的Action
	 * 
	 * @param matchId
	 *            Long
	 * @param maxResult
	 *            Integer
	 * @return List<Action>
	 */
	List<Action> queryListByMatchId(@Param("matchId") Long matchId, @Param("maxResult") Integer maxResult);

	/**
	 * 查询联赛的射手进球数
	 * 
	 * @param matchIds
	 *            (该联赛的所有比赛ID)
	 * @return
	 */
	List<Action> queryShooterInLeague(@Param("matchIds") List<Long> matchIds);

	List<Action> querySavesInLeague(@Param("matchIds") List<Long> matchIds);

	/**
	 * 找出有红黄牌的球员
	 * 
	 * @param matchId
	 * @return
	 */
	List<Action> findFoulPlayerId(@Param("matchId") Long matchId);

	Integer countNum(@Param("matchIds") List<Long> matchIds, @Param("playerId") Long playerId,
			@Param("action") int action);

	Integer countDoubleYellow(@Param("matchId") Long matchId, @Param("playerId") Long playerId);

	Performance queryPerformance(HashMap map);

	List<Action> queryByLeague(@Param("leagueId") Long leagueId);

	List<Action> findActions(Map<String, Object> params);

	void updateAction(Action action);

	void insertAction(Action action);

	/**
	 * 通过联赛ID查询助攻数据
	 * 
	 * @param leagueId
	 * @return
	 */
	List<Action> queryHelp(@Param("leagueId") Long leagueId, @Param("status") Integer status);
}
