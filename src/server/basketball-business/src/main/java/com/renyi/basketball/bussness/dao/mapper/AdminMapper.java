package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Admin;

/**
 * Created by Roy.xiao on 2016/5/25.
 */
public interface AdminMapper extends BaseMapper<Admin, Long> {

}
