package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Area;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/20.
 */
public interface AreaMapper extends BaseMapper<Area,Integer> {
    /**
     * 查询所有省
     * @return
     */
    List<Area> queryAllProvince();

    /**
     * 根据Code查询所有城市
     * @param queryCode
     * @return
     */
    List<Area> queryCities(@Param("province") String queryCode,@Param("fullCode")String fullCode);

    /**
     * 根据城市查所有区
     * @param areaCode
     * @return
     */
    List<Area> queryAreas(@Param("areaCode") String areaCode,@Param("fullCode")String fullCode);
}
