package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Arenas;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
public interface ArenasMapper extends BaseMapper<Arenas, Long> {
	/**
	 * 搜索球场.
	 * 
	 * @param name
	 * @param areaCode
	 * @param chooseStatus
	 * @param companyId
	 * @return
	 */
	List<Arenas> query(@Param("keyword") String name, @Param("areaCode") String areaCode,
			@Param("status") List<Integer> chooseStatus, @Param("companyId") Long companyId);

	/**
	 * 查询公司的场馆
	 * 
	 * @param companyId
	 * @return
	 */
	List<Arenas> queryArenasByCompany(@Param("companyId") Long companyId);
}
