package com.renyi.basketball.bussness.dao.mapper;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Bill;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface BillMapper extends BaseMapper<Bill, Integer> {
	Bill queryBill(@Param("teamId") Long teamId, @Param("matchId") Long matchId);
}
