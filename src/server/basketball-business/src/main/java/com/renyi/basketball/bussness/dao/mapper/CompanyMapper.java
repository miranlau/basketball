package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Company;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/25.
 */
public interface CompanyMapper extends BaseMapper<Company,Integer> {
    /**
     * 条件查询公司
     * @param searchKeyword
     * @param province
     * @return
     */
    List<Company> queryCompanies(@Param("name") String searchKeyword, @Param("province") Integer province);
}
