package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.DynamicComment;

/**
 * Created by Roy.xiao on 2016/6/1.
 */
public interface DynamicCommentMapper extends BaseMapper<DynamicComment,Long> {
    void insertComment(DynamicComment entity);

    void deleteByDynamicId(Long dynamicId);
}
