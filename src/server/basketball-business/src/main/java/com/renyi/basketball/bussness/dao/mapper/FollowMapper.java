package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Follow;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface FollowMapper extends BaseMapper<Follow, Long> {

	/**
	 * 插入关注记录
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 */
	void insertFollow(@Param("uid") Long uid, @Param("lid") Long lid, @Param("type") Integer type);
	/**
	 * 删除关注记录
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 */
	void deletefollow(@Param("uid") Long uid, @Param("lid") Long lid, @Param("type") Integer type);
	/**
	 * 查询关注记录
	 * 
	 * @param uid
	 * @param lid
	 * @param type
	 */
	List<Follow> queryfollow(@Param("uid") Long uid, @Param("lid") Long lid, @Param("type") Integer type);

}
