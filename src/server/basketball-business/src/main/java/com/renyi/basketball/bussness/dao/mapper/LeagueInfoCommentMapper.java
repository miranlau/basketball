package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.LeagueInfoComment;

/**
 * Created by Roy.xiao on 2016/5/27.
 */
public interface LeagueInfoCommentMapper extends BaseMapper<LeagueInfoComment,Integer> {

    void newinsert(LeagueInfoComment comment);
}
