package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.LeagueInfo;

import cn.magicbeans.mybatis.mapper.BaseMapper;
/**
 * Created by Roy.xiao on 2016/5/24.
 */
public interface LeagueInfoMapper extends BaseMapper<LeagueInfo, Long> {

	/**
	 * 查询赛事动态列表
	 * 
	 * @param leagueId
	 * @return
	 */
	List<LeagueInfo> queryInfoByLeagueId(@Param("leagueId") Long leagueId);

	/**
	 * 添加数量
	 * 
	 * @param infoId
	 * @param num
	 */
	void addNum(@Param("newNum") Long newNum, @Param("infoId") Long infoId, @Param("num") String num);
}
