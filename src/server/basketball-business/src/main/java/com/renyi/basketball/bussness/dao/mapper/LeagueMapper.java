package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.League;

import cn.magicbeans.mybatis.mapper.BaseMapper;

/**
 * Created by flyong86 on 2016/5/8.
 */
public interface LeagueMapper extends BaseMapper<League, Integer> {
	/**
	 * 查询赛事列表
	 * 
	 * @param keyword
	 * @return
	 */

	List<League> queryLeagues(@Param("keyword") String keyword, @Param("status") List<Integer> status,
			@Param("companyId") Long companyId);

	/**
	 * 更新联赛信息
	 * 
	 * @param league
	 */
	void updateById(League league);

	/**
	 * 查询该状态的赛事列表
	 * 
	 * @param signing
	 * @return
	 */
	List<League> quertExpireLeague(@Param("dateType") String dateType, @Param("signing") Integer signing);

	/**
	 * 所有联赛的ID
	 * 
	 * @return
	 */
	List<Long> queryAllIds();
}
