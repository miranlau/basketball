package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import com.renyi.basketball.bussness.po.LeagueStage;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface LeagueStageMapper extends BaseMapper<LeagueStage, Integer> {

	List<LeagueStage> queryAll();

}
