package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Match;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface MatchMapper extends BaseMapper<Match, Long> {
	/**
	 * 创建比赛
	 * 
	 * @param match
	 *            比赛Id
	 */
	void newinsert(Match match);

	/**
	 * 查询所有比赛
	 * 
	 * @param searchKeyword
	 *            赛事关键字
	 * @return
	 */
	List<Match> queryMatches(@Param("keyword") String keyword);

	/**
	 * 查询用户的比赛
	 * 
	 * @param userId
	 *            用户ID
	 * @return
	 */
	List<Match> queryByUser(Long userId);

	/**
	 * 删除比赛
	 * 
	 * @param id
	 *            比赛Id
	 */
	void deleteById(Long id);

	/**
	 * 查询比赛详细
	 * 
	 * @param id
	 *            比赛id
	 * @return
	 */
	Match queryById(Long id);

	/**
	 * 更新比赛信息
	 * 
	 * @param match
	 *            比赛信息
	 */
	void updateMatch(Match match);

	/**
	 * 查询赛事比赛列表
	 * 
	 * @param id
	 * @return
	 */
	List<Match> queryLeagueMatch(Long id);

	/**
	 * @param status
	 *            比赛状态
	 * @param expr
	 *            比较间隔
	 * @return 比赛列表
	 */
	List<Match> queryExpireMatch(@Param("status") Integer status, @Param("expr") Long expr);

	/**
	 * 查询球队比赛，最近colseNum场
	 * 
	 * @param teamId
	 * @param closeNum
	 * @return
	 */
	List<Match> queryMatchByTeamId(@Param("teamId") Long teamId, @Param("closeNum") Integer closeNum);
	
	/**
	 * 查询球队赛事比赛
	 * 
	 * @param teamId
	 * @param closeNum
	 * @return
	 */
	List<Match> queryMatchByTeamIdAndLeagueId(@Param("teamId") Long teamId, @Param("leagueId") Long leagueId);

	/**
	 * 分页查询比赛列表
	 * 
	 * @param teanName
	 *            球队名字
	 * @return
	 */
	List<Match> queryMatchesPage(@Param("teamName") String teanName, @Param("leagueName") String leagueName, @Param("start") String start,
			@Param("end") String end);

	/**
	 * 查询附近比赛
	 * 
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	List<Match> queryNearMatches(@Param("miles") Long miles, @Param("longitude") Double longitude, @Param("latitude") Double latitude);

	/**
	 * 查询我的比赛日期
	 * 
	 * @param userid
	 * @return
	 */
	List<Match> findMyMatchDate(Long userid);

	/**
	 * 已完成的比赛
	 * 
	 * @param teamId
	 * @return
	 */
	List<Match> queryDoneMatches(Long teamId);

	/**
	 * 查询需要生成统计数据的比赛
	 */
	List<Match> queryNeedStatsMatches();

	/**
	 * 查询两只球队最近的交战史
	 * 
	 * @param teamId1
	 *            Long
	 * @param taemId2
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryMatchHistoryByTeamIds(@Param("teamId1") Long teamId1, @Param("teamId2") Long teamId2, @Param("start") Integer start,
			@Param("size") Integer size);

	/**
	 * 查询球队未来的比赛
	 * 
	 * @param teamId
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryFutureMatches(@Param("teamId") Long teamId, @Param("start") Integer start, @Param("size") Integer size);

	/**
	 * 查询球队过去的比赛
	 * 
	 * @param teamId
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryPassMatches(@Param("teamId") Long teamId, @Param("start") Integer start, @Param("size") Integer size);

	/**
	 * 更新是否生成数据统计
	 * 
	 * @param id
	 *            Long
	 * @param statsStatus
	 *            Boolean
	 */
	void updateStatsStatus(@Param("id") Long id, @Param("statsStatus") Integer statsStatus);

	/**
	 * 查询记分员的比赛
	 * 
	 * @param recorderId
	 *            Long
	 * @param status
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryMatchsNeedRecorder(@Param("recorderId") Long recorderId);

}
