package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import com.renyi.basketball.bussness.po.MatchRound;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface MatchRoundMapper extends BaseMapper<MatchRound, Integer> {

	List<MatchRound> queryAll();

}
