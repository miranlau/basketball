package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Place;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
public interface PlaceMapper extends BaseMapper<Place, Long> {
	/**
	 * 查询球馆的场地列表
	 * 
	 * @param arenasId
	 * @return
	 */
	List<Place> queryPlaceByArenasId(@Param("arenasId") Long arenasId);
}
