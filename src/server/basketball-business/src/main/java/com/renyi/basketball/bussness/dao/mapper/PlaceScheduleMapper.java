package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.PlaceSchedule;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Roy.xiao on 2016/6/22.
 */
public interface PlaceScheduleMapper extends BaseMapper<PlaceSchedule, Long> {
	List<HashMap<String, String>> querySchedule(@Param("year") int year, @Param("month") int month,
			@Param("day") int day, @Param("placeId") Long placeId);
}
