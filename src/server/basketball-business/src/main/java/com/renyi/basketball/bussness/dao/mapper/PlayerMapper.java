package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Player;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface PlayerMapper extends BaseMapper<Player, Long> {

	Player queryPlayerById(@Param("playerId") Long playerId);
	
	List<Player> queryPlayerList(@Param("playerIds") List<Long> playerIds, @Param("teamId") Long teamId);
	
	List<Player> queryPlayerListByTeamId(@Param("teamId") Long teamId);

	List<Player> queryPlayerListByName(@Param("name")String name);
	
}
