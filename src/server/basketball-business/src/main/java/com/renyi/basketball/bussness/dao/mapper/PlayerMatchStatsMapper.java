package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.PlayerMatchStats;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface PlayerMatchStatsMapper extends BaseMapper<PlayerMatchStats, Long> {
  
	List<PlayerMatchStats> queryByMatchId(@Param("matchId") Long matchId);
	
	List<PlayerMatchStats> queryByPlayerIdAndLeague(@Param("playerId") Long playerId, @Param("leagueId") Long leagueId);
	
	void deleteByMatchId(@Param("matchId")Long matchId);
	
}
