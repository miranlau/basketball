package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.PlayerSeasonStats;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface PlayerSeasonStatsMapper extends BaseMapper<PlayerSeasonStats, Long> {
 
	/**
	 * 按阶段查询球员赛季的技术统计
	 * 
	 * @param leagueId
	 *            Long
	 * @param teamId
	 *            Long
	 * @return TeamSeasonStats
	 */
	PlayerSeasonStats queryByPlayerIdAndLeague(@Param("playerId") Long playerId, @Param("leagueId") Long leagueId,
			@Param("leagueStageId") Long leagueStageId);
	
	/**
	 * 按阶段查询球员赛季的技术统计
	 * 
	 * @param leagueId
	 *            Long
	 * @param teamId
	 *            Long
	 * @return List of TeamSeasonStats
	 */
	List<PlayerSeasonStats> queryByTeamIdAndLeague(@Param("teamId") Long teamId, @Param("leagueId") Long leagueId,
			@Param("leagueStageId") Long leagueStageId);

}
