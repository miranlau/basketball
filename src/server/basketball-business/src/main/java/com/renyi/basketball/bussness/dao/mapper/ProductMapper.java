package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.po.Product;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface ProductMapper extends BaseMapper<Product, Long> {

	/**
	 * 分页查询商品
	 * 
	 * @param searchKeyword
	 * @param companyId
	 * @return
	 */
	List<Product> query(@Param("keyword") String searchKeyword, @Param("companyId") Long companyId,
			@Param("areansId") Long areansId);

	/**
	 * 插入商品和球场的关联
	 * 
	 * @param productId
	 * @param areansId
	 */
	void insertProductArenas(@Param("productId") Long productId, @Param("areansId") Long areansId);

	/**
	 * 查询商品的库存和销量
	 * 
	 * @param productId
	 * @param areansId
	 * @param companyId
	 * @return
	 */
	List<HashMap> queryGoods(@Param("productId") Long productId, @Param("areansId") Long areansId,
			@Param("companyId") Long companyId);

	/**
	 * 删除商品球场的关联
	 * 
	 * @param productId
	 * @param areansId
	 */
	void deletesPA(@Param("productId") Long productId, @Param("areansId") Long areansId);

	/**
	 * 进出货
	 */
	void inoutproduct(InOutProduct inoutproduct);

	/**
	 * 更新销量库存
	 * 
	 * @param inoutproduct
	 */
	void updatePA(InOutProduct inoutproduct);
}