package com.renyi.basketball.bussness.dao.mapper;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Ranking;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface RankingMapper extends BaseMapper<Ranking, Long> {

	Ranking queryTeamRanking(@Param("leagueId") Long leagueId, @Param("leagueStageId") Long leagueStageId);

}
