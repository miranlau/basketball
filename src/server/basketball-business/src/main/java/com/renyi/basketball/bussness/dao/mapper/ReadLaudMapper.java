package com.renyi.basketball.bussness.dao.mapper;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.ReadLaud;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface ReadLaudMapper extends BaseMapper<ReadLaud, Integer> {
	/**
	 * 增加点赞或阅读次数
	 * 
	 * @param id
	 * @param dynamicId
	 * @param numTypeRead
	 */
	void addNum(@Param("id") Long id, @Param("dynamicId") Long dynamicId, @Param("type") Integer numTypeRead);

	/**
	 * 查询用户操作记录条数
	 * 
	 * @param id
	 * @param dynamicId
	 * @param type
	 * @return
	 */
	Integer count(@Param("id") Long id, @Param("dynamicId") Long dynamicId, @Param("type") Integer type);

	void deleteByDynamicId(Long dynamicId);
}
