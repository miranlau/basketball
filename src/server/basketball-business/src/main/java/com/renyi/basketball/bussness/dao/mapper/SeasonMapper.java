package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Season;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface SeasonMapper extends BaseMapper<Season, Long> {

	List<Season> queryByLeague(@Param("leagueId") Long leagueId);
	
	List<Season> queryByIds(@Param("ids") List<Long> ids);
	
	List<Season> queryByLeagueAndName(@Param("leagueId") Long leagueId, @Param("keyword") String keyword);
}
