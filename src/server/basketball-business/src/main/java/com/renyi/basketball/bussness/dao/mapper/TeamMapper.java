package com.renyi.basketball.bussness.dao.mapper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface TeamMapper extends BaseMapper<Team, Long> {
	/**
	 * 保存球队球员关联信息
	 */
	void saveTeam(HashMap map);
	
	/**
	 * 根据多个id，查询球队
	 */
	List<Team> queryByIds(@Param("ids") List<Long> ids);

	/**
	 * 查询球队角色
	 *
	 * @param map
	 * @return
	 */
	int queryRole(HashMap map);

	/**
	 * 删除球队信息
	 *
	 * @param map
	 */
	void deleteTeam(HashMap map);

	/**
	 * 查询指定球员在指定球队中的记录
	 *
	 * @param map
	 * @return
	 */
	int queryRecord(HashMap map);

	/**
	 * 更新球员球衣号码
	 *
	 * @param map
	 */
	void updateNumber(HashMap map);

	/**
	 * 解散球队
	 *
	 * @param id
	 */
	void dissolve(Long id);

	/**
	 * 删除球员球队关联信息
	 *
	 * @param id
	 */
	void deleteUT(Long id);
	/**
	 * 删除球队关注关联信息
	 *
	 * @param lid
	 */
	void deleteFollow(Long lid);

	/**
	 * 通过userid查询球队列表
	 *
	 * @param userId
	 * @return
	 */
	List<Team> queryByUser(Long userId);

	/**
	 * 查询所有球
	 *
	 * @return
	 */
	List<Team> queryAllTeam();

	/**
	 * 查询球队列表.
	 *
	 * @param keyword
	 *            - 搜索关键字
	 * @param isdel
	 *            - 是否删除
	 * @return 球队列表
	 */
	List<Team> queryTeams(@Param("keyword") String keyword, @Param("isDismissed") Boolean isDismissed);
	
	Team queryTeam(@Param("teamId") Long teamId);

	int queryNumber(HashMap map);

	/**
	 * 查询用户创建的球队数量
	 *
	 * @param userId
	 * @return
	 */
	int queryLeader(Long userId);

	/**
	 * 查询队伍所有的球员号码
	 *
	 * @param teamId
	 * @return
	 */
	List<Integer> queryAllNumber(Long teamId);

	/**
	 * 根据球队ID与号码查询球员列表.
	 *
	 * @param teamId
	 *            - 球队ID
	 * @param num
	 *            - 号码
	 * @return 球员集合
	 */
	List<Integer> queryPlayersByNumber(@Param("teamId") Long teamId, @Param("num") Integer num);

	/**
	 * 更新球队球员号码.
	 * 
	 * @param teamId
	 *            球队ID
	 * @param fromNum
	 *            原号码
	 * @param toNum
	 *            新号码
	 */
	void updatePlayerNum(@Param("teamId") Long teamId, @Param("fromNum") int fromNum, @Param("toNum") int toNum);

	/**
	 * 更新球队 赢场、平场、输场 和 等级
	 *
	 * @param team
	 */
	void updateTeam(@Param("team") Team team);

	/**
	 * 该球队的所有球员
	 * 
	 * @param id
	 * @param includeLeader
	 *            是否包含队长
	 * @return
	 */
	List<User> findPlayer(@Param("id") Long id, @Param("includeLeader") Boolean includeLeader);

	List<Map<Integer, Integer>> queryNoLeader();

	void updateNoLeader(@Param("result") Map<Integer, Integer> result);

	List<Team> queryStillNoLead();

	User findOnePlayer(@Param("teamId") Long teamId);

	void updateLeader(@Param("teamId") Long teamId, @Param("userId") Long userId);

	void updateLeaders(@Param("userId") Long userId, @Param("teamId") Long teamId);

	Integer queryName(@Param("name") String name);

	List<Integer> findUsers(@Param("tid") Long id);

	void saveUT(@Param("teamId") Long teamId, @Param("leader") Boolean isLeader, @Param("member") Integer member);

	List<Team> queryThreeDay(@Param("date") Date date);

	List<Match> queryMatch(@Param("teamId") Long teamId, @Param("stauts") Integer stauts);

	/**
	 * 球队里面添加球员.
	 * 
	 * @param uid
	 *            球员ID
	 * @param tid
	 *            球队ID
	 * @param role
	 *            角色
	 */
	void addUser(@Param("uid") Long uid, @Param("tid") Long tid, @Param("role") Integer role);

	/**
	 * 球队里面删除球员.
	 * 
	 * @param uid
	 *            球员ID
	 * @param tid
	 *            球队ID
	 */
	void removeUser(@Param("uid") Long uid, @Param("tid") Long tid);

	/**
	 * 更新球员角色.
	 * 
	 * @param uid
	 *            球员ID
	 * @param tid
	 *            球队ID
	 * @param role
	 *            角色
	 */
	void updateUserRole(@Param("uid") Long uid, @Param("tid") Long tid, @Param("role") Integer role);

	Team queryByName(@Param("name") String name);
}
