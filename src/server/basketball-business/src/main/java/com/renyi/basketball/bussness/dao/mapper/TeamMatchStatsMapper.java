package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.TeamMatchStats;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface TeamMatchStatsMapper extends BaseMapper<TeamMatchStats, Long> {

	List<TeamMatchStats> queryByMatchId(@Param("matchId") Long matchId);
	
	void deleteByMatchId(@Param("matchId")Long matchId);
	
	List<TeamMatchStats> queryByTeamIdAndLeague(@Param("teamId") Long teamId, @Param("leagueId") Long leagueId,
			@Param("leagueStageId") Long leagueStageId);

}
