package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.dto.TeamRankingDto;
import com.renyi.basketball.bussness.po.TeamSeasonStats;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface TeamSeasonStatsMapper extends BaseMapper<TeamSeasonStats, Long> {

	/**
	 * 按阶段查询球队赛季的技术统计
	 * 
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @param teamId
	 *            Long
	 * @return TeamSeasonStats
	 */
	TeamSeasonStats queryByTeamIdAndLeague(@Param("teamId") Long teamId, @Param("leagueId") Long leagueId,
			@Param("leagueStageId") Long leagueStageId);

	/**
	 * 按阶段查询球队的胜负排名
	 * 
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return TeamSeasonStats
	 */
	public List<TeamRankingDto> queryWinLossRanking(@Param("leagueId") Long leagueId,
			@Param("leagueStageId") Long leagueStageId);

}
