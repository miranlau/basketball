package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.UserDynamic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/6/1.
 */
public interface UserDynamicMapper extends BaseMapper<UserDynamic, Integer> {
	void insertDynamic(UserDynamic entity);

	List<UserDynamic> queryFriendDynamic(@Param("ids") List<Long> ids);

	void updateDynamic(UserDynamic userDynamic);

	List<UserDynamic> findAllDynamic();

	void deleteById(Long dynamicId);

	List<UserDynamic> findMyDynamicList(Long userId);
}
