package com.renyi.basketball.bussness.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.mybatis.mapper.BaseMapper;

public interface UserMapper extends BaseMapper<User, Long> {
	List<User> queryByTeam(Long teamId);
	String queryRadar(Long userId);

	/**
	 * 查询球员数据.
	 * 
	 * @param userId
	 *            - 用户ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员
	 */
	User queryPlayer(@Param("userId") Long userId, @Param("teamId") Long teamId);

	/**
	 * 查询球员数据集合
	 * 
	 * @param userIds
	 *            球员ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员集合
	 */
	List<User> queryPlayerList(@Param("userIds") List<Long> userIds, @Param("teamId") Long teamId);

	/**
	 * 通过名字模糊查询用户
	 * 
	 * @param name
	 * @return
	 */
	List<User> queryUserByName(@Param("name") String name);

	/**
	 * 通过球队名称模糊查询用户
	 * 
	 * @param searchKeyword
	 * @return
	 */
	List<User> queryUserByTeamName(@Param("searchKeyword") String searchKeyword);
	/**
	 * 通过球员手机模糊查询用户
	 * 
	 * @param searchKeyword
	 * @return
	 */
	List<User> queryUserByMobile(@Param("searchKeyword") String searchKeyword);

	/**
	 * 更新user的总赛数、总得分、平局、等级
	 * 
	 * @param user
	 */
	void updateUser(@Param("user") User user);

	void updateLeader(@Param("userId") Long id);

	/**
	 * 查询电话号码是否重复
	 * 
	 * @param mobile
	 */
	Integer queryMobile(@Param("mobile") String mobile);
	Long queryIdcard(@Param("idcard") String idcard);

	Long hasSameTeam(@Param("uname") String uname, @Param("tname") String tname);

	/**
	 * 查询最大虚拟电话号码
	 * 
	 * @return
	 */
	String getMaxMobile();
	/**
	 * 更新球员比赛数据
	 * 
	 * @param id
	 * @param score
	 *            得分
	 * @param win
	 *            赢场
	 * @param lose
	 *            输场
	 * @param help
	 *            助攻
	 * @param backboard
	 *            篮板
	 * @param two_in
	 *            两分中
	 * @param two_out
	 *            两分不中
	 * @param three_in
	 *            三分中
	 * @param three_out
	 *            三分不中
	 * @param Freethrow_in
	 *            罚球中
	 * @param Freethrow_out
	 *            罚球不中
	 * @param cover
	 *            盖帽
	 * @param steals
	 *            抢断
	 */
	void updatePlayer(@Param("id") Long id, @Param("score") Integer score, @Param("win") Integer win,
			@Param("lose") Integer lose, @Param("help") Integer help, @Param("backboard") Integer backboard,
			@Param("two_in") Integer two_in, @Param("two_out") Integer two_out, @Param("three_in") Integer three_in,
			@Param("three_out") Integer three_out, @Param("Freethrow_in") Integer Freethrow_in,
			@Param("Freethrow_out") Integer Freethrow_out, @Param("cover") Integer cover,
			@Param("steals") Integer steals, @Param("anError") Integer anError);

	/**
	 * 更新赢场
	 * 
	 * @param id
	 * @param win
	 */
	void updateWin(@Param("id") Long id, @Param("win") int win);

	/**
	 * 更新输场
	 * 
	 * @param id
	 * @param win
	 */
	void updateLose(@Param("id") Long id, @Param("lose") int win);
}