package com.renyi.basketball.bussness.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import com.renyi.basketball.bussness.po.Venue;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VenueMapper extends BaseMapper<Venue, Long> {

	List<Venue> findVenuesPager(@Param("name") String name);
	
	List<Venue> queryByIds(@Param("ids") List<Long> ids);
}
