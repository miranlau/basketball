package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

public class ActionDto implements Serializable, Comparable<ActionDto> {
	private static final long serialVersionUID = 1790362628535122121L;
	private Long id;
	private Long matchId;
	private Long teamId;
	private Long playerId;
	private Integer actionStatus;
	private Long majorPlayer;
	private Long minorPlayer;
	private JSONObject actionContent;
	/** 动态创建时间 格式:0'00'' */
	private String createTime;
	/** 内容(话术) . */
	private String speaks;
	private String dynamicTime = "0";
	private String teamName;
	private PlayerDto playerA;
	private PlayerDto playerB;
	// 是否记录队员信息
	private Boolean bflag = true;

	public Boolean getBflag() {
		return bflag;
	}

	public void setBflag(Boolean bflag) {
		this.bflag = bflag;
	}

	public void setPlayerA(PlayerDto playerA) {
		this.playerA = playerA;
	}

	public void setPlayerB(PlayerDto playerB) {
		this.playerB = playerB;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Long getId() {
		return id;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getActionStatus() {
		return actionStatus;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public JSONObject getActionContent() {
		return actionContent;
	}

	public void setActionContent(JSONObject actionContent) {
		this.actionContent = actionContent;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public void setActionStatus(Integer actionStatus) {
		this.actionStatus = actionStatus;
	}

	public Long getMinorPlayer() {
		return minorPlayer;
	}

	public void setMinorPlayer(Long minorPlayer) {
		this.minorPlayer = minorPlayer;
	}

	public Long getMajorPlayer() {
		return majorPlayer;
	}

	public void setMajorPlayer(Long majorPlayer) {
		this.majorPlayer = majorPlayer;
	}
	public void setPlayer(PlayerDto player) {
		if (actionContent == null) {
			actionContent = new JSONObject();
		}
		actionContent.put("player", player);
	}
	/**
	 * 添加球员.
	 */
	public void setPlayer(PlayerDto playerA, PlayerDto playerB) {
		if (actionContent == null) {
			actionContent = new JSONObject();
		}
		if (null != playerA) {
			actionContent.put("playerA", playerA.toJson());
		}

		if (null != playerB) {
			actionContent.put("playerB", playerB.toJson());
		}
	}

	/**
	 * 获取球员A.
	 * 
	 * @return 球员
	 */
	public PlayerDto getPlayerA() {
		if (actionContent != null) {
			JSONObject player = actionContent.getJSONObject("playerA");
			if (null != player) {
				return (PlayerDto) JSONObject.toBean(player, PlayerDto.class);
			}
		}
		return null;
	}
	public PlayerDto getA() {
		return playerA;
	}
	public PlayerDto getB() {
		return playerB;
	}
	/**
	 * 获取球员B.
	 * 
	 * @return 球员
	 */
	public PlayerDto getPlayerB() {
		if (actionContent != null) {
			JSONObject player = actionContent.getJSONObject("playerB");
			if (null != player) {
				return (PlayerDto) JSONObject.toBean(player, PlayerDto.class);
			}
		}
		return null;
	}

	/**
	 * 获取Action内容
	 * 
	 * @return
	 */
	public String getActionString() {
		if (actionContent != null) {
			return actionContent.toString();
		}

		return "";
	}

	public void setActionString(String str) {
		if (StringUtils.isNotEmpty(str)) {
			actionContent = JSONObject.fromObject(str);

			try {
				JSONObject player = actionContent.getJSONObject("playerA");
				if (null != player) {

					this.majorPlayer = player.getLong("id");
				}
			} catch (Exception e) {
			}
			try {
				JSONObject player = actionContent.getJSONObject("playerB");
				if (null != player) {
					this.minorPlayer = player.getLong("id");
				}
			} catch (Exception e) {
			}
		}
	}

	public String getSpeaks() {
		return speaks;
	}

	public void setSpeaks(String speaks) {
		this.speaks = speaks;
	}

	public String getDynamicTime() {
		return dynamicTime;
	}

	public void setDynamicTime(String dynamicTime) {
		this.dynamicTime = dynamicTime;
	}

	public JSONObject toJson(MatchStats matchStats) {
		JSONObject json = new JSONObject();
		json.put("id", id);
		json.put("matchId", matchId);
		json.put("teamId", teamId);
		json.put("userId", playerId);
		json.put("status", actionStatus);
		json.put("action", getActionString());
		if (null != matchStats && null != majorPlayer) {
//			json.put("yellowSum", matchStats.getActionCountByPlayer(majorPlayer, 43));
//			json.put("redSum", matchStats.getActionCountByPlayer(majorPlayer, 42));
		}
		json.put("dynamicTime", getDynamicTime());
		return json;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ActionDto actionDto = (ActionDto) o;

		// // 开始结束只允许存在一个
		// if ((status.equals(100) || status.equals(11)) &&
		// status.equals(actionDto.getStatus())) {
		// return true;
		// }
		if (!id.equals(actionDto.id))
			return false;
		return actionStatus.equals(actionDto.actionStatus);

	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + actionStatus.hashCode();
		return result;
	}

	@Override
	public int compareTo(ActionDto o) {
		return id.compareTo(o.getId());
	}
}
