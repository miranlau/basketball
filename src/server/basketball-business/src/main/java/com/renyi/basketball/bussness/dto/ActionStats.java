package com.renyi.basketball.bussness.dto;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import com.renyi.basketball.bussness.common.StatsAlgorithm;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.utils.JsonUtil;

public abstract class ActionStats {
	protected List<Action> actionList;

	// --------------------------------------------按status统计-------------------------------------------
	// 两分-左侧
	protected int twoLeftSideStandingIn;
	protected int twoLeftSideStandingOut;
	protected int twoLeftSideDribblingIn;
	protected int twoLeftSideDribblingOut;
	// 两分-右侧
	protected int twoRightSideStandingIn;
	protected int twoRightSideStandingOut;
	protected int twoRightSideDribblingIn;
	protected int twoRightSideDribblingOut;
	// 两分-油漆区
	protected int twoPaintedIn;
	protected int twoPaintedOut;
	// 两分-正面
	protected int twoFrontStandingIn;
	protected int twoFrontStandingOut;
	protected int twoFrontDribblingIn;
	protected int twoFrontDribblingOut;
	// 三分-左侧底角
	protected int threeLeftWingStandingIn;
	protected int threeLeftWingStandingOut;
	protected int threeLeftWingDribblingIn;
	protected int threeLeftWingDribblingOut;
	// 三分-左侧45度
	protected int threeLeftSide45StandingIn;
	protected int threeLeftSide45StandingOut;
	protected int threeLeftSide45DribblingIn;
	protected int threeLeftSide45DribblingOut;
	// 三分-右侧底角
	protected int threeRightWingStandingIn;
	protected int threeRightWingStandingOut;
	protected int threeRightWingDribblingIn;
	protected int threeRightWingDribblingOut;
	// 三分-右侧45度
	protected int threeRightSide45StandingIn;
	protected int threeRightSide45StandingOut;
	protected int threeRightSide45DribblingIn;
	protected int threeRightSide45DribblingOut;
	// 三分-顶弧
	protected int threeRootArcStandingIn;
	protected int threeRootArcStandingOut;
	protected int threeRootArcDribblingIn;
	protected int threeRootArcDribblingOut;
	// 罚篮命中数
	protected int freeThrowIn;
	// 罚篮出手次数
	protected int freeThrowOut;
	// 助攻
	protected int assist;
	// 抢断
	protected int steal;
	// 盖帽
	protected int blockShot;
	// 失误
	protected int turnover;
	// 犯规
	protected int offensiveFoul;
	protected int defensiveFoul;
	protected int technicalFoul;
	protected int flagrantFoul;
	protected int unsportsmanlikeFoul;
	// 篮板
	protected int offensiveRebound;
	protected int defensiveRebound;
	// 连续且至少有一球罚进的命中数
	protected int continuousFreeThrowIn;
	// 连续罚篮的次数
	protected int continuousFreeThrowAttempt;
	// --------------------------------------------按status统计-------------------------------------------

	// --------------------------------------------投篮热区图----------------------------------------------
	// 两分左侧命中数
	protected int twoLeftSideShot;
	// 两分左侧出手次数
	protected int twoLeftsideAttempt;
	// 两分右侧命中数
	protected int twoRightsideShot;
	// 两分右侧出手次数
	protected int twoRightsideAttempt;
	// 油漆区命中数
	protected int twoPaintedShot;
	// 油漆区出手次数
	protected int twoPaintedAttempt;
	// 两分正面命中数
	protected int twoFrontShot;
	// 两分正面出手次数
	protected int twoFrontAttempt;
	// 三分-左侧底角命中数
	protected int threeLeftWingShot;
	// 三分-左侧底角出手次数
	protected int threeLeftWingAttempt;
	// 三分-右侧底角命中数
	protected int threeRightWingShot;
	// 三分-右侧底角出手次数
	protected int threeRightWingAttempt;
	// 三分-左侧45度命中数
	protected int threeLeftside45Shot;
	// 三分-左侧45度出手次数
	protected int threeLeftside45Attempt;
	// 三分-右侧45度命中数
	protected int threeRightside45Shot;
	// 三分-右侧45度出手次数
	protected int threeRightside45Attempt;
	// 三分-顶弧命中数
	protected int threeRootArcShot;
	// 三分-顶弧出手次数
	protected int threeRootArcAttempt;
	// 两分-命中数
	protected int twoPointsShot;
	// 两分-出手次数
	protected int twoPointsAttempt;
	// 三分-命中数
	protected int threePointsShot;
	// 三分-出手次数
	protected int threePointsAttempt;
	// --------------------------------------------投篮热区图----------------------------------------------

	// --------------------------------------------得分----------------------------------------------------
	// 罚篮命中数
	protected int freeThrowShot;
	// 罚篮出手次数
	protected int freeThrowAttempt;
	// 投篮出手命中数
	protected int fieldGoalShot;
	// 投篮出手总次数
	protected int fieldGoalAttempt;
	// 总得分
	protected int score;
	// --------------------------------------------得分-----------------------------------------------------

	// --------------------------------------------运接球投篮-----------------------------------------------
	// 接球投篮命中数
	protected int standingShot;
	// 接球投篮出手次数
	protected int standingAttempt;
	// 运球投篮命中数
	protected int dribblingShot;
	// 运球投篮出手次数
	protected int dribblingAttempt;
	// --------------------------------------------运接球投篮----------------------------------------------

	// --------------------------------------------进攻侧重比----------------------------------------------
	// 左侧-进攻出手次数
	protected int leftOffensiveAttempt;
	// 右侧-进攻出手次数
	protected int rightOffensiveAttempt;
	// 正面-进攻出手次数
	protected int frontOffensiveAttempt;
	// 油漆区-进攻出手次数
	protected int paintedOffensiveAttempt;
	// 进攻总次数
	protected int totalOffensiveAttempt;
	// 左侧进攻侧重比
	protected float leftOffensivePct;
	// 右侧进攻侧重比
	protected float rightOffensivePct;
	// 油漆区进攻侧重比
	protected float paintedOffensivePct;
	// 正面进攻侧重比
	protected float frontOffensivePct;
	// --------------------------------------------进攻侧重比-----------------------------------------------

	// --------------------------------------------进阶数据(其它)-------------------------------------------
	// 进攻成功的总次数
	protected int offsensiveShot;
	// 进攻的总次数(进攻节奏)
	protected int offensiveAttempt;
	// 进攻成功率
	protected float offensivePct;
	// 有效命中率 (Effective Field Goal Percentage)
	protected float efgPct;
	// 真实命中率 (True Shooting Percentage)
	protected float tsPct;
	// 助攻率
	protected float assistPct;
	// 篮板率
	protected float reboundPct;
	// 失误率
	protected float turnoverPct;
	// --------------------------------------------进阶数据(其它)------------------------------------------

	/**
	 * 是否是连续且至少有一球罚进
	 * 
	 * @param extension
	 *            String
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	protected boolean isContinuousFreeThrowIn(String extension) {
		Map<String, Object> extensionMap = JsonUtil.toObject(extension, Map.class);
		if (extensionMap == null) {
			return false;
		}
		Boolean isFreeThrowIn = MapUtils.getBoolean(extensionMap, RequestParams.IS_FREE_THROW_IN);
		if ((isFreeThrowIn != null && isFreeThrowIn)) {
			return true;
		}
		return false;
	}

	/**
	 * 统计不同Action的status
	 * 
	 * @param action
	 *            Action
	 */
	protected void countByStatus(Action action) {
		if (action.getStatus() == ActionStatus.TWO_LEFTSIDE_STANDING_IN) {
			twoLeftSideStandingIn++;
		} else if (action.getStatus() == ActionStatus.TWO_LEFTSIDE_STANDING_OUT) {
			twoLeftSideStandingOut++;
		} else if (action.getStatus() == ActionStatus.TWO_LEFTSIDE_DRIBBLING_IN) {
			twoLeftSideDribblingIn++;
		} else if (action.getStatus() == ActionStatus.TWO_LEFTSIDE_DRIBBLING_OUT) {
			twoLeftSideDribblingOut++;
		} else if (action.getStatus() == ActionStatus.TWO_RIGHTSIDE_STANDING_IN) {
			twoRightSideStandingIn++;
		} else if (action.getStatus() == ActionStatus.TWO_RIGHTSIDE_STANDING_OUT) {
			twoRightSideStandingOut++;
		} else if (action.getStatus() == ActionStatus.TWO_RIGHTSIDE_DRIBBLING_IN) {
			twoRightSideDribblingIn++;
		} else if (action.getStatus() == ActionStatus.TWO_RIGHTSIDE_DRIBBLING_OUT) {
			twoRightSideDribblingOut++;
		} else if (action.getStatus() == ActionStatus.TWO_PAINTED_IN) {
			twoPaintedIn++;
		} else if (action.getStatus() == ActionStatus.TWO_PAINTED_OUT) {
			twoPaintedOut++;
		} else if (action.getStatus() == ActionStatus.TWO_FRONT_STANDING_IN) {
			twoFrontStandingIn++;
		} else if (action.getStatus() == ActionStatus.TWO_FRONT_STANDING_OUT) {
			twoFrontStandingOut++;
		} else if (action.getStatus() == ActionStatus.TWO_FRONT_DRIBBLING_IN) {
			twoFrontDribblingIn++;
		} else if (action.getStatus() == ActionStatus.TWO_FRONT_DRIBBLING_OUT) {
			twoFrontDribblingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFT_WING_STANDING_IN) {
			threeLeftWingStandingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFT_WING_STANDING_OUT) {
			threeLeftWingStandingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFT_WING_DRIBBLING_IN) {
			threeLeftWingDribblingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFT_WING_DRIBBLING_OUT) {
			threeLeftWingDribblingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFTSIDE45_STANDING_IN) {
			threeLeftSide45StandingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFTSIDE45_STANDING_OUT) {
			threeLeftSide45StandingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFTSIDE45_DRIBBLING_IN) {
			threeLeftSide45DribblingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_LEFTSIDE45_DRIBBLING_OUT) {
			threeLeftSide45DribblingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHT_WING_STANDING_IN) {
			threeRightWingStandingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHT_WING_STANDING_OUT) {
			threeRightWingStandingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHT_WING_DRIBBLING_IN) {
			threeRightWingDribblingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHT_WING_DRIBBLING_OUT) {
			threeRightWingDribblingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHTSIDE45_STANDING_IN) {
			threeRightSide45StandingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHTSIDE45_STANDING_OUT) {
			threeRightSide45StandingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_IN) {
			threeRightSide45DribblingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_OUT) {
			threeRightSide45DribblingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_ROOT_ARC_STANDING_IN) {
			threeRootArcStandingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_ROOT_ARC_STANDING_OUT) {
			threeRootArcStandingOut++;
		} else if (action.getStatus() == ActionStatus.THREE_ROOT_ARC_DRIBBLING_IN) {
			threeRootArcDribblingIn++;
		} else if (action.getStatus() == ActionStatus.THREE_ROOT_ARC_DRIBBLING_OUT) {
			threeRootArcDribblingOut++;
		} else if (action.getStatus() == ActionStatus.FREE_THROW_IN) {
			freeThrowIn++;
			String extension = action.getExtension();
			if (StringUtils.isBlank(extension)) {
				return;
			}
			// 连续罚篮且至少有一球罚进
			if (isContinuousFreeThrowIn(extension)) {
				continuousFreeThrowIn++;
			}
			continuousFreeThrowAttempt++;
		} else if (action.getStatus() == ActionStatus.FREE_THROW_OUT) {
			freeThrowOut++;
		} else if (action.getStatus() == ActionStatus.ASSIST) {
			assist++;
		} else if (action.getStatus() == ActionStatus.STEAL) {
			steal++;
		} else if (action.getStatus() == ActionStatus.BLOCKSHOT) {
			blockShot++;
		} else if (action.getStatus() == ActionStatus.TURNOVER) {
			turnover++;
		} else if (action.getStatus() == ActionStatus.OFFENSIVE_FOUL) {
			offensiveFoul++;
		} else if (action.getStatus() == ActionStatus.DEFENSIVE_FOUL) {
			defensiveFoul++;
		} else if (action.getStatus() == ActionStatus.TECHNICAL_FOUL) {
			technicalFoul++;
		} else if (action.getStatus() == ActionStatus.FLAGRANT_FOUL) {
			flagrantFoul++;
		} else if (action.getStatus() == ActionStatus.UNSPORTSMANLIKE_FOUL) {
			unsportsmanlikeFoul++;
		} else if (action.getStatus() == ActionStatus.OFFENSIVE_REBOUND) {
			offensiveRebound++;
		} else if (action.getStatus() == ActionStatus.DEFENSIVE_REBOUND) {
			defensiveRebound++;
		}
	}

	/**
	 * 统计投篮热区
	 */
	protected void calcShottingArea() {
		// 两分左侧命中数
		twoLeftSideShot = StatsAlgorithm.sum(twoLeftSideStandingIn, twoLeftSideDribblingIn);
		// 两分左侧出手次数
		twoLeftsideAttempt = StatsAlgorithm.sum(twoLeftSideShot, twoLeftSideStandingOut, twoLeftSideDribblingOut);
		// 两分右侧命中数
		twoRightsideShot = StatsAlgorithm.sum(twoRightSideStandingIn, twoRightSideDribblingIn);
		// 两分右侧出手次数
		twoRightsideAttempt = StatsAlgorithm.sum(twoRightsideShot, twoRightSideStandingOut, twoRightSideDribblingOut);
		// 油漆区命中数
		twoPaintedShot = twoPaintedIn;
		// 油漆区出手次数
		twoPaintedAttempt = StatsAlgorithm.sum(twoPaintedShot, twoPaintedOut);
		// 两分正面命中数
		twoFrontShot = StatsAlgorithm.sum(twoFrontStandingIn, twoFrontDribblingIn);
		// 两分正面出手次数
		twoFrontAttempt = StatsAlgorithm.sum(twoFrontShot, twoFrontStandingOut, twoFrontDribblingOut);
		// 三分-左侧底角命中数
		threeLeftWingShot = StatsAlgorithm.sum(threeLeftWingStandingIn, threeLeftWingDribblingIn);
		// 三分-左侧底角出手次数
		threeLeftWingAttempt = StatsAlgorithm.sum(threeLeftWingShot, threeLeftWingStandingOut, threeLeftWingDribblingOut);
		// 三分-右侧底角命中数
		threeRightWingShot = StatsAlgorithm.sum(threeRightWingStandingIn, threeRightWingDribblingIn);
		// 三分-右侧底角出手次数
		threeRightWingAttempt = StatsAlgorithm.sum(threeRightWingShot, threeRightWingStandingOut, threeRightWingDribblingOut);
		// 三分-左侧45度命中数
		threeLeftside45Shot = StatsAlgorithm.sum(threeLeftSide45StandingIn, threeLeftSide45DribblingIn);
		// 三分-左侧45度出手次数
		threeLeftside45Attempt = StatsAlgorithm.sum(threeLeftside45Shot, threeLeftSide45StandingOut, threeLeftSide45DribblingOut);
		// 三分-右侧45度命中数
		threeRightside45Shot = StatsAlgorithm.sum(threeRightSide45StandingIn, threeRightSide45DribblingIn);
		// 三分-右侧45度出手次数
		threeRightside45Attempt = StatsAlgorithm.sum(threeRightside45Shot, threeRightSide45StandingOut, threeRightSide45DribblingOut);
		// 三分-顶弧命中数
		threeRootArcShot = StatsAlgorithm.sum(threeRootArcStandingIn, threeRootArcDribblingIn);
		// 三分-顶弧出手次数
		threeRootArcAttempt = StatsAlgorithm.sum(threeRootArcShot, threeRootArcDribblingOut, threeRootArcStandingOut);
	}

	/**
	 * 统计投篮方式
	 */
	protected void calcShottingWay() {
		// 接球投篮命中数
		standingShot = StatsAlgorithm.sum(twoLeftSideStandingIn, twoRightSideStandingIn, twoPaintedIn, twoFrontStandingIn,
				threeLeftWingStandingIn, threeLeftSide45StandingIn, threeRightWingStandingIn, threeRightSide45StandingIn,
				threeRootArcStandingIn);
		// 接球投篮出手次数
		standingAttempt = StatsAlgorithm.sum(standingShot, twoLeftSideStandingOut, twoRightSideStandingOut, twoPaintedOut,
				twoFrontStandingOut, threeLeftWingStandingOut, threeLeftSide45StandingOut, threeRightWingStandingOut,
				threeRightSide45StandingOut, threeRootArcStandingOut);
		// 运球投篮命中数
		dribblingShot = StatsAlgorithm.sum(twoLeftSideDribblingIn, twoRightSideDribblingIn, twoFrontDribblingIn, threeLeftWingDribblingIn,
				threeLeftSide45DribblingIn, threeRightWingDribblingIn, threeRightSide45DribblingIn, threeRootArcDribblingIn);
		// 运球投篮出手次数
		dribblingAttempt = StatsAlgorithm.sum(dribblingShot, twoLeftSideDribblingOut, twoRightSideDribblingOut, twoFrontDribblingOut,
				threeLeftWingDribblingOut, threeLeftSide45DribblingOut, threeRightWingDribblingOut, threeRightSide45DribblingOut,
				threeRootArcDribblingOut);
	}

	/**
	 * 统计进攻比重
	 */
	protected void calcOffensivePct() {
		// 左侧-进攻出手次数
		leftOffensiveAttempt = StatsAlgorithm.sum(twoLeftsideAttempt, threeLeftWingAttempt, threeLeftside45Attempt);
		// 右侧-进攻出手次数
		rightOffensiveAttempt = StatsAlgorithm.sum(twoRightsideAttempt, threeRightWingAttempt, threeRightside45Attempt);
		// 正面-进攻出手次数
		frontOffensiveAttempt = StatsAlgorithm.sum(twoFrontAttempt, threeRootArcAttempt);
		// 油漆区-进攻出手次数
		paintedOffensiveAttempt = twoPaintedAttempt;
		// 进攻总次数
		totalOffensiveAttempt = StatsAlgorithm.sum(leftOffensiveAttempt, rightOffensiveAttempt, frontOffensiveAttempt,
				paintedOffensiveAttempt);
		// 左侧进攻侧重比
		leftOffensivePct = StatsAlgorithm.percent(leftOffensiveAttempt, totalOffensiveAttempt);
		// 右侧进攻侧重比
		rightOffensivePct = StatsAlgorithm.percent(rightOffensiveAttempt, totalOffensiveAttempt);
		// 油漆区进攻侧重比
		paintedOffensivePct = StatsAlgorithm.percent(frontOffensiveAttempt, totalOffensiveAttempt);
		// 正面进攻侧重比
		frontOffensivePct = StatsAlgorithm.percent(paintedOffensiveAttempt, totalOffensiveAttempt);
	}

	/**
	 * 统计得分
	 */
	protected void calcScore() {
		// 两分-命中数
		twoPointsShot = StatsAlgorithm.sum(twoLeftSideStandingIn, twoLeftSideDribblingIn, twoRightSideStandingIn, twoRightSideDribblingIn,
				twoPaintedIn, twoFrontStandingIn, twoFrontDribblingIn);
		// 两分-出手次数
		twoPointsAttempt = StatsAlgorithm.sum(twoPointsShot, twoLeftSideStandingOut, twoLeftSideDribblingOut, twoRightSideStandingOut,
				twoRightSideDribblingOut, twoPaintedOut, twoFrontStandingOut, twoFrontDribblingOut);
		// 三分-命中数
		threePointsShot = StatsAlgorithm.sum(threeLeftWingStandingIn, threeLeftWingDribblingIn, threeRightWingStandingIn,
				threeRightWingDribblingIn, threeLeftSide45StandingIn, threeLeftSide45DribblingIn, threeRightSide45StandingIn,
				threeRightSide45DribblingIn, threeRootArcStandingIn, threeRootArcDribblingIn);
		// 三分-顶弧出手次数
		threePointsAttempt = StatsAlgorithm.sum(threePointsShot, threeLeftWingStandingOut, threeLeftWingDribblingOut,
				threeRightWingStandingOut, threeRightWingDribblingOut, threeLeftSide45StandingOut, threeLeftSide45DribblingOut,
				threeRightSide45StandingOut, threeRightSide45DribblingOut, threeRootArcStandingOut, threeRootArcDribblingOut);
		// 罚篮命中数
		freeThrowShot = freeThrowIn;
		// 罚篮出手次数
		freeThrowAttempt = freeThrowShot + freeThrowOut;
		// 总得分
		score = StatsAlgorithm.score(twoPointsShot, threePointsShot, freeThrowShot);
	}

	/**
	 * 统计球队或球员的进阶数据
	 */
	protected void calcAdvanced() {
		// 投篮出手命中数
		fieldGoalShot = StatsAlgorithm.sum(twoPointsShot, threePointsShot);
		// 投篮出手总次数
		fieldGoalAttempt = StatsAlgorithm.sum(twoPointsAttempt, threePointsAttempt);
		// 进攻成功的总次数
		offsensiveShot = StatsAlgorithm.sum(fieldGoalShot, turnover, offensiveFoul, continuousFreeThrowIn);
		// 进攻的总次数(进攻节奏)
		offensiveAttempt = StatsAlgorithm.sum(fieldGoalAttempt, turnover, offensiveFoul, continuousFreeThrowAttempt);
		// 进攻成功率
		offensivePct = StatsAlgorithm.percent(offsensiveShot, offensiveAttempt);
		// 有效命中率 (Effective Field Goal Percentage)
		efgPct = StatsAlgorithm.percent((twoPointsShot + 0.5 * threePointsShot), fieldGoalAttempt);
		// 真实命中率 (True Shooting Percentage)
		tsPct = StatsAlgorithm.percent(score, (2 * (fieldGoalAttempt + (0.44 * freeThrowAttempt))));
	}

	public List<Action> getActionList() {
		return actionList;
	}

	public int getTwoLeftSideStandingIn() {
		return twoLeftSideStandingIn;
	}

	public int getTwoLeftSideStandingOut() {
		return twoLeftSideStandingOut;
	}

	public int getTwoLeftSideDribblingIn() {
		return twoLeftSideDribblingIn;
	}

	public int getTwoLeftSideDribblingOut() {
		return twoLeftSideDribblingOut;
	}

	public int getTwoRightSideStandingIn() {
		return twoRightSideStandingIn;
	}

	public int getTwoRightSideStandingOut() {
		return twoRightSideStandingOut;
	}

	public int getTwoRightSideDribblingIn() {
		return twoRightSideDribblingIn;
	}

	public int getTwoRightSideDribblingOut() {
		return twoRightSideDribblingOut;
	}

	public int getTwoPaintedIn() {
		return twoPaintedIn;
	}

	public int getTwoPaintedOut() {
		return twoPaintedOut;
	}

	public int getTwoFrontStandingIn() {
		return twoFrontStandingIn;
	}

	public int getTwoFrontStandingOut() {
		return twoFrontStandingOut;
	}

	public int getTwoFrontDribblingIn() {
		return twoFrontDribblingIn;
	}

	public int getTwoFrontDribblingOut() {
		return twoFrontDribblingOut;
	}

	public int getThreeLeftWingStandingIn() {
		return threeLeftWingStandingIn;
	}

	public int getThreeLeftWingStandingOut() {
		return threeLeftWingStandingOut;
	}

	public int getThreeLeftWingDribblingIn() {
		return threeLeftWingDribblingIn;
	}

	public int getThreeLeftWingDribblingOut() {
		return threeLeftWingDribblingOut;
	}

	public int getThreeLeftSide45StandingIn() {
		return threeLeftSide45StandingIn;
	}

	public int getThreeLeftSide45StandingOut() {
		return threeLeftSide45StandingOut;
	}

	public int getThreeLeftSide45DribblingIn() {
		return threeLeftSide45DribblingIn;
	}

	public int getThreeLeftSide45DribblingOut() {
		return threeLeftSide45DribblingOut;
	}

	public int getThreeRightWingStandingIn() {
		return threeRightWingStandingIn;
	}

	public int getThreeRightWingStandingOut() {
		return threeRightWingStandingOut;
	}

	public int getThreeRightWingDribblingIn() {
		return threeRightWingDribblingIn;
	}

	public int getThreeRightWingDribblingOut() {
		return threeRightWingDribblingOut;
	}

	public int getThreeRightSide45StandingIn() {
		return threeRightSide45StandingIn;
	}

	public int getThreeRightSide45StandingOut() {
		return threeRightSide45StandingOut;
	}

	public int getThreeRightSide45DribblingIn() {
		return threeRightSide45DribblingIn;
	}

	public int getThreeRightSide45DribblingOut() {
		return threeRightSide45DribblingOut;
	}

	public int getThreeRootArcStandingIn() {
		return threeRootArcStandingIn;
	}

	public int getThreeRootArcStandingOut() {
		return threeRootArcStandingOut;
	}

	public int getThreeRootArcDribblingIn() {
		return threeRootArcDribblingIn;
	}

	public int getThreeRootArcDribblingOut() {
		return threeRootArcDribblingOut;
	}

	public int getFreeThrowIn() {
		return freeThrowIn;
	}

	public int getFreeThrowOut() {
		return freeThrowOut;
	}

	public int getAssist() {
		return assist;
	}

	public int getSteal() {
		return steal;
	}

	public int getBlockShot() {
		return blockShot;
	}

	public int getTurnover() {
		return turnover;
	}

	public int getOffensiveFoul() {
		return offensiveFoul;
	}

	public int getDefensiveFoul() {
		return defensiveFoul;
	}

	public int getTechnicalFoul() {
		return technicalFoul;
	}

	public int getFlagrantFoul() {
		return flagrantFoul;
	}

	public int getUnsportsmanlikeFoul() {
		return unsportsmanlikeFoul;
	}

	public int getOffensiveRebound() {
		return offensiveRebound;
	}

	public int getDefensiveRebound() {
		return defensiveRebound;
	}

	public int getContinuousFreeThrowIn() {
		return continuousFreeThrowIn;
	}

	public int getContinuousFreeThrowAttempt() {
		return continuousFreeThrowAttempt;
	}

	public int getTwoLeftSideShot() {
		return twoLeftSideShot;
	}

	public int getTwoLeftsideAttempt() {
		return twoLeftsideAttempt;
	}

	public int getTwoRightsideShot() {
		return twoRightsideShot;
	}

	public int getTwoRightsideAttempt() {
		return twoRightsideAttempt;
	}

	public int getTwoPaintedShot() {
		return twoPaintedShot;
	}

	public int getTwoPaintedAttempt() {
		return twoPaintedAttempt;
	}

	public int getTwoFrontShot() {
		return twoFrontShot;
	}

	public int getTwoFrontAttempt() {
		return twoFrontAttempt;
	}

	public int getThreeLeftWingShot() {
		return threeLeftWingShot;
	}

	public int getThreeLeftWingAttempt() {
		return threeLeftWingAttempt;
	}

	public int getThreeRightWingShot() {
		return threeRightWingShot;
	}

	public int getThreeRightWingAttempt() {
		return threeRightWingAttempt;
	}

	public int getThreeLeftside45Shot() {
		return threeLeftside45Shot;
	}

	public int getThreeLeftside45Attempt() {
		return threeLeftside45Attempt;
	}

	public int getThreeRightside45Shot() {
		return threeRightside45Shot;
	}

	public int getThreeRightside45Attempt() {
		return threeRightside45Attempt;
	}

	public int getThreeRootArcShot() {
		return threeRootArcShot;
	}

	public int getThreeRootArcAttempt() {
		return threeRootArcAttempt;
	}

	public int getTwoPointsShot() {
		return twoPointsShot;
	}

	public int getTwoPointsAttempt() {
		return twoPointsAttempt;
	}

	public int getThreePointsShot() {
		return threePointsShot;
	}

	public int getThreePointsAttempt() {
		return threePointsAttempt;
	}

	public int getFreeThrowShot() {
		return freeThrowShot;
	}

	public int getFreeThrowAttempt() {
		return freeThrowAttempt;
	}

	public int getFieldGoalShot() {
		return fieldGoalShot;
	}

	public int getFieldGoalAttempt() {
		return fieldGoalAttempt;
	}

	public int getScore() {
		return score;
	}

	public int getStandingShot() {
		return standingShot;
	}

	public int getStandingAttempt() {
		return standingAttempt;
	}

	public int getDribblingShot() {
		return dribblingShot;
	}

	public int getDribblingAttempt() {
		return dribblingAttempt;
	}

	public int getLeftOffensiveAttempt() {
		return leftOffensiveAttempt;
	}

	public int getRightOffensiveAttempt() {
		return rightOffensiveAttempt;
	}

	public int getFrontOffensiveAttempt() {
		return frontOffensiveAttempt;
	}

	public int getPaintedOffensiveAttempt() {
		return paintedOffensiveAttempt;
	}

	public int getTotalOffensiveAttempt() {
		return totalOffensiveAttempt;
	}

	public float getLeftOffensivePct() {
		return leftOffensivePct;
	}

	public float getRightOffensivePct() {
		return rightOffensivePct;
	}

	public float getPaintedOffensivePct() {
		return paintedOffensivePct;
	}

	public float getFrontOffensivePct() {
		return frontOffensivePct;
	}

	public int getOffsensiveShot() {
		return offsensiveShot;
	}

	public int getOffensiveAttempt() {
		return offensiveAttempt;
	}

	public float getOffensivePct() {
		return offensivePct;
	}

	public float getEfgPct() {
		return efgPct;
	}

	public float getTsPct() {
		return tsPct;
	}

	public float getAssistPct() {
		return assistPct;
	}

	public float getReboundPct() {
		return reboundPct;
	}

	public float getTurnoverPct() {
		return turnoverPct;
	}
	
	public int getRebound() {
		return offensiveRebound + defensiveRebound;
	}
	
	public int getFoul() {
		return StatsAlgorithm.sum(offensiveFoul, defensiveFoul, technicalFoul, flagrantFoul, unsportsmanlikeFoul);
	}
	
}
