package com.renyi.basketball.bussness.dto;

/**
 * Created by Administrator on 2016/6/20 0020.
 */
public class AdvertDto {
    //图片路径
    private String srcPath;

    //广告地址
    private String urlPath;
    //是否开启
    private boolean isOpen = false;

    public String getSrcPath() {
        return srcPath;
    }

    public void setSrcPath(String srcPath) {
        this.srcPath = srcPath;
    }


    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }
}
