package com.renyi.basketball.bussness.dto;

import com.renyi.basketball.bussness.po.Place;

import java.util.*;
/**
 * Created by Roy.xiao on 2016/6/13.
 */
public class ArenasDto {
    private Integer id;
    /** 场馆名字 */
    private  String name;
    /** 图标 */
    private String icon;
    /** 官网地址 */
    private String url;
    /** 地址 */
    private String address;
    /** 联系人 */
    private String telephone_name_1;
    /** 联系电话 */
    private String  telephone_number_1;
    /** 状态 */
    private Integer status;
    /** 经度 */
    private Double longitude;
    /** 纬度 */
    private Double latitude;
    /** 所属公司 */
    private String companyName;
    /** 公司ID. */
    private Integer company_id;
    /** 地区编码 */
    private String areaCode;
    /** 星级. */
    private Integer stars;
    /** 球场介绍 . */
    private String descs;
    /** 审核意见. */
    private String judgement;
    /** 场地. */
    private List<Place> place = new ArrayList<>();

    public List<Place> getPlaces() {
        return place;
    }

    public void setPlaces(List<Place> place) {
        this.place = place;
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJudgement() {
        return judgement;
    }

    public void setJudgement(String judgement) {
        this.judgement = judgement;
    }

    public String getDescs() {
        return descs;
    }

    public void setDescs(String descs) {
        this.descs = descs;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone_name_1() {
        return telephone_name_1;
    }

    public void setTelephone_name_1(String telephone_name_1) {
        this.telephone_name_1 = telephone_name_1;
    }

    public String getTelephone_number_1() {
        return telephone_number_1;
    }

    public void setTelephone_number_1(String telephone_number_1) {
        this.telephone_number_1 = telephone_number_1;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
}
