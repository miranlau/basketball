package com.renyi.basketball.bussness.dto;

import net.sf.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Roy.xiao on 2016/7/14.
 */
public class Attack implements Comparable<Attack>,Serializable{
    /** 球员ID */
    private Integer playerId;
    /** 球员名字.*/
    private String name;
    /** 所在球队名字 .*/
    private String teamName;
    /** 助攻数. */
    private Integer attack = 0;

    public void getAttack(String action) {
        try {
            JSONObject JsonAction = JSONObject.fromObject(action);
            JSONObject playerA = JsonAction.getJSONObject("playerA");
            JSONObject playerB = JsonAction.getJSONObject("playerB");
            if (playerA != null && playerA.size() != 0) {
                if (playerB != null && playerB.size() != 0) {
                    int attackId = playerB.getInt("id");
                    if (attackId != -1) {
                        playerId = attackId;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    @Override
    public int compareTo(Attack o) {
        if (this.attack > o.attack) {
            return -1;//降序
        } else if (this.attack < o.attack) {
            return 1;
        } else {
            if (!this.playerId.equals(o.playerId)) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj ) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Attack other = (Attack) obj;
        if (this.playerId.equals(other.playerId)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.playerId;
    }

    public void countAttack(int assist){
        this.attack = attack + assist;
    }
}
