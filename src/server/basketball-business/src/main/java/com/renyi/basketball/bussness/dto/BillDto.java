package com.renyi.basketball.bussness.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BillDto {
	private Integer id;
	/** 比赛ID. */
	private Long matchId;
	/** 比赛时间. */
	private Date date;
	/** 比赛地点. */
	private String address;
	/** 数据. */
	private List<BillPayDto> billPayDtos = new ArrayList<>();
	/** 总金额. */
	private Double total;
	/** 人均费用. */
	private Double averge;
	/** 球队ID. */
	private Long teamId;
	/** 是否是队长. */
	private Boolean isLeader = false;
	/** 队长ID. */
	private Long leaderId;

	public Boolean getLeader() {
		return isLeader;
	}

	public void setLeader(Boolean leader) {
		isLeader = leader;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<BillPayDto> getBillPayDtos() {
		return billPayDtos;
	}

	public void setBillPayDtos(List<BillPayDto> billPayDtos) {
		this.billPayDtos = billPayDtos;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getAverge() {
		return averge;
	}

	public void setAverge(Double averge) {
		this.averge = averge;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Boolean getIsLeader() {
		return isLeader;
	}

	public void setIsLeader(Boolean isLeader) {
		this.isLeader = isLeader;
	}

	public Long getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(Long leaderId) {
		this.leaderId = leaderId;
	}

}
