package com.renyi.basketball.bussness.dto;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class BillPayDto {
	/** 球员ID. */
	private Long userId;
	/** 球员名字. */
	private String userName;
	/** 头像. */
	private String avatar;
	/** 球衣号码. */
	private Integer number;
	/** 是否付钱. */
	private Boolean ispay;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvatar() {
		if (null != avatar && !avatar.startsWith("http://")) {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();

			return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/" + avatar;
		}
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Boolean getIspay() {
		return ispay;
	}

	public void setIspay(Boolean ispay) {
		this.ispay = ispay;
	}
}
