package com.renyi.basketball.bussness.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/8/3 0003.
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class Count implements Serializable {
    private static final long serialVersionUID = -2303907227460847275L;
    /**
     * 得分
     */
    private int points;
    /**
     *投篮
     */
    private int shooting ;
    /**
     *罚球
     */
    private int penalty ;
    /**
     *助攻
     */
    private int assists;
    /**
     *抢断
     */
    private int steals;
    /**
     *盖帽
     */
    private int blockShot;
    /**
     *失误
     */
    private int muff;
    /**
     *犯规
     */
    private int illegality;
    /**
     * 篮板
     */
    private int backboard;
    /**
     * 三分球
     */
    private int threePointer;

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getShooting() {
        return shooting;
    }

    public void setShooting(int shooting) {
        this.shooting = shooting;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public int getSteals() {
        return steals;
    }

    public void setSteals(int steals) {
        this.steals = steals;
    }

    public int getBlockShot() {
        return blockShot;
    }

    public void setBlockShot(int blockShot) {
        this.blockShot = blockShot;
    }

    public int getMuff() {
        return muff;
    }

    public void setMuff(int muff) {
        this.muff = muff;
    }

    public int getIllegality() {
        return illegality;
    }

    public void setIllegality(int illegality) {
        this.illegality = illegality;
    }

    public int getBackboard() {
        return backboard;
    }

    public void setBackboard(int backboard) {
        this.backboard = backboard;
    }

    public int getThreePointer() {
        return threePointer;
    }

    public void setThreePointer(int threePointer) {
        this.threePointer = threePointer;
    }
}
