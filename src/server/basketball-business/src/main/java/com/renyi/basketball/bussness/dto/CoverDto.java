package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

import net.sf.json.JSONObject;

public class CoverDto implements Comparable<CoverDto>, Serializable {
	/** 球员ID */
	private Long playerId;
	/** 球员名字. */
	private String name;
	/** 所在球队名字 . */
	private String teamName;

	/** 比赛场次 */
	private Integer count = 0;

	/** 所在联赛. */
	private Long leagueId;

	/** 封盖 */
	private Integer cover = 0;
	/** 场均封盖 */
	private double avgCover = 0;

	public Integer getCover() {
		return cover;
	}

	public void setCover(Integer cover) {
		this.cover = cover;
	}

	public double getAvgCover() {
		return avgCover;
	}

	public void setAvgCover(double avgCover) {
		this.avgCover = avgCover;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void getShooter(String action) {
		try {
			JSONObject JsonAction = JSONObject.fromObject(action);
			JSONObject playerA = JsonAction.getJSONObject("playerA");
			JSONObject playerB = JsonAction.getJSONObject("playerB");
			if (playerA != null && playerA.size() != 0) {
				playerId = playerA.getLong("id");
				name = playerA.getString("name");
			} else if (playerB != null && playerB.size() != 0) {
				playerId = playerB.getLong("id");
				name = playerB.getString("name");
			}

		} catch (Exception e) {
			e.printStackTrace();
			JSONObject JsonAction = JSONObject.fromObject(action);
			JSONObject playerA = JsonAction.getJSONObject("playerA");
			JSONObject playerB = JsonAction.getJSONObject("playerB");
			if (playerA != null && playerA.size() != 0) {
				playerId = playerA.getLong("id");
				name = playerA.getString("name");
			} else if (playerB != null && playerB.size() != 0) {
				playerId = playerB.getLong("id");
				name = playerB.getString("name");
			}
		}
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/*
	 * public Integer getIn() { return in; }
	 * 
	 * public void setIn(Integer in) { this.in = in; }
	 */

	@Override
	public int compareTo(CoverDto o) {
		if (this.avgCover > o.avgCover) {
			return -1;// 降序
		} else if (this.avgCover < o.avgCover) {
			return 1;
		} else {
			if (!this.playerId.equals(o.playerId)) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return false;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CoverDto other = (CoverDto) obj;
		if (this.playerId.equals(other.playerId)) {
			return true;
		} else {
			return false;
		}
	}

	public void coverCount() {
		this.cover++;
	}

	public void avgCover() {
		this.avgCover = Double.parseDouble(String.format("%.2f", this.cover * 1.0 / this.count));
	}
}
