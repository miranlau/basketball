package com.renyi.basketball.bussness.dto;

import com.renyi.basketball.bussness.po.User;

/**
 * Created by Roy.xiao on 2016/6/1.
 */
public class DynamicCommentDto {
    private Integer id;
    /** 评论内容*/
    private String content;

    /**接收者*/
    private User reciever;

    /** 发送者*/
    private User sender;

    /**动态ID */
    private Integer dynamicid;

    /**状态*/
    private Integer status;

    /** 评论时间 */
    private String pubTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getReciever() {
        return reciever;
    }

    public void setReciever(User reciever) {
        this.reciever = reciever;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Integer getDynamicid() {
        return dynamicid;
    }

    public void setDynamicid(Integer dynamicid) {
        this.dynamicid = dynamicid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPubTime() {
        return pubTime;
    }

    public void setPubTime(String pubTime) {
        this.pubTime = pubTime;
    }
}
