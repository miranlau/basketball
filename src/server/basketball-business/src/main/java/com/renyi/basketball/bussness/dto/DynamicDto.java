package com.renyi.basketball.bussness.dto;

import java.util.Date;
import java.util.List;

import com.renyi.basketball.bussness.po.User;

public class DynamicDto {
	private Long id;

	private Date createTime;

	private Date updateTime;

	/** 发布内容 */
	private String content;

	/** 图片URL */
	private List<String> pictures;

	/** 图片URL */
	private String images;

	/** 评论数 */
	private Integer commentNum;

	/** 阅读数 */
	private Integer readNum;

	/** 点赞数 */
	private Integer laudNum;

	/** 发布者 */
	private User user;
	/** 发布者ID */
	private Long userId;

	/** 经度 */
	private Double longitude;

	/** 纬度 */
	private Double latitude;

	/** 发布位置 */
	private String addr;

	/** 发布时间 */
	private String pubTime;

	/** 评论 */
	private List<DynamicCommentDto> commentDtos;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public List<DynamicCommentDto> getCommentDtos() {
		return commentDtos;
	}

	public void setCommentDtos(List<DynamicCommentDto> commentDtos) {
		this.commentDtos = commentDtos;
	}

	public String getPubTime() {
		return pubTime;
	}

	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<String> getPictures() {
		return pictures;
	}

	public void setPictures(List<String> pictures) {
		this.pictures = pictures;
	}

	public Integer getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	public Integer getReadNum() {
		return readNum;
	}

	public void setReadNum(Integer readNum) {
		this.readNum = readNum;
	}

	public Integer getLaudNum() {
		return laudNum;
	}

	public void setLaudNum(Integer laudNum) {
		this.laudNum = laudNum;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}
}
