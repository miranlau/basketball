package com.renyi.basketball.bussness.dto;

import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * Created by wangqy on 2016/5/9 0009.
 * 计分dto
 */
public class ExcelDto implements Serializable {
    private User user;
    private Team team;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
