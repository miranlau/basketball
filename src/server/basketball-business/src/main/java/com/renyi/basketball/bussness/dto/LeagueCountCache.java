package com.renyi.basketball.bussness.dto;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;
/**
 * 赛事榜单数据统计 Created by Roy.xiao on 2016/6/7.
 */
public class LeagueCountCache implements Serializable {
	/** 赛事ID. */
	private Long id;

	/** 积分榜单. */
	private Set<ScoresList> scoresList = new TreeSet<>();
	/** 个人得分榜 . */
	private Set<PensoerScoreList> PensoerScoreList = new TreeSet<>();
	/** 助攻榜. */
	private Set<HelpScoreDto> helpScoreList = new TreeSet<>();
	/** 篮板榜. */
	private Set<BackboardDto> BackboardList = new TreeSet<>();
	/** 抢断榜. */
	private Set<StealsDto> StealsList = new TreeSet<>();
	/** 盖帽榜 */
	private Set<CoverDto> coverList = new TreeSet<>();

	public Set<CoverDto> getCoverList() {
		return coverList;
	}

	public void setCoverList(Set<CoverDto> coverList) {
		this.coverList = coverList;
	}

	public Set<com.renyi.basketball.bussness.dto.PensoerScoreList> getPensoerScoreList() {
		return PensoerScoreList;
	}

	public void setPensoerScoreList(Set<com.renyi.basketball.bussness.dto.PensoerScoreList> pensoerScoreList) {
		PensoerScoreList = pensoerScoreList;
	}

	public Set<PensoerScoreList> getShooterLists() {
		return PensoerScoreList;
	}

	public void setShooterLists(Set<PensoerScoreList> shooterLists) {
		this.PensoerScoreList = shooterLists;
	}

	public Set<ScoresList> getScoresList() {
		return scoresList;
	}

	public void setScoresList(Set<ScoresList> scoresList) {
		this.scoresList = scoresList;
	}

	public Set<HelpScoreDto> getHelpScoreList() {
		return helpScoreList;
	}

	public void setHelpScoreList(Set<HelpScoreDto> helpScoreList) {
		this.helpScoreList = helpScoreList;
	}

	public Set<BackboardDto> getBackboardList() {
		return BackboardList;
	}

	public void setBackboardList(Set<BackboardDto> backboardList) {
		BackboardList = backboardList;
	}

	public Set<StealsDto> getStealsList() {
		return StealsList;
	}

	public void setStealsList(Set<StealsDto> stealsList) {
		StealsList = stealsList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
