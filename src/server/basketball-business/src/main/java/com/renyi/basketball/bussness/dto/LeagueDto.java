package com.renyi.basketball.bussness.dto;

import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.Venue;
import com.renyi.basketball.bussness.po.Company;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LeagueDto implements Serializable {
    private static final long serialVersionUID = 502481063735458600L;
    //赛事Id
    private Integer id;
    //赛事名称
    private String name;
    //赛事图片地址
    private String image;
    //参加比赛球队列表
    private List<Team> teams;
    //参赛球队数量
    private Integer teamNum;
    //联赛状态，0：报名中 1：报名结束 2：比赛中 3：比赛结束
    private Integer status;
    //赛事地区编号
    private String areacode;
    //地区名称
    private String areaname;
    //报名截止时间
    private Date deadline;
    //开始时间
    private Date begindate;
    //结束时间
    private Date enddate;
    //联赛详情
    private String details;
    //关注比赛的球员ids
    private String follows;
    //赛制，5,7，9,11
    private Integer matchtype;
    //是否已报名
    private Boolean isGauntlet = false;
    //是否已关注
    private Boolean isfollow = false;
    //所属公司
    private Company company;
    /** 审核意见 */
    private String judgment;
    /**　场地　*/
    private List<Venue> venue;

    public List<Venue> getVenue() {
        return venue;
    }

    public void setVenue(List<Venue> venue) {
        this.venue = venue;
    }

    public String getJudgment() {
        return judgment;
    }

    public void setJudgment(String judgment) {
        this.judgment = judgment;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public Integer getTeamNum() {
        return teamNum;
    }

    public void setTeamNum(Integer teamNum) {
        this.teamNum = teamNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Date getBegindate() {
        return begindate;
    }

    public void setBegindate(Date begindate) {
        this.begindate = begindate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getFollows() {
        return follows;
    }

    public void setFollows(String follows) {
        this.follows = follows;
    }

    public Integer getMatchtype() {
        return matchtype;
    }

    public void setMatchtype(Integer matchtype) {
        this.matchtype = matchtype;
    }

    public Boolean getGauntlet() {
        return isGauntlet;
    }

    public void setGauntlet(Boolean gauntlet) {
        isGauntlet = gauntlet;
    }

    public Boolean getIsfollow() {
        return isfollow;
    }

    public void setIsfollow(Boolean isfollow) {
        this.isfollow = isfollow;
    }

}
