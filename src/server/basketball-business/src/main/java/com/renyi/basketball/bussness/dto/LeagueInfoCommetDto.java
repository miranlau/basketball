package com.renyi.basketball.bussness.dto;

import com.renyi.basketball.bussness.po.User;

import java.io.Serializable;

/**
 * Created by Roy.xiao on 2016/5/27.
 */
public class LeagueInfoCommetDto implements Serializable {
    private static final long serialVersionUID = 1118654189940992611L;
    private Integer id;
    /** 赛事动态ID */
    private Integer league_info_id;
    /** 发送者 */
    private User sender;
    /** 接收者 */
    private User reciever;
    /** 内容 */
    private String content;
    /** 状态 */
    private Integer status;
    /** 发送时间 */
    private String pubTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLeague_info_id() {
        return league_info_id;
    }

    public void setLeague_info_id(Integer league_info_id) {
        this.league_info_id = league_info_id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReciever() {
        return reciever;
    }

    public void setReciever(User reciever) {
        this.reciever = reciever;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPubTime() {
        return pubTime;
    }

    public void setPubTime(String pubTime) {
        this.pubTime = pubTime;
    }
}
