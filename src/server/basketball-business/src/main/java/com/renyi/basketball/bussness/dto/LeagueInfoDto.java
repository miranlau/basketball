package com.renyi.basketball.bussness.dto;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/24.
 */
public class LeagueInfoDto implements Serializable {

    private static final long serialVersionUID = 1254113776946895750L;
    private Integer id;

    /** 发布时间 */
    private String pubTime;

    /** 赛事ID */
    private LeagueDto leagueDto;

    /** 发布内容 */
    private String content;

    /** 阅读数 */
    private Long read_num;

    /** 点赞数 */
    private Long laud_num;

    /** 评论数 */
    private Long comment_num;

    /** 评论 */
    private List<LeagueInfoCommetDto> comments = new ArrayList<>();

    /** 图片 */
    private List<String> imgs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPubTime() {
        return pubTime;
    }

    public void setPubTime(String pubTime) {
        this.pubTime = pubTime;
    }

    public LeagueDto getLeagueDto() {
        return leagueDto;
    }

    public void setLeagueDto(LeagueDto leagueDto) {
        this.leagueDto = leagueDto;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getRead_num() {
        return read_num;
    }

    public void setRead_num(Long read_num) {
        this.read_num = read_num;
    }

    public Long getLaud_num() {
        return laud_num;
    }

    public void setLaud_num(Long laud_num) {
        this.laud_num = laud_num;
    }

    public Long getComment_num() {
        return comment_num;
    }

    public void setComment_num(Long comment_num) {
        this.comment_num = comment_num;
    }

    public List<String> getImgs() {
        return imgs;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }

    @Transient
    public List<LeagueInfoCommetDto> getComments() {
        return comments;
    }

    public void setComments(List<LeagueInfoCommetDto> comments) {
        this.comments = comments;
    }
}
