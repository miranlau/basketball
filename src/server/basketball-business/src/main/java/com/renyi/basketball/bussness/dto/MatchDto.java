package com.renyi.basketball.bussness.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.renyi.basketball.bussness.po.Venue;

/**
 * Created by Administrator on 2016/5/16 0016.
 */
public class MatchDto implements Serializable {
	private static final long serialVersionUID = 282279462006648133L;
	// 比赛id
	private Long matchId;
	// 联赛id
	private Long leagueId;
	// 联赛名称
	private String leagueName;
	// 比赛时间
	private Date matchTime;
	// 比赛赛制3, 4, 5
	private Byte matchFormat;
	// 状态0约赛中,10已匹配,20比赛中,21即将开赛,100结束赛后
	private Integer status;
	// 比赛场馆ID
	private Integer venueId;
	// 是否专业记分
	private Boolean isRecording;
	// 上场时间
	private String playingTime;
	// 主队得分
	private Integer homeScore;
	// 客队得分
	private Integer visitingScore;
	// 是否加时
	private Boolean isOverTime;
	// 主队加时得分
	private Integer homeScoreInOT;
	// 客队加时得分
	private Integer visitingScoreInOT;

	// 比赛数据
	private String matchData;
	// 小节数据
	private String quarterData;

	// 主队用户id
	private List<Long> homePlayerIdList;
	// 客队用户Id
	private List<Long> visitingPlayerIdList;
	// 主队球队
	private TeamDto homeTeam;
	// 客队球队
	private TeamDto visitingTeam;
	// 主队首发球员
	private List<PlayerDto> homeStartingLineupList;
	// 客队首发球员
	private List<PlayerDto> visitingStartingLineupList;
//	// 主队记分员
//	private TeamRecorder homeTeamRecorder;
//	// 客队记分员
//	private TeamRecorder visitingTeamRecorder;
	// 主队球报名球员列表
	private List<PlayerDto> homePlayerList;
	// 客队报名球员列表
	private List<PlayerDto> visitingPlayerList;
	
	// 比赛场馆
	private Venue venue;

	/* 精度 */
	private Double longitude;
	/* 纬度 */
	private Double latitude;
	// 对手年龄
	private String ageRange;
	// 对手战斗力
	private String powerRange;
	/* 地址 */
	private String address;
	/* 地区编码 */
	private String areaCode;
	// 比赛描述
	private String desc;

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Boolean getIsRecording() {
		return isRecording;
	}

	public void setIsRecording(Boolean isRecording) {
		this.isRecording = isRecording;
	}

//	public TeamRecorder getHomeTeamRecorder() {
//		return homeTeamRecorder;
//	}
//
//	public void setHomeTeamRecorder(TeamRecorder homeTeamRecorder) {
//		this.homeTeamRecorder = homeTeamRecorder;
//	}
//
//	public TeamRecorder getVisitingTeamRecorder() {
//		return visitingTeamRecorder;
//	}
//
//	public void setVisitingTeamRecorder(TeamRecorder visitingTeamRecorder) {
//		this.visitingTeamRecorder = visitingTeamRecorder;
//	}

	public Integer getVisitingScore() {
		return visitingScore;
	}

	public Integer getHomeScore() {
		return homeScore;
	}

	public void setHomePlayerIdList(List<Long> homePlayerIdList) {
		this.homePlayerIdList = homePlayerIdList;
	}

	public String getBar() {
		return quarterData;
	}

	public void setBar(String bar) {
		this.quarterData = bar;
	}

	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}

	public List<PlayerDto> getHomePlayerList() {
		return homePlayerList;
	}

	public void setHomePlayerList(List<PlayerDto> homePlayerList) {
		this.homePlayerList = homePlayerList;
	}

	public List<PlayerDto> getVisitingPlayerList() {
		return visitingPlayerList;
	}

	public void setVisitingPlayerList(List<PlayerDto> visitingPlayerList) {
		this.visitingPlayerList = visitingPlayerList;
	}

	public List<Long> getHomePlayerIdList() {
		return homePlayerIdList;
	}

	public void setHomePlayerIds(List<Long> homePlayerIdList) {
		this.homePlayerIdList = homePlayerIdList;
	}

	public List<Long> getVisitingPlayerIdList() {
		return visitingPlayerIdList;
	}

	public void setVisitingPlayerIdList(List<Long> visitingPlayerIdList) {
		this.visitingPlayerIdList = visitingPlayerIdList;
	}

	public String getPlayers() {
		return playingTime;
	}

	public void setPlayers(String players) {
		this.playingTime = players;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setHomeScore(Integer homeScore) {
		this.homeScore = homeScore;
	}

	public void setVisitingScore(Integer visitingScore) {
		this.visitingScore = visitingScore;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	public void setHomeTeam(TeamDto homeTeam) {
		this.homeTeam = homeTeam;
	}

	public void setVisitingTeam(TeamDto visitingTeam) {
		this.visitingTeam = visitingTeam;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public void setMatchFormat(Byte matchFormat) {
		this.matchFormat = matchFormat;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getLeagueName() {
		return leagueName;
	}

	public TeamDto getHomeTeam() {
		return homeTeam;
	}

	public TeamDto getVisitingTeam() {
		return visitingTeam;
	}

	public Integer getStatus() {
		if (status == 20 && StringUtils.isBlank(matchData)) {
			status = 21;
		}
		return status;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public Byte getMatchFormat() {
		return matchFormat;
	}

	public String getDesc() {
		return desc;
	}

	public void setHomeusers(List<PlayerDto> homeusers) {
		this.homePlayerList = homeusers;
	}

	public void setVisitingusers(List<PlayerDto> visitingusers) {
		this.visitingPlayerList = visitingusers;
	}

	/**
	 * 获取主队总比分.
	 */
	public int getHomeTotalScore() {
		return homeScore + homeScoreInOT;
	}

	/**
	 * 客队总进球
	 */
	public int getVistingTotalScore() {
		return visitingScore + visitingScoreInOT;
	}

	public Boolean getIsOverTime() {
		return isOverTime;
	}

	public void setIsOverTime(Boolean isOverTime) {
		this.isOverTime = isOverTime;
	}

	public String getQuarterData() {
		return quarterData;
	}

	public void setQuarterData(String quarterData) {
		this.quarterData = quarterData;
	}

	public Integer getHomeScoreInOT() {
		return homeScoreInOT;
	}

	public void setHomeScoreInOT(Integer homeScoreInOverTime) {
		this.homeScoreInOT = homeScoreInOverTime;
	}

	public Integer getVisitingScoreInOT() {
		return visitingScoreInOT;
	}

	public void setVisitingScoreInOT(Integer visitingScoreInOverTime) {
		this.visitingScoreInOT = visitingScoreInOverTime;
	}

	public Date getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(Date matchTime) {
		this.matchTime = matchTime;
	}

	public List<PlayerDto> getHomeStartingLineupList() {
		return homeStartingLineupList;
	}

	public void setHomeStartingLineupList(List<PlayerDto> homeStartingLineupList) {
		this.homeStartingLineupList = homeStartingLineupList;
	}

	public List<PlayerDto> getVisitingStartingLineupList() {
		return visitingStartingLineupList;
	}

	public void setVisitingStartingLineupList(List<PlayerDto> visitingStartingLineupList) {
		this.visitingStartingLineupList = visitingStartingLineupList;
	}

	public String getMatchData() {
		return matchData;
	}

	public void setMatchData(String matchData) {
		this.matchData = matchData;
	}

	public String getAgeRange() {
		return ageRange;
	}

	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}

	public String getPowerRange() {
		return powerRange;
	}

	public void setPowerRange(String powerRange) {
		this.powerRange = powerRange;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
