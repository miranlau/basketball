package com.renyi.basketball.bussness.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.renyi.basketball.bussness.common.MatchStatus;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.utils.JsonUtil;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import jersey.repackaged.com.google.common.collect.Lists;
import jersey.repackaged.com.google.common.collect.Maps;

public class MatchStats implements Serializable {
	private static final long serialVersionUID = -2793256867268503821L;
	private transient Logger logger = Logger.getLogger(MatchStats.class);

	// 比赛ID
	private Long matchId;
	// 赛事ID
	private Long leagueId;
	// 赛事阶段
	private Long leagueStageId;
	// 比赛轮次
	private Long roundId;
	// 比赛时间
	private Date matchTime;
	// 比赛状态
	private int matchStatus = ActionStatus.NOT_START;
	// 比赛总的节数
	private int quarterCount;
	// 每节的总时间(分钟)
	private int quarterTime;
	// 当节剩余时间(秒)
	private int quarterRemainingTime;
	// 几人制
	private int matchFormat;
	// 主队
	private Team home;
	// 客队.
	private Team visiting;
	// 主队队员
	private List<Player> homePlayerList;
	// 客队队员
	private List<Player> visitingPlayerList;
	// 主队场上球员的IDs
	private Set<Long> homeLineupSet;
	// 客队场上球员的IDs
	private Set<Long> visitingLineupSet;
	// 主队记分员.
	private User homeRecorder;
	// 客队记分员.
	private User visitingRecorder;
	// 比赛动态.
	private List<Action> actionList;
	// 球员上场时间
	private Map<Long, Long> playingTimes;
	// 主队比分
	private int homeScore = 0;
	// 客队比分
	private int visitingScore = 0;
	// 是否暂停
	private boolean isSuspend = false;
	// 当前比赛进行到第几节
	private int quarterIndex = 0;
	// 每节数据
	private List<QuarterData> quarterDataList = Lists.newArrayList();

	/**
	 * 添加动态.
	 *
	 * @param action
	 */
	public void addAction(Action action) {
		if (actionList == null) {
			actionList = new ArrayList<Action>();
		}
		actionList.add(action);
	}

	/**
	 * 获取场上球员
	 * 
	 * @param teamId
	 * @return
	 */
	public List<Player> getLineupPlayers(Long teamId) {
		List<Player> playerList = getPlayers(teamId);
		List<Player> lineUpList = new ArrayList<Player>();
		if (playerList == null) {
			return null;
		}
		for (Player player : playerList) {
			if (player.getIsLineup()) {
				lineUpList.add(player);
			}
		}
		return playerList;
	}

	/**
	 * 获取比赛的球队.
	 *
	 * @param teamId
	 *            球队ID
	 * @return 球队
	 */
	public Team getTeam(Long teamId) {
		Team team = null;
		if (home != null && home.getId().equals(teamId)) {
			team = home;
		} else if (visiting != null && visiting.getId().equals(teamId)) {
			team = visiting;
		}
		return team;
	}

	/**
	 * 获取对手球队.
	 *
	 * @param teamId
	 *            本队ID
	 * @return 对手球队
	 */
	public Team getOpponent(Long teamId) {
		if (getHome().equals(getTeam(teamId))) {
			return visiting;
		}

		return home;
	}

	/**
	 * 获取球员信息.
	 *
	 * @param teamId
	 *            - 球队ID
	 * @param playerId
	 *            - 球员ID
	 * @return 球员
	 */
	public Player getPlayer(Long teamId, Long playerId) {
		List<Player> players = (home != null && home.getId().equals(teamId)) ? homePlayerList : visitingPlayerList;
		if (null != players) {
			for (Player player : players) {
				if (player.getId().equals(playerId)) {
					return player;
				}
			}
		}
		return null;
	}

	/**
	 * 获取球员信息.
	 *
	 * @param teamId
	 *            - 球队ID
	 * @param playerId
	 *            - 球员ID
	 * @return 球员
	 */
	public List<Player> getPlayerByIds(Long teamId, List<Long> playerIds) {
		if (CollectionUtils.isEmpty(playerIds)) {
			return null;
		}
		List<Player> players = (home != null && home.getId().equals(teamId)) ? homePlayerList : visitingPlayerList;
		if (CollectionUtils.isEmpty(players)) {
			return null;
		}
		List<Player> playerList = Lists.newArrayList();
		for (Player player : players) {
			for (int i = 0; i < playerIds.size(); i++) {
				if (playerIds.get(i).equals(player.getId())) {
					playerList.add(player);
					break;
				}
			}
		}
		return playerList;
	}

	/**
	 * 获取球员号码.
	 *
	 * @param teamId
	 *            - 球队ID
	 * @param playerId
	 *            - 球员ID
	 * @return 球员
	 */
	public List<Integer> getPlayerNumberByIds(Long teamId, Long[] playerIds) {
		List<Player> players = (home != null && home.getId().equals(teamId)) ? homePlayerList : visitingPlayerList;
		List<Integer> playerNumberList = Lists.newArrayList();
		if (CollectionUtils.isNotEmpty(players)) {
			for (Player player : players) {
				for (int i = 0; i < playerIds.length; i++) {
					if (playerIds[i].equals(player.getId())) {
						playerNumberList.add(player.getUniformNumber());
						break;
					}
				}
			}
		}
		return playerNumberList;
	}

	/**
	 * 获取所有队员.
	 *
	 * @param teamId
	 * @return
	 */
	public List<Player> getPlayers(Long teamId) {
		return (home != null && home.getId().equals(teamId)) ? homePlayerList : visitingPlayerList;
	}

	/**
	 * 检查球员的有效性.
	 *
	 * @param teamId
	 *            球队ID
	 * @param playerId
	 *            球员ID
	 * @return 有效性
	 */
	public boolean isValidPlayer(Long teamId, Long playerId) {
		Player player = getPlayer(teamId, playerId);
		if (null != player) {
			return true;
		}
		return false;
	}

	/**
	 * 换人
	 *
	 * @param teamId
	 * @param downPlayer
	 * @param upPlayer
	 * @return
	 */
	public void substitute(Long teamId, List<Long> upPlayerIdList, List<Long> downPlayerIdList) {
		List<Player> upPlayers = getPlayerByIds(teamId, upPlayerIdList);
		List<Player> downPlayers = getPlayerByIds(teamId, downPlayerIdList);

		long now = System.currentTimeMillis();
		for (Player up : upPlayers) {
			up.setUpTime(now);
		}

		for (Player down : downPlayers) {
			if (down.getUpdateTime() == null) {
				continue;
			}
			down.setPlayingTime(now - down.getUpTime());
			updateMatchData(down);
		}

		// Update lineup
		if (isHome(teamId)) {
			homeLineupSet.removeAll(downPlayerIdList);
			homeLineupSet.addAll(upPlayerIdList);
		} else {
			visitingLineupSet.removeAll(downPlayerIdList);
			visitingLineupSet.addAll(upPlayerIdList);
		}
	}

	/**
	 * 加分
	 * 
	 * @param teamId
	 * @param n
	 */
	public void addScore(long teamId, int n) {
		boolean isHome = home.getId().equals(teamId);
		if (isHome) {
			homeScore = homeScore + n;
		} else {
			visitingScore = visitingScore + n;
		}
	}

	public void subScore(long teamId, int n) {
		boolean isHome = home.getId().equals(teamId);
		if (isHome) {
			homeScore = homeScore - n;
		} else {
			visitingScore = visitingScore - n;
		}
	}

	/**
	 * 比赛是否结束.
	 */
	public boolean isOver() {
		if (matchStatus == ActionStatus.GAME_OVER) {
			return true;
		}
		return false;
	}

	/**
	 * 比赛是否开始
	 */
	public boolean isStarted() {
		if (matchStatus >= ActionStatus.START) {
			return true;
		}
		return false;
	}

	/**
	 * 构建球队每节的比分
	 * 
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> buildQuarterScoreList() {
		List<QuarterData> quarterDataList = getQuarterDataList();
		if (CollectionUtils.isEmpty(quarterDataList)) {
			return null;
		}

		List<Map<String, Object>> scoreList = new ArrayList<Map<String, Object>>();

		// 设置已经完成或正在打的节的比分
		int quarterCount = getQuarterCount();
		int quarterIndex = quarterDataList.size();
		for (int i = 0, j = 0; i < quarterIndex; i++) {
			QuarterData quarterData = quarterDataList.get(i);
			Map<String, Object> quarterScore = new HashMap<String, Object>();
			quarterScore.put(ResponseParams.HOME, quarterData.getHomeScore());
			quarterScore.put(ResponseParams.VISITING, quarterData.getVisitingScore());
			if ((i + 1) > quarterCount) {
				quarterScore.put(ResponseParams.IS_OT, true);
				quarterScore.put(ResponseParams.QUARTER_INDEX, j + 1);
				j++;
			} else {
				quarterScore.put(ResponseParams.IS_OT, false);
				quarterScore.put(ResponseParams.QUARTER_INDEX, i + 1);
			}
			scoreList.add(quarterScore);
		}

		// 没有开始的节比分设置为0
		if (quarterIndex < quarterCount) {
			for (int i = quarterIndex + 1; i <= quarterCount; i++) {
				Map<String, Object> quarterScore = new HashMap<String, Object>();
				quarterScore.put(ResponseParams.QUARTER_INDEX, i);
				quarterScore.put(ResponseParams.HOME, 0);
				quarterScore.put(ResponseParams.VISITING, 0);
				quarterScore.put(ResponseParams.IS_OT, false);
				scoreList.add(quarterScore);
			}
		}
		// 总分
		Map<String, Object> totalScore = Maps.newHashMap();
		totalScore.put(ResponseParams.HOME, getHomeScore());
		totalScore.put(ResponseParams.VISITING, getVisitingScore());
		scoreList.add(totalScore);
		return scoreList;
	}

	/**
	 * 构建球队比赛的数据统计
	 *
	 * @param isHome
	 *            Boolean
	 * @return TeamMatchStats
	 */
	private TeamMatchStats buildTeamMatchStats(Boolean isHome) {
		Long teamId = 0L;
		Long opposingTeamId = 0L;
		if (isHome) {
			teamId = home.getId();
			opposingTeamId = visiting.getId();
		} else {
			teamId = visiting.getId();
			opposingTeamId = home.getId();
		}

		TeamMatchStats teamMatchStats = new TeamMatchStats();
		try {
			PropertyUtils.copyProperties(buildTeamActionStats(isHome, teamId, opposingTeamId), teamMatchStats);
		} catch (Exception e) {
			logger.error("copy property from TeamActionStats to TeamMatchStats failed, matchId = " + matchId + ", teamId = " + teamId, e);
			return teamMatchStats;
		}

		teamMatchStats.setLeagueId(leagueId);
		teamMatchStats.setLeagueStageId(leagueStageId);
		teamMatchStats.setRoundId(roundId);
		teamMatchStats.setMatchId(matchId);
		teamMatchStats.setHomeScore(homeScore);
		teamMatchStats.setVisitingScore(visitingScore);
		teamMatchStats.setMatchTime(matchTime);
		if (isHome) {
			teamMatchStats.setTeamId(home.getId());
			teamMatchStats.setTeamName(home.getName());
			teamMatchStats.setTeamLogo(home.getLogo());
			teamMatchStats.setIsHomeCourt(true);
			teamMatchStats.setIsWin(homeScore > visitingScore ? true : false);
			teamMatchStats.setOpposingTeamId(visiting.getId());
			teamMatchStats.setOpposingTeamName(visiting.getName());
		} else {
			teamMatchStats.setTeamId(visiting.getId());
			teamMatchStats.setTeamName(visiting.getName());
			teamMatchStats.setTeamLogo(visiting.getLogo());
			teamMatchStats.setIsHomeCourt(false);
			teamMatchStats.setIsWin(visitingScore > homeScore ? true : false);
			teamMatchStats.setOpposingTeamId(home.getId());
			teamMatchStats.setOpposingTeamName(home.getName());
		}

		return teamMatchStats;
	}

	/**
	 * 构建两只球队比赛的数据统计
	 * 
	 * @return Map<String, TeamMatchStats>
	 */
	public Map<String, TeamMatchStats> buildTeamMatchStatsForTeam() {
		logger.info("start to invoke buildTeamMatchStatsForTeam(), matchId = " + matchId);
		if (home == null || visiting == null) {
			logger.error("can not build TeamMatchStats due to home or visiting is empty, matchId = " + matchId);
			return null;
		}
		Map<String, TeamMatchStats> stats = Maps.newHashMap();
		stats.put(ResponseParams.HOME, buildTeamMatchStats(true));
		stats.put(ResponseParams.VISITING, buildTeamMatchStats(false));
		logger.info("build teamMatchStatsForTeam successfully, matchId = " + matchId);
		return stats;
	}

	/**
	 * 构建两只球队报名球员的技术统计
	 * 
	 * @return Map<String, List<PlayerMatchStats>>
	 */
	public Map<String, List<PlayerMatchStats>> buildPlayerMatchStatsForTeam() {
		logger.info("start to invoke buildPlayerMatchStatsForTeam(), matchId = " + matchId);
		if (home == null || visiting == null) {
			logger.error("can not build PlayerMatchStats list due to home or visiting is empty, matchId = " + matchId);
			return null;
		}
		if (CollectionUtils.isEmpty(homePlayerList) || CollectionUtils.isEmpty(visitingPlayerList)) {
			logger.error("can not build PlayerMatchStats list due to playerList is empty, matchId = " + matchId);
			return null;
		}
		Map<String, List<PlayerMatchStats>> playerStats = Maps.newHashMap();
		playerStats.put(ResponseParams.HOME, buildPlayerMatchStatsList(true));
		playerStats.put(ResponseParams.VISITING, buildPlayerMatchStatsList(false));
		logger.info("build playerMatchStats successfully, matchId = " + matchId);
		return playerStats;
	}

	private List<PlayerMatchStats> buildPlayerMatchStatsList(Boolean isHome) {
		List<Player> playerList = isHome ? homePlayerList : visitingPlayerList;
		Long teamId = 0L;
		Long opposingTeamId = 0L;
		if (isHome) {
			teamId = home.getId();
			opposingTeamId = visiting.getId();
		} else {
			teamId = visiting.getId();
			opposingTeamId = home.getId();
		}
		// 计算球员的数据统计之前先计算出球队的数据统计(球员部分数据统计依赖于球队的数据统计)
		TeamActionStats teamActionStats = buildTeamActionStats(isHome, teamId, opposingTeamId);
		List<PlayerMatchStats> playerMatchStatsList = Lists.newArrayList();
		for (Player player : playerList) {
			playerMatchStatsList.add(buildPlayerMatchStats(isHome, player, teamActionStats));
		}
		return playerMatchStatsList;
	}

	private TeamActionStats buildTeamActionStats(Boolean isHome, Long teamId, Long opposingTeamId) {
		return new TeamActionStats(teamId, opposingTeamId, (isHome ? visitingScore : homeScore), actionList);
	}

	/**
	 * 构建球员比赛的统计数据
	 *
	 * @param teamId
	 *            Long
	 * @param player
	 *            Player
	 * @return PlayerMatchStats
	 */
	private PlayerMatchStats buildPlayerMatchStats(Boolean isHome, Player player, TeamActionStats teamActionStats) {
		Long playerId = player.getId();
		if (playerId == null || playerId <= 0) {
			logger.error("can not build PlayerMatchStats due to playerId is invalid");
			return null;
		}
		logger.info("start to build PlayerMatchStats, playerId = " + playerId);

		PlayerMatchStats playerMatchStats = new PlayerMatchStats();
		try {
			PropertyUtils.copyProperties(new PlayerActionStats(isHome, playerId, actionList, teamActionStats), playerMatchStats);
		} catch (Exception e) {
			logger.error(
					"copy property from PlayerActionStats to PlayerMatchStats failed, matchId = " + matchId + ", playerId = " + playerId,
					e);
			return playerMatchStats;
		}
		playerMatchStats.setPlayerId(playerId);
		playerMatchStats.setPlayerName(player.getName());
		playerMatchStats.setUniformNumber(player.getUniformNumber());
		playerMatchStats.setLeagueId(getLeagueId());
		playerMatchStats.setLeagueStageId(getLeagueStageId());
		playerMatchStats.setMatchId(getMatchId());
		playerMatchStats.setRoundId(getRoundId());
		playerMatchStats.setMatchTime(getMatchTime());
		playerMatchStats.setTeamId(isHome ? home.getId() : visiting.getId());
		playerMatchStats.setOpposingTeamId(isHome ? visiting.getId() : home.getId());
		playerMatchStats.setPlayingTime(getPlayingTime(playerId));

		return playerMatchStats;
	}

	/**
	 * 获取球员的上场时间
	 * 
	 * @param playerId
	 *            Long
	 * @return Long
	 */
	public Long getPlayingTime(Long playerId) {
		if (MapUtils.isEmpty(playingTimes)) {
			return 0L;
		}
		return MapUtils.getLongValue(playingTimes, playerId, 0L);
	}

	/***
	 * 获取比赛的基本信息
	 * 
	 * @return Map<String, Object>
	 */
	@JsonIgnore
	public Map<String, Object> getMatchBasicInfo() {
		Map<String, Object> matchInfo = Maps.newHashMap();
		matchInfo.put(ResponseParams.MATCH_ID, getMatchId());
		matchInfo.put(ResponseParams.MATCH_FORMAT, matchFormat);
		matchInfo.put(ResponseParams.MATCH_TIME, getMatchTime());
		matchInfo.put(ResponseParams.HOME_ID, home.getId());
		matchInfo.put(ResponseParams.HOME_NAME, home.getName());
		matchInfo.put(ResponseParams.HOME_LOGO, home.getLogo());
		matchInfo.put(ResponseParams.HOME_SCORE, getHomeScore());
		matchInfo.put(ResponseParams.VISITING_ID, visiting.getId());
		matchInfo.put(ResponseParams.VISITING_NAME, visiting.getName());
		matchInfo.put(ResponseParams.VISITING_LOGO, visiting.getLogo());
		matchInfo.put(ResponseParams.VISITING_SCORE, getVisitingScore());
		int matchStatus = getMatchStatus();
		matchInfo.put(ResponseParams.MATCH_STATUS, matchStatus);
		matchInfo.put(ResponseParams.STATUS_NAME, MatchStatus.getName(matchStatus));
		matchInfo.put(ResponseParams.QUARTER_NAME, MatchStatus.getName(matchStatus, true));
		matchInfo.put(ResponseParams.QUARTER_REMAINING_TIME, getQuarterRemainingTime());
		return matchInfo;
	}

	/**
	 * 撤销操作
	 *
	 * @param teamId
	 */
	public Long revert(Long teamId) {
		logger.debug("revert action, teamId=" + teamId);
		List<Action> actions = getActionList();
		if (actions == null) {
			throw new BusinessException("没有可撤销的操作!", ErrorCode.INVALID_ACTION.code());
		}
		Action action = actions.get(actions.size() - 1);
		if (action == null) {
			logger.debug("last action is null, can not be revoked");
			return 1l;
		}
		int lastActionStatus = action.getStatus();
		if (ActionStatus.canNotRevoke(lastActionStatus)) {
			logger.debug("last action: " + lastActionStatus + "  can not be revoked");
			throw new BusinessException("match status can not revert", ErrorCode.INVALID_ACTION.code());
		}
		switch (action.getStatus()) {
		// 换人
		case ActionStatus.SUBSTITUTE:
			revertSubstitute(action);
			actions.remove(action);
			return action.getId();
		// 罚球(进)
		case ActionStatus.FREE_THROW_IN:
			subScore(action.getTeamId(), 1);
			updateMatchData(action, -1);
			actions.remove(action);
			return action.getId();
		// 罚球(丢)
		case ActionStatus.FREE_THROW_OUT:
			actions.remove(action);
			return action.getId();
		// 2分(中)
		case ActionStatus.TWO_LEFTSIDE_STANDING_IN:
		case ActionStatus.TWO_LEFTSIDE_DRIBBLING_IN:
		case ActionStatus.TWO_RIGHTSIDE_STANDING_IN:
		case ActionStatus.TWO_RIGHTSIDE_DRIBBLING_IN:
		case ActionStatus.TWO_PAINTED_IN:
		case ActionStatus.TWO_FRONT_STANDING_IN:
		case ActionStatus.TWO_FRONT_DRIBBLING_IN:
			subScore(action.getTeamId(), 2);
			updateMatchData(action, -2);
			actions.remove(action);
			return action.getId();
		// 2分(不中)
		case ActionStatus.TWO_LEFTSIDE_STANDING_OUT:
		case ActionStatus.TWO_LEFTSIDE_DRIBBLING_OUT:
		case ActionStatus.TWO_RIGHTSIDE_STANDING_OUT:
		case ActionStatus.TWO_RIGHTSIDE_DRIBBLING_OUT:
		case ActionStatus.TWO_PAINTED_OUT:
		case ActionStatus.TWO_FRONT_STANDING_OUT:
		case ActionStatus.TWO_FRONT_DRIBBLING_OUT:
			actions.remove(action);
			return action.getId();
		// 3分(命中)
		case ActionStatus.THREE_LEFT_WING_STANDING_IN:
		case ActionStatus.THREE_LEFT_WING_DRIBBLING_IN:
		case ActionStatus.THREE_LEFTSIDE45_STANDING_IN:
		case ActionStatus.THREE_LEFTSIDE45_DRIBBLING_IN:
		case ActionStatus.THREE_ROOT_ARC_STANDING_IN:
		case ActionStatus.THREE_ROOT_ARC_DRIBBLING_IN:
		case ActionStatus.THREE_RIGHTSIDE45_STANDING_IN:
		case ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_IN:
		case ActionStatus.THREE_RIGHT_WING_STANDING_IN:
		case ActionStatus.THREE_RIGHT_WING_DRIBBLING_IN:
			subScore(action.getTeamId(), 3);
			updateMatchData(action, -3);
			actions.remove(action);
			return action.getId();
		// 3分(不中)
		case ActionStatus.THREE_LEFT_WING_STANDING_OUT:
		case ActionStatus.THREE_LEFT_WING_DRIBBLING_OUT:
		case ActionStatus.THREE_LEFTSIDE45_STANDING_OUT:
		case ActionStatus.THREE_LEFTSIDE45_DRIBBLING_OUT:
		case ActionStatus.THREE_ROOT_ARC_STANDING_OUT:
		case ActionStatus.THREE_ROOT_ARC_DRIBBLING_OUT:
		case ActionStatus.THREE_RIGHTSIDE45_STANDING_OUT:
		case ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_OUT:
		case ActionStatus.THREE_RIGHT_WING_STANDING_OUT:
		case ActionStatus.THREE_RIGHT_WING_DRIBBLING_OUT:
			actions.remove(action);
			return action.getId();
		// 助攻
		case ActionStatus.ASSIST:
			// 前场篮板
		case ActionStatus.OFFENSIVE_REBOUND:
			// 后场篮板
		case ActionStatus.DEFENSIVE_REBOUND:
			// 盖帽
		case ActionStatus.BLOCKSHOT:
			// 抢断
		case ActionStatus.STEAL:
			// 失误
		case ActionStatus.TURNOVER:
			// 进攻犯规
		case ActionStatus.OFFENSIVE_FOUL:
			// 防守犯规
		case ActionStatus.DEFENSIVE_FOUL:
			// 技术犯规
		case ActionStatus.TECHNICAL_FOUL:
			// 其他犯规
		case ActionStatus.FLAGRANT_FOUL:
		case ActionStatus.UNSPORTSMANLIKE_FOUL:
			actions.remove(action);
			return action.getId();

		default:
			break;
		}
		return 0l;
	}

	/**
	 * 撤销换人
	 * 
	 * @param action
	 *            Action
	 */
	private void revertSubstitute(Action action) {
		Map<String, Object> extension = action.getExtensionMap();
		if (extension == null || extension.isEmpty()) {
			logger.error("no down/up player in action.extension, could not revert substitute");
			throw new BusinessException(String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.EXTENSION),
					ErrorCode.FIELD_NOT_NULL.code());
		}
		// FIXME 从Map里获取的List不能指定泛型为Long
		List<Long> upPlayerIdList = JsonUtil.toObject(JsonUtil.toJson(extension.get(BasketballConstants.KEY_CHANGE_UP)), List.class,
				Long.class);
		List<Long> downPlayerIdList = JsonUtil.toObject(JsonUtil.toJson(extension.get(BasketballConstants.KEY_CHANGE_DOWN)), List.class,
				Long.class);
		if (CollectionUtils.isEmpty(upPlayerIdList) || CollectionUtils.isEmpty(downPlayerIdList)) {
			logger.error("no down/up player in action.extension, could not revert substitute");
			throw new BusinessException(String.format(ResponseMsgConstants.PARAMETER_INVALID, RequestParams.EXTENSION),
					ErrorCode.FIELD_VALUE_INVALID.code());
		}

		substitute(action.getTeamId(), upPlayerIdList, downPlayerIdList);
	}

	/**
	 * 更新小节分数
	 * 
	 * @param action
	 */
	public void updateMatchData(Action action, Integer score) {
		updateMatchData(action.getTeamId(), score);
	}

	/**
	 * 更新小节分数
	 * 
	 * @param action
	 */
	public void updateMatchData(Long teamId, Integer score) {
		if (quarterDataList == null || quarterDataList.isEmpty()) {
			return;
		}
		QuarterData quarterData = quarterDataList.get(quarterDataList.size() - 1);
		quarterData.updateScore(home.getId().equals(teamId), score);
	}

	public void updateMatchData(Player player) {
		if (quarterDataList == null || quarterDataList.isEmpty()) {
			return;
		}
		QuarterData quarterData = quarterDataList.get(quarterDataList.size() - 1);
		if (quarterData == null) {
			quarterData = new QuarterData();
		}
		quarterData.setHomeScore(homeScore);
		quarterData.setVisitingScore(visitingScore);
		quarterData.updatePlayingTime(isHome(player.getTeamId()), player.getId(), player.getPlayingTime());
	}

	/**
	 * 获取状态数组相应的Actions
	 * 
	 * @param statuses
	 * @return
	 */
	public List<Action> getActionList(int[] statuses) {
		List<Action> reulst = new ArrayList<Action>();
		if (actionList != null && !actionList.isEmpty()) {
			OUTER: for (Action action : actionList) {
				for (int status : statuses) {
					if (action.getStatus().equals(status)) {
						try {
							reulst.add(action);
						} catch (Exception e) {
							e.printStackTrace();
						}
						continue OUTER;
					}
				}
			}
		}

		return reulst;
	}

	/**
	 * 判断是否时间停止状态
	 * 
	 * @return boolean
	 */
	public boolean isStop() {
		if (isSuspend || matchStatus == ActionStatus.NOT_START || matchStatus == ActionStatus.QUARTER_END
				|| matchStatus == ActionStatus.STOP_TIME) {
			return true;
		}
		return false;
	}

	/**
	 * 删除action
	 * 
	 * @param actionId
	 */
	public void deleteAction(Long actionId) {
		if (actionList != null && actionList.size() != 0) {
			for (Action actionDto : actionList) {
				if (actionDto.getId().equals(actionId)) {
					actionList.remove(actionDto);
					break;
				}
			}
		}
	}

	/**
	 * 获取所有的球员
	 * 
	 * @return List<Player>
	 */
	@JsonIgnore
	public List<Player> getAllLineupPlayers() {
		List<Player> players = new ArrayList<>();
		if (homePlayerList != null && homePlayerList.size() > 0) {
			for (Player player : homePlayerList) {
				if (homeLineupSet.contains(player.getId())) {
					players.add(player);
				}
			}
		}
		if (visitingPlayerList != null && visitingPlayerList.size() > 0) {
			for (Player player : visitingPlayerList) {
				if (visitingLineupSet.contains(player.getId())) {
					players.add(player);
				}
			}
		}
		return players;
	}

	/**
	 * 获取最新的话术
	 * 
	 * @param maxResult
	 *            Integer
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> getMatchSpeaks(int maxResult) {
		if (CollectionUtils.isEmpty(actionList) || maxResult <= 0) {
			return null;
		}
		List<Map<String, Object>> matchSpeaks = Lists.newArrayList();
		Map<String, Object> speak = null;
		int actionListSize = actionList.size();
		int i = 0;
		while (i < actionListSize && i < maxResult) {
			Action action = actionList.get(actionListSize - 1 - i);
			speak = Maps.newHashMap();
			speak.put(ResponseParams.ID, action.getId());
			speak.put(ResponseParams.STATUS, action.getStatus());
			speak.put(ResponseParams.EXTENSION, action.getExtension());
			speak.put(ResponseParams.SPEAK, action.getSpeaks());
			speak.put(ResponseParams.CREATE_TIME, action.getCreateTime());
			matchSpeaks.add(speak);
			i++;
		}
		return matchSpeaks;
	}

	public boolean isHome(Long teamId) {
		if (home.getId().equals(teamId)) {
			return true;
		}
		return false;
	}

	public boolean isOvertime() {
		if (quarterIndex > quarterCount) {
			return true;
		}
		return false;
	}

	public void initQuarterData() {
		quarterDataList.add(new QuarterData());
	}

	public int getQuarterIndex() {
		return quarterIndex;
	}

	public void setQuarterIndex(int quarterIndex) {
		this.quarterIndex = quarterIndex;
	}

	public void increaseQuarter() {
		this.quarterIndex += 1;
	}

	public int getOvertimeQuarterIndex() {
		return quarterIndex - quarterCount;
	}

	@JsonIgnore
	public Map<Long, Long> getPlayingTimes() {
		return playingTimes;
	}

	public void setPlayingTimes(Map<Long, Long> playingTimes) {
		this.playingTimes = playingTimes;
	}

	public int getQuarterCount() {
		return quarterCount;
	}

	public void setQuarterCount(int quarterCount) {
		this.quarterCount = quarterCount;
	}

	public int getQuarterTime() {
		return quarterTime;
	}

	public void setQuarterTime(int quarterTime) {
		this.quarterTime = quarterTime;
	}

	@JsonIgnore
	public List<QuarterData> getQuarterDataList() {
		return quarterDataList;
	}

	public void setQuarterDataList(List<QuarterData> quarterDataList) {
		this.quarterDataList = quarterDataList;
	}

	public int getQuarterRemainingTime() {
		return quarterRemainingTime;
	}

	public void setQuarterRemainingTime(int quarterRemainingTime) {
		this.quarterRemainingTime = quarterRemainingTime;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long gameId) {
		this.matchId = gameId;
	}

	public Team getHome() {
		return home;
	}

	public void setHome(Team home) {
		this.home = home;
	}

	public Team getVisiting() {
		return visiting;
	}

	public void setVisiting(Team visiting) {
		this.visiting = visiting;
	}

	public List<Player> getHomePlayerList() {
		return homePlayerList;
	}

	public void setHomePlayerList(List<Player> homePlayerList) {
		this.homePlayerList = homePlayerList;
	}

	public List<Player> getVisitingPlayerList() {
		return visitingPlayerList;
	}

	public void setVisitingPlayerList(List<Player> visitingPlayerList) {
		this.visitingPlayerList = visitingPlayerList;
	}

	@JsonIgnore
	public List<Action> getActionList() {
		return actionList;
	}

	public void setActionList(List<Action> actionList) {
		this.actionList = actionList;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	public int getVisitingScore() {
		return visitingScore;
	}

	public void setVisitingScore(int visitingScore) {
		this.visitingScore = visitingScore;
	}

	public Date getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(Date matchTime) {
		this.matchTime = matchTime;
	}

	public int getMatchFormat() {
		return matchFormat;
	}

	public void setMatchFormat(int matchFormat) {
		this.matchFormat = matchFormat;
	}

	public int getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(int matchStatus) {
		this.matchStatus = matchStatus;
	}

	public User getHomeRecorder() {
		return homeRecorder;
	}

	public void setHomeRecorder(User homeRecorder) {
		this.homeRecorder = homeRecorder;
	}

	public User getVisitingRecorder() {
		return visitingRecorder;
	}

	public void setVisitingRecorder(User visitingRecorder) {
		this.visitingRecorder = visitingRecorder;
	}

	public boolean isSuspend() {
		return isSuspend;
	}

	public void setSuspend(boolean suspend) {
		isSuspend = suspend;
	}

	public Set<Long> getHomeLineupSet() {
		return homeLineupSet;
	}

	public void setHomeLineupSet(Set<Long> homeLineupSet) {
		this.homeLineupSet = homeLineupSet;
	}

	public Set<Long> getVisitingLineupSet() {
		return visitingLineupSet;
	}

	public void setVisitingLineupSet(Set<Long> visitingLineupSet) {
		this.visitingLineupSet = visitingLineupSet;
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getLeagueStageId() {
		return leagueStageId;
	}

	public void setLeagueStageId(Long leagueStageId) {
		this.leagueStageId = leagueStageId;
	}

	public Long getRoundId() {
		return roundId;
	}

	public void setRoundId(Long roundId) {
		this.roundId = roundId;
	}

}
