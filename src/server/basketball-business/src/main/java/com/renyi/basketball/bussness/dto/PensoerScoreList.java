package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

import net.sf.json.JSONObject;

/**
 * Created by Roy.xiao on 2016/6/12.
 */
public class PensoerScoreList implements Comparable<PensoerScoreList>, Serializable {
	/** 球员ID */
	private Long playerId;
	/** 球员名字. */
	private String name;
	/** 所在球队名字 . */
	private String teamName;
	/*    *//** 进球数. *//*
						 * private Integer in = 0;
						 */
	/** 比赛场次 */
	private Integer count = 0;
	/** 总分 */
	private Integer score = 0;
	/** 所在联赛. */
	private Long leagueId;
	/** 场均得分. */
	private double avgScore = 0;
	/** 助攻 */
	private Integer help = 0;
	/** 场均助攻 */
	private double avgHelp = 0;

	public double getAvgHelp() {
		return avgHelp;
	}

	public void setAvgHelp(double avgHelp) {
		this.avgHelp = avgHelp;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public double getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(double avgScore) {
		this.avgScore = avgScore;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public void getShooter(String action) {
		try {
			JSONObject JsonAction = JSONObject.fromObject(action);
			JSONObject playerA = JsonAction.getJSONObject("playerA");
			JSONObject playerB = JsonAction.getJSONObject("playerB");
			if (playerA != null && playerA.size() != 0) {
				playerId = playerA.getLong("id");
				name = playerA.getString("name");
			} else if (playerB != null && playerB.size() != 0) {
				playerId = playerB.getLong("id");
				name = playerB.getString("name");
			}

		} catch (Exception e) {
			e.printStackTrace();
			JSONObject JsonAction = JSONObject.fromObject(action);
			JSONObject playerA = JsonAction.getJSONObject("playerA");
			JSONObject playerB = JsonAction.getJSONObject("playerB");
			if (playerA != null && playerA.size() != 0) {
				playerId = playerA.getLong("id");
				name = playerA.getString("name");
			} else if (playerB != null && playerB.size() != 0) {
				playerId = playerB.getLong("id");
				name = playerB.getString("name");
			}
		}
	}

	public Integer getHelp() {
		return help;
	}

	public void setHelp(Integer help) {
		this.help = help;
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/*
	 * public Integer getIn() { return in; }
	 * 
	 * public void setIn(Integer in) { this.in = in; }
	 */

	@Override
	public int compareTo(PensoerScoreList o) {
		if (this.avgScore > o.avgScore) {
			return -1;// 降序
		} else if (this.avgScore < o.avgScore) {
			return 1;
		} else {
			if (!this.playerId.equals(o.playerId)) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return false;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PensoerScoreList other = (PensoerScoreList) obj;
		if (this.playerId.equals(other.playerId)) {
			return true;
		} else {
			return false;
		}
	}

	public void countIn(Integer sco) {
		this.score = this.score + sco;
	}

	public void avgScore() {
		this.avgScore = Double.parseDouble(String.format("%.2f", this.score * 1.0 / this.count));
	}
	public void avgHelp() {
		this.avgHelp = Double.parseDouble(String.format("%.2f", this.help * 1.0 / this.count));
	}
}
