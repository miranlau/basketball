package com.renyi.basketball.bussness.dto;

import com.renyi.basketball.bussness.po.User;

/**
 * Created by Administrator on 2016/6/13 0013.
 */
public class PerformanceDto {
    /**
     * 球员射门数
     */
    Integer uShootNum = 0;
    /**
     * 球员助攻数
     */
    Integer uAssistsNum = 0;
    /**
     * 球员抢断数
     */
    Integer uStealsNum = 0;
    /**
     * 球员扑救数
     */
    Integer uSavesNum = 0;
    /**
     * 球员犯规数
     */
    Integer uFoulNum = 0;
    /**
     * 射门占球队比例
     */
    Double shoot = 0.00;
    /**
     * 助攻占球队比例
     */
    Double assists = 0.00;
    /**
     * 抢断占球队比例
     */
    Double steals = 0.00;
    /**
     * 扑救占球队比例
     */
    Double saves = 0.00;
    /**
     * 犯规占球队比例
     */
    Double foul = 0.00;

    User user;
    public Double getShoot() {
        return shoot;
    }

    public void setShoot(Double shoot) {
        this.shoot = shoot;
    }

    public Double getAssists() {
        return assists;
    }

    public void setAssists(Double assists) {
        this.assists = assists;
    }

    public Double getSteals() {
        return steals;
    }

    public void setSteals(Double steals) {
        this.steals = steals;
    }

    public Double getSaves() {
        return saves;
    }

    public void setSaves(Double saves) {
        this.saves = saves;
    }

    public Double getFoul() {
        return foul;
    }

    public void setFoul(Double foul) {
        this.foul = foul;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getuShootNum() {
        return uShootNum;
    }

    public void setuShootNum(Integer uShootNum) {
        this.uShootNum = uShootNum;
    }

    public Integer getuAssistsNum() {
        return uAssistsNum;
    }

    public void setuAssistsNum(Integer uAssistsNum) {
        this.uAssistsNum = uAssistsNum;
    }

    public Integer getuStealsNum() {
        return uStealsNum;
    }

    public void setuStealsNum(Integer uStealsNum) {
        this.uStealsNum = uStealsNum;
    }

    public Integer getuSavesNum() {
        return uSavesNum;
    }

    public void setuSavesNum(Integer uSavesNum) {
        this.uSavesNum = uSavesNum;
    }

    public Integer getuFoulNum() {
        return uFoulNum;
    }

    public void setuFoulNum(Integer uFoulNum) {
        this.uFoulNum = uFoulNum;
    }
}
