package com.renyi.basketball.bussness.dto;

import net.sf.json.JSONObject;

/**
 * Created by Roy.xiao on 2016/6/22.
 */
public class PlaceScheduleDto {
    private Integer id;
    /** 场馆ID. */
    private Integer arenas_id;
    /** 场地ID. */
    private Integer places_id;
    /** 年. */
    private Integer year;
    /** 月. */
    private Integer month;
    /** 日。 */
    private Integer day;
    /** 开始时间. */
    private String start_time;
    /** 结束时间. */
    private String end_time;
    /** 价格. */
    private Double price;
    /** 状态 . */
    private Integer status;
    /** 详情. */
    private JSONObject scheduleDetail;
    /** 订单ID. */
    private Integer order_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getArenas_id() {
        return arenas_id;
    }

    public void setArenas_id(Integer arenas_id) {
        this.arenas_id = arenas_id;
    }

    public Integer getPlaces_id() {
        return places_id;
    }

    public void setPlaces_id(Integer places_id) {
        this.places_id = places_id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public JSONObject getScheduleDetail() {
        return scheduleDetail;
    }

    public void setScheduleDetail(JSONObject scheduleDetail) {
        this.scheduleDetail = scheduleDetail;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }
}
