package com.renyi.basketball.bussness.dto;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.renyi.basketball.bussness.common.StatsAlgorithm;
import com.renyi.basketball.bussness.po.Action;

/**
 * 按球员统计比赛的Action
 */
public final class PlayerActionStats extends ActionStats {
	// 进攻占比
	private float offensiveRatio;
	// 助攻率
	private float assistPct;
	// 篮板率
	private float reboundPct;
	// 效率值
	private float per;
	// 正负值
	private int rpm;

	public PlayerActionStats(Boolean isHome, Long playerId, List<Action> actionList, TeamActionStats teamActionStats) {
		if (CollectionUtils.isEmpty(actionList)) {
			return;
		}
		this.actionList = actionList;

		init(playerId);

		calcShottingArea();

		calcShottingWay();

		calcOffensivePct();

		calcScore();

		calcAdvanced(teamActionStats);
	}

	private void init(Long playerId) {
		for (Action action : actionList) {
			if (playerId.equals(action.getPlayerId())) {
				countByStatus(action);
			}
		}
	}

	private void calcAdvanced(TeamActionStats teamActionStats) {
		super.calcAdvanced();

		// 进攻占比
		offensiveRatio = StatsAlgorithm.percent(totalOffensiveAttempt, teamActionStats.getOffensiveAttempt());
		// 助攻率
		assistPct = StatsAlgorithm.percent(assist, teamActionStats.getAssist());
		// 篮板率
		reboundPct = StatsAlgorithm.percent(getRebound(), (teamActionStats.getRebound() + teamActionStats.getOpposingRebound()));
		// 失误率
		turnoverPct = StatsAlgorithm.percent(turnover, teamActionStats.getTurnover());
		// 效率值
		per = (score + getRebound() + assist + steal + blockShot) - (fieldGoalAttempt - fieldGoalShot) - (freeThrowAttempt - freeThrowShot)
				- turnover;
		// 正负值
		// rpm = 0; TODO fengwei
	}

	public float getOffensiveRatio() {
		return offensiveRatio;
	}

	public float getAssistPct() {
		return assistPct;
	}

	public float getReboundPct() {
		return reboundPct;
	}

	public float getPer() {
		return per;
	}

	public int getRpm() {
		return rpm;
	}

}
