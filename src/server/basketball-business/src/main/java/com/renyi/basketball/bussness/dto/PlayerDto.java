package com.renyi.basketball.bussness.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.sf.json.JSONObject;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by wangqy on 2016/5/9 0009. 球员dto
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PlayerDto implements Serializable {
	private static final long serialVersionUID = -2689369198966349547L;
	// 球员id
	private Long id;
	// 球员姓名
	private String name;
	// 头像
	private String avatar;

	private String username;
	// 升高
	private Integer height;
	// 体重
	private Integer weight;
	// 年龄
	private Integer age;
	// 位置
	private String position;
	// 星级
	private Integer stars;
	// 比赛数
	private Integer matches;
	// 信誉
	private Integer credit;
	// 等级
	private Integer level;
	// 球衣号码
	private Integer uniformNumber;
	// 属性
	private RadarDto radarDto;
	// 所属球队
	private List<TeamDto> teams;
	// 手机号码
	private String mobile;
	// 相对当前用户是否关注
	private Boolean follow;
	// 真实姓名
	private String realName;
	/**
	 * 平场次数
	 */
	private Integer deuce;
	/** 我的关注. */
	private Integer myFocus;
	/** 我的发表. */
	private Integer myPublish;
	/** 我的粉丝． */
	private Integer myFans;
	/** 上场时间 */
	private Date onTime;
	/** 是否是队长 */
	private Integer isCaptain = 0;
	private long playTime;
	private Boolean canview;
	/** 球员统计 */
	private Count playerCount;

	/** 是否上场 */
	private boolean isLineUp;

	/* 位置x坐标 */
	private Double x;
	/* 位置y坐标 */
	private Double y;

	/** 总得分 */
	private Integer score;
	/** 总篮板 */
	private Integer backboard;
	/** 场均得分 */
	private double scoreCount;
	/** 两分命中率 */
	private double two_rate;
	/** 三分命中率 */
	private double three_rate;
	/** 罚球命中率 */
	private double Freethrow_rate;
	/** 场均盖帽 */
	private double avgCover;
	/** 场均抢断 */
	private double avgSteals;
	/** 场均篮板 */
	private double avgBackboard;
	/** 场均助攻 */
	private double avgHelp;
	/** 进球数 */
	private int two_three;
	/** 参赛总场数 */
	private int joinCount;
	/** 场均失误 */
	private double avgAnError;

	public double getAvgAnError() {
		return avgAnError;
	}

	public void setAvgAnError(double avgAnError) {
		this.avgAnError = avgAnError;
	}

	public Integer getBackboard() {
		return backboard;
	}

	public void setBackboard(Integer backboard) {
		this.backboard = backboard;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public int getJoinCount() {
		return joinCount;
	}

	public void setJoinCount(int joinCount) {
		this.joinCount = joinCount;
	}

	public int getTwo_three() {
		return two_three;
	}

	public void setTwo_three(int two_three) {
		this.two_three = two_three;
	}

	public double getScoreCount() {
		return scoreCount;
	}

	public void setScoreCount(double scoreCount) {
		this.scoreCount = scoreCount;
	}

	public double getTwo_rate() {
		return two_rate;
	}

	public void setTwo_rate(double two_rate) {
		this.two_rate = two_rate;
	}

	public double getThree_rate() {
		return three_rate;
	}

	public void setThree_rate(double three_rate) {
		this.three_rate = three_rate;
	}

	public double getFreethrow_rate() {
		return Freethrow_rate;
	}

	public void setFreethrow_rate(double freethrow_rate) {
		Freethrow_rate = freethrow_rate;
	}

	public double getAvgCover() {
		return avgCover;
	}

	public void setAvgCover(double avgCover) {
		this.avgCover = avgCover;
	}

	public double getAvgSteals() {
		return avgSteals;
	}

	public void setAvgSteals(double avgSteals) {
		this.avgSteals = avgSteals;
	}

	public double getAvgBackboard() {
		return avgBackboard;
	}

	public void setAvgBackboard(double avgBackboard) {
		this.avgBackboard = avgBackboard;
	}

	public double getAvgHelp() {
		return avgHelp;
	}

	public void setAvgHelp(double avgHelp) {
		this.avgHelp = avgHelp;
	}

	public Integer getMyFocus() {
		return myFocus;
	}

	public void setMyFocus(Integer myFocus) {
		this.myFocus = myFocus;
	}

	public Integer getMyPublish() {
		return myPublish;
	}

	public long getPlayTime() {
		return playTime;
	}

	public Date getOnTime() {
		return onTime;
	}

	public void setOnTime(Date onTime) {
		this.onTime = onTime;
	}

	public void setMyPublish(Integer myPublish) {
		this.myPublish = myPublish;
	}

	public Integer getMyFans() {
		return myFans;
	}

	public void setMyFans(Integer myFans) {
		this.myFans = myFans;
	}

	public Boolean getFollow() {
		return follow;
	}

	public Integer getDeuce() {
		return deuce;
	}

	public void setDeuce(Integer deuce) {
		this.deuce = deuce;
	}

	public void setFollow(Boolean follow) {
		this.follow = follow;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public List<TeamDto> getTeams() {
		return teams;
	}

	public void setTeams(List<TeamDto> teams) {
		this.teams = teams;
	}

	public void setMatches(Integer matches) {
		this.matches = matches;
	}

	public void setRadarDto(RadarDto radarDto) {
		this.radarDto = radarDto;
	}

	public RadarDto getRadarDto() {
		return radarDto;
	}

	public Integer getMatches() {
		return matches;
	}

	public void setPlayTime(Long playTime) {
		this.playTime = playTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public void setMatchs(Integer matchs) {
		this.matches = matchs;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public void setUniformNumber(Integer uniformNumber) {
		this.uniformNumber = uniformNumber;
	}

	public String getName() {

		return name;
	}

	public Long getId() {
		return id;
	}

	public Integer getUniformNumber() {
		return uniformNumber;
	}

	public Integer getLevel() {
		return level;
	}

	public Integer getMatchs() {
		return matches;
	}

	public Integer getStars() {
		return stars;
	}

	public String getPosition() {
		return position;
	}

	public Integer getAge() {
		return age;
	}

	public String getAvatar() {
		if (null != avatar && !avatar.startsWith("http://")) {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();

			return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/" + avatar;
		}
		return avatar;
	}

	public Integer getWeight() {
		return weight;
	}

	public Integer getHeight() {
		return height;
	}

	public void setCredit(Integer credit) {
		this.credit = credit;
	}

	public Integer getCredit() {

		return credit;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (getId().equals(o)) {
			return true;
		}
		if (o == null || getClass() != o.getClass())
			return false;

		PlayerDto playerDto = (PlayerDto) o;

		return id.equals(playerDto.id);

	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * 转换为Json.
	 */
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		json.put("id", id);
		json.put("avatar", getAvatar());
		json.put("number", uniformNumber);
		json.put("name", name);
		json.put("playTime", playTime);
		return json;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Boolean getCanview() {
		return canview;
	}

	public void setCanview(Boolean canview) {
		this.canview = canview;
	}

	public Integer getIsCaptain() {
		return isCaptain;
	}

	public void setIsCaptain(Integer isCaptain) {
		this.isCaptain = isCaptain;
	}

	@Override
	public String toString() {
		return "PlayerDto{" + "playTime=" + playTime + ", id=" + id + ", name='" + name + '\'' + ", avatar='" + avatar
				+ '\'' + ", number=" + uniformNumber + '}';
	}

	public boolean isLineUp() {
		return isLineUp;
	}

	public void setLineUp(boolean up) {
		this.isLineUp = up;
	}

	public boolean getUp() {
		return isLineUp;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}
}
