package com.renyi.basketball.bussness.dto;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import jersey.repackaged.com.google.common.collect.Maps;

public class QuarterData implements Serializable {
	private static final long serialVersionUID = -6731516077999276498L;
	@JsonProperty
	private Map<Long, Long> homePlayingTime = Maps.newHashMap();
	@JsonProperty
	private Map<Long, Long> visitingPlayingTime = Maps.newHashMap();
	@JsonProperty
	private int homeScore = 0;
	@JsonProperty
	private int visitingScore = 0;

	public QuarterData() {
	}

	public QuarterData(int homeScore, int visitingScore) {
		this.homeScore = homeScore;
		this.visitingScore = visitingScore;
	}

	public void updatePlayingTime(boolean isHome, Long playerId, long playingTimeInMills) {
		Map<Long, Long> playingTime = isHome ? homePlayingTime : visitingPlayingTime;
		Long oldTime = playingTime.get(playerId);
		if (oldTime == null || oldTime == 0) {
			playingTime.put(playerId, playingTimeInMills / 1000);
		} else {
			playingTime.put(playerId, oldTime + playingTimeInMills / 1000);
		}
	}

	public void updateScore(boolean isHome, int score) {
		if (isHome) {
			this.homeScore += score;
		} else {
			this.visitingScore += score;
		}
	}
	
	public int getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	public int getVisitingScore() {
		return visitingScore;
	}

	public void setVisitingScore(int visitingScore) {
		this.visitingScore = visitingScore;
	}

	public Map<Long, Long> getHomePlayingTime() {
		return homePlayingTime;
	}

	public void setHomePlayingTime(Map<Long, Long> homePlayingTime) {
		this.homePlayingTime = homePlayingTime;
	}

	public Map<Long, Long> getVisitingPlayingTime() {
		return visitingPlayingTime;
	}

	public void setVisitingPlayingTime(Map<Long, Long> visitingPlayingTime) {
		this.visitingPlayingTime = visitingPlayingTime;
	}

}
