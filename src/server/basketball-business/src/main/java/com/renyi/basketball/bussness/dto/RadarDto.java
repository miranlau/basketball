package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public class RadarDto implements Serializable {
    private static final long serialVersionUID = 4481729221730760972L;
    //进攻值
    private Integer jingong;
    //防守值
    private Integer fangshou;
    //体能值
    private Integer tineng;
    //速度值
    private Integer sudu;
    //信誉
    private Integer credit;
    //星级
    private Integer stars;
    
    private float common;

    public Integer getJingong() {
        return jingong == null ? 0 : jingong;
    }

    public Integer getFangshou() {
        return fangshou == null ? 0 : fangshou;
    }

    public Integer getTineng() {
        return tineng == null ? 0 : tineng;
    }

    public Integer getSudu() {
        return sudu == null ? 0 : sudu;
    }

    public Integer getCredit() {
        return credit == null ? 0 : credit;
    }

    public Integer getStars() {
        return stars == null ? 0 : stars;
    }

    public float getCommon() {
        return common;
    }

    public void setJingong(Integer jingong) {
        this.jingong = jingong;
    }

    public void setFangshou(Integer fangshou) {
        this.fangshou = fangshou;
    }

    public void setTineng(Integer tineng) {
        this.tineng = tineng;
    }

    public void setSudu(Integer sudu) {
        this.sudu = sudu;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public void setCommon(float common) {
        this.common = common;
    }
}
