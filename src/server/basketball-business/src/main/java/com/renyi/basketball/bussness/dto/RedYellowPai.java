package com.renyi.basketball.bussness.dto;

import net.sf.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Roy.xiao on 2016/6/12.
 */
public class RedYellowPai implements Comparable<RedYellowPai>,Serializable{
    /** 球员名字. */
    private String name;
    /** 球员ID. */
    private Integer playerId;
    /** 红牌. */
    private Integer redNum = 0;
    /** 黄牌. */
    private Integer yellowNum = 0;
    /** 双簧牌. */
    private Integer doubleYellow = 0;
    public void getRedYellow(String action){
        JSONObject JsonAction = JSONObject.fromObject(action);
        JSONObject playerA = JsonAction.getJSONObject("playerA");
        JSONObject playerB = JsonAction.getJSONObject("playerB");
        if (playerA != null && playerA.size() != 0) {
            playerId = playerA.getInt("id");
//            name = playerA.getString("name");
        }
        else if (playerB != null && playerB.size() != 0) {
            playerId = playerB.getInt("id");
//            name = playerB.getString("name");
        }
    }

    public void countRed(){
        this.redNum ++;
    }

    public void countYellow(){
        this.yellowNum ++;
    }

    public void countDoubleYellow(){
        this.doubleYellow ++;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRedNum() {
        return redNum;
    }

    public void setRedNum(Integer redNum) {
        this.redNum = redNum;
    }

    public Integer getYellowNum() {
        return yellowNum;
    }

    public void setYellowNum(Integer yellowNum) {
        this.yellowNum = yellowNum;
    }

    public Integer getDoubleYellow() {
        return doubleYellow;
    }

    public void setDoubleYellow(Integer doubleYellow) {
        this.doubleYellow = doubleYellow;
    }

    @Override
    public int compareTo(RedYellowPai o) {
        if (this.getRedNum() > o.getRedNum()) {
            return -1;
        } else if (this.getRedNum() < o.getRedNum()) {
            return 1;
        } else {
            if (this.getYellowNum() > o.getYellowNum()) {
                return -1;
            } else if (this.getYellowNum() < o.getYellowNum()) {
                return 1;
            } else {
                return 1;
            }
        }
    }

    @Override
    public int hashCode() {
        return this.playerId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj ) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RedYellowPai other = (RedYellowPai) obj;
        if (this.playerId.equals(other.playerId)) {
            return true;
        } else {
            return false;
        }
    }
}
