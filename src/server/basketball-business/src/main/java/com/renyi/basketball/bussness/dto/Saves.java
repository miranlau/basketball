package com.renyi.basketball.bussness.dto;

import net.sf.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Roy.xiao on 2016/7/14.
 */
public class Saves implements Comparable<Saves>,Serializable{
    /** 球员ID */
    private Integer playerId;
    /** 球员名字.*/
    private String name;
    /** 所在球队名字 .*/
    private String teamName;
    /** 扑救数. */
    private Integer save = 0;

    public void getSaves(String action) {
        try {
            JSONObject JsonAction = JSONObject.fromObject(action);
            JSONObject playerA = JsonAction.getJSONObject("playerA");
            if (playerA != null && playerA.size() != 0) {
                int attackId = playerA.getInt("id");
                playerId = attackId;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getSave() {
        return save;
    }

    public void setSave(Integer save) {
        this.save = save;
    }

    @Override
    public int compareTo(Saves o) {
        if (this.save > o.save) {
            return -1;//降序
        } else if (this.save < o.save) {
            return 1;
        } else {
            if (!this.playerId.equals(o.playerId)) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj ) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Saves other = (Saves) obj;
        if (this.playerId.equals(other.playerId)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.playerId;
    }

    public void countSave(){
        this.save ++;
    }
}
