package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;

public class ScoresList implements Comparable<ScoresList>, Serializable {
	private static final long serialVersionUID = 1L;
	/** 球队名字. */
	private String teamName;
	/** 胜 . */
	private Integer win = 0;

	/** 负 */
	private Integer lose = 0;

	/** 积分. */
	private Integer points = 0;
	/** 球队ID. */
	private Long teamId;

	public ScoresList countHome(Match match, Team team) {
		this.teamId = match.getHomeId();
		this.teamName = team.getName();
		Integer homeScore = match.getHomeScore();
		Integer visitingScore = match.getVisitingScore();
		if (homeScore > visitingScore) {
			this.win++;
		} else if (homeScore < visitingScore) {
			this.lose++;
		}
		return this;
	}

	public ScoresList countVisiting(Match match, Team team) {
		this.teamId = match.getVisitingId();
		this.teamName = team.getName();
		Integer homeScore = match.getHomeScore();
		Integer visitingScore = match.getVisitingScore();
		if (homeScore < visitingScore) {
			this.win++;
		} else if (homeScore > visitingScore) {
			this.lose++;
		}
		return this;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getWin() {
		return win;
	}

	public void setWin(Integer win) {
		this.win = win;
	}

	public Integer getLose() {
		return lose;
	}

	public void setLose(Integer lose) {
		this.lose = lose;
	}

	public Integer getPoints() {
		points = win * BasketballConstants.WIN_TEAM_SCORE + lose * BasketballConstants.LOSE_TEAM_SCORE;
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	@Override
	public int compareTo(ScoresList o) {
		if (this.getPoints() > o.getPoints()) {
			return -1;// 降序
		} else if (this.getPoints() < o.getPoints()) {
			return 1;
		} else {
			/*
			 * //如果积分相同，则净胜球排名靠前者积分排名靠前 if (this.getClean() > o.getClean()) {
			 * return -1; } else if (this.getClean() < o.getClean()){ return 1;
			 * }else { return 1; }
			 */
			return 1;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return false;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ScoresList other = (ScoresList) obj;
		if (other.getTeamId().equals(this.getTeamId())) {
			return true;
		}
		return false;
	}

}
