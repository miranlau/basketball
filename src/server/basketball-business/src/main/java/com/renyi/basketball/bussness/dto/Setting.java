/*
 *
 *
 *
 */
package com.renyi.basketball.bussness.dto;


import net.sf.json.JSONObject;

/**
 * 系统设置
 */
public class Setting implements com.renyi.basketball.bussness.constants.Setting {

	private static final long serialVersionUID = -1478999889661796840L;


	/** 分隔符 */
	private static final String SEPARATOR = ",";

	/** 网站名称 */
	private String siteName;
	
	private String versionAndroid;
	
	private String versionIOS;
	
	/** 广告 */
	private String advert;


	/**
	 * 获取网站名称
	 *
	 * @return 网站名称
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * 设置网站名称
	 *
	 * @param siteName
	 *            网站名称
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getAdvert() {
		return advert;
	}

	public void setAdvert(String advert) {
		this.advert = advert;
	}

	public String getVersionAndroid() {
		return versionAndroid;
	}

	public void setVersionAndroid(String versionAndroid) {
		this.versionAndroid = versionAndroid;
	}

	public String getVersionIOS() {
		return versionIOS;
	}

	public void setVersionIOS(String versionIOS) {
		this.versionIOS = versionIOS;
	}
	
	public Version getVersion(int type) {
		String str = null;
		if (type == 0) {
			str = versionAndroid;
		} else {
			str = versionIOS;
		}
		
		JSONObject json = JSONObject.fromObject(str);
		
		return (Version) JSONObject.toBean(json, Version.class);
	}
	
	public void setVersion(int type,Version version){
		
		if (type == 0) {
			versionAndroid = JSONObject.fromObject(version).toString();
		} else {
			versionIOS = JSONObject.fromObject(version).toString();
		}
	}

	public void setAdvertDto(AdvertDto advertDto) {
		if(advertDto!=null){
			advert = JSONObject.fromObject(advertDto).toString();
		}
	}
	public AdvertDto getAdvertDto(){
		JSONObject json = JSONObject.fromObject(advert);
		AdvertDto advertDto = (AdvertDto)JSONObject.toBean(json,AdvertDto.class);
		return  advertDto;
	}

}
