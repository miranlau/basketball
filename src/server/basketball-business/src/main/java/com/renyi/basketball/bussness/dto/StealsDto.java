package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

import net.sf.json.JSONObject;

/**
 * Created by joefan on 2016/9/7 0007.
 */
public class StealsDto implements Comparable<StealsDto>, Serializable {
	/** 球员ID */
	private Long playerId;
	/** 球员名字. */
	private String name;
	/** 所在球队名字 . */
	private String teamName;

	/** 比赛场次 */
	private Integer count = 0;

	/** 所在联赛. */
	private Long leagueId;

	/** 抢断 */
	private Integer steals = 0;
	/** 场均抢断 */
	private double avgSteals = 0;

	public Integer getSteals() {
		return steals;
	}

	public void setSteals(Integer steals) {
		this.steals = steals;
	}

	public double getAvgSteals() {
		return avgSteals;
	}

	public void setAvgSteals(double avgSteals) {
		this.avgSteals = avgSteals;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void getShooter(String action) {
		try {
			JSONObject JsonAction = JSONObject.fromObject(action);
			JSONObject playerA = JsonAction.getJSONObject("playerA");
			JSONObject playerB = JsonAction.getJSONObject("playerB");
			if (playerA != null && playerA.size() != 0) {
				playerId = playerA.getLong("id");
				name = playerA.getString("name");
			} else if (playerB != null && playerB.size() != 0) {
				playerId = playerB.getLong("id");
				name = playerB.getString("name");
			}

		} catch (Exception e) {
			e.printStackTrace();
			JSONObject JsonAction = JSONObject.fromObject(action);
			JSONObject playerA = JsonAction.getJSONObject("playerA");
			JSONObject playerB = JsonAction.getJSONObject("playerB");
			if (playerA != null && playerA.size() != 0) {
				playerId = playerA.getLong("id");
				name = playerA.getString("name");
			} else if (playerB != null && playerB.size() != 0) {
				playerId = playerB.getLong("id");
				name = playerB.getString("name");
			}
		}
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/*
	 * public Integer getIn() { return in; }
	 * 
	 * public void setIn(Integer in) { this.in = in; }
	 */

	@Override
	public int compareTo(StealsDto o) {
		if (this.avgSteals > o.avgSteals) {
			return -1;// 降序
		} else if (this.avgSteals < o.avgSteals) {
			return 1;
		} else {
			if (!this.playerId.equals(o.playerId)) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return false;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StealsDto other = (StealsDto) obj;
		if (this.playerId.equals(other.playerId)) {
			return true;
		} else {
			return false;
		}
	}

	public void stealsCount() {
		this.steals++;
	}

	public void avgSteals() {
		this.avgSteals = Double.parseDouble(String.format("%.2f", this.steals * 1.0 / this.count));
	}
}
