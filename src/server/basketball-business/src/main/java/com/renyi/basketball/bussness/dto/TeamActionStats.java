package com.renyi.basketball.bussness.dto;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.renyi.basketball.bussness.common.StatsAlgorithm;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.po.Action;

/**
 * 按球队统计比赛的Action
 */
public final class TeamActionStats extends ActionStats {
	// 进攻效率
	private float offensiveEff;
	// 防守效率
	private float defensiveEff;
	// 对手进攻篮板
	private int opposingOffensiveRebound;
	// 对手防守篮板
	private int opposingDefensiveRebound;

	public TeamActionStats(long teamId, long opposingTeamId, int opposingScore, List<Action> actionList) {
		if (CollectionUtils.isEmpty(actionList)) {
			return;
		}
		this.actionList = actionList;

		init(teamId, opposingTeamId);

		calcShottingArea();

		calcShottingWay();

		calcOffensivePct();

		calcScore();

		calcAdvanced(opposingScore);
	}

	private void init(Long teamId, Long opposingTeamId) {
		for (Action action : actionList) {
			if (teamId.equals(action.getTeamId())) {
				countByStatus(action);
			} else if (opposingTeamId.equals(action.getTeamId())) {
				if (action.getStatus() == ActionStatus.OFFENSIVE_REBOUND) {
					opposingOffensiveRebound++;
				} else if (action.getStatus() == ActionStatus.DEFENSIVE_REBOUND) {
					opposingDefensiveRebound++;
				}
			}
		}
	}

	private void calcAdvanced(int opposingScore) {
		super.calcAdvanced();
		// 助攻率
		assistPct = StatsAlgorithm.percent(assist, fieldGoalShot);
		// 篮板率
		reboundPct = StatsAlgorithm.percent(getRebound(), StatsAlgorithm.sum(getRebound(), getOpposingRebound()));
		// 失误率
		turnoverPct = StatsAlgorithm.percent(turnover, offensiveAttempt);
		// 进攻效率
		offensiveEff = StatsAlgorithm.percent(score, offensiveAttempt);
		// 防守效率
		defensiveEff = StatsAlgorithm.percent(opposingScore, offensiveAttempt);
	}

	public int getOpposingRebound() {
		return opposingOffensiveRebound + opposingDefensiveRebound;
	}

	public int getOpposingOffensiveRebound() {
		return opposingOffensiveRebound;
	}

	public int getOpposingDefensiveRebound() {
		return opposingDefensiveRebound;
	}

	public float getOffensiveEff() {
		return offensiveEff;
	}

	public float getDefensiveEff() {
		return defensiveEff;
	}

}
