package com.renyi.basketball.bussness.dto;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.renyi.basketball.bussness.constants.BasketballConstants;

public class TeamDto implements Serializable {
	private static final long serialVersionUID = -3303575622141595642L;
	// 包含队友
	private List<PlayerDto> players;
	// 团队属性
	private RadarDto radarDto;
	// 球队id
	private Long teamId;
	// 球队姓名
	private String name;
	// 球队logo
	private String logo;
	// 球队暗号
	private String password;
	// 队长id
	private Long leader;
	// 队长姓名
	private String leaderName;
	/** 队长电话. */
	private String mobile;
	// 平均年龄
	private Integer age;
	/** 评分(战斗力). */
	private Integer stars;
	// 胜场数
	private Integer win;
	// 平局数
	private Integer deuce;
	// 负场数
	private Integer lose;
	// 等级
	private Integer level;
	// 城市
	private Integer city;
	// 球队属性
	private String attr;
	/** 是否解散. */
	private Boolean isdel;
	/** 与当前用户而言，是否关注 */
	private Boolean follow;
	/** 群ID . */
	private String groupId;
	/**
	 * 球队统计
	 */
	private Count teamCount;

	/** 场均得分 */
	private double countScore;
	/** 场均失分 */
	private double lostscore;
	/** 场均助攻 */
	private double avgHelp;
	/** 场均篮板 */
	private double avgBackboard;
	/** 场均失误 */
	private double avgAnError;
	/** 场均盖帽 */
	private double avgCover;
	/** 场均抢断 */
	private double avgSteals;

	public double getAvgCover() {
		return avgCover;
	}

	public void setAvgCover(double avgCover) {
		this.avgCover = avgCover;
	}

	public double getAvgSteals() {
		return avgSteals;
	}

	public void setAvgSteals(double avgSteals) {
		this.avgSteals = avgSteals;
	}

	public double getCountScore() {
		return countScore;
	}

	public void setCountScore(double countScore) {
		this.countScore = countScore;
	}

	public double getLostscore() {
		return lostscore;
	}

	public void setLostscore(double lostscore) {
		this.lostscore = lostscore;
	}

	public double getAvgHelp() {
		return avgHelp;
	}

	public void setAvgHelp(double avgHelp) {
		this.avgHelp = avgHelp;
	}

	public double getAvgBackboard() {
		return avgBackboard;
	}

	public void setAvgBackboard(double avgBackboard) {
		this.avgBackboard = avgBackboard;
	}

	public double getAvgAnError() {
		return avgAnError;
	}

	public void setAvgAnError(double avgAnError) {
		this.avgAnError = avgAnError;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Boolean getFollow() {
		return follow;
	}

	public void setFollow(Boolean follow) {
		this.follow = follow;
	}

	public Boolean getIsdel() {
		return isdel;
	}

	public void setIsdel(Boolean isdel) {
		this.isdel = isdel;
	}

	/**
	 * 权限: 0:非该队队员 1:该队队员 2:该队队长
	 */

	private Integer right = BasketballConstants.RIGHT_NORMAL;

	public List<PlayerDto> getPlayers() {
		return players;
	}

	public void setRadarDto(RadarDto radarDto) {
		this.radarDto = radarDto;
	}

	public RadarDto getRadarDto() {
		return radarDto;
	}

	public String getLeaderName() {
		return leaderName;
	}

	public Long getTeamId() {
		return teamId;
	}

	public String getName() {
		return name;
	}

	public String getLogo() {
		if (null != logo && !logo.startsWith("http://")) {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();

			return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/" + logo;
		}
		return logo;
	}

	public String getPassword() {
		return password;
	}

	public Long getLeader() {
		return leader;
	}

	public Integer getAge() {
		return age;
	}

	public Integer getStars() {
		return stars;
	}

	public Integer getWin() {
		return win;
	}

	public Integer getDeuce() {
		return deuce;
	}

	public Integer getLose() {
		return lose;
	}

	public Integer getLevel() {
		return level;
	}

	public Integer getCity() {
		return city;
	}

	public String getAttr() {
		return attr;
	}

	public void setPlayers(List<PlayerDto> players) {
		this.players = players;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setLeader(Long leader) {
		this.leader = leader;
	}

	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public void setWin(Integer win) {
		this.win = win;
	}

	public void setDeuce(Integer deuce) {
		this.deuce = deuce;
	}

	public void setLose(Integer lose) {
		this.lose = lose;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public Integer getRight() {
		return right;
	}

	public void setRight(Integer right) {
		this.right = right;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Count getTeamCount() {
		return teamCount;
	}
	public void setTeamCount(Count teamCount) {
		this.teamCount = teamCount;
	}
}
