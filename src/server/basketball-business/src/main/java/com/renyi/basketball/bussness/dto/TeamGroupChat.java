package com.renyi.basketball.bussness.dto;

/**
 * Created by Roy.xiao on 2016/6/20.
 */
public class TeamGroupChat {
	/** 球队名. */
	private String teamName;
	/** 球队ID. */
	private Long teamId;
	/** 球队LOGO. */
	private String logo;

	/** 群聊会话ID. */
	private String groupId;

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}
