package com.renyi.basketball.bussness.dto;

import java.util.List;

import com.renyi.basketball.bussness.utils.JsonUtil;

import jersey.repackaged.com.google.common.collect.Lists;

public class TeamRankingDto {
	private Long teamId;
	private Integer rank = 0;
	private String teamName;
	private String teamLogo;
	private String teamCourt;
	private Integer win = 0;
	private Integer loss = 0;
	private Integer score = 0;

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamLogo() {
		return teamLogo;
	}

	public void setTeamLogo(String teamLogo) {
		this.teamLogo = teamLogo;
	}

	public Integer getWin() {
		return win;
	}

	public void setWin(Integer win) {
		this.win = win;
	}

	public Integer getLoss() {
		return loss;
	}

	public void setLoss(Integer loss) {
		this.loss = loss;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getTeamCourt() {
		return teamCourt;
	}

	public void setTeamCourt(String teamCourt) {
		this.teamCourt = teamCourt;
	}
	
	public static void main(String[] args) {
		TeamRankingDto team1 = new TeamRankingDto();
		TeamRankingDto team2 = new TeamRankingDto();
		TeamRankingDto team3 = new TeamRankingDto();
		team1.setTeamId(200820l);
		team1.setTeamName("辽宁本溪");
		team1.setTeamLogo("http://120.26.87.111:8081/basketball-api/resource/img/default_team.png");
		team1.setTeamCourt("四川省体育馆");
		team1.setRank(2);
		team1.setLoss(10);
		team1.setWin(20);
		
		team2.setTeamId(200810l);
		team2.setTeamName("郑雪篮球队");
		team2.setTeamLogo("http://120.26.87.111:8081/basketball-api/resource/img/default_team.png");
		team2.setTeamCourt("四川省体育馆");
		team2.setRank(1);
		team2.setLoss(10);
		team2.setWin(22);
		
		team3.setTeamId(200811l);
		team3.setTeamName("郑雪篮球队1");
		team3.setTeamLogo("http://120.26.87.111:8081/basketball-api/resource/img/default_team.png");
		team3.setTeamCourt("四川省体育馆");
		team3.setRank(3);
		team3.setLoss(16);
		team3.setWin(20);
		
		List<TeamRankingDto> list = Lists.newArrayList();
		list.add(team2);
		list.add(team1);
		list.add(team3);
		String json = JsonUtil.toJson(list);
		System.out.println(json);
				
	}

}
