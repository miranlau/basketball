package com.renyi.basketball.bussness.dto;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class UserDto implements Serializable {
    private static final long serialVersionUID = 5513849682515769796L;
    private Long Id;
	/** 用户名 */
	private String userName;
	/** 身份证号码 */
	private String identity;
	/** 邮箱 */
	private String email;
	/** 手机号码，可以等于userName */
	private String mobile;
	/** 密码 */
	private String password;
	/** 昵称 */
	private String nickName;
	/** 真实姓名 */
	private String realName;
	/** 用户星级 */
	private Integer stars;
	/** 用户信用 */
	private Integer credit;
	/** 用户角色 */
	private Integer role;
	/** 头像url */
	private String avatar;

	/** 是否可以查看其它人的资料 */
	private Boolean canView;
	/** 是否可以邀请其它人 */
	private Boolean canInvite;
	/** 是否可以聊天 */
	private Boolean chatPush;
	/** 自动更新？ */
	private Boolean autoUpdate;
	/** 上次登录时间 */
	private Date lastLoginTime;
	/** 登录token */
	private String token;
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Integer getStars() {
		return stars;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public Integer getCredit() {
		return credit;
	}

	public void setCredit(Integer credit) {
		this.credit = credit;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public Boolean getCanInvite() {
		return canInvite;
	}

	public void setCanInvite(Boolean canInvite) {
		this.canInvite = canInvite;
	}

	public Boolean getChatPush() {
		return chatPush;
	}

	public void setChatPush(Boolean chatPush) {
		this.chatPush = chatPush;
	}

	public Boolean getAutoUpdate() {
		return autoUpdate;
	}

	public void setAutoUpdate(Boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String deviceToken) {
		this.token = deviceToken;
	}

}
