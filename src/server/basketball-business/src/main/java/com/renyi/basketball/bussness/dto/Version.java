package com.renyi.basketball.bussness.dto;

import java.io.Serializable;

public class Version implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6968575644230487979L;

	private String name;
	
	private String versionNumber;
	
	private Integer code;
	
	private Integer minCode;
	
	private String downloadURL;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getMinCode() {
		return minCode;
	}

	public void setMinCode(Integer minCode) {
		this.minCode = minCode;
	}

	public String getDownloadURL() {
		return downloadURL;
	}

	public void setDownloadURL(String downloadURL) {
		this.downloadURL = downloadURL;
	}

	@Override
	public String toString() {
		return "Version [name=" + name + ", versionNumber=" + versionNumber + ", code=" + code + ", minCode=" + minCode
				+ ", downloadURL=" + downloadURL + "]";
	}
	
	
}
