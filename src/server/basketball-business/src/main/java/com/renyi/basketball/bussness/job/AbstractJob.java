package com.renyi.basketball.bussness.job;

public abstract class AbstractJob {

	/**
	 * 获取任务名称.
	 * 
	 * @return 任务名称
	 */
	abstract String getName();
}
