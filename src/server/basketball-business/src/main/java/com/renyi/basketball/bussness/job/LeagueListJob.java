package com.renyi.basketball.bussness.job;

import com.renyi.basketball.bussness.service.LeagueService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component("leagueListJob")
public class LeagueListJob extends AbstractJob {
	private Logger logger = Logger.getLogger(getClass());

	@Resource
	private LeagueService leagueService;

	@Override
	String getName() {
		return "leagueListJob";
	}

	@Scheduled(cron = "${job.league_list_count}")
	public void update() {
		logger.info(getName() + " start at " + new Date());
		leagueService.updateBang();
		logger.info(getName() + " end at " + new Date());
	}
}
