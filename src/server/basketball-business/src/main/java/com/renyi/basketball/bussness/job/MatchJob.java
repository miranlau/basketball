package com.renyi.basketball.bussness.job;

import com.renyi.basketball.bussness.service.MatchService;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component("matchJob")
public class MatchJob extends AbstractJob {
    private Logger logger = Logger.getLogger(getClass());

    @Resource
    private MatchService matchService;

    @Scheduled(cron = "${job.match_start.cron}")
    public void start() {
        logger.info(getName() + " start at " + new Date());
        matchService.autoStart();
        logger.info(getName() + " stop at " + new Date());
    }

    @Scheduled(cron = "${job.match_stop.cron}")
    public void stop() {
        matchService.autoEnd();
    }

    @Override
    String getName() {
        return "matchJob";
    }

}
