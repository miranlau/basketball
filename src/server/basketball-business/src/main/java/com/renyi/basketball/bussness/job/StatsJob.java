package com.renyi.basketball.bussness.job;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.renyi.basketball.bussness.common.StatsStatus;
import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.StatsService;

/**
 * 生成数据统计的Timer
 */
@Component("statsJob")
public class StatsJob extends AbstractJob {
	private static Logger logger = Logger.getLogger(StatsJob.class);

	@Resource
	private MatchService matchService;
	@Resource
	private MatchStatsService matchStatsService;
	@Resource
	private StatsService statsService;

	@Override
	String getName() {
		return "StatsJob";
	}

	@Scheduled(cron = "${job.stats.cron}")
	public void start() {
		logger.info(getName() + " start");
		Long start = System.currentTimeMillis();

		List<Match> matchList = matchService.queryNeedStatsMatches();
		if (CollectionUtils.isEmpty(matchList)) {
			logger.info("no match need to generate stats");
			logger.info(getName() + " stop, no match need to generate stats.");
			return;
		}

		StringBuilder matchIds = new StringBuilder();
		for (Match match : matchList) {
			generateStats(match);
			matchIds.append(match.getId() + ", ");
		}

		logger.info(getName() + " stop, generate team or player stats finished, matchIds = [" + matchIds.substring(0, matchIds.length() - 2)
				+ "], Execution Time: " + ((System.currentTimeMillis() - start) / 1000) + " seconds.");
	}

	private void generateStats(Match match) {
		Long matchId = match.getId();
		Long leagueId = match.getLeagueId();
		Long leagueStageId = match.getLeagueStageId();
		Integer statsStatus = match.getStatsStatus();

		logger.info("generate team and player stats start, matchId = " + matchId + ", leagueId = " + leagueId + ", leagueStageId = "
				+ leagueStageId + ", statsStatus = " + statsStatus);

		if (isNotValidId(matchId) || isNotValidId(leagueId) || isNotValidId(leagueStageId) || StatsStatus.isNotValid(statsStatus)) {
			logger.error("can not generate stats due to matchId or leagueId or leagueStageId is invalid");
			return;
		}

		generateTeamStats(matchId, leagueId, leagueStageId, statsStatus);

		generatePlayerStats(matchId, statsStatus);
	}

	private void generateTeamStats(Long matchId, Long leagueId, Long leagueStageId, Integer statsStatus) {
		Map<String, TeamMatchStats> teamStats = matchStatsService.getTeamMatchStatsForTeam(matchId);
		if (teamStats == null) {
			logger.error("can not generate team stats due to getTeamMatchStatsForTeam() return null, matchId = " + matchId);
			return;
		}
		TeamMatchStats homeMatchStats = teamStats.get(ResponseParams.HOME);
		TeamMatchStats visitingMatchStats = teamStats.get(ResponseParams.VISITING);
		if (homeMatchStats == null || visitingMatchStats == null) {
			logger.error("can not generate team stats due to home or visiting TeamMatchStats is empty, matchId = " + matchId);
			return;
		}
		if (StatsStatus.not_generated.getId().equals(statsStatus)) {
			statsService.saveTeamMatchStats(matchId, homeMatchStats, visitingMatchStats);
			statsService.saveOrUpdateTeamSeasonStats(matchId, homeMatchStats, visitingMatchStats);
			statsService.updateTeamRanking(matchId, leagueId, leagueStageId);
		} else if (StatsStatus.generate_team_match_stats.getId().equals(statsStatus)) {
			statsService.saveOrUpdateTeamSeasonStats(matchId, homeMatchStats, visitingMatchStats);
			statsService.updateTeamRanking(matchId, leagueId, leagueStageId);
		} else if (StatsStatus.generate_team_season_stats.getId().equals(statsStatus)) {
			statsService.updateTeamRanking(matchId, leagueId, leagueStageId);
		}
	}

	private void generatePlayerStats(Long matchId, Integer statsStatus) {
		Map<String, List<PlayerMatchStats>> playerStats = matchStatsService.getPlayerMatchStatsForTeam(matchId);
		if (playerStats == null) {
			logger.error("can not generate player stats due to getPlayerMatchStatsForTeam() return null, matchId = " + matchId);
			return;
		}
		List<PlayerMatchStats> homePlayerMatchStatsList = playerStats.get(ResponseParams.HOME);
		List<PlayerMatchStats> visitingPlayerMatchStatsList = playerStats.get(ResponseParams.VISITING);

		if (CollectionUtils.isEmpty(homePlayerMatchStatsList) || CollectionUtils.isEmpty(visitingPlayerMatchStatsList)) {
			logger.error("can not generate player stats due to home or visiting PlayerMatchStats list is empty, matchId = " + matchId);
			return;
		}

		if (StatsStatus.generate_team_ranking.getId().equals(statsStatus)) {
			statsService.savePlayerMatchStats(matchId, homePlayerMatchStatsList, visitingPlayerMatchStatsList);
			statsService.saveOrUpdatePlayerSeasonStats(matchId, homePlayerMatchStatsList, visitingPlayerMatchStatsList);
		} else if (StatsStatus.generate_player_match_stats.getId().equals(statsStatus)) {
			statsService.saveOrUpdatePlayerSeasonStats(matchId, homePlayerMatchStatsList, visitingPlayerMatchStatsList);
		}
	}

	private boolean isNotValidId(Long id) {
		return (id == null || id <= 0) ? true : false;
	}

}
