package com.renyi.basketball.bussness.job;

import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.utils.PushMessageUtil;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/5/23 0023.
 */
@Component("TeamJob")
public class TeamJob extends AbstractJob {

	private Logger logger = Logger.getLogger(getClass());

	@Resource
	private TeamService teamService;
	@Resource
	private UserService userService;

	@Scheduled(cron = "${job.team_count}") // 每天两点执行
	public void start() {
		logger.info(getName() + " start at " + new Date());
		List<Team> teams = teamService.checkTeam();
		Map<String, String> extend = new HashMap<>();
		String msg = "您创建的球队3天内球员数没有达到3位，请寻找更多伙伴，下次再努力";
		if (teams != null && teams.size() != 0) {
			for (Team team : teams) {
				extend.put("teamId", team.getId().toString());
				extend.put("pushType", PushMessageUtil.TYPE_SYSTEM_MESSAGE);
				extend.put("senderHeader", "avatar/avatar.png");
				Date date = new Date();
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String sendTime = sf.format(date);
				extend.put("sendTime", sendTime);
				extend.put("content", msg);
				//User user = userService.find(team.getLeader());
				teamService.deleteUT(team.getId());

				// 不做真删除，进行假删除
				Team t = new Team();
				t.setId(team.getId());
				t.setIsDismissed(true);
				teamService.update(t);
				// teamService.delete(Long.valueOf(team.getId()));
//				if (user != null) {
//					PushMessageUtil.pushMessages(user, msg, extend);
//				}
			}
		}

		logger.info(getName() + " stop at " + new Date());
	}

	@Override
	String getName() {
		return "teamJob";
	}

}
