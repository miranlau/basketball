package com.renyi.basketball.bussness.po;

import java.util.Map;

import javax.persistence.Table;

import com.renyi.basketball.bussness.utils.JsonUtil;

@Table(name = "action")
public class Action extends BaseEntity implements Comparable<Action> {
	private static final long serialVersionUID = -8099759764601251346L;
	// 比赛ID
	private Long matchId;
	// 球队ID
	private Long teamId;
	// 球员ID
	private Long playerId;
	// 记分员ID
	private Long recorderId;
	// 操作状态
	private Integer status;
	// 扩展数据(JSON)
	private String extension;
	// 当节比赛剩余时间
	private Integer quarterRemainingTime;
	// 话术
	private String speaks;

	// -------------------------------------------

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public Long getRecorderId() {
		return recorderId;
	}

	public void setRecorderId(Long recorderId) {
		this.recorderId = recorderId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Integer getQuarterRemainingTime() {
		return quarterRemainingTime;
	}

	public void setQuarterRemainingTime(Integer quarterRemainingTime) {
		this.quarterRemainingTime = quarterRemainingTime;
	}

	public String getSpeaks() {
		return speaks;
	}

	public void setSpeaks(String speaks) {
		this.speaks = speaks;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getExtensionMap() {
		if (extension != null) {
			return JsonUtil.toObject(extension, Map.class);
		}
		return null;
	}

	public void setExtensionMap(Map<String, Object> extMap) {
		this.extension = JsonUtil.toJson(extMap);
	}

	@Override
	public int compareTo(Action action) {
		return getId().compareTo(action.getId());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Action action = (Action) o;

		if (!getId().equals(action.getId())) {
			return false;
		}

		return status.equals(action.getStatus());

	}

	@Override
	public String toString() {
		return "Action [matchId=" + matchId + ", teamId=" + teamId + ", playerId=" + playerId + ", recorderId=" + recorderId + ", status=" + status
				+ ", extension=" + extension + ", remainingTime=" + quarterRemainingTime + ", speaks=" + speaks + "]";
	}

}