package com.renyi.basketball.bussness.po;

import java.util.Date;

import javax.persistence.Table;

/**
 * Created by Roy.xiao on 2016/5/25.
 */
@Table(name = "admin")
public class Admin extends BaseEntity {
    /** 管理员账号session保存字段 */
    public final static String SESSION_ADMIN = "session_admin";
    /**超级管理员 */
    public final static Integer SUPER_ADMIN = 0;
    /**公司管理员 */
    public final static Integer COMPANY_ADMIN = 1;
    /** 场馆管理员*/
    public final static Integer ARENAS_ADMIN = 2;

    /** 用户名 */
    public String userName;

    /** 密码 */
    public String password;

    /** 类型 */
    public Integer type;

    /** 邮箱 */
    public String email;

    /** 手机号码 */
    public String mobile;

    /** 公司ID */
    public Long companyId;

    /** 最近登陆日期*/
    public Date loginDate;

    /** 最近登陆IP */
    public String loginIp;

    /** 是否可用 */
    public Boolean enabled = true;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Admin clone(){
        Admin a = null;
        try {
            a = (Admin) super.clone();
        } catch (CloneNotSupportedException c) {
            c.printStackTrace();
        }
        return a;
    }
}
