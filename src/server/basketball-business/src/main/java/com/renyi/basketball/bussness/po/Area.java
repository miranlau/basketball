package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "area")
public class Area extends BaseEntity {
	// 赛事名称
	private String name;
	// 图片路径
	private String fullName;
	// 参赛球队ids,分开
	private String code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
