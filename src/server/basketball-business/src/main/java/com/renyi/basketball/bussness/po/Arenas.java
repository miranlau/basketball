package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
@Table(name = "arenas")
public class Arenas extends BaseEntity {
	/** 场馆名字 */
	private String name;
	/** 图标 */
	private String icon;
	/** 官网地址 */
	private String website;
	/** 地址 */
	private String address;
	/** 联系人 */
	private String contact;
	/** 联系电话 */
	private String telephone;
	/** 状态 */
	private Integer status;
	/** 经度 */
	private Double longitude;
	/** 纬度 */
	private Double latitude;
	/** 所属公司 */
	private Long companyId;
	/** 地区编码 */
	private String areaCode;
	/** 星级. */
	private Integer stars;
	/** 审核意见. */
	private String judgement;
	/** 球场介绍 . */
	private String desc;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Integer getStars() {
		return stars;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public String getJudgement() {
		return judgement;
	}

	public void setJudgement(String judgement) {
		this.judgement = judgement;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
