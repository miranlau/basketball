package com.renyi.basketball.bussness.po;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import cn.magicbeans.mybatis.po.BaseMybatisPojo;

@MappedSuperclass
public class BaseEntity extends BaseMybatisPojo {
	@Id
	private Long id;
	private Date createTime;
	private Date updateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
