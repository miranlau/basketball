package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

/**
 * Created by Roy.xiao on 2016/6/23.
 */
@Table(name = "bill")
public class Bill extends BaseEntity {
	/** 比赛ID. */
	private Long matchId;
	/** 球队ID. */
	private Long teamId;
	/** 数据json数组. */
	private String json;
	/** 总金额. */
	private Double total;
	/** 人均费用. */
	private Double averge;

	public Double getAverge() {
		return averge;
	}

	public void setAverge(Double averge) {
		this.averge = averge;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

}
