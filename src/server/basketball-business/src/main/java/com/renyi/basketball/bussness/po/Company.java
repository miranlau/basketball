package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

/**
 * Created by Roy.xiao on 2016/5/25.
 */
@Table(name = "company")
public class Company extends BaseEntity{
    /** 公司名称. */
    private String name;
    /** 联系人. */
    private String contacter;
    /** 地址 */
    private String addr;
    /** 地区编码 */
    private Integer areaCode;
    /** 组织机构代码 */
    private String organizationCode;
    /** 组织机构代码副本URL */
    private String organizationImg;
    /** 手机 */
    private String mobile;
    /** 邮箱 */
    private String email;
    /** 描述 */
    private String describes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContacter() {
        return contacter;
    }

    public void setContacter(String contacter) {
        this.contacter = contacter;
    }

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public Integer getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationImg() {
        return organizationImg;
    }

    public void setOrganizationImg(String organizationImg) {
        this.organizationImg = organizationImg;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }
}
