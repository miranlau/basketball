package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "dynamic_comment")
public class DynamicComment extends BaseEntity {
	/** 评论内容 */
	private String content;

	/** 接收者 */
	private Long recieverId;

	/** 发送者 */
	private Long senderId;

	/** 动态ID */
	private Long dynamicId;

	/** 状态 */
	private Integer status = 0;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getRecieverId() {
		return recieverId;
	}

	public void setRecieverId(Long recieverId) {
		this.recieverId = recieverId;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Long getDynamicId() {
		return dynamicId;
	}

	public void setDynamicId(Long dynamicId) {
		this.dynamicId = dynamicId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
