package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "follow")
public class Follow extends BaseEntity {
	public static final Integer TEAM_FOLLOW = 0;// 球队关注
	public static final Integer PLAYER_FOLLOW = 2;// 球员关注
	public static final Integer LEAGUE_FOLLOW = 1;// 赛事关注
	/** 关联ID */
	private Long leagueId;
	/** 用户ID */
	private Long userId;
	/** 类型 */
	private Integer type;

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
