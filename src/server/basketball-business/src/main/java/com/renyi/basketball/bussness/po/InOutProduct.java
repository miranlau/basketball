package com.renyi.basketball.bussness.po;

import java.util.Date;

import javax.persistence.Table;

@Table(name = "in_out_product")
public class InOutProduct extends BaseEntity {
	/**
	 * 商品id
	 */
	private Integer productId;

	private Integer arenasId;

	/**
	 * 数量
	 */
	private Integer quantity;

	/**
	 * 类型（1.进货，0出货）
	 */
	private Integer type;

	private String remark;

	private Date crtime;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCrtime() {
		return crtime;
	}

	public void setCrtime(Date crtime) {
		this.crtime = crtime;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getArenasId() {
		return arenasId;
	}

	public void setArenasId(Integer arenasId) {
		this.arenasId = arenasId;
	}

}