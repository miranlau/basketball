package com.renyi.basketball.bussness.po;

import java.util.Date;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.renyi.basketball.bussness.common.IdAnalyzer;
import com.renyi.basketball.bussness.common.LeagueStatus;
import com.renyi.basketball.bussness.common.MatchFormat;

@Table(name = "league")
public class League extends BaseEntity {
	private static final long serialVersionUID = -632091628852255261L;
	// 赛事名称
	private String name;
	// 阶段ID
	private Long stageId;
	// 阶段名
	private String stageName;
	// 图片路径
	private String logo;
	// 参赛球队id,用逗号分开
	private String teams;
	// 赛季id,用逗号分开
	private String seasons;
	// 赛区id,用逗号分开
	private String divisions;
	// 状态
	private Integer status = LeagueStatus.signing.code();
	// 赛事开始日期
	private Date startTime;
	// 赛事结束日期
	private Date endTime;
	// 每节时间(秒)
	private Integer quarterTime;
	// 比赛总共的节数(2, 4)
	private Integer quarterCount;
	// 赛事详情
	private String details;
	// 比赛赛制
	private Integer matchFormat = MatchFormat.five.getId();

	// ==================================================================
	// 参加比赛球队列表
	private List<Team> teamList;
	// 赛季
	private List<Season> seasonList;

	/**
	 * 获取参赛队伍的ID集合.
	 */
	@Transient
	public List<Long> getTeamIds() {
		return IdAnalyzer.toList(teams);
	}

	/**
	 * 设置参数队伍集合.
	 */
	@Transient
	public void setTeamIds(List<Long> ids) {
		teams = IdAnalyzer.toString(ids);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getTeams() {
		return teams;
	}

	public void setTeams(String teams) {
		this.teams = teams;
	}

	public String getSeasons() {
		return seasons;
	}

	public void setSeasons(String seasons) {
		this.seasons = seasons;
	}

	public String getDivisions() {
		return divisions;
	}

	public void setDivisions(String areas) {
		this.divisions = areas;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Integer getMatchFormat() {
		return matchFormat;
	}

	public void setMatchFormat(Integer matchFormat) {
		this.matchFormat = matchFormat;
	}

	public Long getStageId() {
		return stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	@Transient
	public List<Team> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<Team> teamList) {
		this.teamList = teamList;
	}

	@Transient
	public List<Season> getSeasonList() {
		return seasonList;
	}

	public void setSeasonList(List<Season> seasonList) {
		this.seasonList = seasonList;
	}

	public Integer getQuarterTime() {
		return quarterTime;
	}

	public void setQuarterTime(Integer quarterTime) {
		this.quarterTime = quarterTime;
	}

	public Integer getQuarterCount() {
		return quarterCount;
	}

	public void setQuarterCount(Integer quarterCount) {
		this.quarterCount = quarterCount;
	}

}
