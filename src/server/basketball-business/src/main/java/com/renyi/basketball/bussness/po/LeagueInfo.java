package com.renyi.basketball.bussness.po;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.renyi.basketball.bussness.constants.BasketballConstants;

@Table(name = "league_info")
public class LeagueInfo extends BaseEntity {
	/** 赛事ID */
	private Integer leagueId;

	/** 发布内容 */
	private String content;

	/** 阅读数 */
	private Long readNum = 0l;

	/** 点赞数 */
	private Long laudNum = 0l;

	/** 评论数 */
	private Long commentNum = 0l;

	/** 图片 */
	private String pictures;

	public Integer getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getReadNum() {
		return readNum;
	}

	public void setReadNum(Long readNum) {
		this.readNum = readNum;
	}

	public Long getLaudNum() {
		return laudNum;
	}

	public void setLaudNum(Long laudNum) {
		this.laudNum = laudNum;
	}

	public Long getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Long commentNum) {
		this.commentNum = commentNum;
	}

	public String getPictures() {
		return pictures;
	}

	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

	/**
	 * 设置发布图片
	 */
	@Transient
	public void setImgUrls(Collection<String> imgs) {
		Iterator<String> it = imgs.iterator();
		StringBuffer sb = new StringBuffer();
		while (it.hasNext()) {
			sb.append(BasketballConstants.SEPARATOR + it.next());
		}
		pictures = sb.substring(1);
	}

	/**
	 * 获得发布图片
	 */
	@Transient
	public java.util.List<String> getImgUrls() {
		if (null == pictures || pictures.trim().length() == 0) {
			return null;
		}
		java.util.List<String> list = new ArrayList<>();
		String[] imgs = pictures.split(BasketballConstants.SEPARATOR);
		for (String img : imgs) {
			list.add(String.valueOf(img));
		}
		return list;
	}
}
