package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "league_info_comment")
public class LeagueInfoComment extends BaseEntity {
	/** 赛事动态ID */
	private Long leagueId;
	/** 发送者 */
	private Long sender;
	/** 接收者 */
	private Long receiver;
	/** 内容 */
	private String content;
	/** 状态 */
	private Integer status;

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getSender() {
		return sender;
	}

	public void setSender(Long sender) {
		this.sender = sender;
	}

	public Long getReceiver() {
		return receiver;
	}

	public void setReceiver(Long receiver) {
		this.receiver = receiver;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
