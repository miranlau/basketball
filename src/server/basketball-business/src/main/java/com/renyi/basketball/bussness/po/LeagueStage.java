package com.renyi.basketball.bussness.po;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "league_stage")
public class LeagueStage extends BaseEntity {
	@Id
    private Long id;
	private String name;
	private Long adminId;
	private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
