package com.renyi.basketball.bussness.po;

import java.util.Date;
import java.util.List;

import javax.persistence.Table;

import com.renyi.basketball.bussness.common.MatchStatus;
import com.renyi.basketball.bussness.common.IdAnalyzer;

@Table(name = "`match`")
public class Match extends BaseEntity {
	private static final long serialVersionUID = -979889434954202145L;
	// 赛事ID
	private Long leagueId;
	// 赛季ID
	private Long seasonId;
	// 赛事阶段
	private Long leagueStageId;
	// 轮次ID
	private Long roundId;
	// 轮次名
	private String roundName;
	// 场序
	private Integer sequence;
	// 主队ID
	private Long homeId;
	// 客队ID
	private Long visitingId;
	// 主队球员IDs
	private String homePlayerIds;
	// 客队球员IDs
	private String visitingPlayerIds;
	// 主队首发IDs
	private String homeStartingIds;
	// 客队首发IDs
	private String visitingStartingIds;
	// 主队场上球员IDs
	private String homeLineupIds;
	// 客队场上球员IDs
	private String visitingLineupIds;
	// 主队计分员ID
	private Long homeRecorderId;
	// 客队计分员ID
	private Long visitingRecorderId;
	// 比赛时间
	private Date matchTime;
	// 赛制，三人制，五人制
	private int matchFormat;
	// 主队的得分
	private Integer homeScore;
	// 客队的得分
	private Integer visitingScore;
	// 比赛状态
	private Integer status;
	// 数据统计的状态
	private Integer statsStatus;
	// 每节时间(秒)
	private Integer quarterTime;
	// 比赛总共的节数(2, 4)
	private Integer quarterCount;
	// 当节剩余时间(秒)
	private Integer quarterRemainingTime;
	// 比赛数据.
	private String matchData;
	// 比赛话术
	private String matchSpeak;
	// 比赛场馆
	private Long venueId;

	// ------------------------------------------------------------

	// 联赛名称
	private String leagueName;
	// 主队名
	private String homeName;
	// 客队名
	private String visitingName;
	// 主队LOGO
	private String homeLogo;
	// 客队LOGO
	private String visitingLogo;
	// 主队队长ID
	private Long homeCaptainId;
	// 客队队长ID
	private Long visitingCaptainId;
	// 主队球队
	private Team homeTeam;
	// 客队球队
	private Team visitingTeam;
	// 主队首发球员
	private List<Player> homeStartingList;
	// 客队首发球员
	private List<Player> visitingStartingList;
	// 主队记分员
	private User homeRecorder;
	// 客队记分员
	private User visitingRecorder;
	// 主队球报名球员列表
	private List<Player> homePlayerList;
	// 客队报名球员列表
	private List<Player> visitingPlayerList;
	// 比赛场馆
	private Venue venue;
	// 赛季
	private Season season;

	/**
	 * 是否有加时
	 * 
	 * @return Boolean
	 */
	public Boolean getIsOvertime() {
		if (statsStatus != null && statsStatus >= MatchStatus.overtime_1st.getId()) {
			return true;
		} else {
			return false;
		}
	}

	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getVisitingTeam() {
		return visitingTeam;
	}

	public void setVisitingTeam(Team visitingTeam) {
		this.visitingTeam = visitingTeam;
	}

	public List<Player> getHomeStartingList() {
		return homeStartingList;
	}

	public void setHomeStartingList(List<Player> homeStartingList) {
		this.homeStartingList = homeStartingList;
	}

	public List<Player> getVisitingStartingList() {
		return visitingStartingList;
	}

	public void setVisitingStartingList(List<Player> visitingStartingList) {
		this.visitingStartingList = visitingStartingList;
	}

	public User getHomeRecorder() {
		return homeRecorder;
	}

	public void setHomeRecorder(User homeRecorder) {
		this.homeRecorder = homeRecorder;
	}

	public User getVisitingRecorder() {
		return visitingRecorder;
	}

	public void setVisitingRecorder(User visitingRecorder) {
		this.visitingRecorder = visitingRecorder;
	}

	public List<Player> getHomePlayerList() {
		return homePlayerList;
	}

	public void setHomePlayerList(List<Player> homePlayerList) {
		this.homePlayerList = homePlayerList;
	}

	public List<Player> getVisitingPlayerList() {
		return visitingPlayerList;
	}

	public void setVisitingPlayerList(List<Player> visitingPlayerList) {
		this.visitingPlayerList = visitingPlayerList;
	}

	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(Long seasonId) {
		this.seasonId = seasonId;
	}

	public int getMatchFormat() {
		return matchFormat;
	}

	public void setMatchFormat(int matchFormat) {
		this.matchFormat = matchFormat;
	}

	public Integer getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(Integer homeScore) {
		this.homeScore = homeScore;
	}

	public Integer getVisitingScore() {
		return visitingScore;
	}

	public void setVisitingScore(Integer visitingScore) {
		this.visitingScore = visitingScore;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getVenueId() {
		return venueId;
	}

	public void setVenueId(Long venue) {
		this.venueId = venue;
	}

	public Date getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(Date matchTime) {
		this.matchTime = matchTime;
	}

	public String getLatestData() {
		return matchData;
	}

	public void setLatestData(String latestData) {
		this.matchData = latestData;
	}

	public Long getHomeRecorderId() {
		return homeRecorderId;
	}

	public void setHomeRecorderId(Long homeRecorderId) {
		this.homeRecorderId = homeRecorderId;
	}

	public Long getVisitingRecorderId() {
		return visitingRecorderId;
	}

	public void setVisitingRecorderId(Long visitingRecorderId) {
		this.visitingRecorderId = visitingRecorderId;
	}

	public String getHomePlayerIds() {
		return homePlayerIds;
	}

	public void setHomePlayerIds(String homePlayerIds) {
		this.homePlayerIds = homePlayerIds;
	}

	public String getVisitingPlayerIds() {
		return visitingPlayerIds;
	}

	public void setVisitingPlayerIds(String visitingPlayerIds) {
		this.visitingPlayerIds = visitingPlayerIds;
	}

	public Integer getStatsStatus() {
		return statsStatus;
	}

	public void setStatsStatus(Integer statsStatus) {
		this.statsStatus = statsStatus;
	}

	public List<Long> getHomePlayerIdList() {
		return IdAnalyzer.toList(getHomePlayerIds());
	}

	public void setHomePlayerIdList(List<Long> homePlayerIdList) {
		setHomePlayerIds(IdAnalyzer.toString(homePlayerIdList));
	}

	public List<Long> getVisitingPlayerIdList() {
		return IdAnalyzer.toList(getVisitingPlayerIds());
	}

	public void setVisitingPlayerIdList(List<Long> visitingPlayerList) {
		setVisitingPlayerIds(IdAnalyzer.toString(visitingPlayerList));
	}

	public Long getHomeId() {
		return homeId;
	}

	public void setHomeId(Long homeId) {
		this.homeId = homeId;
	}

	public String getHomeLogo() {
		return homeLogo;
	}

	public void setHomeLogo(String homeLogo) {
		this.homeLogo = homeLogo;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}

	public Long getHomeCaptainId() {
		return homeCaptainId;
	}

	public void setHomeCaptainId(Long homeCaptainId) {
		this.homeCaptainId = homeCaptainId;
	}

	public Long getVisitingId() {
		return visitingId;
	}

	public void setVisitingId(Long visitingId) {
		this.visitingId = visitingId;
	}

	public String getVisitingLogo() {
		return visitingLogo;
	}

	public void setVisitingLogo(String visitingLogo) {
		this.visitingLogo = visitingLogo;
	}

	public String getVisitingName() {
		return visitingName;
	}

	public void setVisitingName(String visitingName) {
		this.visitingName = visitingName;
	}

	public Long getVisitingCaptainId() {
		return visitingCaptainId;
	}

	public void setVisitingCaptainId(Long visitingCaptainId) {
		this.visitingCaptainId = visitingCaptainId;
	}

	public String getHomeStartingIds() {
		return homeStartingIds;
	}

	public List<Long> getHomeStartingIdList() {
		return IdAnalyzer.toList(getHomeStartingIds());
	}
	
	public void setHomeStartingIds(String homeStartingIds) {
		this.homeStartingIds = homeStartingIds;
	}

	public String getVisitingStartingIds() {
		return visitingStartingIds;
	}
	
	public List<Long> getVisitingStartingIdList() {
		return IdAnalyzer.toList(getVisitingStartingIds());
	}

	public void setVisitingStartingIds(String visitingStartingIds) {
		this.visitingStartingIds = visitingStartingIds;
	}

	public String getMatchData() {
		return matchData;
	}

	public void setMatchData(String matchData) {
		this.matchData = matchData;
	}

	public Long getLeagueStageId() {
		return leagueStageId;
	}

	public void setLeagueStageId(Long leagueStageId) {
		this.leagueStageId = leagueStageId;
	}

	public Long getRoundId() {
		return roundId;
	}

	public void setRoundId(Long roundId) {
		this.roundId = roundId;
	}

	public String getRoundName() {
		return roundName;
	}

	public void setRoundName(String roundName) {
		this.roundName = roundName;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getQuarterTime() {
		return quarterTime;
	}

	public void setQuarterTime(Integer quarterTime) {
		this.quarterTime = quarterTime;
	}

	public Integer getQuarterCount() {
		return quarterCount;
	}

	public void setQuarterCount(Integer quarterCount) {
		this.quarterCount = quarterCount;
	}

	public Integer getQuarterRemainingTime() {
		return quarterRemainingTime;
	}

	public void setQuarterRemainingTime(Integer quarterRemainingTime) {
		this.quarterRemainingTime = quarterRemainingTime;
	}

	public String getHomeLineupIds() {
		return homeLineupIds;
	}
	
	public List<Long> getHomeLineupIdList() {
		return IdAnalyzer.toList(getHomeLineupIds());
	}

	public void setHomeLineupIds(String homeLineupIds) {
		this.homeLineupIds = homeLineupIds;
	}

	public String getVisitingLineupIds() {
		return visitingLineupIds;
	}
	
	public List<Long> getVisitingLineupIdList() {
		return IdAnalyzer.toList(getVisitingLineupIds());
	}

	public void setVisitingLineupIds(String visitingLineupIds) {
		this.visitingLineupIds = visitingLineupIds;
	}

	public String getMatchSpeak() {
		return matchSpeak;
	}

	public void setMatchSpeak(String actionSpeak) {
		this.matchSpeak = actionSpeak;
	}

}