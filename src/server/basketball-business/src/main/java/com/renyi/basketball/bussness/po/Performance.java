package com.renyi.basketball.bussness.po;

public class Performance {
	/**
	 * 球员射门数
	 */
	Integer uShootNum = 0;
	/**
	 * 球员助攻数
	 */
	Integer uAssistsNum = 0;
	/**
	 * 球员抢断数
	 */
	Integer uStealsNum = 0;
	/**
	 * 球员扑救数
	 */
	Integer uSavesNum = 0;
	/**
	 * 球员犯规数
	 */
	Integer uFoulNum = 0;
	/**
	 * 球队射门数
	 */
	Integer tShootNum = 0;
	/**
	 * 球队助攻数
	 */
	Integer tAssistsNum = 0;
	/**
	 * 球队抢断数
	 */
	Integer tStealsNum = 0;
	/**
	 * 球队扑救数
	 */
	Integer tSavesNum = 0;
	/**
	 * 球队犯规数
	 */
	Integer tFoulNum = 0;
	/**
	 * 射门占球队比例
	 */
	Double shoot = 0.00;
	/**
	 * 助攻占球队比例
	 */
	Double assists = 0.00;
	/**
	 * 抢断占球队比例
	 */
	Double steals = 0.00;
	/**
	 * 扑救占球队比例
	 */
	Double saves = 0.00;
	/**
	 * 犯规占球队比例
	 */
	Double foul = 0.00;

	public Integer getuShootNum() {
		return uShootNum;
	}

	public void setuShootNum(Integer uShootNum) {
		this.uShootNum = uShootNum;
	}

	public Integer getuAssistsNum() {
		return uAssistsNum;
	}

	public void setuAssistsNum(Integer uAssistsNum) {
		this.uAssistsNum = uAssistsNum;
	}

	public Integer getuStealsNum() {
		return uStealsNum;
	}

	public void setuStealsNum(Integer uStealsNum) {
		this.uStealsNum = uStealsNum;
	}

	public Integer getuSavesNum() {
		return uSavesNum;
	}

	public void setuSavesNum(Integer uSavesNum) {
		this.uSavesNum = uSavesNum;
	}

	public Integer getuFoulNum() {
		return uFoulNum;
	}

	public void setuFoulNum(Integer uFoulNum) {
		this.uFoulNum = uFoulNum;
	}

	public Integer gettShootNum() {
		return tShootNum;
	}

	public void settShootNum(Integer tShootNum) {
		this.tShootNum = tShootNum;
	}

	public Integer gettAssistsNum() {
		return tAssistsNum;
	}

	public void settAssistsNum(Integer tAssistsNum) {
		this.tAssistsNum = tAssistsNum;
	}

	public Integer gettStealsNum() {
		return tStealsNum;
	}

	public void settStealsNum(Integer tStealsNum) {
		this.tStealsNum = tStealsNum;
	}

	public Integer gettSavesNum() {
		return tSavesNum;
	}

	public void settSavesNum(Integer tSavesNum) {
		this.tSavesNum = tSavesNum;
	}

	public Integer gettFoulNum() {
		return tFoulNum;
	}

	public void settFoulNum(Integer tFoulNum) {
		this.tFoulNum = tFoulNum;
	}

	public Double getShoot() {
		return (double) uShootNum * 100 / (tShootNum == 0 ? 1 : tShootNum);
	}
	public void setShoot(Double shoot) {
		this.shoot = shoot;
	}

	public Double getAssists() {
		return (double) uAssistsNum * 100
				/ (tAssistsNum == 0 ? 1 : tAssistsNum);
	}

	public void setAssists(Double assists) {
		this.assists = assists;
	}

	public Double getSteals() {
		return (double) uStealsNum * 100 / (tStealsNum == 0 ? 1 : tStealsNum);
	}

	public void setSteals(Double steals) {
		this.steals = steals;
	}

	public Double getSaves() {
		return (double) uSavesNum * 100 / (tSavesNum == 0 ? 1 : tSavesNum);
	}

	public void setSaves(Double saves) {
		this.saves = saves;
	}

	public Double getFoul() {
		return (double) uFoulNum * 100 / (tFoulNum == 0 ? 1 : tFoulNum);
	}

	public void setFoul(Double foul) {
		this.foul = foul;
	}
}
