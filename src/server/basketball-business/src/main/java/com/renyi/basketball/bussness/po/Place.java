package com.renyi.basketball.bussness.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "place")
public class Place extends BaseEntity {
	/** 场地名称 */
	private String name;
	/** 图标 */
	private String icon;
	/** 所属场馆 */
	private Long arenasId;
	/** 营业开始时间 */
	private String dayStartTime;
	/** 结束时间 */
	private String dayEndTime;
	/** 场地价格 */
	private Integer money;
	/** 状态 */
	private Integer status;
	/** 所属公司 */
	private Long companyId;

	private Date createtime;

	private Date updatetime;

	private List<PlaceSchedule> placeScheduleList = new ArrayList<>();

	@Transient
	public List<PlaceSchedule> getPlaceScheduleList() {
		return placeScheduleList;
	}

	public void setPlaceScheduleList(List<PlaceSchedule> placeScheduleList) {
		this.placeScheduleList = placeScheduleList;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Long getArenasId() {
		return arenasId;
	}

	public void setArenasId(Long arenasId) {
		this.arenasId = arenasId;
	}

	public String getDayStartTime() {
		return dayStartTime;
	}

	public void setDayStartTime(String dayStartTime) {
		this.dayStartTime = dayStartTime;
	}

	public String getDayEndTime() {
		return dayEndTime;
	}

	public void setDayEndTime(String dayEndTime) {
		this.dayEndTime = dayEndTime;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Integer getMoney() {
		return money;
	}

	public void setMoney(Integer money) {
		this.money = money;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
