package com.renyi.basketball.bussness.po;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Table;

/**
 * Created by Roy.xiao on 2016/6/22.
 */
@Table(name = "place_schedule")
public class PlaceSchedule extends BaseEntity {
	/** 场馆ID. */
	private Integer arenasId;
	/** 场地ID. */
	private Integer placesId;
	/** 年. */
	private Integer year;
	/** 月. */
	private Integer month;
	/** 日。 */
	private Integer day;
	/** 开始时间. */
	private String start_time;
	/** 结束时间. */
	private String end_time;
	/** 当天的场地安排. */
	private List<HashMap<String, String>> schedule_today;
	/** 价格. */
	private Double price;
	/** 状态 . */
	private Integer status;
	/** 详情. */
	private String descs;
	/** 联系人. */
	private String contact;
	/** 联系电话. */
	private String tel;
	private Date createTime;
	private Date updateTime;

	public List<HashMap<String, String>> getSchedule_today() {
		// if (schedule_today != null) {
		// //将时间戳转为时间
		// for (HashMap<String, String> map : schedule_today) {
		// String start = map.get("start_time");
		// String end = map.get("end_time");
		// map.put("start_time",DateUtil.convertTimeStamp(Long.valueOf(start)));
		// map.put("end_time",DateUtil.convertTimeStamp(Long.valueOf(end)));
		// }
		// }
		//
		return schedule_today;
	}

	public void setSchedule_today(List<HashMap<String, String>> schedule_today) {
		this.schedule_today = schedule_today;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getArenasId() {
		return arenasId;
	}

	public void setArenasId(Integer arenasId) {
		this.arenasId = arenasId;
	}

	public Integer getPlacesId() {
		return placesId;
	}

	public void setPlacesId(Integer placesId) {
		this.placesId = placesId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescs() {
		return descs;
	}

	public void setDescs(String descs) {
		this.descs = descs;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	/**
	 * 设置详情
	 * 
	 * @param json
	 */
	// public void setScheduleJson(JSONObject json) {
	// if (json != null) {
	// desc = json.toString();
	// }
	// }
	//
	// public JSONObject getScheduleJson(){
	// if (null != desc) {
	// return JSONObject.fromObject(desc);
	// }
	// return null;
	// }

}
