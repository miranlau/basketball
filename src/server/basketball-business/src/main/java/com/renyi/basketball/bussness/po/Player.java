package com.renyi.basketball.bussness.po;

import java.util.Date;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.renyi.basketball.bussness.common.PlayerPosition;
import com.renyi.basketball.bussness.utils.JsonUtil;

/**
 * 球员信息
 */
@Table(name = "player")
public class Player extends BaseEntity {
	private static final long serialVersionUID = -6075261270660146997L;
	// 球队ID
	private Long teamId;
	// 姓名
	private String name;
	// 国籍
	private String nationality;
	// 出生日期
	private Date birthDate;
	// 球衣号码
	private Integer uniformNumber;
	// 位置
	private Integer position;
	// 身高
	private Integer height;
	// 体重
	private Integer weight;
	// 头像
	private String avatar;
	// 简介
	private String profile;
	// 运动生涯
	private String career;

	// ----------------Transient-------------------

	// 是否在场上
	private Boolean isLineup = false;
	// 是否报名比赛
	private Boolean isEnroll = false;
	// 是否首发
	private Boolean isStarting = false;
	// 一场比赛中累计上场时间，该字段为中间状态，非最终统计结果
	private Long playingTime;
	// 临时记录球员上场时间，该字段为中间状态，非最终统计结果
	private Long upTime = 0L;
	// 球队名
	private String teamName;
	// 球队Logo
	private String teamLogo;
	
	@Transient
	public Boolean getIsEnroll() {
		return isEnroll;
	}

	public void setIsEnroll(Boolean isRegister) {
		this.isEnroll = isRegister;
	}

	@Transient
	public Boolean getIsStarting() {
		return isStarting;
	}

	public void setIsStarting(Boolean isStarting) {
		this.isStarting = isStarting;
	}

	@Transient
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String playerName) {
		this.teamName = playerName;
	}

	@Transient
	public String getTeamLogo() {
		return teamLogo;
	}

	public void setTeamLogo(String teamLogo) {
		this.teamLogo = teamLogo;
	}

	@Transient
	public Long getPlayingTime() {
		return playingTime;
	}

	public void setPlayingTime(Long playingTime) {
		this.playingTime = playingTime;
	}

	@Transient
	public Long getUpTime() {
		return upTime;
	}

	public void setUpTime(Long upTime) {
		this.upTime = upTime;
	}
	
	@Transient
	public Boolean getIsLineup() {
		return isLineup;
	}

	public void setIsLineup(Boolean isLineup) {
		this.isLineup = isLineup;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getPosition() {
		return position;
	}
	
	@Transient
	public String getPositionName() {
		if (position > 0) {
			return PlayerPosition.valueOf(position).getName();
		}
		return null;
	}
	
	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUniformNumber() {
		return uniformNumber;
	}

	public void setUniformNumber(Integer uniformNumber) {
		this.uniformNumber = uniformNumber;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String toJson() {
		return JsonUtil.toJson(this);
	}

}
