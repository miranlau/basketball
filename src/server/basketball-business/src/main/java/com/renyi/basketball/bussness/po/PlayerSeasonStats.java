package com.renyi.basketball.bussness.po;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.renyi.basketball.bussness.common.StatsAlgorithm;

/**
 * 球员的赛季数据统计
 */
@Table(name = "player_season_stats")
public class PlayerSeasonStats extends BaseEntity {
	private static final long serialVersionUID = -6199314131730179910L;
	// 球员ID
	private long playerId;
	// 赛事ID
	private long leagueId;
	// 赛事阶段
	private long leagueStageId;
	// 比赛IDs
	private String matchIds;
	// 比赛轮次ID
	private long roundId;
	// 球队ID
	private long teamId;
	// 上场时间
	private float playingTime;
	// ---------------------基础数据--------------------------------
	// 得分
	private float score;
	// 两分球命中数
	private float twoPointsShot;
	// 两分球投篮次数
	private float twoPointsAttempt;
	// 三分球命中数
	private float threePointsShot;
	// 三分球投篮次数
	private float threePointsAttempt;
	// 罚篮命中数
	private float freeThrowShot;
	// 罚篮次数
	private float freeThrowAttempt;
	// 进攻篮板
	private float offensiveRebound;
	// 防守篮板
	private float defensiveRebound;
	// 助攻
	private float assist;
	// 抢断
	private float steal;
	// 失误
	private float turnover;
	// 盖帽
	private float blockShot;
	// 犯规
	private float foul;
	// ---------------------基础数据--------------------------------
	// ---------------------进阶数据(投篮区域统计)-------------------
	// 油漆区命中数
	private float twoPaintedShot;
	// 油漆区投篮次数
	private float twoPaintedAttempt;
	// 正面投篮命中数
	private float twoFrontShot;
	// 正面投篮次数
	private float twoFrontAttempt;
	// 左侧投篮命中数
	private float twoLeftsideShot;
	// 左侧投篮次数
	private float twoLeftsideAttempt;
	// 右侧投篮命中数
	private float twoRightsideShot;
	// 右侧投篮次数
	private float twoRightsideAttempt;
	// 左侧底角投篮命中数
	private float threeLeftWingShot;
	// 左侧底角投篮次数
	private float threeLeftWingAttempt;
	// 右侧底角投篮命中数
	private float threeRightWingShot;
	// 右侧底角投篮次数
	private float threeRightWingAttempt;
	// 左侧45度投篮命中数
	private float threeLeftside45Shot;
	// 左侧45度投篮次数
	private float threeLeftside45Attempt;
	// 右侧45度投篮命中数
	private float threeRightside45Shot;
	// 右侧45度投篮次数
	private float threeRightside45Attempt;
	// 顶弧篮命中数
	private float threeRootArcShot;
	// 顶弧投篮次数
	private float threeRootArcAttempt;
	// ---------------------进阶数据(投篮区域统计)------------------

	// ---------------------进阶数据(其它)--------------------------
	// 接球投篮命中数
	private float standingShot;
	// 接球投篮次数
	private float standingAttempt;
	// 运球投篮命中数
	private float dribblingShot;
	// 运球投篮次数
	private float dribblingAttempt;
	// 效率值(The Player Efficiency Rating)
	private float per;
	// 正负值 (real plus-minus)
	private float rpm;
	// 篮下进攻侧重比
	private float paintedOffensivePct;
	// 左侧进攻侧重比
	private float leftOffensivePct;
	// 右侧进攻侧重比
	private float rightOffensivePct;
	// 正面进攻侧重比
	private float frontOffensivePct;
	// 进攻占比
	private float offensiveRatio;
	// 有效命中率 (Effective Field Goal Percentage)
	private float efgPct;
	// 真实命中率 (True Shooting Percentage)
	private float tsPct;
	// 进攻成功率
	private float offensivePct;
	// 助攻率
	private float assistPct;
	// 篮板率
	private float reboundPct;
	// 失误率
	private float turnoverPct;

	// ---------------------进阶数据(其它)--------------------------------

	// -----------------------------非持久化属性--------------------------
	// 球员名
	private String playerName;
	// 球衣号码
	private int uniformNumber;
	// 赛事名
	private String leagueName;
	// 球队名
	private String teamName;
	// 比赛轮次名
	private long roundName;

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(long leagueId) {
		this.leagueId = leagueId;
	}

	public long getLeagueStageId() {
		return leagueStageId;
	}

	public void setLeagueStageId(long leagueStage) {
		this.leagueStageId = leagueStage;
	}

	@Transient
	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	public long getTeamId() {
		return teamId;
	}

	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}

	@Transient
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public float getPlayingTime() {
		return playingTime;
	}

	public void setPlayingTime(float playingTime) {
		this.playingTime = playingTime;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public float getTwoPointsShot() {
		return twoPointsShot;
	}

	public void setTwoPointsShot(float twoPointsShot) {
		this.twoPointsShot = twoPointsShot;
	}

	public float getTwoPointsAttempt() {
		return twoPointsAttempt;
	}

	public void setTwoPointsAttempt(float twoPointsAttempt) {
		this.twoPointsAttempt = twoPointsAttempt;
	}

	public float getTwoPointsPct() {
		return StatsAlgorithm.percent(twoPointsShot, twoPointsAttempt);
	}

	public float getThreePointsShot() {
		return threePointsShot;
	}

	public void setThreePointsShot(float threePointsShot) {
		this.threePointsShot = threePointsShot;
	}

	public float getThreePointsAttempt() {
		return threePointsAttempt;
	}

	public void setThreePointsAttempt(float threePointsAttempt) {
		this.threePointsAttempt = threePointsAttempt;
	}

	public float getThreePointsPct() {
		return StatsAlgorithm.percent(threePointsShot, threePointsAttempt);
	}

	public float getFreeThrowShot() {
		return freeThrowShot;
	}

	public void setFreeThrowShot(float freeThrowShot) {
		this.freeThrowShot = freeThrowShot;
	}

	public float getFreeThrowAttempt() {
		return freeThrowAttempt;
	}

	public void setFreeThrowAttempt(float freeThrowAttempt) {
		this.freeThrowAttempt = freeThrowAttempt;
	}

	public float getOffensiveRebound() {
		return offensiveRebound;
	}

	public void setOffensiveRebound(float offensiveRebound) {
		this.offensiveRebound = offensiveRebound;
	}

	public float getDefensiveRebound() {
		return defensiveRebound;
	}

	public void setDefensiveRebound(float defensiveRebound) {
		this.defensiveRebound = defensiveRebound;
	}

	public float getAssist() {
		return assist;
	}

	public void setAssist(float assist) {
		this.assist = assist;
	}

	public float getSteal() {
		return steal;
	}

	public void setSteal(float steal) {
		this.steal = steal;
	}

	public float getTurnover() {
		return turnover;
	}

	public void setTurnover(float turnover) {
		this.turnover = turnover;
	}

	public float getBlockShot() {
		return blockShot;
	}

	public void setBlockShot(float blockShot) {
		this.blockShot = blockShot;
	}

	public float getFoul() {
		return foul;
	}

	public void setFoul(float foul) {
		this.foul = foul;
	}

	public float getTwoPaintedShot() {
		return twoPaintedShot;
	}

	public void setTwoPaintedShot(float twoPaintedShot) {
		this.twoPaintedShot = twoPaintedShot;
	}

	public float getTwoPaintedAttempt() {
		return twoPaintedAttempt;
	}

	public void setTwoPaintedAttempt(float twoPaintedAttempt) {
		this.twoPaintedAttempt = twoPaintedAttempt;
	}

	public float getTwoFrontShot() {
		return twoFrontShot;
	}

	public void setTwoFrontShot(float twoFrontShot) {
		this.twoFrontShot = twoFrontShot;
	}

	public float getTwoFrontAttempt() {
		return twoFrontAttempt;
	}

	public void setTwoFrontAttempt(float twoFrontAttempt) {
		this.twoFrontAttempt = twoFrontAttempt;
	}

	public float getTwoLeftsideShot() {
		return twoLeftsideShot;
	}

	public void setTwoLeftsideShot(float twoLeftsideShot) {
		this.twoLeftsideShot = twoLeftsideShot;
	}

	public float getTwoLeftsideAttempt() {
		return twoLeftsideAttempt;
	}

	public void setTwoLeftsideAttempt(float twoLeftsideAttempt) {
		this.twoLeftsideAttempt = twoLeftsideAttempt;
	}

	public float getTwoRightsideShot() {
		return twoRightsideShot;
	}

	public void setTwoRightsideShot(float twoRightsideShot) {
		this.twoRightsideShot = twoRightsideShot;
	}

	public float getTwoRightsideAttempt() {
		return twoRightsideAttempt;
	}

	public void setTwoRightsideAttempt(float twoRightsideAttempt) {
		this.twoRightsideAttempt = twoRightsideAttempt;
	}

	public float getThreeLeftWingShot() {
		return threeLeftWingShot;
	}

	public void setThreeLeftWingShot(float threeLeftWingShot) {
		this.threeLeftWingShot = threeLeftWingShot;
	}

	public float getThreeLeftWingAttempt() {
		return threeLeftWingAttempt;
	}

	public void setThreeLeftWingAttempt(float threeLeftWingAttempt) {
		this.threeLeftWingAttempt = threeLeftWingAttempt;
	}

	public float getThreeRightWingShot() {
		return threeRightWingShot;
	}

	public void setThreeRightWingShot(float threeRightWingShot) {
		this.threeRightWingShot = threeRightWingShot;
	}

	public float getThreeRightWingAttempt() {
		return threeRightWingAttempt;
	}

	public void setThreeRightWingAttempt(float threeRightWingAttempt) {
		this.threeRightWingAttempt = threeRightWingAttempt;
	}

	public float getThreeLeftside45Shot() {
		return threeLeftside45Shot;
	}

	public void setThreeLeftside45Shot(float threeLeftside45Shot) {
		this.threeLeftside45Shot = threeLeftside45Shot;
	}

	public float getThreeLeftside45Attempt() {
		return threeLeftside45Attempt;
	}

	public void setThreeLeftside45Attempt(float threeLeftside45Attempt) {
		this.threeLeftside45Attempt = threeLeftside45Attempt;
	}

	public float getThreeRightside45Shot() {
		return threeRightside45Shot;
	}

	public void setThreeRightside45Shot(float threeRightside45Shot) {
		this.threeRightside45Shot = threeRightside45Shot;
	}

	public float getThreeRightside45Attempt() {
		return threeRightside45Attempt;
	}

	public void setThreeRightside45Attempt(float threeRightside45Attempt) {
		this.threeRightside45Attempt = threeRightside45Attempt;
	}

	public float getThreeRootArcShot() {
		return threeRootArcShot;
	}

	public void setThreeRootArcShot(float threeRootArcShot) {
		this.threeRootArcShot = threeRootArcShot;
	}

	public float getThreeRootArcAttempt() {
		return threeRootArcAttempt;
	}

	public void setThreeRootArcAttempt(float threeRootArcAttempt) {
		this.threeRootArcAttempt = threeRootArcAttempt;
	}

	public float getStandingShot() {
		return standingShot;
	}

	public void setStandingShot(float standingShot) {
		this.standingShot = standingShot;
	}

	public float getStandingAttempt() {
		return standingAttempt;
	}

	public void setStandingAttempt(float standingAttempt) {
		this.standingAttempt = standingAttempt;
	}

	public float getDribblingShot() {
		return dribblingShot;
	}

	public void setDribblingShot(float dribblingShot) {
		this.dribblingShot = dribblingShot;
	}

	public float getDribblingAttempt() {
		return dribblingAttempt;
	}

	public void setDribblingAttempt(float dribblingAttempt) {
		this.dribblingAttempt = dribblingAttempt;
	}

	public float getFrontOffensivePct() {
		return frontOffensivePct;
	}

	public void setFrontOffensivePct(float frontOffensivePct) {
		this.frontOffensivePct = frontOffensivePct;
	}

	public float getPer() {
		return per;
	}

	public void setPer(float per) {
		this.per = per;
	}

	public float getRpm() {
		return rpm;
	}

	public void setRpm(float rpm) {
		this.rpm = rpm;
	}

	public float getPaintedOffensivePct() {
		return paintedOffensivePct;
	}

	public void setPaintedOffensivePct(float paintedOffensivePct) {
		this.paintedOffensivePct = paintedOffensivePct;
	}

	public float getLeftOffensivePct() {
		return leftOffensivePct;
	}

	public void setLeftOffensivePct(float leftOffensivePct) {
		this.leftOffensivePct = leftOffensivePct;
	}

	public float getRightOffensivePct() {
		return rightOffensivePct;
	}

	public void setRightOffensivePct(float rightOffensivePct) {
		this.rightOffensivePct = rightOffensivePct;
	}

	public float getOffensiveRatio() {
		return offensiveRatio;
	}

	public void setOffensiveRatio(float offensiveRatio) {
		this.offensiveRatio = offensiveRatio;
	}

	public float getEfgPct() {
		return efgPct;
	}

	public void setEfgPct(float efgPct) {
		this.efgPct = efgPct;
	}

	public float getTsPct() {
		return tsPct;
	}

	public void setTsPct(float tsPct) {
		this.tsPct = tsPct;
	}

	public float getOffensivePct() {
		return offensivePct;
	}

	public void setOffensivePct(float offensivePct) {
		this.offensivePct = offensivePct;
	}

	public float getAssistPct() {
		return assistPct;
	}

	public void setAssistPct(float assistPct) {
		this.assistPct = assistPct;
	}

	public float getReboundPct() {
		return reboundPct;
	}

	public void setReboundPct(float reboundPct) {
		this.reboundPct = reboundPct;
	}

	public float getTurnoverPct() {
		return turnoverPct;
	}

	public void setTurnoverPct(float turnoverPct) {
		this.turnoverPct = turnoverPct;
	}

	public long getRoundId() {
		return roundId;
	}

	public void setRoundId(long roundId) {
		this.roundId = roundId;
	}

	@Transient
	public long getRoundName() {
		return roundName;
	}

	public void setRoundName(long roundName) {
		this.roundName = roundName;
	}

	public String getMatchIds() {
		return matchIds;
	}

	public void setMatchIds(String matchIds) {
		this.matchIds = matchIds;
	}

	@Transient
	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Transient
	public int getUniformNumber() {
		return uniformNumber;
	}

	public void setUniformNumber(int uniformNumber) {
		this.uniformNumber = uniformNumber;
	}

}
