package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "product")
public class Product extends BaseEntity {

	/**
	 * 商品名称
	 */
	private String name;

	/**
	 * 公司名称
	 */
	private String company;

	/**
	 * 进价
	 */
	private Float inPrice;

	/**
	 * 售价
	 */
	private Float outPrice;

	/**
	 * 图片路径
	 */
	private String image;

	/**
	 * T描述
	 */
	private String description;

	

	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}


	public String getCompany() {
		return company;
	}

	
	public void setCompany(String company) {
		this.company = company == null ? null : company.trim();
	}

	
	public Float getInPrice() {
		return inPrice;
	}

	public void setInPrice(Float inPrice) {
		this.inPrice = inPrice;
	}

	public Float getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Float outPrice) {
		this.outPrice = outPrice;
	}


	public String getImage() {
		return image;
	}

	
	public void setImage(String image) {
		this.image = image == null ? null : image.trim();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description == null ? null : description.trim();
	}


}