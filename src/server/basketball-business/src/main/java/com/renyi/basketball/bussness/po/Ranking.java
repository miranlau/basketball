package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "ranking")
public class Ranking extends BaseEntity {
	private Long leagueId;
	private Long leagueStageId;
	private Integer type;
	private String ranks;

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Long getLeagueStageId() {
		return leagueStageId;
	}

	public void setLeagueStageId(Long leagueStageId) {
		this.leagueStageId = leagueStageId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRanks() {
		return ranks;
	}

	public void setRanks(String ranks) {
		this.ranks = ranks;
	}

}
