package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

/**
 * Created by Roy.xiao on 2016/6/1.
 */
@Table(name = "read_laud")
public class ReadLaud extends BaseEntity {
	public static final Integer NUM_TYPE_READ = 0;// 阅读
	public static final Integer NUM_TYPE_LAUD = 1;// 点赞
	/** 用户ID */
	private Long userId;

	/** 动态ID */
	private Long dynamicid;

	/** 类型 */
	private Integer type;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getDynamicid() {
		return dynamicid;
	}

	public void setDynamicid(Long dynamicid) {
		this.dynamicid = dynamicid;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
