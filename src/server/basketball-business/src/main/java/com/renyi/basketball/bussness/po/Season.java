package com.renyi.basketball.bussness.po;

import java.util.Date;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.renyi.basketball.bussness.common.LeagueStatus;

@Table(name = "season")
public class Season extends BaseEntity {
	private static final long serialVersionUID = -8299405346277123357L;
	/** 赛季名称 */
	private String name;
	/** 对应的联赛id */
	private Long leagueId;
	/** 赛事开始日期. */
	private Date startTime;
	/** 赛事结束日期. */
	private Date endTime;
	/** 状态. */
	private Integer status = LeagueStatus.signing.code();
	/** 参赛球队id,用逗号分开. */
	private String teams;
	/** 赛季简介 */
	private String profile;
	
	//==================================================================
	private String leagueName;
    //参加比赛球队列表
    private List<Team> teamList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(Long leagueId) {
		this.leagueId = leagueId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTeams() {
		return teams;
	}

	public void setTeams(String teams) {
		this.teams = teams;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	@Transient
	public List<Team> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<Team> teamList) {
		this.teamList = teamList;
	}

	@Transient
	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

}
