package com.renyi.basketball.bussness.po;

import java.util.List;

import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "team")
public class Team extends BaseEntity {
	private static final long serialVersionUID = 3759450587736776064L;
	/** 球队名称. */
	private String name;
	/** 球队LOGO */
	private String logo;
	/** 球队主场 */
	private Long venueId;
	/** 球队属性. */
	private String profile;
	/** 是否解散. */
	private Boolean isDismissed = false;
	/** 主场Venue*/
	private String venueName;
	/** 球队队员*/
	private List<Player> players;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Boolean getIsDismissed() {
		return isDismissed;
	}

	public void setIsDismissed(Boolean isDismissed) {
		this.isDismissed = isDismissed;
	}

	public Long getVenueId() {
		return venueId;
	}

	public void setVenueId(Long venueId) {
		this.venueId = venueId;
	}

	@Transient
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	@Transient
	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}


}
