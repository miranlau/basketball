package com.renyi.basketball.bussness.po;

import java.util.Date;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.renyi.basketball.bussness.common.StatsAlgorithm;

/**
 * 球队的比赛技术统计
 */
@Table(name = "team_match_stats")
public class TeamMatchStats extends BaseEntity {
	private static final long serialVersionUID = -5450784492921081407L;
	// 球队ID
	private long teamId;
	// 赛事ID
	private long leagueId;
	// 赛事阶段
	private long leagueStageId;
	// 比赛轮次ID
	private long roundId;
	// 比赛ID
	private long matchId;
	// 比赛时间
	private Date matchTime;
	// 是否主场
	private boolean isHomeCourt;
	// 主队得分
	private int homeScore;
	// 客队得分
	private int visitingScore;
	// 是否取胜
	private boolean isWin;
	// 对手球队的ID
	private long opposingTeamId;
	// ---------------------基础数据--------------------------------
	// 两分球命中数
	private int twoPointsShot;
	// 两分球投篮次数
	private int twoPointsAttempt;
	// 三分球命中数
	private int threePointsShot;
	// 三分球投篮次数
	private int threePointsAttempt;
	// 罚篮命中数
	private int freeThrowShot;
	// 罚篮次数
	private int freeThrowAttempt;
	// 进攻篮板
	private int offensiveRebound;
	// 防守篮板
	private int defensiveRebound;
	// 助攻
	private int assist;
	// 抢断
	private int steal;
	// 失误
	private int turnover;
	// 盖帽
	private int blockShot;
	// 犯规
	private int foul;
	// ---------------------基础数据--------------------------------
	// ---------------------进阶数据(投篮区域统计)-------------------
	// 油漆区命中数
	private int twoPaintedShot;
	// 油漆区投篮次数
	private int twoPaintedAttempt;
	// 正面投篮命中数
	private int twoFrontShot;
	// 正面投篮次数
	private int twoFrontAttempt;
	// 左侧投篮命中数
	private int twoLeftsideShot;
	// 左侧投篮次数
	private int twoLeftsideAttempt;
	// 右侧投篮命中数
	private int twoRightsideShot;
	// 右侧投篮次数
	private int twoRightsideAttempt;
	// 左侧底角投篮命中数
	private int threeLeftWingShot;
	// 左侧底角投篮次数
	private int threeLeftWingAttempt;
	// 右侧底角投篮命中数
	private int threeRightWingShot;
	// 右侧底角投篮次数
	private int threeRightWingAttempt;
	// 左侧45度投篮命中数
	private int threeLeftside45Shot;
	// 左侧45度投篮次数
	private int threeLeftside45Attempt;
	// 右侧45度投篮命中数
	private int threeRightside45Shot;
	// 右侧45度投篮次数
	private int threeRightside45Attempt;
	// 顶弧篮命中数
	private int threeRootArcShot;
	// 顶弧投篮次数
	private int threeRootArcAttempt;
	// ---------------------进阶数据(投篮区域统计)------------------

	// ---------------------进阶数据(其它)--------------------------
	// 篮下进攻侧重比
	private float paintedOffensivePct;
	// 左侧进攻侧重比
	private float leftOffensivePct;
	// 右侧进攻侧重比
	private float rightOffensivePct;
	// 正面进攻侧重比
	private float frontOffensivePct;
	// 接球投篮命中数
	private int standingShot;
	// 接球投篮次数
	private int standingAttempt;
	// 运球投篮命中数
	private int dribblingShot;
	// 运球投篮次数
	private int dribblingAttempt;
	// 进攻节奏(进攻的总次数)
	private int offensiveAttempt;
	// 进攻效率 (offensive efficiency)
	private float offensiveEff;
	// 防守效率 (defensive efficiency)
	private float defensiveEff;
	// 有效命中率 (Effective Field Goal Percentage)
	private float efgPct;
	// 真实命中率 (True Shooting Percentage)
	private float tsPct;
	// 进攻成功率
	private float offensivePct;
	// 助攻率
	private float assistPct;
	// 篮板率
	private float reboundPct;
	// 失误率
	private float turnoverPct;
	// ---------------------进阶数据(其它)--------------------------------

	// ---------------------@Transient 非持久化的属性---------------------

	// 球队名
	private String teamName;
	// 球队LOGO
	private String teamLogo;
	// 对手球队名
	private String opposingTeamName;
	// 比赛轮次名
	private long roundName;

	@Transient
	public String getTeamLogo() {
		return teamLogo;
	}

	public void setTeamLogo(String teamLogo) {
		this.teamLogo = teamLogo;
	}

	public int getRebound() {
		return offensiveRebound + defensiveRebound;
	}

	public float getTwoPointsPct() {
		return StatsAlgorithm.percent(twoPointsShot, twoPointsAttempt);
	}

	public float getThreePointsPct() {
		return StatsAlgorithm.percent(threePointsShot, threePointsAttempt);
	}

	public float getFreeThrowPct() {
		return StatsAlgorithm.percent(freeThrowShot, freeThrowAttempt);
	}

	public long getTeamId() {
		return teamId;
	}

	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}

	@Transient
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public long getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(long leagueId) {
		this.leagueId = leagueId;
	}

	public long getLeagueStageId() {
		return leagueStageId;
	}

	public void setLeagueStageId(long leagueStageId) {
		this.leagueStageId = leagueStageId;
	}

	public long getMatchId() {
		return matchId;
	}

	public void setMatchId(long matchId) {
		this.matchId = matchId;
	}

	public Date getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(Date matchTime) {
		this.matchTime = matchTime;
	}

	public boolean getIsHomeCourt() {
		return isHomeCourt;
	}

	public void setIsHomeCourt(boolean isHomeCourt) {
		this.isHomeCourt = isHomeCourt;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	public int getVisitingScore() {
		return visitingScore;
	}

	public void setVisitingScore(int visitingScore) {
		this.visitingScore = visitingScore;
	}

	public boolean getIsWin() {
		return isWin;
	}

	public void setIsWin(boolean isWin) {
		this.isWin = isWin;
	}

	public long getOpposingTeamId() {
		return opposingTeamId;
	}

	public void setOpposingTeamId(long opposingTeamId) {
		this.opposingTeamId = opposingTeamId;
	}

	@Transient
	public String getOpposingTeamName() {
		return opposingTeamName;
	}

	public void setOpposingTeamName(String opposingTeamName) {
		this.opposingTeamName = opposingTeamName;
	}

	public int getTwoPointsShot() {
		return twoPointsShot;
	}

	public void setTwoPointsShot(int twoPointsShot) {
		this.twoPointsShot = twoPointsShot;
	}

	public int getTwoPointsAttempt() {
		return twoPointsAttempt;
	}

	public void setTwoPointsAttempt(int twoPointsAttempt) {
		this.twoPointsAttempt = twoPointsAttempt;
	}

	public int getThreePointsShot() {
		return threePointsShot;
	}

	public void setThreePointsShot(int threePointsShot) {
		this.threePointsShot = threePointsShot;
	}

	public int getThreePointsAttempt() {
		return threePointsAttempt;
	}

	public void setThreePointsAttempt(int threePointsAttempt) {
		this.threePointsAttempt = threePointsAttempt;
	}

	public int getFreeThrowShot() {
		return freeThrowShot;
	}

	public void setFreeThrowShot(int freeThrowShot) {
		this.freeThrowShot = freeThrowShot;
	}

	public int getFreeThrowAttempt() {
		return freeThrowAttempt;
	}

	public void setFreeThrowAttempt(int freeThrowAttempt) {
		this.freeThrowAttempt = freeThrowAttempt;
	}

	public int getOffensiveRebound() {
		return offensiveRebound;
	}

	public void setOffensiveRebound(int offensiveRebound) {
		this.offensiveRebound = offensiveRebound;
	}

	public int getDefensiveRebound() {
		return defensiveRebound;
	}

	public void setDefensiveRebound(int defensiveRebound) {
		this.defensiveRebound = defensiveRebound;
	}

	public int getAssist() {
		return assist;
	}

	public void setAssist(int assist) {
		this.assist = assist;
	}

	public int getSteal() {
		return steal;
	}

	public void setSteal(int steal) {
		this.steal = steal;
	}

	public int getTurnover() {
		return turnover;
	}

	public void setTurnover(int turnover) {
		this.turnover = turnover;
	}

	public int getBlockShot() {
		return blockShot;
	}

	public void setBlockShot(int blockShot) {
		this.blockShot = blockShot;
	}

	public int getFoul() {
		return foul;
	}

	public void setFoul(int foul) {
		this.foul = foul;
	}

	public int getTwoPaintedShot() {
		return twoPaintedShot;
	}

	public void setTwoPaintedShot(int twoPaintedShot) {
		this.twoPaintedShot = twoPaintedShot;
	}

	public int getTwoPaintedAttempt() {
		return twoPaintedAttempt;
	}

	public void setTwoPaintedAttempt(int twoPaintedAttempt) {
		this.twoPaintedAttempt = twoPaintedAttempt;
	}

	public float getTwoPaintedPct() {
		return StatsAlgorithm.percent(twoPaintedShot, twoPaintedAttempt);
	}

	public int getTwoFrontShot() {
		return twoFrontShot;
	}

	public void setTwoFrontShot(int twoFrontShot) {
		this.twoFrontShot = twoFrontShot;
	}

	public int getTwoFrontAttempt() {
		return twoFrontAttempt;
	}

	public void setTwoFrontAttempt(int twoFrontAttempt) {
		this.twoFrontAttempt = twoFrontAttempt;
	}

	public float getTwoFrontPct() {
		return StatsAlgorithm.percent(twoFrontShot, twoFrontAttempt);
	}

	public int getTwoLeftsideShot() {
		return twoLeftsideShot;
	}

	public void setTwoLeftsideShot(int twoLeftsideShot) {
		this.twoLeftsideShot = twoLeftsideShot;
	}

	public int getTwoLeftsideAttempt() {
		return twoLeftsideAttempt;
	}

	public void setTwoLeftsideAttempt(int twoLeftsideAttempt) {
		this.twoLeftsideAttempt = twoLeftsideAttempt;
	}

	public float getTwoLeftsidePct() {
		return StatsAlgorithm.percent(twoLeftsideShot, twoLeftsideAttempt);
	}

	public int getTwoRightsideShot() {
		return twoRightsideShot;
	}

	public void setTwoRightsideShot(int twoRightsideShot) {
		this.twoRightsideShot = twoRightsideShot;
	}

	public int getTwoRightsideAttempt() {
		return twoRightsideAttempt;
	}

	public void setTwoRightsideAttempt(int twoRightsideAttempt) {
		this.twoRightsideAttempt = twoRightsideAttempt;
	}

	public float getTwoRightsidePct() {
		return StatsAlgorithm.percent(twoRightsideShot, twoRightsideAttempt);
	}

	public int getThreeLeftWingShot() {
		return threeLeftWingShot;
	}

	public void setThreeLeftWingShot(int threeLeftWingShot) {
		this.threeLeftWingShot = threeLeftWingShot;
	}

	public int getThreeLeftWingAttempt() {
		return threeLeftWingAttempt;
	}

	public void setThreeLeftWingAttempt(int threeLeftWingAttempt) {
		this.threeLeftWingAttempt = threeLeftWingAttempt;
	}

	public float getThreeLeftWingPct() {
		return StatsAlgorithm.percent(threeLeftWingShot, threeLeftWingAttempt);
	}

	public int getThreeRightWingShot() {
		return threeRightWingShot;
	}

	public void setThreeRightWingShot(int threeRightWingShot) {
		this.threeRightWingShot = threeRightWingShot;
	}

	public void setThreeRightWingAttempt(int threeRightWingAttempt) {
		this.threeRightWingAttempt = threeRightWingAttempt;
	}

	public int getThreeRightWingAttempt() {
		return threeRightWingAttempt;
	}

	public float getThreeRightWingPct() {
		return StatsAlgorithm.percent(threeRightWingShot, threeRightWingAttempt);
	}

	public int getThreeLeftside45Shot() {
		return threeLeftside45Shot;
	}

	public void setThreeLeftside45Shot(int threeLeftside45Shot) {
		this.threeLeftside45Shot = threeLeftside45Shot;
	}

	public int getThreeLeftside45Attempt() {
		return threeLeftside45Attempt;
	}

	public void setThreeLeftside45Attempt(int threeLeftside45Attempt) {
		this.threeLeftside45Attempt = threeLeftside45Attempt;
	}

	public float getThreeLeftside45Pct() {
		return StatsAlgorithm.percent(threeLeftside45Shot, threeLeftside45Attempt);
	}

	public int getThreeRightside45Shot() {
		return threeRightside45Shot;
	}

	public void setThreeRightside45Shot(int threeRightside45Shot) {
		this.threeRightside45Shot = threeRightside45Shot;
	}

	public int getThreeRightside45Attempt() {
		return threeRightside45Attempt;
	}

	public void setThreeRightside45Attempt(int threeRightside45Attempt) {
		this.threeRightside45Attempt = threeRightside45Attempt;
	}

	public float getThreeRightside45Pct() {
		return StatsAlgorithm.percent(threeRightside45Shot, threeRightside45Attempt);
	}

	public int getThreeRootArcShot() {
		return threeRootArcShot;
	}

	public void setThreeRootArcShot(int threeRootArcShot) {
		this.threeRootArcShot = threeRootArcShot;
	}

	public int getThreeRootArcAttempt() {
		return threeRootArcAttempt;
	}

	public void setThreeRootArcAttempt(int threeRootArcAttempt) {
		this.threeRootArcAttempt = threeRootArcAttempt;
	}

	public float getThreeRootArcPct() {
		return StatsAlgorithm.percent(threeRootArcShot, threeRootArcAttempt);
	}

	public float getPaintedOffensivePct() {
		return paintedOffensivePct;
	}

	public void setPaintedOffensivePct(float paintedOffensivePct) {
		this.paintedOffensivePct = paintedOffensivePct;
	}

	public float getLeftOffensivePct() {
		return leftOffensivePct;
	}

	public void setLeftOffensivePct(float leftOffensivePct) {
		this.leftOffensivePct = leftOffensivePct;
	}

	public float getRightOffensivePct() {
		return rightOffensivePct;
	}

	public void setRightOffensivePct(float rightOffensivePct) {
		this.rightOffensivePct = rightOffensivePct;
	}

	public float getFrontOffensivePct() {
		return frontOffensivePct;
	}

	public void setFrontOffensivePct(float frontOffensivePct) {
		this.frontOffensivePct = frontOffensivePct;
	}

	public int getStandingShot() {
		return standingShot;
	}

	public void setStandingShot(int standingShot) {
		this.standingShot = standingShot;
	}

	public int getStandingAttempt() {
		return standingAttempt;
	}

	public void setStandingAttempt(int standingAttempt) {
		this.standingAttempt = standingAttempt;
	}

	public int getDribblingShot() {
		return dribblingShot;
	}

	public void setDribblingShot(int dribblingShot) {
		this.dribblingShot = dribblingShot;
	}

	public int getDribblingAttempt() {
		return dribblingAttempt;
	}

	public void setDribblingAttempt(int dribblingAttempt) {
		this.dribblingAttempt = dribblingAttempt;
	}

	public int getOffensiveAttempt() {
		return offensiveAttempt;
	}

	public void setOffensiveAttempt(int offensiveAttempt) {
		this.offensiveAttempt = offensiveAttempt;
	}

	public float getOffensiveEff() {
		return offensiveEff;
	}

	public void setOffensiveEff(float offensiveEff) {
		this.offensiveEff = offensiveEff;
	}

	public float getDefensiveEff() {
		return defensiveEff;
	}

	public void setDefensiveEff(float defensiveEff) {
		this.defensiveEff = defensiveEff;
	}

	public float getEfgPct() {
		return efgPct;
	}

	public void setEfgPct(float efgPct) {
		this.efgPct = efgPct;
	}

	public float getTsPct() {
		return tsPct;
	}

	public void setTsPct(float tsPct) {
		this.tsPct = tsPct;
	}

	public float getOffensivePct() {
		return offensivePct;
	}

	public void setOffensivePct(float offensivePct) {
		this.offensivePct = offensivePct;
	}

	public float getAssistPct() {
		return assistPct;
	}

	public void setAssistPct(float assistPct) {
		this.assistPct = assistPct;
	}

	public float getReboundPct() {
		return reboundPct;
	}

	public void setReboundPct(float reboundPct) {
		this.reboundPct = reboundPct;
	}

	public float getTurnoverPct() {
		return turnoverPct;
	}

	public void setTurnoverPct(float turnoverPct) {
		this.turnoverPct = turnoverPct;
	}

	public long getRoundId() {
		return roundId;
	}

	public void setRoundId(long roundId) {
		this.roundId = roundId;
	}

	@Transient
	public long getRoundName() {
		return roundName;
	}

	public void setRoundName(long roundName) {
		this.roundName = roundName;
	}

}
