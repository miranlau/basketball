package com.renyi.basketball.bussness.po;

import java.util.Date;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "user")
public class User extends BaseEntity {
	private static final long serialVersionUID = -4800125826611250491L;
	// 用户名
	private String userName;
	// 邮箱
	private String email;
	// 手机号
	private String mobile;
	// 密码
	@JsonIgnore
	private String password;
	// 昵称
	private String nickName;
	// 真实姓名
	private String realName;
	// 用户角色
	private Integer role;
	// 头像URL
	private String avatar;
	// 出生日期
	private Date birthDate;
	// 是否可以查看其它人的资料
	private Boolean canView;
	// 是否可以邀请其它人
	private Boolean canInvite;
	// 是否可以聊天
	private Boolean chatPush;
	// 自动更新
	private Boolean autoUpdate;
	// 设备ID
	@JsonIgnore
	private String deviceId;
	// 设备类型
	@JsonIgnore
	private Integer deviceType;
	// 设备token
	@JsonIgnore
	private String deviceToken;
	// 上次登录时间
	private Date lastLoginTime;

	// ----------------------Transient------------------------------
	// 访问Token
	private String accessToken;
	
	@Transient
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public Boolean getCanInvite() {
		return canInvite;
	}

	public void setCanInvite(Boolean canInvite) {
		this.canInvite = canInvite;
	}

	public Boolean getChatPush() {
		return chatPush;
	}

	public void setChatPush(Boolean chatPush) {
		this.chatPush = chatPush;
	}

	public Boolean getAutoUpdate() {
		return autoUpdate;
	}

	public void setAutoUpdate(Boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Integer getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

}