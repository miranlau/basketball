package com.renyi.basketball.bussness.po;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Table;

import com.renyi.basketball.bussness.constants.BasketballConstants;

@Table(name = "user_dynamic")
public class UserDynamic extends BaseEntity {
	/** 发布内容 */
	private String content;

	/** 图片URL */
	private String images;

	/** 评论数 */
	private Integer commentNum = 0;

	/** 阅读数 */
	private Integer readNum = 0;

	/** 点赞数 */
	private Integer laudNum = 0;

	/** 发布者 */
	private Long userId;

	/** 经度 */
	private Double longitude;

	/** 纬度 */
	private Double latitude;

	/** 发布位置 */
	private String location;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Integer getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	public Integer getReadNum() {
		return readNum;
	}

	public void setReadNum(Integer readNum) {
		this.readNum = readNum;
	}

	public Integer getLaudNum() {
		return laudNum;
	}

	public void setLaudNum(Integer laudNum) {
		this.laudNum = laudNum;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 设置图片字串
	 */
	public void setImageStr(Collection<String> imgs) {
		if (imgs != null) {
			Iterator<String> iterator = imgs.iterator();
			StringBuffer sb = new StringBuffer();
			while (iterator.hasNext()) {
				sb.append(BasketballConstants.SEPARATOR + iterator.next());
			}
			images = sb.substring(1);
		}
	}

	/**
	 * 获得图片集合
	 */
	public List<String> getImagesList() {
		if (images == null || images.trim().length() == 0) {
			return null;
		}
		List<String> result = new ArrayList<>();
		String[] imgs = images.split(BasketballConstants.SEPARATOR);
		for (String str : imgs) {
			result.add(String.valueOf(str));
		}
		return result;
	}
}
