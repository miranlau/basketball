package com.renyi.basketball.bussness.po;

import javax.persistence.Table;

@Table(name = "venue")
public class Venue extends BaseEntity {
	private static final long serialVersionUID = 8062360046753970475L;

	// 名称
	private String name;
	// 图片
	private String image;
	// 地址
	private String address;
	// 联系电话
	private String phone;
	// 球场简介
	private String profile;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

}
