package com.renyi.basketball.bussness.rmi;

import com.renyi.basketball.bussness.constants.Setting;

/**
 * RMI远程调用Service.
 * Created by Holen on 2016/6/22 0022.
 */
public interface SettingService {

    /**
     * 保存设置.
     * @param setting
     */
    void save(Setting setting);

    /**
     * 获取设置.
     * @return setting
     */
    Setting get();
}
