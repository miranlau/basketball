package com.renyi.basketball.bussness.rmi.impl;

import com.renyi.basketball.bussness.utils.SettingUtils;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.constants.Setting;
import com.renyi.basketball.bussness.rmi.SettingService;

/**
 * Created by Administrator on 2016/6/22 0022.
 */
@Service
public class SettingServiceImpl implements SettingService {

    @Override
    public void save(Setting setting) {
        SettingUtils.set(setting);
    }

    @Override
    public Setting get() {
        return SettingUtils.get();
    }
}
