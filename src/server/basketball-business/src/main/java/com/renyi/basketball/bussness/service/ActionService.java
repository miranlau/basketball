package com.renyi.basketball.bussness.service;

import java.util.List;
import java.util.Map;

import com.renyi.basketball.bussness.po.Action;

public interface ActionService extends BaseService<Action, Long> {

	/**
	 * 通过比赛Id查询所有的Action
	 * 
	 * @param matchId
	 *            Long
	 * @return List<Action>
	 */
	List<Action> queryListByMatchId(Long matchId);

	/**
	 * 通过联赛ID查询比赛
	 * 
	 * @param leagueId
	 * @return
	 */
	List<Action> queryByLeague(Long leagueId);

	/**
	 *
	 * @param params
	 * @return
	 */
	List<Action> findActions(Map<String, Object> params);

	Action queryById(Long actionId);

	void updateAction(Action action, Long playerA, Long playerB);

	void saveAction(Action action, Long player1, Long player2);

	void deleteAction(Long actionId);
	/**
	 * 通过联赛ID查询助攻数据
	 * 
	 * @param leagueId
	 * @return
	 */
	List<Action> queryHelp(Long leagueId, int status);
}
