package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.po.Admin;

public interface AdminService extends BaseService<Admin, Long> {
	/**
	 * 检查用户名是否存在
	 * 
	 * @param username
	 * @return
	 */
	boolean userNameExists(String username);

	/**
	 * 通过用户名查询管理员用户
	 * 
	 * @param username
	 * @return
	 */
	Admin findByUserName(String username);
}
