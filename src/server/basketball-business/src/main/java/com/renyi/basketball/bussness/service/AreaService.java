package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.Area;

public interface AreaService extends BaseService<Area, Long> {
	/**
	 * 查询所有省
	 * 
	 * @return
	 */
	List<Area> queryAllProvince();

	/**
	 * 根据code查询该省的所有城市
	 * 
	 * @param code
	 * @return
	 */
	List<Area> queryCities(String code);

	/**
	 * 根据code查询该城市的所有区
	 * 
	 * @param code
	 * @return
	 */
	List<Area> queryAreas(String code);
}
