package com.renyi.basketball.bussness.service;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import com.renyi.basketball.bussness.dto.ArenasDto;
import com.renyi.basketball.bussness.po.Arenas;

import java.util.List;

/**
 * Created by Roy.xiao on 2016/5/26.
 */
public interface ArenasService extends BaseService<Arenas, Long> {
	/**
	 * 查询球场详情
	 * 
	 * @param arenasId
	 * @return
	 */
	ArenasDto queryById(Long arenasId);

	/**
	 * 分页查询球场列表
	 * 
	 * @param name
	 * @param areaCode
	 * @param chooseStatus
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	Page<ArenasDto> query(String name, String areaCode, List<Integer> chooseStatus, Pageable pageable,
			Long companyId);

	/**
	 * 根据公司id获取球场列表
	 * 
	 * @param companyId
	 * @return
	 */
	List<ArenasDto> queryArenasByCompany(Long companyId);
}
