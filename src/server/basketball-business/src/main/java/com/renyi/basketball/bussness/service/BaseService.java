package com.renyi.basketball.bussness.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.BaseEntity;

import cn.magicbeans.common.Filter;
import cn.magicbeans.common.Order;

public interface BaseService<T extends BaseEntity, ID extends Serializable> {

	void save(T entity) throws BusinessException;

	void update(T entity) throws BusinessException;

	void delete(Long id) throws BusinessException;

	List<T> findAll();

	T find(ID id);

	T find(String property, Object value);

	List<T> find(Map<String, Object> params);

	/**
	 * 查找实体对象集合
	 *
	 * @param first
	 *            起始记录
	 * @param count
	 *            数量
	 * @param filters
	 *            筛选
	 * @param orders
	 *            排序
	 * @return 实体对象集合
	 */
	List<T> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);

}
