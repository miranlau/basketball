package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.dto.BillDto;
import com.renyi.basketball.bussness.po.Bill;

public interface BillService extends BaseService<Bill, Long> {
	/**
	 * 用户参加完比赛的账单列表
	 * 
	 * @param userId
	 * @param matchId
	 * @return
	 */
	BillDto queryBillList(Long userId, Long matchId, Long teamId);

	/**
	 * 修改账单
	 * 
	 * @param bill
	 * @param playerIds
	 */
	void updateBill(Bill bill, Long[] playerIds);
}
