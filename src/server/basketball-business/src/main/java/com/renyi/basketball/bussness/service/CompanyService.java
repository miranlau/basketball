package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.po.Company;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface CompanyService extends BaseService<Company, Long> {

	/**
	 * 关键字分页查询
	 * 
	 * @param keyword
	 * @param pageable
	 * @return
	 */
	Page<Company> query(String keyword, Integer province, Pageable pageable);
}
