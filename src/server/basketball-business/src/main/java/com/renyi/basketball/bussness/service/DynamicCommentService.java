package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.po.DynamicComment;
import com.renyi.basketball.bussness.po.User;

public interface DynamicCommentService extends BaseService<DynamicComment, Long> {
	/**
	 * 评论推送
	 * 
	 * @param current
	 * @param dynamicComment
	 */
	void sendPush(User current, DynamicComment dynamicComment);

	void saveDynamic(DynamicComment dynamicComment);
}
