package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.dto.DynamicDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.UserDynamic;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface DynamicService extends BaseService<UserDynamic, Long> {

	/**
	 * 获取动态列表(分页)
	 * 
	 * @param pageable
	 * @return
	 */
	Page<DynamicDto> getAllDynamicPager(Pageable pageable);

	/**
	 * 获取好友动态列表(分页)
	 * 
	 * @param pageable
	 * @param current
	 * @return
	 */
	Page<DynamicDto> friendDynamicPager(Pageable pageable, User current);

	/**
	 * 获取动态详情
	 * 
	 * @param dynamicId
	 * @return
	 */
	DynamicDto queryDynamicDetail(Long dynamicId);

	void saveDynamic(UserDynamic entity) throws BusinessException;

	void updateDynamic(UserDynamic userDynamic);

	/**
	 * 获取个人动态列表
	 * 
	 * @param userId
	 * @param pageable
	 * @return
	 */
	Page<DynamicDto> findMyDynamicList(Long userId, Pageable pageable);

	/**
	 * 删除动态
	 * 
	 * @param dynamicId
	 */
	void deleteDynamic(Long dynamicId);
}
