package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Player;

public interface ExcelInputService {
	
	/**
	 * 导入数据业务
	 * 
	 * @param playerList List<Player>
	 */
	void input(List<Player> playerList) throws BusinessException;
	
}
