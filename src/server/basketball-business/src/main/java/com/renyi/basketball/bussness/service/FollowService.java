package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.User;

public interface FollowService extends BaseService<Follow, Long> {
	/**
	 * 更新登陆用户的通讯录联系人为关注
	 * 
	 * @param original
	 * @param contactList
	 */
	void updateAddressFollow(User original, String[] contactList);
}
