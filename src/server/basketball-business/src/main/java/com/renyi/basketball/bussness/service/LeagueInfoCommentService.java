package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.dto.LeagueInfoCommetDto;
import com.renyi.basketball.bussness.po.LeagueInfoComment;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface LeagueInfoCommentService extends BaseService<LeagueInfoComment, Long> {
	/**
	 * 根据动态ID分页查询评论
	 * 
	 * @param infoId
	 * @param pageable
	 * @return
	 */
	Page<LeagueInfoCommetDto> queryCommentsByInfoId(Long infoId, Pageable pageable);

	/**
	 * 用户评论赛事动态
	 * 
	 * @param user
	 * @param content
	 */
	void addComment(User user, String content, Long Long);
}
