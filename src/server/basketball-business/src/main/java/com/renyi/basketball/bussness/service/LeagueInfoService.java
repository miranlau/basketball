package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.dto.LeagueInfoDto;
import com.renyi.basketball.bussness.po.LeagueInfo;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface LeagueInfoService extends BaseService<LeagueInfo, Long> {

	/**
	 * 查询赛事动态列表
	 * 
	 * @param leagueId
	 * @return
	 */
	Page<LeagueInfoDto> queryInfoByLeagueId(Long leagueId, Pageable pageable);

	/**
	 * 查询动态详情
	 * 
	 * @param infoId
	 * @return
	 */
	LeagueInfoDto queryInfoDetail(Long infoId);

	/**
	 * 阅读+1
	 */
	void addReadNum(Long infoId);

	/**
	 * 点赞+1
	 */
	void addLaudNum(Long infoId);

	/**
	 * 评论+1
	 */
	void addCommentNum(Long infoId);
}
