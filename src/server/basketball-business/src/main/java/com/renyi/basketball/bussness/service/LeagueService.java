package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.dto.LeagueCountCache;
import com.renyi.basketball.bussness.dto.LeagueDto;
import com.renyi.basketball.bussness.po.League;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface LeagueService extends BaseService<League, Long> {
	/**
	 * 关键字分页查询
	 * 
	 * @param keyword
	 * @param pageable
	 * @return
	 */
	Page<League> query(String keyword, List<Integer> status, Pageable pageable, Long companyId);

	/**
	 * 查询赛事详情
	 * 
	 * @param leagueId
	 * @return
	 */
	League queryById(Long leagueId);

	/**
	 * 关注赛事
	 * 
	 * @param userId
	 * @param leagueId
	 */
	void follow(Long userId, Long leagueId);

	/**
	 * 取消关注
	 * 
	 * @param userId
	 * @param leagueId
	 */
	void unfollow(Long userId, Long leagueId);
	/**
	 * 报名比赛
	 * 
	 * @param leagueId
	 *            联赛id
	 * @param userId
	 *            用户id
	 */
	void enroll(Long leagueId, Long userId);
	/**
	 * 取消报名
	 * 
	 * @param leagueId
	 *            联赛id
	 * @param userId
	 *            用户id
	 */
	void cancelEnroll(Long leagueId, Long userId);

	/**
	 * 查询关注，
	 * 
	 * @param id
	 * @param leagueId
	 * @return >=1已关注
	 */
	Integer queryfollow(Long id, Long leagueId);

	/**
	 * 我关注的赛事
	 * 
	 * @param id
	 * @return
	 */
	List<LeagueDto> queryMyFocusLeague(Long id);

	/**
	 * 获取赛事榜单数据
	 * 
	 * @param leagueId
	 * @return
	 */
	LeagueCountCache queryLeagueBang(Long leagueId);

	/**
	 * 计算赛事榜单数据
	 * 
	 * @param leagueId
	 * @return
	 */
	LeagueCountCache countLeagueBang(Long leagueId);

	/**
	 * 自动更新榜单数据
	 */
	void updateBang();
}
