package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.LeagueStage;

public interface LeagueStageService {

	List<LeagueStage> queryAll();

}
