package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.MatchRound;

public interface MatchRoundService {

	List<MatchRound> queryAll();
	
}
