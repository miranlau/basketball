package com.renyi.basketball.bussness.service;

import java.util.Date;
import java.util.List;

import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import net.sf.json.JSONArray;

public interface MatchService extends BaseService<Match, Long> {
	/**
	 * 创建匹配比赛
	 * 
	 * @param teamId
	 *            队伍id
	 * @param userId
	 *            创建者ID
	 * @param date
	 *            日期
	 * @param type
	 *            比赛类型
	 * @param areacode
	 *            地域编号
	 * @param power
	 *            战斗力范围
	 * @param age
	 *            年龄范围
	 * @param desc
	 *            描述
	 * @param longitude
	 *            经度
	 * @param latitude
	 *            维度
	 * @param addr
	 *            地点名称
	 *
	 */
	void createMatching(Long teamId, Long userId, Date date, Byte matchFormat, String areacode, String power,
			String age, String desc, Double longitude, Double latitude, String addr) throws BusinessException;

	/**
	 * 创建单队赛
	 * 
	 * @param teamId
	 *            队伍id
	 * @param date
	 *            日期
	 * @param type
	 *            比赛类型
	 */
	void createSingle(Long teamId, Date date, Byte matchFormat, Long userId) throws BusinessException;

	/**
	 * 分页查询所有比赛
	 * 
	 * @param keyword
	 *            关键字
	 * @param pageable
	 *            分页信息
	 * @param teams
	 *            当前用户所属球队
	 *
	 * @return
	 */
	Page<Match> query(Pageable pageable, final String keyword, final Long userId,
			final Double longitude, final Double latitude) throws BusinessException;

	/**
	 * 分页查询指定用户的比赛
	 * 
	 * @param userId
	 *            用户Id
	 * @param pageable
	 *            分页信息
	 * @return
	 */
	Page<Match> queryByUser(Long userId, Pageable pageable, List<Team> teams) throws BusinessException;

	/**
	 * 查询指定比赛详情
	 * 
	 * @param matchId
	 *            比赛id
	 * @return
	 * @throws BusinessException
	 */
	Match queryMatchById(Long matchId) throws BusinessException;

	/**
	 * 更新比赛信息
	 * 
	 * @param matchId
	 * @param teamId
	 * @param date
	 * @param type
	 * @param s
	 * @param areacode
	 * @param power
	 * @param age
	 * @param longitude
	 * @param latitude
	 * @param addr
	 * @throws BusinessException
	 */
	void update(Long matchId, Long teamId, Date date, Byte matchFormat, String s, String areacode, String power,
			String age, Double longitude, Double latitude, String addr) throws BusinessException;

	/**
	 * 删除比赛
	 * 
	 * @param matchId
	 *            比赛Id
	 * @throws BusinessException
	 */
	@Override
	void delete(Long matchId) throws BusinessException;

	/**
	 * 应战
	 * 
	 * @param matchId
	 *            比赛Id
	 * @param teamId
	 *            队伍id
	 */
	void gauntlet(Long matchId, Long teamId);

	/**
	 * 取消应战
	 * 
	 * @param matchId
	 *            比赛Id
	 * @param teamId
	 *            队伍id
	 */
	void cancelGauntlet(Long matchId, Long teamId);

	/**
	 * 选择应约球队
	 * 
	 * @param matchId
	 *            比赛Id
	 * @param teamId
	 *            队伍id
	 */
	void chooseTeam(Long matchId, Long teamId);

	/**
	 * 报名比赛
	 * 
	 * @param matchId
	 *            比赛id
	 * @param userId
	 *            用户id
	 * @param flag
	 *            0为主队队员，1为客队队员
	 */
	void enroll(Long matchId, Long userId, Long flag);

	/**
	 * 报名比赛.
	 * 
	 * @param matchId
	 *            - 比赛ID
	 * @param teamId
	 *            - 球队ID
	 * @param playerIds
	 *            -
	 */
	void enroll(Match match, Long teamId, Long[] playerIds);

	/**
	 * 取消报名
	 * 
	 * @param matchId
	 *            比赛id
	 * @param userId
	 *            用户id
	 * @param flag
	 *            0为主队队员，1为客队队员
	 */
	void cancel(Long matchId, Long userId, Integer flag);

	/**
	 * 查询赛事比赛列表
	 * 
	 * @param id
	 * @return
	 */
	List<Match> queryLeagueMatch(Long id) throws Exception;

	/**
	 * 自动开始, 进入赛中.
	 */
	void autoStart();

	/**
	 * 赛中状态, 更新结束.
	 */
	void autoEnd();

	/**
	 * 获取报名球员
	 * 
	 * @param teamId
	 * @param matchId
	 */
	List<Player> getPlayers(Long teamId, Long matchId);

	/**
	 * 确认首发阵容
	 * 
	 * @param matchId
	 * @param user
	 */
	void saveStarting(Long matchId, boolean isHome, String startingIds);

	/**
	 * 后台管理为赛事添加比赛
	 * 
	 * @param isSendMessage
	 *            1发送短信
	 * @param match
	 */
	void saveMatch(String isSendMessage, Match match);

	/**
	 * 查询球队比赛
	 * 
	 * @param teamId
	 * @return
	 */
	List<Match> queryMatchByTeamId(Long teamId, Integer closeNum);
	

	/**
	 * 查询球队赛事比赛
	 * 
	 * @param teamId
	 * @return
	 */
    List<Match> queryMatchByTeamIdAndLeagueId(Long teamId, Long leagueId);

	/**
	 * 根据球队查询比赛(只查询比赛表)
	 * 
	 * @param teamId
	 *            球队ID
	 * @param size
	 *            数量
	 * @return 比赛DTO
	 * @throws BusinessException
	 */
	List<Match> queryOnlyMatchByTeam(Long teamId, Integer size) throws BusinessException;

	/**
	 * 查询比赛列表
	 * 
	 * @param pageable
	 * @param teanName
	 *            球队名字
	 * @param companyId
	 *            公司ID
	 * @return
	 */
	Page<Match> queryMatchesPage(Pageable pageable, String teanName, String leagueName, String start, String end);

	/**
	 * 更新比赛信息
	 * 
	 * @param match
	 *            比赛信息
	 */
	void updateMatch(Match match);

	/**
	 * 查询我的赛事安排
	 * 
	 * @param user
	 * @return
	 */
	List<Match> queryMyMatchShedule(User user, String date);

	/**
	 * 查询有比赛的日期
	 * 
	 * @param user
	 * @return
	 */
	List<Long> queryMyMatchSheduleTime(User user);

	public JSONArray createPlayers(Long matchId);

	/**
	 * 已经完成的比赛
	 * 
	 * @param teamId
	 * @return
	 */
	List<Match> queryDoneMatches(Long teamId);

	/**
	 * 获取比赛action
	 * 
	 * @param pageable
	 * @param matchId
	 * @param status
	 *            -1全部；0主队；1客队
	 * @param action
	 * @return
	 */
	Page<Action> queryMatchData(Pageable pageable, Long matchId, Integer status, Integer action);
	
	/**
	 * 查询需要生成数据统计的比赛
	 * 
	 * @return List<Match>
	 */
	public List<Match> queryNeedStatsMatches();
	
	List<Match> queryMatchsNeedRecorder(Long recorderId);
	
}
