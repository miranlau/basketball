package com.renyi.basketball.bussness.service;

import java.util.List;
import java.util.Map;

import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.po.User;

public interface MatchStatsService {

	/**
	 * 比赛动态.
	 * 
	 * @param status
	 *            类型
	 * @param matchId
	 *            比赛ID
	 * @param teamId
	 *            球队ID
	 * @return 结果
	 */
	Map<String, Object> addAction(Long matchId, Long teamId, User recorder, Long playerId, Integer status,
			Integer quarterRemainingTime, String extension);

	/**
	 * 撤销上一个action.
	 * 
	 * @param recorder
	 *            记分员
	 * @param matchId
	 *            比赛ID
	 * @param teamId
	 *            球队ID
	 * @return 结果
	 */
	Map<String, Object> revertAction(Long matchId, Long teamId, User recorder, Integer quarterRemainingTime);

	/**
	 * 比赛中换人处理.
	 * 
	 * @param recorder
	 *            操作员
	 * @param matchId
	 *            比赛ID
	 * @param teamId
	 *            球队ID
	 * @param upPlayerId
	 *            上场队员
	 * @param downPlayerId
	 *            下场队员
	 * @return 结果
	 */
	public Map<String, Object> substituteAction(User recorder, Long matchId, Long teamId, Long[] upPlayerIds,
			Long[] downPlayerIds, Integer quarterRemainingTime) throws BusinessException;

	List<Map<String, Object>> publishMessage(Long matchId, User recorder, String message, Long playerId,
			Integer status);

	void pushMessage(User user, MatchStats matchStats, Team team, Integer status);

	/**
	 * build game
	 * 
	 * @param matchId
	 * @return game
	 */
	MatchStats build(Long matchId);

	/**
	 *
	 * @param matchId
	 * @param force
	 * @return
	 */
	MatchStats build(Long matchId, Boolean isRefresh);

	MatchStats getMatchStatsFromCache(Long matchId);

	void saveToCache(MatchStats matchStats);

	/**
	 * 清除比赛缓存
	 * 
	 * @param matchIds
	 */
	void clear(Long[] matchIds);

	/**
	 * 获取比赛的球队的技术统计
	 * 
	 * @param matchId
	 *            Long
	 * @return Map<String, Object>
	 */
	Map<String, TeamMatchStats> getTeamMatchStatsForTeam(Long matchId);

	/**
	 * 获取比赛的球员的技术统计
	 * 
	 * @param matchId
	 *            Long
	 * @return Map<String, Object>
	 */
	Map<String, List<PlayerMatchStats>> getPlayerMatchStatsForTeam(Long matchId);

}
