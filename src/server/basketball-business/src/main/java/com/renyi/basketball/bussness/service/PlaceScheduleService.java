package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.po.PlaceSchedule;

public interface PlaceScheduleService extends BaseService<PlaceSchedule, Long> {
}
