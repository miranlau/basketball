package com.renyi.basketball.bussness.service;

import java.util.Date;
import java.util.List;

import com.renyi.basketball.bussness.po.PlaceSchedule;
import com.renyi.basketball.bussness.po.Place;

public interface PlacesService extends BaseService<Place, Long> {
	/**
	 * 查询球馆的场地列表
	 * 
	 * @param arenasId
	 * @return
	 */
	List<Place> queryPlaceByArenasId(Long arenasId);

	/**
	 * 查询场地最近N天的安排
	 * 
	 * @param date
	 * @param placeId
	 * @return
	 */
	List<PlaceSchedule> querySchedule(Date date, Long placeId, Integer n);

	/**
	 * 场馆的场地安排列表
	 * 
	 * @param arenasId
	 * @return
	 */
	List<Place> queryScheduleListByArenasId(Long arenasId, Integer maxPlaceSchedule);

}
