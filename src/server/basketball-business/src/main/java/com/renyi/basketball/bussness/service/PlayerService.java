package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.Player;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface PlayerService extends BaseService<Player, Long> {

	void saveBatch(List<Player> playerList);
	
	Player queryPlayerById(Long playerId);
	
	List<Player> queryPlayerList(List<Long> playerIds, Long teamId);
	
	List<Player> queryPlayerListByTeamId(Long teamId);

	Page<Player> queryPlayerPage(Pageable pageable, String keyword, Integer type);
	
}
