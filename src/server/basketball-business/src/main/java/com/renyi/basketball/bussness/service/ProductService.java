package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.dto.ProductDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.po.Product;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface ProductService extends BaseService<Product, Long> {
	/**
	 * 分页查询商品信息
	 * 
	 * @param name
	 * @param pageable
	 * @param areansId
	 * @param companyId
	 * 			@return
	 */
	Page<ProductDto> query(String name, Pageable pageable, Long companyId, Long areansId);

	/**
	 *
	 * @param product
	 * @param areansId
	 */
	void saveProduct(Product product, Long areansId) throws BusinessException;

	void updateProduct(Product product, Long areansId) throws BusinessException;

	void saveTest(Product product) throws Exception;
	/**
	 * 查询一个
	 */
	ProductDto queryOne(Long productId, Long areansId, Long companyId);

	/**
	 * 删除
	 * 
	 * @param productId
	 * @param areansId
	 */
	void deleteOne(Long productId, Long areansId);

	/**
	 * 进出货
	 */
	void inoutproduct(InOutProduct inoutproduct);
}
