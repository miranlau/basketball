package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.dto.TeamRankingDto;

public interface RankingService {

	List<TeamRankingDto> queryTeamRanking(Long leagueId, Long leagueStageId);
	
	TeamRankingDto getRank(Long teamId, Long leagueId, Long leagueStageId);

}
