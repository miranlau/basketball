package com.renyi.basketball.bussness.service;

import com.renyi.basketball.bussness.po.ReadLaud;

public interface ReadLaudRecordService extends BaseService<ReadLaud, Long> {
	/**
	 * 增加阅读或点赞次数
	 * 
	 * @param id
	 * @param dynamicId
	 * @param numTypeRead
	 */
	void addNum(Long id, Long dynamicId, Integer numTypeRead);

	/**
	 * 判断用户是否点赞或阅读过
	 * 
	 * @param id
	 * @param dynamicId
	 * @param numTypeRead
	 * @return
	 */
	Boolean isClick(Long id, Long dynamicId, Integer numTypeRead);
}
