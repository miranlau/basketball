package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.Season;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface SeasonService extends BaseService<Season, Long> {
	Page<Season> findSeasonPage(Pageable pageable, Long leagueId, String name);
	
	Page<Season> findSeasonPage(Pageable pageable, Long leagueId);
	
	/**
	 * 根据多个id，查询球队
	 *
	 * @param userId
	 * @param teamId
	 * @return
	 */
	List<Season> queryByIds(List<Long> seasonIds);
}
