package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.PlayerSeasonStats;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.po.TeamSeasonStats;

public interface StatsService {

	/***
	 * 保存球队比赛的数据统计
	 * 
	 * @param matchId
	 *            Long
	 * @param homeMatchStats
	 *            TeamMatchStats
	 * @param visitingMatchStats
	 *            TeamMatchStats
	 */
	void saveTeamMatchStats(Long matchId, TeamMatchStats homeMatchStats, TeamMatchStats visitingMatchStats);

	/**
	 * 保存或更新球队赛季的数据统计
	 * 
	 * @param matchId
	 *            Long
	 * @param homeMatchStats
	 *            TeamMatchStats
	 * @param visitingMatchStats
	 *            TeamMatchStats
	 */
	void saveOrUpdateTeamSeasonStats(Long matchId, TeamMatchStats homeMatchStats, TeamMatchStats visitingMatchStats);

	/**
	 * 保存球员的比赛数据统计
	 * 
	 * @param matchId
	 *            Long
	 * @param playerStats
	 *            Map<String, List<PlayerMatchStats>>
	 */
	void savePlayerMatchStats(Long matchId, List<PlayerMatchStats> homeMatchStatsList, List<PlayerMatchStats> visitingMatchStatsList);

	/**
	 * 保存或更新球员赛季的数据统计
	 * 
	 * @param matchId
	 *            Long
	 * @param playerStats
	 *            Map<String, List<PlayerMatchStats>>
	 */
	void saveOrUpdatePlayerSeasonStats(Long matchId, List<PlayerMatchStats> homeMatchStatsList,
			List<PlayerMatchStats> visitingMatchStatsList);

	/**
	 * 查询球队赛季不同阶段的技术统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return TeamSeasonStats
	 */
	TeamSeasonStats getTeamSeasonStats(Long teamId, Long leagueId, Long leagueStageId);

	/**
	 * 查询球队该赛事阶段所有比赛的技术统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return TeamSeasonStats
	 */
	List<TeamMatchStats> getTeamMatchStats(Long teamId, Long leagueId, Long leagueStageId);

	/**
	 * 查询球队球员该赛事阶段所有比赛的技术统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return PlayerSeasonStats
	 */
	List<PlayerSeasonStats> getTeamPlayerSeasonStats(Long teamId, Long leagueId, Long leagueStageId);

	/**
	 * 更新球队的排名
	 * 
	 * @param matchId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 */
	void updateTeamRanking(Long matchId, Long leagueId, Long leagueStageId);
	
	/**
	 * 查询球员该赛事阶段所有比赛的技术统计
	 * 
	 * @param teamId
	 *            Long
	 * @param leagueId
	 *            Long
	 * @param leagueStageId
	 *            Long
	 * @return PlayerMatchStats
	 */
	List<PlayerMatchStats> getPlayerMatchStats(Long teamId, Long leagueId, Long leagueStageId);

}
