package com.renyi.basketball.bussness.service;

import java.util.List;
import java.util.Map;

import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface TeamService extends BaseService<Team, Long> {
	
	/**
	 * 创建队伍
	 * 
	 * @param entity
	 *            球队信息
	 * @throws Exception
	 */
	void create(Team entity) throws BusinessException;
	
	/**
	 * 根据Id查询teamDto
	 * 
	 * @param teamId
	 *            球队ID
	 * @param loginUserId
	 *            登录者ID
	 * @return
	 * @throws Exception
	 */
	Team queryTeam(Long teamId, Boolean isWithPlayers) throws BusinessException;
	
	List<Team> queryTeamByIds(List<Long> ids) throws BusinessException;

	/**
	 * 加入球队
	 * 
	 * @param userId
	 *            用户id
	 * @param teamId
	 *            球队id
	 * @throws Exception
	 */
	int join(Long userId, Long teamId) throws BusinessException;

	/**
	 * 退出球队
	 * 
	 * @param userId
	 * @param teamid
	 * @throws Exception
	 */
	int quit(Long userId, Long teamid) throws BusinessException;

	/**
	 * 将球员踢出球队.
	 * 
	 * @param teamId
	 * @param userId
	 */
	void kickout(Long userId, Long teamId) throws BusinessException;

	/**
	 * 查询出所有球队
	 * 
	 * @return
	 */
	List<Team> queryAll() throws BusinessException;

	/**
	 * 修改球队球员的球衣号码
	 * 
	 * @param userId
	 *            用户id
	 * @param teamId
	 *            球队id
	 * @param number
	 *            球衣号码
	 * @throws BusinessException
	 */
	int modifyNumber(Long userId, Long teamId, Integer number) throws BusinessException;

	/**
	 * 解散球队
	 * 
	 * @param teamId
	 *            球队id
	 */
	void dissolve(Long teamId);

	/**
	 * 搜索球队.
	 * 
	 * @param keyword
	 *            - 关键词
	 * @param isdel
	 *            - 是否删除
	 * @param pageable
	 *            - 分页
	 * @return 搜索结果
	 */
	Page<Team> query(String keyword, Boolean isdel, Pageable pageable);

	/**
	 * 通过用户ID查询球队列表.
	 * 
	 * @param userId
	 *            - 用户ID
	 * @return 球队集合
	 */
	List<Team> queryByUser(Long userId);

	/**
	 * 根据赛事id查询该赛事参与队伍
	 * 
	 * @param id
	 * @return
	 */
	List<Team> queryTeamsByLeagueId(Long id);

	/**
	 * 通过队长名来查询队伍
	 * 
	 * @param name
	 * @param isDel
	 * @param pageable
	 * @return
	 */
	Page<Team> queryByLeaderName(String name, Boolean isDel, Pageable pageable);
	/**
	 * 更新球队 赢场、平场、输场 和 等级
	 * 
	 * @param team
	 */
	void updateTeam(Team team);

	/**
	 * 关注球队
	 * 
	 * @param userId
	 * @param teamId
	 */
	void follow(Long userId, Long teamId);

	/**
	 * 取消关注
	 * 
	 * @param userId
	 * @param teamId
	 */
	void unfollow(Long userId, Long teamId);
	/**
	 * 查询关注，
	 * 
	 * @param id
	 * @param teamId
	 * @return >=1已关注
	 */
	Integer queryfollow(Long id, Long teamId);

	/**
	 * 根据球队ID与号码查询球员列表.
	 *
	 * @param teamId
	 *            - 球队ID
	 * @param num
	 *            - 号码
	 * @return 球员集合
	 */
	List<Integer> queryPlayersByNumber(Long teamId, Integer num) throws BusinessException;

	/**
	 * 更新球队球员号码.
	 *
	 * @param teamId
	 *            球队ID
	 * @param fromNum
	 *            原号码
	 * @param toNum
	 *            新号码
	 */
	void updatePlayerNum(Long teamId, int fromNum, int toNum) throws BusinessException;

	/**
	 * 获取我关注的球队
	 * 
	 * @param id
	 * @return
	 */
	List<Team> queryMyFocusTeam(Long id);

	int queryRecord(Long id, Long visiting);

	List<Team> queryByUsers(Long id);

	/**
	 * 更新球队属性
	 * 
	 * @param teamId
	 * @throws Exception
	 */
	void updateRadar(Long teamId) throws BusinessException;
	/**
	 * 查询球衣号码
	 *
	 * @param userId
	 * @param teamId
	 * @return
	 */
	int queryNumber(Long userId, Long teamId);

	/**
	 * 找出该球队的成员
	 * 
	 * @param id
	 * @return
	 */
	List<User> findPlayer(Long id, Boolean includeLeader);

	List<Map<Integer, Integer>> queryNoLeader();

	void updateNoLeader(Map<Integer, Integer> result);

	List<Team> queryStillNoLead();

	User findOnePlayer(Long teamId);

	void updateLeader(Long teamId, Long userId);

	void updateLeaders(Long userId, Long teamId);

	void updateUserTeam(Team team, Long orginLeaderId, Long[] member);

	void deleteUT(Long teamId);

	List<Team> checkTeam();
	
	Team queryByName(String name);
	
	/**
	 * 查询两只球队最近的交战史
	 * 
	 * @param teamId1
	 *            Long
	 * @param taemId2
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryMatchHistory(Long teamId1, Long teamId2, Integer start, Integer size);

	/**
	 * 查询球队未来的比赛
	 * 
	 * @param teamId
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryFutureMatches(Long teamId, Integer start, Integer size);

	/**
	 * 查询球队过去的比赛
	 * 
	 * @param teamId
	 *            Long
	 * @param start
	 *            Integer
	 * @param size
	 *            Integer
	 * @return List<Match>
	 */
	List<Match> queryPassMatches(Long teamId, Integer start, Integer size);
	
}
