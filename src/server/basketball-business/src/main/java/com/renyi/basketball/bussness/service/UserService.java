package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.dto.PerformanceDto;
import com.renyi.basketball.bussness.dto.PlayerDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.User;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface UserService extends BaseService<User, Long> {

	/**
	 * 通过用户名查询用户.
	 * 
	 * @return 用户对象
	 */
	User queryByUsername(String username);

	/**
	 * 通过手机号查询用户.
	 * 
	 * @param mobile
	 *            手机号
	 * @return 用户
	 */
	User queryByMobile(String mobile);

	/**
	 * 通过球队Id查询球员列表
	 * 
	 * @param teamId
	 *            球队id
	 * @return 球队球员信息
	 * @throws Exception
	 */
	List<PlayerDto> queryByTeam(Long teamId);

	/**
	 * 通过用户Id查询球员详细信息
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	PlayerDto queryByUser(Long userId) throws BusinessException;

	/**
	 * 查询球员数据.
	 * 
	 * @param userId
	 *            - 用户ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员
	 */
	PlayerDto queryPlayer(Long userId, Long teamId) throws BusinessException;

	/**
	 * 查询球员集合.
	 * 
	 * @param userIds
	 *            球员ID集合
	 * @param teamId
	 *            球队ID
	 * @return 球员集合
	 * @throws BusinessException
	 */
	List<PlayerDto> queryPlayerList(List<Long> userIds, Long teamId) throws BusinessException;

	/**
	 * 分页查询所有球员列表
	 * 
	 * @param pageable
	 * @param keyword
	 * @param type
	 *            0通过球员姓名 1通过球队名称(可null)
	 * @return
	 */
	Page<PlayerDto> queryAllPlayerPage(Pageable pageable, String keyword, Integer type);
	/**
	 * 更新user的总赛数、总得分、平局、等级
	 * 
	 * @param user
	 */
	void updateUser(User user);

	/**
	 * 关注球员
	 * 
	 * @param userId
	 * @param focusId
	 */
	void follow(Long userId, Long focusId);

	/**
	 * 取消关注
	 * 
	 * @param userId
	 * @param focusId
	 */
	void unfollow(Long userId, Long focusId);
	/**
	 * 查询关注，
	 * 
	 * @param id
	 * @param focusId
	 * @return >=1已关注
	 */
	Integer queryfollow(Long id, Long focusId);

	/**
	 * 获取关注我的球员
	 * 
	 * @param id
	 * @return
	 */
	List<PlayerDto> queryMyFocusPlayer(Long id);

	/**
	 * 获取我的粉丝
	 * 
	 * @param id
	 * @return
	 */
	List<PlayerDto> queryMyFans(Long id);
	List<User> queryByTeam1(Long teamId);

	/**
	 * 球员本场表现
	 * 
	 * @param matchId
	 * @param teamId
	 * @param userId
	 * @return
	 */
	PerformanceDto getPerformace(Long matchId, Long teamId, Long userId);

	/**
	 * 更新球员比赛数据
	 * 
	 * @param id
	 * @param score
	 * @param win
	 * @param lose
	 * @param help
	 * @param backboard
	 * @param two_in
	 * @param two_out
	 * @param three_in
	 * @param three_out
	 * @param Freethrow_in
	 * @param Freethrow_out
	 * @param cover
	 * @param steals
	 */
	void updatePlayer(Long id, Integer score, Integer win, Integer lose, Integer help, Integer backboard,
			Integer two_in, Integer two_out, Integer three_in, Integer three_out, Integer Freethrow_in,
			Integer Freethrow_out, Integer cover, Integer steals, Integer anError);

	/**
	 * 更新赢场
	 * 
	 * @param id
	 * @param win
	 */
	void updateWin(Long id, int win);

	/**
	 * 更新输场
	 * 
	 * @param id
	 * @param lose
	 */
	void updateLose(Long id, int lose);

}
