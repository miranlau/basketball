package com.renyi.basketball.bussness.service;

import java.util.List;

import com.renyi.basketball.bussness.po.Venue;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

public interface VenueService extends BaseService<Venue, Long> {
	Page<Venue> findVenuesPager(Pageable pageable, String name);
	
	/**
	 * 根据多个id，查询球队
	 *
	 * @param userId
	 * @param teamId
	 * @return
	 */
	List<Venue> queryByIds(List<Long> venusIds);
}
