package com.renyi.basketball.bussness.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.ActionDao;
import com.renyi.basketball.bussness.dao.MatchDao;
import com.renyi.basketball.bussness.dao.PlayerDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.service.ActionService;

@Service
public class ActionServiceImpl extends BaseServiceImpl<Action, Long> implements ActionService {
	@Resource
	private ActionDao actionDao;
	@Resource
	private TeamDao teamDao;
	@Resource
	private MatchDao matchDao;
	@Resource
	private PlayerDao playerDao;

	@Resource
	public void setBaseDao(ActionDao actionDao) {
		super.setBaseDao(actionDao);
	}

	@Override
	public List<Action> queryListByMatchId(Long matchId) {
		return actionDao.queryListByMatchId(matchId);
	}

	@Override
	public List<Action> queryByLeague(Long leagueId) {
		return actionDao.queryByLeague(leagueId);

	}

	@Override
	public List<Action> findActions(Map<String, Object> params) {

		return actionDao.findActions(params);
	}

	@Override
	public Action queryById(Long actionId) {
		return actionDao.find(actionId);
	}

	@Override
	public void updateAction(Action origin, Long playerA, Long playerB) {
		// TODO 涉及比赛分数的action需要更新其他数据
		Player pA = null;
		Player pB = null;
		try {
			pA = playerDao.find(playerA);
			pB = playerDao.find(playerB);
		} catch (BusinessException e) {
		} finally {
			// TODO 球员上场时间、球衣号码
			//origin.setPlayer(pA, pB);
		}
		Action action = new Action();
		try {
			//FIXME xfenwei
//			action.setId(origin.getId());
//			action.setTeamId(origin.getTeamId());
//			action.setMatchId(origin.getMatchId());
//			action.setPlayer(origin.getPlayerId());
//			action.setDynamicTime(origin.getDynamicTime());
//			action.setStatus(origin.getStatus());
//			action.setContent(origin.getContent());
//			action.setUpdateTime(new Date());
//			action.setSpeaks(origin.getSpeaks());

			actionDao.updateAction(action);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException();
		}
	}

	@Override
	public void saveAction(Action origin, Long player1, Long player2) {
		// TODO 涉及比赛分数的action需要更新其他数据
		Player pA = null;
		Player pB = null;
		try {
			pA = playerDao.find(player1);
			pB = playerDao.find(player2);
		} catch (BusinessException e) {
		} finally {
			// TODO 球员上场时间、球衣号码
			//FIXME xfenwei
//			origin.setPlayer(pA, pB);
		}
		Action action = new Action();
		try {
			action.setId(origin.getId());
			action.setTeamId(origin.getTeamId());
			action.setMatchId(origin.getMatchId());
			action.setRecorderId(origin.getPlayerId());
			action.setQuarterRemainingTime(origin.getQuarterRemainingTime());
			action.setStatus(origin.getStatus());
			action.setExtension(origin.getExtension());
			action.setUpdateTime(new Date());
			// 判断是否是自定义话术
			action.setSpeaks(origin.getSpeaks());
			actionDao.insertAction(action);
			countScore(action);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException();
		}
	}

	@Override
	public void deleteAction(Long actionId) {
		Action action = find(actionId);
		actionDao.delete(Long.valueOf(actionId));
		try {
			countScore(action);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException();
		}
	}

	@Override
	public List<Action> queryHelp(Long leagueId, int status) {
		List<Action> list = actionDao.queryHelp(leagueId, status);
		/*
		 * if (null == list) { return null; } List<Action> leagueDtos = new
		 * ArrayList<>(); for (Action action : list) { Action actionDto = new
		 * Action(); actionDto.setId(action.getId());
		 * actionDto.setStatus(action.getStatus().intValue());
		 * actionDto.setTeamId(action.getTeamId());
		 * actionDto.setUserId(action.getUserId());
		 * actionDto.setContent(action.getContent());
		 * actionDto.setDynamicTime(String.valueOf(action.getDynamicTime())); if
		 * (action.getStatus().intValue() != GameConstants.ACTION_11) {
		 * actionDto.setActionString(action.getAction()); }
		 * if(action.getCreateTime()!=null) {
		 * actionDto.setCreateTime(action.getCreateTime().getTime() + ""); }
		 * actionDto.setMatchId(action.getMatchId()); leagueDtos.add(actionDto);
		 * }
		 */

		return list;
	}

	/**
	 * 重新算话术分数
	 * 
	 * @param origin
	 */
	private void countScore(Action origin) throws Exception {
		List<Action> actions = queryListByMatchId(origin.getMatchId());
		Match match = matchDao.find(origin.getMatchId());
		// 涉及比分的状态
		Integer[] status = { 23, 33, 38, 39, 57, 70, 100 };
		boolean flag = false;// 是否需要修改话术
		// 主队
		Team home = teamDao.find(match.getHomeId());
		// 主队得分
		int homeScore = 0;
		int homePenalty = 0;
		// 客队
		Team visiting = null;
		// 客队得分
		int visitingScore = 0;
		int visitingPenalty = 0;
		if (match.getVisitingId() != null) {
			visiting = teamDao.find(match.getVisitingId());
		}
		String message = "";// 新话术
		for (Action action : actions) {
			for (int i = 0; i < status.length; i++) {
				if (status[i].equals(action.getStatus())) {
					flag = true;
					break;
				}
			}
//FIXME xfenwei
//			if (flag) {
//				if (action.getStatus().equals(23)) {
//					if (action.getTeamId().equals(match.getHomeId())) {
//						message = ActionSpeaker.getSpeak(action.getStatus().intValue(), home.getName(),
//								action.getPlayerA() == null ? "" : action.getPlayerA().getRealName(), homeScore + "",
//								++visitingScore + "");
//					}
//					if (action.getTeamId().equals(match.getVisitingId())) {
//						message = ActionSpeaker.getSpeak(action.getStatus().intValue(), home.getName(),
//								action.getPlayerA() == null ? "" : action.getPlayerA().getRealName(), ++homeScore + "",
//								visitingScore + "");
//					}
//					Action newAction = new Action();
//					newAction.setId(action.getId());
//					newAction.setSpeaks(message);
//					actionDao.update(newAction);
//					flag = false;
//					continue;
//				}
//				if (action.getStatus().equals(33) || action.getStatus().equals(38)
//						|| action.getStatus().equals(39)) {
//					if (action.getTeamId().equals(match.getHomeId())) {
//						message = ActionSpeaker.getSpeak(
//								action.getBflag() ? action.getStatus().intValue()
//										: action.getStatus().intValue() + 1,
//								home.getName(), action.getPlayerA() == null ? "" : action.getPlayerA().getRealName(),
//								(action.getBflag() ? ++homeScore : homeScore + (++homePenalty)) + "",
//								visitingScore + "");
//					}
//					if (action.getTeamId().equals(match.getVisitingId())) {
//						message = ActionSpeaker.getSpeak(
//								action.getBflag() ? action.getStatus().intValue()
//										: action.getStatus().intValue() + 1,
//								home.getName(), action.getPlayerA() == null ? "" : action.getPlayerA().getRealName(),
//								homeScore + "",
//								(action.getBflag() ? ++visitingScore : visitingScore + (++visitingPenalty)) + "");
//					}
//					Action newAction = new Action();
//					newAction.setSpeaks(message);
//					newAction.setId(action.getId());
//					actionDao.update(newAction);
//					flag = false;
//					continue;
//				}
//				/*
//				 * if (action.getTeamId().equals(match.getHome())) { message =
//				 * ActionSpeaker.getSpeak(action.getBflag() ?
//				 * action.getStatus().intValue() :
//				 * action.getStatus().intValue(), home.getName(), pA == null ?
//				 * "" : pA.getName(), homeScore + "", visitingScore + ""); } if
//				 * (action.getTeamId().equals(match.getVisiting())) { message =
//				 * ActionSpeaker.getSpeak(action.getBflag() ?
//				 * action.getStatus().intValue() :
//				 * action.getStatus().intValue(), visiting.getName(), pA == null
//				 * ? "" : pA.getName(), homeScore + "", visitingScore + ""); }
//				 */
//				if (action.getStatus().equals(100)) {
//					if (homeScore == visitingScore) {
//						message = ActionSpeaker.getSpeak(101);
//					} else {
//						String teamName = homeScore > visitingScore ? home.getName() : visiting.getName();
//						message = ActionSpeaker.getSpeak(100, homeScore + homePenalty + "",
//								visitingScore + visitingPenalty + "", teamName);
//					}
//
//					Action newAction = new Action();
//					newAction.setSpeaks(message);
//					newAction.setId(action.getId());
//					actionDao.update(newAction);
//					flag = false;
//					continue;
//				}
//				message = ActionSpeaker.getSpeak(action.getStatus().intValue(), homeScore + homePenalty + "",
//						visitingScore + visitingPenalty + "");
//				Action newAction = new Action();
//				newAction.setSpeaks(message);
//				newAction.setId(action.getId());
//				actionDao.update(newAction);
//				flag = false;
//			}
		}
		match.setHomeScore(homeScore);
		match.setVisitingScore(visitingScore);
		matchDao.update(match);
	}

	@Override
	protected Class<?> getClazz() {
		return ActionService.class;
	}
}
