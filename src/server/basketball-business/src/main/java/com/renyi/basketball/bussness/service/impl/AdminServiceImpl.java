package com.renyi.basketball.bussness.service.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.AdminDao;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.service.AdminService;

@Service
public class AdminServiceImpl extends BaseServiceImpl<Admin, Long> implements AdminService {
	@Resource
	private AdminDao adminDao;

	@Resource
	public void setBaseDao(AdminDao adminDao) {
		super.setBaseDao(adminDao);
	}

	@Override
	public boolean userNameExists(String username) {
		if (StringUtils.isEmpty(username)) {
			return false;
		}
		if (findByUserName(username) != null) {
			return true;
		}
		return false;
	}

	@Override
	public Admin findByUserName(String userName) {
		if (StringUtils.isEmpty(userName)) {
			return null;
		}
		return adminDao.find("user_name", userName);
	}

	@Override
	protected Class<?> getClazz() {
		return AdminService.class;
	}
}
