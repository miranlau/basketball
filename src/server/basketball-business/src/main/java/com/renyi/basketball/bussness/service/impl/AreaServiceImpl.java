package com.renyi.basketball.bussness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.AreaDao;
import com.renyi.basketball.bussness.po.Area;
import com.renyi.basketball.bussness.service.AreaService;

@Service
public class AreaServiceImpl extends BaseServiceImpl<Area, Long> implements AreaService {
	@Autowired
	private AreaDao areaDao;

	@Autowired
	public void setBaseDao(AreaDao areaDao) {
		super.setBaseDao(areaDao);
	}

	@Override
	public List<Area> queryAllProvince() {
		return areaDao.queryAllProvince();
	}

	@Override
	public List<Area> queryCities(String code) {
		return areaDao.queryCities(code);
	}

	@Override
	public List<Area> queryAreas(String code) {
		return areaDao.queryAreas(code);
	}

	@Override
	protected Class<?> getClazz() {
		return AreaService.class;
	}
}
