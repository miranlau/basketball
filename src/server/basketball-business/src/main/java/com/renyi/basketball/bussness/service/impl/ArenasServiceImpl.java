package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.ArenasDao;
import com.renyi.basketball.bussness.dto.ArenasDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Arenas;
import com.renyi.basketball.bussness.service.ArenasService;
import com.renyi.basketball.bussness.service.CompanyService;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class ArenasServiceImpl extends BaseServiceImpl<Arenas, Long> implements ArenasService {
	@Resource
	private ArenasDao arenasDao;
	@Resource
	private CompanyService companyService;
	@Resource
	public void setBaseDao(ArenasDao arenasDao) {
		super.setBaseDao(arenasDao);
	}

	@Override
	public ArenasDto queryById(Long arenasId) {
		Arenas arenas = arenasDao.find(arenasId);
		ArenasDto arenasDto = new ArenasDto();
		try {
			PropertyUtils.copyProperties(arenas, arenasDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arenasDto;
	}

	@Override
	public Page<ArenasDto> query(final String name, final String areaCode, final List<Integer> chooseStatus,
			Pageable pageable, final Long companyId) {
		Page<ArenasDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<ArenasDto>>() {
			@Override
			public List<ArenasDto> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(name)) {
					searchKeyword = "%" + name + "%";
				}
				List<Arenas> list = arenasDao.query(searchKeyword, areaCode, chooseStatus, companyId);
				if (list != null) {
					List<ArenasDto> result = new ArrayList<ArenasDto>();
					for (Arenas arenas : list) {
						ArenasDto dto = new ArenasDto();
						PropertyUtils.copyProperties(arenas, dto);
						result.add(dto);
					}
					return result;
				}
				return null;
			}
		});
		return page;
	}

	@Override
	public List<ArenasDto> queryArenasByCompany(Long companyId) {
		List<Arenas> arenases = arenasDao.queryArenasByCompany(companyId);
		List<ArenasDto> arenasDtos = new ArrayList<>();
		for (Arenas arenas : arenases) {
			ArenasDto arenasDto = new ArenasDto();
			try {
				PropertyUtils.copyProperties(arenas, arenasDto);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException("po转Dto失败");
			}
			arenasDtos.add(arenasDto);
		}
		return arenasDtos;
	}

	@Override
	protected Class<?> getClazz() {
		return ArenasService.class;
	}
}
