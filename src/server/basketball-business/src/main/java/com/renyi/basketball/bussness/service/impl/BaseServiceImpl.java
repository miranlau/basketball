package com.renyi.basketball.bussness.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.BaseEntity;
import com.renyi.basketball.bussness.service.BaseService;

import cn.magicbeans.common.Filter;
import cn.magicbeans.common.Order;
import cn.magicbeans.mybatis.dao.BaseDao;

public abstract class BaseServiceImpl<T extends BaseEntity, ID extends Serializable> implements BaseService<T, ID> {
	/** baseDao */
	private BaseDao<T, ID> baseDao;
	/** logger, all subclass should implement getClazz() */
	protected Logger logger = Logger.getLogger(getClazz());

	public void setBaseDao(BaseDao<T, ID> baseDao) {
		this.baseDao = baseDao;
	}

	@Override
	public void save(T entity) throws BusinessException {
		try {
			Date date = new Date();
			entity.setCreateTime(date);
			entity.setUpdateTime(date);
			baseDao.insert(entity);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("保存出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public void update(T entity) throws BusinessException {
		try {
			entity.setUpdateTime(new Date());
			baseDao.update(entity);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("更新出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public void delete(Long id) throws BusinessException {
		try {
			baseDao.delete(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("删除出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<T> findAll() {
		return baseDao.findAll();
	}

	@Override
	public T find(ID id) {
		return baseDao.find(id);
	}

	@Override
	public T find(String property, Object value) {
		return baseDao.find(property, value);
	}

	@Override
	public List<T> find(Map<String, Object> params) {
		return baseDao.find(params);
	}

	@Override
	public List<T> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders) {
		return baseDao.findList(first, count, filters, orders);
	}

	abstract protected Class<?> getClazz();
}
