package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.renyi.basketball.bussness.dao.BillDao;
import com.renyi.basketball.bussness.dto.BillDto;
import com.renyi.basketball.bussness.dto.BillPayDto;
import com.renyi.basketball.bussness.po.Bill;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.BillService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;

@Service
public class BillServiceImpl extends BaseServiceImpl<Bill, Long> implements BillService {
	@Resource
	private BillDao billDao;
	@Resource
	private MatchService matchService;
	@Resource
	private TeamService teamService;
	@Resource
	private UserService userService;

	Logger logger = Logger.getLogger(getClass());

	@Resource
	public void setBaseDao(BillDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public BillDto queryBillList(Long userId, Long matchId, Long teamId) {
		try {
			// 查出该球队已经比赛完的比赛
			Match match = matchService.find(matchId);
			BillDto billDto = new BillDto();
			if (match != null) {
				//billDto.setAddress(match.getAddress());
				billDto.setDate(match.getMatchTime());
				billDto.setMatchId(match.getId());
				// 找出该比赛的已有账本
				Bill bill = billDao.queryBill(teamId, match.getId());
				if (bill == null) {
					// 创建bill
					bill = new Bill();
					bill.setMatchId(match.getId());
					bill.setCreateTime(new Date());
					bill.setUpdateTime(new Date());
					bill.setTeamId(teamId);
					bill.setJson(createJsonBill(teamService.findPlayer(teamId, true), teamId));
					billDao.insert(bill);
				}
				billDto.setTotal(bill.getTotal() == null ? 0 : bill.getTotal());
				billDto.setAverge(bill.getAverge() == null ? 0 : bill.getAverge());
				billDto.setBillPayDtos(findBillPay(bill.getJson()));
				// 设置是否是队长
				Team team = teamService.find(teamId);
//				billDto.setLeaderId(team == null ? 0 : team.getLeader());
//				if (team != null && team.getLeader().equals(userId)) {
//					billDto.setLeader(true);
//				}
			}
			return billDto;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	private String createJsonBill(List<User> player, Long teamId) {
		if (player == null) {
			return "[]";
		}
		try {
			List<BillPayDto> billPay = new ArrayList<>();
			for (User user : player) {
				BillPayDto payDto = new BillPayDto();
				payDto.setUserName(user.getRealName());
				payDto.setAvatar(user.getAvatar());
				payDto.setIspay(false);
				int number = teamService.queryNumber(user.getId(), teamId);
				payDto.setNumber(number);
				payDto.setUserId(user.getId());
				billPay.add(payDto);
			}
			return JSON.toJSONString(billPay);
		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			return null;
		}
	}

	private List<BillPayDto> findBillPay(String json) {
		try {
			return JSON.parseArray(json, BillPayDto.class);
		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public void updateBill(Bill bill, Long[] playerIds) {
		try {
			Bill origin = billDao.queryBill(bill.getTeamId(), bill.getMatchId());
			if (origin != null) {

				List<BillPayDto> payDtos = JSON.parseArray(origin.getJson(), BillPayDto.class);
				for (BillPayDto payDto : payDtos) {
					payDto.setIspay(false);
					if (playerIds != null) {
						if (Arrays.asList(playerIds).contains(payDto.getUserId())) {
							payDto.setIspay(true);
						}
					}
				}
				origin.setJson(JSON.toJSONString(payDtos));

				origin.setUpdateTime(new Date());
				if (bill.getAverge() != null) {
					origin.setAverge(bill.getAverge());
				}
				if (origin.getTotal() != null) {
					origin.setTotal(bill.getTotal());
				}
				billDao.update(origin);
			} else {
				bill.setCreateTime(new Date());
				bill.setUpdateTime(new Date());
				bill.setJson(createJsonBill(teamService.findPlayer(bill.getTeamId(), true), bill.getTeamId()));
				billDao.insert(bill);
			}

		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
		}
	}

	@Override
	protected Class<?> getClazz() {
		return BillService.class;
	}
}
