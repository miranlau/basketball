package com.renyi.basketball.bussness.service.impl;

import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.CompanyDao;
import com.renyi.basketball.bussness.po.Company;
import com.renyi.basketball.bussness.service.CompanyService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class CompanyServiceImpl extends BaseServiceImpl<Company, Long> implements CompanyService {
	@Resource
	private CompanyDao companyDao;

	@Resource
	public void setBaseDao(CompanyDao companyDao) {
		super.setBaseDao(companyDao);
	}

	@Override
	public Page<Company> query(final String keyword, final Integer province, Pageable pageable) {
		Page<Company> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Company>>() {
			@Override
			public List<Company> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(keyword)) {
					searchKeyword = "%" + keyword + "%";
				}
				List<Company> companies = companyDao.queryCompanies(searchKeyword, province);
				return companies;
			}
		});
		return page;
	}

	@Override
	protected Class<?> getClazz() {
		return CompanyService.class;
	}
}
