package com.renyi.basketball.bussness.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.DynamicCommentDao;
import com.renyi.basketball.bussness.dao.DynamicDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.po.DynamicComment;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.DynamicCommentService;
import com.renyi.basketball.bussness.utils.PushMessageUtil;

@Service
public class DynamicCommentServiceImpl extends BaseServiceImpl<DynamicComment, Long> implements DynamicCommentService {
	@Resource
	private DynamicCommentDao dynamicCommentDao;
	@Resource
	private DynamicDao dynamicDao;
	@Resource
	private UserDao userDao;

	@Resource
	public void setBaseDao(DynamicCommentDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public void sendPush(User current, DynamicComment dynamicComment) {
		// 楼主
		User publisher = userDao.find(dynamicDao.find(dynamicComment.getDynamicId()).getUserId());
		User reciever = dynamicComment.getRecieverId() == null
				? publisher
				: userDao.find(dynamicComment.getRecieverId());
		String status = dynamicComment.getRecieverId() == null ? "评论" : "回复";
		if (!reciever.getId().equals(current.getId()) && !current.getId().equals(publisher.getId())) {
			// 评论者不为楼主
			String tip = current.getNickName() + status + "了您，点击查看";
			Map<String, String> extend = new HashMap<>();
			extend.put("senderId", current.getId().toString());// 发送者ID
			extend.put("senderName", current.getNickName());// 发送者昵称
			extend.put("senderHeader", current.getAvatar());// 发送者头像
			extend.put("dynamicId", dynamicComment.getDynamicId().toString());// 动态ID
			extend.put("sendTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));// 评论时间
			extend.put("content", dynamicComment.getContent());// 内容
			extend.put("pushType", PushMessageUtil.DYNAMIC_COMMENT_TYPE_);// 推送类型0--动态评论推送
			PushMessageUtil.pushMessages(reciever, dynamicComment.getContent(), extend);
		}
	}

	@Override
	public void saveDynamic(DynamicComment dynamicComment) {
		dynamicCommentDao.insertComment(dynamicComment);
	}

	@Override
	protected Class<?> getClazz() {
		return DynamicCommentService.class;
	}
}
