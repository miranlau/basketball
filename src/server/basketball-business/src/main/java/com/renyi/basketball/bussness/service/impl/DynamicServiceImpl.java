package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dao.DynamicCommentDao;
import com.renyi.basketball.bussness.dao.DynamicDao;
import com.renyi.basketball.bussness.dao.FollowDao;
import com.renyi.basketball.bussness.dao.ReadLaudRecordDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dto.DynamicCommentDto;
import com.renyi.basketball.bussness.dto.DynamicDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.UserDynamic;
import com.renyi.basketball.bussness.po.DynamicComment;
import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.DynamicService;
import com.renyi.basketball.bussness.utils.DateUtil;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class DynamicServiceImpl extends BaseServiceImpl<UserDynamic, Long> implements DynamicService {
	Logger logger = Logger.getLogger(getClass());
	@Resource
	private DynamicDao dynamicDao;
	@Resource
	private DynamicCommentDao dynamicCommentDao;
	@Resource
	private FollowDao followDao;
	@Resource
	private UserDao userDao;
	@Resource
	private ReadLaudRecordDao readLaudRecordDao;
	@Resource
	public void setBaseDao(DynamicDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public Page<DynamicDto> getAllDynamicPager(Pageable pageable) {
		Page<DynamicDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<DynamicDto>>() {
			@Override
			public List<DynamicDto> call() throws Exception {
				List<UserDynamic> list = dynamicDao.findAllDynamic();
				List<DynamicDto> dtos = new ArrayList<DynamicDto>();
				if (list != null) {
					for (UserDynamic userDynamic : list) {
						DynamicDto d = new DynamicDto();
						PropertyUtils.copyPropertiesExclude(userDynamic, d);
						d.setPubTime(DateUtil.friendTime(userDynamic.getCreateTime()));
						d.setPictures(userDynamic.getImagesList());
						dtos.add(d);
					}
				}
				return dtos;
			}
		});
		List<DynamicDto> dynamicDtos = page.getContent();
		for (DynamicDto d : dynamicDtos) {
			User user = userDao.find(d.getUserId());
			d.setUser(user);
		}
		return page;
	}

	@Override
	public Page<DynamicDto> friendDynamicPager(Pageable pageable, User current) {
		// 查询出当前用户的关注用户
		final List<Follow> friendsFollow = followDao.queryfollow(current.getId(), null, Follow.PLAYER_FOLLOW);
		// List<User> users = (List<User>)
		// followDao.queryEntity(current.getId(),null,Follow.PLAYER_FOLLOW);
		Page<DynamicDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<DynamicDto>>() {
			@Override
			public List<DynamicDto> call() throws Exception {
				List<Long> friends = new ArrayList<Long>();
				if (friendsFollow != null) {
					for (Follow f : friendsFollow) {
						friends.add(f.getLeagueId());
					}
				}
				List<UserDynamic> userDynamics = dynamicDao.queryFriendDynamic(friends);
				List<DynamicDto> result = new ArrayList<DynamicDto>();
				if (userDynamics != null) {
					for (UserDynamic userDynamic : userDynamics) {
						DynamicDto d = new DynamicDto();
						PropertyUtils.copyPropertiesExclude(userDynamic, d);
						d.setPubTime(DateUtil.friendTime(userDynamic.getCreateTime()));
						d.setPictures(userDynamic.getImagesList());
						result.add(d);
					}
				}
				return result;
			}
		});
		List<DynamicDto> dynamicDtos = page.getContent();
		for (DynamicDto d : dynamicDtos) {
			User user = userDao.find(d.getUserId());
			d.setUser(user);
		}
		return page;
	}

	@Override
	public DynamicDto queryDynamicDetail(Long dynamicId) {
		DynamicDto dynamicDto = new DynamicDto();
		UserDynamic userDynamic = dynamicDao.find(dynamicId);
		if (userDynamic != null) {
			try {
				PropertyUtils.copyPropertiesExclude(userDynamic, dynamicDto);
				dynamicDto.setPubTime(DateUtil.friendTime(userDynamic.getCreateTime()));
				dynamicDto.setPictures(userDynamic.getImagesList());
				// 查询评论
				Map<String, Object> params = new HashMap<>();
				params.put("dynamicid", dynamicId);
				List<DynamicComment> comments = dynamicCommentDao.find(params);
				List<DynamicCommentDto> commentDtos = new ArrayList<>();
				for (DynamicComment comment : comments) {
					DynamicCommentDto dto = new DynamicCommentDto();
					PropertyUtils.copyPropertiesExclude(comment, dto);
					dto.setPubTime(DateUtil.friendTime(comment.getCreateTime()));
					User sender = userDao.find(comment.getSenderId());
					dto.setSender(sender);
					commentDtos.add(dto);
				}
				dynamicDto.setCommentDtos(commentDtos);
				User user = userDao.find(dynamicDto.getUserId());
				dynamicDto.setUser(user);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return dynamicDto;
	}
	@Override
	public void saveDynamic(UserDynamic entity) throws BusinessException {
		dynamicDao.insertDynamic(entity);
	}

	@Override
	public void updateDynamic(UserDynamic userDynamic) {
		dynamicDao.updateDynamic(userDynamic);
	}

	@Override
	public Page<DynamicDto> findMyDynamicList(final Long userId, Pageable pageable) {
		Page<DynamicDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<DynamicDto>>() {
			@Override
			public List<DynamicDto> call() throws Exception {
				// Map<String,Object> params = new HashMap<String, Object>();
				// params.put("userId",userId);
				List<UserDynamic> userDynamics = dynamicDao.findMyDynamicList(userId);
				List<DynamicDto> result = new ArrayList<DynamicDto>();
				if (userDynamics != null) {
					for (UserDynamic userDynamic : userDynamics) {
						DynamicDto d = new DynamicDto();
						PropertyUtils.copyPropertiesExclude(userDynamic, d);
						d.setPubTime(DateUtil.friendTime(userDynamic.getCreateTime()));
						d.setPictures(userDynamic.getImagesList());
						result.add(d);
					}
				}
				return result;
			}
		});
		List<DynamicDto> dynamicDtos = page.getContent();
		for (DynamicDto d : dynamicDtos) {
			User user = userDao.find(d.getUserId());
			d.setUser(user);
		}
		return page;
	}

	@Override
	public void deleteDynamic(Long dynamicId) {
		if (dynamicId != null) {
			try {
				// 删除评论
				dynamicCommentDao.deleteByDynamicId(dynamicId);
				// 删除数量
				readLaudRecordDao.deleteByDynamicId(dynamicId);
				// 删除动态
				dynamicDao.deleteById(dynamicId);
			} catch (BusinessException e) {
				logger.error(e.getMessage(), e);
				throw new BusinessException("删除动态异常", e, ErrorCode.DB_EXCEPTION.code());
			}
		}
	}

	@Override
	protected Class<?> getClazz() {
		return DynamicService.class;
	}
}
