package com.renyi.basketball.bussness.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magicbeans.huanxin.HuanXinUtil;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.dao.PlayerDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dto.ExcelDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.ExcelInputService;
import com.renyi.basketball.bussness.service.PlayerService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.utils.ChineseToEnglish;

@Service
public class ExcelInputServiceImpl implements ExcelInputService {

	private Logger logger = Logger.getLogger(getClass());
	@Resource
	private PlayerDao playerDao;
	@Resource
	private TeamDao teamDao;
	@Resource
	private PlayerService playerService;
	@Resource
	private TeamService teamService;

	@Override
	@Transactional
	public void input(List<Player> playerList) throws BusinessException {
//		int i = 0;
//		for (ExcelDto excelDto : excelDtos) {
//			User user = excelDto.getUser();
//			Team team = excelDto.getTeam();
//			Team tempTeam = null;
//
//			logger.info("导入第" + i + "个球员:" + user);
//			if (teamDao.hasExist(team)) {
//				List<Team> teams = teamDao.queryTeams(team.getName(), false);
//				tempTeam = teams.get(0);
//				user = saveUser(user, team);
//
//				teamService.join(user.getId(), tempTeam.getId());
//				if (user.getUniformNumber() != null) {
//					teamService.modifyNumber(user.getId(), tempTeam.getId(), user.getUniformNumber());
//				}

				// User leader = userDao.find(tempTeam.getLeader());
				// if (tempTeam.getGroupId() == null ||
				// tempTeam.getGroupId().equals("")) {
				// String groupId = HuanXinUtil.createGroup(team.getName(),
				// team.getName(), false, null, false,
				// leader.getUserName(), null);
				// tempTeam.setGroupId(groupId);
				// teamService.update(tempTeam);
				// }
//			} else {
////				team.setPassword(team.getName());
//				team.setCreateTime(new Date());
//				user = saveUser(user, team);
//				String groupId = HuanXinUtil.createGroup(team.getName(), team.getName(), false, null, false,
//						user.getUserName(), null);
////				team.setGroupId(groupId);
//				// team.setLeader(user.getId());
//				teamService.save(team);
//				tempTeam = team;
//				teamService.join(user.getId(), tempTeam.getId());
////				if (user.getUniformNumber() != null) {
////					teamService.modifyNumber(user.getId(), tempTeam.getId(), user.getUniformNumber());
////				}
//			}
//			if (HuanXinUtil.existUser(user.getUserName())) {
////				HuanXinUtil.addBatchuserToGroup(tempTeam.getGroupId(), new String[] { user.getUserName() });
//			}
//
//			logger.info("导入第" + i + "个球员导入结束:" + user);
//			i++;
//
//		}
	}

	private User saveUser(User user, Team team) throws BusinessException {
//		// 如果电话号码存在且相同为同一个人，跟新信息
//		user.setPassword(DigestUtils.md5Hex(PASSWORD + BasketballConstants.PASSWORD_MD5_PRIFX));
//		user.setPassword(PASSWORD);
//		// FIXME xfenwei 删除了Radar
//		//user.setRadar(RADAR);
//
//		boolean isExits = false;
//		if (StringUtils.isNotEmpty(user.getMobile()) && userDao.mobileHasExist(user)) {
//			User olduser = userDao.find("mobile", user.getMobile());
//			user.setId(olduser.getId());
//			user.setUserName(olduser.getUserName());
//			// userService.update(user);
//
//			isExits = true;
//
//			// return user;
////		} else if (StringUtils.isNotEmpty(user.getIdentity()) && user.getIdentity().length() > 6
////				&& userDao.idCardHasExist(user)) {
////			// 如果身份证十一位存在且相同为同一个人，更新信息
////
////			Long id = userDao.queryByIdcard(user);
////			user.setId(id);
////			User olduser = userDao.find(id);
////			user.setUserName(olduser.getUserName());
////			if (("").equals(user.getMobile())) {
////				user.setMobile(null);
////			}
////			isExits = true;
//			// userService.update(user);
//			// Boolean exist = HuanXinUtil.existUser(user.getUserName());
//			// if (!exist && user.getUserName() != null && user.getPassword() !=
//			// null) {
//			// HuanXinUtil.addUser(user.getUserName(),user.getUserName(),user.getNickName());
//			// }
//			// return user;
//		} else if (userDao.hasSameTeam(user, team)) {
//			// 如果同队同名为同一个人，更新信息
//			// user.setId(userDao.queryByTeam(user,team));
//			User orginal = userDao.find(userDao.queryByTeam(user, team));
//			user.setId(orginal.getId());
//			isExits = true;
//			// user.setUserName(orginal.getUserName());
//			if ("".equals(user.getMobile())) {
//				user.setMobile(null);
//			}
//			// userService.update(user);
//			// Boolean exist = HuanXinUtil.existUser(user.getUserName());
//			// if (!exist && user.getUserName() != null && user.getPassword() !=
//			// null) {
//			// HuanXinUtil.addUser(user.getUserName(),user.getUserName(),user.getNickName());
//			// }
//			// return user;
//		} else if (StringUtils.isNotEmpty(user.getMobile())) {
//			// 不是同一个人且电话号码不存在
//
//			user.setUserName(user.getMobile());
//			// userService.save(user);
//			// Boolean exist = HuanXinUtil.existUser(user.getUserName());
//			// if (!exist && user.getUserName() != null && user.getPassword() !=
//			// null) {
//			// HuanXinUtil.addUser(user.getUserName(),user.getUserName(),user.getNickName());
//			// }
//			// return user;
////		} else if (user.getIdentity() != null) {
////			// 不是同一个人且身份证存在
////
////			String username = "";
////			try {
////				username = ChineseToEnglish.getPinYinHeadChar(user.getNickName() + user.getIdentity().substring(0, 11));
////			} catch (Exception e) {
////				username = ChineseToEnglish.getPinYinHeadChar(
////						user.getNickName() + user.getIdentity().substring(0, user.getIdentity().length()));
////			}
////
////			user.setUserName(username);
////			String number = userDao.getMaxMobile();
////			if (number == null) {
////				user.setMobile("12700000000");
////			} else {
////				user.setMobile(Long.valueOf(number) + 1 + "");
////			}
//
//			// userService.save(user);
//			// Boolean exist = HuanXinUtil.existUser(user.getUserName());
//			// if (!exist && user.getUserName() != null && user.getPassword() !=
//			// null) {
//			// HuanXinUtil.addUser(user.getUserName(),user.getUserName(),user.getNickName());
//			// }
//			// return user;
//		} else {
//			// 不是同一个人且都不存在
//			String number = userDao.getMaxMobile();
//			if (number == null) {
//				user.setMobile("12700000000");
//				user.setUserName(Long.valueOf(number) + 1 + "");
//			} else {
//				user.setMobile(Long.valueOf(number) + 1 + "");
//				user.setUserName(Long.valueOf(number) + 1 + "");
//			}
//			// Boolean exist = HuanXinUtil.existUser(user.getUserName());
//			// if (!exist && user.getUserName() != null && user.getPassword() !=
//			// null) {
//			// HuanXinUtil.addUser(user.getUserName(),user.getUserName(),user.getNickName());
//			// }
//			// userService.save(user);
//		}
//
//		if (isExits) {
//			userService.update(user);
//		} else {
//			userService.save(user);
//		}
//
//		Boolean exist = HuanXinUtil.existUser(user.getUserName());
//		if (!exist && user.getUserName() != null && user.getPassword() != null) {
//			HuanXinUtil.addUser(user.getUserName(), user.getUserName(), user.getNickName());
//		}

		return null;
	}
}
