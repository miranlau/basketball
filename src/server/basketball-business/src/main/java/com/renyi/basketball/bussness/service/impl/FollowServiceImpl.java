package com.renyi.basketball.bussness.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.FollowDao;
import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.FollowService;
import com.renyi.basketball.bussness.service.UserService;

@Service
public class FollowServiceImpl extends BaseServiceImpl<Follow, Long> implements FollowService {
	@Resource
	private FollowDao followDao;
	@Resource
	private UserService userService;
	@Resource
	public void setBaseDao(FollowDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public void updateAddressFollow(User original, String[] contactList) {
		// 查出联系人中注册的用户
		for (String add : contactList) {
			User user = userService.queryByMobile(add);
			if (user != null) {
				List<Follow> result = followDao.queryfollow(original.getId(), user.getId(), Follow.PLAYER_FOLLOW);
				if (result == null || result.size() <= 0) {
					followDao.insertFollow(original.getId(), user.getId(), Follow.PLAYER_FOLLOW);
				}
			}
		}
	}

	@Override
	protected Class<?> getClazz() {
		return FollowService.class;
	}
}
