package com.renyi.basketball.bussness.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.InOutProductDao;
import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.service.InOutProductService;

@Service
public class InOutProductServiceImpl extends BaseServiceImpl<InOutProduct, Long> implements InOutProductService {

	@Resource
	public void setBaseDao(InOutProductDao inOutProductDao) {
		super.setBaseDao(inOutProductDao);
	}
	@Resource
	private InOutProductDao inOutProductDao;

	@Override
	protected Class<?> getClazz() {
		return InOutProductService.class;
	}

}
