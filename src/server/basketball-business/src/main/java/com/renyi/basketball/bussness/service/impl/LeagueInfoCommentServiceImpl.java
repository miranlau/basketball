package com.renyi.basketball.bussness.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.LeagueInfoCommentDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dto.LeagueInfoCommetDto;
import com.renyi.basketball.bussness.po.LeagueInfoComment;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.LeagueInfoCommentService;
import com.renyi.basketball.bussness.utils.DateUtil;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class LeagueInfoCommentServiceImpl extends BaseServiceImpl<LeagueInfoComment, Long>
		implements
			LeagueInfoCommentService {
	@Resource
	private LeagueInfoCommentDao leagueInfoCommentDao;
	@Resource
	private UserDao userDao;
	Logger logger = Logger.getLogger(getClass());
	@Resource
	public void setBaseDao(LeagueInfoCommentDao leagueInfoCommentDao) {
		super.setBaseDao(leagueInfoCommentDao);
	}

	@Override
	public Page<LeagueInfoCommetDto> queryCommentsByInfoId(final Long infoId, Pageable pageable) {
		Page<LeagueInfoCommetDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<LeagueInfoCommetDto>>() {
			@Override
			public List<LeagueInfoCommetDto> call() throws Exception {
				if (infoId == null) {
					return null;
				}
				Map<String, Object> params = new HashMap<>();
				params.put("league_info_id", infoId);
				List<LeagueInfoComment> comments = leagueInfoCommentDao.find(params);
				List<LeagueInfoCommetDto> comDto = null;
				if (comments != null) {
					comDto = new ArrayList<>();
					for (LeagueInfoComment comment : comments) {
						LeagueInfoCommetDto dto = new LeagueInfoCommetDto();
						try {
							PropertyUtils.copyPropertiesExclude(comment, dto, new String[]{"reciever", "sender"});
							dto.setPubTime(DateUtil.friendTime(comment.getCreateTime()));
							dto.setSender(userDao.find(comment.getSender()));
							comDto.add(dto);
						} catch (Exception e) {
							logger.error(e.getMessage(), e);
						}
					}
				}
				return comDto;
			}
		});
		return page;
	}

	@Override
	public void addComment(User user, String content, Long infoId) {
		try {
			LeagueInfoComment comment = new LeagueInfoComment();
			comment.setSender(user.getId());
			comment.setContent(content);
			comment.setId(infoId);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			comment.setCreateTime(new Date());
			comment.setUpdateTime(new Date());
			comment.setStatus(0);
			leagueInfoCommentDao.insert(comment);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	protected Class<?> getClazz() {
		return LeagueInfoCommentService.class;
	}
}
