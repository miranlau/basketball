package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.dao.LeagueInfoCommentDao;
import com.renyi.basketball.bussness.dao.LeagueInfoDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dto.LeagueInfoDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.LeagueInfo;
import com.renyi.basketball.bussness.service.LeagueInfoService;
import com.renyi.basketball.bussness.utils.DateUtil;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class LeagueInfoServiceImpl extends BaseServiceImpl<LeagueInfo, Long> implements LeagueInfoService {
	@Resource
	private LeagueInfoDao leagueInfoDao;
	@Resource
	private LeagueInfoCommentDao leagueInfoCommentDao;
	@Resource
	private UserDao userDao;
	@Resource
	public void setBaseDao(LeagueInfoDao leagueInfoDao) {
		super.setBaseDao(leagueInfoDao);
	}
	Logger logger = Logger.getLogger(getClass());
	@Override
	public Page<LeagueInfoDto> queryInfoByLeagueId(final Long leagueId, Pageable pageable) {
		Page<LeagueInfoDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<LeagueInfoDto>>() {
			@Override
			public List<LeagueInfoDto> call() throws Exception {
				if (leagueId == null) {
					return null;
				}
				List<LeagueInfo> infos = leagueInfoDao.queryInfoByLeagueId(leagueId);
				List<LeagueInfoDto> dtos = null;
				if (infos != null) {
					dtos = new ArrayList<LeagueInfoDto>();
					for (LeagueInfo info : infos) {
						LeagueInfoDto leagueInfoDto = new LeagueInfoDto();
						PropertyUtils.copyProperties(info, leagueInfoDto);
						leagueInfoDto.setPubTime(DateUtil.friendTime(info.getCreateTime()));
						leagueInfoDto.setImgs(info.getImgUrls());
						dtos.add(leagueInfoDto);
					}
				}
				return dtos;
			}
		});
		return page;
	}

	@Override
	public LeagueInfoDto queryInfoDetail(Long infoId) throws BusinessException {
		// 查询该动态的评论
		// Map<String,Object> params = new HashMap<>();
		// params.put("league_info_id",infoId);
		// List<LeagueInfoComment> comments = leagueInfoCommentDao.find(params);
		// List<LeagueInfoCommetDto> comDto = null;
		// if (comments != null) {
		// comDto = new ArrayList<>();
		// for (LeagueInfoComment comment: comments ) {
		// LeagueInfoCommetDto dto = new LeagueInfoCommetDto();
		// try {
		// PropertyUtils.copyPropertiesExclude(comment,dto,new
		// String[]{"reciever","sender"});
		// dto.setPubTime(DateUtil.friendTime(comment.getCreateTime()));
		// dto.setSender(userDao.find(comment.getSender()));
		// comDto.add(dto);
		// } catch (Exception e) {
		// logger.error(e.getMessage(),e);
		// }
		// }
		// }
		// 查询动态
		LeagueInfo info = leagueInfoDao.find(infoId);
		LeagueInfoDto infoDto = new LeagueInfoDto();
		try {
			PropertyUtils.copyProperties(info, infoDto);
			infoDto.setPubTime(DateUtil.friendTime(info.getCreateTime()));
			infoDto.setImgs(info.getImgUrls());
			// infoDto.setComments(comDto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return infoDto;
	}

	@Override
	public void addReadNum(Long infoId) {
		LeagueInfo leagueInfo = leagueInfoDao.find(infoId);
		if (leagueInfo != null) {
			Long num = leagueInfo.getReadNum() + 1l;
			try {
				leagueInfoDao.addNum(num, infoId, BasketballConstants.READ_NUM);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Override
	public void addLaudNum(Long infoId) {
		LeagueInfo leagueInfo = leagueInfoDao.find(infoId);
		if (leagueInfo != null) {
			try {
				leagueInfoDao.addNum(leagueInfo.getLaudNum() + 1, infoId, BasketballConstants.LAUD_NUM);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Override
	public void addCommentNum(Long infoId) {
		LeagueInfo leagueInfo = leagueInfoDao.find(infoId);
		if (leagueInfo != null) {
			try {
				leagueInfoDao.addNum(leagueInfo.getCommentNum() + 1, infoId, BasketballConstants.COMMENT_NUM);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@Override
	protected Class<?> getClazz() {
		return LeagueInfoService.class;
	}
}
