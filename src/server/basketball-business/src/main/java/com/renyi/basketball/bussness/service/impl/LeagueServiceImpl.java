package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.common.IdAnalyzer;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dao.ActionDao;
import com.renyi.basketball.bussness.dao.CompanyDao;
import com.renyi.basketball.bussness.dao.FollowDao;
import com.renyi.basketball.bussness.dao.LeagueDao;
import com.renyi.basketball.bussness.dao.MatchDao;
import com.renyi.basketball.bussness.dao.SeasonDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dto.BackboardDto;
import com.renyi.basketball.bussness.dto.CoverDto;
import com.renyi.basketball.bussness.dto.HelpScoreDto;
import com.renyi.basketball.bussness.dto.LeagueCountCache;
import com.renyi.basketball.bussness.dto.LeagueDto;
import com.renyi.basketball.bussness.dto.PensoerScoreList;
import com.renyi.basketball.bussness.dto.ScoresList;
import com.renyi.basketball.bussness.dto.StealsDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.ActionService;
import com.renyi.basketball.bussness.service.LeagueService;
import com.renyi.basketball.bussness.utils.CachedManager;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class LeagueServiceImpl extends BaseServiceImpl<League, Long> implements LeagueService {
	@Resource
	private LeagueDao leagueDao;
	@Resource
	private TeamDao teamDao;
	@Resource
	private CompanyDao companyDao;
	@Resource
	private FollowDao followDao;
	@Resource
	private MatchDao matchDao;
	@Resource
	private ActionDao actionDao;
	@Resource
	private UserDao userDao;
	@Resource
	private SeasonDao seasonDao;
	@Resource
	private ActionService actionService;
	private Logger logger = Logger.getLogger(getClass());

	@Resource
	public void setBaseDao(LeagueDao leagueDao) {
		super.setBaseDao(leagueDao);
	}

	@Override
	public Page<League> query(final String keyword, final List<Integer> status, Pageable pageable, final Long companyId) {
		Page<League> page = PaginationUtil.pagedQuery(pageable, new Callable<List<League>>() {
			@Override
			public List<League> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(keyword)) {
					searchKeyword = "%" + keyword + "%";
				}
				List<League> leagues = leagueDao.queryLeagues(searchKeyword, status, companyId);
//				if (CollectionUtils.isNotEmpty(leagues)) {
//					for (League league : leagues) {
//						league.setCompany(companyDao.find(league.getCompanyId()));
//						league.setTeamList(teamDao.queryByIds(IdAnalyzer.toList(league.getTeams())));
//					}
//				}
				
				return leagues;
			}
		});
		return page;
	}

	@Override
	public League queryById(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		League league = leagueDao.find(leagueId);
		league.setTeamList(teamDao.queryByIds(IdAnalyzer.toList(league.getTeams())));
		league.setSeasonList(seasonDao.queryByLeague(league.getId()));

		return league;
	}

	@Override
	public void follow(Long userId, Long leagueId) {
		followDao.insertFollow(userId, leagueId, Follow.LEAGUE_FOLLOW);
	}

	@Override
	public void unfollow(Long userId, Long leagueId) {
		followDao.deletefollow(userId, leagueId, Follow.LEAGUE_FOLLOW);
	}

	@Override
	public void enroll(Long leagueId, Long teamId) {
		League oldLeague = leagueDao.find(leagueId);
		String teams = oldLeague.getTeams();
		if (!StringUtils.isEmpty(teams)) {
			teams = teams + "," + teamId;
		} else {
			teams = teamId + "";
		}
		League league = new League();
		league.setId(leagueId);
		league.setTeams(teams);
		leagueDao.updateLeague(league);
	}

	@Override
	public void cancelEnroll(Long leagueId, Long teamId) {
		League oldLeague = leagueDao.find(leagueId);
		String teams = oldLeague.getTeams();
		List<Long> list = oldLeague.getTeamIds();
		List<Long> updateList = new ArrayList<>();
		if (list != null) {
			for (Long id : list) {
				if (!teamId.equals(id)) {
					updateList.add(id);
				}
			}
			League league = new League();
			league.setId(leagueId);
			league.setTeamIds(updateList);
			leagueDao.updateLeague(league);
		}
	}

	@Override
	public Integer queryfollow(Long userId, Long leagueId) {
		List<Follow> list = followDao.queryfollow(userId, leagueId, Follow.LEAGUE_FOLLOW);
		if (list != null) {
			return list.size();
		} else {
			return 0;
		}
	}

	@Override
	public List<LeagueDto> queryMyFocusLeague(Long id) {
		try {
			List<League> leagues = (List<League>) followDao.queryEntity(id, null, Follow.LEAGUE_FOLLOW);
			List<LeagueDto> leagueDtos = null;
			if (leagues != null) {
				leagueDtos = new ArrayList<>();
				for (League league : leagues) {
					LeagueDto leagueDto = new LeagueDto();
					PropertyUtils.copyPropertiesExclude(league, leagueDto, new String[] { "teams" });
					leagueDtos.add(leagueDto);
				}
			}
			return leagueDtos;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询我关注的赛事异常", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public LeagueCountCache queryLeagueBang(Long leagueId) {

		try {
			LeagueCountCache leagueCountCache = (LeagueCountCache) CachedManager.get(BasketballConstants.CACHE_LEAGUE_NAME,
					BasketballConstants.CACHE_LEAGUE_BANG_PREFIX + leagueId);
			if (leagueCountCache != null) {
				return leagueCountCache;
			} else {
				// 重新计算
				leagueCountCache = countLeagueBang(leagueId);
				CachedManager.put(BasketballConstants.CACHE_LEAGUE_NAME, BasketballConstants.CACHE_LEAGUE_BANG_PREFIX + leagueId, leagueCountCache);
				return leagueCountCache;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public void update(League entity) throws BusinessException {
		leagueDao.updateLeague(entity);
	}

	@Override
	public LeagueCountCache countLeagueBang(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		League league = leagueDao.find(leagueId);
		if (league == null) {
			return null;
		}
		LeagueCountCache leagueCountCache = new LeagueCountCache();
		try {
			leagueCountCache.setId(leagueId);
			leagueCountCache.setScoresList(getScoreList(leagueId));
			leagueCountCache.setShooterLists(getPensoerScoreList(leagueId));
			leagueCountCache.setHelpScoreList(getRedYellowPai(leagueId));
			leagueCountCache.setBackboardList(getAttack(leagueId));
			leagueCountCache.setStealsList(getSaves(leagueId));
			leagueCountCache.setCoverList(getcoverList(leagueId));
			return leagueCountCache;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public void updateBang() {
		try {
			// 查出所有赛事
			List<Long> leagueIds = leagueDao.queryAllIds();
			if (leagueIds != null) {
				for (Long leagueId : leagueIds) {
					// 清除缓存中的数据
					CachedManager.remove(BasketballConstants.CACHE_LEAGUE_NAME, BasketballConstants.CACHE_LEAGUE_BANG_PREFIX + leagueId);
					// 重新查询
					// queryLeagueBang(leagueId);
					LeagueCountCache leagueCountCache = countLeagueBang(leagueId);
					CachedManager.put(BasketballConstants.CACHE_LEAGUE_NAME, BasketballConstants.CACHE_LEAGUE_BANG_PREFIX + leagueId,
							leagueCountCache);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	// 助攻榜
	private Set<HelpScoreDto> getRedYellowPai(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		try {
			// 查出所有比赛
			List<Match> matches = matchDao.queryLeagueMatch(leagueId);
			// 统计所有队伍的比赛场次
			Map<Long, Integer> count = countMatch(matches);

			/*
			 * List<Integer> matchIds = new ArrayList<>(); for (Match match :
			 * matches) { matchIds.add(match.getId()); }
			 */
			// 添加助攻信息
			List<Action> actions = actionService.queryHelp(leagueId, ActionStatus.ASSIST);
			Map<Long, Object> map = new HashMap<>();
			if (actions != null && actions.size() > 0) {
				for (Action action : actions) {
					if (action.getExtension() == null || "".equals(action.getExtension())) {
						continue;
					}
					Team team = teamDao.find(action.getTeamId());
					// action转shooter对象
					HelpScoreDto helpScoreDto = new HelpScoreDto();
					helpScoreDto.setTeamName(team == null ? "无" : team.getName());
					helpScoreDto.getShooter(action.getExtension());
					if (null != helpScoreDto.getPlayerId()) {

						if (map.containsKey(helpScoreDto.getPlayerId())) {
							HelpScoreDto origin = (HelpScoreDto) map.get(helpScoreDto.getPlayerId());
							origin.helpCount();
							origin.setCount(count.get(action.getTeamId()));
							origin.avgHelp();
							map.put(helpScoreDto.getPlayerId(), origin);
						} else {

							User user = userDao.find(helpScoreDto.getPlayerId());
							if (user != null) {
								if (user.getRealName() != null) {
									helpScoreDto.setName(user.getRealName());
								} else {
									helpScoreDto.setName(user.getNickName());
								}
							} else {
								helpScoreDto.setName("无名");
							}

							if (helpScoreDto.getPlayerId() != null && !"".equals(helpScoreDto.getPlayerId())) {
								helpScoreDto.helpCount();
								helpScoreDto.setCount(count.get(action.getTeamId()));
								helpScoreDto.avgHelp();
								map.put(helpScoreDto.getPlayerId(), helpScoreDto);
							}
						}
					}
				}
			}

			Set<HelpScoreDto> newList = new TreeSet<HelpScoreDto>();
			for (Map.Entry<Long, Object> foul : map.entrySet()) {
				newList.add((HelpScoreDto) foul.getValue());
			}
			return newList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	// 个人得分榜
	private Set<PensoerScoreList> getPensoerScoreList(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		try {
			// 查出所有比赛
			List<Match> matches = matchDao.queryLeagueMatch(leagueId);
			// 统计所有队伍的比赛场次
			Map<Long, Integer> count = countMatch(matches);
			List<Long> matchIds = new ArrayList<>();
			for (Match match : matches) {
				matchIds.add(match.getId());
			}
			Map<Long, Object> map = new HashMap<>();
			List<Action> result = actionDao.queryShooterInLeague(matchIds);// 该联赛所有进球的action
			if (result != null && result.size() > 0) {
				for (Action action : result) {
					if (action.getExtension() == null || "".equals(action.getExtension())) {
						continue;
					}
					Team team = teamDao.find(action.getTeamId());
					// action转shooter对象
					PensoerScoreList shooterList = new PensoerScoreList();
					shooterList.setTeamName(team == null ? "无" : team.getName());
					shooterList.getShooter(action.getExtension());
					if (null != shooterList.getPlayerId()) {

						if (map.containsKey(shooterList.getPlayerId())) {
							PensoerScoreList origin = (PensoerScoreList) map.get(shooterList.getPlayerId());
							if (action.getStatus() == ActionStatus.TWO_LEFTSIDE_STANDING_IN) {
								origin.countIn(2);
							} else if (action.getStatus() == ActionStatus.THREE_LEFT_WING_STANDING_IN) {
								origin.countIn(3);
							} else if (action.getStatus() == ActionStatus.FREE_THROW_IN) {
								origin.countIn(1);
							}
							origin.setCount(count.get(action.getTeamId()));
							origin.avgScore();
							map.put(shooterList.getPlayerId(), origin);
						} else {

							User user = userDao.find(shooterList.getPlayerId());
							if (user != null) {
								if (user.getRealName() != null) {
									shooterList.setName(user.getRealName());
								} else {
									shooterList.setName(user.getNickName());
								}
							} else {
								shooterList.setName("无名");
							}

							if (shooterList.getPlayerId() != null && !"".equals(shooterList.getPlayerId())) {
								if (action.getStatus() == ActionStatus.TWO_LEFTSIDE_STANDING_IN) {
									shooterList.countIn(2);
								} else if (action.getStatus() == ActionStatus.THREE_LEFT_WING_STANDING_IN) {
									shooterList.countIn(3);
								} else if (action.getStatus() == ActionStatus.FREE_THROW_IN) {
									shooterList.countIn(1);
								}
								shooterList.setCount(count.get(action.getTeamId()));
								shooterList.avgScore();
								map.put(shooterList.getPlayerId(), shooterList);
							}
						}
					}
				}
			}

			Set<PensoerScoreList> newList = new TreeSet<PensoerScoreList>();
			Set<Map.Entry<Long, Object>> list = map.entrySet();
			for (Map.Entry<Long, Object> integerObjectEntry : list) {
				PensoerScoreList shooter = (PensoerScoreList) integerObjectEntry.getValue();

				newList.add(shooter);
			}
			return newList;

		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			return null;
		}
	}

	// 抢断榜
	private Set<StealsDto> getSaves(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		try {
			// 查出所有比赛
			List<Match> matches = matchDao.queryLeagueMatch(leagueId);
			// 统计所有队伍的比赛场次
			Map<Long, Integer> count = countMatch(matches);
			/*
			 * List<Integer> matchIds = new ArrayList<>(); for (Match match :
			 * matches) { matchIds.add(match.getId()); }
			 */
			// 添加抢断
			List<Action> actions = actionService.queryHelp(leagueId, ActionStatus.STEAL);
			Map<Long, Object> map = new HashMap<>();
			if (actions != null && actions.size() > 0) {
				for (Action action : actions) {
					if (action.getExtension() == null || "".equals(action.getExtension())) {
						continue;
					}
					Team team = teamDao.find(action.getTeamId());
					// action转shooter对象
					StealsDto stealsDto = new StealsDto();
					stealsDto.setTeamName(team == null ? "无" : team.getName());
					stealsDto.getShooter(action.getExtension());
					if (null != stealsDto.getPlayerId()) {

						if (map.containsKey(stealsDto.getPlayerId())) {
							StealsDto origin = (StealsDto) map.get(stealsDto.getPlayerId());
							origin.stealsCount();
							origin.setCount(count.get(action.getTeamId()));
							origin.avgSteals();
							map.put(stealsDto.getPlayerId(), origin);
						} else {

							User user = userDao.find(stealsDto.getPlayerId());
							if (user != null) {
								if (user.getRealName() != null) {
									stealsDto.setName(user.getRealName());
								} else {
									stealsDto.setName(user.getNickName());
								}
							} else {
								stealsDto.setName("无名");
							}

							if (stealsDto.getPlayerId() != null && !"".equals(stealsDto.getPlayerId())) {
								stealsDto.stealsCount();
								stealsDto.setCount(count.get(action.getTeamId()));
								stealsDto.avgSteals();
								map.put(stealsDto.getPlayerId(), stealsDto);
							}
						}
					}
				}
			}

			Set<StealsDto> newList = new TreeSet<StealsDto>();
			for (Map.Entry<Long, Object> foul : map.entrySet()) {
				newList.add((StealsDto) foul.getValue());
			}
			return newList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	// 篮板榜
	private Set<BackboardDto> getAttack(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		try {
			// 查出所有比赛
			List<Match> matches = matchDao.queryLeagueMatch(leagueId);
			// 统计所有队伍的比赛场次
			Map<Long, Integer> count = countMatch(matches);
			/*
			 * List<Integer> matchIds = new ArrayList<>(); for (Match match :
			 * matches) { matchIds.add(match.getId()); }
			 */

			List<Action> actions = actionService.queryHelp(leagueId, ActionStatus.OFFENSIVE_REBOUND);
			Map<Long, Object> map = new HashMap<>();
			if (actions != null && actions.size() > 0) {
				for (Action action : actions) {
					if (action.getExtension() == null || "".equals(action.getExtension())) {
						continue;
					}
					Team team = teamDao.find(action.getTeamId());
					// action转shooter对象
					BackboardDto backboardDto = new BackboardDto();
					backboardDto.setTeamName(team == null ? "无" : team.getName());
					backboardDto.getShooter(action.getExtension());
					if (null != backboardDto.getPlayerId()) {

						if (map.containsKey(backboardDto.getPlayerId())) {
							BackboardDto origin = (BackboardDto) map.get(backboardDto.getPlayerId());
							origin.backboardCount();
							origin.setCount(count.get(action.getTeamId()));
							origin.avgBackboard();
							map.put(backboardDto.getPlayerId(), origin);
						} else {

							User user = userDao.find(backboardDto.getPlayerId());
							if (user != null) {
								if (user.getRealName() != null) {
									backboardDto.setName(user.getRealName());
								} else {
									backboardDto.setName(user.getNickName());
								}
							} else {
								backboardDto.setName("无名");
							}

							if (backboardDto.getPlayerId() != null && !"".equals(backboardDto.getPlayerId())) {
								backboardDto.backboardCount();
								backboardDto.setCount(count.get(action.getTeamId()));
								backboardDto.avgBackboard();
								map.put(backboardDto.getPlayerId(), backboardDto);
							}
						}
					}
				}
			}

			Set<BackboardDto> newList = new TreeSet<BackboardDto>();
			for (Map.Entry<Long, Object> foul : map.entrySet()) {
				newList.add((BackboardDto) foul.getValue());
			}
			return newList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	// 盖帽榜
	private Set<CoverDto> getcoverList(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		try {
			// 查出所有比赛
			List<Match> matches = matchDao.queryLeagueMatch(leagueId);
			// 统计所有队伍的比赛场数
			Map<Long, Integer> count = countMatch(matches);

			List<Long> matchIds = new ArrayList<>();
			for (Match match : matches) {
				matchIds.add(match.getId());
			}

			List<Action> actions = actionService.queryHelp(leagueId, ActionStatus.BLOCKSHOT);
			Map<Long, Object> map = new HashMap<>();
			if (actions != null && actions.size() > 0) {
				for (Action action : actions) {
					if (action.getExtension() == null || "".equals(action.getExtension())) {
						continue;
					}
					Team team = teamDao.find(action.getTeamId());
					// action转shooter对象
					CoverDto coverDto = new CoverDto();
					coverDto.setTeamName(team == null ? "无" : team.getName());
					coverDto.getShooter(action.getExtension());
					if (null != coverDto.getPlayerId()) {

						if (map.containsKey(coverDto.getPlayerId())) {
							CoverDto origin = (CoverDto) map.get(coverDto.getPlayerId());
							origin.coverCount();
							origin.setCount(count.get(action.getTeamId()));
							origin.avgCover();
							map.put(coverDto.getPlayerId(), origin);
						} else {

							User user = userDao.find(coverDto.getPlayerId());
							if (user != null) {
								if (user.getRealName() != null) {
									coverDto.setName(user.getRealName());
								} else {
									coverDto.setName(user.getNickName());
								}
							} else {
								coverDto.setName("无名");
							}

							if (coverDto.getPlayerId() != null && !"".equals(coverDto.getPlayerId())) {
								coverDto.coverCount();
								coverDto.setCount(count.get(action.getTeamId()));
								coverDto.avgCover();
								map.put(coverDto.getPlayerId(), coverDto);
							}
						}
					}
				}
			}

			Set<CoverDto> newList = new TreeSet<CoverDto>();
			for (Map.Entry<Long, Object> foul : map.entrySet()) {
				newList.add((CoverDto) foul.getValue());
			}
			return newList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	// 计算积分榜排名
	private Set<ScoresList> getScoreList(Long leagueId) {
		if (leagueId == null) {
			return null;
		}
		try {
			Set<ScoresList> result = new TreeSet<>();
			// 查询出比赛结束的该联赛的所有比赛
			Map<String, Object> params = new HashMap<>();
			params.put("lid", leagueId);
			params.put("status", 100);
			List<Match> matches = matchDao.find(params);
			if (matches != null && matches.size() != 0) {
				for (Match match : matches) {
					if (result == null) {
						ScoresList home = new ScoresList();
						ScoresList visiting = new ScoresList();
						// 设置球队名字
						Team homeT = teamDao.find(home.getTeamId());
						Team visiT = teamDao.find(visiting.getTeamId());
						home = home.countHome(match, homeT);
						visiting = visiting.countVisiting(match, visiT);
						home.setTeamName(homeT == null ? "" : homeT.getName());
						visiting.setTeamName(visiT == null ? "" : visiT.getName());
						// 判断该队是否在Set集合中，如果存在则替换，不存在则添加进去
						result.add(home);
						result.add(visiting);
					} else {
						Iterator iterator = result.iterator();
						Integer homeCount = 0;
						Integer visitingCount = 0;
						while (iterator.hasNext()) {
							ScoresList s = (ScoresList) iterator.next();
							if (s.getTeamId().equals(match.getHomeId())) {// 主队
								Team homeT = teamDao.find(s.getTeamId());
								s = s.countHome(match, homeT);
								s.setTeamName(homeT == null ? "" : homeT.getName());
								homeCount++;
							} else if (s.getTeamId().equals(match.getVisitingId())) {// 客队
								Team visiT = teamDao.find(s.getTeamId());
								s = s.countVisiting(match, visiT);
								s.setTeamName(visiT == null ? "" : visiT.getName());
								visitingCount++;
							} else {
								;
							}
						}
						if (homeCount.equals(0)) {// 添加新的主队球队
							ScoresList home = new ScoresList();
							Team homeT = teamDao.find(home.getTeamId());
							home = home.countHome(match, homeT);
							// 设置球队名字
							home.setTeamName(homeT == null ? "" : homeT.getName());
							result.add(home);
						}
						if (visitingCount.equals(0)) {// 添加新的客队球队
							ScoresList visiting = new ScoresList();
							Team visiT = teamDao.find(visiting.getTeamId());
							visiting = visiting.countVisiting(match, visiT);
							// 设置球队名字
							visiting.setTeamName(visiT == null ? "" : visiT.getName());
							result.add(visiting);
						}
					}
				}
			}
			Set<ScoresList> newList = new TreeSet<ScoresList>();
			for (ScoresList scoresList : result) {
				newList.add(scoresList);
			}
			return newList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	// 所有队伍的比赛场次
	private Map<Long, Integer> countMatch(List<Match> matches) {
		Map<Long, Integer> count = new HashMap<>();
		for (Match match : matches) {
			if (count.containsKey(match.getHomeId())) {
				Integer cou = count.get(match.getHomeId());
				cou++;
				count.put(match.getHomeId(), cou);
			} else {
				Integer cou = 1;
				count.put(match.getHomeId(), cou);
			}
			if (count.containsKey(match.getVisitingId())) {
				Integer cou = count.get(match.getHomeId());
				cou++;
				count.put(match.getHomeId(), cou);
			} else {
				Integer cou = 1;
				count.put(match.getVisitingId(), cou);
			}
		}
		return count;
	}

	@Override
	protected Class<?> getClazz() {
		return LeagueService.class;
	}
}
