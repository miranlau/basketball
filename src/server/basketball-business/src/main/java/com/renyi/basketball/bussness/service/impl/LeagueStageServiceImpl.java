package com.renyi.basketball.bussness.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.LeagueStageDao;
import com.renyi.basketball.bussness.po.LeagueStage;
import com.renyi.basketball.bussness.service.LeagueStageService;

@Service
public class LeagueStageServiceImpl implements LeagueStageService {
	private static Logger logger = Logger.getLogger(LeagueStageServiceImpl.class);

	@Resource
	private LeagueStageDao leagueStageDao;

	@Override
	public List<LeagueStage> queryAll() {
		logger.info("Start to invoke queryAll()");
		return leagueStageDao.queryAll();
	}

}
