package com.renyi.basketball.bussness.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.MatchRoundDao;
import com.renyi.basketball.bussness.po.MatchRound;
import com.renyi.basketball.bussness.service.MatchRoundService;

@Service
public class MatchRoundServiceImpl implements MatchRoundService {
	private static Logger logger = Logger.getLogger(MatchRoundServiceImpl.class);

	@Resource
	private MatchRoundDao matchRoundDao;

	@Override
	public List<MatchRound> queryAll() {
		logger.info("Start to invoke queryAll()");
		return matchRoundDao.queryAll();
	}

}
