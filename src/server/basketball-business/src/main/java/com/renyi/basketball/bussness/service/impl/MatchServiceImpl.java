package com.renyi.basketball.bussness.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.common.MatchStatus;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dao.LeagueDao;
import com.renyi.basketball.bussness.dao.MatchDao;
import com.renyi.basketball.bussness.dao.PlayerDao;
import com.renyi.basketball.bussness.dao.SeasonDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dao.VenueDao;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.po.Venue;
import com.renyi.basketball.bussness.service.ActionService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.sms.YTXSMSTransmitter;
import com.renyi.basketball.bussness.utils.DateUtil;
import com.renyi.basketball.bussness.utils.PropertyUtils;
import com.renyi.basketball.bussness.utils.PushMessageUtil;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;
import jersey.repackaged.com.google.common.collect.Lists;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by Administrator on 2016/5/16 0016.
 */
@Service
public class MatchServiceImpl extends BaseServiceImpl<Match, Long> implements MatchService {
	@Resource
	private MatchDao matchDao;
	@Resource
	private TeamDao teamDao;
	@Resource
	private VenueDao venueDao;
	@Resource
	private UserDao userDao;
	@Resource
	private PlayerDao playerDao;
	@Resource
	private LeagueDao leagueDao;
	@Resource
	private SeasonDao seasonDao;
	@Resource
	private MatchStatsService matchStatsService;
	@Resource
	private ActionService actionService;

	/** 云通讯 ，需要填写帐号和密码 */
	YTXSMSTransmitter ytxsmsTransmitter = new YTXSMSTransmitter(BasketballConstants.SMSConstant.ACCOUNT_SID,
			BasketballConstants.SMSConstant.AUTH_TOKEN, BasketballConstants.SMSConstant.APP_ID);

	@Resource
	public void setBaseDao(MatchDao matchDao) {
		super.setBaseDao(matchDao);
	}

	protected org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());

	@Override
	public void createMatching(Long teamId, Long userId, Date date, Byte matchFormat, String areacode, String power,
			String age, String desc, Double longitude, Double latitude, String addr) throws BusinessException {
		Match match = new Match();
		match.setHomeId(teamId);
		match.setCreateTime(date);
		match.setMatchTime(date);
		// match.setDesc(desc);
		// match.setAgeRange(age);
		// match.setPowerRange(power);
		// match.setMatchFormat(matchFormat);
		// match.setStatus(MatchStatus.booking.code());
		// match.setLatitude(latitude);
		// match.setLongitude(longitude);
		// match.setAddress(addr);
		match.setVenueId(1L);// 设置场地
		match.setLeagueId(10100l);// 创建匹配赛

		// match.setHomeusers(userId.toString()); // 创建者

		try {
			matchDao.insert(match);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("创建新匹配赛错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public void createSingle(Long teamId, Date date, Byte matchFormat, Long userId) throws BusinessException {
		Match match = new Match();
		match.setHomeId(teamId);
		match.setCreateTime(date);
		match.setMatchTime(date);
		match.setMatchFormat(matchFormat);
		// match.setAgeRange("18-60");
		// match.setPowerRange("0-5");
		// FIXME xfenwei 已经匹配约赛的status
//		match.setStatus(MatchStatus.matched.getId());
		match.setVenueId(1L);// 设置场地
		match.setLeagueId(10000l);// 创建单队
		// match.setHomeusers(userId.toString());

		try {
			matchDao.insert(match);
			Date nowDate = new Date();
			Long chazhi = date.getTime() - nowDate.getTime();
			if (chazhi / (1000 * 60) <= 30) {
				autoStart();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("创建新单队赛错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Page<Match> query(Pageable pageable, final String keyword, final Long userId,
			final Double longitude, final Double latitude) throws BusinessException {
		Page<Match> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Match>>() {
			@Override
			public List<Match> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(keyword)) {
					searchKeyword = "%" + keyword + "%";
				}
				List<Match> list = null;
				if (latitude == null || longitude == null) {
					list = matchDao.queryMatches(searchKeyword);
				} else {
					list = matchDao.queryNearMatches(BasketballConstants.CLEAR_MILES, longitude, latitude);
				}
				return list;
			}
		});
		return page;
	}

	@Override
	public Page<Match> queryByUser(final Long userId, Pageable pageable, final List<Team> teams)
			throws BusinessException {
		Page<Match> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Match>>() {
			@Override
			public List<Match> call() throws Exception {

				List<Match> list = matchDao.queryByUser(userId);
				List<Match> matchDtos = null;
				if (null != list) {
					matchDtos = new ArrayList<>();
					for (Match match : list) {
						matchDtos.add(convertToMatch(match));
					}
				}
				return matchDtos;
			}
		});
		/*
		 * for(Match matchDto : page.getContent()){ try { matchDto =
		 * addMoreInfo(matchDto,teams,userId); } catch (Exception e) {
		 * logger.error(e.getMessage(), e); throw new
		 * BusinessException("po转dto失败!", e, ErrorCode.PO_CANNOT_TO_DTO.code());
		 * } }
		 */
		return page;
	}

	@Override
	public Match queryMatchById(Long matchId) throws BusinessException {
		Match match = matchDao.queryById(matchId);
		if (match == null) {
			return null;
		}
		
		// 主客队
		Long homeId = match.getHomeId();
		if (homeId != null && homeId > 0) {
			match.setHomeTeam(teamDao.find(homeId));
		}
		Long visitingId = match.getVisitingId();
		if (visitingId != null && visitingId > 0) {
			match.setVisitingTeam(teamDao.find(visitingId));
		}
		
		// 主客队球员
		List<Long> homePlayIdList = match.getHomePlayerIdList();
		if (CollectionUtils.isNotEmpty(homePlayIdList)) {
			match.setHomePlayerList(
					getPlayerList(homeId, homePlayIdList, match.getHomeStartingIdList(), match.getHomeLineupIdList()));
		}
		List<Long> visitingPlayerIdList = match.getVisitingPlayerIdList();
		if (CollectionUtils.isNotEmpty(visitingPlayerIdList)) {
			match.setVisitingPlayerList(getPlayerList(visitingId, visitingPlayerIdList,
					match.getVisitingStartingIdList(), match.getVisitingLineupIdList()));
		}
		// 主客队记分员
		Long homeRecorderId = match.getHomeRecorderId();
		if (homeRecorderId != null && homeRecorderId > 0) {
			match.setHomeRecorder(userDao.find(match.getHomeRecorderId()));
		}
		Long visitingRecorderId = match.getVisitingRecorderId();
		if (visitingRecorderId != null && visitingRecorderId > 0) {
			match.setVisitingRecorder(userDao.find(match.getVisitingRecorderId()));
		}
		// 场馆
		match.setVenue(venueDao.find(match.getVenueId()));
		// 赛季
		match.setSeason(seasonDao.find(match.getSeasonId()));

		return match;
	}

	private List<Player> getPlayerList(Long teamId, List<Long> playerIdList, List<Long> startingIdList, List<Long> lineupIdList) {
		List<Player> playerList = playerDao.queryPlayerList(playerIdList, teamId);
		if (playerList == null || playerList.isEmpty()) {
			return null;
		}
		for (Player player : playerList) {
			player.setIsEnroll(true);
			if (startingIdList != null && startingIdList.contains(player.getId())) {
				player.setIsStarting(true);
			}
			if (lineupIdList != null && lineupIdList.contains(player.getId())) {
				player.setIsLineup(true);
			}
		}
		return playerList;
	}
	
	@Override
	public void update(Long matchId, Long teamId, Date date, Byte matchFormat, String areaCode, String power,
			String age, String desc, Double longitude, Double latitude, String addr) throws BusinessException {
		Match match = new Match();
		match.setId(matchId);
		match.setHomeId(teamId);
		match.setMatchTime(date);
		if (matchFormat != null) {
			match.setMatchFormat(matchFormat);
		}
		// match.setAreaCode(areaCode);
		// match.setPowerRange(power);
		// match.setAgeRange(age);
		// match.setDesc(desc);
		// match.setLongitude(longitude);
		// match.setLatitude(latitude);
		// match.setAddress(addr);
		matchDao.updateMatch(match);
	}

	@Override
	public void delete(Long matchId) throws BusinessException {
		matchDao.deleteById(matchId);
	}

	@Override
	public void gauntlet(Long matchId, Long teamId) {
		// Match match = new Match();
		// match.setId(matchId);
		// Match oldmatch = matchDao.find(matchId);
		// String dating = oldmatch.getDatingTeamIds();
		//
		// if (StringUtils.isNotEmpty(dating)) {
		// dating = dating + "," + teamId;
		// String[] strs = dating.split(BasketballConstants.TEAM_ID_SEPARATOR);
		// Set<String> collections = new HashSet<>();
		// for (String str : strs) {
		// collections.add(str);
		// }
		//
		// // 重新生成应约球队集合
		// StringBuffer sb = new StringBuffer();
		// for (String team : collections) {
		// sb.append(BasketballConstants.TEAM_ID_SEPARATOR + team);
		// }
		//
		// dating = sb.substring(1);
		// } else {
		// dating = teamId + "";
		// }
		// // dating = dating + teamId+",";
		// match.setDatingTeamIds(dating);
		// matchDao.updateMatch(match);

		// User user =
		// userService.find(teamService.find(oldmatch.getHomeId()).getLeader());
		// List<User> users = new ArrayList();
		// users.add(user);
		// String msg = "您创建的比赛有人应战啦，点击查看详情";
		// sendPush(users, match, msg);
	}

	@Override
	public void cancelGauntlet(Long matchId, Long teamId) {
		// Match match = new Match();
		// match.setId(matchId);
		// Match oldmatch = matchDao.find(matchId);
		// String dating = oldmatch.getDatingTeamIds();
		// String s[] = dating.split(",");
		// if (s.length > 1)
		// dating = dating.replaceAll("," + teamId, "");
		// else {
		// dating = dating.replaceAll("" + teamId, "");
		// }
		// dating = dating.replaceAll(teamId + ",", "");
		// match.setDatingTeamIds(dating);
		// matchDao.updateMatch(match);
		// User user =
		// userService.find(teamService.find(oldmatch.getHomeId()).getLeader());
		// List<User> users = new ArrayList();
		// String msg = "您创建的比赛有人取消应战了，点击查看详情";
		// sendPush(users, match, msg);
	}

	@Override
	public void chooseTeam(Long matchId, Long teamId) {
		Match match = new Match();
		match.setId(matchId);
		match.setVisitingId(teamId);
		// match.setVisitingstrating(teamDao.find(teamId).getLeader()+""); //
		// 不是首发, 是报名名单
		// match.setVisitingusers(teamDao.find(teamId).getLeader() + "");
		// FIXME xfenwei已经删除状态matched
//		match.setStatus(MatchStatus.matched.getId());// 进入赛前状态
		matchDao.updateMatch(match);
		Date nowDate = new Date();
		Date date = matchDao.find(matchId).getMatchTime();
		Long chazhi = date.getTime() - nowDate.getTime();
		if (chazhi / (1000 * 60) <= 30) {
			autoStart();
		}
		// User user = userService.find(teamService.find(teamId).getLeader());
		// List<User> users = new ArrayList();
		// users.add(user);
		// String msg = "您向" +
		// teamService.find(matchDao.find(matchId).getHomeId()).getName() +
		// "队发起的约战，选择和您对战！点击查看详情";
		// sendPush(users, match, msg);
	}

	@Override
	public void enroll(Long matchId, Long userId, Long flag) {
		// FIXME 重新写
		Match oldmatch = matchDao.find(matchId);
		// //String visitingusers = oldmatch.getVisitingUsersIds();
		// //String homeusers = oldmatch.getHomeusers();
		// if (flag == 0) {
		// // homeusers = homeusers + userId+",";
		// if (!StringUtils.isEmpty(homeusers)) {
		// homeusers = homeusers + "," + userId;
		// } else {
		// homeusers = userId + "";
		// }
		// }
		// if (flag == 1) {
		// // visitingusers = visitingusers +userId+",";
		// if (!StringUtils.isEmpty(visitingusers)) {
		// visitingusers = visitingusers + "," + userId;
		// } else {
		// visitingusers = userId + "";
		// }
		// }
		// Match match = new Match();
		// match.setId(matchId);
		// match.setVisitingusers(visitingusers);
		// match.setHomeusers(homeusers);
		// matchDao.updateMatch(match);
	}

	@Override
	public void enroll(Match match, Long teamId, Long[] playerIds) throws BusinessException {
		if (match.getHomeId().equals(teamId)) {
			match.setHomePlayerIdList(Arrays.asList(playerIds));
		} else {
			match.setVisitingPlayerIdList(Arrays.asList(playerIds));
		}
		try {
			matchDao.update(match);
			MatchStats matchStats = matchStatsService.getMatchStatsFromCache(match.getId());
			if (matchStats != null) {
				List<Player> addedPlayers = playerDao.queryPlayerList(Arrays.asList(playerIds), teamId);
				if (match.getHomeId().equals(teamId)) {
					matchStats.setHomePlayerList(mergePlayerList(addedPlayers, matchStats.getHomePlayerList()));
				} else {
					matchStats.setVisitingPlayerList(mergePlayerList(addedPlayers, matchStats.getVisitingPlayerList()));
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException(ErrorCode.DB_EXCEPTION.code());
		}
	}

	private List<Player> mergePlayerList(List<Player> addedPlayers, List<Player> playerList) {
		if(playerList == null) {
            playerList = new ArrayList<Player>();
        }
		playerList.addAll(addedPlayers);
		return playerList;
	}

	// FIXME 重新写
	@Override
	public void cancel(Long matchId, Long userId, Integer flag) {
		// Match oldmatch = matchDao.find(matchId);
		//
		// String visitingusers = oldmatch.getVisitingPlayerList();
		// String homeusers = oldmatch.getHomeusers();
		//
		// if (flag == 0) {
		// Set homeusersIds = oldmatch.getHomeusersIds();
		// homeusersIds.remove(userId);
		// homeusers = StringUtils.join(homeusersIds.toArray(), ",");
		// }
		// if (flag == 1) {
		// Set visitingusersIds = oldmatch.getVisitingusersIds();
		// visitingusersIds.remove(userId);
		// visitingusers = StringUtils.join(visitingusersIds.toArray(), ",");
		// }
		// Match match = new Match();
		// match.setId(matchId);
		// match.setVisitingusers(visitingusers);
		// match.setHomeusers(homeusers);
		// matchDao.updateMatch(match);
	}

	@Override
	public List<Match> queryLeagueMatch(Long id) throws Exception {
		List<Match> result = new ArrayList<Match>();

		try {
			List<Match> matches = matchDao.queryLeagueMatch(id);
			// 赛事
			for (Match match : matches) {
				Match matchDto = convertToMatch(match);
				League l = leagueDao.find(match.getLeagueId());
				matchDto.setLeagueName(l.getName());
				try {
					matchDto = setTeam(matchDto);
					matchDto = setPlayerList(matchDto);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					throw new BusinessException("po转dto失败!", e, ErrorCode.PO_CANNOT_TO_DTO.code());
				}
				result.add(matchDto);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询数据库出错!", e, ErrorCode.DB_EXCEPTION.code());
		}

		return result;
	}

	@Override
	// FIXME 重新写
	public void autoStart() {
		// // 查询待更新的比赛
		// List<Match> list =
		// matchDao.queryExpireMatch(MatchStatus.matched.code(),
		// Constants.START_MATCH_EXPR);
		// if (null == list) {
		// logger.info("No match need update to start.");
		// return;
		// }
		//
		// // 更新状态
		// for (Match match : list) {
		// Match match1 = matchDao.find(match.getId());
		// try {
		// if (match1.getVisitingusers() == null ||
		// StringUtils.isEmpty(match1.getVisitingusers())) {
		// Match newmatch = new Match();
		// newmatch.setId(match.getId());
		// if (match1.getVisiting() != null) {
		// Team team1 = teamDao.find(match1.getVisiting());
		// newmatch.setVisitingusers(team1.getLeader() + "");
		// matchDao.update(newmatch);
		// }
		// }
		// if (match1.getHomeusers() == null ||
		// StringUtils.isEmpty(match1.getHomeusers())) {
		// Match newmatch = new Match();
		// newmatch.setId(match.getId());
		// Team team1 = teamDao.find(match1.getHome());
		// newmatch.setHomeusers(team1.getLeader() + "");
		// matchDao.update(newmatch);
		// }
		// matchDao.updateStatus(match.getId(), MatchStatus.game);
		// } catch (Exception e) {
		// logger.error(e.getMessage(), e);
		// }
		// }

	}

	@Override
	public void autoEnd() {
		// 查询待更新的比赛
		// FIXME xfenwei已经删除game的status
		List<Match> list = null;	//matchDao.queryExpireMatch(MatchStatus.game.getId(), BasketballConstants.END_MATCH_EXPR);
		if (null == list) {
			logger.info("No match need update to ended.");
			return;
		}

		// 更新状态
		for (Match match : list) {
			try {
				matchDao.updateStatus(match.getId(), MatchStatus.end);
				if (matchStatsService.build(match.getId()) != null) {
					matchStatsService.build(match.getId()).setMatchStatus(ActionStatus.GAME_OVER);
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

	@Override
	public List<Player> getPlayers(Long teamId, Long matchId) {
		Match match = matchDao.queryById(matchId);
		List<Long> userIds = null;
		List<Player> playerDtos = new ArrayList<>();
		if (teamId.equals(match.getHomeId())) {
			userIds = match.getHomePlayerIdList();
		}
		if (teamId.equals(match.getVisitingId())) {
			userIds = match.getVisitingPlayerIdList();
		}
		if (userIds != null) {
			for (Long id : userIds) {
				// Player playerDto = userService.queryByUser(id);
				// playerDtos.add(playerDto);
			}
		}
		return playerDtos;
	}

	@Override
	public void saveStarting(Long matchId, boolean isHome, String startingIds) {
		try {
			Match match = new Match();
			match.setId(matchId);
			if (isHome) {
				match.setHomeStartingIds(startingIds);
				match.setHomeLineupIds(startingIds);
			} else {
				match.setVisitingStartingIds(startingIds);
				match.setVisitingLineupIds(startingIds);
			}
			matchDao.update(match);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("更新球队首发异常!", e, ErrorCode.FAIL.code());
		}
	}

	@Override
	public JSONArray createPlayers(Long matchId) {
		List<Object> hPlayers = new ArrayList<>();
		MatchStats matchStats = matchStatsService.build(matchId);
		List<Player> homePlayers = matchStats.getHomePlayerList();
		for (Player player : homePlayers) {
			player.setPlayingTime(new Date().getTime());

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", player.getId());
			jsonObject.put("name", player.getName());
			jsonObject.put("avatar", player.getAvatar());
			jsonObject.put("number", player.getUniformNumber());
			jsonObject.put("playingTime", player.getPlayingTime());
			// Player tempPlayer = new Player();
			// tempPlayer.setOnTime(new Date());
			// tempPlayer.setId(player.getId());
			// tempPlayer.setName(player.getName());
			// tempPlayer.setAvatar(player.getAvatar());
			// tempPlayer.setNumber(player.getNumber());
			hPlayers.add(jsonObject);
		}
		List<Object> vPlayers = new ArrayList<>();
		if (null != matchStats.getVisiting()) {
			List<Player> visitingPlayers = matchStats.getVisitingPlayerList();
			for (Player player : visitingPlayers) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", player.getId());
				jsonObject.put("name", player.getName());
				jsonObject.put("avatar", player.getAvatar());
				jsonObject.put("number", player.getUniformNumber());
				jsonObject.put("playTime", player.getPlayingTime());
				// player.setOnTime(new Date());
				// Player tempPlayer = new Player();
				// tempPlayer.setOnTime(new Date());
				// tempPlayer.setId(player.getId());
				// tempPlayer.setName(player.getName());
				// tempPlayer.setAvatar(player.getAvatar());
				// tempPlayer.setNumber(player.getNumber());
				vPlayers.add(jsonObject);
			}
		}
		List<Object> rootList = new ArrayList<>();
		rootList.add(hPlayers);
		rootList.add(vPlayers);
//		matchStats.setPlayingTime(JSONArray.fromObject(rootList).toString());
		Match match = new Match();
		match.setId(matchId);
		// FIXME 重构？
		// match.setPlayers(JSONArray.fromObject(rootList).toString());
		matchDao.updateMatch(match);
		return JSONArray.fromObject(rootList);
	}

	@Override
	public List<Match> queryDoneMatches(Long teamId) {
		return matchDao.queryDoneMatches(teamId);
	}

	@Override
	public List<Match> queryNeedStatsMatches() {
		return matchDao.queryNeedStatsMatches();
	}

	@Override
	public Page<Action> queryMatchData(Pageable pageable, final Long matchId, final Integer status,
			final Integer action) {
		// 找出主客队ID
		Match match = matchDao.find(matchId);
		if (match == null) {
			return null;
		}
		final Long homeId = match.getHomeId();
		final Long visitingId = match.getVisitingId();
		Page<Action> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Action>>() {
			@Override
			public List<Action> call() throws Exception {
				List<Action> actions = new ArrayList<Action>();
				Map<String, Object> params = new HashMap<String, Object>();
				if (status.equals(0)) {
					params.put("tid", homeId);
					params.put("mid", matchId);
				} else if (status.equals(1)) {
					params.put("tid", visitingId);
					params.put("mid", matchId);
				} else {
					params.put("mid", matchId);
				}
				params.put("action", action);
				actions = actionService.findActions(params);
				List<Action> actionDtos = new ArrayList<Action>();
				for (Action action : actions) {
					Action actionDto = new Action();
					actionDto.setId(action.getId());
					actionDto.setStatus(action.getStatus().intValue());
					actionDto.setTeamId(action.getTeamId());
					actionDto.setPlayerId(action.getPlayerId());
					actionDto.setSpeaks(action.getSpeaks());
					actionDto.setQuarterRemainingTime(action.getQuarterRemainingTime());
					if (action.getStatus().intValue() != ActionStatus.START) {
						actionDto.setExtension(action.getExtension());
					}
					if (action.getCreateTime() != null) {
						actionDto.setCreateTime(action.getCreateTime());
					}
					actionDto.setMatchId(action.getMatchId());
					actionDtos.add(actionDto);
				}
				return actionDtos;
			}
		});
		// FIXME xfenwei
		// List<Action> actionDtos = page.getContent();
		// for (Action action : actionDtos) {
		// // 注入球队名称
		// Team team = teamDao.find(action.getTeamId());
		// action.setTeamName(team == null ? null : team.getName());
		// }

		return page;
	}

	@Override
	public void saveMatch(String isSendMessage, Match match) throws BusinessException {
		// 查询场地信息
		try {
			Venue places = venueDao.find(match.getVenueId());
			if (places != null) {
				// match.setAddress(places.getAddress() + places.getName());
				// match.setLongitude(places.getLng());
				// match.setLatitude(places.getLat());
				// match.setAgeRange("18-60");
				// match.setPowerRange("");
				// TODO match的areaCode?
			}
			save(match);
			// 主客队球员推送通知
			List<User> aceptor = new ArrayList<>();
			List<Long> homeusers = match.getHomePlayerIdList();
			List<Long> visitingusers = match.getVisitingPlayerIdList();
			homeusers.addAll(visitingusers);
			// for (Long homeuser : homeusers) {
			// User user = userService.find(homeuser);
			// if (user != null) {
			// aceptor.add(user);
			// }
			// }
			// sendPush(aceptor, match, "您的球队有了新的比赛");
			// if ("1".equals(isSendMessage)) {
			// // 发送短信息给队长
			// Team home = teamService.find(match.getHomeId());
			// Team visiting = teamService.find(match.getVisitingId());
			// if (home != null && visiting != null) {
			// User homeLead = userService.find(home.getLeader());
			// User visiLead = userService.find(visiting.getLeader());
			// if (homeLead != null && visiLead != null) {
			// ytxsmsTransmitter.sendSMS(homeLead.getMobile(),
			// new String[]{
			// ":" + new SimpleDateFormat("yyyy-MM-dd
			// HH:mm:ss").format(match.getMatchTime()),
			// ":" + match.getAddress(), ":" + visiting.getName()},
			// BasketballConstants.SMSConstant.ADD_MATCH_TEMPLATE_ID);
			// ytxsmsTransmitter.sendSMS(visiLead.getMobile(),
			// new String[]{
			// ":" + new SimpleDateFormat("yyyy-MM-dd
			// HH:mm:ss").format(match.getMatchTime()),
			// ":" + match.getAddress(), ":" + home.getName()},
			// BasketballConstants.SMSConstant.ADD_MATCH_TEMPLATE_ID);
			// }
			// }
			// }
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	@Override
	public List<Match> queryMatchByTeamId(Long teamId, Integer closeNum) {
		List<Match> matches = matchDao.queryMatchByTeamId(teamId, closeNum);
		return matches;
	}
	
	@Override
	public List<Match> queryMatchByTeamIdAndLeagueId(Long teamId, Long leagueId) {
		List<Match> matches = matchDao.queryMatchByTeamIdAndLeagueId(teamId, leagueId);
		return matches;
	}

	@Override
	public List<Match> queryOnlyMatchByTeam(Long teamId, Integer size) throws BusinessException {
		try {
			List<Match> matches = matchDao.queryMatchByTeamId(teamId, size);
			if (matches == null) {
				return null;
			}

			List<Match> list = new ArrayList<>();
			// for (Match match : matches) {
			// Match matchDto = new Match();
			// PropertyUtils.copyPropertiesExclude(match, matchDto,
			// new String[] { "home", "visiting", "visitingusers", "homeusers",
			// "dating" });
			//
			// // 主队
			// if (match.getHomeId() != null) {
			// Team team = teamService.find(match.getHomeId());
			// if (null != team) {
			// TeamDto teamDto = new TeamDto();
			// PropertyUtils.copyPropertiesExclude(team, teamDto);
			// matchDto.setHomeTeam(teamDto);
			// }
			// }
			//
			// // 客队
			// if (match.getVisitingId() != null) {
			// Team team = teamService.find(match.getVisitingId());
			// if (team != null) {
			// TeamDto teamDto = new TeamDto();
			// PropertyUtils.copyPropertiesExclude(team, teamDto);
			// matchDto.setVisitingTeam(teamDto);
			// }
			// }
			//
			// list.add(matchDto);
			// }
			return list;
		} catch (Exception e) {
			logger.error(e.getMessage() + "11111111111111111111111", e);
			logger.info(e.getMessage() + "222222222222222222222222");
			throw new BusinessException("获取比赛失败!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Page<Match> queryMatchesPage(Pageable pageable, final String teanName, 
			final String leagueName, final String start, final String end) {
		Page<Match> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Match>>() {
			@Override
			public List<Match> call() throws Exception {
				String startTime = null;
				String endTime = null;
				if (StringUtils.isNotEmpty(start)) {
					startTime = start + " 00:00:00";
				}
				if (StringUtils.isNotEmpty(end)) {
					endTime = end + " 23:59:59";
				}
				return matchDao.queryMatchesPage(teanName, leagueName, startTime, endTime);
			}
		});
		return page;
	}

	@Override
	public void updateMatch(Match match) {
		try {
			matchDao.updateMatch(match);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("更新比赛错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<Match> queryMyMatchShedule(User user, String date) throws BusinessException {
		if (user == null) {
			return null;
		}
		try {
			List<Match> matches = matchDao.findMyMatchDate(user.getId());
			List<Match> myMatches = null;
			List<Match> result = null;
			if (matches != null && matches.size() > 0) {
				result = new ArrayList<>();
				myMatches = new ArrayList<>();
				for (Match match : matches) {
					List<Long> home = match.getHomePlayerIdList();
					List<Long> visiting = match.getVisitingPlayerIdList();
					if (home != null && home.size() > 0) {
						for (Long id : home) {
							if (user.getId().equals(id)) {
								myMatches.add(match);
								break;
							}
						}
					}
					if (visiting != null && visiting.size() > 0) {
						for (Long id : visiting) {
							if (user.getId().equals(id)) {
								myMatches.add(match);
								break;
							}
						}
					}
				}
				Date queryDate;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				for (Match myMatch : myMatches) {
					Date matchDate = myMatch.getMatchTime();
					queryDate = sdf.parse(date);
					if (DateUtil.isSameDate(queryDate, matchDate)) {
						Match matchDto = new Match();
						// PropertyUtils.copyPropertiesExclude(myMatch,
						// matchDto,
						// new String[] { "home", "visiting", "visitingusers",
						// "homeusers", "dating" });
						// // 主队
						// if (myMatch.getHomeId() != null) {
						// Team team = teamService.find(myMatch.getHomeId());
						// TeamDto teamDto = new TeamDto();
						// PropertyUtils.copyPropertiesExclude(team, teamDto);
						// matchDto.setHomeTeam(teamDto);
						// }
						// // 客队
						// if (myMatch.getVisitingId() != null) {
						// Team team =
						// teamService.find(myMatch.getVisitingId());
						// TeamDto teamDto = new TeamDto();
						// PropertyUtils.copyPropertiesExclude(team, teamDto);
						// matchDto.setVisitingTeam(teamDto);
						// }
						result.add(matchDto);
					}
				}
			}
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException();
		}
	}

	@Override
	public List<Long> queryMyMatchSheduleTime(User user) {
		if (user == null) {
			return null;
		}
		try {
			List<Match> matches = matchDao.findMyMatchDate(user.getId());
			List<Date> result = null;
			if (matches != null && matches.size() > 0) {
				result = new ArrayList<>();
				for (Match match : matches) {
					List<Long> home = match.getHomePlayerIdList();
					List<Long> visiting = match.getVisitingPlayerIdList();
					if (home != null && home.size() > 0) {
						for (Long id : home) {
							if (user.getId().equals(id)) {
								result.add(match.getMatchTime());
								break;
							}
						}
					}
					if (visiting != null && visiting.size() > 0) {
						for (Long id : visiting) {
							if (user.getId().equals(id)) {
								result.add(match.getMatchTime());
								break;
							}
						}
					}
				}
			}
			if (result != null && result.size() > 0) {
				List<Long> time = new ArrayList<>();
				for (Date date : result) {
					time.add(date.getTime());
				}
				return time;
			}
			return null;
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException();
		}
	}

	/**
	 * 设置球队和球员相关信息
	 */
	protected Match setTeam(Match matchDto) throws Exception {
		Long homeTeamId = matchDto.getHomeTeam().getId();
		if (homeTeamId == null || homeTeamId <= 0) {
			return null;
		}
		Long visitingTeamId = matchDto.getVisitingTeam().getId();
		if (visitingTeamId == null || visitingTeamId <= 0) {
			return null;
		}

		// 设置主队
		Team homeTeam = teamDao.find(homeTeamId);
		if (homeTeam != null) {
			PropertyUtils.copyPropertiesExclude(homeTeam, matchDto.getHomeTeam());
		}

		// 设置客队
		Team visitingTeam = teamDao.find(visitingTeamId);
		if (visitingTeam != null) {
			PropertyUtils.copyPropertiesExclude(visitingTeam, matchDto.getVisitingTeam());
		}
		return matchDto;
	}

	/**
	 * 设置球队和球员相关信息
	 */
	protected Match setPlayerList(Match match) throws Exception {
		// 设置主队球员
		List<Long> homePlayerIdList = match.getHomePlayerIdList();
		if (homePlayerIdList != null && homePlayerIdList.size() != 0) {
			match.setHomePlayerList(playerDao.queryPlayerList(homePlayerIdList, match.getHomeTeam().getId()));
		}
		// 设置客队球员
		List<Long> visitingPlayerIdList = match.getVisitingPlayerIdList();
		if (visitingPlayerIdList != null && visitingPlayerIdList.size() != 0) {
			match.setVisitingPlayerList(
					playerDao.queryPlayerList(visitingPlayerIdList, match.getVisitingTeam().getId()));
		}
		return match;
	}

	/**
	 * 基本数据po转dto
	 * 
	 * @param match
	 * @return
	 * @throws Exception
	 */
	// FIXME 重新做
	protected Match convertToMatch(Match match) throws Exception {
		Match matchDto = new Match();
		// PropertyUtils.copyPropertiesExclude(match, matchDto,
		// new String[] { "home", "visiting", "dating", "homeusers",
		// "visitingusers" });
		// TeamDto hometeam = new TeamDto();
		// hometeam.setMatchId(match.getHome());
		// matchDto.setHome(hometeam);
		// if (null != match.getStatus()) {
		// matchDto.setStatus(match.getStatus());
		// }
		// TeamDto visitingteam = new TeamDto();
		// visitingteam.setMatchId(match.getVisiting());
		// matchDto.setVisiting(visitingteam);
		// matchDto.setLeagueId(match.getLeagueId());
		// if (match.getLeagueId().equals(10000)) {
		// matchDto.setPlayerType(0);
		// } else if (match.getLeagueId().equals(10100)) {
		// matchDto.setPlayerType(1);
		// } else {
		// matchDto.setPlayerType(2);
		// }
		// if (userId != null) {
		// // FIXME
		// // matchDto.setLeagueName(match.getLeagueName());
		// if (teams != null && teams.size() != 0) {
		// boolean flag = false;
		// String dating = match.getDatingTeamIds();
		// for (Team team : teams) {
		// if (team.getLeader().equals(userId)) {// 如果是主队的队长访问，就不显示已应约
		// // if (team.getId().equals(matchDto.getHome().getId()))
		// // {
		// // flag = true;
		// // }
		// if (dating != null && dating.indexOf(team.getId().toString()) >= 0) {
		// flag = true;
		// break;
		// }
		// }
		// }
		// matchDto.setGauntlet(flag);
		// }
		// // if (userId != null) {
		// String allusers = match.getHomeUsers() + "," +
		// match.getVisitingusers();
		// if (allusers != null && allusers.indexOf(userId.toString()) >= 0) {
		// matchDto.setEnroll(true);
		// } else {
		// matchDto.setEnroll(false);
		// }
		// // }
		//
		// }
		// if (match.getDatingIds() != null && !"".equals(match.getDating())) {
		// matchDto.setDatings(match.getDatingIds().size());
		// } else {
		// matchDto.setDatings(0);
		// }
		// hometeam.setLogo(match.getHomeLogo());
		// hometeam.setName(match.getHomeName());
		// hometeam.setId(match.getHome());
		// matchDto.setHome(hometeam);
		//
		// visitingteam.setLogo(match.getVisitingLogo());
		// visitingteam.setName(match.getVisitingName());
		// visitingteam.setId(match.getVisiting());
		// matchDto.setVisiting(visitingteam);
		//
		// matchDto.setHomeUserIds(match.getHomeusersIds());
		// matchDto.setVisitingUserIds(match.getVisitingusersIds());
		return matchDto;
	}

	/**
	 * 推送方法
	 * 
	 * @param users
	 * @param match
	 * @param msg
	 */
	public void sendPush(List<User> users, Match match, String msg) {
		// 楼主
		if (users != null && users.size() != 0 && match != null) {
			match = matchDao.find(match.getId());
			Map<String, String> extend = new HashMap<>();
			extend.put("matchId", match.getId().toString());
			extend.put("matchStatus", match.getStatus().toString());
			extend.put("pushType", PushMessageUtil.MATCH_MANAGE_TYPE_);
			extend.put("senderHeader", "avatar/avatar.png");
			Date date = new Date();
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime = sf.format(date);
			extend.put("sendTime", sendTime);
			extend.put("content", msg);
			for (User user : users) {
				PushMessageUtil.pushMessages(user, msg, extend);
			}
		}
	}
	
	@Override
	public List<Match> queryMatchsNeedRecorder(Long recorderId) {
		List<Match> matches = matchDao.queryMatchsNeedRecorder(recorderId);
		if (CollectionUtils.isEmpty(matches)) {
			return Lists.newArrayList();
		}
		for (Match match : matches) {
			match.setHomeTeam(teamDao.queryTeam(match.getHomeId()));
			match.setVisitingTeam(teamDao.queryTeam(match.getVisitingId()));
			match.setVenue(venueDao.find(match.getVenueId()));
		}
		
		return matches;
	}
	

	@Override
	protected Class<?> getClazz() {
		return MatchService.class;
	}

}
