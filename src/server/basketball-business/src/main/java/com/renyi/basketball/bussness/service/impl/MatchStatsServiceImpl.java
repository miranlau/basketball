package com.renyi.basketball.bussness.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.common.IdAnalyzer;
import com.renyi.basketball.bussness.constants.ActionSpeaker;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.ResponseParams;
import com.renyi.basketball.bussness.dao.TeamMatchStatsDao;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.dto.QuarterData;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.ActionService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.PlayerService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.utils.CachedManager;
import com.renyi.basketball.bussness.utils.JpushUtils;
import com.renyi.basketball.bussness.utils.JsonUtil;
import com.renyi.basketball.bussness.utils.PushMessageUtil;

import jersey.repackaged.com.google.common.collect.Lists;
import jersey.repackaged.com.google.common.collect.Maps;
import net.sf.json.JSONObject;

@Service
public class MatchStatsServiceImpl implements MatchStatsService {
	private Logger logger = Logger.getLogger(getClass());

	@Resource
	private PlayerService playerService;
	@Resource
	private TeamService teamService;
	@Resource
	private MatchService matchService;
	@Resource
	private ActionService actionService;
	@Resource
	private TeamMatchStatsDao teamMatchStatsDao;
	
	/**
	 * 获取比赛.
	 * 
	 * @param matchId
	 *            比赛ID
	 * @return 比赛
	 */
	@Override
	public MatchStats getMatchStatsFromCache(Long matchId) {
		MatchStats matchStats = CachedManager.get(BasketballConstants.CACHE_GAME_TEAM_PREFIX + matchId,
				MatchStats.class);
		return matchStats;
	}

	/**
	 * 将比赛放入缓存.
	 * 
	 * @param matchStats
	 */
	@Override
	public void saveToCache(MatchStats matchStats) {
		CachedManager.put(BasketballConstants.CACHE_GAME_TEAM_PREFIX + matchStats.getMatchId(), matchStats);
	}

	@Override
	public void pushMessage(User user, MatchStats matchStats, Team team,Integer status) {
		try {
			if (matchStats.getVisiting() == null) {
				return;
			}
			String content = "";
			Map<String, String> extendsparameter = new HashMap<>();
			extendsparameter.put("pushType", "3");
			if (matchStats.getHome().getId().equals(team.getId())) {
				// 主队记分员操作，向客队计分员推送消息
				if (matchStats.getVisitingRecorder() == null) {
					logger.warn("客队记分员为空，无需推送");
					return;
				}
				logger.debug("Push message to visiting recorder: " + matchStats.getVisitingRecorder()
						+ ", conetent=" + content + ", extendsparameter=" + extendsparameter);
				PushMessageUtil.pushMessages(matchStats.getVisitingRecorder(), content, extendsparameter);
			} else {
				// 客队记分员操作，向主队计分员推送消息
				if (matchStats.getHomeRecorder() == null) {
					logger.warn("主队记分员为空，无需推送");
					return;
				}
				logger.debug("Push message to home recorder: " + matchStats.getHomeRecorder() + ", conetent="
						+ content + ", extendsparameter=" + extendsparameter);
				PushMessageUtil.pushMessages(matchStats.getHomeRecorder(), content, extendsparameter);
			}
		} catch (Exception e) {
			logger.error("比赛推送通知错误！", e);
		}
	}

	@Override
	public MatchStats build(Long matchId, Boolean isResfresh) {
		if (isResfresh == null || !isResfresh) {
			logger.info("Load MatchStats from Cache, matchId = " + matchId);
			MatchStats matchStats = getMatchStatsFromCache(matchId);
			if (matchStats != null) {
				logger.info("Return MatchStats from Cache, matchId = " + matchId);
				return matchStats;
			}
		}

		logger.info("Return MatchStats from DB, matchId = " + matchId);
		
		return matchToMatchStats(matchService.queryMatchById(matchId));
	}

	@Override
	public MatchStats build(Long matchId) {
		return build(matchId, false);
	}
	
	private MatchStats matchToMatchStats(Match match) {
		if (match == null) {
			return null;
		}
		MatchStats matchStats = new MatchStats();
		matchStats.setMatchId(match.getId());
		matchStats.setLeagueId(match.getLeagueId());
		matchStats.setLeagueStageId(match.getLeagueStageId());
		matchStats.setRoundId(match.getRoundId());
		matchStats.setMatchStatus(match.getStatus());
		matchStats.setQuarterCount(match.getQuarterCount());
		matchStats.setQuarterTime(match.getQuarterTime());
		matchStats.setMatchFormat(match.getMatchFormat());
		matchStats.setMatchTime(match.getMatchTime());
		matchStats.setHomeScore(match.getHomeScore());
		matchStats.setVisitingScore(match.getVisitingScore());
		// 设置球队
		matchStats.setHome(match.getHomeTeam());
		matchStats.setVisiting(match.getVisitingTeam());
		// 设置球员
		matchStats.setHomePlayerList(match.getHomePlayerList());
		matchStats.setVisitingPlayerList(match.getVisitingPlayerList());
		// 设置记分员
		matchStats.setHomeRecorder(match.getHomeRecorder());
		matchStats.setVisitingRecorder(match.getVisitingRecorder());
		// 设置场上的球员IDs
		matchStats.setHomeLineupSet(IdAnalyzer.toSet(match.getHomeStartingIds()));
		matchStats.setVisitingLineupSet(IdAnalyzer.toSet(match.getVisitingStartingIds()));
		
		// 设置当前比赛在第几节
		if (matchStats.getMatchStatus() > ActionStatus.STOP_TIME) {
			matchStats.setQuarterIndex(match.getStatus() - ActionStatus.STOP_TIME);
		} else if (matchStats.getMatchStatus() > ActionStatus.NORMAL_TIME) {
			matchStats.setQuarterIndex(match.getStatus() - ActionStatus.NORMAL_TIME);
		}
		
		// 设置每节的数据(每节比分|球员上场时间)
		String matchData = match.getMatchData();
		List<QuarterData> quarterDataList = null;
		if (StringUtils.isNotBlank(matchData)) {
			quarterDataList = JsonUtil.toObject(matchData, List.class, QuarterData.class);
			matchStats.setQuarterDataList(quarterDataList);
		}
		
		// 设置球员的比赛时间
		Map<Long, Long> playingTimes = new HashMap<Long, Long>();
		if (quarterDataList != null && quarterDataList.size() > 0) {
			for (QuarterData quarterData : quarterDataList) {
				Map<Long, Long> homePlayingTimes = quarterData.getHomePlayingTime();
				if (homePlayingTimes != null && homePlayingTimes.size() > 0) {
					playingTimes.putAll(homePlayingTimes);
				}
				Map<Long, Long> visitingPlayingTimes = quarterData.getVisitingPlayingTime();
				if (visitingPlayingTimes != null && visitingPlayingTimes.size() > 0) {
					playingTimes.putAll(visitingPlayingTimes);
				}
			}
		}
		matchStats.setPlayingTimes(playingTimes);
		
		// 设置比赛的所有Action
		List<Action> actionList = actionService.queryListByMatchId(match.getId());
		if (actionList == null) {
			actionList = Lists.newArrayList();
		}
		matchStats.setActionList(actionList);
		
		saveToCache(matchStats);

		return matchStats;
	}

	@Override
	public List<Map<String, Object>> publishMessage(Long matchId, User recorder, String message, Long playerId, Integer status) {
		logger.debug("enter publishMessage, matchId=" + matchId + ", playerId=" + playerId + ", status=" + status);
		Match match = null;
		MatchStats matchStats = getMatchStatsFromCache(matchId);
		if (null == matchStats) {
			match = matchService.queryMatchById(matchId);
			if (match == null) {
				throw new BusinessException("比赛不存在!", ErrorCode.OBJECT_EXIST_ERROR.code());
			}
			matchStats = build(matchId);
		}

		Player player = playerService.find(playerId);
		String tempMsg = null;
		boolean needSave = false;

		switch (status) {
		case ActionStatus.LIVE_MESSAGE: // 直播消息
			JSONObject js = new JSONObject();
			js.put("status", status);
			js.put("name", player.getName());
			js.put("photo", player.getAvatar());
			js.put("message", message);
			tempMsg = js.toString();
			break;
		case ActionStatus.LIVE_PHOTO: // 直播图片
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", status);
			jsonObject.put("name", player.getName());
			jsonObject.put("photo", player.getAvatar());
			jsonObject.put("message", message);
			tempMsg = jsonObject.toString();
			break;
		default:
			throw new BusinessException("操作无法识别!", ErrorCode.INVALID_ACTION.code());
		}

		logger.debug(tempMsg);
		Action action = new Action();
		action.setRecorderId(recorder.getId());
		action.setExtension(tempMsg);
		action.setCreateTime(new Date());
		action.setMatchId(matchId);
		action.setStatus(status);
		action.setSpeaks(message);
		action.setPlayerId(playerId);

		actionService.save(action);
		matchStats.addAction(action);

		if (needSave) {
			updateMatch(matchStats, null);
		}
		
		return matchStats.getMatchSpeaks(20);
	}

	@Override
	public Map<String, Object> substituteAction(User recorder, Long matchId, Long teamId, Long[] upPlayerIds, Long[] downPlayerIds, Integer quarterRemainingTime)
			throws BusinessException {
		logger.debug("enter substitute, matchId=" + matchId + ", teamId=" + teamId + ", upPlayerId=" + upPlayerIds
				+ ", downPlayerId=" + downPlayerIds);

		MatchStats matchStats = build(matchId);
		if(matchStats.isOver() || !matchStats.isStarted()) {
			logger.error("Match is not in progress, cannot substitute!");
			throw new BusinessException("Match is not in progress, can not substitute!", ErrorCode.INVALID_ACTION.code());
		}

		Action action = new Action();
		action.setStatus(ActionStatus.SUBSTITUTE);
		action.setMatchId(matchId);
		action.setTeamId(teamId);
		action.setCreateTime(new Date());
		action.setRecorderId(recorder.getId());

		Team team = matchStats.getTeam(teamId);
		action.setSpeaks(ActionSpeaker.getSpeak(ActionStatus.SUBSTITUTE, team.getName(),
				matchStats.getPlayerNumberByIds(teamId, upPlayerIds), matchStats.getPlayerNumberByIds(teamId, downPlayerIds)));

		Map<String, Object> ext = Maps.newHashMap();
		ext.put(BasketballConstants.KEY_CHANGE_UP, upPlayerIds);
		ext.put(BasketballConstants.KEY_CHANGE_DOWN, downPlayerIds);
		action.setExtension(JsonUtil.toJson(ext));
		action.setQuarterRemainingTime(quarterRemainingTime);

		matchStats.substitute(team.getId(), Arrays.asList(upPlayerIds), Arrays.asList(downPlayerIds));
		matchStats.addAction(action);
		
		saveAction(action);
		
		matchStats.getActionList().add(action);
		
		updateMatch(matchStats, quarterRemainingTime);
		
		return buildActionResponse(matchStats);
	}

	private Map<String, Object> buildActionResponse(MatchStats matchStats) {
		Map<String, Object> data = Maps.newHashMap();
		data.put(ResponseParams.MATCH_STATUS, matchStats.getMatchStatus());
		data.put(ResponseParams.HOME_SCORE, matchStats.getHomeScore());
		data.put(ResponseParams.VISITING_SCORE, matchStats.getVisitingScore());
		data.put(ResponseParams.MATCH_SPEAK_LIST, matchStats.getMatchSpeaks(20));
		data.put(ResponseParams.HOME_LINEUP_ID_LIST, matchStats.getHomeLineupSet());
		data.put(ResponseParams.VISITING_LINEUP_ID_LIST, matchStats.getVisitingLineupSet());
		return data;
	}

	@Override
	public Map<String, Object> addAction(Long matchId, Long teamId, User recorder, Long playerId, Integer actionStatus, Integer quarterRemainingTime,
			String extension) throws BusinessException {
		logger.debug("enter addAction, matchId=" + matchId + ", teamId=" + teamId + ", recorder=" + recorder
				+ ", playerId=" + playerId + ", statusAction=" + actionStatus);

		MatchStats matchStats = build(matchId);
		if (matchStats == null) {
			logger.error("matchStats is null, cannot add action");
			throw new BusinessException("比赛不存在!", ErrorCode.OBJECT_EXIST_ERROR.code());
		}

		synchronized (matchStats) {
			Date now = new Date();
			Team home = matchStats.getHome();
			Team visiting = matchStats.getVisiting();
			// 获取球队名称
			String homeName = (home == null ? BasketballConstants.DEFAULT_HOME_TEAM_NAME : home.getName());
			String visitingName = (visiting == null ? BasketballConstants.DEFAULT_VISITING_TEAM_NAME : visiting.getName());
			String team = teamId.equals(home.getId()) ? homeName : visitingName;
			// 获取球员名称
			String playerName = getPlayerName(teamId, playerId, matchStats);
			// 话术
			String speak;
			String pushTag;
			if (teamId.equals(home.getId())){
				pushTag = visiting.getId()+"";
			}else {
				pushTag = home.getId()+"";
			}
			boolean isPush = false;
			switch (actionStatus) {
			// 比赛开始，第一节开始
			case ActionStatus.START:
				matchStats.increaseQuarter();
				checkCanRecord(teamId, matchStats, ActionStatus.NORMAL_TIME);
				logger.info("the match starts at " + now);
				matchStats.setMatchStatus(ActionStatus.NORMAL_TIME + matchStats.getQuarterIndex());
				matchStats.initQuarterData();
				setPlayerUpTime(matchStats, now);
				speak = ActionSpeaker.getSpeak(actionStatus);
				// pushMessage(user, game, team, status);
				isPush = true;
				break;
			// 本节结束
			case ActionStatus.QUARTER_END:
				checkCanRecord(teamId, matchStats, ActionStatus.STOP_TIME);
				logger.info("normal quarter: " + matchStats.getQuarterIndex() + " ends at " + now);
				matchStats.setMatchStatus(ActionStatus.STOP_TIME + matchStats.getQuarterIndex());
				calcPlayingTime(matchStats);
				speak = ActionSpeaker.getSpeak(actionStatus, matchStats.getQuarterIndex());
				isPush = true;
				break;
			// 下一节开始
			case ActionStatus.QUARTER_START:
				matchStats.increaseQuarter();
				checkCanRecord(teamId, matchStats, ActionStatus.NORMAL_TIME);
				logger.info("normal quarter: " + matchStats.getQuarterIndex() + " starts at " + now);
				matchStats.setMatchStatus(ActionStatus.NORMAL_TIME + matchStats.getQuarterIndex());
				matchStats.initQuarterData();
				setPlayerUpTime(matchStats, now);
				// pushMessage(user, game, team, status);
				speak = ActionSpeaker.getSpeak(actionStatus, matchStats.getQuarterIndex());
				isPush = true;
				break;
			// 加时开始
			case ActionStatus.OVERTIME_START:
				matchStats.increaseQuarter();
				checkCanRecord(teamId, matchStats, ActionStatus.NORMAL_TIME);
				logger.info("overtime quarter: " + matchStats.getOvertimeQuarterIndex() + " starts at " + now);
				matchStats.setMatchStatus(ActionStatus.NORMAL_TIME + matchStats.getQuarterIndex());
				matchStats.initQuarterData();
				setPlayerUpTime(matchStats, now);
				speak = ActionSpeaker.getSpeak(actionStatus, matchStats.getOvertimeQuarterIndex());
				// pushMessage(user, game, team, status);
				isPush = true;
				break;
			// 加时小节结束
			case ActionStatus.OVERTIME_END:
				checkCanRecord(teamId, matchStats, ActionStatus.STOP_TIME);
				logger.info("overtime quarter: " + matchStats.getOvertimeQuarterIndex() + " ends at " + now);
				matchStats.setMatchStatus(ActionStatus.STOP_TIME + matchStats.getQuarterIndex());
				calcPlayingTime(matchStats);
				speak = ActionSpeaker.getSpeak(actionStatus, matchStats.getOvertimeQuarterIndex());
				// pushMessage(user, game, team, status);
				isPush = true;
				break;
			// 暂停
			case ActionStatus.TIMEOUT:
				logger.info("the match is timeout at " + now);
				matchStats.setSuspend(true);
				speak = ActionSpeaker.getSpeak(actionStatus, matchStats.getHomeScore(), matchStats.getVisitingScore());
				calcPlayingTime(matchStats);
				// pushMessage(user, game, team, status);
				break;
			// 暂停后继续
			case ActionStatus.TIMEOUT_RESUME:
				logger.info("the match resumes from timeout at " + now);
				matchStats.setSuspend(false);
				speak = ActionSpeaker.getSpeak(actionStatus, matchStats.getHomeScore(), matchStats.getVisitingScore());
				setPlayerUpTime(matchStats, now);
				// pushMessage(user, game, team, status);
				break;
			// 比赛结束
			case ActionStatus.GAME_OVER:
				checkCanRecord(teamId, matchStats, ActionStatus.GAME_OVER);
				logger.info("the match ends at " + now);
				matchStats.setMatchStatus(ActionStatus.GAME_OVER);
				calcPlayingTime(matchStats);

				String winteamName = matchStats.getHomeScore() > matchStats.getVisitingScore() ? homeName : visitingName;
				String loseteamName = matchStats.getHomeScore() > matchStats.getVisitingScore() ? visitingName : homeName;
				speak = (ActionSpeaker.getSpeak(actionStatus, matchStats.getHomeScore(),
						matchStats.getVisitingScore(), winteamName, loseteamName));
				// pushMessage(user, game, team, status);
				isPush = true;
				break;
			// 助攻
			case ActionStatus.ASSIST:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 前场篮板
			case ActionStatus.OFFENSIVE_REBOUND:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 后场篮板
			case ActionStatus.DEFENSIVE_REBOUND:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 盖帽
			case ActionStatus.BLOCKSHOT:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 抢断
			case ActionStatus.STEAL:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 失误
			case ActionStatus.TURNOVER:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 进攻犯规
			case ActionStatus.OFFENSIVE_FOUL:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 防守犯规
			case ActionStatus.DEFENSIVE_FOUL:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 技术犯规
			case ActionStatus.TECHNICAL_FOUL:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 恶意犯规
			case ActionStatus.FLAGRANT_FOUL:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 违体犯规
			case ActionStatus.UNSPORTSMANLIKE_FOUL:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;				
			// 罚球(进)
			case ActionStatus.FREE_THROW_IN:
				matchStats.addScore(teamId, 1);
				matchStats.updateMatchData(teamId, 1);
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName, matchStats.getHomeScore(),
						matchStats.getVisitingScore());
				isPush = true;
				break;
			// 罚球(丢)
			case ActionStatus.FREE_THROW_OUT:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName);
				break;
			// 2分(中)
			case ActionStatus.TWO_LEFTSIDE_STANDING_IN:
			case ActionStatus.TWO_LEFTSIDE_DRIBBLING_IN:
			case ActionStatus.TWO_RIGHTSIDE_STANDING_IN:
			case ActionStatus.TWO_RIGHTSIDE_DRIBBLING_IN:
			case ActionStatus.TWO_PAINTED_IN:
			case ActionStatus.TWO_FRONT_STANDING_IN:
			case ActionStatus.TWO_FRONT_DRIBBLING_IN:
				matchStats.addScore(teamId, 2);
				matchStats.updateMatchData(teamId, 2);
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName, matchStats.getHomeScore(),
						matchStats.getVisitingScore());
				isPush = true;
				break;
			// 2分(不中)
			case ActionStatus.TWO_LEFTSIDE_STANDING_OUT:
			case ActionStatus.TWO_LEFTSIDE_DRIBBLING_OUT:
			case ActionStatus.TWO_RIGHTSIDE_STANDING_OUT:
			case ActionStatus.TWO_RIGHTSIDE_DRIBBLING_OUT:
			case ActionStatus.TWO_PAINTED_OUT:
			case ActionStatus.TWO_FRONT_STANDING_OUT:
			case ActionStatus.TWO_FRONT_DRIBBLING_OUT:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName, matchStats.getHomeScore(),
						matchStats.getVisitingScore());
				break;
			// 3分(命中)
			case ActionStatus.THREE_LEFT_WING_STANDING_IN:
			case ActionStatus.THREE_LEFT_WING_DRIBBLING_IN:
			case ActionStatus.THREE_LEFTSIDE45_STANDING_IN:
			case ActionStatus.THREE_LEFTSIDE45_DRIBBLING_IN:
			case ActionStatus.THREE_ROOT_ARC_STANDING_IN:
			case ActionStatus.THREE_ROOT_ARC_DRIBBLING_IN:
			case ActionStatus.THREE_RIGHTSIDE45_STANDING_IN:
			case ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_IN:
			case ActionStatus.THREE_RIGHT_WING_STANDING_IN:
			case ActionStatus.THREE_RIGHT_WING_DRIBBLING_IN:
				matchStats.addScore(teamId, 3);
				matchStats.updateMatchData(teamId, 3);
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName, matchStats.getHomeScore(),
						matchStats.getVisitingScore());
				isPush = true;
				break;

			// 3分(不中)
			case ActionStatus.THREE_LEFT_WING_STANDING_OUT:
			case ActionStatus.THREE_LEFT_WING_DRIBBLING_OUT:
			case ActionStatus.THREE_LEFTSIDE45_STANDING_OUT:
			case ActionStatus.THREE_LEFTSIDE45_DRIBBLING_OUT:
			case ActionStatus.THREE_ROOT_ARC_STANDING_OUT:
			case ActionStatus.THREE_ROOT_ARC_DRIBBLING_OUT:
			case ActionStatus.THREE_RIGHTSIDE45_STANDING_OUT:
			case ActionStatus.THREE_RIGHTSIDE45_DRIBBLING_OUT:
			case ActionStatus.THREE_RIGHT_WING_STANDING_OUT:
			case ActionStatus.THREE_RIGHT_WING_DRIBBLING_OUT:
				speak = ActionSpeaker.getSpeak(actionStatus, team, playerName, matchStats.getHomeScore(),
						matchStats.getVisitingScore());
				break;

			default:
				throw new BusinessException("非法的操作代码: " + actionStatus, ErrorCode.INVALID_ACTION.code());
			}
			
			logger.info("speak for action: " + actionStatus + "is: " + speak);

			// 撤销操作不需要保存Action
			if (!actionStatus.equals(ActionStatus.REVOKE_ACTION)) {
				Action action = new Action();
				action.setPlayerId(playerId);
				action.setMatchId(matchId);
				action.setTeamId(teamId);
				action.setRecorderId(recorder.getId());
				action.setStatus(actionStatus);
				action.setSpeaks(speak);
				action.setQuarterRemainingTime(quarterRemainingTime);
				action.setCreateTime(new Date());
				if (StringUtils.isNotBlank(extension)) {
					action.setExtension(extension);
				}
				
				saveAction(action);
				
				matchStats.addAction(action);
			}

			updateMatch(matchStats, quarterRemainingTime);
			if (isPush) {
				JpushUtils.sendForMatch(pushTag);
			}
			return buildActionResponse(matchStats);
		}

	}
	
	@Override
	public Map<String, Object> revertAction(Long matchId, Long teamId, User recorder, Integer quarterRemainingTime) {
		MatchStats matchStats = build(matchId);
		
		long i = matchStats.revert(teamId);
		
		if (i == 0) {
			throw new BusinessException("没有更多操作!", ErrorCode.INVALID_ACTION.code());
		} else if (i == 1) {
			throw new BusinessException("比赛状态不能撤销!", ErrorCode.INVALID_ACTION.code());
		}
		
		actionService.delete(i);
		
		List<Action> actionList = matchStats.getActionList();
		if (CollectionUtils.isNotEmpty(actionList)) {
			actionList.remove(actionList.size() - 1);
		}
		
		updateMatch(matchStats, quarterRemainingTime);
		// pushMessage(user, game, team, status);

		return buildActionResponse(matchStats);
	}

	@Override
	public Map<String, TeamMatchStats> getTeamMatchStatsForTeam(Long matchId) {
		logger.info("start to invoke buildTeamMatchStatsForTeam(), matchId = " + matchId);
		if (matchId == null || matchId <= 0) {
			logger.error("can not buildPlayerMatchStatsForTeam due to matchId is invalid, matchId = " + matchId);
			return null;
		}
		MatchStats matchStats = build(matchId);
		if (matchStats == null) {
			logger.error("can not buildPlayerMatchStatsForTeam due to get MatchStats failed, matchId = " + matchId);
			return null;
		}
		return matchStats.buildTeamMatchStatsForTeam();
	}

	@Override
	public Map<String, List<PlayerMatchStats>> getPlayerMatchStatsForTeam(Long matchId) {
		logger.info("start to invoke buildPlayerMatchStatsForTeam(), matchId = " + matchId);
		if (matchId == null || matchId <= 0) {
			logger.error("can not buildPlayerMatchStatsForTeam due to matchId is invalid, matchId = " + matchId);
			return null;
		}
		MatchStats matchStats = build(matchId);
		if (matchStats == null) {
			logger.error("can not buildPlayerMatchStatsForTeam due to get MatchStats failed, matchId = " + matchId);
			return null;
		}
		logger.info("start to invoke buildPlayerMatchStatsForTeam(), matchId = " + matchId);
		return matchStats.buildPlayerMatchStatsForTeam();
	}
	
	private void checkCanRecord(Long teamId, MatchStats matchStats, int phaseBase) {
		if (!matchStats.isHome(teamId)) {
			logger.error("visiting team: " + teamId + " recorder try to record the match status, forbidden");
			throw new BusinessException("只有主队才能记录比赛状态!", ErrorCode.INVALID_ACTION.code());
		}
		if (matchStats.getMatchStatus() == phaseBase + matchStats.getQuarterIndex()) {
			if (phaseBase == ActionStatus.NORMAL_TIME) {
				logger.error("the match is started, cannot re-start");
				throw new BusinessException("比赛已经开始，不能重新开始!", ErrorCode.INVALID_ACTION.code());
			} else if (phaseBase == ActionStatus.STOP_TIME) {
				logger.error("the quarter: " + matchStats.getQuarterIndex() + " is ended, cannot re-end");
				throw new BusinessException("该小节已经结束，不能重新结束!", ErrorCode.INVALID_ACTION.code());
			}
		}
		if (matchStats.getMatchStatus() == ActionStatus.GAME_OVER) {
			logger.error("the match is ended, cannot re-end");
			throw new BusinessException("比赛已经结束，不能重新结束!", ErrorCode.INVALID_ACTION.code());
		}
	}

	private String getPlayerName(Long teamId, Long playerId, MatchStats matchStats) {
		if (playerId == null || playerId <= 0) {
			return "";
		}
		String playerName;
		Player player = matchStats.getPlayer(teamId, playerId);
		if (StringUtils.isBlank(player.getName())) {
			playerName = player.getUniformNumber() + "";
		} else {
			playerName = player.getName();
		}
		return playerName;
	}

	// 球员上场时间计算
	private void calcPlayingTime(MatchStats matchStats) {
		List<QuarterData> matchData = matchStats.getQuarterDataList();
		List<Player> players = matchStats.getAllLineupPlayers();
		Long current = System.currentTimeMillis();
		for (Player player : players) {
			player.setPlayingTime(current - player.getUpTime());
			// 重设上场时间为0
			player.setUpTime(0l);
			QuarterData quarterData = matchData.get(matchStats.getQuarterIndex() - 1);
			if (quarterData == null) {
				quarterData = new QuarterData();
				matchData.add(quarterData);
			}
			quarterData.setHomeScore(matchStats.getHomeScore());
			quarterData.setVisitingScore(matchStats.getVisitingScore());
			quarterData.updatePlayingTime(matchStats.isHome(player.getTeamId()), player.getId(), player.getPlayingTime());
		}
	}

	private void setPlayerUpTime(MatchStats matchStats, Date now) {
		List<Player> players = matchStats.getAllLineupPlayers();
		Long current = now.getTime();
		for (Player player : players) {
			player.setUpTime(current);
		}
	}

	/**
	 * 保存比赛.
	 * 
	 * @param matchStats
	 */
	private void updateMatch(MatchStats matchStats, Integer quarterRemainingTime) {
		Match match = new Match();
		match.setId(matchStats.getMatchId());
		match.setStatus(matchStats.getMatchStatus());
		match.setVisitingScore(matchStats.getVisitingScore());
		match.setHomeScore(matchStats.getHomeScore());
		match.setHomeLineupIds(IdAnalyzer.toString(matchStats.getHomeLineupSet()));
		match.setVisitingLineupIds(IdAnalyzer.toString(matchStats.getVisitingLineupSet()));
		match.setQuarterRemainingTime(quarterRemainingTime);
		if (matchStats.getQuarterDataList() != null) {
			match.setMatchData(JsonUtil.toJson(matchStats.getQuarterDataList()));
		}
		try {
			matchService.updateMatch(match);
		} catch (Exception e) {
			String errorInfo = "update match failed after add action, matchId = " + matchStats.getMatchId();
			logger.error(errorInfo, e);
			throw new BusinessException(errorInfo, ErrorCode.FAIL.code());
		}
	}

	@Override
	public void clear(Long[] matchIds) {
		for (Long matchId : matchIds) {
			Match match = matchService.find(matchId);
			if (match != null && match.getStatus() == ActionStatus.GAME_OVER) {
				CachedManager.remove(BasketballConstants.CACHE_GAME_TEAM_PREFIX + matchId);
			}
		}
	}
	
	private void saveAction(Action action) {
		logger.debug("save action, action=" + action);
		actionService.save(action);
	}

	
}
