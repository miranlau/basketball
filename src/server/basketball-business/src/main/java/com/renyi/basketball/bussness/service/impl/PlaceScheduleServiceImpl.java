package com.renyi.basketball.bussness.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.PlaceScheduleDao;
import com.renyi.basketball.bussness.po.PlaceSchedule;
import com.renyi.basketball.bussness.service.PlaceScheduleService;

@Service
public class PlaceScheduleServiceImpl extends BaseServiceImpl<PlaceSchedule, Long> implements PlaceScheduleService {
	@Resource
	private PlaceScheduleDao placeScheduleDao;

	@Resource
	public void setBaseDao(PlaceScheduleDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	protected Class<?> getClazz() {
		return PlaceScheduleService.class;
	}
}
