package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.PlaceScheduleDao;
import com.renyi.basketball.bussness.dao.PlacesDao;
import com.renyi.basketball.bussness.po.PlaceSchedule;
import com.renyi.basketball.bussness.po.Place;
import com.renyi.basketball.bussness.service.PlacesService;

@Service
public class PlacesServiceImpl extends BaseServiceImpl<Place, Long> implements PlacesService {
	@Resource
	private PlacesDao placesDao;
	@Resource
	private PlaceScheduleDao placeScheduleDao;
	@Resource
	public void setBaseDao(PlacesDao placesDao) {
		super.setBaseDao(placesDao);
	}

	@Override
	public List<Place> queryPlaceByArenasId(Long arenasId) {
		return placesDao.queryPlaceByArenasId(arenasId);
	}

	@Override
	public List<PlaceSchedule> querySchedule(Date date, Long placeId, Integer n) {
		if (placeId == null) {
			return null;
		}
		List<PlaceSchedule> result = new ArrayList<>();
		if (n != null) {

			// 查询最近N天的场地安排
			for (int i = 0; i < n; ++i) {
				Calendar cl = Calendar.getInstance();
				cl.setTime(date);
				cl.add(Calendar.DATE, i);
				int year = cl.get(Calendar.YEAR);
				int month = cl.get(Calendar.MONTH) + 1;
				int day = cl.get(Calendar.DAY_OF_MONTH);
				PlaceSchedule schedule = new PlaceSchedule();
				schedule.setYear(year);
				schedule.setMonth(month);
				schedule.setDay(day);
				// 查出这一天的场地安排
				List<HashMap<String, String>> schedule_today = placeScheduleDao.querySchedule(year, month, day,
						placeId);
				schedule.setSchedule_today(schedule_today);
				result.add(schedule);
			}

		}
		return result;
	}

	@Override
	public List<Place> queryScheduleListByArenasId(Long arenasId, Integer n) {
		if (arenasId == null) {
			return null;
		}

		List<Place> result = placesDao.queryPlaceByArenasId(arenasId);
		if (n != null) {
			for (Place place : result) {
				List<PlaceSchedule> schedules = new ArrayList<>();
				// 查询最近N天的场地安排
				for (int i = 0; i < n; ++i) {
					Calendar cl = Calendar.getInstance();
					cl.setTime(new Date());
					cl.add(Calendar.DATE, i);
					int year = cl.get(Calendar.YEAR);
					int month = cl.get(Calendar.MONTH) + 1;
					int day = cl.get(Calendar.DAY_OF_MONTH);
					PlaceSchedule schedule = new PlaceSchedule();
					schedule.setYear(year);
					schedule.setMonth(month);
					schedule.setDay(day);
					// 查出这一天的场地安排
					List<HashMap<String, String>> schedule_today = placeScheduleDao.querySchedule(year, month, day,
							place.getId());
					schedule.setSchedule_today(schedule_today);
					schedules.add(schedule);
				}
				place.setPlaceScheduleList(schedules);
			}
		}
		return result;
	}

	@Override
	protected Class<?> getClazz() {
		return PlacesService.class;
	}
}
