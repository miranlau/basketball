package com.renyi.basketball.bussness.service.impl;

import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dao.PlayerDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.service.PlayerService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class PlayerServiceImpl extends BaseServiceImpl<Player, Long> implements PlayerService {
	@Resource
	private PlayerDao playerDao;
	@Resource
	private TeamDao teamDao;

	@Override
	@Transactional
	public void saveBatch(List<Player> playerList) {
		logger.info("Start to invoke saveBatch()");
		for (Player player : playerList) {
			try {
				logger.info("Import player: " + player.getName() + ", teamName: " + player.getTeamName());
				playerDao.insert(player);
				System.out.println(player.getTeamId() + ": " + player.getName());
			} catch (Exception e) {
				logger.info("Import player failed, player: " + player.getName() + ", teamName: "
						+ player.getTeamName() + ", error: " + e.getMessage());
			}
		}
		logger.info("save batch player successfully");
	}
	
	@Override
	public Player queryPlayerById(Long playerId) {
		return playerDao.queryPlayerById(playerId);
	}

	@Override
	public List<Player> queryPlayerList(List<Long> playerIds, Long teamId) {
		try {
			List<Player> playerList = playerDao.queryPlayerList(playerIds, teamId);
			if (playerList == null || playerList.size() == 0) {
				return null;
			}
			return playerList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("Can not get palyer list by playerIds and teamId",
					ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<Player> queryPlayerListByTeamId(Long teamId) {
		try {
			List<Player> playerList = playerDao.queryPlayerListByTeamId(teamId);
			return playerList;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("Can not get palyer list by teamId", ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Page<Player> queryPlayerPage(Pageable pageable, String keyword, Integer type) {
		Page<Player> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Player>>() {
			@Override
			public List<Player> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(keyword)) {
					searchKeyword = "%" + keyword + "%";
				}
				return playerDao.queryPlayerListByName(searchKeyword);
			}
		});
		return page;
	}

	@Override
	protected Class<?> getClazz() {
		return PlayerService.class;
	}

}
