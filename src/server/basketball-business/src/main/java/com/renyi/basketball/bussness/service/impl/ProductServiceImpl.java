package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.renyi.basketball.bussness.dao.ProductDao;
import com.renyi.basketball.bussness.dto.ProductDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.po.Product;
import com.renyi.basketball.bussness.service.ProductService;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product, Long> implements ProductService {

	@Resource
	public void setBaseDao(ProductDao productDao) {
		super.setBaseDao(productDao);
	}
	@Resource
	private ProductDao productDao;

	@Override
	public Page<ProductDto> query(final String name, Pageable pageable, final Long companyId,
			final Long areansId) {
		Page<ProductDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<ProductDto>>() {
			@Override
			public List<ProductDto> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(name)) {
					searchKeyword = "%" + name + "%";
				}
				List<Product> list = productDao.query(searchKeyword, companyId, areansId);
				if (list != null) {
					List<ProductDto> result = new ArrayList<>();
					for (Product goods : list) {
						ProductDto dto = new ProductDto();
						PropertyUtils.copyProperties(goods, dto);
						result.add(dto);
					}
					return result;
				}
				return null;
			}
		});
		List<ProductDto> productDtos = page.getContent();
		// List<ProductDto> productDtos1 = new ArrayList<>();
		/* 设置销量和库存 */
		for (ProductDto productDto : productDtos) {
			List<HashMap> list = productDao.queryGoods(productDto.getId(), areansId, companyId);
			if (list != null && list.size() != 0) {
				productDto.setSales(Integer.parseInt(list.get(0).get("sales").toString()));
				productDto.setStack(Integer.parseInt(list.get(0).get("stack").toString()));
				productDto.setArenasId(Integer.parseInt(list.get(0).get("id").toString()));
				productDto.setArenasName(list.get(0).get("name").toString());
			}
		}

		return page;
	}

	@Override
	public void saveProduct(Product product, Long areansId) throws BusinessException {
		try {
			productDao.insert(product);
			productDao.insertProductArenas(product.getId(), areansId);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("连接数据库异常");
		}
	}

	@Override
	public void updateProduct(Product product, Long areansId) {

	}

	@Override
	@Transactional
	public void saveTest(Product product) {
		// TODO 事务不起作用
		Product productRight = new Product();
		Product productWrong = new Product();
		productRight.setName("aa");
		productRight.setCompany("bb");
		productRight.setInPrice(10f);
		productRight.setOutPrice(20f);
		productRight.setImage("image");
		productRight.setDescription("desc");
		productRight.setCreateTime(new Date());
		try {
			productDao.insert(productRight);
			productDao.insert(productWrong);
			// throw new RuntimeException();
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException();
		}
	}

	@Override
	public ProductDto queryOne(Long productId, Long areansId, Long companyId) {
		Product product = productDao.find(productId);
		ProductDto productDto = new ProductDto();
		try {
			PropertyUtils.copyProperties(product, productDto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("po转dto错误");
		}
		if (productDto != null) {
			List<HashMap> list = productDao.queryGoods(productDto.getId(), areansId, companyId);
			if (list != null && list.size() != 0) {
				productDto.setSales(Integer.parseInt(list.get(0).get("sales").toString()));
				productDto.setStack(Integer.parseInt(list.get(0).get("stack").toString()));
				productDto.setArenasId(Integer.parseInt(list.get(0).get("id").toString()));
				productDto.setArenasName(list.get(0).get("name").toString());
			}
		}
		return productDto;
	}

	@Override
	public void deleteOne(Long productId, Long areansId) {
		productDao.delete(Long.valueOf(productId));
		productDao.deletesPA(productId, areansId);
	}

	@Override
	public void inoutproduct(InOutProduct inoutproduct) {
		productDao.inoutproduct(inoutproduct);
		productDao.updatePA(inoutproduct);
	}

	@Override
	protected Class<?> getClazz() {
		return ProductService.class;
	}
}
