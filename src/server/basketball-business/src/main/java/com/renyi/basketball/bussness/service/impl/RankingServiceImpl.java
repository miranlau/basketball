package com.renyi.basketball.bussness.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.RankingDao;
import com.renyi.basketball.bussness.po.Ranking;
import com.renyi.basketball.bussness.service.RankingService;
import com.renyi.basketball.bussness.utils.JsonUtil;
import com.renyi.basketball.bussness.dto.TeamRankingDto;

@Service
public class RankingServiceImpl implements RankingService {
	@Resource
	private RankingDao rankingDao;

	@Override
	public List<TeamRankingDto> queryTeamRanking(Long leagueId, Long leagueStageId) {
		Ranking ranking = rankingDao.queryTeamRanking(leagueId, leagueStageId);
		if (ranking == null) {
			return null;
		}
		return JsonUtil.toObject(ranking.getRanks(), List.class, TeamRankingDto.class);
	}
	
	@Override
	public TeamRankingDto getRank(Long teamId, Long leagueId, Long leagueStageId) {
		List<TeamRankingDto> ranking = queryTeamRanking(leagueId, leagueStageId);
		if (CollectionUtils.isEmpty(ranking)) {
			return new TeamRankingDto();
		}
		
		for (int i = 0; i < ranking.size(); i++) {
			TeamRankingDto teamRank = ranking.get(i);
			if (teamId.equals(teamRank.getTeamId())) {
				teamRank.setRank(i + 1);
				return teamRank;
			}
		}
		
		return new TeamRankingDto();
	}

}
