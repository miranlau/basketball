package com.renyi.basketball.bussness.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.renyi.basketball.bussness.dao.ReadLaudRecordDao;
import com.renyi.basketball.bussness.po.ReadLaud;
import com.renyi.basketball.bussness.service.ReadLaudRecordService;

@Repository
public class ReadLaudRecordServiceImpl extends BaseServiceImpl<ReadLaud, Long> implements ReadLaudRecordService {
	@Resource
	private ReadLaudRecordDao readLaudRecordDao;

	@Resource
	public void setBaseDao(ReadLaudRecordDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public void addNum(Long id, Long dynamicId, Integer numTypeRead) {
		readLaudRecordDao.addNum(id, dynamicId, numTypeRead);
	}

	@Override
	public Boolean isClick(Long id, Long dynamicId, Integer numTypeRead) {
		return readLaudRecordDao.count(id, dynamicId, numTypeRead) > 0;
	}

	@Override
	protected Class<?> getClazz() {
		return ReadLaudRecordService.class;
	}
}
