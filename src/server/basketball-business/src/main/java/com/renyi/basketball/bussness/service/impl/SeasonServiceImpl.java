package com.renyi.basketball.bussness.service.impl;

import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.SeasonDao;
import com.renyi.basketball.bussness.po.Season;
import com.renyi.basketball.bussness.service.SeasonService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;
import jersey.repackaged.com.google.common.collect.Lists;

@Service
public class SeasonServiceImpl extends BaseServiceImpl<Season, Long> implements SeasonService {
	@Resource
	private SeasonDao seasonDao;

	@Resource
	public void setBaseDao(SeasonDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public Page<Season> findSeasonPage(Pageable pageable, final Long leagueId, final String name) {
		Page<Season> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Season>>() {
			@Override
			public List<Season> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(name)) {
					searchKeyword = "%" + name + "%";
				}
				List<Season> result = seasonDao.queryByName(leagueId, searchKeyword);
				return result;
			}
		});
		return page;
	}
	
	@Override
	public Page<Season> findSeasonPage(Pageable pageable, final Long leagueId) {
		Page<Season> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Season>>() {
			@Override
			public List<Season> call() throws Exception {
				List<Season> result = seasonDao.queryByLeague(leagueId);
				return result;
			}
		});
		return page;
	}
	
	@Override
	public List<Season> queryByIds(List<Long> venusIds) {
		return Lists.newArrayList();
	}

	@Override
	protected Class<?> getClazz() {
		return SeasonService.class;
	}
}
