package com.renyi.basketball.bussness.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.renyi.basketball.bussness.common.RankingType;
import com.renyi.basketball.bussness.common.StatsBuilder;
import com.renyi.basketball.bussness.common.StatsStatus;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.dao.MatchDao;
import com.renyi.basketball.bussness.dao.PlayerMatchStatsDao;
import com.renyi.basketball.bussness.dao.PlayerSeasonStatsDao;
import com.renyi.basketball.bussness.dao.RankingDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.TeamMatchStatsDao;
import com.renyi.basketball.bussness.dao.TeamSeasonStatsDao;
import com.renyi.basketball.bussness.dto.TeamRankingDto;
import com.renyi.basketball.bussness.po.PlayerMatchStats;
import com.renyi.basketball.bussness.po.PlayerSeasonStats;
import com.renyi.basketball.bussness.po.Ranking;
import com.renyi.basketball.bussness.po.TeamMatchStats;
import com.renyi.basketball.bussness.po.TeamSeasonStats;
import com.renyi.basketball.bussness.service.StatsService;
import com.renyi.basketball.bussness.utils.JsonUtil;

@Service
public class StatsServiceImpl implements StatsService {
	private static Logger logger = Logger.getLogger(StatsServiceImpl.class);

	@Resource
	private TeamDao teamDao;
	@Resource
	private MatchDao matchDao;
	@Resource
	private TeamMatchStatsDao teamMatchStatsDao;
	@Resource
	private TeamSeasonStatsDao teamSeasonStatsDao;
	@Resource
	private PlayerMatchStatsDao playerMatchStatsDao;
	@Resource
	private PlayerSeasonStatsDao playerSeasonStatsDao;
	@Resource
	private RankingDao rankingDao;

	@Override
	@Transactional
	public void saveTeamMatchStats(Long matchId, TeamMatchStats homeMatchStats, TeamMatchStats visitingMatchStats) {
		logger.info("start to invoke saveTeamMatchStats(), matchId = " + matchId);

		List<TeamMatchStats> teamMatchStatsList = teamMatchStatsDao.queryByMatchId(matchId);
		if (CollectionUtils.isNotEmpty(teamMatchStatsList)) {
			logger.info("delete team match stats, matchId = " + matchId);
			teamMatchStatsDao.deleteByMatchId(matchId);
		}

		homeMatchStats = saveTeamMatchStats(homeMatchStats);
		if (homeMatchStats == null || homeMatchStats.getId() == null) {
			logger.error("save homeTeamMatchStats failed, matchId = " + matchId);
			return;
		}
		visitingMatchStats = saveTeamMatchStats(visitingMatchStats);
		if (visitingMatchStats == null || visitingMatchStats.getId() == null) {
			logger.error("save visitingTeamMatchStats failed, matchId = " + matchId);
			return;
		}

		matchDao.updateStatsStatus(matchId, StatsStatus.generate_team_match_stats.getId());

		logger.info("save TeamMatchStats successfully");
	}

	@Override
	@Transactional
	public void saveOrUpdateTeamSeasonStats(Long matchId, TeamMatchStats homeTeamMatchStats, TeamMatchStats visitingTeamMatchStats) {
		logger.info("start to invoke saveOrUpdateTeamSeasonStats(), matchId = " + matchId);

		TeamSeasonStats homeSeasonStats = saveOrUpdateTeamSeasonStats(homeTeamMatchStats);
		if (homeSeasonStats == null || homeSeasonStats.getId() == null) {
			logger.error("save homeTeamSeasonStats failed, matchId = " + matchId);
			return;
		}
		TeamSeasonStats visitingSeasonStats = saveOrUpdateTeamSeasonStats(visitingTeamMatchStats);
		if (visitingSeasonStats == null || visitingSeasonStats.getId() == null) {
			logger.error("save visitingTeamSeasonStats failed, matchId = " + matchId);
			return;
		}

		matchDao.updateStatsStatus(matchId, StatsStatus.generate_team_season_stats.getId());

		logger.info("save or update TeamSeasonStats successfully, matchId = " + matchId);
	}

	@Override
	@Transactional
	public void updateTeamRanking(Long matchId, Long leagueId, Long leagueStageId) {
		logger.info("start to invoke updateTeamRanking(), matchId = " + matchId);
		List<TeamRankingDto> teamRankingList = teamSeasonStatsDao.queryWinLossRanking(leagueId, leagueStageId);
		if (CollectionUtils.isEmpty(teamRankingList)) {
			logger.error("can not save team ranking due to List<TeamSeasonStats> is empty leagueId + " + leagueId + ", leagueStageId = "
					+ leagueId);
		}

		Ranking ranking = rankingDao.queryTeamRanking(leagueId, leagueStageId);
		if (ranking == null) {
			ranking = new Ranking();
			ranking.setLeagueId(leagueId);
			ranking.setLeagueStageId(leagueStageId);
			ranking.setType(RankingType.TEAM_RANKING.getId());
			ranking.setRanks(JsonUtil.toJson(teamRankingList));
			try {
				ranking = rankingDao.insert(ranking);
			} catch (Exception e) {
				logger.error(String.format(ResponseMsgConstants.INSERT_RANKING_ERROR, leagueId, leagueStageId), e);
				return;
			}
		} else {
			ranking.setRanks(JsonUtil.toJson(teamRankingList));
			try {
				ranking = rankingDao.update(ranking);
			} catch (Exception e) {
				logger.error(String.format(ResponseMsgConstants.UPDATE_RANKING_ERROR, leagueId, leagueStageId), e);
				return;
			}
		}

		matchDao.updateStatsStatus(matchId, StatsStatus.generate_team_ranking.getId());

		logger.info("update TeamRanking successfully, matchId = " + matchId);
	}

	@Override
	@Transactional
	public void savePlayerMatchStats(Long matchId, List<PlayerMatchStats> homeMatchStatsList,
			List<PlayerMatchStats> visitingMatchStatsList) {
		logger.info("start to invoke savePlayerMatchStats(), matchId = " + matchId);

		List<PlayerMatchStats> playerMatchStatsList = playerMatchStatsDao.queryByMatchId(matchId);
		if (CollectionUtils.isNotEmpty(playerMatchStatsList)) {
			logger.info("delete player match stats, matchId = " + matchId);
			playerMatchStatsDao.deleteByMatchId(matchId);
		}

		playerMatchStatsDao.batchSave(homeMatchStatsList);
		playerMatchStatsDao.batchSave(visitingMatchStatsList);

		matchDao.updateStatsStatus(matchId, StatsStatus.generate_player_match_stats.getId());

		logger.info("save PlayerMatchStats successfully, matchId = " + matchId);
	}

	@Override
	@Transactional
	public void saveOrUpdatePlayerSeasonStats(Long matchId, List<PlayerMatchStats> homeMatchStatsList,
			List<PlayerMatchStats> visitingMatchStatsList) {
		logger.info("start to invoke saveOrUpdatePlayerSeasonStats(), matchId = " + matchId);

		saveOrUpdatePlayerSeasonStats(homeMatchStatsList);
		saveOrUpdatePlayerSeasonStats(visitingMatchStatsList);

		matchDao.updateStatsStatus(matchId, StatsStatus.tenerate_player_season_stats.getId());

		logger.info("save or update PlayerSeasonStats successfully, matchId = " + matchId);
	}

	@Override
	public TeamSeasonStats getTeamSeasonStats(Long teamId, Long leagueId, Long leagueStageId) {
		return teamSeasonStatsDao.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
	}

	@Override
	public List<TeamMatchStats> getTeamMatchStats(Long teamId, Long leagueId, Long leagueStageId) {
		return teamMatchStatsDao.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
	}

	@Override
	public List<PlayerSeasonStats> getTeamPlayerSeasonStats(Long teamId, Long leagueId, Long leagueStageId) {
		return playerSeasonStatsDao.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
	}
	
	@Override
	public List<PlayerMatchStats> getPlayerMatchStats(Long teamId, Long leagueId, Long leagueStageId) {
		return null;
	}

	private TeamMatchStats saveTeamMatchStats(TeamMatchStats teamMatchStats) {
		try {
			return teamMatchStatsDao.insert(teamMatchStats);
		} catch (Exception e) {
			logger.error(String.format(ResponseMsgConstants.INSERT_TEAM_MATCH_STATS_ERROR, teamMatchStats.getTeamId(),
					teamMatchStats.getMatchId()), e);
		}
		return null;
	}

	private TeamSeasonStats saveOrUpdateTeamSeasonStats(TeamMatchStats teamMatchStats) {
		Long teamId = teamMatchStats.getTeamId();
		Long leagueId = teamMatchStats.getLeagueId();
		Long leagueStageId = teamMatchStats.getLeagueStageId();
		if ((teamId == null || teamId <= 0) || (leagueId == null || leagueId <= 0) || (leagueStageId == null || leagueStageId <= 0)) {
			logger.error("can not save or update TeamSeasonStats due to teamId = " + teamId + ", leagueId = " + leagueId
					+ ", leagueStageId = " + leagueStageId);
			return null;
		}
		TeamSeasonStats teamSeasonStats = teamSeasonStatsDao.queryByTeamIdAndLeague(teamId, leagueId, leagueStageId);
		if (teamSeasonStats == null) {
			logger.info(
					"start to save TeamSeasonStats, leagueId = " + leagueId + ", leagueStage = " + leagueStageId + ", teamId = " + teamId);
			try {
				return teamSeasonStatsDao.insert(StatsBuilder.buildTeamSeasonStatsForSave(teamMatchStats));
			} catch (Exception e) {
				logger.error(String.format(ResponseMsgConstants.INSERT_TEAM_SEASON_STATS_ERROR, teamId, leagueId, leagueStageId), e);
			}
		} else {
			logger.info("start to update TeamSeasonStats, leagueId = " + leagueId + ", leagueStage = " + leagueStageId + ", teamId = "
					+ teamId);
			try {
				return teamSeasonStatsDao.update(StatsBuilder.buildTeamSeasonStatsForUpdate(teamSeasonStats, teamMatchStats));
			} catch (Exception e) {
				logger.error(String.format(ResponseMsgConstants.UPDATE_TEAM_SEASON_STATS_ERROR, teamId, leagueId, leagueStageId), e);
			}
		}
		return null;
	}

	private boolean saveOrUpdatePlayerSeasonStats(List<PlayerMatchStats> playerMatchStatsList) {
		if (CollectionUtils.isEmpty(playerMatchStatsList)) {
			logger.info("can not save or update TeamSeasonStats due to playerMatchStatsList is empty");
			return false;
		}
		Long playerId = null;
		Long leagueId = null;
		Long leagueStageId = null;
		for (PlayerMatchStats playerMatchStats : playerMatchStatsList) {
			playerId = playerMatchStats.getPlayerId();
			leagueId = playerMatchStats.getLeagueId();
			leagueStageId = playerMatchStats.getLeagueStageId();
			if ((playerId == null || playerId <= 0) || (leagueId == null || leagueId <= 0)
					|| (leagueStageId == null || leagueStageId <= 0)) {
				logger.error("can not save or update PlayerSeasonStats due to playerId = " + playerId + ", leagueId = " + leagueId
						+ ", leagueStageId = " + leagueStageId);
				return false;
			}
			PlayerSeasonStats playerSeasonStats = playerSeasonStatsDao.queryByPlayerIdAndLeague(playerMatchStats.getPlayerId(),
					playerMatchStats.getLeagueId(), playerMatchStats.getLeagueStageId());
			if (playerSeasonStats == null) {
				logger.info("start to save PlayerSeasonStats, leagueId = " + leagueId + ", leagueStage = " + leagueStageId + ", playerId = "
						+ playerId);
				try {
					playerSeasonStatsDao.insert(StatsBuilder.buildPlayerSeasonStatsForSave(playerMatchStats));
				} catch (Exception e) {
					logger.error(String.format(ResponseMsgConstants.INSERT_PLAYER_SEASON_STATS_ERROR, playerId, leagueId, leagueStageId),
							e);
					return false;
				}
			} else {
				logger.info("start to update PlayerSeasonStats, leagueId = " + leagueId + ", leagueStage = " + leagueStageId
						+ ", playerId = " + playerId);
				try {
					playerSeasonStatsDao.update(StatsBuilder.buildPlayerSeasonStatsForUpdate(playerSeasonStats, playerMatchStats));
				} catch (Exception e) {
					logger.error(String.format(ResponseMsgConstants.UPDATE_PLAYER_SEASON_STATS_ERROR, playerId, leagueId, leagueStageId),
							e);
				}
			}
		}
		return true;
	}

}
