package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dao.FollowDao;
import com.renyi.basketball.bussness.dao.LeagueDao;
import com.renyi.basketball.bussness.dao.MatchDao;
import com.renyi.basketball.bussness.dao.PlayerDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.dto.RadarDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.utils.AnalyzeStrings;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

/**
 * Created by flyong86 on 2016/5/8.
 */
@Service
public class TeamServiceImpl extends BaseServiceImpl<Team, Long> implements TeamService {
	protected Logger logger = Logger.getLogger(getClass());
	
	@Resource
	private TeamDao teamDao;
	@Resource
	private PlayerDao playerDao;
	@Resource
	private LeagueDao leagueDao;
	@Resource
	private FollowDao followDao;
	@Resource
	private MatchDao matchDao;
	@Resource
	private MatchStatsService matchStatsService;

	@Resource
	public void setBaseDao(TeamDao teamDao) {
		super.setBaseDao(teamDao);
	}

	@Override
	public List<Match> queryMatchHistory(Long teamId1, Long teamId2, Integer start, Integer size) {
		logger.info("Start to invoke queryMatchHistoryByTeamIds()");
		return matchDao.queryMatchHistoryByTeamIds(teamId1, teamId2, start, size);
	}

	@Override
	public List<Match> queryFutureMatches(Long teamId, Integer start, Integer size) {
		logger.info("Start to invoke queryFutureMatches()");
		return matchDao.queryFutureMatches(teamId, start, size);
	}

	@Override
	public List<Match> queryPassMatches(Long teamId, Integer start, Integer size) {
		logger.info("Start to invoke queryPassMatches()");
		return matchDao.queryPassMatches(teamId, start, size);
	}
	
	@Override
	public void create(Team team) throws BusinessException {
		try {
			team.setCreateTime(new Date());
			team.setUpdateTime(new Date());
			teamDao.insert(team);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("新增球队错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Team queryTeam(Long teamId, Boolean isWithPlayers) throws BusinessException {
		Team team = teamDao.queryTeam(teamId);
		if (isWithPlayers) {
			team.setPlayers(playerDao.queryPlayerListByTeamId(teamId));
		}
		return team;
	}
	
	@Override
	public List<Team> queryTeamByIds(List<Long> ids) throws BusinessException {
		return teamDao.queryByIds(ids);
	}

	@Override
	public int join(Long userId, Long teamId) throws BusinessException {
		Team team = teamDao.find(teamId);
		int record = 0;
		try {
			record = teamDao.queryRecord(userId, teamId);
			if (record == 0) {
				teamDao.create(team, userId);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("加入球队错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
		return record;
	}

	@Override
	public int quit(Long userId, Long teamId) throws BusinessException {
		int record = 0;
		try {
			record = teamDao.queryRecord(userId, teamId);
			if (record >= 1) {
				teamDao.deleteTeam(userId, teamId);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("退出球队错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
		return record;
	}

	@Override
	public void kickout(Long userId, Long teamId) throws BusinessException {
		try {
			teamDao.deleteTeam(userId, teamId);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("踢出队员错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<Team> queryAll() throws BusinessException {
		List<Team> teams = teamDao.queryAll();
		List<Team> teamDtos = new ArrayList<>();
		for (Team team : teams) {
			Team teamDto = new Team();
			try {
				PropertyUtils.copyPropertiesExclude(team, teamDto);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new BusinessException("po转dto失败!", e, ErrorCode.PO_CANNOT_TO_DTO.code());
			}
			teamDtos.add(teamDto);
		}
		return teamDtos;
	}

	@Override
	public int modifyNumber(Long userId, Long teamId, Integer number) throws BusinessException {
		try {
			// 查询原来该号码的球员ID
			List<Integer> orginal = teamDao.queryPlayersByNumber(teamId, number);
			if (null != orginal && !orginal.isEmpty()) {
				// 将原来此号码的球员更新为100号
				teamDao.updatePlayerNum(teamId, number, 100);
			}
			teamDao.updateNumber(userId, teamId, number);
			// 查询状态为比赛中的比赛
			Integer stauts = 20;
			List<Match> matchs = teamDao.queryMatch(teamId, stauts);
			// 把阵容的球衣号码修改
			for (Match match : matchs) {
				MatchStats matchStats = matchStatsService.getMatchStatsFromCache(match.getId());
				if (matchStats == null) {
					continue;
				} else {
					List<Player> displayDtos = matchStats.getLineupPlayers(teamId);
					for (Player displayDto : displayDtos) {
						if (displayDto.getId().equals(userId)) {
							displayDto.setUniformNumber(number);
						}
					}
					List<Player> reserves = new ArrayList<>(matchStats.getPlayers(teamId));
					for (Player playerDto : reserves) {
						if (playerDto.getId().equals(userId)) {
							playerDto.setUniformNumber(number);
						}
					}
				}
			}

			return 1;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("更新队员球衣号码错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public void dissolve(Long teamId) throws BusinessException {
		try {
			teamDao.dissolve(teamId);
			teamDao.delete(teamId);
			teamDao.deleteFollow(teamId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("解散球队错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Page<Team> query(final String keyword, final Boolean isDismissed, Pageable pageable) {
		Page<Team> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Team>>() {
			@Override
			public List<Team> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(keyword)) {
					searchKeyword = "%" + keyword + "%";
				}
				return teamDao.queryTeams(searchKeyword, isDismissed);
			}
		});

		return page;
	}

	@Override
	public List<Team> queryByUser(Long userId) {
		// 从缓存中获取
		// List<Team> list =
		// CachedManager.get(Constants.CACHE_USER_TEAM_PREFIX + userId,
		// List.class);
		List<Team> list = null;
		// if (null == list) {

		// 缓存为null，查询数据库
		List<Team> teams = teamDao.queryByUser(userId);

		list = new ArrayList<>();
		if (null != list) {
			list = new ArrayList<>();
			for (Team team : teams) {
				Team teamDto = new Team();
				try {
					PropertyUtils.copyPropertiesExclude(team, teamDto);
				} catch (Exception e) {
					e.printStackTrace();
				}
				list.add(teamDto);
			}
			// CachedManager.put(Constants.CACHE_USER_TEAM_PREFIX + userId,
			// list);
			// }
		}
		return list;
	}

	@Override
	public List<Team> queryTeamsByLeagueId(Long id) {
		try {
			League league = leagueDao.find(id); // 查询赛事
			String ids = league.getTeams(); // 球队ID字符串
			List<Team> teams = null;
			if (StringUtils.isNotEmpty(ids)) {
				teams = new ArrayList<>();
				List<Long> idList = AnalyzeStrings.analyzeIds(ids);
				for (Long teamId : idList) {
					Team team = teamDao.find(teamId);
					Team teamDto = new Team();
					if (team != null) {
						PropertyUtils.copyPropertiesExclude(team, teamDto, new String[] { "players", "radarDto" });
						teams.add(teamDto);
					}
				}
			}
			return teams;
		} catch (Exception b) {
			logger.error(b);
			throw new BusinessException("查询赛事球队异常", b, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Page<Team> queryByLeaderName(final String name, final Boolean isDel, final Pageable pageable) {
		// 查找出用户

		// final List<User> leader = userDao.queryUserByName(name);
		//
		// Page<Team> page = PaginationUtil.pagedQuery(pageable, new
		// Callable<List<Team>>() {
		// @Override
		// public List<Team> call() throws Exception {
		// List<Team> list = new ArrayList<Team>();
		// if (leader != null) {
		// for (User lead : leader) {
		// Map<String, Object> params = new HashMap<String, Object>();
		// params.put("leader", lead.getId());
		// if (isDel != null) {
		// params.put("isdel", isDel ? true : false);
		// }
		// List<Team> team = teamDao.find(params);
		// list.addAll(team);
		// }
		// }
		// List<Team> teamDots = null;
		// if (null != list) {
		// teamDots = new ArrayList<>();
		// for (Team team : list) {
		// Team teamDto = new Team();
		// PropertyUtils.copyPropertiesExclude(team, teamDto);
		// teamDots.add(teamDto);
		// }
		// }
		// return teamDots;
		// }
		// });
		// List<Team> teamDtos = page.getContent();
		// for (Team team : teamDtos) {
		// // 找出用户
		// User lead = userDao.find(team.getLeader());
		// team.setLeaderName(lead.getNickName());
		// }

		return null;
	}

	@Override
	public void updateTeam(Team team) {
		teamDao.updateTeam(team);
	}

	@Override
	public void follow(Long userId, Long teamId) {
		followDao.insertFollow(userId, teamId, Follow.TEAM_FOLLOW);
	}

	@Override
	public void unfollow(Long userId, Long teamId) {
		followDao.deletefollow(userId, teamId, Follow.TEAM_FOLLOW);
	}

	@Override
	public Integer queryfollow(Long id, Long teamId) {
		List<Follow> list = followDao.queryfollow(id, teamId, Follow.TEAM_FOLLOW);
		return list == null ? 0 : list.size();
	}

	@Override
	public List<Integer> queryPlayersByNumber(Long teamId, Integer num) throws BusinessException {
		try {
			return teamDao.queryPlayersByNumber(teamId, num);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询球员号码出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public void updatePlayerNum(Long teamId, int fromNum, int toNum) throws BusinessException {
		try {
			teamDao.updatePlayerNum(teamId, fromNum, toNum);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("更新球员号码出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<Team> queryMyFocusTeam(Long id) {
		try {
			List<Team> teams = (List<Team>) followDao.queryEntity(id, null, Follow.TEAM_FOLLOW);
			List<Team> teamDtos = null;
			if (teams != null) {
				teamDtos = new ArrayList<>();
				for (Team team : teams) {
					Team teamDto = new Team();
					PropertyUtils.copyPropertiesExclude(team, teamDto, new String[] { "players", "radarDto" });

					/*
					 * int countScore = team.getScore(); int lostscore =
					 * team.getLostscore(); int count = (team.getWin() +
					 * team.getLose())==0?1:team.getWin() + team.getLose();
					 * double avgHelp = team.getHelp()*1.0 / count ; double
					 * avgBackboard = team.getBackboard()*1.0 /count ; double
					 * avgAnError = team.getAnError()*1.0 / count ; avgHelp =
					 * Double.parseDouble(String.format("%.1f",avgHelp));
					 * avgBackboard =
					 * Double.parseDouble(String.format("%.1f",avgBackboard));
					 * avgAnError =
					 * Double.parseDouble(String.format("%.1f",avgAnError));
					 * double avgCountScore = countScore*1.0 /
					 * (count==0?1:count); //double avgLostScore = lostscore*1.0
					 * / (count==0?1:count); double avgCover = team.getCover()
					 * /count; double avgSteals = team.getSteals() /count;
					 * 
					 * teamDto.setCountScore(Double.parseDouble(String.format(
					 * "%.2f",avgCountScore)));
					 * //teamDto.setLostscore(Double.parseDouble(String.format(
					 * "%.2f",avgLostScore)));
					 * teamDto.setAvgHelp(Double.parseDouble(String.format(
					 * "%.1f",avgHelp)));
					 * teamDto.setAvgBackboard(Double.parseDouble(String.format(
					 * "%.1f",avgBackboard)));
					 * teamDto.setAvgAnError(Double.parseDouble(String.format(
					 * "%.1f",avgAnError)));
					 * teamDto.setAvgCover(Double.parseDouble(String.format(
					 * "%.1f",avgCover)));
					 * teamDto.setAvgSteals(Double.parseDouble(String.format(
					 * "%.1f",avgSteals)));
					 */

					teamDtos.add(teamDto);
				}
			}
			return teamDtos;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询我关注的赛事异常", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public int queryRecord(Long id, Long visiting) throws BusinessException {
		try {
			return teamDao.queryRecord(id, visiting);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询数据库出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<Team> queryByUsers(Long id) throws BusinessException {
		try {
			return teamDao.queryByUser(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询数据库出错!", e, ErrorCode.DB_EXCEPTION.code());
		}

	}

	@Override
	public void updateRadar(Long teamId) throws BusinessException {
		Integer jingong = 0;
		Integer fangshou = 0;
		Integer tineng = 0;
		Integer sudu = 0;
		Integer credit = 0;
		Integer stars = 0;
		Integer age = 0;
		float common = 0;
		List<Player> playerDtos = null;
		try {
			playerDtos = null;// FIXME xfenwei playerDao.queryByTeam(teamId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询球队错误!", e, ErrorCode.DB_EXCEPTION.code());
		}
		// 获取team的attr通过计算球队人员的平均值
		RadarDto radarDto = new RadarDto();
		int size = playerDtos.size();
		for (Player playerDto : playerDtos) {
			// FIXME xfenwei 删除了radar
			// if (playerDto.getRadarDto() != null) {
			// common += playerDto.getRadarDto().getCommon();
			// jingong += playerDto.getRadarDto().getJingong();
			// fangshou += playerDto.getRadarDto().getFangshou();
			// tineng += playerDto.getRadarDto().getTineng();
			// sudu += playerDto.getRadarDto().getSudu();
			// credit += playerDto.getRadarDto().getCredit();
			// stars += playerDto.getRadarDto().getStars();
			// age += playerDto.getAge();
			// }
		}
		if (size == 0) {
			size = 1;
		}
		radarDto.setCommon(common / size);
		radarDto.setCredit(credit / size);
		radarDto.setFangshou(fangshou / size);
		radarDto.setJingong(jingong / size);
		radarDto.setStars(stars / size);
		radarDto.setSudu(sudu / size);
		radarDto.setTineng(tineng / size);
		Team team = new Team();
		// team.setAgeGroup(age / size);
		team.setId(teamId);
		try {
			teamDao.update(team);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("更新球队信息出错!", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public int queryNumber(Long userId, Long teamId) {
		return teamDao.queryNumber(userId, teamId);
	}

	@Override
	public List<User> findPlayer(Long id, Boolean includeLeader) {
		return teamDao.findPlayer(id, includeLeader);
	}

	@Override
	public List<Map<Integer, Integer>> queryNoLeader() {
		return teamDao.queryNoLeader();
	}

	@Override
	public void updateNoLeader(Map<Integer, Integer> result) {
		teamDao.updateNoLeader(result);
	}

	@Override
	public List<Team> queryStillNoLead() {
		return teamDao.queryStillNoLead();
	}

	@Override
	public User findOnePlayer(Long teamId) {
		return teamDao.findOnePlayer(teamId);
	}

	@Override
	public void updateLeader(Long teamId, Long userId) {
		teamDao.updateLeader(teamId, userId);
	}

	@Override
	public void updateLeaders(Long userId, Long teamId) {
		teamDao.updateLeaders(userId, teamId);
	}

	@Override
	public void updateUserTeam(Team team, Long originalLeaderId, Long[] playerIds) {
		try {
			if (team != null && playerIds != null) {
				// 删除该球队的所有队员
				// teamDao.delete(team.getId());
				List<Long> userIds = new ArrayList<>();
				for (Long uid : playerIds) {
					userIds.add(uid);
				}
				userIds.add(team.getId());
				userIds.add(originalLeaderId);
				List<Long> removes = new ArrayList<>();
				List<Long> adds = new ArrayList<>();
				List<User> originals = null;// FIXME xfenwei
											// playerDao.queryByTeam(team.getId());
				OUTER: for (User user : originals) {
					boolean isExits = false;
					INNER: for (Long id : userIds) {
						if (user.getId().equals(id)) {
							isExits = true;
							break INNER;
						}
					}

					if (!isExits) {
						removes.add(user.getId());
					}
				}

				OUTER: for (Long id : userIds) {
					boolean isExits = false;
					INNER: for (User user : originals) {
						if (user.getId().equals(id)) {
							isExits = true;
							break INNER;
						}
					}

					if (!isExits) {
						adds.add(id);
					}
				}

				// 移除
				for (Long uid : removes) {
					teamDao.removeUser(uid, team.getId());
				}

				// 添加新球员
				for (Long uid : adds) {
					try {
						teamDao.addUser(uid, team.getId(), 0);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
				}

				// //添加成员
				// for (int i = 0; i < member.length; i++) {
				// teamDao.saveUT(team.getId(), false, member[i]);
				// }
				// teamDao.saveUT(team.getId(), true, team.getLeader());

				// if (null != originalLeaderId &&
				// !team.getLeader().equals(originalLeaderId)) {
				// // 更新队长
				// teamDao.updateUserRole(originalLeaderId, team.getId(), 0);
				// teamDao.updateUserRole(team.getLeader(), team.getId(), 0);
				// }

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void deleteUT(Long teamId) {
		teamDao.delete(teamId);
	}

	@Override
	public List<Team> checkTeam() {
		Date date = new Date();
		List<Team> teams = teamDao.queryThreeDay(date);
		return teams;
	}

	@Override
	protected Class<?> getClazz() {
		return TeamService.class;
	}

	@Override
	public Team queryByName(String name) {
		return teamDao.queryByName(name);
	}
	
}
