package com.renyi.basketball.bussness.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.dao.DynamicDao;
import com.renyi.basketball.bussness.dao.FollowDao;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.UserDao;
import com.renyi.basketball.bussness.dto.MatchStats;
import com.renyi.basketball.bussness.dto.PerformanceDto;
import com.renyi.basketball.bussness.dto.PlayerDto;
import com.renyi.basketball.bussness.dto.RadarDto;
import com.renyi.basketball.bussness.dto.TeamDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.UserDynamic;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Follow;
import com.renyi.basketball.bussness.po.Performance;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.ActionService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;

@Service
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {
	protected Logger logger = Logger.getLogger(getClass());
	@Resource
	private UserDao userDao;
	@Resource
	private TeamDao teamDao;
	@Resource
	private TeamService teamService;
	@Resource
	private FollowDao followDao;
	@Resource
	private DynamicDao dynamicDao;
	@Resource
	private ActionService actionService;
	@Resource
	private MatchStatsService matchStatsService;
	@Resource
	public void setBaseDao(UserDao baseDao) {
		super.setBaseDao(baseDao);
	}
	
	@Override
	public User queryByUsername(String username) {
		return userDao.find("user_name", username);
	}

	@Override
	public User queryByMobile(String mobile) {
		return userDao.find("mobile", mobile);
	}

	@Override
	public List<PlayerDto> queryByTeam(Long teamId) {
		List<User> users = userDao.queryByTeam(teamId);
		List<PlayerDto> players = new ArrayList<>();
		// FIXME xfenwei 删除了Player.radar
//		for (User user : users) {
//			// String radar = userDao.queryRadar(user.getId());//获取用户的属性值
//			RadarDto radarDto = JSON.parseObject(user.getRadar(), RadarDto.class);// 转化成dto对象
//			PlayerDto playerDto = new PlayerDto();
//			try {
//				PropertyUtils.copyPropertiesExclude(user, playerDto);
//			} catch (Exception e) {
//				logger.error(e.getMessage(), e);
//				throw new BusinessException("po转dto失败!", e, ErrorCode.PO_CANNOT_TO_DTO.code());
//			}
//			// if (StringUtils.isEmpty(user.getStars())) {
//			// playerDto.setStars(1);
//			// } else {
//			// playerDto.setStars(user.getStars().length());
//			// }
//			playerDto.setRadarDto(radarDto);
//			Integer number = teamDao.queryNumber(user.getId(), teamId);
//			if (number == null) {
//				number = 0;
//			}
//			playerDto.setUniformNumber(number);
//			players.add(playerDto);
//		}
		return players;
	}

	@Override
	public PlayerDto queryByUser(Long userId) throws BusinessException {

		try {
			PlayerDto playerDto = new PlayerDto();
			User user = userDao.find(userId);

			// 判断是否存在
			if (user == null) {
				throw new BusinessException("球员不存在!", ErrorCode.OBJECT_NOT_EXIST.code());
			}
			PropertyUtils.copyPropertiesExclude(user, playerDto);

			// String radar = userDao.queryRadar(user.getId());//获取用户的属性值
//			RadarDto radarDto = JSON.parseObject(user.getRadar(), RadarDto.class);// 转化成dto对象
			List<Team> teams = teamDao.queryByUser(user.getId());// 获取用户的所有球队
			List<TeamDto> teamDtos = new ArrayList<>();
			for (Team team : teams) {
				// TeamDto teamDto = teamService.getTeamDto(team.getId(), null);
				TeamDto teamDto = new TeamDto();
				PropertyUtils.copyPropertiesExclude(team, teamDto);
				// teamDto.setPlayers(null);
				teamDtos.add(teamDto);
			}
			playerDto.setTeams(teamDtos);
//			playerDto.setRadarDto(radarDto);
			// 设置三个数值(我关注的。我的粉丝。我的发表)
			List<Follow> follows = followDao.queryfollow(userId, null, null);
			List<Follow> fans = followDao.queryfollow(null, userId, Follow.PLAYER_FOLLOW);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			List<UserDynamic> userDynamics = dynamicDao.find(params);
			playerDto.setMyFocus(follows == null ? 0 : follows.size());
			playerDto.setMyFans(fans == null ? 0 : fans.size());
			playerDto.setMyPublish(userDynamics == null ? 0 : userDynamics.size());
			// if (StringUtils.isEmpty(user.getStars())) {
			// playerDto.setStars(1);
			// } else {
			// playerDto.setStars(user.getStars().length());
			// }
//			int score = (user.getTwo_in() == 0 ? 0 : user.getTwo_in() * 2)
//					+ (user.getThree_in() == 0 ? 0 : user.getThree_in() * 3)
//					+ (user.getFreethrow_in() == 0 ? 0 : user.getFreethrow_in() * 1);
//			double two_rate = user.getTwo_in() * 1.0
//					/ ((user.getTwo_in() + user.getTwo_out()) == 0 ? 1 : (user.getTwo_in() + user.getTwo_out()));
//			double three_rate = user.getThree_in() * 1.0 / ((user.getThree_in() + user.getThree_out()) == 0
//					? 1
//					: (user.getThree_in() + user.getThree_out()));
//			double Freethrow_rate = user.getFreethrow_in() * 1.0
//					/ ((user.getFreethrow_in() + user.getFreethrow_out()) == 0
//							? 1
//							: (user.getFreethrow_in() + user.getFreethrow_out()));
//			int count = (user.getWin() + user.getLose()) == 0 ? 1 : (user.getWin() + user.getLose());
//			// FIXME 重构
//			// double avgCover = user.getCover() * 1.0 / count;
//			double avgSteals = user.getSteals() * 1.0 / count;
//			double avgBackboard = user.getRebound() * 1.0 / count;
//			double avgHelp = user.getAssist() * 1.0 / count;
//			int two_three = user.getThree_in() + user.getTwo_in();
//			double avgScore = score * 1.0 / (count == 0 ? 1 : count);
//			double avgAnError = user.getAnError() / count;

//			playerDto.setScoreCount(Double.parseDouble(String.format("%.2f", avgScore)));
//			playerDto.setTwo_rate(Double.parseDouble(String.format("%.2f", two_rate)));
//			playerDto.setThree_rate(Double.parseDouble(String.format("%.2f", three_rate)));
//			playerDto.setFreethrow_rate(Double.parseDouble(String.format("%.2f", Freethrow_rate)));
//			playerDto.setMatches(count);
//			// FIXME 重构
//			// playerDto.setAvgCover(Double.parseDouble(String.format("%.1f",
//			// avgCover)));
//			playerDto.setAvgSteals(Double.parseDouble(String.format("%.1f", avgSteals)));
//			playerDto.setAvgBackboard(Double.parseDouble(String.format("%.1f", avgBackboard)));
//			playerDto.setAvgHelp(Double.parseDouble(String.format("%.1f", avgHelp)));
//			playerDto.setTwo_three(two_three);
//			playerDto.setScore(score);
//			playerDto.setAvgAnError(Double.parseDouble(String.format("%.1f", avgAnError)));
			return playerDto;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("获取球员信息异常!", e, ErrorCode.FAIL.code());
		}
	}

	/**
	 * 查询球员数据.
	 * 
	 * @param userId
	 *            - 用户ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员
	 */
	@Override
	public PlayerDto queryPlayer(Long userId, Long teamId) throws BusinessException {
		try {
			User user = userDao.queryPlayer(userId, teamId);
			if (null == user) {
				return null;
			}
			PlayerDto playerDto = new PlayerDto();
			PropertyUtils.copyPropertiesExclude(user, playerDto);
//			RadarDto radarDto = JSON.parseObject(user.getRadar(), RadarDto.class);// 转化成dto对象
//			playerDto.setRadarDto(radarDto);
			return playerDto;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("获取球员信息错误!", ErrorCode.DB_EXCEPTION.code());
		}
	}

	/**
	 * 查询球员数据.
	 * 
	 * @param userIds
	 *            - 用户ID
	 * @param teamId
	 *            - 球队ID
	 * @return 球员
	 */
	@Override
	public List<PlayerDto> queryPlayerList(List<Long> userIds, Long teamId) throws BusinessException {
		try {
			List<User> users = userDao.queryPlayerList(userIds, teamId);
			if (null == users) {
				return null;
			}
			List<PlayerDto> players = new ArrayList<>();
			Team team = teamDao.find(teamId);
			for (User user : users) {
				PlayerDto playerDto = new PlayerDto();
				PropertyUtils.copyPropertiesExclude(user, playerDto);
//				RadarDto radarDto = JSON.parseObject(user.getRadar(), RadarDto.class);// 转化成dto对象
//				playerDto.setRadarDto(radarDto);
//				playerDto.setPlayTime(user.getPlayingTime());
//				if (team.getLeader().equals(user.getId())) {
//					playerDto.setIsCaptain(1);
//				}
				players.add(playerDto);
			}
			return players;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("获取球员信息错误!", ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public Page<PlayerDto> queryAllPlayerPage(final Pageable pageable, final String keyword, final Integer type) {
		Page<PlayerDto> page = PaginationUtil.pagedQuery(pageable, new Callable<List<PlayerDto>>() {
			@Override
			public List<PlayerDto> call() throws Exception {
				List<User> users = null;
				if (type == 0) {
					// 通过球员姓名查询
					users = userDao.queryUserByName(keyword);
				} else if (type == 1) {
					// 通过球队姓名查询
					users = userDao.queryUserByTeamName(keyword);
				} else if (type == 2) {
					users = userDao.queryUserByMobile(keyword);
				}
				List<PlayerDto> players = new ArrayList<>();
				if (users != null) {
					for (User user : users) {
						PlayerDto playerDto = new PlayerDto();
						PropertyUtils.copyPropertiesExclude(user, playerDto);
						players.add(playerDto);
					}
				}
				return players;
			}
		});
		List<PlayerDto> result = page.getContent();
		if (result != null) {
			for (PlayerDto user : result) {
				List<Team> teams = teamService.queryByUser(user.getId());
				//FIXME xfenwei user.setTeams(teams);
			}
		}
		return page;
	}

	@Override
	public void updateUser(User user) {
		// FIXME xfenwei 删除了Player.radar
//		// 设置速度
//		if (user.getRadar() == null || user.getRadar().equals("")) {
//			float parm;
//			User olderUser = userDao.find(user.getId());
//			Integer weight = olderUser.getWeight();
//			Integer height = olderUser.getHeight();
//			Integer age = olderUser.getAge();
//			RadarDto radarDto = JSON.parseObject(olderUser.getRadar(), RadarDto.class);
//			if (user.getHeight() != null) {
//				height = user.getHeight();
//			}
//			if (user.getWeight() != null) {
//				weight = user.getWeight();
//			}
//			if (user.getAge() != null) {
//				age = user.getAge();
//			}
//			parm = ((float) weight) / (height - 105);
//			if (age >= 0 && age <= 21) {
//				radarDto.setSudu((int) (parm * 80) == 0 ? 50 : (int) (parm * 80));
//			} else if (age >= 22 && age <= 32) {
//				radarDto.setSudu((int) (parm * 100) == 0 ? 50 : (int) (parm * 100));
//			} else if (age >= 33 && age <= 50) {
//				radarDto.setSudu((int) (parm * 80) == 0 ? 50 : (int) (parm * 80));
//			} else {
//				radarDto.setSudu((int) (parm * 60) == 0 ? 50 : (int) (parm * 60));
//			}
//			if (radarDto.getSudu() > 100) {
//				radarDto.setSudu(100);
//			}
//			user.setRadar(JSON.toJSONString(radarDto));
//		}

		userDao.updateUser(user);
	}

	@Override
	public void follow(Long userId, Long focusId) {
		followDao.insertFollow(userId, focusId, Follow.PLAYER_FOLLOW);
	}

	@Override
	public void unfollow(Long userId, Long focusId) {
		followDao.deletefollow(userId, focusId, Follow.PLAYER_FOLLOW);
	}

	@Override
	public Integer queryfollow(Long id, Long focusId) {
		List<Follow> list = followDao.queryfollow(id, focusId, Follow.PLAYER_FOLLOW);
		return list == null ? 0 : list.size();
	}

	@Override
	public List<PlayerDto> queryMyFocusPlayer(Long id) {
		try {
			List<User> users = (List<User>) followDao.queryEntity(id, null, Follow.PLAYER_FOLLOW);
			List<PlayerDto> playerDtos = null;
			if (users != null) {
				playerDtos = new ArrayList<>();
				for (User user : users) {
					PlayerDto playerDto = new PlayerDto();
					PropertyUtils.copyPropertiesExclude(user, playerDto);

//					int score = (user.getTwo_in() == 0 ? 0 : user.getTwo_in() * 2)
//							+ (user.getThree_in() == 0 ? 0 : user.getThree_in() * 3)
//							+ (user.getFreethrow_in() == 0 ? 0 : user.getFreethrow_in() * 1);
//					int count = (user.getWin() + user.getLose()) == 0 ? 1 : (user.getWin() + user.getLose());
					/*
					 * double two_rate = user.getTwo_in()*1.0 / (
					 * (user.getTwo_in() +
					 * user.getTwo_out())==0?1:(user.getTwo_in() +
					 * user.getTwo_out()) ); double three_rate =
					 * user.getThree_in()*1.0 / ( (user.getThree_in() +
					 * user.getThree_out()) == 0?1:(user.getThree_in() +
					 * user.getThree_out()) ); double Freethrow_rate =
					 * user.getFreethrow_in()*1.0 /( (user.getFreethrow_in() +
					 * user.getFreethrow_out()) == 0?1:(user.getFreethrow_in() +
					 * user.getFreethrow_out()) );
					 * 
					 * double avgCover = user.getCover() *1.0 / count; double
					 * avgSteals = user.getSteals() *1.0 /count; double
					 * avgBackboard = user.getBackboard() *1.0 /count; double
					 * avgHelp = user.getHelp() * 1.0 /count; int two_three =
					 * user.getThree_in() + user.getTwo_in(); double avgScore =
					 * score*1.0 / (count==0?1:count); double avgAnError =
					 * user.getAnError() / count;
					 * 
					 * playerDto.setScoreCount(Double.parseDouble(String.format(
					 * "%.2f",avgScore)));
					 * playerDto.setTwo_rate(Double.parseDouble(String.format(
					 * "%.2f",two_rate)));
					 * playerDto.setThree_rate(Double.parseDouble(String.format(
					 * "%.2f",three_rate)));
					 * playerDto.setFreethrow_rate(Double.parseDouble(String.
					 * format("%.2f",Freethrow_rate)));
					 * 
					 * playerDto.setAvgCover(Double.parseDouble(String.format(
					 * "%.1f",avgCover)));
					 * playerDto.setAvgSteals(Double.parseDouble(String.format(
					 * "%.1f",avgSteals)));
					 * playerDto.setAvgBackboard(Double.parseDouble(String.
					 * format("%.1f",avgBackboard)));
					 * playerDto.setAvgHelp(Double.parseDouble(String.format(
					 * "%.1f",avgHelp))); playerDto.setTwo_three(two_three);
					 * 
					 * playerDto.setAvgAnError(Double.parseDouble(String.format(
					 * "%.1f",avgAnError)));
					 */
//					playerDto.setMatches(count);
//					playerDto.setScore(score);
//					playerDtos.add(playerDto);
				}
			}
			return playerDtos;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询我关注的赛事异常", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<PlayerDto> queryMyFans(Long id) {
		try {
			List<Follow> follows = followDao.queryfollow(null, id, Follow.PLAYER_FOLLOW);
			List<PlayerDto> playerDtos = null;
			if (follows != null) {
				playerDtos = new ArrayList<>();
				for (Follow follow : follows) {
					User user = userDao.find(follow.getUserId());
					PlayerDto playerDto = new PlayerDto();
					PropertyUtils.copyPropertiesExclude(user, playerDto);

//					int score = (user.getTwo_in() == 0 ? 0 : user.getTwo_in() * 2)
//							+ (user.getThree_in() == 0 ? 0 : user.getThree_in() * 3)
//							+ (user.getFreethrow_in() == 0 ? 0 : user.getFreethrow_in() * 1);
//					int count = (user.getWin() + user.getLose()) == 0 ? 1 : (user.getWin() + user.getLose());
					/*
					 * double two_rate = user.getTwo_in()*1.0 / (
					 * (user.getTwo_in() +
					 * user.getTwo_out())==0?1:(user.getTwo_in() +
					 * user.getTwo_out()) ); double three_rate =
					 * user.getThree_in()*1.0 / ( (user.getThree_in() +
					 * user.getThree_out()) == 0?1:(user.getThree_in() +
					 * user.getThree_out()) ); double Freethrow_rate =
					 * user.getFreethrow_in()*1.0 /( (user.getFreethrow_in() +
					 * user.getFreethrow_out()) == 0?1:(user.getFreethrow_in() +
					 * user.getFreethrow_out()) );
					 * 
					 * double avgCover = user.getCover() *1.0 / count; double
					 * avgSteals = user.getSteals() *1.0 /count; double
					 * avgBackboard = user.getBackboard() *1.0 /count; double
					 * avgHelp = user.getHelp() * 1.0 /count; int two_three =
					 * user.getThree_in() + user.getTwo_in(); double avgScore =
					 * score*1.0 / (count==0?1:count); double avgAnError =
					 * user.getAnError() / count;
					 * 
					 * playerDto.setScoreCount(Double.parseDouble(String.format(
					 * "%.2f",avgScore)));
					 * playerDto.setTwo_rate(Double.parseDouble(String.format(
					 * "%.2f",two_rate)));
					 * playerDto.setThree_rate(Double.parseDouble(String.format(
					 * "%.2f",three_rate)));
					 * playerDto.setFreethrow_rate(Double.parseDouble(String.
					 * format("%.2f",Freethrow_rate)));
					 * 
					 * playerDto.setAvgCover(Double.parseDouble(String.format(
					 * "%.1f",avgCover)));
					 * playerDto.setAvgSteals(Double.parseDouble(String.format(
					 * "%.1f",avgSteals)));
					 * playerDto.setAvgBackboard(Double.parseDouble(String.
					 * format("%.1f",avgBackboard)));
					 * playerDto.setAvgHelp(Double.parseDouble(String.format(
					 * "%.1f",avgHelp))); playerDto.setTwo_three(two_three);
					 * 
					 * playerDto.setAvgAnError(Double.parseDouble(String.format(
					 * "%.1f",avgAnError)));
					 */
					//playerDto.setMatches(count);
//					playerDto.setScore(score);

					playerDtos.add(playerDto);
				}
			}
			return playerDtos;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询我粉丝异常", e, ErrorCode.DB_EXCEPTION.code());
		}
	}

	@Override
	public List<User> queryByTeam1(Long teamId) {
		return userDao.queryByTeam(teamId);
	}

	@Override
	public PerformanceDto getPerformace(Long matchId, Long teamId, Long userId) {
		PerformanceDto performanceDto = new PerformanceDto();
		Performance performance = new Performance();
		MatchStats matchStats = new MatchStats();
		List<Action> actionDtos = actionService.queryListByMatchId(matchId);
		if (actionDtos != null && actionDtos.size() != 0) {
			matchStats.setActionList(actionDtos);
			// setuAssistsNum(game.getCountByPlayer(userId,
			// GameConstants.ACTION_26));
			//performance.setuFoulNum(matchStats.getCountByPlayer(userId, ActionStatus.TURNOVER));
			// performance.setuSavesNum(game.getCountByPlayer(userId,
			// GameConstants.ACTION_29));
			//performance.setuStealsNum(matchStats.getCountByPlayer(userId, ActionStatus.FREE_THROW_IN));
			// performance.setuShootNum(game.getCountByPlayer(userId,
			// GameConstants.ACTION_33) + game.getCountByPlayer(userId,
			// GameConstants.ACTION_32) + game.getCountByPlayer(userId,
			// GameConstants.ACTION_34) + game.getCountByPlayer(userId,
			// GameConstants.ACTION_25));

			// performance.settAssistsNum(game.getCountByTeam(teamId,
			// GameConstants.ACTION_26));
			//performance.settFoulNum(matchStats.getCountByTeam(teamId, ActionStatus.TURNOVER));
			// performance.settSavesNum(game.getCountByTeam(teamId,
			// GameConstants.ACTION_29));
			//performance.settStealsNum(matchStats.getCountByTeam(teamId, ActionStatus.FREETHROW_IN));
			// performance.settShootNum(game.getCountByTeam(teamId,
			// GameConstants.ACTION_33) + game.getCountByTeam(teamId,
			// GameConstants.ACTION_32) + game.getCountByTeam(teamId,
			// GameConstants.ACTION_34) + game.getCountByTeam(teamId,
			// GameConstants.ACTION_25));
			int tAssistsNum = 0;
			int uAssistsNum = 0;
			for (Action actionDto : actionDtos) {
				// FIXME xfenwei
//				if (actionDto.getTeamId() != null && actionDto.getTeamId().equals(teamId)
//						&& actionDto.getPlayer2() != null) {
//					tAssistsNum++;
//					if (actionDto.getPlayer2().equals(userId)) {
//						uAssistsNum++;
//					}
//				}
			}
			performance.settAssistsNum(tAssistsNum);
			performance.setuAssistsNum(uAssistsNum);
		}
		try {
			PropertyUtils.copyPropertiesExclude(performance, performanceDto);
			User user = userDao.find(userId);
			String avatar = user.getAvatar();
			if (null != avatar && !avatar.startsWith("http://")) {
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
						.getRequest();

				avatar = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
						+ request.getContextPath() + "/" + avatar;
			}
			user.setAvatar(avatar);
			performanceDto.setUser(userDao.find(userId));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException("查询本场表现异常", e, ErrorCode.DB_EXCEPTION.code());
		}
		return performanceDto;
	}

	@Override
	public void updatePlayer(Long id, Integer score, Integer win, Integer lose, Integer help, Integer backboard,
			Integer two_in, Integer two_out, Integer three_in, Integer three_out, Integer Freethrow_in,
			Integer Freethrow_out, Integer cover, Integer steals, Integer anError) {
		userDao.updatePlayer(id, score, win, lose, help, backboard, two_in, two_out, three_in, three_out, Freethrow_in,
				Freethrow_out, cover, steals, anError);
	}

	@Override
	public void updateWin(Long id, int win) {
		userDao.updateWin(id, win);
	}

	@Override
	public void updateLose(Long id, int lose) {
		userDao.updateLose(id, lose);
	}

	@Override
	protected Class<?> getClazz() {
		return UserService.class;
	}

}
