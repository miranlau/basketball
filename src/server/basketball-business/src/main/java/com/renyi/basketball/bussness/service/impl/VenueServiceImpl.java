package com.renyi.basketball.bussness.service.impl;

import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.renyi.basketball.bussness.dao.VenueDao;
import com.renyi.basketball.bussness.po.Venue;
import com.renyi.basketball.bussness.service.VenueService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;
import jersey.repackaged.com.google.common.collect.Lists;

@Service
public class VenueServiceImpl extends BaseServiceImpl<Venue, Long> implements VenueService {
	@Resource
	private VenueDao venueDao;

	@Resource
	public void setBaseDao(VenueDao baseDao) {
		super.setBaseDao(baseDao);
	}

	@Override
	public Page<Venue> findVenuesPager(Pageable pageable, final String name) {
		Page<Venue> page = PaginationUtil.pagedQuery(pageable, new Callable<List<Venue>>() {
			@Override
			public List<Venue> call() throws Exception {
				String searchKeyword = null;
				if (StringUtils.isNotEmpty(name)) {
					searchKeyword = "%" + name + "%";
				}
				List<Venue> result = venueDao.findVenuesPager(searchKeyword);
				return result;
			}
		});
		return page;
	}
	
	public List<Venue> queryByIds(List<Long> venusIds) {
		return Lists.newArrayList();
	}

	@Override
	protected Class<?> getClazz() {
		return VenueService.class;
	}
}
