package com.renyi.basketball.bussness.sms;

import com.guotion.api.RemoteApiAccess;
import com.guotion.api.RemoteApiAccessFactory;
import com.guotion.api.RemoteApiAccessType;
import net.sf.json.JSONObject;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 单元科技
 */
public class CellSMSTransmitter implements SMSTransmitter {

    private String userId;
    private String password;
    final String REQUEST_URL = "http://183.61.109.140:9801/CASServer/SmsAPI/SendMessage.jsp";

    public CellSMSTransmitter(String userId, String password) {
        this.userId = userId;
        this.password = password;
    }

    @Override
    public boolean sendSMS(String mobile, String content) throws Exception {
        RemoteApiAccess remoteApiAccess = RemoteApiAccessFactory.getApiAccess(RemoteApiAccessType.POST_METHOD);
        Map<String,String> requestParams = new HashMap<String, String>();
        requestParams.put("userid", userId);
        requestParams.put("password",password);
        requestParams.put("destnumbers", mobile);
        requestParams.put("msg", content);

        String response = remoteApiAccess.accessApi(REQUEST_URL, requestParams, "UTF-8");
        JSONObject jsonObject = JSONObject.fromObject(response);
        SAXBuilder builder=new SAXBuilder();
        Document doc = builder.build(new InputSource(new StringReader(jsonObject.getString("responseData"))));
        Element element = doc.getRootElement();
        if(!element.getAttributeValue("return").equals("0")){
            throw new Exception(element.getAttributeValue("info"));
        }
        return element.getAttributeValue("return").equals("0");
    }
}
