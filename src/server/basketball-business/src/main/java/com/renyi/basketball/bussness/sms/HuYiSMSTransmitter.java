package com.renyi.basketball.bussness.sms;


import com.guotion.api.RemoteApiAccess;
import com.guotion.api.RemoteApiAccessFactory;
import com.guotion.api.RemoteApiAccessType;
import com.renyi.basketball.bussness.utils.XmlUtil;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 互亿无线
 */
public class HuYiSMSTransmitter implements SMSTransmitter{

    private String account;
    private String password;
    private final String URL = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";

    public HuYiSMSTransmitter(String account,String password){
        this.account = account;
        this.password = password;
    }

    @Override
    public boolean sendSMS(String mobile, String content) throws Exception {
        RemoteApiAccess apiAccess = RemoteApiAccessFactory.getApiAccess(RemoteApiAccessType.POST_METHOD);
        Map<String,String> params = new HashMap<>();
        params.put("account",account);
        params.put("password", password);
        params.put("mobile", mobile);
        params.put("content", content);

        String response = apiAccess.accessApi(URL, params, "UTF-8");
        JSONObject jsonObject = JSONObject.fromObject(response);
        System.out.println(response);
        String code = (String) XmlUtil.get(jsonObject.getString("responseData"), "code");

        if(!code.equals("2")){
            throw new SMSSendFailException((String) XmlUtil.get(jsonObject.getString("responseData"),"msg"));
        }
        return code.equals("2");
    }
}
