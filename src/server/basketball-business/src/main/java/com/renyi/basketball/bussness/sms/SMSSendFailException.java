package com.renyi.basketball.bussness.sms;

/**
 * Created by Administrator on 2015/10/22 0022.
 */
public class SMSSendFailException extends Exception {

    public SMSSendFailException() {
    }

    public SMSSendFailException(String message) {
        super(message);
    }
}
