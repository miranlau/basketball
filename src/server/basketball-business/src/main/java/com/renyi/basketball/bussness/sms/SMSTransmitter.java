package com.renyi.basketball.bussness.sms;

/**
 * Created by Administrator on 2015/10/22 0022.
 */
public interface SMSTransmitter {
    /**
     * 向指定的人发送短信
     * @param mobile 接收者电话号码
     * @param content 内容
     * @return
     */
    boolean sendSMS(String mobile, String content) throws Exception;


}
