package com.renyi.basketball.bussness.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class AnalyzeStrings {
	// 分隔符
	private final static String SEPRETOR = "\\,";

	/**
	 * 解析存放ID的字符串
	 * 
	 * @param ids
	 * @return
	 */
	public static List<Long> analyzeIds(String ids) {
		List<Long> result = new ArrayList<Long>();
		if (StringUtils.isNotEmpty(ids)) {
			String[] idList = ids.split("\\,");
			for (int i = 0; i < idList.length; i++) {
				result.add(Long.valueOf(idList[i]));
			}
		}
		return result;
	}

	/**
	 * 将id合成一个字符串字段
	 * 
	 * @param idList
	 * @return
	 */
	public static String gatherIds(List idList) {
		StringBuffer result = new StringBuffer("");
		String names = "";
		if (idList != null) {
			for (Object id : idList) {
				// 中间用“,”分割
				result.append(id + ",");
			}
			names = String.valueOf(result);
		}
		if (names.endsWith(",")) {
			// 去掉最后一个分隔符
			names = names.substring(0, names.length() - 1);
		}
		return names;
	}

	public static void main(String[] args) {
		System.out.println(analyzeIds("12,21,33,15"));
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		System.out.println(gatherIds(ids));
	}
}
