package com.renyi.basketball.bussness.utils;

import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 验证码
 * @author 肖兆升
 */
public class CaptchaUtil {
	
	/**
	 * 获取验证码
	 */
	public static String code() throws Exception {
		char[] code = new char[] {'0', '1', '2', '3', '4', '5', '6','7', '8', '9'};
		Random random = new Random();
		
		String sRand = "";
		for (int i = 0; i < 4; i++) {
			String rand = String.valueOf(code[random.nextInt(code.length)]);
			sRand += rand;
		}
		return sRand;
	}
	public static String code(int len) {
//		char[] code = new char[] {'0', '1', '2', '3', '4', '5', '6','7', '8', '9', 'a', 'b', 'c', 'd','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		char[] code = new char[] {'0', '1', '2', '3', '4', '5', '6','7', '8', '9'};
		Random random = new Random();

		String sRand = "";
		for (int i = 0; i < len; i++) {
			String rand = String.valueOf(code[random.nextInt(code.length)]);
			sRand += rand;
		}
		return sRand;
	}

}
