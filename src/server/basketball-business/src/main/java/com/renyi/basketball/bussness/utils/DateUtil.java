package com.renyi.basketball.bussness.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 时间操作工具
 * 
 * @author zhengzhi [zheng_zhi@163.com]
 * 
 */
public final class DateUtil {
	private static Logger logger = Logger.getLogger(DateUtil.class);
	
	/**
	 * 以友好的方式显示时间
	 * 
	 * @param time
	 * @return
	 */
	public static String friendTime(Date time) {
		SimpleDateFormat format_date = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
		String ftime = "";
		Calendar cal = Calendar.getInstance();

		// 判断是否是同一天
		String curDate = format_date.format(cal.getTime());
		String paramDate = format_date.format(time);
		if (curDate.equals(paramDate)) {
			int hour = (int) ((cal.getTimeInMillis() - time.getTime()) / 3600000);
			if (hour == 0)
				ftime = Math.max((cal.getTimeInMillis() - time.getTime()) / 60000, 1) + "分钟前";
			else
				ftime = hour + "小时前";
			return ftime;
		}

		long lt = time.getTime() / 86400000;
		long ct = cal.getTimeInMillis() / 86400000;
		int days = (int) (ct - lt);
		if (days == 0) {
			int hour = (int) ((cal.getTimeInMillis() - time.getTime()) / 3600000);
			if (hour == 0)
				ftime = Math.max((cal.getTimeInMillis() - time.getTime()) / 60000, 1) + "分钟前";
			else
				ftime = hour + "小时前";
		} else if (days == 1) {
			ftime = "昨天";
		} else if (days == 2) {
			ftime = "前天";
		} else if (days > 2 && days <= 10) {
			ftime = days + "天前";
		} else if (days > 10) {
			ftime = format_date.format(time);
		}
		return ftime;
	}

	/**
	 * 以友好的方式显示时间
	 * 
	 * @param date
	 * @return
	 */
	public static String friendTime(String date) {
		SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
		Date time;
		try {
			time = format_datetime.parse(date);
		} catch (ParseException e) {
			return "Unknown";
		}

		return friendTime(time);
	}


	/**
	 * 判断两个时间是否为同一天
	 * @param date1
	 * @param date2
     * @return
     */
	public static boolean isSameDate(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);

		boolean isSameYear = cal1.get(Calendar.YEAR) == cal2
				.get(Calendar.YEAR);
		boolean isSameMonth = isSameYear
				&& cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
		boolean isSameDate = isSameMonth
				&& cal1.get(Calendar.DAY_OF_MONTH) == cal2
				.get(Calendar.DAY_OF_MONTH);

		return isSameDate;
	}

	/**
	 * 友好显示时间(昨天，前天，今天，后天，周几)
	 * @param date
	 * @return
     */
	public static String showWeek(Date date) {
		Calendar today = Calendar.getInstance();
		today.setTime(new Date());
		int ty = today.get(Calendar.YEAR);
		int tm = today.get(Calendar.MONTH) + 1;
		int td = today.get(Calendar.DAY_OF_MONTH);
		Calendar compare = Calendar.getInstance();
		compare.setTime(date);
		int cy = compare.get(Calendar.YEAR);
		int cm = compare.get(Calendar.MONTH) + 1;
		int cd = compare.get(Calendar.DAY_OF_MONTH);
		int week = compare.get(Calendar.DAY_OF_WEEK);
		if (ty == cy && cm == tm && Math.abs(cd - td) < 7) {
			if (Math.abs(cd - td) == 0) {
				return "今天";
			} else if ((cd - td) == 1){
				return "明天";
			} else if ((cd - td) == 2) {
				return "后天";
			} else if ((td - cd) == 1) {
				return "昨天";
			} else if ((td - cd) == 2) {
				return "前天";
			} else {
				//显示周几
				return returnWeek(week);
			}
		} else if((tm - cm) == 1 && (compare.getActualMaximum(Calendar.DAY_OF_MONTH) -cd + td)<7) {//月底和月末 特殊判断
			if ((cd - td) == (compare.getActualMaximum(Calendar.DAY_OF_MONTH) - 2)){
				return "前天";
			} else if ((cd - td) == (compare.getActualMaximum(Calendar.DAY_OF_MONTH) - 1)) {
				return "昨天";
			} else {
				//显示周几
				return returnWeek(week);
			}
		}  else if((cm - tm) == 1 && (today.getActualMaximum(Calendar.DAY_OF_MONTH) - td + cd) < 7) {//月底和月末 特殊判断
			if ((td - cd) == (today.getActualMaximum(Calendar.DAY_OF_MONTH) - 2)){
				return "后天";
			} else if ((td - cd) == (today.getActualMaximum(Calendar.DAY_OF_MONTH) - 1)) {
				return "明天";
			} else {
				//显示周几
				return returnWeek(week);
			}
		} else {
			return cy + "-" + cm + "-" + cd;
		}
	}

	public static String dateToString(long currentms,long oldms){
		if(currentms <= oldms){
			return "0'00''";
		}
		long temss = (currentms - oldms) / 1000;
		long mm = temss / 60;
		long ss = temss % 60;
		return mm+"'"+ss+"''";
	}

	public static String changeDateToString(long ms){
		if(ms <= 0){
			return "0'00''";
		}
		long mm = ms / 1000;
		return mm / 60 +"'"+ mm % 60 +"''";
	}

	public static String returnWeek(Integer week){
		if (week == 1) {
			return "星期日";
		}
		if (week == 2) {
			return "星期一";
		}
		if (week == 3) {
			return "星期二";
		}
		if (week == 4) {
			return "星期三";
		}
		if (week == 5) {
			return "星期四";
		}
		if (week == 6) {
			return "星期五";
		} else {
			return "星期六";
		}
	}

	public static String convertTimeStamp(Long timeStamp) {
		Date date=new Date(timeStamp);
		SimpleDateFormat format=new SimpleDateFormat("HH:mm");
		String str=format.format(date);
		return str;
	}
	
	public static String format(String pattern, Date date) {
		if (StringUtils.isBlank(pattern)) {
			logger.error("pattern is invalid");
			return null;
		}
		if (date == null) {
			logger.error("date is invalid");
			return null;
		}
		return new SimpleDateFormat(pattern).format(date);
	}
	
	public static Date format(String pattern, String date) {
		if (StringUtils.isBlank(pattern)) {
			logger.error("pattern is invalid");
			return null;
		}
		if (StringUtils.isBlank(date)) {
			logger.error("date is invalid");
			return null;
		}
		try {
			return new SimpleDateFormat(pattern, Locale.CHINA).parse(date);
		} catch (ParseException e) {
			logger.error("can not convert string to date, date = " + date);
		}
		return null;
	}
	

	public static void main(String[] args) {
//		System.out.println(convertTimeStamp(1460624400l));

		long time = 100000000L;
		System.out.println(changeDateToString(time));
	}
}
