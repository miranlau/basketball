package com.renyi.basketball.bussness.utils;

import java.util.List;

/**
 * Created by Administrator on 2016/5/20 0020.
 */
public class IdCheckUtil {

    /**
     * 判断是否包含.
     * @param source
     * @param dest
     * @return true:包含 false:不包含
     */
    public static boolean isInclude(List<Integer> source, List<Integer> dest) {
        if (null == source || null == dest) {
            return false;
        }

        for (Integer sId : source) {
            for (Integer dId : dest) {
                if (sId.equals(dId)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 判断是否包含ID.
     * @param source 源对象
     * @param dest 比较目标对象
     * @return true:包含 false:不包含
     */
    public static boolean isIdInclude(List<? extends  Object> source, List<? extends  Object> dest) {
        if (source == null || dest == null) {
            return false;
        }

        for (Object srcObj : source) {
            for (Object destObj : dest) {
                Object srcVal = ReflectionUtil.getFieldValue(source, "id");
                Object destVal = ReflectionUtil.getFieldValue(dest, "id");
                if (srcVal != null && destVal != null) {
                    if (srcVal.equals(destVal)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
