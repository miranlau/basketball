package com.renyi.basketball.bussness.utils;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.ClientConfig;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;
import com.magicbeans.push.queue.PushUtil;
import com.renyi.basketball.bussness.po.User;

import java.util.logging.Logger;

import static cn.jpush.api.push.model.notification.PlatformNotification.ALERT;

/**
 * Created by Administrator on 2017/5/24 0024.
 */
public class JpushUtils {

    public static JPushClient jpushClient = new JPushClient("9089bee4e7fc16ef6e241dfa", "adcf964f5eafd706d20f4c11", null, ClientConfig.getInstance());

    public static PushPayload payload;

    public static void sendForMatch(String pushTag) {
        payload = buildPushMatch_all(Audience.alias(pushTag));
        try {
            jpushClient.sendPush(payload);
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
    }

    public static PushPayload buildPushMatch_all(Audience audience) {

        return PushPayload.newBuilder()
                .setAudience(audience)
                .setPlatform(Platform.all())
                .setMessage(Message.newBuilder()
                        .setMsgContent("match")
                        .setTitle("")
                        .addExtra("pushType", "2")
                        .build())
                .build();
    }

}
