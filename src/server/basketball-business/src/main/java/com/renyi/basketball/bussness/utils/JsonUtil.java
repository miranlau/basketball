package com.renyi.basketball.bussness.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * json处理工具类
 * 
 * @author zhangle
 */
@Component
public class JsonUtil {
	private static Logger logger = Logger.getLogger(JsonUtil.class);
	
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final ObjectMapper mapper;

	static {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		mapper = new ObjectMapper();
		mapper.setDateFormat(dateFormat);
	}

	public static String toJson(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			logger.error("Can not convert Java object to json string, object = " + obj.getClass().getName());
			return null;
		}
	}

	public static <T> T toObject(String json, Class<T> clazz) {
		if (StringUtils.isBlank(json)) {
			return null;
		}
		try {
			return mapper.readValue(json, clazz);
		} catch (IOException e) {
			logger.error("Can not convert json string to Java class: " + clazz.getName());
			return null;
		}
	}

	private static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
		return mapper.getTypeFactory().constructParametrizedType(collectionClass, collectionClass, elementClasses);
	}

	public static <T> T toObject(String json, Class<?> collectionClass, Class<?>... elementClasses) {
		if (StringUtils.isBlank(json)) {
			return null;
		}
		try {
			JavaType javaType = getCollectionType(collectionClass, elementClasses);
			return mapper.readValue(json, javaType);
		} catch (IOException e) {
			logger.error("Can not convert json string to Java class");
			return null;
		}
	}
}