package com.renyi.basketball.bussness.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by flyong86 on 2016/5/6.
 */
public class PathPatternMatcher {

    public static boolean isPattern(String str) {
        return str.indexOf(42) != -1 || str.indexOf(63) != -1;
    }

    public static boolean match(String pattern, String str) {
        if(str.startsWith("/") != pattern.startsWith("/")) {
            return false;
        } else {
            List patDirs = tokenizePath(pattern);
            List strDirs = tokenizePath(str);
            int patIdxStart = 0;
            int patIdxEnd = patDirs.size() - 1;
            int strIdxStart = 0;

            int strIdxEnd;
            String i;
            for(strIdxEnd = strDirs.size() - 1; patIdxStart <= patIdxEnd && strIdxStart <= strIdxEnd; ++strIdxStart) {
                i = (String)patDirs.get(patIdxStart);
                if(i.equals("**")) {
                    break;
                }

                if(!matchStrings(i, (String)strDirs.get(strIdxStart))) {
                    return false;
                }

                ++patIdxStart;
            }

            int var16;
            if(strIdxStart > strIdxEnd) {
                for(var16 = patIdxStart; var16 <= patIdxEnd; ++var16) {
                    if(!patDirs.get(var16).equals("**")) {
                        return false;
                    }
                }

                return true;
            } else if(patIdxStart > patIdxEnd) {
                return false;
            } else {
                while(patIdxStart <= patIdxEnd && strIdxStart <= strIdxEnd) {
                    i = (String)patDirs.get(patIdxEnd);
                    if(i.equals("**")) {
                        break;
                    }

                    if(!matchStrings(i, (String)strDirs.get(strIdxEnd))) {
                        return false;
                    }

                    --patIdxEnd;
                    --strIdxEnd;
                }

                if(strIdxStart > strIdxEnd) {
                    for(var16 = patIdxStart; var16 <= patIdxEnd; ++var16) {
                        if(!patDirs.get(var16).equals("**")) {
                            return false;
                        }
                    }

                    return true;
                } else {
                    while(patIdxStart != patIdxEnd && strIdxStart <= strIdxEnd) {
                        var16 = -1;

                        int patLength;
                        for(patLength = patIdxStart + 1; patLength <= patIdxEnd; ++patLength) {
                            if(patDirs.get(patLength).equals("**")) {
                                var16 = patLength;
                                break;
                            }
                        }

                        if(var16 == patIdxStart + 1) {
                            ++patIdxStart;
                        } else {
                            patLength = var16 - patIdxStart - 1;
                            int strLength = strIdxEnd - strIdxStart + 1;
                            int foundIdx = -1;
                            int i1 = 0;

                            label110:
                            while(i1 <= strLength - patLength) {
                                for(int j = 0; j < patLength; ++j) {
                                    String subPat = (String)patDirs.get(patIdxStart + j + 1);
                                    String subStr = (String)strDirs.get(strIdxStart + i1 + j);
                                    if(!matchStrings(subPat, subStr)) {
                                        ++i1;
                                        continue label110;
                                    }
                                }

                                foundIdx = strIdxStart + i1;
                                break;
                            }

                            if(foundIdx == -1) {
                                return false;
                            }

                            patIdxStart = var16;
                            strIdxStart = foundIdx + patLength;
                        }
                    }

                    for(var16 = patIdxStart; var16 <= patIdxEnd; ++var16) {
                        if(!patDirs.get(var16).equals("**")) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }
    }

    private static boolean matchStrings(String pattern, String str) {
        char[] patArr = pattern.toCharArray();
        char[] strArr = str.toCharArray();
        int patIdxStart = 0;
        int patIdxEnd = patArr.length - 1;
        int strIdxStart = 0;
        int strIdxEnd = strArr.length - 1;
        boolean containsStar = false;

        int i;
        for(i = 0; i < patArr.length; ++i) {
            if(patArr[i] == 42) {
                containsStar = true;
                break;
            }
        }

        char ch;
        if(!containsStar) {
            if(patIdxEnd != strIdxEnd) {
                return false;
            } else {
                for(i = 0; i <= patIdxEnd; ++i) {
                    ch = patArr[i];
                    if(ch != 63 && ch != strArr[i]) {
                        return false;
                    }
                }

                return true;
            }
        } else if(patIdxEnd == 0) {
            return true;
        } else {
            while((ch = patArr[patIdxStart]) != 42 && strIdxStart <= strIdxEnd) {
                if(ch != 63 && ch != strArr[strIdxStart]) {
                    return false;
                }

                ++patIdxStart;
                ++strIdxStart;
            }

            if(strIdxStart > strIdxEnd) {
                for(i = patIdxStart; i <= patIdxEnd; ++i) {
                    if(patArr[i] != 42) {
                        return false;
                    }
                }

                return true;
            } else {
                while((ch = patArr[patIdxEnd]) != 42 && strIdxStart <= strIdxEnd) {
                    if(ch != 63 && ch != strArr[strIdxEnd]) {
                        return false;
                    }

                    --patIdxEnd;
                    --strIdxEnd;
                }

                if(strIdxStart > strIdxEnd) {
                    for(i = patIdxStart; i <= patIdxEnd; ++i) {
                        if(patArr[i] != 42) {
                            return false;
                        }
                    }

                    return true;
                } else {
                    while(patIdxStart != patIdxEnd && strIdxStart <= strIdxEnd) {
                        i = -1;

                        int patLength;
                        for(patLength = patIdxStart + 1; patLength <= patIdxEnd; ++patLength) {
                            if(patArr[patLength] == 42) {
                                i = patLength;
                                break;
                            }
                        }

                        if(i == patIdxStart + 1) {
                            ++patIdxStart;
                        } else {
                            patLength = i - patIdxStart - 1;
                            int strLength = strIdxEnd - strIdxStart + 1;
                            int foundIdx = -1;
                            int i1 = 0;

                            label132:
                            while(i1 <= strLength - patLength) {
                                for(int j = 0; j < patLength; ++j) {
                                    ch = patArr[patIdxStart + j + 1];
                                    if(ch != 63 && ch != strArr[strIdxStart + i1 + j]) {
                                        ++i1;
                                        continue label132;
                                    }
                                }

                                foundIdx = strIdxStart + i1;
                                break;
                            }

                            if(foundIdx == -1) {
                                return false;
                            }

                            patIdxStart = i;
                            strIdxStart = foundIdx + patLength;
                        }
                    }

                    for(i = patIdxStart; i <= patIdxEnd; ++i) {
                        if(patArr[i] != 42) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }
    }

    private static List tokenizePath(String path) {
        ArrayList ret = new ArrayList();
        StringTokenizer st = new StringTokenizer(path, "/");

        while(st.hasMoreTokens()) {
            ret.add(st.nextToken());
        }

        return ret;
    }

    public static boolean urlPathMatch(List<String> pathes, String urlPath) {
        Iterator i$ = pathes.iterator();

        boolean f;
        do {
            if(!i$.hasNext()) {
                return false;
            }

            String path = (String)i$.next();
            f = false;
            if(isPattern(path)) {
                f = match(path, urlPath);
            } else {
                f = urlPath.equals(path);
            }
        } while(!f);

        return true;
    }

    public static void main(String[] args) {
        System.out.println("-- " + match("/**/*.jsp", "/e.jsp"));
    }

}
