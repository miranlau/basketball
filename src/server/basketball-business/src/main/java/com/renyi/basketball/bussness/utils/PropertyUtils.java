package com.renyi.basketball.bussness.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PropertyUtils {

	/**
	 * 利用反射实现对象之间属性复制
	 * 
	 * @param from
	 * @param to
	 */
	public static void copyProperties(Object from, Object to) throws Exception {
		copyPropertiesExclude(from, to, null);
	}

	/**
	 * 复制对象属性
	 * 
	 * @param source
	 * @param target
	 * @param excluds
	 *            排除属性列表
	 * @throws Exception
	 */
	public static void copyPropertiesExclude(Object source, Object target, String... excluds) throws Exception {
		List<String> excludesList = null;
		if (excluds != null && excluds.length > 0) {
			excludesList = Arrays.asList(excluds); // 构造列表对象
		}
		List<Method> fromMethods = getDeclaredMethodsWithSupper(source.getClass());
		List<Method> toMethods = getDeclaredMethodsWithSupper(target.getClass());
		Method fromMethod = null, toMethod = null;
		String fromMethodName = null, toMethodName = null;
		for (int i = 0; i < fromMethods.size(); i++) {
			fromMethod = fromMethods.get(i);
			fromMethodName = fromMethod.getName();
			if (!fromMethodName.contains("get"))
				continue;
			// 排除列表检测
			if (excludesList != null && excludesList.contains(fromMethodName.substring(3).toLowerCase())) {
				continue;
			}
			toMethodName = "set" + fromMethodName.substring(3);
			toMethod = findMethodByName(toMethods, toMethodName);
			if (toMethod == null)
				continue;
			Object value = fromMethod.invoke(source, new Object[0]);
			if (value == null)
				continue;
			// 集合类判空处理
			if (value instanceof Collection) {
				Collection newValue = (Collection) value;
				if (newValue.size() <= 0)
					continue;
			}
			toMethod.invoke(target, new Object[]{value});
		}
	}

	/**
	 * 对象属性值复制，仅复制指定名称的属性值
	 * 
	 * @param from
	 * @param to
	 * @param includsArray
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void copyPropertiesInclude(Object from, Object to, String[] includsArray) throws Exception {

		List<String> includesList = null;
		if (includsArray != null && includsArray.length > 0) {
			includesList = Arrays.asList(includsArray); // 构造列表对象
		} else {
			return;
		}
		List<Method> fromMethods = getDeclaredMethodsWithSupper(from.getClass());
		List<Method> toMethods = getDeclaredMethodsWithSupper(to.getClass());
		Method fromMethod = null, toMethod = null;
		String fromMethodName = null, toMethodName = null;
		for (int i = 0; i < fromMethods.size(); i++) {
			fromMethod = fromMethods.get(i);
			fromMethodName = fromMethod.getName();
			if (!fromMethodName.contains("get"))
				continue;
			// 排除列表检测
			String str = fromMethodName.substring(3);
			if (!includesList.contains(str.substring(0, 1).toLowerCase() + str.substring(1))) {
				continue;
			}
			toMethodName = "set" + fromMethodName.substring(3);
			toMethod = findMethodByName(toMethods, toMethodName);
			if (toMethod == null)
				continue;
			Object value = fromMethod.invoke(from, new Object[0]);
			if (value == null)
				continue;
			// 集合类判空处理
			if (value instanceof Collection) {
				Collection newValue = (Collection) value;
				if (newValue.size() <= 0)
					continue;
			}
			toMethod.invoke(to, new Object[]{value});
		}
	}

	/**
	 * 从方法数组中获取指定名称的方法
	 *
	 * @param methods
	 * @param name
	 * @return
	 */
	public static Method findMethodByName(List<Method> methods, String name) {
		for (int j = 0; j < methods.size(); j++) {
			if (methods.get(j).getName().equals(name))
				return methods.get(j);
		}
		return null;
	}

	private static List<Method> getDeclaredMethodsWithSupper(Class<?> clazz) {
		List<Method> list = new ArrayList<Method>();
		for (Method method : clazz.getDeclaredMethods()) {
			list.add(method);
		}
		if (clazz.getSuperclass() != null) {
			list.addAll(getDeclaredMethodsWithSupper(clazz.getSuperclass()));
		}

		return list;
	}
	
}
