package com.renyi.basketball.bussness.utils;

import java.util.Map;

import org.apache.log4j.Logger;

import com.magicbeans.push.bean.DeviceType;
import com.magicbeans.push.bean.Message;
import com.magicbeans.push.bean.NotificationType;
import com.magicbeans.push.queue.PushUtil;
import com.renyi.basketball.bussness.po.User;

public class PushMessageUtil {
	private static Logger logger = Logger.getLogger(PushMessageUtil.class);
	public static final String APP_TYPE = "member";
	public static final String DYNAMIC_COMMENT_TYPE_ = "0";// 动态评论推送
	public static final String MATCH_MANAGE_TYPE_ = "1";// 赛事管家推送
	public static final String TYPE_SYSTEM_MESSAGE = "2"; // 系统消息

	public static void pushMessages(User aceptor, String content, Map<String, String> extend) {
		Message message = new Message();
		// 设备类型
		Integer type = aceptor.getDeviceType();
		if (type != null) {
			if (type == 1) {
				message.setDeviceType(DeviceType.ios);
			} else if (type == 0) {
				message.setDeviceType(DeviceType.android);
			}
		}
		// token
		if (aceptor.getDeviceToken() != null) {
			message.setDeviceId(aceptor.getDeviceToken());
		}
		// 透传信息
		message.setNotificationType(NotificationType.notification_passthrough);
		// 推送内容
		if (content != null) {
			message.setContent(content);
		}
		// 用户类型
		message.setAppType(APP_TYPE);
		// 设置扩展参数
		if (extend != null) {
			message.setExtend(extend);
		}
		try {
			PushUtil.pushMessage(message);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("推动消息失败!", e);
		}

	}
	
}
