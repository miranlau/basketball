package com.renyi.basketball.bussness.utils;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/31 0031.
 */
public class XmlUtil {

    private String rootElementName;
    private StringBuffer xml = new StringBuffer();

    public XmlUtil(){
        rootElementName = "xml";
        createRoot();
    }

    public XmlUtil(String rootElementName){
        this.rootElementName = rootElementName;
        createRoot();
    }

    private StringBuffer createRoot(){
        xml.append("<"+rootElementName+">\n");
        return xml;
    }

    public String endRoot(){
        xml.append("</"+this.rootElementName+">\n");
        return xml.toString();
    }

    public void appendSingleElement(String key,Object value){
        if(value instanceof  Long || value instanceof Integer || value instanceof Double) {
            xml.append("<"+key+">"+value+"</"+key+">\n");
        }else{
            xml.append("<"+key+"><![CDATA["+value+"]]></"+key+">\n");
        }
    }

    public void appendParentElement(String parentNodeName,String childrenNodeString){
        xml.append("<"+parentNodeName+">\n");
        xml.append(childrenNodeString);
        xml.append("</"+parentNodeName+">\n");
    }



    public static Object get(String resource,String key) {
        try{
            SAXBuilder builder=new SAXBuilder();
            Document doc = builder.build(new InputSource(new StringReader(resource)));
            Element element = doc.getRootElement();
            List<Element> children = element.getChildren();
            List<String> values = new ArrayList<String>();
            for (Element childrenElement : children) {
                if(childrenElement.getName().equals(key)){
                    values.add(childrenElement.getValue());
                }
            }
            if(values.size() == 1) {
                return values.get(0);
            }else if(values.size() > 1) {
                return values;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
