package com.renyi.basketball.bussness.test;

import com.magicbeans.push.bean.DeviceType;
import com.magicbeans.push.bean.Message;
import com.magicbeans.push.queue.PushUtil;

/**
 * Created by Administrator on 2016/6/1 0001.
 */
public class PushTest {

    public final static void main(String[] args) {
        Message message = new Message();
        message.setAppType("member");
        message.setDeviceId("1");
        message.setContent("This is test message.");
        message.setDeviceType(DeviceType.android);
        PushUtil.pushMessage(message);
    }
}
