package com.renyi.basketball.bussness.test;

import cn.magicbeans.common.Filter;
import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;
import com.renyi.basketball.bussness.dao.TeamDao;
import com.renyi.basketball.bussness.dao.mapper.TeamMapper;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.utils.SpringUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by flyong86 on 2016/5/13.
 */
public class TeamTester {

    private static Logger logger = Logger.getLogger(TeamTester.class);

    private static boolean initialed = false;

    @Before
    public void setUp() throws Exception {
        if(!initialed) {
            ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
            initialed = true;
        }
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testPagedQueue() {
        TeamService teamService = SpringUtils.getBean(TeamService.class);
        Pageable pageable = new Pageable(1, 20);
        Page page = teamService.query(null, false, pageable);


        logger.info(page);
    }

    @Test
    public void testFilter() {
        Integer teamId = 1;
        UserService userService = SpringUtils.getBean(UserService.class);

        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("name", "AAA"));
        filters.add(Filter.like("name", "AAA"));
        userService.findList(null, null, filters, null);

    }

}
