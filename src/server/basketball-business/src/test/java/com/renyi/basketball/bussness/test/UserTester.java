package com.renyi.basketball.bussness.test;

import cn.magicbeans.pagination.Pageable;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.UserService;
import com.renyi.basketball.bussness.utils.SpringUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by flyong86 on 2016/4/5.
 */
public class UserTester {
    private static Logger logger = Logger.getLogger(UserTester.class);

    private static boolean initialed = false;

    @Before
    public void setUp() throws Exception {
        if(!initialed) {
            ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
            initialed = true;
        }
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testUser() {
        UserService userService = SpringUtils.getBean(UserService.class);
        try {
            User user = userService.find("mobile", "13888888888");
            logger.info("user:" + user);
        } catch (BusinessException e) {
            logger.error(e);
        }
    }

    @Test
    public void testSaveUser() {
        UserService userService = SpringUtils.getBean(UserService.class);
        User user = new User();
//        user.setPhone("13222222222");
//        user.setPassword(DigestUtils.md5Hex("123456"));
        try {
            userService.save(user);

            logger.info("user:" + user);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Test
    public void testPagination() {
        UserService userService = SpringUtils.getBean(UserService.class);

        Pageable pageable = new Pageable(1, 1);

//        Page<UserDto> page = userService.findPage(pageable);
//        for (UserDto user : page.getContent()) {
//            logger.info(user);
//        }
    }

    @Test
    public void testBaseService() {
        UserService userService = SpringUtils.getBean(UserService.class);
        List<User> user = userService.findAll();

        logger.info(user);
    }
}
