package com.renyi.basketball.bussness.dto;

import java.awt.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.renyi.basketball.bussness.utils.JsonUtil;

import jersey.repackaged.com.google.common.collect.Maps;
import net.sf.json.JSONObject;

public class MatchStatsTest {

    @Test
    public void testMatchData() {
	MatchStats matchStats = new MatchStats();
	Map<Integer, QuarterData> matchData = Maps.newHashMap();
	QuarterData q1 = new QuarterData(10, 20);
	QuarterData q2 = new QuarterData(20, 10);
	QuarterData q3 = new QuarterData(11, 22);
	QuarterData q4 = new QuarterData(22, 11);
	Map<Long, Long> homePlayingTime = Maps.newHashMap();
	homePlayingTime.put(1000l, 5555l);
	homePlayingTime.put(1000l, 5555l);
	homePlayingTime.put(1000l, 5555l);
	Map<Long, Long> visitingPlayingTime = Maps.newHashMap();
	visitingPlayingTime.put(1001l, 5556l);
	visitingPlayingTime.put(1001l, 5556l);
	visitingPlayingTime.put(1001l, 5556l);
	
	q1.setHomePlayingTime(homePlayingTime);
	q1.setVisitingPlayingTime(visitingPlayingTime);
	q2.setHomePlayingTime(homePlayingTime);
	q2.setVisitingPlayingTime(visitingPlayingTime);
	q3.setHomePlayingTime(homePlayingTime);
	q3.setVisitingPlayingTime(visitingPlayingTime);
	q4.setHomePlayingTime(homePlayingTime);
	q4.setVisitingPlayingTime(visitingPlayingTime);
	
	Set<Long> homeLineup = new HashSet(Arrays.asList(new Long[]{100l,200l,300l,400l,500l}));
	Set<Long> visitingLineup = new HashSet(Arrays.asList(new Long[]{100l,200l,300l,400l,500l}));
//	q1.setHomeLineup(homeLineup);
//	q1.setVisitingLineup(visitingLineup);
//	q2.setHomeLineup(homeLineup);
//	q2.setVisitingLineup(visitingLineup);
//	q3.setHomeLineup(homeLineup);
//	q3.setVisitingLineup(visitingLineup);
//	q4.setHomeLineup(homeLineup);
//	q4.setVisitingLineup(visitingLineup);
	
	matchData.put(1, q1);
	matchData.put(2, q2);
	matchData.put(3, q3);
	matchData.put(4, q4);
	
	String json = JsonUtil.toJson(matchData);
	System.out.println(json);
	
	Map<Integer, QuarterData> obj = (Map<Integer, QuarterData>)JsonUtil.toObject(json, Map.class, Integer.class, QuarterData.class);
	
	QuarterData qd1 = obj.get(1);
	QuarterData qd2 = obj.get(2);
	
    }

}
