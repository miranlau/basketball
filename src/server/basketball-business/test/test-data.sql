--home player
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小明','中国',1,1);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小李','中国',2,2);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小王','中国',3,3);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小薛','中国',4,4);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小蔡','中国',5,5);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小黄','中国',6,6);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小曾','中国',7,7);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小谢','中国',8,8);
insert into player(team_id,name,nationality,uniform_number,position) values(200810,'小顾','中国',9,9);

--visiting player
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小张','中国',1,1);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小范','中国',2,2);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小陆','中国',3,3);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小曹','中国',4,4);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小戴','中国',5,5);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小郑','中国',6,6);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小白','中国',7,7);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小饶','中国',8,8);
insert into player(team_id,name,nationality,uniform_number,position) values(200811,'小韩','中国',9,9);

--homeStarting: 1,3,4,5,6
--visitingStarting: 7,8,8,10,11