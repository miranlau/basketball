package com.renyi.basketball.mgmt.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.dto.AdvertDto;
import com.renyi.basketball.bussness.dto.Setting;
import com.renyi.basketball.bussness.rmi.SettingService;
import com.renyi.basketball.mgmt.common.Message;

@Controller
@RequestMapping("/advert")
public class AdvertController extends BaseAdminController {

	@Resource
	private SettingService settingService;

	@RequestMapping("/getAdvert")
	public String getAdvert(ModelMap map) {
		Setting setting = (Setting) settingService.get();
		AdvertDto advertDto = setting.getAdvertDto();
		map.put("advert", advertDto);
		return "/advert/advert";
	}
	@RequestMapping("/save")
	@ResponseBody
	public Message save(AdvertDto advertDto) {
		Setting setting = (Setting) settingService.get();
		setting.setAdvertDto(advertDto);
		settingService.save(setting);
		return new Message(Message.Type.success, "保存成功!");
	}

	@Override
	protected Class<?> getClazz() {
		return AdvertController.class;
	}
}
