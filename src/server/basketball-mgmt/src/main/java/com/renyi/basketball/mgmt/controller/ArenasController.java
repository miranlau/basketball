package com.renyi.basketball.mgmt.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.constants.BasketballConstants;
import com.renyi.basketball.bussness.dto.ArenasDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.Arenas;
import com.renyi.basketball.bussness.po.Company;
import com.renyi.basketball.bussness.po.PlaceSchedule;
import com.renyi.basketball.bussness.po.Place;
import com.renyi.basketball.bussness.service.AreaService;
import com.renyi.basketball.bussness.service.ArenasService;
import com.renyi.basketball.bussness.service.CompanyService;
import com.renyi.basketball.bussness.service.PlaceScheduleService;
import com.renyi.basketball.bussness.service.PlacesService;
import com.renyi.basketball.bussness.utils.PropertyUtils;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/arenas")
public class ArenasController extends BaseAdminController {
	@Resource
	private ArenasService arenasService;
	@Resource
	private CompanyService companyService;
	@Resource
	private PlacesService placesService;
	@Resource
	private PlaceScheduleService placeScheduleService;
	@Resource
	private AreaService areaService;

	/**
	 * 去场馆列表页面
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String list() {
		return "/arenas/list";
	}

	/**
	 * 去场馆添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String add() {
		// areaService.f
		return "/arenas/add";
	}

	/**
	 * 球场列表
	 * 
	 * @param request
	 * @param pageable
	 * @param name
	 * @param status
	 * @return
	 */
	@RequestMapping("/arenasList")
	@ResponseBody
	public ViewData arenasList(HttpServletRequest request, Pageable pageable, String name, Integer status) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, "请登录!");
		}
		if (Admin.COMPANY_ADMIN == admin.getType()) {
			return buildFailureJson(ApiConstants.FAIL, "无权限!");
		}
		try {
			Long companyId = null;
			if (Admin.ARENAS_ADMIN == admin.getType()) {
				companyId = admin.getCompanyId();
			}
			List<Integer> chooseStatus = new ArrayList<Integer>();
			if (status == null) {
				chooseStatus.add(0);// 未通过
				chooseStatus.add(1);// 审核通过
				chooseStatus.add(2);// 待审核
			} else {
				chooseStatus.add(status);
			}
			Page<ArenasDto> page = arenasService.query(name, null, chooseStatus, pageable, companyId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", page);
		} catch (BusinessException b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "异常");
		}
	}

	/**
	 * 球馆审核页面
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping("/check")
	public String check(Long id, ModelMap map) {
		if (id == null) {
			map.put("msg", "请选择场馆");
		}
		try {
			Arenas arenas = arenasService.find(id);
			if (arenas == null) {
				map.put("msg", "请选择场馆");
			} else {
				ArenasDto arenasDto = new ArenasDto();
				PropertyUtils.copyProperties(arenas, arenasDto);
				Company company = companyService.find(arenas.getCompanyId());
				arenasDto.setCompanyName(company.getName());
				map.put("arenas", arenasDto);
			}
		} catch (Exception e) {
			map.put("msg", "网络异常");
		}
		return "/arenas/check";
	}

	/**
	 * 审核结果
	 * 
	 * @param id
	 * @param request
	 * @param result
	 * @param judgment
	 * @return
	 */
	@RequestMapping("/checkArenas")
	public String checkLeague(Long id, HttpServletRequest request, Integer result, String judgment, ModelMap map) {
		if (id == null || result == null) {
			map.put("msg", "参数不足，审核失败");
			return "/arenas/list";
		}
		Admin admin = getCurrent(request);
		if (admin == null) {
			return "redirect:/login.jsp";
		}
		if (Admin.SUPER_ADMIN != admin.getType()) {
			map.put("msg", "权限不足");
			return "/arenas/list";
		}
		try {
			Arenas arenas = arenasService.find(id);
			if (result == 1) {// 审核通过---报名中状态
				arenas.setStatus(1);
			} else if (result == 0) {// 审核未通过
				arenas.setStatus(0);
			}
			arenas.setJudgement(judgment);
			arenasService.update(arenas);
			map.put("msg", "审核已处理");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "审核球馆异常");
		}
		return "/arenas/list";
	}

	/**
	 * 添加场馆
	 * 
	 * @param arenas
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping("/save")
	public String save(Arenas arenas, ModelMap map, HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			map.put("msg", "未登录");
		}
		if (!Admin.ARENAS_ADMIN.equals(admin.getType())) {
			map.put("msg", "无权限!");
		} else {
			arenas.setStatus(2);// 待审核
			arenas.setCompanyId(admin.getCompanyId());
			try {
				arenasService.save(arenas);
				map.put("msg", "添加成功");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				map.put("msg", "添加异常");
			}
		}
		return "/arenas/list";
	}

	/**
	 * 跳转编辑界面.
	 * 
	 * @param id
	 */
	@RequestMapping("/edit")
	public String edit(Long id, ModelMap model) {
		Arenas arenas = arenasService.find(id);
		model.put("arenas", arenas);
		return "/arenas/edit";
	}

	/**
	 * 更新场馆.
	 * 
	 * @param arenas
	 * @param model
	 */
	@RequestMapping("/update")
	public String update(Arenas arenas, ModelMap model) {
		Arenas original = arenasService.find(arenas.getId());
		try {
			PropertyUtils.copyPropertiesInclude(arenas, original, new String[] { "name", "icon", "address",
					"telephone_name_1", "telephone_number_1", "longitude", "latitude", "descs", "areaCode" });
			arenasService.update(original);
			model.put("msg", "更新场馆信息成功!");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return "redirect:list";
	}

	/**
	 * 去球馆的场地列表页面
	 * 
	 * @param arenasId
	 * @param map
	 * @return
	 */
	@RequestMapping("/placesList")
	public String placesList(HttpServletRequest request, Long arenasId, ModelMap map) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			map.put("msg", "未登录");
			return "../login.jsp";
		}
		if (Admin.COMPANY_ADMIN.equals(admin.getType())) {
			map.put("msg", "无权限!");
			return "/arenas/list";
		} else {
			List<Place> place = placesService.queryPlaceByArenasId(arenasId);
			Arenas arenas = arenasService.find(arenasId);
			map.put("places", place);
			map.put("arenas", arenas);
		}
		return "/arenas/placesList";
	}

	/**
	 * 去添加场地页面
	 * 
	 * @param arenasId
	 * @param map
	 * @return
	 */
	@RequestMapping("/addPlace")
	public String addPlace(Integer arenasId, ModelMap map) {
		map.put("arenasId", arenasId);
		return "/arenas/addPlace";
	}

	/**
	 * 保存场地
	 * 
	 * @param place
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping("/savePlace")
	public String savePlace(Place place, ModelMap map, HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			map.put("msg", "未登录");
			return "../login.jsp";
		}
		if (!Admin.ARENAS_ADMIN.equals(admin.getType())) {
			map.put("msg", "无权限!");
			return "/arenas/arenasId";
		} else {
			try {
				place.setCompanyId(admin.getCompanyId());
				placesService.save(place);
				map.put("msg", "添加场地成功");
				List<Place> places = placesService.queryPlaceByArenasId(place.getArenasId());
				Arenas arenas = arenasService.find(place.getArenasId());
				map.put("places", places);
				map.put("arenas", arenas);
			} catch (BusinessException e) {
				logger.error(e.getMessage(), e);
				map.put("msg", "添加场地异常");
			}
		}
		return "redirect:placesList?arenasId=" + place.getArenasId();
	}

	/**
	 * 跳转编辑场地
	 * 
	 * @param placeId
	 * @param model
	 */
	@RequestMapping("/editPlace")
	public String editPlace(Long placeId, ModelMap model) {
		Place place = placesService.find(placeId);
		model.put("place", place);
		return "/arenas/editPlace";
	}

	@RequestMapping("/updatePlace")
	public String updatePlace(Place place, ModelMap model) {
		try {
			placesService.update(place);
			model.put("msg", "更新成功!");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			model.put("msg", "更新失败!");
			model.put("place", place);
			return "/arenas/editPlace";
		}

		return "redirect:placesList?arenasId=" + place.getArenasId();
	}

	/**
	 * 删除场地.
	 * 
	 * @param placeId
	 */
	@RequestMapping("/deletePlace")
	@ResponseBody
	public ViewData deletePlace(Long placeId) {
		try {
			placesService.delete(placeId);
			return buildSuccessCodeJson(0, "删除成功!");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(-1, "删除失败!");
		}
	}

	/**
	 * 场地安排页面
	 * 
	 * @param placeId
	 * @param map
	 * @return
	 */
	@RequestMapping("/schedule")
	public String schedule(Long placeId, ModelMap map) {
		map.put("place", placesService.find(placeId));
		return "/arenas/schedule";
	}

	@RequestMapping("/getSchedule")
	@ResponseBody
	public ViewData getSchedule(Date date, Long placeId) {
		try {
			List<PlaceSchedule> schedules = placesService.querySchedule(date, placeId, BasketballConstants.MAX_PLACE_SCHEDULE);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取场地安排成功", schedules);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ApiConstants.FAIL, "获取场地安排异常");
		}
	}

	/**
	 * 根据公司名称获取球场列表
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/getArenasByCompany")
	@ResponseBody
	public ViewData getArenasByCompany(HttpServletRequest request) {
		Admin admin = getCurrent(request);
		Long companyId = null;
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, "请登录!");
		}
		// if (Admin.COMPANY_ADMIN == admin.getType()) {
		companyId = admin.getCompanyId();
		// }
		try {
			List<ArenasDto> arenasDtos = arenasService.queryArenasByCompany(companyId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", arenasDtos);
		} catch (BusinessException b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "异常");

		}
	}

	/**
	 * 保存场地安排
	 * 
	 * @param schedule
	 * @return
	 */
	@RequestMapping("/saveSchedule")
	@ResponseBody
	public ViewData saveSchedule(PlaceSchedule schedule) {
		try {
			placeScheduleService.save(schedule);
			return buildSuccessCodeJson(ApiConstants.SUCCESS, "保存成功");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("保存失败!");
		}
	}

	/**
	 * 删除场馆
	 * 
	 * @param id
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public ViewData delete(Long id) {
		try {
			arenasService.delete(id);
			return buildSuccessCodeJson(0, "删除成功!");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(-1, "删除失败!");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return ArenasController.class;
	}
}
