package com.renyi.basketball.mgmt.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.renyi.basketball.bussness.po.Admin;

public abstract class BaseAdminController extends BaseController {

	protected Logger logger = Logger.getLogger(getClazz());

	/**
	 * 日期类型数据绑定
	 * 
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * 获得当前管理员用户
	 * 
	 * @param request
	 * @return
	 */
	public Admin getCurrent(HttpServletRequest request) {
		Object temp = request.getSession().getAttribute(Admin.SESSION_ADMIN);
		if (temp != null) {
			return (Admin) temp;
		}
		return null;
	}

	protected abstract Class<?> getClazz();
}
