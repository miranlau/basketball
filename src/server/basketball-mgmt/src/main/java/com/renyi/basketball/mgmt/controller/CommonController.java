package com.renyi.basketball.mgmt.controller;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.service.AdminService;
import com.renyi.basketball.mgmt.common.Message;

@RequestMapping("/common")
@Controller
public class CommonController extends BaseAdminController {
	@Resource
	private AdminService adminService;
	
	/**
	 * 登陆                                     zadsfasfa fa df
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	@ResponseBody
	public Message login(String userName, String password, HttpServletRequest request) {
		Admin admin = adminService.findByUserName(userName);
		if (admin == null) {
			return new Message(Message.Type.error, "账号不存在!");
		}
		if (!DigestUtils.md5Hex(password).equals(admin.getPassword())) {
			return new Message(Message.Type.error, "密码错误!");
		}

		if (!admin.getEnabled()) {
			return new Message(Message.Type.error, "该账号未启用!");
		}
		// 将更新前的信息存入SESSION
		// Admin temp = admin.clone();
		HttpSession session = request.getSession();
		session.setAttribute(Admin.SESSION_ADMIN, admin);
		// 更新IP和登陆时间
		admin.setLoginDate(new Date());
		String ip = request.getRemoteHost();
		admin.setLoginIp(ip);

		adminService.update(admin);

		return new Message(Message.Type.success, "登陆成功!");
	}

	/**
	 * 进入首页
	 * 
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		Admin temp = getCurrent(request);
		// if (temp.getType().equals(Admin.Type.superAdmin)){
		return "/common/main";
		// }
		// else if (temp.getType().equals(Admin.Type.company)){
		// return "/common/"
		// }
	}

	@RequestMapping("/nav")
	public String nav() {
		return "/common/index";
	}
	/**
	 * 校验用户名是否存在
	 * 
	 * @param username
	 * @return
	 */
	@RequestMapping("/checkUserName")
	public @ResponseBody boolean checkUserName(String userName) {
		if (StringUtils.isEmpty(userName)) {
			return false;
		}
		if (adminService.userNameExists(userName)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 退出当前登陆
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		// 清空session
		HttpSession session = request.getSession();
		session.removeAttribute(Admin.SESSION_ADMIN);
		// 注销session
		session.invalidate();

		return "redirect:/login.jsp";
	}

	@Override
	protected Class<?> getClazz() {
		return CommonController.class;
	}
}
