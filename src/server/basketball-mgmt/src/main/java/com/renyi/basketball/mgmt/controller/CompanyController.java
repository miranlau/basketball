package com.renyi.basketball.mgmt.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.Area;
import com.renyi.basketball.bussness.po.Company;
import com.renyi.basketball.bussness.service.AdminService;
import com.renyi.basketball.bussness.service.AreaService;
import com.renyi.basketball.bussness.service.CompanyService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/company")
public class CompanyController extends BaseAdminController {
	@Resource
	private CompanyService companyService;
	@Resource
	private AreaService areaService;
	@Resource
	private AdminService adminService;

	/**
	 * 公司列表
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping("/list")
	public String list() {
		return "/company/list";
	}

	@RequestMapping("/companyList")
	@ResponseBody
	public ViewData companyList(Pageable pageable, String name, Integer province) {
		try {
			Page<Company> page = companyService.query(name, province, pageable);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", page);
		} catch (BusinessException b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "异常");
		}
	}

	/**
	 * 前往添加公司页面
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String add() {
		return "/company/add";
	}

	/**
	 * 保存公司
	 * 
	 * @param admin
	 * @param company
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Admin admin, Company company, ModelMap map) {
		if (admin == null || company == null) {
			map.put("msg", "添加失败，请填写完整资料");
			return "/company/add";
		}
		try {
			// 保存公司
			companyService.save(company);
			Long companyId = company.getId();
			// 密码加密
			admin.setPassword(DigestUtils.md5Hex(admin.getPassword()));
			admin.setCompanyId(companyId);
			//admin.setType(Admin.COMPANY_ADMIN);
			// 保存管理员账号
			adminService.save(admin);
			map.put("msg", "添加成功");
		} catch (BusinessException e) {
			logger.error(e);
			map.put("msg", "添加公司异常");
		}

		return "/company/list";
	}

	/**
	 * 获得省份列表
	 * 
	 * @return
	 */
	@RequestMapping("/getProvince")
	@ResponseBody
	public List<Area> getProvince() {
		return areaService.queryAllProvince();
	}

	/**
	 * 查找公司
	 * 
	 * @param companyId
	 * @return
	 */
	@RequestMapping("/queryCompanyById")
	@ResponseBody
	public Company queryCompanyById(Long companyId) {
		return companyService.find(companyId);
	}

	@Override
	protected Class<?> getClazz() {
		return CompanyController.class;
	}
}
