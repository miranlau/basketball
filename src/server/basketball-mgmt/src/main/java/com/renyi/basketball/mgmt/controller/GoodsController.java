package com.renyi.basketball.mgmt.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.dto.ProductDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.InOutProduct;
import com.renyi.basketball.bussness.po.Product;
import com.renyi.basketball.bussness.service.ProductService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/goods")
public class GoodsController extends BaseAdminController {

	@Resource
	private ProductService productService;

	/**
	 * 跳转到商品列表界面
	 * 
	 * @return
	 */
	@RequestMapping("/toList")
	public String toList() {
		return "/goods/list";
	}

	/**
	 * 查询商品列表
	 * 
	 * @param name
	 * @param areansId
	 * @param pageable
	 * @param request
	 * @return
	 */
	@RequestMapping("/list")
	@ResponseBody
	public ViewData list(String name, Long areansId, Pageable pageable, HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, "请登录!");
		}
		try {
			Long companyId = null;
			if (Admin.ARENAS_ADMIN == admin.getType()) {
				companyId = admin.getCompanyId();
			}
			if (areansId != null && areansId.equals(new Integer(-1))) {
				areansId = null;
			}
			Page<ProductDto> page = productService.query(name, pageable, companyId, areansId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", page);
		} catch (BusinessException b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "异常");
		}
	}

	/**
	 * 跳转到增加商品界面
	 * 
	 * @return
	 */
	@RequestMapping("/toAdd")
	public String toAdd(ModelMap map, Integer areansId) {
		map.put("areansId", areansId);
		return "/goods/add";
	}

	/**
	 * 跳转到修改商品界面
	 * 
	 * @return
	 */
	@RequestMapping("/toModify")
	public String toModify(ModelMap map, Long productId) {
		if (productId == null) {
			map.put("msg", "商品Id不能为空");
			return "/goods/list";
		}
		Product product = productService.find(productId);
		if (product == null) {
			map.put("msg", "商品不存在");
			return "/goods/list";
		}
		map.put("product", product);
		return "/goods/modify";
	}
	/**
	 * 保存商品
	 * 
	 * @return
	 */
	@RequestMapping("/saveProduct")
	public String saveProduct(Product product, ModelMap map, Long areansId) {
		try {
			if (product == null) {
				map.put("msg", "未填写商品信息");
				return "/goods/add";
			}
			if (product.getId() != null) {
				productService.update(product);
			} else {
				product.setCreateTime(new Date());
				productService.saveProduct(product, areansId);
			}
		} catch (BusinessException e) {
			map.put("msg", "添加商品异常");
			return "/goods/add";
		}
		return "/goods/list";
	}

	/**
	 * 检查商品名称是否重复
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping("/checkName")
	@ResponseBody
	public boolean checkName(String name) {
		if (name != null || name.equals("")) {
			Map map = new HashMap<String, Object>();
			map.put("name", name);
			List<Product> list = productService.find(map);
			if (list != null && list.size() > 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 商品
	 * 
	 * @param productId
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(Long productId, Long areansId) {
		if (productId != null) {
			productService.deleteOne(productId, areansId);
		}
		return "/goods/list";
	}

	/**
	 * 查询一个
	 * 
	 * @param productId
	 * @return
	 */
	@RequestMapping("/find")
	@ResponseBody
	public ViewData find(Long productId, Long areansId, HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, "请登录!");
		}
		if (productId == null) {
			return buildFailureJson(ApiConstants.FAIL, "产品Id不能为空!");
		}
		Long companyId = null;
		if (Admin.ARENAS_ADMIN == admin.getType()) {
			companyId = admin.getCompanyId();
		}
		if (areansId != null && areansId.equals(new Integer(-1))) {
			areansId = null;
		}
		ProductDto productDto = productService.queryOne(productId, areansId, companyId);
		return buildSuccessJson(ApiConstants.SUCCESS, "成功", productDto);
	}

	/**
	 * 进出货
	 * 
	 * @param inoutproduct
	 * @return
	 */
	@RequestMapping("/inoutproduct")
	public String inoutproduct(InOutProduct inoutproduct, HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return "/goods/list";
		}
		if (inoutproduct == null) {
			return "/goods/list";
		}
		if (inoutproduct.getQuantity() == null) {
			inoutproduct.setQuantity(0);
		}
		inoutproduct.setCrtime(new Date());
		productService.inoutproduct(inoutproduct);
		return "/goods/list";
	}

	@Override
	protected Class<?> getClazz() {
		return GoodsController.class;
	}
}
