package com.renyi.basketball.mgmt.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.LeagueStatus;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.dto.LeagueCountCache;
import com.renyi.basketball.bussness.dto.LeagueInfoDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.LeagueInfo;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.po.Venue;
import com.renyi.basketball.bussness.service.LeagueInfoService;
import com.renyi.basketball.bussness.service.LeagueService;
import com.renyi.basketball.bussness.service.LeagueStageService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.VenueService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import jersey.repackaged.com.google.common.collect.Lists;

@Controller
@RequestMapping("/league")
public class LeagueController extends BaseAdminController {
	@Resource
	private TeamService teamService;
	@Resource
	private LeagueService leagueService;
	@Resource
	private VenueService venueService;
	@Resource
	private MatchService matchService;
	@Resource
	private LeagueInfoService leagueInfoService;
	@Resource
	private LeagueStageService leagueStageService;
	
	/**
	 * 赛事列表
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping("/list")
	public String list() {
		return "/league/list";
	}
	
	/**
	 * 前往添加赛事页面
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String add(ModelMap map) {
		map.put("stageList", leagueStageService.queryAll());
		return "/league/add";
	}
	
	@RequestMapping("/season")
	public String season(ModelMap map, Long id) {
		map.put("leagueId", id);
		return "/season/list";
	}
	
	@RequestMapping("/division")
	public String division(ModelMap map, Long id) {
		map.put("leagueId", id);
		return "/division/list";
	}

	/**
	 * 获取赛事列表分页
	 * 
	 * @param request
	 * @param pageable
	 * @param name
	 * @param status
	 * @return
	 */
	@RequestMapping("/leagueList")
	@ResponseBody
	public ViewData leagueList(HttpServletRequest request, Pageable pageable, String name, Integer status) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, "请登录!");
		}
		if (Admin.ARENAS_ADMIN == admin.getType()) {
			return buildFailureJson(ApiConstants.FAIL, "无权限!");
		}
		try {
			Long companyId = null;
			if (Admin.COMPANY_ADMIN == admin.getType()) {
				companyId = admin.getCompanyId();
			}
			List<Integer> chooseStatus = new ArrayList<Integer>();
			if (status == null) {
				chooseStatus.add(LeagueStatus.end.code());// 比赛结束
				chooseStatus.add(LeagueStatus.gaming.code());// 比赛中
				chooseStatus.add(LeagueStatus.signend.code());// 报名截止
				chooseStatus.add(LeagueStatus.signing.code());// 报名中
				chooseStatus.add(LeagueStatus.not_through.code());// 未通过
				chooseStatus.add(LeagueStatus.wait_through.code());// 待审核
			} else {
				chooseStatus.add(status);
			}
			Page<League> page = leagueService.query(name, chooseStatus, pageable, companyId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", page);
		} catch (Exception b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "异常");
		}

	}

	/**
	 * 添加赛事
	 * 
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(League league, HttpServletRequest request, ModelMap map) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return "redirect:/login.jsp";
		}
		try {
			league.setStatus(LeagueStatus.signing.code());
			leagueService.save(league);
			map.put("msg", "添加赛事成功,等待审核!");
		} catch (BusinessException e) {
			logger.error(e);
			map.put("msg", "添加赛事异常");
		}

		return "/league/list";
	}

	/**
	 * 获得队伍列表
	 * 
	 * @return
	 */
	@RequestMapping("/getTeams")
	@ResponseBody
	public List<Team> getTeams() {
		return teamService.queryAll();
	}
	
	@RequestMapping("/getVenues")
	@ResponseBody
	public List<Venue> getVenues(HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return null;
			}
			if (Admin.COMPANY_ADMIN == admin.getType()) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("company_id", admin.getCompanyId());
				return venueService.find(params);
			} else if (Admin.SUPER_ADMIN == admin.getType()) {
				return venueService.findAll();
			}
			return null;
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * 去详情页面
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping("/detail")
	public String detailPage(Long id, ModelMap map) {
		List<Match> matchs = null;
		League league = null;
		try {
			matchs = matchService.queryLeagueMatch(id);
			league = leagueService.queryById(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		map.put("matches", matchs);
		map.put("league", league);
		return "/league/detail";
	}

	/**
	 * 去统计页面
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping("/count")
	public String countPage(Long id, ModelMap map) {
		try {
			LeagueCountCache leagueCount = leagueService.queryLeagueBang(id);
			if (leagueCount != null) {
				map.put("leagueBang", leagueCount);
			}
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
		}
		return "/league/count";
	}

	/**
	 * 去动态列表页面
	 * 
	 * @return
	 */
	@RequestMapping("/publishList")
	public String publishListPage(Integer leagueId, ModelMap map) {
		map.put("leagueId", leagueId);
		return "/league/publishList";
	}

	/**
	 * 去发布动态页面
	 * 
	 * @return
	 */
	@RequestMapping("/publish")
	public String publishPage(Integer leagueId, ModelMap map) {
		map.put("leagueId", leagueId);
		return "/league/publish";
	}

	/**
	 * 添加动态
	 * 
	 * @param pushImgs
	 * @param content
	 * @param leagueId
	 * @param map
	 * @return
	 */
	@RequestMapping("/saveInfo")
	public String saveInfo(String[] pushImgs, String content, Long leagueId, ModelMap map) {
		if (leagueId == null) {
			map.put("msg", "请选择赛事");
		} else {
			try {
				LeagueInfo info = new LeagueInfo();
				info.setContent(content);
				info.setImgUrls(Arrays.asList(pushImgs));
				info.setLeagueId(leagueId.intValue());
				leagueInfoService.save(info);
				map.put("msg", "发布动态成功");
			} catch (BusinessException e) {
				map.put("msg", "发布动态异常");
				logger.error(e.getMessage(), e);
			}
			map.put("leagueId", leagueId);
		}

		return "/league/publishList";
	}

	/**
	 * 获得赛事动态列表
	 * 
	 * @param leagueId
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/getLeagueInfo")
	@ResponseBody
	public ViewData getLeagueInfo(Long leagueId, Pageable pageable) {
		if (leagueId == null) {
			return buildSuccessCodeJson(ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			Page<LeagueInfoDto> infos = leagueInfoService.queryInfoByLeagueId(leagueId, pageable);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取列表成功", infos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "获取动态列表异常");
		}
	}
	
	/**
	 * 获得赛事
	 * 
	 * @param leagueId
	 * @param pageable
	 * @return
	 */
	@RequestMapping("/getLeague")
	@ResponseBody
	public ViewData getLeague(Long leagueId) {
		if (leagueId == null) {
			return buildFailureJson(ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			League league = leagueService.queryById(leagueId);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取赛事成功", league);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "获取动态列表异常");
		}
	}
	
	/**
	 * 获得所有赛事
	 * 
	 * @return
	 */
	@RequestMapping("/getAll")
	@ResponseBody
	public List<League> getAllLeague() {
		try {
			List<League> leagues = leagueService.findAll();
			return leagues;
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return Lists.newArrayList();
		}
	}

	/**
	 * 去审核赛事页面
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping("/check")
	public String checkPage(Long id, ModelMap map, HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return "redirect:/login.jsp";
		}
		if (Admin.SUPER_ADMIN != admin.getType()) {
			map.put("msg", "权限不足");
			return "/league/list";
		}
		League league = leagueService.queryById(id);
		map.put("league", league);
		return "/league/check";
	}

	/**
	 * 审核结果
	 * 
	 * @param id
	 * @param request
	 * @param result
	 * @param judgment
	 * @return
	 */
	@RequestMapping("/checkLeague")
	public String checkLeague(Long id, HttpServletRequest request, Integer result, String judgment, ModelMap map) {
		if (id == null || result == null) {
			map.put("msg", "参数不足，审核失败");
			return "/league/list";
		}
		Admin admin = getCurrent(request);
		if (admin == null) {
			return "redirect:/login.jsp";
		}
		if (Admin.SUPER_ADMIN != admin.getType()) {
			map.put("msg", "权限不足");
			return "/league/list";
		}
		try {
			League league = leagueService.find(id);
			if (result == 1) {// 审核通过---报名中状态
				league.setStatus(LeagueStatus.signing.code());
			} else if (result == 0) {// 审核未通过
				league.setStatus(LeagueStatus.not_through.code());
			}
			leagueService.update(league);
			map.put("msg", "审核已处理");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "审核赛事异常");
		}
		return "/league/list";
	}

	/**
	 * 删除联赛
	 * 
	 * @param id
	 * @param request
	 * @param map
	 * @return
	 */
	@RequestMapping("/delete")
	public String deleteLeague(Integer id, HttpServletRequest request, ModelMap map) {
		if (id == null) {
			map.put("msg", "请选择您要删除的联赛");
			return "/league/list";
		}
		Admin admin = getCurrent(request);
		if (admin == null) {
			return "redirect:/login.jsp";
		}
		if (!Admin.SUPER_ADMIN.equals(admin.getType())) {
			map.put("msg", "权限不足");
			return "/league/list";
		}
		try {
			leagueService.delete(Long.valueOf(id));
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "删除赛事异常");
		}
		return "/league/list";
	}

	// 去修改赛事页面
	@RequestMapping("/modify")
	public String modifyPage(Long id, ModelMap map) {
		if (id == null) {
			map.put("msg", "请选择您要修改的赛事");
		}
		try {
			map.put("league", leagueService.queryById(id));
			map.put("stageList", leagueStageService.queryAll());
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "服务器异常");
		}
		return "/league/modify";
	}

	/**
	 * 编辑赛事
	 * 
	 * @param league
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String updateLeague(Long[] teams, League league, ModelMap map) {
		try {
			leagueService.update(league);
			map.put("msg", "修改赛事成功");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "修改赛事异常");
		}
		return "/league/list";
	}

	/**
	 * 手动设置赛事状态
	 * 
	 * @return
	 */
	@RequestMapping("/updateStatus")
	public String updateStatus(Long leagueId, Integer status, ModelMap map) {
		try {
			League league = leagueService.find(leagueId);
			if (status == 0) {
				league.setStatus(1);
			} else if (status == 1) {
				league.setStatus(2);
			} else if (status == 2) {
				league.setStatus(3);
			} else if (status == 6) {
				league.setStatus(0);
			}
			
			leagueService.update(league);
			map.put("msg", "修改状态成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "修改状态异常");
		}
		return "/league/list";
	}

	@Override
	protected Class<?> getClazz() {
		return LeagueController.class;
	}
}
