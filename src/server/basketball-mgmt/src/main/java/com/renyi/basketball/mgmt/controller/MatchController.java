package com.renyi.basketball.mgmt.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.IdAnalyzer;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ActionStatus;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.constants.ErrorCode;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Action;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Match;
import com.renyi.basketball.bussness.po.MatchRound;
import com.renyi.basketball.bussness.service.ActionService;
import com.renyi.basketball.bussness.service.LeagueService;
import com.renyi.basketball.bussness.service.MatchRoundService;
import com.renyi.basketball.bussness.service.MatchService;
import com.renyi.basketball.bussness.service.MatchStatsService;
import com.renyi.basketball.bussness.service.TeamService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/match")
public class MatchController extends BaseAdminController {
	@Resource
	private LeagueService leagueService;
	@Resource
	private MatchService matchService;
	@Resource
	private MatchRoundService matchRoundService;
	@Resource
	private ActionService actionService;
	@Resource
	private MatchStatsService matchStatsService;
	@Resource
	private TeamService teamService;

	/**
	 * 去添加比赛页面
	 * 
	 * @param leagueId
	 * @param map
	 * @return
	 */
	@RequestMapping("/addPage")
	public String addPage(ModelMap map) {
		map.put("leagues", leagueService.findAll());
		map.put("matchRoundList", matchRoundService.queryAll());
		return "/match/add";
	}

	/**
	 * 保存比赛
	 * 
	 * @param isSendMessage
	 *            （1代表发送）
	 * @param homeMember
	 * @param visitingMember
	 * @param counter
	 * @param match
	 * @param map
	 * @return
	 */
	@RequestMapping("/save")
	public String save(Match match, Long[] homePlayerIdList, Long[] visitingPlayerIdList, ModelMap map) {
		try {
			League league = leagueService.find(match.getLeagueId());
			if (league == null) {
				map.put("msg", "您选择的联赛不存在");
				return "/match/list";
			}
			match.setLeagueStageId(league.getStageId());
			match.setMatchFormat(league.getMatchFormat());
			match.setQuarterCount(league.getQuarterCount());
			match.setQuarterTime(league.getQuarterTime());
			match.setStatus(ActionStatus.NOT_START);
			match.setHomePlayerIds(IdAnalyzer.toString(homePlayerIdList));
			match.setVisitingPlayerIds(IdAnalyzer.toString(visitingPlayerIdList));
			matchService.save(match);
			map.put("msg", "创建比赛成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "创建比赛异常");
		}
		return "/match/list";
	}

	/**
	 * 去比赛列表页面
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String list() {
		return "/match/list";
	}

	/**
	 * 获得赛事比赛列表分页
	 * 
	 * @param teamName
	 * @param pageable
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryMatches")
	@ResponseBody
	public ViewData queryMatches(HttpServletRequest request, Pageable pageable, String teamName, String leagueName, String start,
			String end) {
		// 判断用户
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FIELD_NOT_NULL, "未登录");
		} else if (admin.getType().equals(Admin.ARENAS_ADMIN)) {
			return buildFailureJson(ApiConstants.FAIL, "未授权");
		}
		try {
			Page<Match> matchDtos = matchService.queryMatchesPage(pageable, teamName, leagueName, start, end);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取比赛列表成功", matchDtos);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取比赛列表异常");
		}
	}

	/**
	 * 查看比赛详情
	 * 
	 * @param matchId
	 * @param map
	 * @return
	 */
	@RequestMapping("/detailPage")
	public String detailPage(Long matchId, ModelMap map) {
		if (matchId == null) {
			map.put("msg", "请选中比赛");
			return "/match/list";
		}
		try {
			Match match = matchService.queryMatchById(matchId);
			map.put("match", match);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
		}
		return "/match/detail";
	}

	/**
	 * 去比赛编辑页面
	 * 
	 * @param matchId
	 * @param map
	 * @return
	 */
	@RequestMapping("/editPage")
	public String editPage(Long matchId, ModelMap map) {
		if (matchId == null) {
			map.put("msg", "请选中比赛");
			return "/match/list";
		}
		try {
			map.put("match", matchService.queryMatchById(matchId));
			map.put("matchRoundList", matchRoundService.queryAll());
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
		}
		return "/match/edit";
	}

	/**
	 * 修改比赛
	 * 
	 * @param homeMember
	 * @param visitingMember
	 * @param counter
	 * @param match
	 * @param map
	 * @return
	 */
	@RequestMapping("/update")
	public String update(Match match, Long[] homePlayerIdList, Long[] visitingPlayerIdList, ModelMap map) {
		try {
			match.setHomePlayerIds(IdAnalyzer.toString(homePlayerIdList));
			match.setVisitingPlayerIds(IdAnalyzer.toString(visitingPlayerIdList));
			matchService.update(match);
			map.put("msg", "修改比赛成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "修改比赛异常");
		}
		return "/match/list";
	}

	/**
	 * 删除比赛
	 * 
	 * @param matchId
	 * @param map
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(Long matchId, ModelMap map) {
		if (matchId == null) {
			map.put("msg", "请选择您要删除的比赛");
		}
		try {
			matchService.delete(matchId);
			map.put("msg", "删除比赛成功");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "删除比赛异常");
		}

		return "/match/list";
	}

	/**
	 * 去比赛数据查看页面
	 * 
	 * @param matchId
	 * @return
	 */
	@RequestMapping("/dataPage")
	public String dataPage(Long matchId, ModelMap map) {
		try {
			Match matchDto = matchService.queryMatchById(matchId);
			map.put("match", matchDto);
			return "/match/dataList";
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查看比赛数据异常");
			return "/match/list";
		}
	}

	/**
	 * 获取比赛数据
	 * 
	 * @param pageable
	 * @param matchId
	 * @param status
	 * @return
	 */
	@RequestMapping("/getAction")
	@ResponseBody
	public ViewData getAction(Pageable pageable, Long matchId, Integer status, Integer action) {
		try {
			Page<Action> page = matchService.queryMatchData(pageable, matchId, status, action);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取比赛数据成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取比赛数据异常");
		}
	}

	/**
	 * 删除比赛某条数据
	 * 
	 * @param actionId
	 * @param matchId
	 * @param map
	 * @return
	 */
	@RequestMapping("/deleteAction")
	public String deleteAction(Long actionId, Long matchId, ModelMap map) {
		if (actionId == null || matchId == null) {
			map.put("msg", "数据异常,无法删除");
		} else {
			try {
				actionService.deleteAction(actionId);
				map.put("msg", "删除数据成功");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				map.put("msg", "删除数据异常");
			}
		}
		Match matchDto = matchService.queryMatchById(matchId);
		map.put("match", matchDto);
		return "/match/dataList";
	}

	/**
	 * 编辑数据
	 * 
	 * @param actionId
	 * @param matchId
	 * @param map
	 * @return
	 */
	@RequestMapping("/editActionPage")
	public String editActionPage(Long actionId, Long matchId, ModelMap map) {
		if (actionId == null || matchId == null) {
			map.put("msg", "数据异常,无法编辑");
			return "/match/dataList";
		} else {
			try {
				Action action = actionService.queryById(actionId);
				map.put("action", action);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				map.put("msg", "获取数据异常");
				return "/match/dataList";
			}
		}
		return "/match/editAction";
	}

	/**
	 * 新增比赛action页面
	 * 
	 * @param matchId
	 * @param map
	 * @return
	 */
	@RequestMapping("/addDataPage")
	public String addDataPage(Long matchId, ModelMap map) {
		if (matchId == null) {
			map.put("msg", "数据异常,无法新增");
			return "/match/dataList";
		} else {
			try {
				// Action action = actionService.queryById(actionId);
				map.put("matchId", matchId);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				map.put("msg", "获取数据异常");
				return "/match/dataList";
			}
		}
		return "/match/addData";
	}

	/**
	 * 新增action数据
	 * 
	 * @param action
	 * @param map
	 * @return
	 */
	@RequestMapping("/addAction")
	public String addAction(Action action, Long player1, Long player2, ModelMap map) {
		try {
			actionService.saveAction(action, player1, player2);
			map.put("msg", "新增数据成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "新增数据异常");
		}
		Match matchDto = matchService.queryMatchById(action.getMatchId());
		map.put("match", matchDto);
		return "/match/dataList";
	}

	/**
	 * 获取比赛的主客队
	 * 
	 * @param matchId
	 * @return
	 */
	@RequestMapping("/getMatchTeam")
	@ResponseBody
	public ViewData getMatchTeam(Long matchId) {
		try {
			Match matchDto = matchService.queryMatchById(matchId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", matchDto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("服务器异常");
		}
	}

	/**
	 * 更新action
	 * 
	 * @param action
	 * @param map
	 * @param player1
	 * @param player2
	 * @return
	 */
	@RequestMapping("/editAction")
	public String editAction(Action action, ModelMap map, Long player1, Long player2) {
		try {
			actionService.updateAction(action, player1, player2);
			map.put("msg", "更新数据成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "更新数据异常");
		}
		Match matchDto = matchService.queryMatchById(action.getMatchId());
		map.put("match", matchDto);
		return "/match/dataList";
	}

	/**
	 * 刷新比赛ction
	 *
	 * @return
	 */
	@RequestMapping("/refresh")
	public String refresh(Long[] matchIds, ModelMap map, Integer type) {
		if (matchIds != null && matchIds.length != 0) {
			matchStatsService.clear(matchIds);
			map.put("msg", "刷新成功");
		}
		if (type != null) {
			return "redirect:/ctl/match/dataPage?matchId=" + matchIds[0];
		}
		return "/match/list";
	}

	@Override
	protected Class<?> getClazz() {
		return MatchController.class;
	}
}
