package com.renyi.basketball.mgmt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.Place;
import com.renyi.basketball.bussness.service.PlacesService;

@Controller
@RequestMapping("/place")
public class PlacesController extends BaseAdminController {
	@Resource
	private PlacesService placesService;

	/**
	 * 查询公司所有场地
	 * 
	 * @return
	 */
	@RequestMapping("/queryPlaceByArenasId")
	@ResponseBody
	public ViewData queryPlaceByArenasId(HttpServletRequest request) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FIELD_NOT_NULL, "用户未登录");
		}
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("company_id", admin.getCompanyId());
			List<Place> place = placesService.find(params);
			return buildSuccessJson(ApiConstants.SUCCESS, "查询场地成功", place);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "查询场地异常");
		}

	}

	@Override
	protected Class<?> getClazz() {
		return PlacesController.class;
	}
}
