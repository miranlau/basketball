package com.renyi.basketball.mgmt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.constants.ResponseMsgConstants;
import com.renyi.basketball.bussness.constants.RequestParams;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.Player;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.service.PlayerService;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.utils.FileDownload;
import com.renyi.basketball.mgmt.util.ExcelReader;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/player")
public class PlayerController extends BaseAdminController {
	private static final String EXCEL_PATH = "download";
	private static final String EXCEL_NAME = "PlayerList.xls";
	private static final String REAL_NAME = "球员导入模板.xls";
	
	@Resource
	private PlayerService playerService;
	@Resource
	private TeamService teamService;
	
	/**
	 * 显示球员列表页面
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String showList() {
		return "/player/list";
	}

	/**
	 * 显示添加球员页面
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String addPage() {
		return "/player/add";
	}
	
	/**
	 * 球显示球员详情页面
	 */
	@RequestMapping("/detailPage")
	public String detailPage(Long playerId, ModelMap map) {
		if (playerId == null) {
			map.put("msg", "请选择您要查看的球员");
			return "/player/list";
		}
		try {
//			PlayerDto playerDto = playerService.queryByUser(playerId);
//			map.put("player", playerDto);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查看球员异常");
		}
		return "/player/detail";
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response) {
		try {
			String contentType = "application/octet-stream";
			FileDownload.download(request, response, EXCEL_NAME, contentType, REAL_NAME, EXCEL_PATH);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/upload", produces = "application/json;charset=UTF-8")
	public void upload(@RequestParam(value = "excel", required = false) MultipartFile file,
			@RequestParam(value = "type", defaultValue = "excel") String type, HttpServletRequest request,
			HttpServletResponse response) {
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			out = response.getWriter();
			List<Player> playerList = ExcelReader.getPlayerList(file.getInputStream());
			if (playerList == null || playerList.isEmpty()) {
				out.print(String.format(ResponseMsgConstants.UPLOAD_PLAYER_ERROR, "There is no player data in Excel"));
			} else {
				// 验证球员的球队名是否合法并设置球队ID
				for (Player player : playerList) {
					if (player.getUniformNumber() == null) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "号码", player.getName()));
						return;
					}
					if (StringUtils.isBlank(player.getName())) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "姓名", player.getName()));
						return;
					}
					if (StringUtils.isBlank(player.getNationality())) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "国籍", player.getName()));
					}
					if (player.getBirthDate() == null) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "出生日期", player.getName()));
					}
					if (player.getPosition() == null) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "位置", player.getName()));
					}
					if (player.getHeight() == null) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "身高", player.getName()));
					}		
					if (player.getWeight() == null) {
						out.print(String.format(ResponseMsgConstants.EXCEL_PLAYER_PARAMETER_REQUIRED, "体重", player.getName()));
					}
					String teamName = player.getTeamName();
					Team team = teamService.queryByName(teamName);
					if (team == null) {
						String error = String.format(ResponseMsgConstants.TEAM_NAME_EXIST, teamName);
						logger.error(error);
						out.print(String.format(ResponseMsgConstants.UPLOAD_PLAYER_ERROR, error));
						return;
					}
					player.setTeamId(team.getId());
				}
				// 批量保存球员
				playerService.saveBatch(playerList);
				out.print(String.format(ResponseMsgConstants.UPLOAD_PLAYER_SUCCESS, "导入成功"));
			}
		} catch (BusinessException e) {
			logger.error("Import player failed, error: " + e.getMessage());
			out.print(String.format(ResponseMsgConstants.UPLOAD_PLAYER_ERROR, e.getMessage()));
		} catch (IOException e) {
			logger.error("Import player failed, error: " + e.getMessage());
			out.print(String.format(ResponseMsgConstants.UPLOAD_PLAYER_ERROR, e.getMessage()));
		} finally {
			out.flush();
			out.close();
		}
	}
	
	/**
	 *
	 * 获取球员列表分页
	 * 
	 * @param request
	 * @param pageable
	 * @param keyword
	 * @param type
	 *            0球员姓名 1球队名称
	 * @return
	 */
	@RequestMapping("/queryPlayerPage")
	@ResponseBody
	public ViewData queryPlayerPage(HttpServletRequest request, Pageable pageable, String keyword, @RequestParam(required = false, defaultValue = "0") Integer type) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, ResponseMsgConstants.LOGIN_REQUIRED);
		}
		if (type == null || type < 0) {
			return buildFailureJson(ApiConstants.FAIL, String.format(ResponseMsgConstants.PARAMETER_REQUIRED, RequestParams.TYPE));
		}
		try {
			Page<Player> page = playerService.queryPlayerPage(pageable, keyword, type);
			return buildSuccessJson(ApiConstants.SUCCESS, "Get player page successfully", page);
		} catch (BusinessException b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "Get player page failed");
		}
	}

	/**
	 * 添加球员
	 * 
	 * @param user
	 * @param isSendMessage
	 * @return
	 */
	@RequestMapping("/save")
	public String save(ModelMap map, Player player,
			@RequestParam(value = "isSendMessage", required = false) String isSendMessage, HttpServletRequest request) {
//		try {
//			String ordingPassword = user.getPassword();
//			if (playerService.queryByMobile(user.getMobile()) != null) {
//				map.put("msg", "手机号码" + user.getMobile() + "已注册，请重新输入!");
//			} else {
				// 设置头像路径
				StringBuffer picURL = new StringBuffer();
				picURL.append(request.getScheme() + "://");
				picURL.append(request.getServerName() + ":");
				picURL.append(request.getServerPort() + "");
				picURL.append(request.getContextPath() + "/");
				picURL.append("resource/img/default_header.png");
//				user.setAvatar(picURL.toString());
//				user.setUserName(user.getMobile());
//				user.setRealName(user.getRealName());
//				user.setMobile(user.getMobile());
//				user.setPassword(DigestUtils.md5Hex(ordingPassword + BasketballConstants.PASSWORD_MD5_PRIFX));
//				playerService.save(user);
//				/**
//				 * 注册环信账号
//				 */
//				Boolean exist = HuanXinUtil.existUser(user.getUserName());
//				if (!exist && user.getUserName() != null && user.getPassword() != null) {
//					HuanXinUtil.addUser(user.getUserName(), user.getUserName(), user.getNickName());
//				}
//				if ("1".equals(isSendMessage)) {
//					// 发送短信息
//					ytxsmsTransmitter.sendSMS(user.getMobile(), new String[]{user.getUserName(), ordingPassword},
//							BasketballConstants.SMSConstant.ADD_PLAYER_TEMPLATE_ID);
//				}
				map.put("msg", "添加球员成功!");
//			}
//		} catch (BusinessException e) {
//			logger.error(e.getMessage(), e);
//			map.put("msg", "添加球员异常!");
//		} catch (SMSSendFailException e) {
//			logger.error(e.getMessage(), e);
//			map.put("msg", "发送短信失败!");
//		}
		return "/player/list";
	}

	/**
	 * 删除球员
	 * 
	 * @param playerId
	 * @param map
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(Integer playerId, ModelMap map) {
		if (playerId == null) {
			map.put("msg", "请选择您要删除的球员");
			return "/player/list";
		}
		try {
			playerService.delete(Long.valueOf(playerId));
			map.put("msg", "删除球员成功");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "删除球员异常");
		}
		return "/player/list";
	}

	/**
	 * 修改球员页面
	 * 
	 * @param playerId
	 * @param map
	 * @return
	 */
	@RequestMapping("/editPage")
	public String editPage(Long playerId, ModelMap map) {
		if (playerId == null) {
			map.put("msg", "请选择您要修改的球员");
			return "/player/list";
		}
		try {
			Player playerDto = playerService.find(playerId);
			map.put("player", playerDto);
			return "/player/edit";
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "异常");
		}
		return "/player/list";
	}

	/**
	 * 编辑球员
	 * 
	 * @param user
	 * @param map
	 * @return
	 */
	@RequestMapping("/edit")
	public String edit(Player player, ModelMap map) {
		try {
			playerService.update(player);
			map.put("msg", "修改成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "修改异常");
		}
		return "/player/list";
	}
	/**
	 * 球员加入球队页面
	 * 
	 * @param playerId
	 * @param map
	 * @return
	 */
	@RequestMapping("/joinPage")
	public String joinPage(Long playerId, ModelMap map) {
		if (playerId == null) {
			map.put("msg", "请选择球员");
			return "/player/list";
		}
		try {
			Player user = playerService.find(playerId);
			map.put("user", user);
			return "/player/join";
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "加入球队异常");
		}
		return "/player/detail";
	}

	/**
	 * 球员加入球队
	 * 
	 * @param playerId
	 * @param team
	 * @param map
	 * @return
	 */
	@RequestMapping("/addPlayer")
	public String addPlayer(Long playerId, Long[] team, ModelMap map) {
		if (team != null && team.length > 0 && playerId != null) {
			for (Long teamId : team) {
				teamService.join(Long.valueOf(playerId), teamId);
			}
			map.put("msg", "加入球队成功");
		} else {
			map.put("msg", "加入球队未成功");
		}
		return "/player/list";
	}

	/**
	 * 获得用户名字
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping("/getUserName")
	@ResponseBody
	public ViewData getUserName(Long userId) {
		try {
			Player user = playerService.find(userId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", user == null ? null : user.getName());
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	@RequestMapping("/getPlayers")
	@ResponseBody
	public ViewData getPlayersByTeamId(Long teamId) {
		try {
			List<Player> players = playerService.queryPlayerListByTeamId(teamId);
			return buildSuccessJson(ApiConstants.SUCCESS, "成功", players);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	protected Class<?> getClazz() {
		return PlayerController.class;
	}
	
}
