package com.renyi.basketball.mgmt.controller;

import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.utils.FileUpload;
import com.renyi.basketball.bussness.utils.UuidUtil;

@Controller
@RequestMapping("/resource")
public class ResourceController extends BaseController {

	@RequestMapping(value = "/upload", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ViewData upload(@RequestParam(value = "type", defaultValue = "other") String type,
			HttpServletRequest request) {

		Calendar ca = Calendar.getInstance();
		if (request instanceof MultipartHttpServletRequest) {
			String url = "";
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
			Map<String, MultipartFile> multipartFileMap = multipartHttpServletRequest.getFileMap();
			if (multipartFileMap != null) {
				for (Map.Entry<String, MultipartFile> entry : multipartFileMap.entrySet()) {
					MultipartFile multipartFile = entry.getValue();

					String filename = multipartFile.getOriginalFilename();
					String filePath = "upload/" + ca.get(Calendar.YEAR) + "/" + ca.get(Calendar.MONTH) + "/"
							+ ca.get(Calendar.DAY_OF_MONTH);
					String resName = FileUpload.fileUp(multipartFile, filePath, UuidUtil.get32UUID());

					StringBuffer picURL = new StringBuffer();
					picURL.append(request.getScheme() + "://");
					picURL.append(request.getServerName() + ":");
					picURL.append(request.getServerPort() + "");
					picURL.append(request.getContextPath() + "/");
					picURL.append(filePath + resName);
					url = picURL.toString();
				}
			}

			return buildSuccessJson(ApiConstants.SUCCESS, "上传成功", url);
		}
		return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.DB_EXCEPTION, "上传失败");

	}

}
