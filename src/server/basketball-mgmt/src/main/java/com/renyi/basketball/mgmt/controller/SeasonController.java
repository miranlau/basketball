package com.renyi.basketball.mgmt.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.IdAnalyzer;
import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.Season;
import com.renyi.basketball.bussness.service.LeagueService;
import com.renyi.basketball.bussness.service.SeasonService;
import com.renyi.basketball.bussness.service.TeamService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/season")
public class SeasonController extends BaseAdminController {
	@Resource
	private SeasonService seasonService;
	@Resource
	private TeamService teamService;
	/**
	 * 赛季列表
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String list(ModelMap map, Long leagueId) {
		map.put("leagueId", leagueId);
		return "/season/list";
	}

	/**
	 * 去赛季添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String addPage(ModelMap map, Long leagueId) {
		map.put("leagueId", leagueId);
		return "/season/add";
	}
	
	/**
	 * 去赛季添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	public String editPage(ModelMap map, Long seasonId) {
		try {
			Season season = seasonService.find(seasonId);
			season.setTeamList(teamService.queryTeamByIds(IdAnalyzer.toList(season.getTeams())));
			map.put("season", season);
			return "/season/edit";
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查询赛季异常");
			return "/season/list";
		}
	}

	/**
	 * 添加赛季
	 * 
	 * @param map
	 * @param season
	 * @return
	 */
	@RequestMapping("/save")
	public String save(ModelMap map, Season season, HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return "redirect:/login.jsp";
			}
			seasonService.save(season);
			map.put("leagueId", season.getLeagueId());
			map.put("msg", "添加赛季成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "添加赛季异常");
		}
		return "/season/list";
	}
	
	/**
	 * 添加赛季
	 * 
	 * @param map
	 * @param season
	 * @return
	 */
	@RequestMapping("/update")
	public String update(ModelMap map, Season season, HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return "redirect:/login.jsp";
			}
			seasonService.update(season);
			map.put("leagueId", season.getLeagueId());
			map.put("msg", "更新赛季成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "更新赛季异常");
		}
		return "/season/list";
	}

	/**
	 * 获取所有赛季分页
	 * 
	 * @return
	 */
	@RequestMapping("/listSeasons")
	@ResponseBody
	public ViewData search(HttpServletRequest request, Pageable pageable, Long leagueId, String name) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return null;
			}
			Page<Season> page = seasonService.findSeasonPage(pageable, leagueId, name);
			return buildSuccessJson(ApiConstants.SUCCESS, "搜索赛季成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("搜索赛季异常");
		}
	}

	/**
	 * 删除赛季
	 * 
	 * @param seasonId
	 * @param map
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(Long seasonId, Long leagueId, ModelMap map) {
		if (seasonId == null) {
			map.put("msg", "参数不足");
			return "/season/list";
		}
		try {
			seasonService.delete(seasonId);
			map.put("leagueId", leagueId);
			map.put("msg", "删除赛季成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "删除赛季异常");
		}
		return "/season/list";
	}

	@Override
	protected Class<?> getClazz() {
		return SeasonController.class;
	}
}
