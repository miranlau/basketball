package com.renyi.basketball.mgmt.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.Team;
import com.renyi.basketball.bussness.service.TeamService;
import com.renyi.basketball.bussness.service.UserService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/team")
public class TeamController extends BaseAdminController {
	@Resource
	private TeamService teamService;
	@Resource
	private UserService userService;
	Logger logger = Logger.getLogger(getClass());

	/**
	 * 去球队列表页面
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String listPage() {
		return "/team/list";
	}
	
	/**
	 * 获得队伍列表
	 * 
	 * @return
	 */
	@RequestMapping("/getTeams")
	@ResponseBody
	public List<Team> getTeams() {
		return teamService.queryAll();
	}

	/**
	 * 获取球队列表分页
	 * 
	 * @param request
	 * @param pageable
	 * @param name
	 * @param isDel
	 *            0全部 1解散 2未解散
	 * @param type
	 *            0队名 1队长
	 * @return
	 */
	@RequestMapping("/teamList")
	@ResponseBody
	public ViewData teamList(HttpServletRequest request, Pageable pageable, String name, Integer type, Integer status) {
		Admin admin = getCurrent(request);
		if (admin == null) {
			return buildFailureJson(ApiConstants.FAIL, "请登录!");
		}
		Boolean isDismissed = null;
		if (status != null && status != 0) {
			isDismissed = (status == 2) ? false : true;
		}
		try {
			Page<Team> page = teamService.query(name, isDismissed, pageable);
			return buildSuccessJson(ApiConstants.SUCCESS, "获得球队列表成功", page);
		} catch (BusinessException b) {
			logger.error(b);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "获取球队列表异常");
		}
	}
	/**
	 * 通过赛事ID查找参赛队伍
	 * 
	 * @param leagueId
	 * @return
	 */
	@RequestMapping("/queryTeamByLeagueId")
	@ResponseBody
	public ViewData queryTeamByLeagueId(Long leagueId) {
		if (leagueId == null) {
			return buildFailureJson(ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			List<Team> teams = teamService.queryTeamsByLeagueId(leagueId);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取参赛球队成功", teams);
		} catch (BusinessException e) {
			logger.error("获取参赛球队异常!", e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获参赛球队异常!");
		}
	}

	/**
	 * 去球队添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/addTeam")
	public String addPage() {
		return "/team/add";
	}

	/**
	 * 添加球队
	 * 
	 * @param map
	 * @param team
	 * @return
	 */
	@RequestMapping("/save")
	public String save(ModelMap map, Team team, HttpServletRequest request) {
		try {
			// 设置默认头像路径
			if (StringUtils.isBlank(team.getLogo())) {
				StringBuffer picURL = new StringBuffer();
				picURL.append(request.getScheme() + "://");
				picURL.append(request.getServerName() + ":");
				picURL.append(request.getServerPort() + "");
				picURL.append(request.getContextPath() + "/");
				picURL.append("resource/img/default_team.png");
				team.setLogo(picURL.toString());
			}

			// 保存
			teamService.save(team);
			map.put("msg", "创建球队成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "创建球队异常");
		}
		return "/team/list";
	}

	/**
	 * 修改球员号码
	 * 
	 * @param playerId
	 * @param ballNum
	 * @return
	 */
	@RequestMapping("/modifyBallNum")
	@ResponseBody
	public ViewData modifyBallNum(Long playerId, Long teamId, Integer ballNum) {
		if (playerId == null || ballNum == null) {
			return buildFailureJson(ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			teamService.modifyNumber(playerId, teamId, ballNum);
			return buildSuccessJson(ApiConstants.SUCCESS, "修改球员号码成功", null);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ApiConstants.FAIL, "修改球员号码异常");
		}
	}

	/**
	 * 球队详情页面
	 * 
	 * @param teamId
	 * @param map
	 * @return
	 */
	@RequestMapping("/detailPage")
	public String detailPage(Long teamId, ModelMap map) {
		if (teamId == null) {
			map.put("msg", "请选择你要查看的球队");
			return "/team/list";
		}
		try {
			Team teamDto = teamService.queryTeam(teamId, true);
			map.put("team", teamDto);
			return "/team/detail";
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查看球队详情异常");
			return "/team/list";
		}
	}

	/**
	 * 去球队编辑页面
	 * 
	 * @param teamId
	 * @param map
	 * @return
	 */
	@RequestMapping("/editPage")
	public String editPage(Long teamId, ModelMap map) {
		if (teamId == null) {
			map.put("msg", "请选择你要修改的球队");
			return "/team/list";
		}
		try {
			Team teamDto = teamService.queryTeam(teamId, true);
			map.put("team", teamDto);
			return "/team/edit";
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查询球队详情异常");
			return "/team/list";
		}
	}

	/**
	 * 修改球队信息
	 * 
	 * @param team
	 * @param member
	 * @param map
	 * @return
	 */
	@RequestMapping("/edit")
	public String edit(Team team, Long[] member, ModelMap map) {
		try {
			teamService.updateTeam(team);
			map.put("msg", "修改成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "修改球队异常");
		}
		return "/team/list";
	}

	/**
	 * 删除球队
	 * 
	 * @param teamId
	 * @param map
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(Long teamId, ModelMap map) {
		if (teamId == null) {
			map.put("msg", "请选择你要删除的球队");
		}
		try {
			teamService.delete(Long.valueOf(teamId));
			teamService.deleteUT(teamId);
			map.put("msg", "删除成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "删除球队异常");
		}
		return "/team/list";
	}

	@Override
	protected Class<?> getClazz() {
		return TeamController.class;
	}
}
