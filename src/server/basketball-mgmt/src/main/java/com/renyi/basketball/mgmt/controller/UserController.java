package com.renyi.basketball.mgmt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.dto.PlayerDto;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.User;
import com.renyi.basketball.bussness.service.UserService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/user")
public class UserController extends BaseAdminController {
	@Resource
	private UserService userService;

	/**
	 * 通过队伍ID查找队员
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping("/queryMemberByTeamId")
	@ResponseBody
	public ViewData queryMemberByTeamId(Long teamId) {
		if (teamId == null) {
			return buildFailureJson(ApiConstants.FIELD_NOT_NULL, "参数不足");
		}
		try {
			List<PlayerDto> players = userService.queryByTeam(teamId);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取球队队员成功", players);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获球队队员异常!");
		}
	}

	/**
	 * 获取所有用户列表
	 * 
	 * @return
	 */
	@RequestMapping("/queryAllUser")
	@ResponseBody
	public ViewData queryAllUser() {
		try {
			List<User> players = userService.findAll();
			return buildSuccessJson(ApiConstants.SUCCESS, "获取所有队员成功", players);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, e.getErrorCode(), "获所有队员异常!");
		}
	}
	
	/**
	 * 检查手机号码是否存在
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping("/mobileExists")
	public @ResponseBody Boolean mobileExists(String mobile) {
		if (mobile == null || "".equals(mobile)) {
			return false;
		}
		User user = userService.queryByMobile(mobile);
		return user == null ? false : true;
	}
	
	/**
	 * 检查手机号码是否存在
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping("/queryUserByMobile")
	@ResponseBody
	public ViewData queryUserByMobile(String mobile) {
		if (mobile == null || "".equals(mobile)) {
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "参数不足");
		}
		try {
			User user = userService.queryByMobile(mobile);
			return buildSuccessJson(ApiConstants.SUCCESS, "查询用户成功", user);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureJson(ViewData.FlagEnum.ERROR, ApiConstants.FAIL, "查询用户失败");
		}
	}

	@Override
	protected Class<?> getClazz() {
		return UserController.class;
	}
}
