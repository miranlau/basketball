package com.renyi.basketball.mgmt.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.renyi.basketball.bussness.common.ViewData;
import com.renyi.basketball.bussness.constants.ApiConstants;
import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.po.League;
import com.renyi.basketball.bussness.po.Venue;
import com.renyi.basketball.bussness.service.LeagueService;
import com.renyi.basketball.bussness.service.VenueService;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;

@Controller
@RequestMapping("/venue")
public class VenueController extends BaseAdminController {
	@Resource
	private VenueService venueService;
	@Resource
	private LeagueService leagueService;
	/**
	 * 场地列表
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String list() {
		return "/venue/list";
	}

	/**
	 * 去场地添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String addPage() {
		return "/venue/add";
	}
	
	/**
	 * 去场地添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	public String editPage(ModelMap map, Long venueId) {
		try {
			Venue venue = venueService.find(venueId);
			map.put("venue", venue);
			return "/venue/edit";
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "查询场地异常");
			return "/venue/list";
		}
	}

	/**
	 * 添加场地
	 * 
	 * @param map
	 * @param venue
	 * @return
	 */
	@RequestMapping("/save")
	public String save(ModelMap map, Venue venue, HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return "redirect:/login.jsp";
			}
			venueService.save(venue);
			map.put("msg", "添加场地成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "添加场地异常");
		}
		return "/venue/list";
	}
	
	/**
	 * 添加场地
	 * 
	 * @param map
	 * @param venue
	 * @return
	 */
	@RequestMapping("/update")
	public String update(ModelMap map, Venue venue, HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return "redirect:/login.jsp";
			}
			venueService.update(venue);
			map.put("msg", "更新场地成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "更新场地异常");
		}
		return "/venue/list";
	}

	/**
	 * 获取所有场地
	 * 
	 * @return
	 */
	@RequestMapping("/getAllVenues")
	@ResponseBody
	public List<Venue> getAllVenues(HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return null;
			}
			return venueService.findAll();
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	/**
	 * 获取所有场地分页
	 * 
	 * @return
	 */
	@RequestMapping("/getAllVenuesPager")
	@ResponseBody
	public ViewData getAllVenuesPager(String name, Pageable pageable, HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return null;
			}
			Page<Venue> page = venueService.findVenuesPager(pageable, name);
			return buildSuccessJson(ApiConstants.SUCCESS, "获取场地列表成功", page);
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取场地列表异常");
		}
	}

	/**
	 * 获取联赛可选场地
	 * 
	 * @return
	 */
	@RequestMapping("/getLeagueVenues")
	@ResponseBody
	public ViewData getLeagueVenues(Long leagueId, HttpServletRequest request) {
		try {
			Admin admin = getCurrent(request);
			if (admin == null) {
				return null;
			}
			League league = leagueService.find(leagueId);
			if (league != null) {
//				List<Long> ids = league.getVenueIdS();
//				if (ids != null && ids.size() != 0) {
//					List<Venue> venues = new ArrayList<Venue>();
//					for (Long id : ids) {
//						Venue venue = venueService.find(id);
//						if (venue != null) {
//							venues.add(venue);
//						}
//					}
//					return buildSuccessJson(ApiConstants.SUCCESS, "获取场地列表成功", venues);
//				}
			}
			return null;
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			return buildFailureMessage("获取场地异常");
		}
	}

	/**
	 * 删除场地
	 * 
	 * @param venueId
	 * @param map
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(Long venueId, ModelMap map) {
		if (venueId == null) {
			map.put("msg", "参数不足");
			return "/venue/list";
		}
		try {
			venueService.delete(venueId);
			map.put("msg", "删除场地成功");
		} catch (BusinessException e) {
			logger.error(e.getMessage(), e);
			map.put("msg", "删除场地异常");
		}
		return "/venue/list";
	}

	@Override
	protected Class<?> getClazz() {
		return VenueController.class;
	}
}
