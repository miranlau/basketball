package com.renyi.basketball.mgmt.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.renyi.basketball.bussness.dto.Setting;
import com.renyi.basketball.bussness.dto.Version;
import com.renyi.basketball.bussness.po.Admin;
import com.renyi.basketball.bussness.rmi.SettingService;

@Controller
@RequestMapping("/version")
public class VersionController extends BaseAdminController {
	@Resource
	private SettingService settingService;
	// 去安卓更新页面 0
	@RequestMapping("/androidPage")
	public String androidPage(HttpServletRequest request, ModelMap map) {
		Admin admin = getCurrent(request);
		if (admin == null || !admin.getType().equals(Admin.SUPER_ADMIN)) {
			// map.put("msg","权限不足");
			return "redirect:/login.jsp";
		}
		Setting setting = (Setting) settingService.get();
		Version android = setting.getVersion(0);
		map.put("old", android);
		return "/version/androidPage";
	}

	// 去IOS更新页面 1
	@RequestMapping("/iosPage")
	public String iosPage(HttpServletRequest request, ModelMap map) {
		Admin admin = getCurrent(request);
		if (admin == null || !admin.getType().equals(Admin.SUPER_ADMIN)) {
			// map.put("msg","权限不足");
			return "redirect:/login.jsp";
		}
		Setting setting = (Setting) settingService.get();
		Version ios = setting.getVersion(1);
		map.put("old", ios);
		return "/version/iosPage";
	}
	/**
	 * 上传更新版本
	 * 
	 * @param URL
	 *            下载地址
	 * @param version
	 *            版本号
	 * @param code
	 *            版本code
	 * @param limitCode
	 *            最低版本code
	 * @param type
	 *            类型（0安卓，1苹果）
	 * @return
	 */
	@RequestMapping("/update")
	public String updateVersion(HttpServletRequest request, String URL, String version, Integer code, Integer limitCode,
			Integer type, ModelMap map) {
		Admin admin = getCurrent(request);
		if (admin == null || !admin.getType().equals(Admin.SUPER_ADMIN)) {
			return "redirect:/login.jsp";
		}
		if (StringUtils.isEmpty(version) || code == null || limitCode == null || URL == null) {
			map.put("msg", "请填写完整信息");
			return "/version/androidPage";
		}
		if (type == 0) {
			Setting setting = (Setting) settingService.get();
			Version android = new Version();
			android.setCode(limitCode);
			android.setDownloadURL(URL);
			android.setMinCode(limitCode);
			android.setName("android");
			android.setVersionNumber(version);
			setting.setVersion(0, android);
			settingService.save(setting);
			map.put("msg", "更新安卓成功");
			return "forward:androidPage";
		} else {
			Setting setting = (Setting) settingService.get();
			Version ios = new Version();
			ios.setCode(limitCode);
			ios.setDownloadURL(URL);
			ios.setMinCode(limitCode);
			ios.setName("ios");
			ios.setVersionNumber(version);
			setting.setVersion(1, ios);
			settingService.save(setting);
			map.put("msg", "更新ios成功");
			return "forward:iosPage";
		}
	}

	@Override
	protected Class<?> getClazz() {
		return VersionController.class;
	}

}
