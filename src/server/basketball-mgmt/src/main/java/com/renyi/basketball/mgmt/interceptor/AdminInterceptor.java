package com.renyi.basketball.mgmt.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.renyi.basketball.bussness.po.Admin;

public class AdminInterceptor extends HandlerInterceptorAdapter {

	/** 默认登录URL */
	private static final String DEFAULT_LOGIN_URL = "/login.jsp";

	/** 登录URL */
	private String loginUrl = DEFAULT_LOGIN_URL;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HttpSession session = request.getSession();
		Object temp = session.getAttribute(Admin.SESSION_ADMIN);

		String path = request.getRequestURI();
		if (path.contains("/common/login")) {
			return true;
		}
		if (null != temp) {
			return true;
		} else {
			String requestType = request.getHeader("X-Requested-With");
			if (requestType != null && requestType.equalsIgnoreCase("XMLHttpRequest")) {
				response.addHeader("loginStatus", "accessDenied");
				response.sendError(HttpServletResponse.SC_FORBIDDEN);
				return false;
			} else {
				response.sendRedirect(request.getContextPath() + loginUrl);
				return false;
			}
		}
	}

}
