package com.renyi.basketball.mgmt.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellType;

import com.renyi.basketball.bussness.exception.BusinessException;
import com.renyi.basketball.bussness.po.Player;

/**
 * 操作Excel表格的功能类
 */
public class ExcelReader {
	private static POIFSFileSystem fs;
	private static HSSFWorkbook wb;
	private static HSSFSheet sheet;
	private static HSSFRow row;

	/**
	 * 读取Excel表格表头的内容
	 * 
	 * @param is
	 * @return String 表头内容的数组
	 */
	public String[] readExcelTitle(InputStream is) {
		try {
			fs = new POIFSFileSystem(is);
			wb = new HSSFWorkbook(fs);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = wb.getSheetAt(0);
		row = sheet.getRow(0);
		// 标题总列数
		int colNum = row.getPhysicalNumberOfCells();
		System.out.println("colNum:" + colNum);
		String[] title = new String[colNum];
		for (int i = 0; i < colNum; i++) {
			// title[i] = getStringCellValue(row.getCell((short) i));
			title[i] = getCellFormatValue(row.getCell((short) i));
		}
		return title;
	}

	/**
	 * 读取Excel数据内容
	 * 
	 * @param is
	 * @return Map 包含单元格数据内容的Map对象
	 */
	public Map<Integer, String> readExcelContent(InputStream is) {
		Map<Integer, String> content = new HashMap<Integer, String>();
		String str = "";
		try {
			fs = new POIFSFileSystem(is);
			wb = new HSSFWorkbook(fs);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = wb.getSheetAt(0);
		// 得到总行数
		int rowNum = sheet.getLastRowNum();
		row = sheet.getRow(0);
		int colNum = row.getPhysicalNumberOfCells();
		// 正文内容应该从第二行开始,第一行为表头的标题
		for (int i = 1; i <= rowNum; i++) {
			row = sheet.getRow(i);
			int j = 0;
			while (j < colNum) {
				// 每个单元格的数据内容用"-"分割开，以后需要时用String类的replace()方法还原数据
				// 也可以将每个单元格的数据设置到一个javabean的属性中，此时需要新建一个javabean
				// str += getStringCellValue(row.getCell((short) j)).trim() +
				// "-";
				str += getCellFormatValue(row.getCell((short) j)).trim() + "_";
				j++;
			}
			content.put(i, str);
			str = "";
		}
		return content;
	}

	private static Date getDateCellValue(HSSFCell cell) {
		cell.setCellType(CellType.NUMERIC);
		if (HSSFDateUtil.isCellDateFormatted(cell)) { 
			// POI的BUG: 只识别2014/02/02
			return cell.getDateCellValue();  
        } 
		return null;
	}
	
	/**
	 * 根据HSSFCell类型设置数据
	 * 
	 * @param cell
	 * @return
	 */
	private String getCellFormatValue(HSSFCell cell) {
		String cellvalue = "";
		if (cell != null) {
			// 判断当前Cell的Type
			switch (cell.getCellType()) {
				// 如果当前Cell的Type为NUMERIC
				case HSSFCell.CELL_TYPE_NUMERIC :
				case HSSFCell.CELL_TYPE_FORMULA : {
					// 判断当前的cell是否为Date
					if (HSSFDateUtil.isCellDateFormatted(cell)) {
						// 如果是Date类型则，转化为Data格式

						// 方法1：这样子的data格式是带时分秒的：2011-10-12 0:00:00
						// cellvalue = cell.getDateCellValue().toLocaleString();

						// 方法2：这样子的data格式是不带带时分秒的：2011-10-12
						Date date = cell.getDateCellValue();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						cellvalue = sdf.format(date);

					}
					// 如果是纯数字
					else {
						// 取得当前Cell的数值
						cellvalue = String.valueOf(cell.getNumericCellValue());
					}
					break;
				}
				// 如果当前Cell的Type为STRIN
				case HSSFCell.CELL_TYPE_STRING :
					// 取得当前的Cell字符串
					cellvalue = cell.getRichStringCellValue().getString();
					break;
				// 默认的Cell值
				default :
					cellvalue = " ";
			}
		} else {
			cellvalue = "";
		}
		return cellvalue;

	}
	
	public static List<Player> getPlayerList(InputStream is) throws BusinessException {
		List<Player> list = new ArrayList<Player>();
		try {
			fs = new POIFSFileSystem(is);
			wb = new HSSFWorkbook(fs);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (int i = 0; i < wb.getNumberOfSheets(); i++) {
			HSSFSheet sheet = wb.getSheetAt(i);
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				Player player = rowToPlayer(sheet.getRow(j));
				player.setTeamName(sheet.getSheetName());
				list.add(player);
			}
		}
		
		return list;
	}
	
	private static Player rowToPlayer(HSSFRow row) {
		Player player = new Player();
		int i = 0;
		int size = row.getPhysicalNumberOfCells();
		while (i <= size && !isEmpty(row)) {
			HSSFCell cell = row.getCell(i);
			if (cell == null) {
				i++;
				continue;
			}
			switch (i) {
				case 0 :
					cell.setCellType(CellType.STRING);
					player.setUniformNumber(Integer.parseInt(cell.getStringCellValue()));
					break;
				case 1 :
					cell.setCellType(CellType.STRING);
					player.setName(cell.getStringCellValue());
					break;
				case 2 :
					cell.setCellType(CellType.STRING);
					player.setNationality(cell.getStringCellValue());
					break;
				case 3 :
					player.setBirthDate(getDateCellValue(cell));
					break;
				case 4 :
					cell.setCellType(CellType.STRING);
					player.setPosition(Integer.parseInt(cell.getStringCellValue()));
					break;
				case 5 :
					cell.setCellType(CellType.STRING);
					player.setHeight(Integer.parseInt(cell.getStringCellValue()));
					break;
				case 6 :
					cell.setCellType(CellType.STRING);
					player.setWeight(Integer.parseInt(cell.getStringCellValue()));
					break;
				case 7 :
					cell.setCellType(CellType.STRING);
					player.setAvatar(cell.getStringCellValue());
					break;
				case 8 :
					cell.setCellType(CellType.STRING);
					player.setProfile(cell.getStringCellValue());
					break;
				case 9 :
					cell.setCellType(CellType.STRING);
					player.setCareer(cell.getStringCellValue());
					break;
			}
			i++;
		}
		return player;
	}
	
	private static boolean isEmpty(HSSFRow row) {
		if (row == null) {
			return true;
		}
		int colNum = row.getPhysicalNumberOfCells();
		for (int i = 0; i < colNum; i++) {
			HSSFCell cell = row.getCell(i);
			if (cell != null && !cell.toString().equals("")) {
				return false;
			}
		}
		return true;
	}
}