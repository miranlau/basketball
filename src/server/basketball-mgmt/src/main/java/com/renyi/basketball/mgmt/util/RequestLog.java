package com.renyi.basketball.mgmt.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

public class RequestLog implements Filter {
	private static Logger logger = Logger.getLogger(RequestLog.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;

		Map<String, String[]> map = request.getParameterMap();
		JSONObject jsonObject = new JSONObject();
		for (Map.Entry<String, String[]> entry : map.entrySet()) {
			try {
				jsonObject.put(entry.getKey(), entry.getValue());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		logger.info(dateFormat.format(new Date()) + " 来访者IP：" + request.getRemoteHost() + ", 访问的URI："
				+ request.getRequestURI() + ", 请求参数：" + jsonObject.toString());
		filterChain.doFilter(servletRequest, servletResponse);
	}

	public void destroy() {

	}
}
