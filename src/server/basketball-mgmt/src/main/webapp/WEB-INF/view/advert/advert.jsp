<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #province {
            width: 10%!important;
            float: left;
        }
        #detailarea{
            width: 90%!important;
            float: left;
        }
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">广告</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        广告
        <small>
            Advert
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <c:choose>
                <c:when test="${advert.isOpen}">
                    <span class="widget-caption"><input  type="checkbox" name="isOpen" checked="checked" class="isOpen" onclick="fn_showorhidden()"></span>
                </c:when>
                <c:otherwise>
                    <span class="widget-caption"><input  type="checkbox" name="isOpen" class="isOpen" onclick="fn_showorhidden()"></span>
                </c:otherwise>
            </c:choose>

            <span class="widget-caption">是否开启广告</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post" id="imagefrom">
                    <div class="form-group" id="image">
                        <label>图片上传</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" multiple class="file-loading" >
                        </div>
                        <div id="imageDiv"><input type="hidden" class="goodsImage"  name="srcPath" value="${advert.srcPath}"></div>
                    </div>
                    <div>
                        <label><span class="widget-caption">广告连接地址</span></label>
                    <label>
                        <input type="text" id="urlPath" name="urlPath" value="${advert.urlPath}" class="form-control input-sm" size="100">
                    </label>
                    </div>
                </form>
                <input type="button" value="保存" target="iframe"  id="confirmBtn" class="btn btn-info"/>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    $(function() {
        if($('.isOpen').is(':checked')){
            $("#imagefrom").show();
        }else {
            $("#imagefrom").hide();
        }
    })
    function  fn_showorhidden() {
        var flag = $('.isOpen').is(':checked');
        if(flag){
            $("#imagefrom").show();
        }else{
            $("#imagefrom").hide();
        }
    }
    var imageCount=0;
    function initFileUpload(initialPreview) {
        $("#iconUpload").fileinput({
            initialPreview: initialPreview,
            language:'zh',
            uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
            showPreview : true,
            showRemove: false,
            maxFileSize : 10240,  //上传图片的最大限制  50KB
            allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
            initialCaption: "请选择图片"
        });
        $("#iconUpload").on("fileuploaded", function (event, data, previewId, index) {
            if (null != data) {
                fillPath = data.response.data;
                $("#imageDiv input").val(fillPath);
                imageCount ++;
                $('.kv-file-remove').eq(index).click(function(){
                    $("#imageDiv input").eq(index).remove();
                })
            } else {
                Notify("上传图片失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        });
    }
    var fillPath="",imageCount=0,initialPreview=[];
    $(function(){
        getBannerInfo();//获取数据方法

    })
    function getBannerInfo(){

        if(${advert.srcPath!=null} && ${advert.srcPath!=""}){
            for(var i=0;i<1;i++){  //把图片循环读取出来
                initialPreview[i] = '<img width="auto" height="160" class="file-preview-image"  src="${advert.srcPath}">'+'<div class="file-thumbnail-footer">'+
                        '<div class="file-caption-name" title="" style="width: 250px;"></div>'+
                        '<div class="file-actions">'+
                        '<div class="file-footer-buttons">'+
                        '<button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file">'+
                        '<i class="glyphicon glyphicon-trash text-danger"></i>'+
                        '</button>'+
                        '</div>'+
                        '<div class="file-upload-indicator" tabindex="-1" title="Not uploaded yet">'+
                        '<i class="glyphicon glyphicon-hand-down text-warning"></i>'+
                        '</div>'+
                        '<div class="clearfix"></div>'+
                        '</div>'+
                        '</div>';
                 //$("#imageDiv input").val();
                imageCount ++;
            }
        }

        initFileUpload(initialPreview);  //初始化图片上传插件方法
        $('.kv-file-remove').each(function(index){
            $(this).click(function(){
                $(this).parent().parent().parent().parent().fadeOut('slow',function(){
                    var imgSrc = $(".file-preview-image").eq($(this).index()).attr("src");
                    $(this).remove();
                    $('.file-caption-name').attr('name',$(".kv-file-remove").length+' file selected'),
                            $('.file-caption-name').html('<span class="glyphicon glyphicon-file kv-caption-icon"></span>'+$(".kv-file-remove").length+' file selected');
                    $('.goodsImage').each(function() {
                        var fid = $(this).val();
                        if (imgSrc == fid) {
                            $(this).remove();
                        }
                    });
                });
            })
        })
    }


    $('#confirmBtn').click(function(){
        $('#btn').click();


        var $images = $(".goodsImage").val();
        if ($images == "" && $('.isOpen').is(':checked')) {
            Notify("请先上传图片!", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }else{

            $.ajax({
                url:"save?isOpen="+$('.isOpen').is(':checked'),
                method : "post",
                data: $("#imagefrom").serialize(),
                success : function (data) {

                    if(data.type=="success"){
                        Notify("保存成功!", 'top-right', '5000', 'success', 'fa-desktop', true);
                    }else{
                        Notify("保存失败!", 'top-right', '5000', 'danger', 'fa-desktop', true);
                    }
                },
                error:function () {
                    Notify("服务器超时!", 'top-right', '5000', 'danger', 'fa-desktop', true);
                }
            })
        }
    });
</script>
</body>
<!--  /Body -->
</html>

