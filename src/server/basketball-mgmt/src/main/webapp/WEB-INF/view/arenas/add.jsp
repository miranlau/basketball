<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
        #province,#city,#area {
            width: 33.3% !important;
            float: left;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加场馆</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        场馆
        <small>
            Arenas
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加场馆</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post">
                    <div class="form-group">
                        <label>图标</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" class="file-loading" >
                        </div>
                            <span class="input-icon icon-right">
                            	<div class="input-group" style="margin-top: 5px;">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input type="hidden" id="filePath" type="text" name="icon" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>场馆名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="name" id="name" type="text" maxlength="32" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>所在地区</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input type="hidden" name="areaCode" id="areaCode"/>
                                    <select id="province" class="form-control">
                                        <option disabled selected>请选择省</option>
                                    </select>
                                    <select id="city" class="form-control">
                                        <option disabled selected>请选择市</option>
                                    </select>
                                </div>
                            </span>
                    </div>
                    <%--<div class="form-group">--%>
                        <%--<label>星级</label>--%>
                            <%--<span class="input-icon icon-right">--%>
                            	<%--<div class="input-group">--%>
                            		 <%--<span class="input-group-addon">--%>
                                        <%--<i class="fa fa-laptop"></i>--%>
                                    <%--</span>--%>
                                    <%--<select name="stars">--%>
                                        <%--<option value="1"></option>--%>
                                    <%--</select>--%>
                                <%--</div>--%>
                            <%--</span>--%>
                    <%--</div>--%>
                    <div class="form-group">
                        <label>地址</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="address" id="address" type="text" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>联系人</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="telephone_name_1" id="telephone_name_1" type="text" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>联系电话</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="telephone_number_1" id="telephone_number_1" type="text" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>经纬度：<a href="http://cloud.sinyway.com/Service/amap.html" class="btn btn-palegreen" target="_Blank">查询地址</a></label>
                        <input type="text" name="longitude" id="lng" class="form-control input-sm" placeholder="经度"  maxlength="50" /><br>
                        <input type="text" name="latitude" id="lat" class="form-control input-sm" placeholder="纬度"  maxlength="50" />
                    </div>
                    <div class="form-group">
                        <label>场馆介绍</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="descs" id="descs" maxlength="200"></textarea>
                            </div>
                        </span>
                    </div>
                    <input type="submit" value="确认" target="iframe"  id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    var flag = false;
    var imageCount=0;
    $("#iconUpload").fileinput({
        language:'zh',
        uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
        showPreview : true,
        showRemove: false,
        maxFileSize : 10240,  //上传图片的最大限制  50KB
        allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
        initialCaption: "请选择场馆图标Logo"
    });
    $("#iconUpload").on("fileuploaded", function (event, data, previewId, index) {
        if (null != data) {
            console.log(data);
            fillPath = data.response.data;
            $("#filePath").val(fillPath);
            imageCount ++;
        } else {
            Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    });

    $('#confirmBtn').click(function(){
        var name = $("#name").val();
        var telephone = $("#telephone_number_1").val();
        var address = $("#address").val();
        var desc = $("#descs").val();
        var jingweiPattern = /[0-9]+.[0-9]+/  //经纬度正则
        var lng=$("#lng").val();
        var lat=$("#lat").val();
        var icon = $("#filePath").val();
        if (name =="") {
            Notify("请填写场馆名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }

        if ($("#areaCode").val() == "") {
            Notify("请选择所在城市", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }

        if (icon == "") {
            Notify("请上传球场Icon.", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }

        if(telephone ==""){
            Notify("请场馆联系电话", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(address ==""){
            Notify("请填写场馆地址", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(lng==""){
            Notify("请填写经度", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        } else if (lng!="" && !jingweiPattern.test(lng)){
            Notify("经度格式不正确(如103.8246843192)", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(lat==""){
            Notify("请填写纬度", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        } else if (lat!="" && !jingweiPattern.test(lat)){
            Notify("纬度格式不正确(如103.8246843192)", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(desc ==""){
            Notify("请填写场馆描述", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    });

    $().ready(function() {
        /*获得省份*/
        $.get("<%=base%>/ctl/area/getProvince", function(json){
            $("#province").empty();
            $("#city").empty();
            $("#area").empty();
            if (json != "" || null != json) {
                $("#province").append('<option disabled selected>请选择省</option>');
                $("#city").append('<option disabled selected>请选择市</option>');
                $("#area").append('<option disabled selected>请选择区</option>');
                $.each(json,function(index,province){
                    $("#province").append('<option value="' + province.code + '" >' + province.name + '</option>');
                })

            }
        })

        $("#province").change(function(){
            $("#areaName").val($("#province :selected").text());
            //获取该省的所有市
            $.get("<%=base%>/ctl/area/getCity",{
                code:$(this).val()
            },function(json){
                $("#city").empty();
//                $("#area").empty();
                if (json != "" || null != json) {
                    $("#city").append('<option disabled selected>请选择市</option>');
                    $.each(json,function(index,city){
                        $("#city").append('<option value="' + city.code + '" >' + city.name + '</option>');
                    })

                }
            })
        });

        $("#city").change(function(){
            $("#areaCode").val($("#city :selected").val());
        });

    });
</script>
</body>
<!--  /Body -->
</html>

