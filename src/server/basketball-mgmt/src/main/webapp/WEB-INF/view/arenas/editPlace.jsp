<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
        #province,#city,#area {
            width: 33.3% !important;
            float: left;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">编辑场地</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        场地
        <small>
            Place
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加场地</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="updatePlace" method="post">
                    <div class="form-group" style="display: none;">
                        <label>图标</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" class="file-loading" >
                        </div>
                            <span class="input-icon icon-right">
                            	<div class="input-group" style="margin-top: 5px;">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input type="hidden" id="filePath" type="text" name="icon" value="${place.icon}"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>场地名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="name" id="name" type="text" maxlength="32" value="${place.name}" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>营业开始时间</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="day_start_time" id="day_start_time" type="text" value="${place.day_start_time}"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>营业结束时间</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="day_end_time" id="day_end_time" type="text" value="${place.day_end_time}"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>价格</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="money" id="money" type="number" value="${place.money}"/>
                                </div>
                            </span>
                    </div>
                    <input type="hidden" name="id" value="${place.id}">
                    <input type="hidden" name="arenas_id" value="${place.arenas_id}">
                    <input type="submit" value="确认" target="iframe"  id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script type="text/javascript">
    $().ready(function() {
        var fillPath='${place.icon}';
        var initialPreview = [];
        initialPreview[0] = '<img width="auto" height="160" class="file-preview-image"  src="'+fillPath+'">'+
                '<div class="file-thumbnail-footer">'+
                '<div class="file-caption-name" title="teacher2.jpg" style="width: 250px;"></div>'+
                '<div class="file-actions">'+
                '<div class="file-footer-buttons">'+
                '<button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file" onclick="fn_remove()">'+
                '<i class="glyphicon glyphicon-trash text-danger"></i>'+
                '</button>'+
                '</div>'+
                '<div class="file-upload-indicator" tabindex="-1" title="Not uploaded yet">'+
                '<i class="glyphicon glyphicon-hand-down text-warning"></i>'+
                '</div>'+
                '<div class="clearfix"></div>'+
                '</div>'+
                '</div>';
        $("#filePath").val(fillPath);
        initFileUpload(initialPreview);  //初始化图片上传插件方法
        $('.kv-file-remove').each(function(index){
            $(this).click(function(){
                $(this).parent().parent().parent().parent().fadeOut('slow',function(){
//                    var imgSrc = $(".file-preview-image").eq($(this).index()).attr("src");
                    $(this).remove();
                    $('.file-caption-name').attr('name',$(".kv-file-remove").length+' file selected'),
                            $('.file-caption-name').html('<span class="glyphicon glyphicon-file kv-caption-icon"></span>'+$(".kv-file-remove").length+' file selected');
                    $("#filePath").val("");
                });
            });
        });


        function initFileUpload(initialPreview) {
            $("#iconUpload").fileinput({
                language: 'zh',
                initialPreview: initialPreview,
                uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg", "bmp"],
                maxFileSize: 1024,//上传图片的最大限制  50KB
                overwriteInitial: true,
                initialPreviewShowDelete: true,
                minImageWidth: 50,
                minImageHeight: 50
            }).on("fileuploaded", function (event, data, previewId, index) {
                if (null != data) {
                    fillPath = data.response.data;
                    $("#filePath").val(fillPath);
                } else {
                    Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
                }
            });
        }
    });
    var flag = false;

    var imageCount=0;
    $("#iconUpload").fileinput({
        language:'zh',
        uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
        showPreview : true,
        showRemove: false,
        maxFileSize : 10240,  //上传图片的最大限制  50KB
        allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
        initialCaption: "请选择场地Logo"
    });
    $("#iconUpload").on("fileuploaded", function (event, data, previewId, index) {
        if (null != data) {
            console.log(data);
            fillPath = data.response.data;
            $("#filePath").val(fillPath);
            imageCount ++;
        } else {
            Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    });

    $('#confirmBtn').click(function(){
        var name = $("#name").val();
        if(name ==""){
            Notify("请填写场地名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    });
</script>
</body>
<!--  /Body -->
</html>

