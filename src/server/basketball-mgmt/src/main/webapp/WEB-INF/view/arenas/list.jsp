<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon" rel="shortcut icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看场馆</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        场馆
        <small>
            Arenas
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看场馆</span>
            <div class="widget-buttons">
                <c:if test="<%=Admin.ARENAS_ADMIN.equals(admin.getType())%>">
                    <a href="add" target="iframe" class="btn btn-info">添加场馆</a>
                </c:if>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>场馆名称：
                        <input type="text" name="name" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <select id="status" name="status">
                            <option value="-1">全部</option>
                            <option value="1">审核通过</option>
                            <option value="2">待审核</option>
                            <option value="0">审核未通过</option>
                        </select>
                    </label>
                    <label>
                        <a class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>场馆名称</th>
                    <th>场馆图标</th>
                    <th>星级</th>
                    <th>地址</th>
                    <th>联系人</th>
                    <th>联系电话</th>
                    <%--<th>所属公司</th>--%>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/js/jqPaginator.js"></script>
<script src="<%=base%>/resource/js/jqPage.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>
<script>
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var inputVal = '';
    var status = '';
    $(function(){
        addpage(); //加载分页方法
        getData(1);

    })

    function getData(page,inputVal,status) {
        var listHtml = '';
        $.ajax({
            type:"POST",
            async:true,//异步请求
            url:'<%=base%>/ctl/arenas/arenasList',
            data:{pageNumber:page,name:inputVal,status:status},
            dataType:"json",
            timeout:5000,
            success:function(json) {
                console.log(json);
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0){
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){

                            listHtml += '<tr id="tr_' + obj.id + '">'
                                    + '<td>' + obj.id +'</td>'
                                    + '<td>' + obj.name +'</td>'
                                    + '<td><img src="' + obj.icon + '" /></td>'
                                    + '<td>' + obj.stars + '</td>'
                                    + '<td>' + obj.address + '</td>'
                                    + '<td>' + obj.telephone_name_1 + "</td>"
                                    + '<td>' + obj.telephone_number_1 + "</td>"
//                                    + '<td>' + obj.companyName + "</td>"
                                    + '<td>' + convertStatus(obj.status) + "</td>"
                                    + '<td>';
                            <%
                                if (Admin.SUPER_ADMIN.equals(admin.getType())){
                            %>
                            if (obj.status == 1) {
                                listHtml += '<a target="iframe" class="btn btn-info" disabled="disabled">审核</a>';
                            } else {
                                listHtml += '<a href="check?id=' + obj.id + '" target="iframe" class="btn btn-danger">审核</a>';
                            }
                            <%
                                }
                            %>
                            listHtml += '<a href="placesList?arenasId=' + obj.id + '" target="iframe" class="btn btn-info">场地</a>'
                            listHtml += '<a href="edit?id=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'
                            listHtml += '<a href="javascript:deleteArenas(' + obj.id + ')" target="iframe" class="btn btn-danger" arenasId=' + obj.id + '>删除</a>'
//                                    + '<a href="detail?id=' + obj.id + '" target="iframe" class="btn btn-info">详情</a>';
//                                    + '<a href="count?id=' + obj.id + '" target="iframe" class="btn btn-info">统计</a>'
//
                            + '</td></tr>';

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    getData(num,inputVal,status);
                                }
                            }
                        });
                    } else {
                        listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
        $(".delete").on("click",function(){
            if(confirm("确定删除吗?")==false)return false;
        })

        $("#qShoppingTemplet").click(function(){
            inputVal = $("#name").val();
            if ($("#status").val() != -1) {
                status = $("#status").val();
                getData(1,inputVal,status);
            } else {
                getData(1,inputVal);
            }
        });

        function convertStatus(status){
            if (status == 1) {
                return '审核通过';
            } else if (status == 0) {
                return '审核未通过';
            } else if (status == 2) {
                return '待审核';
            } else {

            }
        }
    }

    /**
     * 删除场馆.
     * @param id
     */
    function deleteArenas(id) {
        if (confirm("您确定要删除该场馆吗?")) {
            $.ajax({
                url:"delete",
                data:{id:id},
                success: function (result) {
                    if (result.code == 0) {
                        $("#tr_" + id).remove();
                    } else {
                        Notify(result.msg, 'top-right', '5000', 'danger', 'fa-desktop', true);
                    }
                }
            });
        }
    }

</script>
</body>
<!--  /Body -->
</html>

