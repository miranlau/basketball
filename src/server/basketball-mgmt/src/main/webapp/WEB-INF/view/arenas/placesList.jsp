<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>
    <style>
        img{
            height: 55px;
            width: 60px;
        }

    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">赛事场地</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        场地
        <small>
            Place
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">赛事场地</span>
            <div class="widget-buttons">
                <c:if test="<%=Admin.ARENAS_ADMIN.equals(admin.getType())%>">
                    <a href="<%=base%>/ctl/arenas/addPlace?arenasId=${arenas.id}" target="iframe" class="btn btn-info">添加场地</a>
                </c:if>
            </div>
        </div>
        <div class="widget-body">
            <div class="box-body">
                <div align="center">
                    <h2>${arenas.name}</h2>
                    <div class="form-group">
                        <label>Icon</label>
                        <%--<label><img src="${arenas.icon}" /></label></br>--%>
                        <label>${arenas.address}</label>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>名称</th>
                    <%--<th>图标</th>--%>
                    <th>营业时间</th>
                    <th>场地价格</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${places}" var="place">
                    <tr id="tr_${place.id}">
                        <td>${place.id}</td>
                        <%--<td><img src="${place.icon}" /> </td>--%>
                        <td>${place.name}</td>
                        <td>${place.day_start_time}~${place.day_end_time}</td>
                        <td>${place.money}</td>
                        <td>
                            <a href="schedule?placeId=${place.id}" class="btn btn-purple">场地安排</a>
                            <a href="editPlace?placeId=${place.id}" class="btn btn-info">编辑</a>
                            <a href="javascript:deletePlace(${place.id})" class="btn btn-danger">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script type="text/javascript">
    function deletePlace(id) {
        if (confirm("您确定要删除该场地吗?")) {
            $.ajax({
                url:"deletePlace",
                data:{placeId:id},
                success: function (result) {
                    if (result.code == 0) {
                        $("#tr_" + id).remove();
                    } else {
                        Notify(result.msg, 'top-right', '5000', 'danger', 'fa-desktop', true);
                    }
                }
            });
        }
    }
</script>
</body>
<!--  /Body -->
</html>

