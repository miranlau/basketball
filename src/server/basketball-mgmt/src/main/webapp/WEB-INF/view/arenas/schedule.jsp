<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
</head>
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">场地安排</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        场地
        <small>
            Place
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">场地安排</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div class="mui-content">
                <div class="adminDetailBox">
                    <ul class="content">
                        <li>
                            <div class="title">${place.name}</div>
                        </li>
                        <li class="tell">
                            场地营业时间：<span class="mui-pull-right">${place.day_start_time}~${place.day_end_time}</span>
                        </li>
                        <li class="address">
                            场地价格：<span class="mui-pull-right">${place.money}元</span>
                        </li>
                    </ul>
                        <hr/>
                    <div class="courtState">
                        <div class="title">选择日期：</div>
                        <div class="time"><input class="input-sm" type="date" id="chooseDate"/></div>
                        <div class="stateBox">
                            <table>
                                <thead>
                                    <tr>
                                        <th>时间\日期</th>
                                        <th>03-26</th>
                                        <th>03-26</th>
                                        <th>03-27</th>
                                        <th>03-28</th>
                                        <th>03-26</th>
                                        <th>03-26</th>
                                        <th>03-27</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="eight">
                                        <th>08:00~10:00</th>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                    </tr>
                                    <tr id="ten">
                                        <th>10:00~12:00</th>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                    </tr>
                                    <tr id="fourteen">
                                        <th>14:00~16:00</th>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                    </tr>
                                    <tr id="sixteen">
                                        <th>16:00~18:00</th>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                    </tr>
                                    <tr id="nineteen">
                                        <th>19:00~21:00</th>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                    </tr>
                                    <tr id="twenty-one">
                                        <th>21:00~23:00</th>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                        <td><span class="null">空</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/assets/js/bootbox/bootbox.js"></script>
<script>
    $(function() {
        var today = new Date();
        var date = today.getFullYear() + "-"
        + (parseInt(today.getMonth()) + 1)
        + "-" + today.getDate()
        + " " + today.getHours()
        + ":" + today.getMinutes()
        + ":" + today.getSeconds();
        getSchedule(date,${place.id});
    })

    $("#chooseDate").on("change",function(){
        var choose = $(this).val() + " 00:00:00";
        getSchedule(choose,${place.id});
    })

    function getSchedule(date,placeId){
        $.get("getSchedule",{
            date:date,placeId:placeId
        }, function(json){
            if (json.code == 0 && json.flag == 0) {
                for (var i = 1; i < 8; ++i) {
                    var date = $(".stateBox table thead tr").find("th").eq(i);
                    var eight = $("#eight").find("td").eq(i - 1);
                    var ten = $("#ten").find("td").eq(i - 1);
                    var fourteen = $("#fourteen").find("td").eq(i - 1);
                    var sixteen = $("#sixteen").find("td").eq(i - 1);
                    var nineteen = $("#nineteen").find("td").eq(i - 1);
                    var twentyone  = $("#twenty-one").find("td").eq(i - 1);
                    //更新时间
                    date.empty();
                    eight.empty();
                    ten.empty();
                    fourteen.empty();
                    sixteen.empty();
                    nineteen.empty();
                    twentyone.empty();
                    //注入时间日期<每一天的6场时间安排>
                    date.html(json.data[i-1].month + "-" + json.data[i-1].day);

                    var year = '<input type="hidden" name="year" value="'+json.data[i-1].year+'">';
                    var month = '<input type="hidden" name="month" value="'+json.data[i-1].month+'">';
                    var day = '<input type="hidden" name="day" value="'+json.data[i-1].day+'">';
                    var start_time8 = '<input type="hidden" name="start_time" value="08:00~10:00">';
                    var start_time10 = '<input type="hidden" name="start_time" value="10:00~12:00">';
                    var start_time14 = '<input type="hidden" name="start_time" value="14:00~16:00">';
                    var start_time16 = '<input type="hidden" name="start_time" value="16:00~18:00">';
                    var start_time19 = '<input type="hidden" name="start_time" value="19:00~21:00">';
                    var start_time21 = '<input type="hidden" name="start_time" value="21:00~23:00">';
                    eight.html('<span class="null">空</span>' + year + month + day + start_time8);
                    ten.html('<span class="null">空</span>' + year + month + day + start_time10);
                    fourteen.html('<span class="null">空</span>' + year + month + day + start_time14);
                    sixteen.html('<span class="null">空</span>' + year + month + day + start_time16);
                    nineteen.html('<span class="null">空</span>' + year + month + day + start_time19);
                    twentyone.html('<span class="null">空</span>' + year + month + day + start_time21);
                    $.each(json.data[i-1].schedule_today,function(index,item) {
                        var time = item.start_time;
                        var contactStr = item.contact;
                        var telStr = item.tel;
                        var descsStr = item.descs;
                        var data = '<input type="hidden" name="contactData" value="'+contactStr+'">' +
                                    '<input type="hidden" name="telData" value="'+telStr+'">'+
                                    '<input type="hidden" name="descsData" value="'+descsStr+'">';
                        if (time == '08:00~10:00') {
                            eight.empty();
                            eight.html('<span>满</span>' + year + month + day + start_time8 + data);
                        } else if (time == '10:00~12:00') {
                            ten.empty();
                            ten.html('<span>满</span>' + year + month + day + start_time10 + data);
                        } else if (time == '14:00~16:00') {
                            fourteen.empty();
                            fourteen.html('<span>满</span>' + year + month + day + start_time14 + data);
                        } else if (time == '16:00~18:00') {
                            sixteen.empty();
                            sixteen.html('<span>满</span>' + year + month + day + start_time16 + data);
                        } else if (time == '19:00~21:00') {
                            nineteen.empty();
                            nineteen.html('<span>满</span>' + year + month + day + start_time19 + data);
                        } else if (time == '21:00~23:00') {
                            twentyone.empty();
                            twentyone.html('<span>满</span>' + year + month + day + start_time21 + data);
                        }
                    })
                }

                $(".stateBox tbody span").on("click",function(){

                    var year = $(this).parent().find("input[name='year']").eq(0).val();
                    var month = $(this).parent().find("input[name='month']").eq(0).val();
                    var day = $(this).parent().find("input[name='day']").eq(0).val();
                    var start_time = $(this).parent().find("input[name='start_time']").eq(0).val();
                    var contactData = $(this).parent().find("input[name='contactData']").eq(0).val();
                    var telData = $(this).parent().find("input[name='telData']").eq(0).val();
                    var descsData = $(this).parent().find("input[name='descsData']").eq(0).val();
                    var flag = false;//是否为满
                    var contact = '';
                    var tel = '';
                    var ps = '';
                    var $span = $(this);
                    if ($span.hasClass("null")) {
                        contact = '<input type="text" name="contact"/>';
                        tel = '<input type="text" name="tel"/>';
                        ps = '<textarea style="resize: none;width: inherit"></textarea>';
                    } else {
                        flag = true;
                        contact = '<input disabled type="text" name="contact" value="' + contactData + '"/>';
                        tel = '<input disabled type="text" name="tel" value="' + telData + '"/>';
                        ps = '<textarea disabled cols="15" style="resize: none;width: inherit">' + descsData + '</textarea>';
                    }

                    var str= '';
                        str='<div id="myModal">'+
                                '<table class="table table-striped table-hover" id="simpledatatable">'+
                                '<tbody>'+
                                '<tr>'+
                                '<th text-align="center" width="20%">联系人:</th>'+
                                '<td>' + contact + '</td>'+
                                '</tr>'+
                                '<tr>'+
                                '<th text-align="center" width="20%">联系电话:</th>'+
                                '<td>' + tel + '</td>'+
                                '</tr>'+
                                '<tr>'+
                                '<th text-align="center" width="20%">备注:</th>'+
                                '<td width="80%">' + ps + '</td>'+
                                '</tr>'+
                                '</tbody>';
                        str +=  '</table>'+
                                '</div>';
                    bootbox.dialog({
                        message: str,
                        title: "预约详情",
                        buttons: {
                            "关闭": {
                                className: "btn-default",
                                callback: function () {
                                }
                            },
                            success: {
                                label: "确定",
                                className: "btn-primary",
                                callback: function () {
                                    var $ps = $("#myModal textarea").eq(0).val();
                                    var $contact = $("#myModal input[name='contact']").eq(0).val();
                                    var $tel = $("#myModal input[name='tel']").eq(0).val();

                                    if (!flag) {
                                        $.get("saveSchedule",{
                                            places_id:${place.id},
                                            arenas_id:${place.arenas_id},
                                            year:year,
                                            month:month,
                                            day:day,
                                            start_time:start_time,
                                            price:0,
                                            descs:$ps,//备注
                                            contact:$contact,//联系人
                                            tel:$tel
                                        },function(json){
                                            if (json.flag == 0 && json.code == 0) {
                                                Notify("保存成功", 'top-right', '5000', 'success', 'fa-desktop', true);
                                                $span.removeClass("null");
                                                var data = '满' + '<input type="hidden" name="contactData" value="'+$contact+'">' +
                                                        '<input type="hidden" name="telData" value="'+$tel+'">'+
                                                        '<input type="hidden" name="descsData" value="'+$ps+'">';
                                                $span.html(data);
                                            } else {
                                                Notify("保存失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    });
                })
            } else {
                alert("服务器异常!");
            }
        })
    }



</script>
</body>
</html>
