<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>全网管理后台</title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon" rel="shortcut icon" >


    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css"/>
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css"/>

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>
    <style>
        .page-sidebar .sidebar-menu .submenu>li>a:hover {
            color: white;
        }
    </style>
    <script>
        function resizeHeight() {

            var ifm= document.getElementById("iframe");
            var subWeb = document.frames ? document.frames["iframe"].document : ifm.contentDocument;
            if (ifm != null && subWeb != null) {
                ifm.height = subWeb.body.scrollHeight;
            }
        }

    </script>
</head>
<!-- /Head -->
<!-- Body -->
<body>
<!-- Loading Container -->
<div class="loading-container">
    <div class="loading-progress">
        <div class="rotator">
            <div class="rotator">
                <div class="rotator colored">
                    <div class="rotator">
                        <div class="rotator colored">
                            <div class="rotator colored"></div>
                            <div class="rotator"></div>
                        </div>
                        <div class="rotator colored"></div>
                    </div>
                    <div class="rotator"></div>
                </div>
                <div class="rotator"></div>
            </div>
            <div class="rotator"></div>
        </div>
        <div class="rotator"></div>
    </div>
</div>
<!--  /Loading Container -->
<!-- Navbar -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="navbar-container">
            <!-- Navbar Barnd -->
            <div class="navbar-header pull-left">
                <a href="#" class="navbar-brand">
                    <span class="logo-mini"><b>篮球</b></span>
                    <span class="logo-lg"><b>全网篮球</b>管理</span>
                </a>
            </div>
            <!-- /Navbar Barnd -->

            <!-- Sidebar Collapse -->
            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="collapse-icon fa fa-bars"></i>
            </div>
            <!-- /Sidebar Collapse -->
            <!-- Account Area and Settings --->
            <div class="navbar-header pull-right">

            </div>
            <!-- /Account Area and Settings -->
        </div>
    </div>
</div>
<!-- /Navbar -->
<!-- Main Container -->
<div class="main-container container-fluid">
    <!-- Page Container -->
    <div class="page-container">
        <!-- Page Sidebar -->
        <div class="page-sidebar" id="sidebar">
            <!-- Page Sidebar Header-->
            <div class="sidebar-header-wrapper">功能</div>
            <!-- /Page Sidebar Header -->
            <!-- Sidebar Menu -->
            <ul class="nav sidebar-menu">
                <!--Dashboard-->
                <%--<li class="active">--%>
                    <%--<a href="index.jsp" target="iframe">--%>
                        <%--<i class="menu-icon glyphicon glyphicon-home"></i>--%>
                        <%--<span class="menu-text"> 首页 </span>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType()%>">
                    <li>
                        <a href="../company/list" target="iframe">
                            <i class="menu-icon glyphicon glyphicon-copyright-mark"></i>
                            <span class="menu-text"> 公司 </span>
                        </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType() || Admin.COMPANY_ADMIN == admin.getType()%>">
                    <li>
                        <a href="../match/list" target="iframe">
                            <i class="menu-icon glyphicon glyphicon-expand"></i>
                            <span class="menu-text"> 比赛 </span>
                        </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType() || Admin.COMPANY_ADMIN == admin.getType()%>">
                    <li>
                        <a href="../league/list" target="iframe">
                            <i class="menu-icon glyphicon glyphicon-tower"></i>
                            <span class="menu-text"> 赛事 </span>
                        </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType()%>">
                <li>
                    <a href="../team/list" target="iframe">
                        <i class="menu-icon glyphicon glyphicon-text-width"></i>
                        <span class="menu-text"> 球队 </span>
                    </a>
                </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType()%>">
                <li>
                    <a href="../player/list" target="iframe">
                        <i class="menu-icon glyphicon glyphicon-user"></i>
                        <span class="menu-text"> 球员 </span>
                    </a>
                </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType()%>">
                    <li>
                        <a href="../advert/getAdvert" target="iframe">
                            <i class="menu-icon glyphicon glyphicon-bullhorn"></i>
                            <span class="menu-text"> 广告 </span>
                        </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.COMPANY_ADMIN == admin.getType()%>">
                    <li>
                    <a href="../venue/list" target="iframe">
                    <i class="menu-icon glyphicon glyphicon-home"></i>
                    <span class="menu-text"> 场地 </span>
                    </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType() || Admin.ARENAS_ADMIN == admin.getType()%>">
                    <li>
                        <a href="../venue/list" target="iframe">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> 球场 </span>
                        </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType() || Admin.ARENAS_ADMIN == admin.getType()%>">
                    <li>
                        <a href="../goods/toList" target="iframe">
                            <i class="menu-icon glyphicon glyphicon-shopping-cart"></i>
                            <span class="menu-text"> 商品 </span>
                        </a>
                    </li>
                </c:if>
                <c:if test="<%=Admin.SUPER_ADMIN == admin.getType()%>">
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-envelope"></i>
                            <span class="menu-text"> 版本更新 </span>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="../version/iosPage" target="iframe">
                                    <span class="menu-text">iOS</span>
                                </a>
                                <a href="../version/androidPage" target="iframe">
                                    <span class="menu-text">安卓</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </c:if>
                <li>
                    <a href="../common/logout">
                        <i class="menu-icon fa fa-sign-out"></i>
                        <span class="menu-text"> 登出 </span>
                    </a>
                </li>
                <!--UI Elements-->

            </ul>
            <!-- /Sidebar Menu -->
        </div>
        <!-- /Page Sidebar -->
        <!-- Page Content -->
        <div class="page-content" id="main-content">
            <iframe id="iframe" name="iframe" onload="resizeHeight()" src="../common/nav" frameborder="0" style="width: 100%;height: 1000px;"></iframe>
        </div>
        <!-- /Page Content -->
    </div>
    <!-- /Page Container -->
    <!-- Main Container -->

</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>

<script>
    var secthei = document.documentElement.clientHeight || document.body.clientHeight;
    var headerh = 55;
    //        document.getElementById('sect').style.height = (secthei-headerh) + "px";
    document.getElementById("iframe").style.height = (secthei-headerh) + "px";
    $('#sidebar-collapse').click(function(){
        if($(this).hasClass('active')){
            $('.navbar-brand .logo-lg').hide();
            $('.navbar-brand .logo-mini').show();
        }else{
            $('.navbar-brand .logo-lg').show();
            $('.navbar-brand .logo-mini').hide();
        }
    })

    $('.sidebar-menu li').each(function(){
        var $this=$(this).find('a');
        $(this).click(function(){
            $('.sidebar-menu li').removeClass('active');
            $(this).addClass('active');
        })
    })
</script>
</body>
<!--  /Body -->
</html>

