<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #province {
            width: 10%!important;
            float: left;
        }
        #detailarea{
            width: 90%!important;
            float: left;
        }
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加公司</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        公司
        <small>
            Register
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加公司</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post">
                    <div class="form-group">
                        <label>登录账号</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="userName" id="userName" type="text" maxlength="32" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>登录密码</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <input class="form-control" name="password" id="password" type="password" maxlength="32" />
                            </div>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>公司类型</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <select id="type" name="type" class="form-control">
                                    <option selected value="1">赛事方</option>
                                    <option value="2">场馆方</option>
                                </select>
                            </div>
                        </span>
                    </div>
                        <div class="form-group">
                            <label>公司名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="name" id="name" type="text" maxlength="32" />
                                </div>
                            </span>
                        </div>
                    <div class="form-group">
                        <label>联系人</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="contacter" id="contacter" type="text" maxlength="20"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>联系电话</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="mobile" id="mobile" type="text" maxlength="20"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>邮箱</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="email" id="email" type="text" maxlength="25"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>公司地址</label><span>
                        <input type="hidden" name="areaCode" id="areaCode"/>
                        </span>
                            <span class="input-icon icon-right">
                            	<select id="province" class="form-control">
                                    <option disabled>请选择省</option>
                                </select>
                                <input type="text" class="form-control" name="addr" id="detailarea" placeholder="详细地址" maxlength="50">
                            </span>
                    </div>
                    <div class="form-group">
                        <label>组织机构代码</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <input class="form-control input-sm" name="organizationCode" id="organizationCode" type="text" maxlength="50"/>
                            </div>
                        </span>
                    </div>
                    <div class="form-group">
                    <label>组织机构代码副本上传</label>
                    <div>
                    <input id="iconUpload1" name="res" type="file" class="file-loading" >
                    </div>
                    <span class="input-icon icon-right">
                    <div class="input-group" style="margin-top: 5px;">
                    <span class="input-group-addon">
                    <i class="fa fa-laptop"></i>
                    </span>
                    <input type="hidden" id="filePath1" type="text" name="organizationImg" />
                    </div>
                    </span>
                    </div>
                    <div class="form-group">
                        <label>公司描述</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="describes" id="describe" maxlength="200"></textarea>
                            </div>
                        </span>
                    </div>
                    <input type="submit" value="确认" target="iframe"  id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    $(function(){
        <!--获取队伍列表-->
//        $.get("getTeams",function(json){
//            $("#team").empty();
//            if (json != "" || null != json) {
//                $.each(json,function(index,team){
//                    $("#team").append('<option value="' + team.id + '" >' + team.name + '</option>');
//                })
//
//            }
//        })
        /*获得省份*/
        $.get("<%=base%>/ctl/company/getProvince",function(json){
            $("#province").empty();
            $("#city").empty();
            $("#area").empty();
            if (json != "" || null != json) {
                $("#province").append('<option disabled selected>请选择省</option>');
                $.each(json,function(index,province){
                    $("#province").append('<option value="' + province.code + '" >' + province.name + '</option>');
                })
            }
        })
        $("#province").change(function(){
            $("#detailarea").val($("#province :selected").text())
            $("#areaCode").val($("#province :selected").val())
        })



    })

    var imageCount1=0;
    $("#iconUpload1").fileinput({
        language:'zh',
        uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
        showPreview : true,
        showRemove: false,
        maxFileSize : 10240,  //上传图片的最大限制  50KB
        allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
        initialCaption: "请选择组织机构代码证副本"
    });
    $("#iconUpload1").on("fileuploaded", function (event, data, previewId, index) {
        if (null != data) {
            fillPath = data.response.data;
            $("#filePath1").val(fillPath);
            imageCount1 ++;
        } else {
            Notify("上传组织结构代码证副本失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    });

    $("#userName").blur(function(){
        var userName = $("#userName").val();
        if(userName ==""){
            Notify("请填写登录账号", 'top-right', '5000', 'danger', 'fa-desktop', true);
        } else {
            $.get("<%=base%>/ctl/common/checkUserName",{
            	userName: userName
            },function(json){
                if (json) {
                    Notify("账号可用", 'top-right', '5000', 'danger', 'fa-desktop', true);
                } else {
                    Notify("账号已存在，请重新输入", 'top-right', '5000', 'danger', 'fa-desktop', true);
                }
            })
        }
    })
    <%--$("#name").blur(function(){--%>
        <%--var name = $("#name").val();--%>
        <%--if(name ==""){--%>
            <%--Notify("请填写公司名称", 'top-right', '5000', 'danger', 'fa-desktop', true);--%>
        <%--} else {--%>
            <%--$.get("<%=base%>/ctl/common/checkUserName",{--%>
                <%--username:name--%>
            <%--},function(json){--%>
                <%--if (json) {--%>
                    <%----%>
                <%--} else {--%>
                    <%--Notify("该公司已存在，请重新输入", 'top-right', '5000', 'danger', 'fa-desktop', true);--%>
                <%--}--%>
            <%--})--%>
        <%--}--%>
    <%--})--%>

    $('#confirmBtn').click(function(){
        var userName = $("#userName").val();
        var name = $("#name").val();
        var password = $("#password").val();
        var contacter = $("#contacter").val();
        var mobile = $("#mobile").val();
        var email = $("#email").val();
        var areaCode = $("#areaCode").val();
        var detailarea = $("#detailarea").val();
        var organization = $("#filePath1").val();
        var organizationCode = $("#organizationCode").val();
        var describe = $("#describe").val();
        if(userName ==""){
            Notify("请填写登陆账号", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(password ==""){
            Notify("请填写登陆密码", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(name ==""){
            Notify("请填写公司名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(contacter ==""){
            Notify("请填写公司联系人", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(mobile ==""){
            Notify("请填写联系电话", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(email ==""){
            Notify("请填写公司邮箱", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(areaCode ==""){
            Notify("请选择所在省份", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(detailarea == ""){
            Notify("请填写公司地址", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(organizationCode ==""){
            Notify("请填写组织机构代码证编号", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(organization ==""){
            Notify("请上传组织机构代码证副本", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(describe ==""){
            Notify("请填写公司描述", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
</script>
</body>
<!--  /Body -->
</html>

