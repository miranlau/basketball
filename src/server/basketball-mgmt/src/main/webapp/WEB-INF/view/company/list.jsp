<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon" rel="shortcut icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看公司</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        公司
        <small>
            列表
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看公司</span>
            <div class="widget-buttons">
                <a href="add" target="iframe" class="btn btn-info">添加公司</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>公司名称：
                        <input type="text" name="name" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <select id="province" name="areaCode">
                            <option value="-1" selected>全部</option>
                        </select>
                    </label>
                    <label>
                        <a class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>公司名称</th>
                    <th>地址</th>
                    <th width="600px">描述</th>
                    <th>申请时间</th>
                    <%--<th>操作</th>--%>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/js/jqPaginator.js"></script>
<script src="<%=base%>/resource/js/jqPage.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>
<script>
   var totalpage= '';
   var nowPage = 1;
   var total = '';
   var inputVal = '';
   var province = '';
    $(function(){
        /*获得省份*/
        $.get("<%=base%>/ctl/company/getProvince",function(json){
            $("#province").empty();
            $("#city").empty();
            $("#area").empty();
            if (json != "" || null != json) {
                $("#province").append('<option value="-1" selected>全部</option>');
                $.each(json,function(index,province){
                    $("#province").append('<option value="' + province.code + '" >' + province.name + '</option>');
                })
            }
        })
        addpage(); //加载分页方法
        getData(1);
    })

   function getData(page,inputVal,province) {
        var listHtml = '';
        $.ajax({
            type:"POST",
            async:true,//异步请求
            url:'<%=base%>/ctl/company/companyList',
            data:{pageNumber:page,name:inputVal,province:province},
            dataType:"json",
            timeout:5000,
            success:function(json) {
                console.log(json);
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0){
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){
                            listHtml += '<tr>'
                                    + '<td>' + obj.id +'</td>'
                                    + '<td>' + obj.name + '</td>'
                                    + '<td>' + obj.addr + "</td>"
                                    + '<td>' + obj.describes + "</td>"
                                    + '<td>' + getTime(obj.createTime) + "</td>"
//                                    + '<td>'
//                                    + '<a href="detail?id=' + obj.id + '" target="iframe" class="btn btn-info">详情</a>'
//                                    + '<a href="modify?id=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'
//                                    + '<a href="delete?id=' + obj.id + '" class="btn btn-danger">删除</a>'
//                                    + '</td></tr>';

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    getData(num,inputVal,province);
                                }
                            }
                        });
                    } else {
                        listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })

        $("#qShoppingTemplet").click(function(){
            inputVal = $("#name").val();
            if ($("#province").val() != -1) {
                province = $("#province").val();
                getData(1,inputVal,province);
            } else {
                getData(1,inputVal);
            }

        })

    }
</script>
</body>
<!--  /Body -->
</html>

