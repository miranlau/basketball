<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #province {
            width: 10%!important;
            float: left;
        }
        #detailarea{
            width: 90%!important;
            float: left;
        }
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加商品</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        商品
        <small>
            Add
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加商品</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="<%=base%>/ctl/goods/saveProduct" method="post">
                    <div class="form-group">
                        <label>商品名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="name" id="name" type="text" maxlength="32" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>所属场馆</label>
                        <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                   <select id="areans" name="areansId" onchange="fn_setAreansId()" class="form-control">
                                <option value="-1" selected>全部</option>
                                    </select>
                                </div>
                            </span>

                    </div>
                    <div class="form-group">
                        <label>生产公司</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <input class="form-control" name="company" id="company" type="text" maxlength="32" />
                            </div>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>进价</label>
                             <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="inPrice" id="inPrice" type="text" maxlength="32" />
                                </div>
                            </span>
                    </div>
                        <div class="form-group">
                            <label>售价</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="outPrice" id="outPrice" type="text" maxlength="32" />
                                </div>
                            </span>
                        </div>
                    <div class="form-group">
                    <label>图片上传</label>
                    <div>
                    <input id="iconUpload1" name="res" type="file" class="file-loading" >
                    </div>
                    <span class="input-icon icon-right">
                    <div class="input-group" style="margin-top: 5px; display: none" >
                    <span class="input-group-addon">
                    <i class="fa fa-laptop"></i>
                    </span>
                    <input  id="filePath1" type="text" name="image" />
                    </div>
                    </span>
                    </div>
                    <div class="form-group">
                        <label>商品描述</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="description" id="description" maxlength="200"></textarea>
                            </div>
                        </span>
                    </div>
                    <input value="${areansId}" id="areansId" type="hidden">
                    <input type="submit" value="确认" id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>


    var imageCount1=0;
    $("#iconUpload1").fileinput({
        language:'zh',
        uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
        showPreview : true,
        showRemove: false,
        maxFileSize : 10240,  //上传图片的最大限制  50KB
        allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
        initialCaption: "请选择图片"
    });
    $("#iconUpload1").on("fileuploaded", function (event, data, previewId, index) {
        if (null != data) {
            fillPath = data.response.data;
            $("#filePath1").val(fillPath);
            imageCount1 ++;
        } else {
            Notify("上传图片失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    });

    $("#name").blur(function(){
        var name = $("#name").val();
        if(name ==""){
            Notify("请商品名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
        } else {
            $.get("<%=base%>/ctl/goods/checkName",{
                name:name
            },function(json){
                if (json) {
                    Notify("商品名称可用", 'top-right', '5000', 'success', 'fa-desktop', true);
                } else {
                    Notify("商品名称已存在，请重新输入", 'top-right', '5000', 'danger', 'fa-desktop', true);
                }
            })
        }
    })
    $(function(){
        /*获得场馆*/
        $.get("<%=base%>/ctl/arenas/getArenasByCompany",{

        },function(json){
            $("#areans").empty();
            console.log(json);
            if (json != "" || null != json) {
                $("#areans").append('<option value="-1" selected>全部</option>');
                $.each(json.data,function(index,areans){
                    if(areans.id==${areansId}) {
                        $("#areans").append('<option value="' + areans.id + '" selected>' + areans.name + '</option>');
                    }else {
                        $("#areans").append('<option value="' + areans.id + '" >' + areans.name + '</option>');
                    }
                })
            }
        })
        //设置下拉框的值
         $("#areans").val($('#areansId').val());
    })

    $('#confirmBtn').click(function(){
        var name = $("#name").val();
        var inPrice = $("#inPrice").val();
        var outPrice = $("#outPrice").val();
        var areansId = $("#areans").val();
        if(name ==""){
            Notify("请填写商品名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }

        if(inPrice ==""){
            Notify("请填写进价", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(outPrice ==""){
            Notify("请填写售价", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(areansId=='-1'){
            Notify("请选择球场", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
</script>
</body>
<!--  /Body -->
</html>

