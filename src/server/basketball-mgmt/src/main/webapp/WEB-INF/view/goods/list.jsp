<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon" rel="shortcut icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>
<style>
    .form-group {
        margin-bottom: 15px;
        width: 45%;
        float: left;
        margin-left: 22px;
    }
    .remark {
        width: 100%;
        height: 59px;
    }
</style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看商品</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        商品
        <small>
            列表
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看商品</span>
            <div class="widget-buttons">
                <a id="toAdd"  target="iframe" class="btn btn-info" >添加商品</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>名称：
                        <input type="text" name="name" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <select id="areans" name="areansId" onchange="fn_setAreansId()">
                            <option value="-1" selected>全部</option>
                        </select>
                    </label>
                    <label>
                        <a class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th  width="100px">ID</th>
                    <th  width="100px">图标</th>
                    <th width="100px">商品名称</th>
                    <th width="200px">球场</th>
                    <th width="100px">进货价格</th>
                    <th width="100px">出售价格</th>
                    <th width="100px">销量</th>
                    <th width="100px">库存</th>
                    <%--<th>操作</th>--%>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/js/jqPaginator.js"></script>
<script src="<%=base%>/resource/js/jqPage.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>
<script src="<%=base%>/resource/assets/js/bootbox/bootbox.js"></script>
<script>
   var totalpage= '';
   var nowPage = 1;
   var total = '';
   var inputVal = '';
   var areans = '';
    $(function(){
        /*获得场馆*/
        $.get("<%=base%>/ctl/arenas/getArenasByCompany",{

        },function(json){
            $("#areans").empty();
            console.log(json);
            if (json != "" || null != json) {
                $("#areans").append('<option value="-1" selected>全部</option>');
                $.each(json.data,function(index,areans){
                    $("#areans").append('<option value="' + areans.id + '" >' + areans.name + '</option>');
                })
            }
        })
        addpage(); //加载分页方法
        getData(1);
    })

function fn_detail(id) {

    $.ajax({
        type: "POST",
        async: true,//异步请求
        url: '<%=base%>/ctl/goods/find?productId='+id,
        dataType: "json",
        data:{areansId:areans},
        success : function (json) {
            var good  = json.data;
            str = "<div class='page-body'>"+
                    "<div class='widget'>"+
            "<div class='widget-header '>"+
            " <span class='widget-caption'>"+good.name+"</span>"+
            "</div>"+
            "<div class='widget-body'>"+
            "<div class='box-body'>"+
            "<div align='center'>"+
            " <h2></h2>"+
            "  </div>"+

            " <div class='form-group'>"+
            " <label></label>"+
            "  <img style='width: 200px' src='"+good.image+"'>"+
            " </div>"+
            "<div class='form-group'>"+
            " <label>进价:</label>"+
            " <label>"+good.inPrice+"</label>"+
            "</div>"+
            " <div class='form-group'>"+
            " <label>售价:</label>"+
            "  <label>"+good.outPrice+"</label>"+
            "    </div>"+
                    " <div class='form-group'>"+
                    " <label>销量:</label>"+
                    "  <label>"+good.sales+"</label>"+
                    "    </div>"+
                    " <div class='form-group'>"+
                    " <label>库存:</label>"+
                    "  <label>"+good.stack+"</label>"+
                    "    </div>"+
            "      </div>"+
            "  </div>"+
            "    </div>";
            bootbox.dialog({
                message: str,
                title: "商品详情",
                buttons: {
                    "关闭": {
                        className: "btn-default",
                        callback: function () {

                        }
                    }
                }
            });
        }
    });

}
/*   $("body").delegate(".detail","click",function () {

   })*/
   
   function getData(page,inputVal,areans) {
        var listHtml = '';
        $.ajax({
            type:"POST",
            async:true,//异步请求
            url:'<%=base%>/ctl/goods/list',
            data:{pageNumber:page,name:inputVal,areansId:areans},
            dataType:"json",
            timeout:5000,
            success:function(json) {
                //console.log(json);
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0){
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){
                            console.log(obj)
                            listHtml += '<tr>'
                                    + '<td>' + obj.id +'</td>'
                                    + '<td><img src="' + obj.image + '" /></td>'
                                    + '<td>' + obj.name + "</td>"
                                    + '<td>' + obj.arenasName + "</td>"
                                    + '<td>' + obj.inPrice + "</td>"
                                    + '<td>' + obj.outPrice + "</td>"
                                    + '<td>' + obj.sales + "</td>"
                                    + '<td>' + obj.stack + "</td>"
                                    + '<td>'
                                    + '<a href="javascript:void(0)" target="iframe" class="btn btn-info detail" onclick="fn_detail('+obj.id+')">详情</a>'
                                    + '<a href="toModify?productId=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'
                                    + '<a  class="btn btn-danger" onclick="beforDel(' + obj.id + ')">删除</a>'
                                    + '<a  target="iframe" class="btn btn-info" onclick="fn_stock('+obj.id+','+obj.areansId+')">进货</a>'
                                    + '<a  target="iframe" class="btn btn-info" onclick="fn_sell('+obj.id+','+obj.areansId+')">出售</a>'
                                    + '</td></tr>';

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    getData(num,inputVal,province);
                                }
                            }
                        });
                    } else {
                        listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })

        $("#qShoppingTemplet").click(function(){
            inputVal = $("#name").val();
            if ($("#areans").val() != -1) {
                areans = $("#areans").val();
                getData(1,inputVal,areans);
            } else {
                getData(1,inputVal);
            }

        })

    }
   $('#toAdd').attr('href','toAdd?areansId='+$("#areans").val());
    function fn_setAreansId() {
        $('#toAdd').attr('href','toAdd?areansId='+$("#areans").val());
    }
    function fn_toadd() {
        if($("#areans").val()==null || $("#areans").val()==""|| $("#areans").val()==-1){
            Notify("请选择添加商品的球场", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    }
   /**
    * 删除之前的判断
    */
   function beforDel(id) {
       if (confirm("确定删除？")) {
           window.location.href="delete?productId=" + id;
       }
   }
   /**
    * 进货
    * @param id
    * @param arenasId
    */
    function fn_stock(id,arenasId) {
        $.ajax({
            type: "POST",
            async: true,//异步请求
            url: '<%=base%>/ctl/goods/find?productId=' + id,
            dataType: "json",
            success: function (json) {
                var good = json.data;
                arenasId = good.arenasId;
                str = "<div class='page-body'>" +
                        "<div class='widget'>" +
                        "<div class='widget-header '>" +
                        " <span class='widget-caption'>" + good.name + "</span>" +
                        "</div>" +
                        "<div class='widget-body'>" +
                        "<div class='box-body'>" +
                        "<div align='center'>" +
                        " <h2></h2>" +
                        "  </div>" +
                        " <div class='form-group'>" +
                        " <label></label>" +
                        "  <img style='width: 200px' src='" + good.image + "'>" +
                        " </div>" +
                        "<div class='form-group'>" +
                        " <label>进价:</label>" +
                        " <label>" + good.inPrice + "</label>" +
                        "</div>" +
                        " <div class='form-group'>" +
                        " <label>售价:</label>" +
                        "  <label>" + good.outPrice + "</label>" +
                        "    </div>" +
                        " <div class='form-group'>" +
                        " <label>销量:</label>" +
                        "  <label>" + good.sales + "</label>" +
                        "    </div>" +
                        " <div class='form-group'>" +
                        " <label>库存:</label>" +
                        " <label>" + good.stack + "</label>" +
                        "    </div>" +
                        " <div class='form-group'>" +
                        " <label>进货量:</label>" +
                        "  <input name='quantity'id='quantity'>" +
                        "    </div>" +
                        " <div class='form-group'>" +
                        " <label>备注:</label>" +
                        " <textarea name='remark' class='remark' id='remark'> </textarea>" +
                        "    </div>" +
                        "      </div>" +
                        "  </div>"
                        "    </div>";
                bootbox.dialog({
                    message: str,
                    title: "进货详情&nbsp <span style='padding-left: 100px'>" + good.arenasName+"</span>",
                    buttons: {
                        "关闭": {
                            className: "btn-default",
                            callback: function () {
                            }
                        },
                        success: {
                            label: "确定进货",
                            className: "btn-primary",
                            callback: function () {
                                var remark = $('#remark').val();
                                var quantity = $('#quantity').val();
                                alert(arenasId)
                                if (isPositiveNum(quantity)) {
                                    window.location.href = "inoutproduct?pid=" + id + "&aid=" +arenasId + "&remark=" + remark + "&quantity=" + quantity+ "&type=" + 1;
                                    Notify("添加进货成功！", 'top-right', '5000', 'success', 'fa-desktop', true);
                                }else {
                                    Notify("添加进货失败，进货数量只能为正整数", 'top-right', '5000', 'danger', 'fa-desktop', true);
                                }
                            }
                        }
                    }
                });
            }
        });
    }
   function isPositiveNum(s){//是否为正整数
       var re = /^[0-9]*[1-9][0-9]*$/ ;
       return re.test(s)
   }
   /**
    * 出货
    * @param id
    * @param arenasId
    */
   function fn_sell(id,arenasId) {
       $.ajax({
           type: "POST",
           async: true,//异步请求
           url: '<%=base%>/ctl/goods/find?productId=' + id,
           dataType: "json",
           success: function (json) {
               var good = json.data;
               arenasId = good.arenasId;
               str = "<div class='page-body'>" +
                       "<div class='widget'>" +
                       "<div class='widget-header '>" +
                       " <span class='widget-caption'>" + good.name + "</span>" +
                       "</div>" +
                       "<div class='widget-body'>" +
                       "<div class='box-body'>" +
                       "<div align='center'>" +
                       " <h2></h2>" +
                       "  </div>" +
                       " <div class='form-group'>" +
                       " <label></label>" +
                       "  <img style='width: 200px' src='" + good.image + "'>" +
                       " </div>" +
                       "<div class='form-group'>" +
                       " <label>进价:</label>" +
                       " <label>" + good.inPrice + "</label>" +
                       "</div>" +
                       " <div class='form-group'>" +
                       " <label>售价:</label>" +
                       "  <label>" + good.outPrice + "</label>" +
                       "    </div>" +
                       " <div class='form-group'>" +
                       " <label>销量:</label>" +
                       "  <label>" + good.sales + "</label>" +
                       "    </div>" +
                       " <div class='form-group'>" +
                       " <label>库存:</label>" +
                       " <label>" + good.stack + "</label>" +
                       "    </div>" +
                       " <div class='form-group'>" +
                       " <label>售出量:</label>" +
                       "  <input name='quantity'id='quantity'>" +
                       "    </div>" +
                       " <div class='form-group'>" +
                       " <label>备注:</label>" +
                       " <textarea name='remark' class='remark' id='remark'> </textarea>" +
                       "    </div>" +
                       "      </div>" +
                       "  </div>"
               "    </div>";
               bootbox.dialog({
                   message: str,
                   title: "售出详情&nbsp <span style='padding-left: 100px'>" + good.arenasName+"</span>",
                   buttons: {
                       "关闭": {
                           className: "btn-default",
                           callback: function () {
                           }
                       },
                       success: {
                           label: "确定售出",
                           className: "btn-primary",
                           callback: function () {
                               var remark = $('#remark').val();
                               var quantity = $('#quantity').val();
                               if (isPositiveNum(quantity)) {
                                   window.location.href = "inoutproduct?pid=" + id + "&aid=" +arenasId + "&remark=" + remark + "&quantity=" + quantity+ "&type=" + 0;
                                   Notify("售出成功！", 'top-right', '5000', 'success', 'fa-desktop', true);
                               }else {
                                   Notify("售出失败，售出数量只能为正整数", 'top-right', '5000', 'danger', 'fa-desktop', true);
                               }
                           }
                       }
                   }
               });
           }
       });
   }
</script>
</body>
<!--  /Body -->
</html>

