<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #province {
            width: 10%!important;
            float: left;
        }
        #detailarea{
            width: 90%!important;
            float: left;
        }
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加商品</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        商品
        <small>
            Modify
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">修改商品</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="<%=base%>/ctl/goods/saveProduct" method="post">
                    <input name="id" value="${product.id}" type="hidden">
                    <div class="form-group">
                        <label>商品名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="name" id="name" type="text" maxlength="32" value="${product.name}"/>
                                </div>
                            </span>
                    </div>
  <%--                  <div class="form-group">
                        <label>所属场馆</label>
                        <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                   <select id="areans" name="areansId" onchange="fn_setAreansId()" class="form-control">
                                <option value="-1" selected>全部</option>
                                    </select>
                                </div>
                            </span>

                    </div>--%>
                    <div class="form-group">
                        <label>生产公司</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <input class="form-control" name="company" id="company" type="text" maxlength="32" value="${product.company}"/>
                            </div>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>进价</label>
                             <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="inPrice" id="inPrice" type="text" maxlength="32" value="${product.inPrice}"/>
                                </div>
                            </span>
                    </div>
                        <div class="form-group">
                            <label>售价</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="outPrice" id="outPrice" type="text" maxlength="32" value="${product.outPrice}"/>
                                </div>
                            </span>
                        </div>
                    <div class="form-group">
                        <label>商品图片</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" multiple class="file-loading" >
                        </div>
                            <span class="input-icon icon-right">
                            	<div class="input-group" style="margin-top: 5px;">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input  id="filePath" type="hidden" name="image" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>商品描述</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="description" id="description" maxlength="200">${product.description}</textarea>
                            </div>
                        </span>
                    </div>
                  <input type="submit" value="确认" id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    $(function(){
        getBannerInfo();//获取数据方法
    })
    var fillPath='',initialPreview=[];
    var url=window.document.location.href;
    var pName=window.document.location.pathname;
    var localhostPaht=url.substring(0,url.indexOf(pName))+'/basketball-api/';
    var urlPattern = /^((http)?:\/\/)[^\s]+/;
    function getBannerInfo(){
        for(var i=0;i<1;i++){  //把图片循环读取出来
            if(urlPattern.test('${product.image}')){
                fillPath='${product.image}';
            }else{
                fillPath=localhostPaht + '${product.image}';
            }
            initialPreview[i] = '<img width="auto" height="160" class="file-preview-image"  src="'+fillPath+'">'+
                    '<div class="file-thumbnail-footer">'+
                    '<div class="file-caption-name" title="teacher2.jpg" style="width: 250px;"></div>'+
                    '<div class="file-actions">'+
                    '<div class="file-footer-buttons">'+
                    '<button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file" onclick="fn_remove()">'+
                    '<i class="glyphicon glyphicon-trash text-danger"></i>'+
                    '</button>'+
                    '</div>'+
                    '<div class="file-upload-indicator" tabindex="-1" title="Not uploaded yet">'+
                    '<i class="glyphicon glyphicon-hand-down text-warning"></i>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '</div>'+
                    '</div>';
            $("#filePath").val(fillPath);
        }
        initFileUpload(initialPreview);  //初始化图片上传插件方法
        $('.kv-file-remove').each(function(index){
            $(this).click(function(){
                $(this).parent().parent().parent().parent().fadeOut('slow',function(){
                    var imgSrc = $(".file-preview-image").eq($(this).index()).attr("src");
                    $(this).remove();
                    $('.file-caption-name').attr('name',$(".kv-file-remove").length+' file selected'),
                            $('.file-caption-name').html('<span class="glyphicon glyphicon-file kv-caption-icon"></span>'+$(".kv-file-remove").length+' file selected');
                    $('.goodsImage').each(function() {
                        var fid = $(this).val();
                        if (imgSrc.indexOf(fid) > 0) {
                            $(this).remove();
                        }
                    });
                });
            })
        })
    }


    function initFileUpload(initialPreview) {
        $("#iconUpload").fileinput({
            language:'zh',
            initialPreview: initialPreview,
            uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
            allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
            maxFileSize : 1024,//上传图片的最大限制  50KB
            overwriteInitial:true,
            initialPreviewShowDelete:true,
            minImageWidth: 50,
            minImageHeight: 50
        }).on("fileuploaded", function (event, data, previewId, index) {
            if (null != data) {
                fillPath = data.response.data;
                $("#filePath").val(fillPath);
            } else {
                Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        });

    }


    $("#name").blur(function(){
        var name = $("#name").val();
        if(name ==""){
            Notify("请商品名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
        } else {
            $.get("<%=base%>/ctl/goods/checkName",{
                name:name
            },function(json){
                if (json) {
                    Notify("商品名称可用", 'top-right', '5000', 'success', 'fa-desktop', true);
                } else {
                    Notify("商品名称已存在，请重新输入", 'top-right', '5000', 'danger', 'fa-desktop', true);
                }
            })
        }
    })

    $('#confirmBtn').click(function(){
        var name = $("#name").val();
        var inPrice = $("#inPrice").val();
        var outPrice = $("#outPrice").val();
        var areansId = $("#areans").val();
        var image = $("#filepath").val();
        if(name ==""){
            Notify("请填写商品名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }

        if(inPrice ==""){
            Notify("请填写进价", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(outPrice ==""){
            Notify("请填写售价", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(areansId=='-1'){
            Notify("请选择球场", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if (fillPath==""){
            Notify("请先上传图片", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
    function fn_remove() {
        $("#filePath").val("");
    }
</script>
</body>
<!--  /Body -->
</html>

