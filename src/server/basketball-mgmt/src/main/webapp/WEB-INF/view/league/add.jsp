<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
        #province,#city,#area {
            width: 33.3% !important;
            float: left;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加赛事</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加赛事</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post">
                    <div class="form-group">
                        <label>賽事logo</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" class="file-loading" multiple data-min-file-count="1" data-max-file-count="3" />
                        </div>
                        <div class="input-icon icon-right">
                            <span class="input-group" style="margin-top: 5px;">
                                <input type="hidden" id="filePath" type="text" name="logo" />
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>赛事名称</label>
                        <div class="input-icon icon-right">
                            <span class="input-group">
                           		 <span class="input-group-addon">
                                       <i class="fa fa-laptop"></i>
                                   </span>
                                   <input class="form-control" name="name" id="name" type="text" maxlength="32" />
                            </span>
                        </div>
                    </div>
                     <div class="form-group">
                        <label>阶段<span style="color:red">&nbsp;new</span></label>
                        <select id="stageId" name="stageId" class="form-control">
                            <option disabled selected>请选择阶段</option>
                            <c:forEach items="${stageList}" var="stage">
                                <option value="${stage.id}">
                                    ${stage.name}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>开始时间</label>
                       	<div class="input-icon icon-right">
                            <span class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="startTime" id="startTime" type="text" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" placeholder="年/月/日" />
                            </span>
						</div>
                    </div>
                    <div class="form-group">
                        <label>结束时间</label>
                        <div class="input-icon icon-right">
                            <span class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="endTime" id="endTime" type="text" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" placeholder="年/月/日" />
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>参赛球队</label>
                            <span class="input-icon icon-right">
                            	<select id="teamSelection" name="teamIds" multiple="multiple" class="form-control">

                                </select>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>赛制</label>
                        <span class="input-icon icon-right">
                        	<select id="matchFormat" name="matchFormat" class="form-control">
                        		<option value="1" >一人制</option>
                                <option value="2" >二人制</option>
                                <option value="3" >三人制</option>
                                <option value="4" >四人制</option>
                                <option value="5" selected="selected">五人制</option>
                            </select>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>比赛的节数<span style="color:red">&nbsp;new</span></label>
                        <select id="quarterCount" name="quarterCount" class="form-control">
                            <option value="2">2节</option>
                            <option value="4" selected="selected">4节</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>每节的时间<span style="color:red">&nbsp;new</span></label>
                        <select id="quarterTime" name="quarterTime" class="form-control">
                        	<option disabled selected>请选择每节的时间</option>
                            <option value="600">10分钟</option>
                            <option value="720" selected="selected">12分钟</option>
                            <option value="1200">20分钟</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>赛事详情</label>
                        <div class="input-icon icon-right">
                        	<span class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="details" id="details" maxlength="200"></textarea>
                        	</span>
				 		</div>
                    </div>
                    <input type="submit" value="确认" target="iframe" id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    var flag = false;
    $(function(){
        $("#teamSelection").select2({
            placeholder: "请选择球队",
            allowClear: true
        });
        
        <!--获取队伍列表-->
        $.get("getTeams",function(json){
            $("#teamSelection").empty();
            if (json != "" || null != json) {
            	$("#teamSelection").append('<option disabled selected>请选择球队</option>');
                $.each(json,function(index,team){
                    $("#teamSelection").append('<option value="' + team.id + '" >' + team.name + '</option>');
                })

            }
        })

        getBannerInfo();
    })

    var fillPath='',initialPreview=[];
    var url=window.document.location.href;
    var pName=window.document.location.pathname;
    var localhostPaht=url.substring(0,url.indexOf(pName))+'/basketball-api/';
    var urlPattern = /^((http)?:\/\/)[^\s]+/;
    function getBannerInfo(){
        initFileUpload();  //初始化图片上传插件方法
        $('.kv-file-remove').each(function(index){
            $(this).click(function(){
                $(this).parent().parent().parent().parent().fadeOut('slow',function(){
                    var imgSrc = $(".file-preview-image").eq($(this).index()).attr("src");
                    $(this).remove();
                    $('.file-caption-name').attr('name',$(".kv-file-remove").length+' file selected'),
                            $('.file-caption-name').html('<span class="glyphicon glyphicon-file kv-caption-icon"></span>'+$(".kv-file-remove").length+' file selected');
                    $('.goodsImage').each(function() {
                        var fid = $(this).val();
                        if (imgSrc == fid) {
                            $(this).remove();
                        }
                    });
                });
            })
        })
    }


    function initFileUpload() {
        $("#iconUpload").fileinput({
            language:'zh',
            uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
            allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
            maxFileSize : 1024,//上传图片的最大限制  50KB
            overwriteInitial:true,
            initialPreviewShowDelete:true,
            minImageWidth: 50,
            minImageHeight: 50
        }).on("fileuploaded", function (event, data, previewId, index) {
            if (null != data) {
                fillPath = data.response.data;
                $("#filePath").val(fillPath);
            } else {
                Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        });

    }

    $('#confirmBtn').click(function(){
        var imgUrl = $("#filePath").val();//图片url
        var name = $("#name").val();        //赛事名称
        var beginDate = $("#startTime").val();
        var endDate = $("#endTime").val();

        if(imgUrl ==""){
            Notify("请上传赛事Logo", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(name ==""){
            Notify("请填写赛事名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(beginDate > endDate){
            Notify("开始日期不能大于结束日期", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
</script>
</body>
</html>

