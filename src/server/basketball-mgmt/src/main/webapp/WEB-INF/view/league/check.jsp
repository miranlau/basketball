<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>
    <style>
        textarea {
            resize: none;
            height: 150px !important;
            width: 350px !important;
            overflow: auto;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">审核赛事</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
        <small>
            Play
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">审核赛事</span>
        </div>
        <div class="widget-body">
            <form action="checkLeague" method="post" id="check">
                <div class="box-body">
                    <div>
                        <h2>赛事&nbsp;${league.name}</h2>
                        <div class="form-group">
                            <label>logo</label>
                            <label><img src="${league.image}" /></label>
                        </div>
                        <div class="form-group">
                            <label>公司:</label>
                            <label>${league.company.name}</label>
                        </div>
                        <div class="form-group">
                            <label>报名截止时间:</label>
                            <label><fmt:formatDate value="${league.deadline}" pattern="yyyy-MM-dd HH:mm:ss" /> </label>
                        </div>
                        <div class="form-group">
                            <label>赛事开始时间:</label>
                            <label><fmt:formatDate value="${league.begindate}" pattern="yyyy-MM-dd HH:mm:ss" /> </label>
                        </div>
                        <div class="form-group">
                            <label>赛事结束时间:</label>
                            <label><fmt:formatDate value="${league.enddate}" pattern="yyyy-MM-dd HH:mm:ss" /> </label>
                        </div>
                        <div class="form-group">
                            <label>赛制:</label>
                            <label>${league.matchtype}人制</label>
                        </div>
                        <div class="form-group">
                            <label>地区:</label>
                            <label>${league.areaname}</label>
                        </div>
                        <div class="form-group">
                            <label>赛事详情:</label>
                            <label><textarea class="form-control" disabled>${league.details}</textarea></label>
                        </div>
                        <div class="form-group">
                            <label>审核意见:</label>
                            <label><textarea class="form-control" name="judgment" maxlength="200">${league.judgment}</textarea>
                                    <input type="hidden" name="id" value="${league.id}">
                                    <input type="hidden" name="result">
                            </label>
                        </div>
                    </div>
                    <div>
                        <a target="iframe" id="yes" onclick="submit(1);" class="btn btn-primary">通过</a>
                        <a target="iframe" id="no" onclick="submit(0);" class="btn btn-primary">未通过</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script>
    function submit(data){
        $("input[name='result']").val(data);
        $("#check").submit();
    }
</script>
</body>
<!--  /Body -->
</html>
