<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">赛事统计</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
        <small>
            Play
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">赛事统计</span>
            <%--<div class="widget-buttons">--%>
                <%--<a href="" target="iframe" class="btn btn-info">导出</a>--%>
            <%--</div>--%>
        </div>
        <div class="widget-body">
            <div class="tabbable">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a data-toggle="tab" href="#home">积分榜</a></li>
                    <li class="tab-red"><a data-toggle="tab" href="#shooter">得分榜</a></li>
                    <li class="tab-red"><a data-toggle="tab" href="#card">抢断牌</a></li>
                    <li class="tab-red"><a data-toggle="tab" href="#save">篮板榜</a></li>
                    <li class="tab-red"><a data-toggle="tab" href="#assists">助攻榜</a></li>
                    <li class="tab-red"><a data-toggle="tab" href="#cover">盖帽榜</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane in active">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>排名</th>
                                <th>球队</th>
                                <th>胜</th>
                                <th>负</th>
                                <th>积分</th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${leagueBang.scoresList}" var="point" varStatus="status">
                                    <tr>
                                        <td>${status.index + 1}</td>
                                        <td>${point.teamName}</td>
                                        <td>${point.win}</td>
                                        <td>${point.lose}</td>
                                        <td>${point.points}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div id="shooter" class="tab-pane">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>排名</th>
                                <th>球员</th>
                                <th>球队</th>
                                <th>场均得分</th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:if test="${leagueBang.pensoerScoreList != null}">
                                <c:forEach items="${leagueBang.pensoerScoreList}" var="shooter" varStatus="status">
                                    <tr>
                                        <td>${status.index + 1}</td>
                                        <td>${shooter.name}</td>
                                        <td>${shooter.teamName}</td>
                                        <td>${shooter.avgScore}</td>
                                    </tr>
                                </c:forEach>
                                    </c:if>
                            </tbody>
                        </table>
                    </div>
                    <div id="assists" class="tab-pane">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>排名</th>
                                <th>球员</th>
                                <th>球队</th>
                                <th>场均助攻</th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:if test="${leagueBang.helpScoreList != null}">
                                <c:forEach items="${leagueBang.helpScoreList}" var="helpScoreList" varStatus="status">
                                    <tr>
                                        <td>${status.index + 1}</td>
                                        <td>${helpScoreList.name}</td>
                                        <td>${helpScoreList.teamName}</td>
                                        <td>${helpScoreList.avgHelp}</td>
                                    </tr>
                                </c:forEach>
                                    </c:if>
                            </tbody>
                        </table>
                    </div>
                    <div id="save" class="tab-pane">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>排名</th>
                                <th>球员</th>
                                <th>球队</th>
                                <th>场均篮板</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:if test="${leagueBang.backboardList != null}">
                            <c:forEach items="${leagueBang.backboardList}" var="backboardList" varStatus="status">
                                <tr>
                                    <td>${status.index + 1}</td>
                                    <td>${backboardList.name}</td>
                                    <td>${backboardList.teamName}</td>
                                    <td>${backboardList.avgBackboard}</td>
                                </tr>
                            </c:forEach>
                                </c:if>
                            <%--<tr>--%>
                                <%--<td colspan="4">暂时没有数据</td>--%>
                            <%--</tr>--%>
                            </tbody>
                        </table>
                    </div>
                    <div id="card" class="tab-pane">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>排名</th>
                                <th>球员</th>
                                <th>球队</th>
                                <th>场均抢断</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:if test="${leagueBang.stealsList != null}">
                            <c:forEach items="${leagueBang.stealsList}" var="stealsList" varStatus="status">
                                <tr>
                                    <td>${status.index + 1}</td>
                                    <td>${stealsList.name}</td>
                                    <td>${stealsList.teamName}</td>
                                    <td>${stealsList.avgSteals}</td>
                                </tr>
                            </c:forEach>
                                </c:if>
                            </tbody>
                        </table>
                    </div>
                    <div id="cover" class="tab-pane">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>排名</th>
                                <th>球员</th>
                                <th>球队</th>
                                <th>场均盖帽</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:if test="${leagueBang.coverList != null}">
                                <c:forEach items="${leagueBang.coverList}" var="coverList" varStatus="status">
                                    <tr>
                                        <td>${status.index + 1}</td>
                                        <td>${coverList.name}</td>
                                        <td>${coverList.teamName}</td>
                                        <td>${coverList.avgCover}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>
<script>
    var totalpage=10;
    $(function(){

    })
</script>
</body>
<!--  /Body -->
</html>

