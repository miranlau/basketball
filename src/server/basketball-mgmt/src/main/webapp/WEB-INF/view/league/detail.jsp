<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>
    <style>
        img{
            height: 60px;
            width: 60px;
        }

    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">赛事详情</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
        <small>
            Play
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">赛事详情</span>
            <div class="widget-buttons">
                <c:if test="<%=Admin.COMPANY_ADMIN.equals(admin.getType())%>">
                    <a href="<%=base%>/ctl/match/addMatch?leagueId=${league.id}" target="iframe" class="btn btn-info">添加比赛</a>
                </c:if>
            </div>
        </div>
        <div class="widget-body">
            <div class="box-body">
                <div align="center">
                    <h2>${league.name}</h2>
                    <div class="form-group">
                        <label>logo</label>
                        <label><img src="${league.image}" /></label>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>主队</th>
                    <th>客队</th>
                    <th>状态</th>
                    <th>比赛日期</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${matches}" var="matches">
                        <tr>
                            <td>${matches.id}</td>
                            <td>${matches.home.name}</td>
                            <td>${matches.visiting.name}</td>
                            <td>
                                <c:if test="${matches.status == 0}">
                                    约赛中
                                </c:if>
                                <c:if test="${matches.status == 10}">
                                    已匹配
                                </c:if>
                                <c:if test="${matches.status == 20 or matches.status == 21}">
                                    比赛中
                                </c:if>
                                <c:if test="${matches.status == 100}">
                                    结束赛后
                                </c:if>
                            </td>
                            <td>
                                <fmt:formatDate value="${matches.date}" pattern="yyyy-MM-dd HH:mm:ss" />
                            </td>
                            <td>
                                <a href="../match/detail?matchId=${matches.id}" target="iframe" class="btn btn-info">详情</a>
                                <a href="../match/editPage?matchId=${matches.id}&league=${matches.lid}" target="iframe" class="btn btn-info">修改</a>
                                <%--<a href="../data/list.html" target="iframe" class="btn btn-info">查看</a>--%>
                                <a  onclick="beforDel(${matches.id},${matches.lid})" class="btn btn-danger">删除</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script>
    /**
     * 删除之前的判断
     */
    function beforDel(matchId,leagueId) {
        if (confirm("确定删除？")) {
            window.location.href="../match/delete?matchId="+matchId+"&league="+leagueId;
        }
    }
</script>
</body>
<!--  /Body -->
</html>

