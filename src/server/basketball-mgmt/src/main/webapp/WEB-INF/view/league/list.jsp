<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon" rel="shortcut icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看赛事</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看赛事</span>
            <div class="widget-buttons">
                <c:if test="<%=Admin.SUPER_ADMIN.equals(admin.getType())%>">
                    <a href="add" target="iframe" class="btn btn-info">添加赛事</a>
                </c:if>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>赛事名称：
                        <input type="text" name="name" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <select id="status" name="status">
                            <option value="-1">全部</option>
                            <option value="0">报名中</option>
                            <option value="1">报名结束</option>
                            <option value="2">比赛中</option>
                            <option value="3">赛事结束</option>
                            <option value="6">待审核</option>
                            <option value="4">审核未通过</option>
                        </select>
                    </label>
                    <label>
                        <a class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>图标</th>
                    <th>赛事名称</th>
                    <th>阶段</th>
                    <th>球队</th>
                    <th>开始时间</th>
                    <th>结束时间</th>
                    <th>赛制</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/js/jqPaginator.js"></script>
<script src="<%=base%>/resource/js/jqPage.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>
<script>
   var totalpage= '';
   var nowPage = 1;
   var total = '';
   var inputVal = '';
   var status = '';
    $(function(){
        addpage(); //加载分页方法
        getData(1);

    })

   var deleteById = function() {
    	if(confirm("确定删除吗?") == false) {
     	   return false;
        }
    	return true;
   };
  
   function getData(page,inputVal,status) {
        var listHtml = '';
        $.ajax({
            type:"POST",
            async:true,//异步请求
            url:'<%=base%>/ctl/league/leagueList',
            data:{pageNumber:page,name:inputVal,status:status},
            dataType:"json",
            timeout:5000,
            success:function(json) {
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0){
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){

                            listHtml += '<tr>'
                                    + '<td>' + obj.id +'</td>'
                                    + '<td><img src="' + obj.logo + '" /></td>'
                                    + '<td><span data-toggle="popover-hover" data-title="赛事名称" data-content="' + obj.name +'">' + obj.name + '</span></td>'
                                    + '<td><span data-toggle="popover-hover" data-title="阶段" data-content="' + moveNull(obj.stageName) +'">' + moveNull(obj.stageName) + '</span></td>'
                                    + '<td><span data-toggle="popover-hover" data-title="球队" data-content="">' + obj.teams + '</span></td>'
                                    + '<td>' + getTime(obj.startTime) + "</td>"
                                    + '<td>' + getTime(obj.endTime) + "</td>"
                                    + '<td>' + obj.matchFormat + "人制</td>"
                                    + '<td>' + convertStatus(obj.status) + "</td>"
                                    + '<td>';
                            if (obj.status != 3) {
                                listHtml += '<a href="updateStatus?leagueId=' + obj.id + '&status=' + obj.status + '" target="iframe" class="btn btn-darkorange">' + leagueStatus(obj.status) + '</a>';
                            } else {
                            	listHtml += '<a disabled="disabled" href="updateStatus?leagueId=' + obj.id + '&status=' + obj.status + '" target="iframe" class="btn btn-darkorange">' + leagueStatus(obj.status) + '</a>';
                            }
                            listHtml += '<a href="detail?id=' + obj.id + '" target="iframe" class="btn btn-info">详情</a>';

                            <%
                                if (Admin.SUPER_ADMIN.equals(admin.getType())){
                            %>
                                    listHtml += '<a href="modify?id=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>';
                                    listHtml += '<a href="season?id=' + obj.id + '" target="iframe" class="btn btn-info">赛季</a>';
                                    listHtml += '<a href="delete?id=' + obj.id + '" class="btn btn-danger delete" onclick="return deleteById()">删除</a>';

                            <%
                                }
                            %>
                                    + '</td></tr>';

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    getData(num,inputVal,status);
                                }
                            }
                        });
                    } else {
                        listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })

        $("#qShoppingTemplet").click(function(){
            inputVal = $("#name").val();
            if ($("#status").val() != -1) {
                status = $("#status").val();
                getData(1,inputVal,status);
            } else {
                getData(1,inputVal);
            }
        })

        function convertStatus(status){
            if (status == 0) {
                return '报名中';
            } else if (status == 1) {
                return '报名结束';
            } else if (status == 2) {
                return '比赛中';
            } else if (status == 3) {
                return '赛事结束';
            } else if (status == 4) {
                return '审核未通过';
            } else if (status == 6) {
                return '待审核';
            } else {
                ;
            }
        }
    }
//    修改赛事状态显示
    function leagueStatus(status) {
        if (status == 0) {
            return '设置报名截止';
        } else if (status == 1) {
            return '设置赛事开始';
        } else if (status == 2) {
            return '设置赛事结束';
        } else if (status == 3) {
            return '赛事已结束';
        } else if (status == 4) {
            return '审核未通过';
        } else if (status == 6) {
            return '设置审核通过';
        } else {
            ;
        }
    }
    
    function moveNull(data) {
        if (data == null) {
            return "";
        } else {
            return data;
        }
    }
</script>
</body>
<!--  /Body -->
</html>

