<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
    </style>
</head>
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">编辑赛事</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">编辑赛事</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="edit" method="post">
                    <div class="form-group">
                        <label>赛事logo</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" multiple class="file-loading" data-min-file-count="1" data-max-file-count="3" />
                        </div>
                        <div class="input-icon icon-right">
                            <span  class="input-group" style="margin-top: 5px;">
                                 <input type="hidden" id="filePath" type="text" name="logo" />
                                 <input type="hidden" id="id" type="text" name="id" value="${league.id}"/>
                                 <input type="hidden" type="text" name="status" value="${league.status}"/>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>赛事名称</label>
                        <div class="input-icon icon-right">
                        	<span class="input-group">
                        		 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <input class="form-control" id="eventName" name="name" value="${league.name}" type="text">
                        	</span>
	                   </div>
                    </div>
                    <div class="form-group">
                        <label>阶段<span style="color:red">&nbsp;new</span></label>
                        <select id="stageId" name="stageId" class="form-control">
                            <option disabled selected>请选择阶段</option>
                            <c:forEach items="${stageList}" var="stage">
                                <option value="${stage.id}" <c:if test="${stage.id == league.stageId}">selected</c:if>>${stage.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>开始时间</label>
                        	<div class="input-icon icon-right">
                            	<span class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="startTime" value="<fmt:formatDate value="${league.startTime}" pattern="YYYY-MM-dd hh:mm:ss" />" type="text" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" />
                            	</span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>结束时间</label>
                        	<div  class="input-icon icon-right">
                            	<span class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="endTime" value="<fmt:formatDate value="${league.endTime}" pattern="YYYY-MM-dd hh:mm:ss" />" type="text" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" />
                            	</span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>赛制<span style="color:red">&nbsp;new</span></label>
                        <select id="matchFormat" name="matchFormat" class="form-control">
                        	<option disabled selected>请选择参加比赛的人数</option>
                            <option value="1" <c:if test="${league.matchFormat == 1}">selected</c:if>>一人制</option>
                            <option value="2" <c:if test="${league.matchFormat == 2}">selected</c:if>>两人制</option>
                            <option value="3" <c:if test="${league.matchFormat == 3}">selected</c:if>>三人制</option>
                            <option value="4" <c:if test="${league.matchFormat == 4}">selected</c:if>>四人制</option>
                            <option value="5" <c:if test="${league.matchFormat == 5}">selected</c:if>>五人制</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>比赛的节数<span style="color:red">&nbsp;new</span></label>
                        <select id="quarterCount" name="quarterCount" class="form-control">
                        	<option disabled selected>请选择比赛有几节</option>
                            <option value="2" <c:if test="${league.quarterCount == 2}">selected</c:if>>2节</option>
                            <option value="4" <c:if test="${league.quarterCount == 4}">selected</c:if>>4节</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>每节的时间<span style="color:red">&nbsp;new</span></label>
                        <select id="quarterTime" name="quarterTime" class="form-control">
                        	<option disabled selected>请选择每节的时间</option>
                            <option value="600" <c:if test="${league.quarterTime == 600}">selected</c:if>>10分钟</option>
                            <option value="720" <c:if test="${league.quarterTime == 720}">selected</c:if>>12分钟</option>
                            <option value="1200" <c:if test="${league.quarterTime == 1200}">selected</c:if>>20分钟</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>参赛球队</label>
                            <span class="input-icon icon-right">
                            	<select id="teamSelection" multiple="multiple" class="form-control" name="teams">
                                    <c:forEach items="${league.teamList}" var="team">
                                        <option value="${team.id}" selected="selected">${team.name}</option>
                                    </c:forEach>
                                </select>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>赛事详情</label>
                        <div class="input-icon icon-right">
                            <span class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" id="details" name="details" maxlength="200"></textarea>
                        	</span>
                       	</div>
                    </div>
                    <input type="submit" id="confirmBtn" class="btn btn-info" value="确认" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/laydate/laydate.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    $(function(){
    	$("#matchFormat").find("option[value='${league.matchFormat}']").attr("selected",true);
    	$("#details").val('${league.details}');
    	
        $("#teamSelection").select2({
            placeholder: "请选择球队",
            allowClear: true
        });
        <!--获取队伍列表-->
        $.get("getTeams",function(json){
        	$("#teamSelection").empty();
            if (json != "" || null != json) {
                $.each(json,function(index,team){
                    $("#teamSelection").append('<option value="' + team.id + '" >' + team.name + '</option>');
                })
            }
        })
        getBannerInfo();//获取数据方法
    })
    var fillPath='',initialPreview=[];
    var url=window.document.location.href;
    var pName=window.document.location.pathname;
    var localhostPaht=url.substring(0,url.indexOf(pName))+'/basketball-api/';
    var urlPattern = /^((http)?:\/\/)[^\s]+/;
    function getBannerInfo(){
        for(var i=0;i<1;i++){  //把图片循环读取出来
            if(urlPattern.test('${league.logo}')){
                fillPath='${league.logo}';
            }else{
                fillPath=localhostPaht + '${league.logo}';
            }

            initialPreview[i] = '<img width="auto" height="160" class="file-preview-image"  src="'+fillPath+'">'+
                    '<div class="file-thumbnail-footer">'+
                    '<div class="file-caption-name" title="teacher2.jpg" style="width: 250px;"></div>'+
                    '<div class="file-actions">'+
                    '<div class="file-footer-buttons">'+
                    '<button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file">'+
                    '<i class="glyphicon glyphicon-trash text-danger"></i>'+
                    '</button>'+
                    '</div>'+
                    '<div class="file-upload-indicator" tabindex="-1" title="Not uploaded yet">'+
                    '<i class="glyphicon glyphicon-hand-down text-warning"></i>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '</div>'+
                    '</div>';
            $("#filePath").val(fillPath);
        }
        initFileUpload(initialPreview);  //初始化图片上传插件方法
        $('.kv-file-remove').each(function(index){
            $(this).click(function(){
                $(this).parent().parent().parent().parent().fadeOut('slow',function(){
                    var imgSrc = $(".file-preview-image").eq($(this).index()).attr("src");
                    $(this).remove();
                    $('.file-caption-name').attr('name',$(".kv-file-remove").length+' file selected'),
                            $('.file-caption-name').html('<span class="glyphicon glyphicon-file kv-caption-icon"></span>'+$(".kv-file-remove").length+' file selected');
                    $('.goodsImage').each(function() {
                        var fid = $(this).val();
                        if (imgSrc == fid) {
                            $(this).remove();
                        }
                    });
                });
            })
        })
    }


    function initFileUpload(initialPreview) {
        $("#iconUpload").fileinput({
            language:'zh',
            initialPreview: initialPreview,
            uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
            allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
            maxFileSize : 1024,//上传图片的最大限制  50KB
            overwriteInitial:true,
            initialPreviewShowDelete:true,
            minImageWidth: 50,
            minImageHeight: 50
        }).on("fileuploaded", function (event, data, previewId, index) {
            if (null != data) {
                fillPath = data.response.data;
                $("#filePath").val(fillPath);
            } else {
                Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        });

    }

    $('#confirmBtn').click(function(){

    })
</script>
</body>
</html>
