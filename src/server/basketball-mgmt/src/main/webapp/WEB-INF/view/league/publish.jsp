<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <link href="<%=base%>/resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #province {
            width: 10%!important;
            float: left;
        }
        #detailarea{
            width: 90%!important;
            float: left;
        }
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
        textarea {
            resize: none;
            height: 200px !important;
            overflow: auto;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li><a href="">赛事详情</a></li>
        <li><a href="">动态列表</a></li>
        <li class="active">发布动态</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛事
        <small>
            Play
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">发布动态</span>
            <span class="widget-caption"></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="saveInfo" method="post">
                    <div class="form-group">
                        <label>发布内容</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <textarea class="form-control" name="content" id="content" type="text" maxlength="250" ></textarea>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>图片上传（请上传高度为200左右的图片）</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" multiple class="file-loading" >
                        </div>
                        <div id="imageDiv"></div>
                    <span class="input-icon icon-right">
                        <div class="input-group" style="margin-top: 5px;">
                            <span class="input-group-addon">
                            <i class="fa fa-laptop"></i>
                            </span>
                            <input type="hidden"  type="text" name="leagueId" VALUE="${leagueId}"/>
                        </div>
                    </span>
                    </div>
                    <input type="submit" value="发布" target="iframe"  id="confirmBtn" class="btn btn-info" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script src="<%=base%>/resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="<%=base%>/resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    $(function() {

    })

    var imageCount=0;
    $("#iconUpload").fileinput({
        language:'zh',
        uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
        showPreview : true,
        showRemove: false,
        maxFileSize : 10240,  //上传图片的最大限制  50KB
        allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
        initialCaption: "请选择图片"
    });
    $("#iconUpload").on("fileuploaded", function (event, data, previewId, index) {
        if (null != data) {
            fillPath = data.response.data;
            $("#imageDiv").append("<input type='hidden' class='goodsImage' name='pushImgs' value='"+fillPath+"'>");
            imageCount ++;
            $('.kv-file-remove').eq(index).click(function(){
                $("#imageDiv input").eq(index).remove();
            })
        } else {
            Notify("上传图片失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    });


    $('#confirmBtn').click(function(){
        var content = $("#content").val();
        var $images = $(".goodsImage");
        if(content ==""){
            Notify("请填写动态内容", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }

        if ($images.size() == 0) {
            Notify("请先上传图片!", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }else if ($images.size() > 9) {
            Notify("图片不能超过九张!", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }else{
            for (var i = 0; i < $images.size(); i ++) {
                imgArr.push($images[i].value);
            }
        }
    })
</script>
</body>
<!--  /Body -->
</html>

