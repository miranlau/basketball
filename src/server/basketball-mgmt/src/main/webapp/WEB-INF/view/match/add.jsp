<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=base%>/resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="<%=base%>/resource/css/common.css" rel="stylesheet" />
    <script src="<%=base%>/resource/assets/js/skins.min.js"></script>

    <style>
        input[type=checkbox], input[type=radio]{
            opacity: 1;
            position: inherit !important;
            vertical-align: middle;
            margin: 0 0 0 5px;
        }
    </style>
</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li><a href="">赛事详情</a></li>
        <li class="active">添加比赛</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        比赛
        <small>
            Play
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加比赛</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post">
                    <div class="form-group">
                        <label>赛事</label>
                            <span class="input-icon icon-right">
                            	<select id="leagueSelect" name="leagueId" class="form-control" >
                            		<option selected value="-1">请选择赛事</option>
                                </select>
                            </span>
                    </div>
                    <!-- 
                    <div class="form-group">
                        <label>赛季</label>
                        <span class="input-icon icon-right">
                            <select id="seasonSelect" name="seasonId" class="form-control" >
                                <option selected value="-1">请选择赛季</option>
                            </select>
                        </span>
                    </div>
                     -->
                    <div class="form-group">
                        <label>轮次<span style="color:red">&nbsp;new</span></label>
                        <select id="roundId" name="roundId" class="form-control">
                        	<option disabled selected>请选择轮次</option>
                            <c:forEach items="${matchRoundList}" var="matchRound">
                                <option value="${matchRound.id}">${matchRound.name}</option>
                            </c:forEach>
                        </select>
                    </div>
					<div class="form-group">
                        <label>场序(1-200)<span style="color:red">&nbsp;new</span></label>
                        <div class="input-icon icon-right">
                        	<span class="spinner">
                                <input type="number" name="sequence" id="sequence" class="form-control" />
	                        </span>
                         </div>
                    </div>
                    <div class="form-group">
                        <label>主队</label>
                        <span class="input-icon icon-right">
                            <select id="homeSelect" name="homeId" class="form-control" >
                                <option selected value="-1">请选择主队</option>
                            </select>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>主队报名队员</label>
                        <span class="input-icon icon-right">
                        	<select id="homePlayerSelect" name="homePlayerIdList" multiple="multiple" class="form-control"></select>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>主队记分员手机号</label>
                        <input class="form-control input-sm" id="homeRecorderMobile" name="homeRecorderMobile" type="text" maxlength="30" placeholder="请输入用户电话">
                        <label id="homeRecorderPrompt"></label>
                        <input type="hidden" id="homeRecorderId" type="text" name="homeRecorderId" />
                    </div>
                    <div class="form-group">
                        <label>客队</label>
                        <span class="input-icon icon-right">
                            <select id="visitingSelect" name="visitingId" class="form-control" >
                                <option selected value="-1">请选择客队</option>
                            </select>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>客队报名队员</label>
                        <span class="input-icon icon-right">
                        	<select id="visitingPlayerSelect" name="visitingPlayerIdList" multiple="multiple" class="form-control"></select>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>客队记分员手机号</label>
                        <input class="form-control input-sm" id="visitingRecorderMobile" name="visitingRecorderMobile" type="text" maxlength="30" placeholder="请输入用户电话">
                        <label id="visitingRecorderPrompt"></label>
                        <input type="hidden" id="visitingRecorderId" type="text" name="visitingRecorderId" />
                    </div>
                    <div class="form-group">
                        <label>比赛日期</label>
                        <div class="input-icon icon-right">
                          	<span class="input-group">
                           		<span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                </span>
                                <input name="matchTime" class="form-control date-picker" id="date-picker" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:MM:ss'})"/>
                           </span>
                       </div>
                    </div>
                    <div class="form-group">
                        <label>比赛球场</label>
                            <span class="input-icon icon-right">
                            	<select id="venueSelect" name="venueId" class="form-control">
                                    <option selected disabled value="-1">请选择球场</option>
                                </select>
                            </span>
                    </div>
                    <input type="submit" target="iframe"  id="confirmBtn" class="btn btn-info" value="确认"/>
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="<%=base%>/resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="<%=base%>/resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="<%=base%>/resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="<%=base%>/resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="<%=base%>/resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="<%=base%>/resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="<%=base%>/resource/js/commons.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>
<script>

	$.get('../league/getAll',function(json){
        $("#leagueSelect").empty();
        if (json != "" || null != json) {
            $("#leagueSelect").append('<option disabled selected>请选择赛事</option>');
            $.each(json,function(index,league){
                $("#leagueSelect").append('<option value="' + league.id + '" >' + league.name + '</option>');
            })
        }
    })
    
    $.get("../venue/getAllVenues",function(json){
        $("#venueSelect").empty();
        if (json != "" || null != json) {
            $("#venueSelect").append('<option disabled selected>请选择球场</option>');
            $.each(json,function(index,venue){
                $("#venueSelect").append('<option value="' + venue.id + '" >' + venue.name + '</option>');
            })
        }
    })
    
  	$('#leagueSelect').change(function(){
   		var leagueId=$('#leagueSelect').val();
   		updateSelect(leagueId);
   	});
	
 	//主队记分员输入时
    $("#homeRecorderMobile").change(function(){
    	var mobile = $("#homeRecorderMobile").val();
        checkMobile(mobile, true);
    })

 	//客队记分员输入时
    $("#visitingRecorderMobile").change(function(){
    	var mobile = $("#visitingRecorderMobile").val();
        checkMobile(mobile, false);
    })
    
    //主队选择改变时
    $("#homeSelect").change(function(){
        var zTeamId = $("#homeSelect :selected").val();
        //获取该队伍成员
        $.get("<%=base%>/ctl/player/getPlayers",{
            teamId:zTeamId
        },function(json){
            if (json.code == 0 && json.flag == 0) {
                if (json.data.length > 0) {
                    var str='';
                    $.each(json.data, function (index, player) {
                        str+='<option selected value="' + player.id + '">' + player.name + '</option>';
                        $("#homePlayerSelect").html(str);
                        $("#homePlayerSelect").select2({
                            placeholder: "请选择主队报名",
                            allowClear: true
                        });
                    })
                }
            }
        })
    })
    //客队选择改变时
    $("#visitingSelect").change(function(){
        var kTeamId = $("#visitingSelect :selected").val();
        //获取该队伍成员
        $.get("<%=base%>/ctl/player/getPlayers", {
            teamId:kTeamId
        },function(json){
            if (json.code == 0 && json.flag == 0) {
                if (json.data.length > 0) {
                    var str='';
                    $.each(json.data, function (index, player) {
                        str+='<option selected value="' + player.id + '">' + player.name + '</option>';
                        $("#visitingPlayerSelect").html(str);
                        $("#visitingPlayerSelect").select2({
                            placeholder: "请选择客队报名",
                            allowClear: true
                        });
                    })
                }
            }
        })
    });
 	
    function checkMobile(mobile, isHome) {
 	     $.get("../user/queryUserByMobile",{ 
 	    	mobile:mobile
          },function(json){
              if (json.code == 0 && json.flag == 0) {
                  if (json.data != null) {
                      // 正确
                      if (isHome) {
                    	  $("#homeRecorderPrompt").html("合法用户，ID=" + json.data.id).css('color','green');
                    	  $("#homeRecorderId").val(json.data.id);
                      } else {
                    	  $("#visitingRecorderPrompt").html("合法用户，ID=" + json.data.id).css('color','green');
                    	  $("#visitingRecorderId").val(json.data.id);
                      }
                  } else {
                	  alert("非法");
                	  // 错误
                	  if (isHome) {
                    	  $("#homeRecorderPrompt").html("非法用户").css('color','red');
                    	  $("#homeRecorderId").val("");
                      } else {
                    	  $("#visitingRecorderPrompt").html("非法用户").css('color','red');
                    	  $("#visitingRecorderId").val("");
                      }
                  }
              }
          })
      }

      function updateSelect(leagueId) {  
 	     $.get("../league/getLeague",{ 
              leagueId:leagueId
          },function(json){
              $("#homeSelect").empty();
              $("#visitingSelect").empty();
              $("#seasonSelect").empty();
              if (json.code == 0 && json.flag == 0) {
                  if (json.data != null) {
                      $("#homeSelect").append('<option selected value="-1" disabled>请选择主队</option>');
                      $("#visitingSelect").append('<option selected value="-1" disabled>请选择客队</option>');
                      var league = json.data;
                      $.each(league.teamList, function (index, team) {
                          $("#homeSelect").append('<option value="' + team.id + '">' + team.name + '</option>');
                          $("#visitingSelect").append('<option value="' + team.id + '">' + team.name + '</option>');
                      })
                      $.each(league.seasonList, function (index, season) {
                          $("#seasonSelect").append('<option value="' + season.id + '">' + season.name + '</option>');
                      })
                  }
              }
          })
      }



</script>
</body>
<!--  /Body -->
</html>

