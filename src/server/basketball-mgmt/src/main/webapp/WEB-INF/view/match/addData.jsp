<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">新增数据</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        数据
        <small>
            Action
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">新增数据</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="addAction">
                    <input type="hidden" name="mid" value="${matchId}">
                    <div class="form-group">
                        <label>球队</label>
                            <span class="input-icon icon-right">
                            	<select id="teamName" class="form-control" name="tid">
                                    <%--<option value="${action.tid}" selected="selected">${action.teamName}</option>--%>
                                </select>
                            </span>
                    </div>
                    <div class="form-group" style="display: none">
                        <label>记录员<span></span></label>
                            <span class="input-icon icon-right">
                                <input type="text" id="recorderName" class="form-control" value="30001" name="uid" placeholder="请输入用户ID">
                            </span>
                    </div>
                    <div class="form-group">
                        <label id="playerA">球员A<span></span></label>
                            <span class="input-icon icon-right">
                            	<select id="teamPeopleA" class="form-control" name="player1">
                                    <%--<option value="${action.playerA.id}" selected="selected">${action.playerA.name}</option>--%>
                                </select>
                            </span>
                        <label id="playerB">球员B<span></span></label>
                        <span class="input-icon icon-right">
                            	<select id="teamPeopleB" class="form-control" name="player2">
                                    <%--<option value="${action.playerB.id}" selected="selected">${action.playerB.name}</option>--%>
                                </select>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>状态</label>
                            <span class="input-icon icon-right">
                            	<select id="state" class="form-control" name="status">

                                </select>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>比赛时间(秒)</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                    <input class="form-control " id="dynamicTime" name="dynamicTime" value="" >
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>时间</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                    <input class="form-control " id="time" name="createTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" placeholder="年-月-日  时:分:秒" type="text" value="" >
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>话术</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                    <input class="form-control " id="content" name="content" type="text" value="" maxlength="100">
                                </div>
                            </span>
                    </div>
                    <input type="submit" target="iframe"  id="confirmBtn" class="btn btn-info" value="确认" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/laydate/laydate.js"></script>
<script>
    var recorderFlag = true;
    $(function(){
        //获取比赛的主客队
        getMatchTeam(${matchId});
        //action类型
        <%--var state = '<option value="${action.status}">' + getStatus(${action.status}) + '</option>';--%>
        <%--$("#state").append(state);--%>
        getState();

    })

    //action类型数组
    var actionStatus = [/*11,22,*/31,32,33,34,35,36,37,41,42,43,45/*,55,56,61,62,63,64,65*/];

    function getState(state) {
        var str = '';
        changeMessage(state)
        for (var i = 0; i < actionStatus.length; ++i) {
            if (state != null && state == actionStatus[i]) {
                str += '<option value="' + actionStatus[i] + '" selected>' + getStatus(actionStatus[i]) + '</option>';
            } else {
                str += '<option value="' + actionStatus[i] + '">' + getStatus(actionStatus[i]) + '</option>';
            }
        }
        $("#state").append(str);
    }
/*
    $("#recorderName").change(function(){
        var userid = $(this).val();
        var show = $(this).parent().prev().find("span");
        $.get("<%=base%>/ctl/player/getUserName",{
            userId:userid
        },function(json){
            show.empty();
            if (json.code == 0 && json.flag == 0) {
                if (json.data != undefined && json.data != '' && json.data != null) {
                    show.html(json.data);
                    show.css("color","blue");
                    recorderFlag = true;
                } else {
                    show.html("查无此人");
                    show.css("color","red");
                }
            }
        })
    })*/

    $("#state").change(function () {
        var status = $(this).val();
        $(this).empty();
        getState(status);
        changeMessage(status)
    })


    $("#teamName").change(function(){
        var select = $(this).val();
        //获取该队伍成员
        getMember(select);
    })
    function getMember(select){
        $.get("<%=base%>/ctl/user/queryMemberByTeamId",{
            teamId:select
        },function(json){
            if (json.code == 0 && json.flag == 0) {
                if (json.data.length > 0) {
                    var str = '';
                    $("#teamPeopleA").empty();
                    $("#teamPeopleB").empty();
                    str += '<option value="">--</option>'
                    $.each(json.data, function (index, zPeople) {
                        str+='<option value="' + zPeople.id + '">' + zPeople.name + '</option>';
                    })
                    $("#teamPeopleA").append(str);
                    $("#teamPeopleB").append(str);
                }
            }
        })
    }

    function changeMessage(status){
        $("#playerA > span").eq(0).html('');
        $("#playerB > span").eq(0).html('');

        if (status == 22) {
            $("#playerA > span").eq(0).html('(下场)');
            $("#playerB > span").eq(0).html('(上场)');
        } else if (status == 33){
            $("#playerA > span").eq(0).html('(进球)');
            $("#playerB > span").eq(0).html('(助攻)');
        }
    }

    function getStatus(status){
        switch (status){
            case 11:
                return '比赛开始';
                break;
            case 22:
                return '换人';
                break;
            case 31:
                return '罚球(进)';
                break;
            case 32:
                return '罚球(丢)';
                break;
            case 33:
                return '2分(中)';
                break;
            case 34:
                return '2分(不中)';
                break;
            case 35:
                return '3分(命中)';
                break;
            case 36:
                return '3分(不中)';
                break;
            case 37:
                return '助攻';
                break;
            case 41:
                return '篮板';
                break;
            case 42:
                return '盖帽';
                break;
            case 43:
                return '抢断';
                break;
            case 44:
                return '失误';
                break;
            case 45:
                return '犯规';
                break;
            case 55:
                return '暂停';
                break;
            case 56:
                return '继续';
                break;
            case 61:
                return '下节开始';
                break;
            case 62:
                return '本节结束';
                break;
            case 63:
                return '加时开始';
                break;
            case 64:
                return '加时结束';
                break;
            case 65:
                return '加时准备';
                break;
            case 100:
                return '比赛结束';
                break;
            case 110:
                return '比赛取消';
                break;
            default:
                return '--';
                break;
        }
    }

    function getMatchTeam(matchId) {
        $.get('getMatchTeam',{
            matchId:matchId
        },function(json){
            if (json.code == 0 && json.flag == 0) {
                $("#teamName").empty();
                if (json.data.home != null) {
                    $("#teamName").append('<option value="' + json.data.home.id+ '">' + json.data.home.name+ '</option>');
                    initPlayer(json.data.home.id)
                }
                if (json.data.visiting != null) {
                    $("#teamName").append('<option value="' + json.data.visiting.id+ '">' + json.data.visiting.name+ '</option>');

                    if (json.data.home == null){
                        initPlayer(json.data.visiting.id)
                    }
                }
            }
        })
    }
    function initPlayer(teamId){
        $("#teamPeopleA,#teamPeopleB").each(function(){
            var me = $(this);
                $.get("<%=base%>/ctl/user/queryMemberByTeamId",{
                    teamId:teamId
                },function(json){
                    if (json.code == 0 && json.flag == 0) {
                        if (json.data.length > 0) {
                            var str = '';
                            str+='<option value="">--</option>';
                            $.each(json.data, function (index, zPeople) {
                                str+='<option value="' + zPeople.id + '">' + zPeople.name + '</option>';
                            })
                            me.append(str);
                        }
                    }
                })
        })
    }


    $('#confirmBtn').click(function(){
        var dynamicTime = $("#dynamicTime").val();
        var numPattern = /[0-9]+/;
        var time = $("#time").val();
        if(!numPattern.test(dynamicTime)){
            Notify("比赛时间为数字，单位为秒", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(!recorderFlag){
            Notify("请选择正确的记录员", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(time == ''){
            Notify("请选择数据创建时间", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
</script>
</body>
<!--  /Body -->
</html>
