<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>
    <style>
        tbody span{
            display: inline-block;
            width: 20px;
            height: 20px;
            margin-left: 10px;
            vertical-align: middle;
        }
        span.up{
            background: url(<%=base%>/resource/img/icon/up.png) no-repeat right center;
            background-size: 100%;
        }
        span.down{
            background: url(<%=base%>/resource/img/icon/down.png) no-repeat right center;
            background-size: 100%;
        }

        span.attack {
            background: url(<%=base%>/resource/img/icon/shoe2.png) no-repeat right center;
            background-size: 120%;
        }
        span.goalin {
            background: url(<%=base%>/resource/img/icon/ball2.png) no-repeat right center;
            background-size: 100%;
        }
        span.own-goal {
            width: 35px;
            background: url(<%=base%>/resource/img/icon/own_goal2.png) no-repeat right center;
            background-size: 100%;
        }
        span.red-card {
            width: 35px;
            background: url(<%=base%>/resource/img/icon/red-card.png) no-repeat right center;
            background-size: 100%;
        }
        span.yellow-card {
            width: 35px;
            background: url(<%=base%>/resource/img/icon/yellow-card.png) no-repeat right center;
            background-size: 100%;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看数据</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        数据
        <small>
            Action
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看数据</span>
            <div class="widget-buttons">
            <a href="addDataPage?matchId=${match.id}" target="iframe" class="btn btn-info">添加数据</a>
                <a onclick="fn_refresh(${match.id})" target="iframe" class="btn btn-info">刷新比赛</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <%--<label>--%>
                        <%--<input type="text" name="action" id="action" class="form-control input-sm" placeholder="这个传什么参数?"/>--%>
                    <%--</label>--%>
                    <label>
                        <select id="status">
                            <option value="-1">全部</option>
                            <option value="0">主队</option>
                            <option value="1">客队</option>
                        </select>
                        <select id="action">
                            <option value="-1">全部</option>
                            <option value="31">罚球(进)</option>
                            <option value="32">罚球(丢)</option>
                            <option value="33">2分(中)</option>
                            <option value="34">2分(不中)</option>
                            <option value="35">3分(命中)</option>
                            <option value="36">3分(不中)</option>
                            <option value="37">助攻</option>
                            <option value="41">篮板</option>
                            <option value="42">盖帽</option>
                            <option value="43">抢断</option>
                            <option value="44">失误</option>
                            <option value="45">犯规</option>
                        </select>
                    </label>
                    <label>
                        <a href="javascript:void(0);" class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>时间</th>
                    <th>球队</th>
                    <th>类型</th>
                    <th>比赛时间</th>
                    <th width="35%">球员</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/js/jqPaginator.js"></script>
<script src="../../resource/js/jqPage.js"></script>
<script src="../../resource/laydate/laydate.js"></script>
<script>
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var status = '';
    var action = '';
    $(function(){
        addpage(); //加载分页方法
        getData(1,-1,-1);

    })
    function getData(page,status,action) {
        $.ajax({
            type:"POST",
            async:true,//异步请求
            url:'getAction',
            data:{matchId:${match.id},
                  status:status,
                  action:action,
                  pageNumber:page},
            dataType:"json",
            timeout:5000,
            success:function(json) {
                var listHtml = '';
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0){
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,data){
                            listHtml += '<tr>'+
                                    '<td>' + data.id + '</td>'+
                                    '<td>' + getTime(data.createTime) +
                                    '</td>'+
                                    '<td>' + getvalue(data.teamName) + '</td>'+
                                    '<td>' + getStatusMessage(data.status) + '</td>'+
                                    '<td>' + convertTime(data.dynamicTime) + '</td>'+
                                    '<td>' ;
                                    if (data.playerA != null) {
                                        listHtml += '<img src="' + data.playerA.avatar + '">' + data.playerA.name;
                                        if (data.status == 22) {//换人
                                            listHtml += '<span class="down"></span>';
                                        } else if (data.status == 33) {//进球
                                            listHtml += '<span class="goalin"></span>';
                                        } else if (data.status == 23) {//乌龙球
                                            listHtml += '<span class="own-goal"></span>';
                                        } else if (data.status == 42) {//红牌
                                            listHtml += '<span class="red-card"></span>';
                                        } else if (data.status == 43) {//黄牌
                                            listHtml += '<span class="yellow-card"></span>';
                                        }
                                    }
                                    if (data.playerB != null) {
                                        if (data.playerA != null) {
                                            listHtml += '</br>';
                                        }
                                        listHtml += '<img src="' + data.playerB.avatar + '">' + data.playerB.name;
                                        if (data.status == 22) {//换人
                                            listHtml += '<span class="up"></span>';
                                        } else if (data.status == 33) {
                                            listHtml += '<span class="attack"></span>';
                                        } else if (data.status == 23) {//乌龙球
                                            listHtml += '<span class="own-goal"></span>';
                                        }else if (data.status == 42) {//红牌
                                            listHtml += '<span class="red-card"></span>';
                                        } else if (data.status == 43) {//黄牌
                                            listHtml += '<span class="yellow-card"></span>';
                                        }
                                    }
                                    listHtml += '</td><td>'+
                                    '<a href="editActionPage?actionId=' + data.id + '&matchId=${match.id}" target="iframe" class="btn btn-info">修改</a>'+
                                    '<a  class="btn btn-danger" onclick="beforDel('+data.id+',${match.id})">删除</a>'+
                                    '</td>'+
                                    '</tr>';
                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    getData(num,status);
                                }
                            }
                        });
                    } else {
                        listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
    }
    $("#qShoppingTemplet").click(function(){
        status = $("#status").val();
        action = $("#action").val();
        getData(1,status,action);
    })

    //时间戳转换为日期
    function getTime(ns){
        var val=getvalue(ns);
        if(val!='--'){
            //val=val.time;
            val = parseInt(val,10);
            var myDate = new Date(val);
            return myDate.format("yyyy-MM-dd hh:mm:ss");
        }else{
            return '--';
        }
    }
    //判断值是否为空
    function getvalue(val){
        if(val==undefined){
            val='--';
            return val ;
        }else{
            return val;
        }
    }

    /**获取比赛状态*/
    function getStatusMessage(status){
        switch (status){
            case 11:
                return '比赛开始';
                break;
            case 22:
                return '换人';
                break;
            case 31:
                return '罚球(进)';
                break;
            case 32:
                return '罚球(丢)';
                break;
            case 33:
                return '2分(中)';
                break;
            case 34:
                return '2分(不中)';
                break;
            case 35:
                return '3分(命中)';
                break;
            case 36:
                return '3分(不中)';
                break;
            case 37:
                return '助攻';
                break;
            case 41:
                return '篮板';
                break;
            case 42:
                return '盖帽';
                break;
            case 43:
                return '抢断';
                break;
            case 44:
                return '失误';
                break;
            case 45:
                return '犯规';
                break;
            case 55:
                return '暂停';
                break;
            case 56:
                return '继续';
                break;
            case 61:
                return '下节开始';
                break;
            case 62:
                return '本节结束';
                break;
            case 63:
                return '加时开始';
                break;
            case 64:
                return '加时结束';
                break;
            case 65:
                return '加时准备';
                break;
            case 100:
                return '比赛结束';
                break;
            case 110:
                return '比赛取消';
                break;
            default:
                return '--';
                break;
        }
    }

    ////时间转换
    function convertTime(dynamicTime){
        var minu=parseInt(dynamicTime/60);
        var second=dynamicTime-(minu*60);
        if(minu<10){
            minu="0"+minu;
        }else{
            minu=minu;
        }
        if(second<10){
            second="0"+second;
        }else{
            second=second;
        }

        var time=(minu+"'"+second+'"');
        return time;
    }
    /**
     * 删除之前的判断
     */
    function beforDel(id,matchId) {
        if (confirm("确定删除？")) {
            window.location.href="deleteAction?actionId=" + id+ "&matchId="+matchId;
        }
    }
    function fn_refresh(matchId) {
        var matchIds = [matchId] ;
        $.ajax({
            url : "http://localhost:8080/basketball-api/ctl/game/clear",
            data : {matchIds : matchId},
            type : "get",
            success : function (data) {
                Notify("刷新成功", 'top-right', '5000', 'success', 'fa-desktop', true);
            },
            error : function () {
                Notify("刷新失败，服务器异常", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        });
    }
</script>
</body>
<!--  /Body -->
</html>

