<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">比赛详情</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        比赛
        <small>
            Play
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">比赛详情</span>
        </div>
        <div class="widget-body">
            <div class="box-body">
                <div align="center">
                    <h2>
                        <%--<c:choose>--%>
                            <%--<c:when test="${match.lid == 10000}">--%>
                                <%--单队赛：--%>
                            <%--</c:when>--%>
                            <%--<c:when test="${match.lid == 10100}">--%>
                                <%--匹配赛：--%>
                            <%--</c:when>--%>
                            <%--<c:otherwise>--%>
                                <%--赛事：--%>
                            <%--</c:otherwise>--%>
                        <%--</c:choose>--%>
                        ${match.leagueName}</h2>
                    <div class="form-group">
                        <label>比赛时间:</label>
                        <label><fmt:formatDate value="${match.date}" pattern="yyyy-MM-dd HH:mm:ss" /> </label>
                    </div>
                    <div class="form-group">
                        <label>比分:</label>
                        <label>${match.homescore}:${match.visitingscore}</label>
                    </div>
                    <div class="form-group">
                        <label>状态:</label>
                        <label>
                            <c:choose>
                                <c:when test="${match.status == 0}">
                                    约赛中
                                </c:when>
                                <c:when test="${match.status == 10}">
                                    已匹配
                                </c:when>
                                <c:when test="${match.status == 20}">
                                    比赛中
                                </c:when>
                                <c:when test="${match.status == 100}">
                                    比赛结束
                                </c:when>
                                <c:otherwise>
                                    比赛取消
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>主队:</label>
                        <label>${match.home.name}</label>
                    </div>
                    <div class="form-group">
                        <label>主队记分员:</label>
                        <label>${match.homerecord}</label>
                    </div>
                    <div class="form-group">
                        <label>主队阵容:</label>
                        <label>
                            <c:forEach items="${match.homeusers}" var="homeusers">
                                ${homeusers.name}&nbsp;&nbsp;
                            </c:forEach>
                        </label>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>客队:</label>
                        <label>${match.visiting.name}</label>
                    </div>
                    <div class="form-group">
                        <label>客队记分员:</label>
                        <label>${match.visitingrecord}</label>
                    </div>
                    <div class="form-group">
                        <label>客队阵容:</label>
                        <label>
                            <c:forEach items="${match.visitingusers}" var="visitingusers">
                                ${visitingusers.name}&nbsp;&nbsp;
                            </c:forEach>
                        </label>
                    </div>
                </div>
                <%--<div align="center">--%>
                    <%--<a href="" target="iframe" class="btn btn-primary">修改</a>--%>
                <%--</div>--%>
            </div>
        <%--</div>--%>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>

</body>
<!--  /Body -->
</html>
