<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>
</head>
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看比赛</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        比赛
        <small>
            Play
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看比赛</span>
            <div class="widget-buttons">
                <a href="addPage" target="iframe" class="btn btn-info">添加比赛</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>
                        <input type="text" name="leagueName" id="leagueName" class="form-control" placeholder="赛事名称" />
                    </label>
                    <label>
                        <input type="text" name="teamName" id="teamName" class="form-control" placeholder="球队名字" />
                    </label>
                    <label>
                        <input type="text" name="start" id="start"  class="form-control date-picker" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})" placeholder="比赛开始日期" />
                    </label>
                    <label>
                        <input type="text" name="end" id="end" class="form-control date-picker" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})" placeholder="比赛结束日期"/>
                    </label>
                    <label>
                        <a href="javascript:void(0);" class="btn btn-info" id="qShoppingTemplet">查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>赛事</th>
                    <th>轮次</th>
                    <th>场序</th>
                    <th>主队</th>
                    <th>客队</th>
                    <th>状态</th>
                    <th>时间</th>
                    <th>比分</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/js/jqPaginator.js"></script>
<script src="../../resource/js/jqPage.js"></script>
<script src="../../resource/assets/js/bootbox/bootbox.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script>
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var teamName = '';
    var leagueName = '';
    var start = '';
    var end = '';
    $(function(){
        addpage(); //加载分页方法
        getData(1);
    })
	
    var deleteById = function() {
    	if(confirm("确定删除吗?") == false) {
     	   return false;
        }
    	return true;
   };
   
    function getData(page,teamName,leagueName,start,end) {
    	var listHtml = '';
    	$.ajax({
            type: "POST",
            async: true,//异步请求
            url: '<%=base%>/ctl/match/queryMatches',
            data:{pageNumber:page, teamName:teamName, leagueName:leagueName, start:start, end:end, pageSize: 20},
            dataType: "json",
            timeout: 5000,
            success: function (json) {
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0) {
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){
                            listHtml += '<tr>'+
                                        '<td>' + obj.id + '</td>'+
                                        '<td><span data-toggle="popover-hover" data-title="主队" data-content="">' + obj.leagueName + '</span></td>'+
                                        '<td>' + moveNull(obj.roundName) + '</td>'+
                                        '<td>' + moveNull(obj.sequence) + '</td>'+
                                        '<td>' + moveNull(obj.homeName) + '</td>'+
                                        '<td>' + moveNull(obj.visitingName) + '</td>'+
                                        '<td>' + convertStatus(obj.status) + '</td>'+
                                        '<td>' + getTime(obj.matchTime) + '</td>'+
                                        '<td>' + obj.homeScore + '-' + obj.visitingScore + '</td>'+
                                        '<td>'+
                                        '<a disabled="disabled" href="detailPage?matchId=' + obj.id + '" target="iframe" class="btn btn-info">详情</a>'+
                                        '<a href="editPage?matchId=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'+
                                        '<a disabled="disabled" href="dataPage?matchId=' + obj.id + '" target="iframe" class="btn btn-info">查看</a>'+
                                        '<a href="delete?matchId='+obj.id+'" class="btn btn-danger" onclick="return deleteById()">删除</a>'+
                                        '<a disabled="disabled" class="btn btn-danger" onclick="fn_refresh('+ obj.id+')">刷新</a>'+
                                        '</td>'+
                                        '</tr>';
                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    getData(num,teamName,leagueName,start,end);
                                }
                            }
                        });
                    } else {
                        listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml='<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
    }
    function convertStatus(status){
        if (status == 0) {
            return '未开赛';
        } else if (status == 100) {
            return '比赛结束';
        } else if(status > 0 && status < 399) {
        	return '比赛中';
        } else {
            return '比赛取消';
        }
    }
    function moveNull(data) {
        if (data == null) {
            return "";
        } else {
            return data;
        }
    }
    $("#qShoppingTemplet").click(function(){
        teamName = $("#teamName").val();
        leagueName = $("#leagueName").val();
        start = $("#start").val();
        end = $("#end").val();
        getData(1,teamName,leagueName,start,end);
    })

</script>
</body>

</html>

