<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加球员</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        球员
        <small>
            Player
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加球员</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post">
                    <%--<div class="form-group">--%>
                        <%--<label>头像</label>--%>
                        <%--<div>--%>
                            <%--<input id="headUpload" name="res" type="file" class="file-loading" >--%>
                        <%--</div>--%>
                            <%--<span class="input-icon icon-right">--%>
                            	<%--<div class="input-group" style="margin-top: 5px;">--%>
                            		 <%--<span class="input-group-addon">--%>
                                        <%--<i class="fa fa-laptop"></i>--%>
                                    <%--</span>--%>
                                    <%--<input type="hidden" id="filePath" type="text" name="avatar" />--%>
                                <%--</div>--%>
                            <%--</span>--%>
                    <%--</div>--%>
                    <div class="form-group">
                        <label>手机号码</label>
                            <span class="input-icon icon-right">
                            	<div class="spinner">
                                    <div class="input-group">
	                            		 <span class="input-group-addon">
	                                        <i class="fa fa-laptop"></i>
	                                    </span>
                                        <input class="form-control" name="mobile" id="phone" type="text" maxlength="20"  />
                                    </div>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>姓名</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" id="name" name="name" type="text" maxlength="20">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>身份证号码</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" id="idcard" name="idcard" type="text" maxlength="20">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>密码</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" id="pwd" name="pwd" type="text" maxlength="20">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>身高(cm)</label>
                            <span class="input-icon icon-right">
                            	<div class="spinner">
                                    <div class="input-group">
	                            		 <span class="input-group-addon">
	                                        <i class="fa fa-laptop"></i>
	                                    </span>
                                        <input class="form-control spinner-input" name="height" id="height" type="text" maxlength="5">
                                    </div>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>体重（Kg）</label>
                            <span class="input-icon icon-right">
                            	<div class="spinner">
                                    <div class="input-group">
	                            		 <span class="input-group-addon">
	                                        <i class="fa fa-laptop"></i>
	                                    </span>
                                        <input class="form-control spinner-input" name="weight" id="weight" type="text" maxlength="5">
                                    </div>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>年龄</label>
                            <span class="input-icon icon-right">
                            	<div class="spinner">
                                    <div class="input-group">
	                            		 <span class="input-group-addon">
	                                        <i class="fa fa-laptop"></i>
	                                    </span>
                                        <input class="form-control spinner-input" name="age" id="age" type="text" maxlength="3">
                                    </div>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                            <span class="input-icon icon-right">
                            	<div class="checkbox">
                                    <label>
                                        <%--<input type="checkbox" checked="checked" value="1" name="isSendMessage">是否发送短信通知--%>
                                        <input type="checkbox"  value="0" name="isSendMessage">是否发送短信通知
                                    </label>
                                </div>
                            </span>
                    </div>
                    <input type="submit" target="iframe"  id="confirmBtn" class="btn btn-info" value="确认" />
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/common.js"></script>
<script src="../../resource/laydate/laydate.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    <%--$("#headUpload").fileinput({--%>
        <%--language:'zh',--%>
        <%--uploadUrl: '<%=base%>/ctl/resource/upload',// 图片上传接口--%>
        <%--showPreview : true,--%>
        <%--showRemove: false,--%>
        <%--maxFileSize : 2048,  //上传图片的最大限制  50KB--%>
        <%--allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],--%>
        <%--initialCaption: "请选择球员头像"--%>
    <%--});--%>
    <%--$("#headUpload").on("fileuploaded", function (event, data, previewId, index) {--%>
        <%--if (null != data) {--%>
            <%--fillPath = data.response.data;--%>
            <%--$("#filePath").val(fillPath);--%>
        <%--} else {--%>
            <%--Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);--%>
        <%--}--%>
    <%--});--%>

    $("#phone").blur(function(){
        $.get("../user/mobileExists",{
            mobile:$("#phone").val()
        },function(json){
            if (json) {
                Notify("该手机号码已注册，请重新输入!", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        })
    })

    $('.spinner').spinner();

    $('#confirmBtn').click(function(){
        var phonePattern = /0?(13|14|15|18)[0-9]{9}/;
        var numPattern = /[0-9]+/;
        var phone = $("#phone").val();
        var idcard = $("#idcard").val();
        var name = $("#name").val();
        var pwd = $("#pwd").val();
        var height = $("#height").val();
        var weight = $("#weight").val();
        var age = $("#age").val();
        var avatar = $("#filePath").val();
//        if(avatar ==""){
//            Notify("请上传球员头像", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }
        if(phone ==""){
            Notify("请填写手机号码", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        } else if(!phonePattern.test(phone)){
            Notify("手机格式不正确", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(name ==""){
            Notify("请填写球员名字", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
//        if(idcard ==""){
//            Notify("请填写身份证号码", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }else if(!numPattern.test(idcard)){
//            Notify("身份证号码输入错误，请输入数字", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }
        if(pwd ==""){
            Notify("请填写密码", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
//        if(height ==""){
//            Notify("请填写球员身高", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }else if(!numPattern.test(height)){
//            Notify("身高输入错误，请输入数字", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }
//        if(weight ==""){
//            Notify("请填写球员体重", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }else if(!numPattern.test(weight)){
//            Notify("体重输入错误，请输入数字", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }
//        if(age ==""){
//            Notify("请填写球员年龄", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }else if(!numPattern.test(age)){
//            Notify("年龄输入错误，请输入数字", 'top-right', '5000', 'danger', 'fa-desktop', true);
//            return false;
//        }
    })
</script>
</body>
<!--  /Body -->
</html>
