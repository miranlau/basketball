<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">球员详情</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        球员
        <small>
            Player
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">球员详情</span>
        </div>
        <div class="widget-body">
            <div class="box-body">
                <div align="center">
                    <h2>球员 ${player.name}</h2>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label>密码</label>--%>
                    <%--<label>${player.}</label>--%>
                    <%--<img src="">--%>
                <%--</div>--%>

                <div class="form-group">
                    <label>球员头像</label>
                    <img src="${player.avatar}">
                </div>

                <div class="form-group">
                    <label>所属球队</label>
                    <label>
                        <c:forEach items="${player.teams}" var="team">
                            ${team.name}
                        </c:forEach>
                    </label>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label>队长</label>--%>
                    <%--<label></label>--%>
                <%--</div>--%>

                <div class="form-group">
                    <label>手机</label>
                    <label>${player.mobile}</label>
                </div>


                <div class="form-group">
                    <label>身高</label>
                    <label>${player.height}</label>
                </div>

                <div class="form-group">
                    <label>体重</label>
                    <label>${player.weight}</label>
                </div>


                <div class="form-group">
                    <label>年龄</label>
                    <label>${player.age}</label>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label>记分员证书</label>--%>
                    <%--<img src="http://.9026.com/api/avatar/avatar.png">--%>
                <%--</div>--%>

                <div align="center">
                    <a href="editPage?playerId=${player.id}" target="iframe" class="btn btn-info">修改</a>
                    <a href="joinPage?playerId=${player.id}" target="iframe" class="btn btn-darkorange">加入球队</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
</body>
<!--  /Body -->
</html>
