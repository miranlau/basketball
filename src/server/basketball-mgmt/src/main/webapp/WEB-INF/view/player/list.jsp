<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
    <style>
        .uploadBox{
            width: 100%;
            -moz-background-clip: padding!important;
            border-radius: 0!important;
            background-clip: padding-box!important;
            color: #858585;
            background-color: #fbfbfb;
            border: 1px solid #d5d5d5;
            position: relative;
            height: 34px;
        }
        .uploadBox p{
            margin: 0;
            line-height: 34px;
            padding: 0 10px;
            font-size: 13px;
        }
        .uploadBox a{
            width: 100px;
            display: block;
            text-align: center;
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            line-height: 34px;
            color: #fff;
            font-size: 13px;
            background: #3c8dbc;
        }
        .uploadBox a i{
            margin-right: 10px;
        }
        .uploadBox input{
            width: 100px;
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            z-index: 5;
            opacity: 0;
            -moz-opacity: 0;
            -webkit-opacity: 0;

        }
    </style>
</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看球员</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        球员
        <small>
            Player
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看球员</span>
            <div class="widget-buttons">
              <%--  <a href="javascript:void(0)" id="fastAddBtn" class="btn btn-danger">快速</a>--%>
                <a href="add" target="iframe" class="btn btn-warning">详细</a>
                <a href="javascript:void(0)" id="importBtn"  class="btn btn-info">导入</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>
                        <select class="form-control input-sm" name="type"  id="type" >
                            <option value="0">球员姓名</option>
                            <!-- <option value="1">球队名称</option>  -->
                            <!-- <option value="2">球员手机号</option> -->
                        </select>
                    </label>
                    <label>
                        <input type="text" id="keyword" class="form-control input-sm" />
                    </label>
                    <label>
                        <a href="javascript:void(0);" class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>

            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>姓名</th>
                    <th>头像</th>
                    <th>号码</th>
                    <th>所属球队</th>
                    <th>国籍</th>
                    <th>位置</th>
                    <th>身高</th>
                    <th>体重</th>
                    <th>简介</th>
                    <th>运动生涯</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <iframe  name='hidden_frame' id="hidden_frame" style='display:none'></iframe>
        </div>
    </div>
</div>
<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<!--Common Scripts -->
<script src="../../resource/js/common.js"></script>
<script src="../../resource/js/jqPaginator.js"></script>
<script src="../../resource/js/jqPage.js"></script>
<script src="../../resource/assets/js/bootbox/bootbox.js"></script>

<script type="text/javascript">
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var keyword = ''; //输入内容
    var type = '';  //0球员  1球队
    $(function(){
        addpage(); //加载分页方法
        loadPlayerList(1, 0);
    })
	
    var moveNull = function(data) {
        if (data == null) {
            return "";
        } else {
            return data;
        }
    };
    var loadPlayerList = function(page, type, keyword) {
        var listHtml = '';
        $.ajax({
            type: "POST",
            async: true,//异步请求
            url: '<%=base%>/ctl/player/queryPlayerPage',
            data: {
            	pageNumber: page, 
            	keyword: keyword, 
            	type: type
            },
            dataType: "json",
            timeout: 5000,
            success: function (json) {
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0) {
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){
                            listHtml += '<tr>'
                              + '<td>' + obj.id + '</td>'
                              + '<td>' + obj.name + '</td>'
                              + '<td><img src="' + obj.avatar + '" /></td>'
                              + '<td>' + obj.uniformNumber + '</td>'
                              + '<td>' + obj.teamName + '</td>'
                              + '<td>' + obj.nationality + '</td>'
                              + '<td>' + obj.position + '</td>'
                              + '<td>' + obj.height + '</td>'
                              + '<td>' + obj.weight + '</td>'
                              + '<td>' + moveNull(obj.profile)	 + '</td>'
                              + '<td>' + moveNull(obj.career) + '</td>'
                              + '<td>'
                              + '<a href="editPage?playerId='+ obj.id + '" target="iframe" class="btn btn-info">修改</a>'
                              + '<a href="detailPage?playerId=' + obj.id + '" target="iframe" class="btn btn-info">详情</a>'
                              + '</td>'
                              + '</tr>';
                        });
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    keyword = $("#name").val();
                                    type = $("#type :selected").val();
                                    loadPlayerList(num, type, keyword);
                                }
                            }
                        });
                    } else {
                        listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
    }
    $("#qShoppingTemplet").click(function(){
    	keyword = $("#keyword").val();
        type = $("#type :selected").val();
        loadPlayerList(1, type, keyword);
    })

    $("#importBtn").on('click', function () {
        var str='<div id="myModal">'+
                '<div class="form-group">'+
                '<div class="uploadBox">'+
                '<p></p>'+'<form id="upload" method="post" enctype="multipart/form-data" target="hidden_frame" action="../player/upload">'+
                '<input id="wordUpload" type="file" name="excel" accept=".xls,.xlsx" ><a href="javascript:void(0)"><i class="glyphicon glyphicon-folder-open"></i>选择 …</a>'+
                '</form>'
                '</div>'+
                '<span class="input-icon icon-right">'+
                '<div class="input-group" style="margin-top: 5px;">'+
                '<span class="input-group-addon">'+
                '<i class="fa fa-laptop"></i>'+
                '</span>'+
                '<div class="form-control" id="wordFilePath" type="text"></div>'+
                '</div>'+
                ' </span>'+
                '</div> '+
                '</div>';
        bootbox.dialog({
            message: str,
            title: "批量导入球员",
            buttons: {
                "导入模板下载": {
                    className: "btn-success",
                    callback: function () {
                        window.open("<%=base%>/download/PlayerList.xls", "_blank");
                    }
                },
                success: {
                    label: "导入",
                    className: "btn-primary",
                    callback: function () {
                        var $input=document.getElementById('wordUpload');
                        if($input.value==null || $input.value==''){
                            Notify("请选择上传文件", 'top-right', '5000', 'danger', 'fa-desktop', true);
                            return;
                        }
                        else  if( $input.value.indexOf(".xlsx")<0 && $input.value.indexOf(".xls")<0){
                            Notify('只能上传.xls或者.xls格式的Excel文件', 'top-right', '5000', 'danger', 'fa-desktop', true);
                        }
                        else{
                            Notify("正在导入, 请稍后", 'top-right', '5000', 'success', 'fa-desktop', true);

                            $('#upload').submit();
                        }

                    }
                },
                "关闭": {
                    className: "btn-default",
                    callback: function () {

                    }
                }
            }
        });
        $('#wordUpload').change(function(){
            var file = this.files[0];
            $('.uploadBox p').html(file.name);
            if (window.FileReader) {
                var fr = new FileReader();
                fr.onloadend = function(e) {
                };
                fr.readAsDataURL(file);
            }
        })
    });


</script>
</body>
<!--  /Body -->
</html>
