<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">编辑赛季</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛季
        <small>
            Season
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">编辑赛季</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="update" method="post">
                    <div class="form-group">
                    	<input type="hidden" id="id" type="text" name="id" value="${season.id}" />
                        <input type="hidden" id="leagueId" type="text" name="leagueId" value="${season.leagueId}" />
                        <input type="hidden" id="teams" type="text" name="teams" value="${season.teams}" />
                    </div>
                    <div class="form-group">
                        <label>名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" id="name" type="text" name="name" value="${season.name}" maxlength="18">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>开始时间</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control input-sm" name="startTime" value="<fmt:formatDate value="${season.startTime}" pattern="YYYY-MM-dd hh:mm:ss" />" type="text" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>结束时间</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                   <input class="form-control input-sm" name="endTime" value="<fmt:formatDate value="${season.endTime}" pattern="YYYY-MM-dd hh:mm:ss" />" type="text" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>参赛球队</label>
                            <span class="input-icon icon-right">
                            	<select id="teamSelection" multiple="multiple" class="form-control" name="teams">
                                    <c:forEach items="${season.teamList}" var="team">
                                        <option value="${team.id}" selected="selected">${team.name}</option>
                                    </c:forEach>
                                </select>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>赛季简介</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="profile" id="profile" maxlength="200"></textarea>
                            </div>
                        </span>
                    </div>
                    <input type="submit" target="iframe"  id="confirmBtn" class="btn btn-info" value="确认">
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Page Related Scripts-->

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/laydate/laydate.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>

$(function(){
	$("#profile").val('${season.profile}');
	
	$("#teamSelection").select2({
        placeholder: "请选择球队",
        allowClear: true
    });
    
    <!--获取队伍列表-->
    $.get('<%=base%>/ctl/team/getTeams',function(json){
        $("#teamSelection").empty();
        if (json != "" || null != json) {
        	$("#teamSelection").append('<option disabled selected>请选择球队</option>');
            $.each(json,function(index,team){
                $("#teamSelection").append('<option value="' + team.id + '" >' + team.name + '</option>');
            })
        }
    })

})

    $('#confirmBtn').click(function(){
        var name = $("#name").val();
        var startTime = $("#startTime").val();
        var endTime = $("#endTime").val(); 
        if(name ==""){
            Notify("请填写赛季名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(startTime ==""){
            Notify("请填写开始时间", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(endTime ==""){
            Notify("请填写结束时间", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(startTime >= endTime){
            Notify("赛季开始时间不能大于结束时间", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
</script>
</body>
<!--  /Body -->
</html>
