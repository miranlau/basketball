<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看赛季</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        赛季
        <small>
            Season
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <input type="hidden" id="leagueId" type="text" name="id" value="${leagueId}"/>
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看赛季</span>
            <div class="widget-buttons">
                <a href="javascript:void(0);" class="btn btn-info" id="createSeason"> 添加赛季</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>
                        <select class="form-control input-sm" id="type" >
                            <option value="0">赛季名称</option>
                        </select>
                    </label>
                    <label>
                        <input type="text" name="name" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <a href="javascript:void(0);" class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>名称</th>
                    <th>赛事</th>
                    <th>开始时间</th>
                    <th>结束时间</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/js/jqPaginator.js"></script>
<script src="../../resource/js/jqPage.js"></script>
<script src="../../resource/assets/js/bootbox/bootbox.js"></script>
<script src="<%=base%>/resource/laydate/laydate.js"></script>

<script>
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var inputVal = ''; //输入内容
    $(function(){
        addpage(); //加载分页方法
        getData(1, ${leagueId});
    })

    function getData(page,leagueId, inputVal) {
        var listHtml = '';
        $.ajax({
            type: "POST",
            async: true,//异步请求
            url: '<%=base%>/ctl/season/listSeasons',
            data: {pageNumber: page, leagueId: leagueId, name: inputVal},
            dataType: "json",
            timeout: 5000,
            success: function (json) {
                console.log(json);
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0) {
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){

                            listHtml += '<tr>'
                                        +' <td>' + obj.id + '</td>'
                                    +'<td>' + obj.name + '</td>'
                                    +'<td>' + obj.leagueName + '</td>'
                                    +' <td>' + getTime(obj.startTime) + '</td>'
                                    +'  <td>' + getTime(obj.endTime) + '</td>'
                                    + '<td>' + convertStatus(obj.status) + "</td>"
                                    +'  <td>'
                                    +'  <a href="<%=base%>/ctl/season/edit?seasonId=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'
                                    +'  <a class="btn btn-danger" onclick="beforDel('+obj.id+')">删除</a>'
                                    +'  </td>'
                                    +'  </tr>';

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    inputVal = $("#name").val();
                                    getData(num,inputVal);
                                }
                            }
                        });
                    } else {
                        listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
    }
    $("#qShoppingTemplet").click(function(){
        inputVal = $("#name").val();
        leagueId = $("#leagueId").val();
        getData(1,leagueId,inputVal);
    })
    
    $("#createSeason").click(function(){
        leagueId = $("#leagueId").val();
        window.location.href="<%=base%>/ctl/season/add?leagueId="+leagueId;
    })
    
    function beforDel(seasonId) {
        if (confirm("确定删除 ?")) {
        	leagueId = $("#leagueId").val();
            window.location.href="<%=base%>/ctl/season/delete?seasonId="+seasonId+"&leagueId="+leagueId;
        }
    }
    
    function convertStatus(status){
        if (status == 0) {
            return '报名中';
        } else if (status == 1) {
            return '报名结束';
        } else if (status == 2) {
            return '比赛中';
        } else if (status == 3) {
            return '赛事结束';
        } else if (status == 4) {
            return '审核未通过';
        } else if (status == 6) {
            return '待审核';
        } else {
            ;
        }
    }

</script>
</body>
<!--  /Body -->
</html>
