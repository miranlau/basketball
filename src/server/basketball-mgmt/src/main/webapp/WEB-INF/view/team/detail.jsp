<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">球队详情</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        球队
        <small>
            Team
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">球队详情</span>
        </div>
        <div class="widget-body">
            <div class="box-body">
                <div align="center">
                    <h2>${team.name}</h2>
                </div>

                <div class="form-group">
                    <label>球队Logo</label>
                    <div class="input-group">
                    	<img src="${team.logo}">
                    </div>
                </div>
                <div class="form-group">
                    <label>球队主场</label>
                    <div class="input-group">
                    	<label>${team.venueName}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label>球员:</label>
                    <div class="input-group">
                        <label id="players" />
                    </div>
                </div>
                <div class="form-group">
                    <label>球队详情</label>
                    <div class="input-group">
                    	<label>${team.profile}</label>
                    </div>
                </div>
                <div align="center">
                    <a href="editPage?teamId=${team.id}" target="iframe"  id="confirmBtn" class="btn btn-info">修改</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js" />

<script>
$.get("../player/getPlayers",function(json){
    if (json.code == 0 && json.flag == 0) {
        $.each(json.data,function(index,val){
            $("#players").append(val.realName + '(' + val.uniformNumber + ')' + '\n');
        })
    }
})

</script>

</body>
<!--  /Body -->
</html>
