<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }

    </style>
</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">编辑球队</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        球队
        <small>
            Team
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">编辑球队</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="edit">
                    <div class="form-group">
                        <label>球队Logo</label>
                        <div>
                            <input id="iconUpload" name="res" type="file" multiple class="file-loading" >
                        </div>
                            <span class="input-icon icon-right">
                            	<div class="input-group" style="margin-top: 5px;">
                                    <input type="hidden" id="filePath" type="text" name="logo" />
                                    <input type="hidden" id="id" type="text" name="id" value="${team.id}"/>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>球队名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" id="name" name="name" type="text" value="${team.name}">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>球队主场</label>
                            <span class="input-icon icon-right">
                            	<select id="venueId" name="venueId" class="form-control" >
                            	</select>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>球队成员</label>
                            <span class="input-icon icon-right">
                                <div class="memberList">
                                    <c:forEach items="${team.players}" var="player">
                                            <div>
                                                <label>${player.uniformNumber}</label>
                                                <label>${player.realName}</label>
                                            </div>
                                    </c:forEach>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>球队详情</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <textarea class="form-control" id="profile" name="profile" maxlength="200"></textarea>
                                </div>
                            </span>
                    </div>
                    <input type="submit" target="iframe"  id="confirmBtn" class="btn btn-info" value="修改"/>
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->
<!--Jquery Select2-->
<script src="../../resource/assets/js/select2/select2.js"></script>
<!--Bootstrap Tags Input-->
<script src="../../resource/assets/js/tagsinput/bootstrap-tagsinput.js"></script>

<!--Bootstrap Date Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-datepicker.js"></script>

<!--Bootstrap Time Picker-->
<script src="../../resource/assets/js/datetime/bootstrap-timepicker.js"></script>

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/laydate/laydate.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>

    $(function(){
    	$("#profile").val('${team.profile}');
        getBannerInfo();//获取数据方法
    })

    var fillPath='',initialPreview=[];
    var url=window.document.location.href;
    var pName=window.document.location.pathname;
    var localhostPaht=url.substring(0,url.indexOf(pName))+'/basketball-api/';
    var urlPattern = /^((http)?:\/\/)[^\s]+/;
    function getBannerInfo(){
        for(var i=0;i<1;i++){  //把图片循环读取出来
            if(urlPattern.test('${team.logo}')){
                fillPath='${team.logo}';
            } else{
                fillPath=localhostPaht + '${team.logo}';
            }
            initialPreview[i] = '<img width="auto" height="160" class="file-preview-image"  src="'+fillPath+'">'+
                    '<div class="file-thumbnail-footer">'+
                    '<div class="file-caption-name" title="teacher2.jpg" style="width: 250px;"></div>'+
                    '<div class="file-actions">'+
                    '<div class="file-footer-buttons">'+
                    '<button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file">'+
                    '<i class="glyphicon glyphicon-trash text-danger"></i>'+
                    '</button>'+
                    '</div>'+
                    '<div class="file-upload-indicator" tabindex="-1" title="Not uploaded yet">'+
                    '<i class="glyphicon glyphicon-hand-down text-warning"></i>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '</div>'+
                    '</div>';
            $("#filePath").val(fillPath);
        }
        initFileUpload(initialPreview);  //初始化图片上传插件方法
        $('.kv-file-remove').each(function(index){
            $(this).click(function(){
                $(this).parent().parent().parent().parent().fadeOut('slow',function(){
                    var imgSrc = $(".file-preview-image").eq($(this).index()).attr("src");
                    $(this).remove();
                    $('.file-caption-name').attr('name',$(".kv-file-remove").length+' file selected'),
                            $('.file-caption-name').html('<span class="glyphicon glyphicon-file kv-caption-icon"></span>'+$(".kv-file-remove").length+' file selected');
                    $('.goodsImage').each(function() {
                        var fid = $(this).val();
                        if (imgSrc.indexOf(fid) > 0) {
                            $(this).remove();
                        }
                    });
                });
            })
        })
    }

    function initFileUpload(initialPreview) {
        $("#iconUpload").fileinput({
            language:'zh',
            initialPreview: initialPreview,
            uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
            allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
            maxFileSize : 1024,//上传图片的最大限制  50KB
            overwriteInitial:true,
            initialPreviewShowDelete:true,
            minImageWidth: 50,
            minImageHeight: 50
        }).on("fileuploaded", function (event, data, previewId, index) {
            if (null != data) {
                fillPath = data.response.data;
                $("#filePath").val(fillPath);
            } else {
                Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
            }
        });
    }
    
    $.get("../venue/getAllVenues",function(json){
        if (json != "" || null != json) {
            $("#venueId").append('<option disabled selected>请选择球场</option>');
            $.each(json,function(index,venue){
            	if (venue.name == '${team.venueName}') {
                	$("#venueId").append('<option value="' + venue.id + '" selected="selected" >' + venue.name + '</option>');
            	} else {
            		$("#venueId").append('<option value="' + venue.id + '" >' + venue.name + '</option>');
            	}
            })
        }
    })

</script>
</body>
<!--  /Body -->
</html>
