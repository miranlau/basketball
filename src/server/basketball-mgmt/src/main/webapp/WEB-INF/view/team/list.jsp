<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>


</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看球队</li>
    </ul>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="header-title">
    <h1>
        球队
        <small>
            Team
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看球队</span>
            <div class="widget-buttons">
                <a href="addTeam" target="iframe" class="btn btn-info">添加球队</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>
                        <select class="form-control input-sm" id="type" >
                            <option value="0">队名</option>
                        </select>
                    </label>
                    <label>
                        <input type="text" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <select class="form-control input-sm" id="status" >
                            <option value="0">全部</option>
                            <option value="1">解散</option>
                            <option value="2">未解散</option>
                        </select>
                    </label>
                    <label>
                        <a href="javascript:void(0);" class="btn btn-info"  id="qShoppingTemplet">查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>队名</th>
                    <th>Logo</th>
                    <th>主场</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/js/jqPaginator.js"></script>
<script src="../../resource/js/jqPage.js"></script>
<script src="../../resource/assets/js/bootbox/bootbox.js"></script>
<script>
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var inputVal = ''; //输入内容
    var type = '';  //1队长   0队名
    var status = '';//0全部 1解散  2未解散
    $(function(){
        addpage(); //加载分页方法
        getData(1,0);
    })

    function getData(page,type,name,status) {
        var listHtml = '';
        $.ajax({
            type: "POST",
            async: true,//异步请求
            url: '<%=base%>/ctl/team/teamList',
            data: {pageNumber: page, name: name, type: type, status: status, pageSize: 20 },
            dataType: "json",
            timeout: 5000,
            success: function (json) {
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0) {
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){

                            listHtml += '<tr>'
                                     + '<td>' + obj.id + '</td>'
                                     + '<td>' + obj.name +'</td>'
                                     + '<td><img src="' + obj.logo + '" /></td>'
                                     + '<td>' + obj.venueName + '</td>'
                                     + '<td>' + convertStatus(obj.isDismissed) + '</td>'
                                     + '<td>'
                                     + '<a href="javascript:void(0)" class="btn btn-info ballBtn">球号</a>'
                                     + '<a href="detailPage?teamId=' + obj.id + '" target="iframe" class="btn btn-info">详情</a>'
                                     + '<a href="editPage?teamId=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'
                                     + '<a  class="btn btn-danger" onclick="beforDel('+obj.id+')">删除</a>'
                                     + '</td>'
                                     + '</tr>'

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    inputVal = $("#name").val();
                                    status = $("#status :selected").val();
                                    type = $("#type :selected").val();
                                    getData(num,type,inputVal,status);
                                }
                            }
                        });
                    } else {
                        listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
    }
    $("#qShoppingTemplet").click(function(){
        name = $("#name").val();
        status = $("#status :selected").val();
        type = $("#type :selected").val();
        if (status == 0) {
            getData(1,type,name);
        } else {
            getData(1,type,name,status);
        }

    })

    function convertStatus(status){
        if (status == true) {
            return '已解散';
        } else {
            return '未解散';
        }
    }

    $("body").delegate(".ballBtn","click",function(){
        var str= '';
        var teamID = $(this).parent().parent().find("td").eq(0).html();//球队ID
        /**找出所有球员*/
        $.get("../user/queryMemberByTeamId",{
            teamId:teamID
        },function(json){
            if (json.code == 0 && json.flag == 0) {
                if (json.data.length > 0) {
                    str='<div id="myModal">'+
                            '<table class="table table-striped table-hover" id="simpledatatable">'+
                            '<thead>'+
                            '<tr>'+
                            '<th width="25%">姓名</th>'+
                            '<th width="25%">球衣号码</th>'+
                            '<th>修改</th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody>';
                    $.each(json.data,function(index,player){
                        str += '<tr>';
                            if (player.realName != null) {
                                str +='<td>' + player.realName + '</td>';
                            } else {
                                str +='<td>' + player.name + '</td>';
                            }
                         str += '<td>' + player.number + '</td>'+
                                '<td><input type="hidden" name="playerId" value="' + player.id + '"></td>'+
                                '<td><input class="form-control" name="ballNum" type="text" value="' + player.number + '"></td>'+
                                '</tr>';
                    })
                    str +=  '</tbody>'+
                            '</table>'+
                            '</div>';
                    bootbox.dialog({
                        message: str,
                        title: "修改球号",
                        buttons: {
                            "关闭": {
                                className: "btn-default",
                                callback: function () {

                                }
                            },
                            success: {
                                label: "确定修改",
                                className: "btn-primary",
                                callback: function () {
                                    var numFlag = true;
                                    var existFlag = true;
                                    var $success = true;
                                    $('input[name="ballNum"]').each(function(indexOut){
                                        var ballNum = $(this).val();
                                        if (ballNum < 0 || ballNum > 100) {
                                            numFlag = false;
                                        }else{
                                            $('input[name="ballNum"]').each(function(indexIn){
                                                if(indexIn != indexOut) {
                                                    if(ballNum == $(this).val()){
                                                        existFlag = false;
                                                    }
                                                }
                                            })
                                        }
                                    })
                                    if (!numFlag) {
                                        Notify("请输入数字1~100作为球员号码", 'top-right', '5000', 'danger', 'fa-desktop', true);
                                    }
                                    if (!existFlag) {
                                        Notify("球员号码不能相同", 'top-right', '5000', 'danger', 'fa-desktop', true);
                                    }
                                    if(numFlag && existFlag) {
                                        $('input[name="ballNum"]').each(function(){
                                            var playerId = $(this).parent().prev().children().val();
                                            var ballNum = $(this).val();

                                            //调用接口修改球员求号
                                            $.get("modifyBallNum",{
                                                playerId:playerId,teamId:teamID,ballNum:ballNum
                                            },function(json){
                                                if(json.flag == 0 && json.code == 0) {
                                                    $success = true;
                                                } else {
                                                    $success = false;
                                                }
                                            })
                                        })
                                        if($success) {
                                            Notify("修改成功", 'top-right', '5000', 'danger', 'fa-desktop', true);
                                        } else {
                                            Notify("修改失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
                                        }
                                    }
                                }
                            }
                        }
                    });
                } else {
                    Notify("该球队还没有队员", 'top-right', '5000', 'danger', 'fa-desktop', true);
                }
            }
        })
    })
    /**
     * 删除之前的判断
     */
    function beforDel(teamId) {
        if (confirm("确定删除？")) {
            window.location.href="delete?teamId="+teamId;
        }
    }
</script>
</body>
<!--  /Body -->
</html>
