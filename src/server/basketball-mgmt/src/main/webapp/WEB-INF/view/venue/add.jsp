<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />

    <style>
        #filePath{
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            background: #EEEEEE;
        }
    </style>

</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">添加球场</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        球场
        <small>
            Venue
        </small>
    </h1>
</div>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">添加球场</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" action="save" method="post">
                    <div class="form-group">
                        <label>图片</label>
                        <div>
                            <input id="fileUpload" name="res" type="file" class="file-loading" >
                        </div>
                            <span class="input-icon icon-right">
                            	<div class="input-group" style="margin-top: 5px;">
                                    <input type="hidden" id="filePath" type="text" name="image" />
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>名称</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" id="name" type="text" name="name" maxlength="18">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>联系电话</label>
                            <span class="input-icon icon-right">
                            	<div class="spinner">
                                    <div class="input-group">
	                            		 <span class="input-group-addon">
	                                        <i class="fa fa-laptop"></i>
	                                    </span>
                                        <input class="form-control" id="phone" name="phone" maxlength="15" type="text">
                                    </div>
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>地址</label>
                            <span class="input-icon icon-right">
                            	<div class="input-group">
                            		 <span class="input-group-addon">
                                        <i class="fa fa-laptop"></i>
                                    </span>
                                    <input class="form-control" name="address" maxlength="50" id="address" type="text">
                                </div>
                            </span>
                    </div>
                    <div class="form-group">
                        <label>球场简介</label>
                        <span class="input-icon icon-right">
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </span>
                                <textarea class="form-control" name="profile" id="profile" maxlength="200"></textarea>
                            </div>
                        </span>
                    </div>
                    <input type="submit" target="iframe"  id="confirmBtn" class="btn btn-info" value="确认">
                </form>
            </div>
        </div>
    </div>
</div>

<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<!--Page Related Scripts-->

<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/laydate/laydate.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<script>
    $("#fileUpload").fileinput({
        language:'zh',
        uploadUrl: '<%=base%>/ctl/resource/upload', // 图片上传接口
        showPreview : true,
        showRemove: false,
        maxFileSize : 10240,  //上传图片的最大限制  50KB
        allowedFileExtensions: ["jpg", "png", "gif","jpeg","bmp"],
        initialCaption: "请选择场地图片"
    });
    $("#fileUpload").on("fileuploaded", function (event, data, previewId, index) {
        if (null != data) {
            fillPath = data.response.data;
            $("#filePath").val(fillPath);
        } else {
            Notify("上传失败", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    });

    $('#confirmBtn').click(function(){
        var jingweiPattern = /[0-9]+.[0-9]+/  //经纬度正则
        var name = $("#name").val();
        var phone = $("#phone").val();
        var address = $("#address").val();
        var image = $("#filePath").val();
        if(name ==""){
            Notify("请填写场地名称", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(phone ==""){
            Notify("请填写联系电话", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(address ==""){
            Notify("请填写场地地址", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
        if(image ==""){
            Notify("请上传场地图片", 'top-right', '5000', 'danger', 'fa-desktop', true);
            return false;
        }
    })
</script>
</body>
<!--  /Body -->
</html>
