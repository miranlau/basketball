<%@ page import="com.renyi.basketball.bussness.po.Admin" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
    //当前管理员类型
    Admin admin = (Admin)request.getSession().getAttribute(Admin.SESSION_ADMIN);
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title></title>

    <meta name="description" content="Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />
    <!--Fonts-->

    <!--Beyond styles-->
    <link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
    <link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

    <link href="../../resource/css/common.css" rel="stylesheet" />
    <script src="../../resource/assets/js/skins.min.js"></script>

    <link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li><a href="">首页</a></li>
        <li class="active">查看场地</li>
    </ul>
</div>
<div class="header-title">
    <h1>
        场地
        <small>
            Venue
        </small>
    </h1>
</div>
<span style="text-align: center;color: red;font-weight: 600">${msg}</span>
<div class="page-body">
    <div class="widget">
        <div class="widget-header ">
            <span class="widget-caption">查看场地</span>
            <div class="widget-buttons">
                <a href="add" target="iframe" class="btn btn-info">创建场地</a>
            </div>
        </div>
        <div class="widget-body">
            <div class="row">
                <div class="col-sm-12">
                    <label>
                        <select class="form-control input-sm" id="type" >
                            <option value="0">球场名称</option>
                        </select>
                    </label>
                    <label>
                        <input type="text" name="name" id="name" class="form-control input-sm" />
                    </label>
                    <label>
                        <a href="javascript:void(0);" class="btn btn-info"  id="qShoppingTemplet"> 查找</a>
                    </label>
                </div>
            </div>
            <table class="table table-striped table-hover" id="simpledatatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>场地</th>
                    <th>图片</th>
                    <th>地址</th>
                    <th>联系电话</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<!--Basic Scripts-->
<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="../../resource/assets/js/bootstrap.min.js"></script>

<!--Beyond Scripts-->
<script src="../../resource/assets/js/beyond.min.js"></script>
<script src="../../resource/assets/js/toastr/toastr.js"></script>
<!--Fuelux Spinner-->
<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
<!--Common Scripts -->
<script src="../../resource/js/commons.js"></script>
<script src="../../resource/js/jqPaginator.js"></script>
<script src="../../resource/js/jqPage.js"></script>
<script src="../../resource/assets/js/bootbox/bootbox.js"></script>

<script>
    var totalpage= '';
    var nowPage = 1;
    var total = '';
    var inputVal = ''; //输入内容
    $(function(){
        addpage(); //加载分页方法
        getData(1);
    })

    function getData(page,inputVal) {
        var listHtml = '';
        $.ajax({
            type: "POST",
            async: true,//异步请求
            url: '<%=base%>/ctl/venue/getAllVenuesPager',
            data: {pageNumber: page, name: inputVal},
            dataType: "json",
            timeout: 5000,
            success: function (json) {
                console.log(json);
                if (json.code == 0 && json.flag == 0) {
                    if (json.data.content.length > 0) {
                        $('#pageCon').show();
                        totalpage = json.data.totalPages;
                        $.each(json.data.content,function(index,obj){

                            listHtml += '<tr>'
                                        +' <td>' + obj.id + '</td>'
                                    +'<td>' + obj.name + '</td>'
                                    +'<td><img src="' + obj.image + '" /></td>'
                                    +' <td>' + obj.address + '</td>'
                                    +'  <td>' + obj.phone + '</td>'
                                    +'  <td>'
                                    +'  <a href="edit?venueId=' + obj.id + '" target="iframe" class="btn btn-info">修改</a>'
                                    +'  <a class="btn btn-danger" onclick="beforDel('+obj.id+')">删除</a>'
                                    +'  </td>'
                                    +'  </tr>';

                        })
                        $("#simpledatatable tbody").html(listHtml);
                        $.jqPaginator('#pagination', {
                            totalPages: totalpage,  //总页数
                            visiblePages: 3,  //可见页面
                            currentPage: page,   //当前页面
                            onPageChange: function (num, type) {
                                nowPage = num;
                                total = json.data.total;
                                $('#showing').text('共'+ total +'条  第' + json.data.pageNumber +'/'+totalpage+'页');
                                if(type != "init"){
                                    inputVal = $("#name").val();
                                    getData(num,inputVal);
                                }
                            }
                        });
                    } else {
                        listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                        $("#simpledatatable tbody").html(listHtml);
                    }
                } else {
                    listHtml = '<tr><td colspan="12" style="text-align: center;color: red;">暂时没有数据</td></tr>';
                    $("#simpledatatable tbody").html(listHtml);
                }
            }
        })
    }
    $("#qShoppingTemplet").click(function(){
        inputVal = $("#name").val();
        getData(1,inputVal);
    })
    
    function beforDel(venueId) {
        if (confirm("确定删除 ?")) {
            window.location.href="delete?venueId="+venueId;
        }
    }

</script>
</body>
<!--  /Body -->
</html>
