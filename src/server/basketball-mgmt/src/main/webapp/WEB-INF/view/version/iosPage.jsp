<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%
	String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
<meta charset="utf-8" />
<title></title>

	<meta name="description" content="Dashboard" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="../../resource/assets/img/favicon.png" type="image/x-icon">

	<!--Basic Styles-->
	<link href="../../resource/assets/css/bootstrap.min.css" rel="stylesheet" />
	<link id="bootstrap-rtl-link" href="" rel="stylesheet" />
	<link href="../../resource/assets/css/font-awesome.min.css" rel="stylesheet" />
	<link href="../../resource/assets/css/weather-icons.min.css" rel="stylesheet" />
	<!--Fonts-->

	<!--Beyond styles-->
	<link href="../../resource/assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
	<link href="../../resource/assets/css/demo.min.css" rel="stylesheet" />
	<link href="../../resource/assets/css/typicons.min.css" rel="stylesheet" />
	<link href="../../resource/assets/css/animate.min.css" rel="stylesheet" />
	<link href="../../resource/assets/css/skins/deepblue.min.css" rel="stylesheet" type="text/css" />

	<link href="../../resource/css/common.css" rel="stylesheet" />
	<script src="../../resource/assets/js/skins.min.js"></script>

	<link href="../../resource/fileupload/css/fileinput.css"  rel="stylesheet" type="text/css" />
<style>
#content {
	height: 200px;
	resize: none;
	overflow: auto;
}

textarea {
	resize: none;
}
.showOld{
	font-weight:600;
	float:left;
}
</style>
</head>
<!-- /Head -->
<!-- Body -->
<body>
<div class="page-breadcrumbs">
	<ul class="breadcrumb">
		<li><a href="">首页</a></li>
		<li class="active">版本更新</li>
	</ul>
</div>
<div class="header-title">
	<h1>
		iOS版本
		<small>
			Version
		</small>
	</h1>
</div>
	<div class="page-body">
		<div class="widget">
		<span style="text-align:center;color:red;font-weight:600"></span><br><br>
					<span class="showOld">当前版本号：${old.versionNumber }</span><br><br>
				<span class="showOld">当前版本code：${old.code }</span><br><br>
				<span class="showOld">当前要求最低版本code：${old.minCode }</span><br><br>
			<div class="widget-header ">
				<span class="widget-caption">iOS版本更新</span>
			</div>
			<form action="update" method="post">
				<div class="widget-body">
					<div class="form-group">
						<label for="version">版本号：</label> <input id="version" type="text"
							name="version" class="form-control input-sm" maxlength="11" />
					</div>
					<div class="form-group">
						<label for="code">版本code：</label>
						<input type="text" class="form-control input-sm" id="code"
							name="code" maxlength="11"/>
					</div>
					<div class="form-group">
						<label for="limitCode">最低要求版本code：</label>
						<input type="text" class="form-control input-sm" id="limitCode"
							name="limitCode" maxlength="11"/>
					</div>
					<div class="form-group">
						<label for="URL">AppStore：</label>
						<div>
							<input id="URL" name="URL" type="text" 
								class="form-control input-sm">
							<input type="hidden" name="type" value="1" maxlength="100"/>
						</div>
					</div>
					<div class="form-group">
						<input type="submit" id="confirmBtn" class="btn btn-azure"
							value="更新" />
					</div>
				</div>
			</form>
		</div>
	</div>
	<!--Basic Scripts-->
	<script src="../../resource/assets/js/jquery-2.0.3.min.js"></script>
	<script src="../../resource/assets/js/bootstrap.min.js"></script>

	<!--Beyond Scripts-->
	<script src="../../resource/assets/js/beyond.min.js"></script>
	<script src="../../resource/assets/js/toastr/toastr.js"></script>
	<!--Fuelux Spinner-->
	<script src="../../resource/assets/js/fuelux/spinner/fuelux.spinner.min.js"></script>

	<script src="../../resource/fileupload/js/fileinput.js" type="text/javascript"></script>
	<script src="../../resource/fileupload/js/fileinput_locale_zh.js" type="text/javascript"></script>
	<!--Common Scripts -->
	<script src="../../resource/js/commons.js"></script>
	<script src="../../resource/js/jqPaginator.js"></script>
	<script src="../../resource/js/jqPage.js"></script>
	<script src="../../resource/assets/js/bootbox/bootbox.js"></script>
	<script type="text/javascript">
 
	    $().ready(function () {
			var msg = '${msg }';
			if (msg) {
				Notify(msg, 'top-right', '5000', 'success', 'fa-desktop', true);
			}
		});

		$('#confirmBtn').click(function(){
			
			var version=$('#version').val();//
			var code=$('#code').val();//
			var limitCode=$('#limitCode').val();//
			var URL=$('#URL').val();//
			var urlStr="",imgArr = [];
				if(version==""){
					Notify("请填写版本号", 'top-right', '5000', 'danger', 'fa-desktop', true);
					return false;
				}
			    
			    if(code==""){
					Notify("请填写版本code", 'top-right', '5000', 'danger', 'fa-desktop', true);
					return false;
				} else if(code <= '${old.code }'){
					Notify("版本code要大于当前", 'top-right', '5000', 'danger', 'fa-desktop', true);
					return false;
				}
			    
			    if(limitCode==""){
					Notify("请填写要求最低版本code", 'top-right', '5000', 'danger', 'fa-desktop', true);
					return false;
				}
			
			    if (!$('#URL')) {
			      	Notify("请先上传apk!", 'top-right', '5000', 'danger', 'fa-desktop', true);
			    	return false;
			    }
		    
		    
		  
			
		})
	</script>
</body>
<!--  /Body -->
</html>
