<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String base = request.getContextPath();
%>
<!DOCTYPE html>
<!--
Beyond Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3
Version: 1.0.0
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!--Head-->
<head>
    <meta charset="utf-8" />
    <title>Login Page</title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<%=base%>/resource/assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<%=base%>/resource/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--Beyond styles-->
    <link id="beyond-link" href="<%=base%>/resource/assets/css/beyond.min.css" rel="stylesheet" />
    <link href="<%=base%>/resource/assets/css/animate.min.css" rel="stylesheet" />


    <style>
        html,body,.login-container{
            width: 100%;
            height: 100%;
        }
        .login-container{
            margin: auto;
        }
        .login-container .loginbox{
            width: 400px!important;
            height: 320px !important;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-top: -250px;
            margin-left: -200px;
        }
        .login-container .loginbox .social-buttons{
            height: auto !important;
        }
        .login-container .loginbox .social-buttons img{
            width: 100%;
            height: 50px;
        }
    </style>
</head>
<!--Head Ends-->
<!--Body-->
<body>
<div class="login-container animated fadeInDown">
    <div class="loginbox bg-white">
        <div class="loginbox-title">全网篮球</div>
        <%--<div class="loginbox-social">--%>
            <%--<div class="social-buttons">--%>
                <%--<img src=" " alt="该位置放置logo  logo高度为50px" />--%>
            <%--</div>--%>
        <%--</div>--%>
        <div class="loginbox-or">
            <div class="or-line"></div>
            <div class="or">OR</div>
        </div>
        <div class="loginbox-textbox">
            <input type="text" class="form-control" placeholder="登录名"  id="userName"/>
            <i class="fa fa-user darkorange"></i>
        </div>
        <div class="loginbox-textbox">
            <input type="password" class="form-control" placeholder="密码" id="userpwd"/>
            <i class="fa fa-unlock-alt darkorange"></i>
        </div>
        <div class="loginbox-submit">
            <input type="button" class="btn btn-primary btn-block" value="登录">
        </div>
    </div>
</div>
<!--Basic Scripts-->
<script src="<%=base%>/resource/assets/js/jquery-2.0.3.min.js"></script>
<script src="<%=base%>/resource/assets/js/bootstrap.min.js"></script>
<script src="<%=base%>/resource/bootstrap/js/beyond.min.js"></script>
<script src="<%=base%>/resource/bootstrap/js/toastr/toastr.js"></script>
<script type="text/javascript">
    function login(){
        var name = $('#userName').val();
        var pwd = $('#userpwd').val();
        if(name!="" && pwd !=""){
            $.ajax({ //调用接口
                type: "GET",
                async : false,  //同步请求
                url:"<%=base%>/ctl/common/login",
                data:{userName:name,password:pwd},
                dataType:"json",
                timeout:5000,
                success:function(result){
                    console.log(result);
                    if(result.type == "success"){
                        location.href = '<%=base%>/ctl/common/index';
                    }else{
                        Notify(result.content, 'top-right', '5000', 'danger', 'fa-desktop', true);
                    }
                }
            })

        }else{
            Notify("请输入用户名或密码", 'top-right', '5000', 'danger', 'fa-desktop', true);
        }
    }

    $('.loginbox-submit input').click(function(){
        login();
    })
</script>
</body>
</html>
