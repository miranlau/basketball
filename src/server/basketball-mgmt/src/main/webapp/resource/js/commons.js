/**
 * Created by wc on 2015/5/17.
 */
var crab = {
		base : 'http://192.168.1.21:8080/crab'
}

//判断值是否为空
function getvalue(val){
    if(val==undefined){
        val='--';
        return val ;
    }else{
        return val;
    }
}
//(function($){
//    $.fn.UI = function(options){
//        var defaults = {
//            iconClass    :    'darkorange',
//            ButtonClass    :    'yellow'
//        };
//
//        $.extend(defaults,options);
//
//        $("i").removeClass("darkorange");
//        $("i").addClass(options.iconClass);
//    }
//})(jQuery);

jQuery.bar = function(options) {
    var defaults = {
        iconClass    :    'darkorange',
        ButtonClass    :    'yellow'
    };

    $.extend(defaults,options);

    $("i").removeClass("darkorange");
    $("i").addClass(options.iconClass);

    $(".btn").removeClass("btn-success");
    $(".btn").addClass(options.ButtonClass);

};
//jQuery.bar = function(param) {
//    alert('This function takes a parameter, which is "' + param + '".');
//};
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//日期转换为时间戳
function getTimeStamp(time){
	time=time.replace(/-/g, '/'); 
	 var date=new Date(time);
	return date.getTime(); 
}
//时间戳转换为日期
function getTime(ns){
	var val=getvalue(ns);
	if(val!='--'){
		//val=val.time;
		var myDate = new Date(val);
		 return myDate.format("yyyy-MM-dd hh:mm:ss");
	}else{
		return '--';
	}
}
//判断是否为整数
function isInt(num) {
    var type = /^[0-9]*[0-9][0-9]*$/;
    var re = new RegExp(type);
    if (num.match(re) == null) {
      return true;
    }
    return false;
}
//$(document).ajaxComplete(function(event, request, settings) {
//	//console.log(request);
//	try{
//		var demo=JSON.parse(request.responseText);
//		if(demo.flag !=0){
//			if(demo.code==401){
//				location.href = '../login.jsp';
//				//Notify(demo.msg, 'top-right', '500', 'danger', 'fa-desktop', true);
//			}else if(demo.code!=0 && demo.msg != ""){
//				Notify(demo.msg, 'top-right', '500', 'danger', 'fa-desktop', true);
//			}
//		}else{
//			if(demo.code!=0){
//				Notify(demo.msg, 'top-right', '500', 'danger', 'fa-desktop', true);
//			}
//
//		}
//	}catch(e){
//		return;
//	}
//
//	//flag:请求成功失败  0,1
//	//code:操作成功失败  0,1
//})

//去掉尾部空格
function trimStr(str){
	return str.replace(/(^\s*)|(\s*$)/g,"");
}
