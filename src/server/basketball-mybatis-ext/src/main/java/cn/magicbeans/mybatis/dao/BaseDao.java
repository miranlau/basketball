package cn.magicbeans.mybatis.dao;


import cn.magicbeans.common.Filter;
import cn.magicbeans.common.Order;
import cn.magicbeans.mybatis.mapper.BaseMapper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by flyong86 on 2016/5/2.
 */
public interface BaseDao<T, ID extends Serializable> {
    void setBaseMapper(BaseMapper mapper);

    T insert(T entity) throws Exception;

    T update(T entity) throws  Exception;

    void delete(Long id);

    List<T> findAll();

    T find (ID id);

    T find(String property, Object value);

    List<T> find(Map<String, Object> params);

    /**
     * 查找实体对象集合
     *
     * @param first
     *            起始记录
     * @param count
     *            数量
     * @param filters
     *            筛选
     * @param orders
     *            排序
     * @return 实体对象集合
     */
    List<T> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);
}
