package cn.magicbeans.mybatis.dao.impl;

import cn.magicbeans.common.Filter;
import cn.magicbeans.common.Order;
import cn.magicbeans.mybatis.dao.BaseDao;
import cn.magicbeans.mybatis.mapper.BaseMapper;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.magicbeans.mybatis.provider.CURDProvider.*;

/**
 * Created by flyong86 on 2016/5/2.
 */
public class BaseDaoImpl<T, ID extends Serializable> implements BaseDao<T, ID> {
    /** 实体类类型 */
    private Class<T> entityClass;
    private BaseMapper baseMapper;

    public void setBaseMapper(BaseMapper baseMapper) {
        this.baseMapper = baseMapper;
    }
    public BaseDaoImpl() {
        Type type = getClass().getGenericSuperclass();
        Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();
        entityClass = (Class<T>) parameterizedType[0];
    }

    @Override
    public T insert(T entity) throws Exception {
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    public T update(T entity) throws Exception {
        baseMapper.update(entity);
        return entity;
    }


    @Override
    public void delete(Long id) {
        String sql = "delete from " + tableName() + " where " + idname() + "=" + id;
        baseMapper.delete(sql);
    }

    @Override
    public List findAll() {
        List<T> list = baseMapper.getAll(tableName());
        return list;
    }

    @Override
    public T find(ID id) {
        return find(idname(), id);
    }

    @Override
    public T find(String property, Object value) {
        String sql = "SELECT * FROM " + tableName() + " WHERE " + property + "='" + value + "'";
        return (T) baseMapper.find(sql);
    }

    @Override
    public List<T> find(Map<String, Object> params) {
        params.put(TABLE_PARAM, tableName());
        return baseMapper.findByProperties(params);
    }

    @Override
    public List<T> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders) {
        Map<String, Object> params = new HashMap<>();
        params.put(PAGE_FIRST,    first);
        params.put(PAGE_COUNT,    count);
        params.put(TABLE_PARAM,   tableName());
        params.put(ORDERS_PARAM,  orders);
        params.put(FILTERS_PARAM, filters);
        return baseMapper.findList(params);
    }


    private String tableName() {
        Table table = entityClass.getAnnotation(Table.class);
        if (null != table) {
            return table.name();
        }

        throw new RuntimeException("undefine POJO @Table, need Tablename(@Table)");
    }

    public String idname() {

        String idname = getIdname(entityClass);
        if (idname != null) {
            return idname;
        }
        throw new RuntimeException("undefine POJO @Id");
    }

    private String getIdname(Class entityClass) {
        String idname = null;
        if (null != entityClass.getSuperclass()) {
            idname = getIdname(entityClass.getSuperclass());
        }

        if (null != idname) {
            return idname;
        }
        Field[] fields = entityClass.getDeclaredFields();
        for(Field field : fields) {
            if(field.isAnnotationPresent(Id.class))
                return field.getName();
        }

        return null;
    }

    public List<String> fieldNames() {
        List<String> fields = new ArrayList<>();
        for(Field field : entityClass.getDeclaredFields()) {
            if(!field.isAnnotationPresent(Transient.class))
                fields.add(field.getName());
        }

        return fields;
    }

    private Map<String, Object> getFiledValues(T entity) throws Exception {
        Map<String, Object> map = new HashMap<>();
        for(Field field : entityClass.getDeclaredFields()) {
            try {
                PropertyDescriptor pd = new PropertyDescriptor(field.getName(), entityClass);
                Method getMethod = pd.getReadMethod();//获得get方法
                if (!getMethod.isAnnotationPresent(Transient.class)) {
                    Object val = getMethod.invoke(entity);//执行get方法返回一个Object
                    if (null != val) {
                        map.put(field.getName(), val);
                    }
                }
            } catch (IntrospectionException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

}
