package cn.magicbeans.mybatis.mapper;

import cn.magicbeans.mybatis.provider.CURDProvider;
import org.apache.ibatis.annotations.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by flyong86 on 2016/5/2.
 */
public interface BaseMapper<T, ID extends Serializable> {

    public final static String BASE_RESULT_MAP = "BaseResultMap";

    @InsertProvider(type = CURDProvider.class, method = "insert")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", resultType = java.lang.Long.class, before=false)
    void insert(T entity);

    @UpdateProvider(type = CURDProvider.class, method = "update")
    void update(T entity);

    @DeleteProvider(type = CURDProvider.class, method = "delete")
    void delete(String sql);

    @SelectProvider(type = CURDProvider.class, method = "getAll")
    @ResultMap(BASE_RESULT_MAP)
    List<T> getAll(String table);

    @SelectProvider(type = CURDProvider.class, method = "find")
    @ResultMap(BASE_RESULT_MAP)
    T find(String sql);

    @SelectProvider(type = CURDProvider.class, method = "findByProperties")
    @ResultMap(BASE_RESULT_MAP)
    List<T> findByProperties(Map<String, Object> params);

    @SelectProvider(type = CURDProvider.class, method = "findList")
    @ResultMap(BASE_RESULT_MAP)
    List<T> findList(Map<String, Object> params);

//    @SelectProvider(type = CURDProvider.class,method = "find")
//    @ResultMap("baseResultMap")
//    T find(@Param("table") String table, @Param("key") String key, @Param("value") Object value);

}
