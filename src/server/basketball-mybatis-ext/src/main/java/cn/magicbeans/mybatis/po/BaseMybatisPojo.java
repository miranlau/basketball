package cn.magicbeans.mybatis.po;


import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Created by flyong86 on 2016/5/2.
 */
public class BaseMybatisPojo implements Serializable {

    /**
     * 获取POJO对应的表名
     * 需要POJO中的属性定义@Table(name)
     * @return
     */
    public String tablename() {
        Table table = this.getClass().getAnnotation(Table.class);
        if(table != null)
            return table.name();
        else
            throw new RuntimeException("undefine POJO @Table, need Tablename(@Table)");
    }

    /**
     * 获取POJO对应的主键名称
     * 需要POJO中的属性定义@Id
     * @return
     */
    public String id() {
        for(Field field : this.getClass().getDeclaredFields()) {
            if(field.isAnnotationPresent(Id.class))
                return field.getName();
        }

        throw new RuntimeException("undefine POJO @Id");

    }

}
