package cn.magicbeans.mybatis.provider;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import cn.magicbeans.common.Filter;
import cn.magicbeans.common.Order;

/**
 * Created by flyong86 on 2016/5/2.
 */
public class CURDProvider<T, ID extends Serializable> {
	private static final String GET_ID = "getId";
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public final static String PAGE_FIRST    = "_first";
    public final static String PAGE_COUNT    = "_count";
    public final static String TABLE_PARAM   = "_TABLE_NAME_";
    public final static String FILTERS_PARAM = "_FILTERS_";
    public final static String ORDERS_PARAM  = "_ORDERS_";
    
    public String insert(T entity) throws Exception {

        Map<String, Object> data = getFiledValues(entity);

        StringBuffer sql = new StringBuffer();
        sql.append("INSERT INTO " + tableName(entity) + "(");

        int index = 0;
        for (String fieldName : data.keySet()) {
            if (index > 0) {
                sql.append(",");
            }
            sql.append(fieldName);

            index ++;
        }
        sql.append(")");

        sql.append("VALUES");
        sql.append("(");
        index = 0;
        for (String fieldName : data.keySet()) {
            if (index > 0) {
                sql.append(",");
            }

            Object val = data.get(fieldName);
            if (val == null) {
            	sql.append("''");
			} else {
				if (val instanceof Boolean) {
	                sql.append(((Boolean) val) ? 1 : 0);
	            } else if (val instanceof Date) {
					val = dateFormat.format(val);
					sql.append("'" + val + "'");
	            } else if (val instanceof Byte || val instanceof Integer || val instanceof Long || val instanceof Float || val instanceof Double) {
	            	sql.append(val);
	            } else {
	            	sql.append("'" + val + "'");
	            }
			}
            
            index ++;
        }
        sql.append(")");

        return sql.toString();
    }

    public String update(T entity) throws Exception {
        StringBuffer sql = new StringBuffer();

        sql.append("UPDATE " + tableName(entity));
        Map<String, Object> data = getFiledValues(entity);
        int index = 0;
        for (String key : data.keySet()) {
            Object value = data.get(key);
            if (value != null) {
                if (value instanceof Boolean) {
                    value = (Boolean) value ? 1 : 0;
                }
            }
            if (value instanceof Date) {
                if (null != value) {
                    value = dateFormat.format(value);
                }
            }
            if (index == 0) {
                sql.append(" SET " + key + "='" + value + "' ");
            } else {
                sql.append("," + key + "='" + value + "'");
            }

            index ++;
        }

        String idname = idname(entity);
        Object idValue = getFieldValue(idname, entity);
        sql.append(" WHERE " + idname + "='" + idValue + "'");
        return sql.toString();
    }



    public String delete(String sql) {
//        String tableName = tableName(clazz);
//        String idname = idname(clazz);
//        String sql = "DELETE FROM " + tableName + " WHERE " + idname + "=#{" + idname + "}";
        return sql;
    }

    public String getAll(String table) {
        return "SELECT * FROM " + table;
    }

    public String find(String sql) {
        return sql;
    }

    public String findByProperties(Map<String, Object> params) {
        String tableName = params.get(TABLE_PARAM).toString();
        params.remove(TABLE_PARAM);
        String sql = "SELECT * FROM " + tableName;
        if (params.size() > 0) {
            int i = 0;
            for (String key : params.keySet()) {
                if (i == 0) {
                    sql += " WHERE ";
                } else {
                    sql += " AND ";
                }

                Object value = params.get(key);
                sql += key + "='" + value + "'";
                i ++;
            }
        }
        return sql;
    }

    public String findList(Map<String, Object> params) {
        StringBuffer sql = new StringBuffer();
        String tableName = params.get(TABLE_PARAM).toString();
        List<Filter> filters = (List<Filter>) params.get(FILTERS_PARAM);
        List<Order> orders = (List<Order>) params.get(ORDERS_PARAM);
        Integer first = (Integer) params.get(PAGE_FIRST);
        Integer size = (Integer) params.get(PAGE_COUNT);

        sql.append("SELECT * FROM " + tableName);
        if (null != filters && !filters.isEmpty()) {
            sql.append(" WHERE ");
            int i = 0;
            for (Filter filter : filters) {
                if (i > 0) {
                    sql.append(" AND ");
                }
                sql.append(build(filter));
                i ++;
            }
        }
        if (null != orders && orders.size() > 0) {
            sql.append(" ORDER BY ");
            int i = 0;
            for (Order order : orders) {
                if (i > 0) {
                    sql.append(", ");
                }

                sql.append(order.getProperty()).append(" ").append(order.getDirection().name());
                i ++;
            }
        }

        if (null != first || null != size) {
            sql.append(" LIMIT ");
            if (null != first) {
                sql.append(first);
            }
            if (null != size) {
                if (null != first) {
                    sql.append(",");
                }
                sql.append(size);
            }
        }
        return sql.toString();
    }


    public String idname(T entity) {
        String idname = idname(entity.getClass());
        if (null != idname) {
            return idname;
        }
        throw new RuntimeException("undefine POJO @Id");
    }

    private String idname(Class entityClass) {
        String idname = null;
        if (null != entityClass.getSuperclass()) {
            idname = idname(entityClass.getSuperclass());
        }

        if (null != idname) {
            return idname;
        }
        Field[] fields = entityClass.getDeclaredFields();
        for(Field field : fields) {
            if(field.isAnnotationPresent(Id.class))
                return field.getName();
        }

        return null;
    }

    private String build(Filter filter) {
        StringBuffer buff = new StringBuffer();
        if (filter.getOperator() == Filter.Operator.eq && filter.getValue() != null) {
            if (filter.getIgnoreCase() != null && filter.getIgnoreCase() && filter.getValue() instanceof String) {
                buff.append(" lower(" + filter.getProperty() + ") = '" + filter.getValue().toString().toLowerCase() + "'");
            } else {
                buff.append(" " + filter.getProperty() + " = '" + filter.getValue() + "'");
            }
        } else if (filter.getOperator() == Filter.Operator.ne && filter.getValue() != null) {
            if (filter.getIgnoreCase() != null && filter.getIgnoreCase() && filter.getValue() instanceof String) {
                buff.append(" lower(" + filter.getProperty() + ") <> '" + filter.getValue().toString().toLowerCase() + "'");
            } else {
                buff.append(" " + filter.getProperty() + " <> '" + filter.getValue() + "'");
            }
        } else if (filter.getOperator() == Filter.Operator.gt && filter.getValue() != null) {
            buff.append(" " + filter.getProperty() + " > " + filter.getValue());

        } else if (filter.getOperator() == Filter.Operator.lt && filter.getValue() != null) {
            buff.append(" " + filter.getProperty() + " < " + filter.getValue());
        } else if (filter.getOperator() == Filter.Operator.ge && filter.getValue() != null) {
            buff.append(" " + filter.getProperty() + " >= " + filter.getValue());
        } else if (filter.getOperator() == Filter.Operator.le && filter.getValue() != null) {
            buff.append(" " + filter.getProperty() + " <= " + filter.getValue());
        } else if (filter.getOperator() == Filter.Operator.like && filter.getValue() != null && filter.getValue() instanceof String) {
            buff.append(" " + filter.getProperty() + " LIKE " + filter.getValue());
        } else if (filter.getOperator() == Filter.Operator.in && filter.getValue() != null) {
            if (filter.getValue().getClass().isArray()) {
//                filter.getValue()
            } else {
                buff.append(" AND " + filter.getProperty() + "='" + filter.getValue() + "'");
            }

        } else if (filter.getOperator() == Filter.Operator.isNull) {
            buff.append(" AND " + filter.getProperty() + " IS NULL");
        } else if (filter.getOperator() == Filter.Operator.isNotNull) {
            buff.append(" AND " + filter.getProperty() + " IS NOT NULL");
        } else if (filter.getOperator() == Filter.Operator.lessThan) {
//            restrictions = criteriaBuilder.lessThan(root.<Date>get(filter.getProperty()), (Date) filter.getValue());
        } else if (filter.getOperator() == Filter.Operator.greaterThan) {
//            restrictions = criteriaBuilder.greaterThan(root.<Date>get(filter.getProperty()), (Date) filter.getValue());
        } else if (filter.getOperator() == Filter.Operator.or) {
            Filter[] orFilters = (Filter[]) filter.getValue();

            buff.append(" (");

            for (int i = 0; i < orFilters.length; i ++) {
                if (i > 0) {
                    buff.append(" OR ");
                }
                buff.append(build(orFilters[i]));
            }
            buff.append(")");
        }

        return buff.toString();
    }

    private Object getFieldValue(String fieldName, T entity) throws Exception {
        Class entityClass = entity.getClass();
        PropertyDescriptor pd = new PropertyDescriptor(fieldName, entityClass);
        Method getMethod = pd.getReadMethod();//获得get方法
        Object val = getMethod.invoke(entity);//执行get方法返回一个Object

        if (null != val) {
            if (val instanceof Boolean) {
                val = (Boolean)val ? 1 : 0;
            }
        }
        return val;
    }

    // FIXME xfenwei 需要重构此方法
    private Map<String, Object> getFiledValues(T entity) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Class entityClass = entity.getClass();
        for(Field field : entityClass.getDeclaredFields()) {
            try {
                PropertyDescriptor pd = new PropertyDescriptor(field.getName(), entityClass);
                Method getMethod = pd.getReadMethod();//获得get方法
                if (!getMethod.isAnnotationPresent(Transient.class)) {
                    Object val = getMethod.invoke(entity);//执行get方法返回一个Object
                    if (null != val) {
                        map.put(replaceUpperCase(field.getName()), val);
                    }
                }
            } catch (IntrospectionException e) {

            }
		}

        // 从BaseEntity中提取createTime和updateTime 
		Class superClass = entity.getClass().getSuperclass();
		Field[] fields = superClass.getDeclaredFields();
		for (Field field : fields) {
			try {
                PropertyDescriptor pd = new PropertyDescriptor(field.getName(), entityClass);
                Method getMethod = pd.getReadMethod();	//获得get方法
                if (getMethod.getName().equals(GET_ID)) {
					continue;
				}
                if (!getMethod.isAnnotationPresent(Transient.class)) {
                    Object val = getMethod.invoke(entity);//执行get方法返回一个Object
                    if (null != val) {
                        map.put(replaceUpperCase(field.getName()), val);
                    }
                }
            } catch (IntrospectionException e) {

            }
		}
		
        return map;
    }

	public String replaceUpperCase(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (Character.isUpperCase(c)) {
				String upperCase = String.valueOf(c);
				str = str.replace(upperCase, "_" + upperCase.toLowerCase());
			}
		}
		return str;
	}
    
    private String tableName(T entity) {
        Table table = (Table) entity.getClass().getAnnotation(Table.class);
        if (null != table) {
            return table.name();
        }

        throw new RuntimeException("undefine POJO @Table, need Tablename(@Table)");
    }


}
