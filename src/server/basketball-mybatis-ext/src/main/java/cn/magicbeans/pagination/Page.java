package cn.magicbeans.pagination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页.
 * Created by flyong86 on 2016/4/15.
 */
public class Page<T> implements Serializable {
    private static final long serialVersionUID = -2958413241403930084L;

    /** 内容 */
    private final List<T> content = new ArrayList<T>();

    /** 分页信息 */
    private final Pageable pageable;

    /**
     * 初始化一个新创建的Page对象
     */
    public Page() {
        this.pageable = new Pageable();
    }

    /**
     * @param content
     *            内容
     *            总记录数
     * @param pageable
     *            分页信息
     */
    public Page(List<T> content, Pageable pageable) {
        this.content.addAll(content);
        this.pageable = pageable;
    }

    /**
     * 获取页码
     *
     * @return 页码
     */
    public int getPageNumber() {
        return pageable.getPageNumber();
    }

    /**
     * 获取每页记录数
     *
     * @return 每页记录数
     */
    public int getPageSize() {
        return pageable.getPageSize();
    }

    /**
     * 获取总页数
     *
     * @return 总页数
     */
    public int getTotalPages() {
        return (int) Math.ceil((double) getTotal() / (double) getPageSize());
    }

    /**
     * 获取内容
     *
     * @return 内容
     */
    public List<T> getContent() {
        return content;
    }

    /**
     * 获取总记录数
     *
     * @return 总记录数
     */
    public long getTotal() {
        return pageable.getTotal();
    }

    /**
     * 获取分页信息
     *
     * @return 分页信息
     */
    public Pageable getPageable() {
        return pageable;
    }
}
