package cn.magicbeans.pagination;

public class PaginationContext {
    private static ThreadLocal<PaginationContext> local = new ThreadLocal() {
        protected PaginationContext initialValue() {
            return new PaginationContext();
        }
    };
    private Pageable pagingParameter;

    public PaginationContext() {
    }

    public Pageable getPagingParameter() {
        return this.pagingParameter;
    }

    public void setPagingParameter(Pageable pagingParameter) {
        this.pagingParameter = pagingParameter;
    }

    public boolean isPaged() {
        return this.pagingParameter != null;
    }

    public static PaginationContext current() {
        return (PaginationContext)local.get();
    }

    public static void clear() {
        PaginationContext current = current();
        current.pagingParameter = null;
    }
}