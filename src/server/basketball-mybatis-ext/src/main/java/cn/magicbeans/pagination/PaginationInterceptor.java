package cn.magicbeans.pagination;

import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 拦截分页处理.
 * Created by flyong86 on 2016/4/15.
 */
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare",  args = {Connection.class})})
public class PaginationInterceptor implements Interceptor {

    public static final ThreadLocal<Page> localPage = new ThreadLocal<Page>();
    private boolean isPaged = false;
    public Object intercept(Invocation invocation) throws Throwable {
        PaginationContext context = PaginationContext.current();
        if(!context.isPaged()) {
            return invocation.proceed();
        } else {
            StatementHandler handler = (StatementHandler)invocation.getTarget();
            Connection connection = (Connection)invocation.getArgs()[0];
            BoundSql boundSql = handler.getBoundSql();
            String originSql = boundSql.getSql();
            Pageable paginationParam = context.getPagingParameter();
            Integer count = this.count(handler, connection, originSql);
            paginationParam.setTotal(count);
            this.replaceWithLimitedSql(handler, originSql, paginationParam);
            return invocation.proceed();
        }
    }

    private void replaceWithLimitedSql(StatementHandler handler, String originSql, Pageable pageable) {
        final Integer offset = (pageable.getPageNumber() - 1) * pageable.getPageSize();
        final Integer length = pageable.getPageSize();
        final ParameterHandler parameterHandler = handler.getParameterHandler();
        final String limitedSql = originSql + " limit ?, ?";
        ParameterHandler limitedParameterHandler = new ParameterHandler() {
            public void setParameters(PreparedStatement ps) throws SQLException {
                parameterHandler.setParameters(ps);
                int count = PaginationInterceptor.this.countParameter(limitedSql);
                ps.setInt(count - 1, offset.intValue());
                ps.setInt(count, length.intValue());
            }

            public Object getParameterObject() {
                return parameterHandler.getParameterObject();
            }
        };
        MetaObject metaObject = SystemMetaObject.forObject(handler);
        metaObject.setValue("boundSql.sql", limitedSql);
        metaObject.setValue("delegate.parameterHandler", limitedParameterHandler);
    }

    private int countParameter(String sql) {
        int count = 0;

        for(int i = 0; i < sql.length(); ++i) {
            if(sql.charAt(i) == 63) {
                ++count;
            }
        }

        return count;
    }

    private Integer count(StatementHandler statementHandler, Connection connection, String originSql) throws SQLException {
        String countingSql = "select count(*) from (" + originSql + ") t";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Integer count = null;

        try {
            pstmt = connection.prepareStatement(countingSql);
            statementHandler.parameterize(pstmt);
            rs = pstmt.executeQuery();
            if(rs.next()) {
                count = Integer.valueOf(rs.getInt(1));
            }
        } finally {
            try {
                rs.close();
                pstmt.close();
            } catch (Exception var14) {
                ;
            }

        }

        return count;
    }

    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    public void setProperties(Properties properties) {
    }
}
