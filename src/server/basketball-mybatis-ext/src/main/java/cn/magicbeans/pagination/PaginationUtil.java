package cn.magicbeans.pagination;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by flyong86 on 2016/4/15.
 */
public class PaginationUtil {

    public static <T> Page<T> pagedQuery(Pageable pageable, Callable<List<T>> command) {
        Page page;
        try {
            PaginationContext e = PaginationContext.current();
            e.setPagingParameter(pageable);
            List data = (List)command.call();
            page = new Page(data, e.getPagingParameter());
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            PaginationContext.clear();
        }

        return page;
    }

}
