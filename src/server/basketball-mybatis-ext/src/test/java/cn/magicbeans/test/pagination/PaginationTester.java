package cn.magicbeans.test.pagination;

import cn.magicbeans.pagination.Page;
import cn.magicbeans.pagination.Pageable;
import cn.magicbeans.pagination.PaginationUtil;
import cn.magicbeans.test.pagination.dao.AccountDao;
import cn.magicbeans.test.pagination.dao.impl.AccountDaoImpl;
import cn.magicbeans.test.pagination.dao.mapper.AccountMapper;
import cn.magicbeans.test.pagination.po.Account;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by flyong86 on 2016/4/15.
 */
public class PaginationTester {

    private Logger logger = Logger.getLogger(getClass());

    public static SqlSessionFactory sqlSessionFactory = null;

    private static boolean initialed = false;

    @Before
    public void setUp() throws Exception {
        if(!initialed) {
            InputStream is = Resources.getResourceAsStream("mybatis.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

            initialed = true;
        }
    }

    @After
    public void tearDown() throws Exception {

    }
    @Test
    public void testPagination() {
        Pageable pageable = new Pageable(2, 1);

        SqlSession session = sqlSessionFactory.openSession();
        final AccountMapper accountMapper = session.getMapper(AccountMapper.class);

        final AccountDaoImpl accountDao = new AccountDaoImpl();
        accountDao.setBaseMapper(accountMapper);
        accountDao.setAccountMapper(accountMapper);
        Page page = PaginationUtil.pagedQuery(pageable, new Callable<List<Account>>() {
            public List<Account> call() throws Exception {
                return accountDao.listAll(true);
            }
        });

        logger.info(page);
    }

    @Test
    public void testBaseMapper() {
        SqlSession session = sqlSessionFactory.openSession(true);
        final AccountMapper accountMapper = session.getMapper(AccountMapper.class);

        AccountDaoImpl accountDao = new AccountDaoImpl();
        accountDao.setBaseMapper(accountMapper);
        accountDao.setAccountMapper(accountMapper);

        // 测试删除
        accountDao.delete(1L);
        session.commit();

        // 测试列表查询
        List list = accountDao.findAll();
        for (Object object : list) {
            logger.info(object);
        }

        // 测试查询单个
        Account account = accountDao.find(3);
        logger.info(account);

        // 条件查询列表
        Map<String, Object> params = new HashMap<>();
        params.put("name", "测试");
        list = accountDao.find(params);
        for (Object object : list) {
            logger.info(object);
        }

        account = new Account();
        account.setName("测试");
        account.setEmail("test@test.com");
        try {
            accountDao.insert(account);
            logger.info("保存数据成功，id=" + account.getId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        account.setEmail("111111111111");
        account.setName("2222222222222");
        try {
            accountDao.update(account);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
