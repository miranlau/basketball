package cn.magicbeans.test.pagination.dao;

import cn.magicbeans.mybatis.dao.BaseDao;
import cn.magicbeans.test.pagination.po.Account;

import java.util.List;

/**
 * Created by flyong86 on 2016/5/2.
 */
public interface AccountDao extends BaseDao<Account, Integer> {

    List<Account> listAll(Boolean enable);

}
