package cn.magicbeans.test.pagination.dao.impl;

import cn.magicbeans.mybatis.dao.impl.BaseDaoImpl;
import cn.magicbeans.test.pagination.dao.AccountDao;
import cn.magicbeans.test.pagination.dao.mapper.AccountMapper;
import cn.magicbeans.mybatis.mapper.BaseMapper;
import cn.magicbeans.test.pagination.po.Account;

import java.util.List;

/**
 * Created by flyong86 on 2016/5/2.
 */
public class AccountDaoImpl extends BaseDaoImpl<Account, Integer> implements AccountDao {

    private AccountMapper accountMapper;

    public void setBaseMapper(BaseMapper mapper) {
        super.setBaseMapper(mapper);
    }

    public void setAccountMapper(AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    @Override
    public List<Account> listAll(Boolean enable) {
        return accountMapper.listAll(enable);
    }

}
