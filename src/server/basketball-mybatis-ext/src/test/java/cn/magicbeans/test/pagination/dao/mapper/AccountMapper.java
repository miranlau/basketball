package cn.magicbeans.test.pagination.dao.mapper;

import cn.magicbeans.mybatis.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by flyong86 on 2016/4/15.
 */
public interface AccountMapper<Account, Long> extends BaseMapper {

    List<Account> listAll(@Param("enabled") Boolean enable);

}
