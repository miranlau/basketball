package cn.magicbeans.test.pagination.po;

import cn.magicbeans.mybatis.po.BaseMybatisPojo;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by flyong86 on 2016/4/15.
 */
@Table(name = "account")
public class Account extends BaseEntity {

    private static final long serialVersionUID = -374120758898879978L;
    private String name;
    private String email;
    private Boolean enabled;
    private Date createTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
