package cn.magicbeans.test.pagination.po;

import cn.magicbeans.mybatis.po.BaseMybatisPojo;

import javax.persistence.Id;

/**
 * Created by flyong86 on 2016/5/6.
 */
public class BaseEntity extends BaseMybatisPojo {
    @Id
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
